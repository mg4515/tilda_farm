<?php
	//include
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
	
	//object
	$oSiteProDraw=new cSiteProDraw();
	
?>
<!DOCTYPE html>
<html lang="fa">
   <head>
		<?= 
			$oSiteProDraw->HeadMetaTag([
				'title'=>'سبد خرید'
			]); 
		?>

		<!-- StyleSheet -->
		<?php include_once "contents/content_css.php"; ?>

		<!-- js header Scripts -->
		<?php include_once "contents/content_js_header.php"; ?>	
		
   </head>

   <body class="bg-body cart">
		<section class="intro">
			<!-- Header -->
			<?php 
				include_once "contents/content_header.php"; 
			?>
		</section>

    <section class="container sm-t sp-t">
      <!-- Header -->
      <header
        class="section-header d-flex flex-row justify-content-center position-relative mb-3"
      >
        <div class="k-bl c-dark title">سبد خرید</div>
      </header>

      <div class="row">
        <!-- Order List -->
        <aside class="col-lg-8 mb-3 mb-md-0">
          <div class="card border-0 p-3">
            <!-- Header -->
            <div class="row ol-header d-none d-md-flex align-items-center py-3">
              <aside class="col-1 col-md-1 text-center ir-b c-regular">
                #
              </aside>
              <aside class="col-10 col-md-4 text-center ir-b c-regular">
                محصول
              </aside>
              <aside class="col-6 col-md-2 text-center ir-b c-regular">
                مقدار (کیلوگرم)
              </aside>
              <aside class="col-6 col-md-2 text-center ir-b c-regular">
                تعداد
              </aside>
              <aside class="col-md-2 text-center ir-b c-regular">
                قیمت (تومان)
              </aside>
              <aside class="col-md-1"></aside>

            </div>
            
            <hr class="my-0 d-none d-md-block" />

            <div class="row ol-content py-3 d-flex align-items-center">
              <!-- Responsive Mode -->
              <div class="col-4 d-block d-md-none">
                <a
                  class="text-decoration-none ir-r fs-small c-regular"
                  href="./product-detail.html"
                >
                  <img
                    class="d-block w-100 border border-secondary rounded-sm"
                    src="<?= $oPath->asset('client/img/product-1-tb.png') ?>"
                    alt="عنوان محصول"
                  />
                </a>
              </div>
              <div class="col-8 d-block d-md-none">
                <a
                  class="text-decoration-none ir-r fs-small text-right mb-3 c-regular d-block d-md-none"
                  href="#"
                >
                  برنج دم سیاه درجه یک آقاجانیان
                </a>

                <div class="d-flex-row justify-content-center mb-3">
                  <!-- Amount -->
                  <div class="selection ml-auto w-50 mb-3">
                    <select class="form-control shadow-none ir-r" name="" id="">
                      <option value="10">10</option>
                      <option value="20">20</option>
                      <option value="50">50</option>
                    </select>
                  </div>

                  <!-- Quantity -->
                  <div
                    class="quantity text-center c-regular d-flex align-items-center justify-content-start"
                  >
                    <span
                      class="ir-r text-center text-white q-btn rounded-sm increase-btn ml-2"
                      >+</span
                    >
                    <input
                      class="ir-r form-control text-center q-input shadow-none"
                      type="text"
                      value="1"
                      placeholder="مقدار"
                    />
                    <span
                      class="ir-r text-center text-white q-btn rounded-sm decrease-btn mr-2"
                      >-</span
                    >
                  </div>
                </div>

                <!-- Price -->
                <div class="price clearfix w-100 mb-3">
                  <p
                    class="currently-price d-block float-right ir-b ml-2 text-success"
                  >
                    150,000 تومان
                  </p>
                  <p
                    class="last-price d-block float-right ir-b text-decoration-line text-danger"
                  >
                    200,000 تومان
                  </p>
                </div>

                <!-- Delete -->
                <div
                  class="cursor-pointer delete-order ir-r d-flex flex-row justify-content-start align-items-center text-right mb-0 text-danger"
                >
                  <i class="icon-trash ml-1"></i>
                  <span>حذف</span>
                </div>
              </div>

              <!-- Desktop Mode -->
              <aside
                class="col-2 col-md-1 text-center c-regular ir-r d-none d-md-block"
              >
                1
              </aside>

              <aside
                class="col-10 col-md-4 text-center c-regular d-none d-md-block"
              >
                <a
                  class="text-decoration-none ir-r fs-small c-regular"
                  href="./product-detail.html"
                >
                  <div class="row">
                    <div class="col-3">
                      <img
                        class="d-block w-100 border border-secondary rounded-sm"
                        src="<?= $oPath->asset('client/img/product-1-tb.png') ?>"
                        alt="عنوان محصول"
                      />
                    </div>
                    <div class="col-9 align-self-center text-right">
                      برنج دم سیاه درجه یک آقاجانیان
                    </div>
                  </div>
                </a>
              </aside>

              <aside
                class="col-6 col-md-2 text-center c-regular align-items-center justify-content-center d-none d-md-flex"
              >
                <select class="form-control shadow-none ir-r" name="" id="">
                  <option value="10">10</option>
                  <option value="20">20</option>
                  <option value="50">50</option>
                </select>
              </aside>

              <aside
                class="col-md-2 text-center c-regular align-items-center justify-content-center d-none d-md-flex"
              >
                <span
                  class="ir-r text-center text-white q-btn rounded-sm increase-btn ml-2"
                  >+</span
                >
                <input
                  class="ir-r form-control text-center q-input shadow-none"
                  type="text"
                  value="1"
                  placeholder="مقدار"
                />
                <span
                  class="ir-r text-center text-white q-btn rounded-sm decrease-btn mr-2"
                  >-</span
                >
              </aside>

              <aside
                class="col-6 col-md-2 text-center c-regular d-none d-md-block c-regular ir-r"
              >
                180,000
              </aside>

              <aside
                class="col-md-1 d-none d-md-flex flex-row justify-content-center align-items-center"
              >
                <div
                  class="cursor-pointer delete-order ir-r d-flex flex-row justify-content-start align-items-center text-right mb-0 text-danger"
                >
                  <i class="icon-trash ml-1"></i>
                </div>
              </aside>

            </div>
            
            <hr class="my-0 separator" />

            <div class="row ol-content py-3 d-flex align-items-center">
              <!-- Responsive Mode -->
              <div class="col-4 d-block d-md-none">
                <a
                  class="text-decoration-none ir-r fs-small c-regular"
                  href="./product-detail.html"
                >
                  <img
                    class="d-block w-100 border border-secondary rounded-sm"
                    src="../assets/img/product-1-tb.png"
                    alt="عنوان محصول"
                  />
                </a>
              </div>
              <div class="col-8 d-block d-md-none">
                <a
                  class="text-decoration-none ir-r fs-small text-right mb-3 c-regular d-block d-md-none"
                  href="./product-detail.html"
                >
                  برنج دم سیاه درجه یک آقاجانیان
                </a>

                <div class="d-flex-row justify-content-center mb-3">
                  <!-- Amount -->
                  <div class="selection ml-auto w-50 mb-3">
                    <select class="form-control shadow-none ir-r" name="" id="">
                      <option value="10">10</option>
                      <option value="20">20</option>
                      <option value="50">50</option>
                    </select>
                  </div>

                  <!-- Quantity -->
                  <div
                    class="quantity text-center c-regular d-flex align-items-center justify-content-start"
                  >
                    <span
                      class="ir-r text-center text-white q-btn rounded-sm increase-btn ml-2"
                      >+</span
                    >
                    <input
                      class="ir-r form-control text-center q-input shadow-none"
                      type="text"
                      value="1"
                      placeholder="مقدار"
                    />
                    <span
                      class="ir-r text-center text-white q-btn rounded-sm decrease-btn mr-2"
                      >-</span
                    >
                  </div>
                </div>

                <!-- Price -->
                <div class="price clearfix w-100 mb-3">
                  <p
                    class="currently-price d-block float-right ir-b ml-2 text-success"
                  >
                    150,000 تومان
                  </p>
                  <p
                    class="last-price d-block float-right ir-b text-decoration-line text-danger"
                  >
                    200,000 تومان
                  </p>
                </div>

                <!-- Delete -->
                <div
                  class="cursor-pointer delete-order ir-r d-flex flex-row justify-content-start align-items-center text-right mb-0 text-danger"
                >
                  <i class="icon-trash ml-1"></i>
                  <span>حذف</span>
                </div>
              </div>

              <!-- Desktop Mode -->
              <aside
                class="col-2 col-md-1 text-center c-regular ir-r d-none d-md-block"
              >
                1
              </aside>

              <aside
                class="col-10 col-md-4 text-center c-regular d-none d-md-block"
              >
                <a
                  class="text-decoration-none ir-r fs-small c-regular"
                  href="#"
                >
                  <div class="row">
                    <div class="col-3">
                      <img
                        class="d-block w-100 border border-secondary rounded-sm"
                        src="<?= $oPath->asset('client/img/product-1-tb.png') ?>"
                        alt="عنوان محصول"
                      />
                    </div>
                    <div class="col-9 align-self-center text-right">
                      برنج دم سیاه درجه یک آقاجانیان
                    </div>
                  </div>
                </a>
              </aside>

              <aside
                class="col-6 col-md-2 text-center c-regular align-items-center justify-content-center d-none d-md-flex"
              >
                <select class="form-control shadow-none ir-r" name="" id="">
                  <option value="10">10</option>
                  <option value="20">20</option>
                  <option value="50">50</option>
                </select>
              </aside>

              <aside
                class="col-md-2 text-center c-regular align-items-center justify-content-center d-none d-md-flex"
              >
                <span
                  class="ir-r text-center text-white q-btn rounded-sm increase-btn ml-2"
                  >+</span
                >
                <input
                  class="ir-r form-control text-center q-input shadow-none"
                  type="text"
                  value="1"
                  placeholder="مقدار"
                />
                <span
                  class="ir-r text-center text-white q-btn rounded-sm decrease-btn mr-2"
                  >-</span
                >
              </aside>

              <aside
                class="col-6 col-md-2 text-center c-regular d-none d-md-block c-regular ir-r"
              >
                180,000
              </aside>

              <aside
                class="col-md-1 d-none d-md-flex flex-row justify-content-center align-items-center"
              >
                <div
                  class="cursor-pointer delete-order ir-r d-flex flex-row justify-content-start align-items-center text-right mb-0 text-danger"
                >
                  <i class="icon-trash ml-1"></i>
                </div>
              </aside>
          </div>
        </aside>

        <!-- Go to Checkout -->
        <aside class="col-lg-4">
          <div class="card p-3 mb-3">
            <div class="d-flex justify-content-between align-items-center py-3">
              <span class="ir-r fs-small c-regular">قیمت کالا (ها)</span>
              <span class="ir-r fs-small c-regular">500,000 تومان</span>
            </div>

            <hr class="my-0 separator" />

            <div class="d-flex justify-content-between align-items-center py-3">
              <span class="ir-r fs-small c-regular">جمع</span>
              <span class="ir-r fs-small c-regular">500,000 تومان</span>
            </div>

            <hr class="my-0 separator" />

            <div class="d-flex justify-content-between align-items-center py-3">
              <span class="ir-b fs-small c-regular">مبلغ قابل پرداخت</span>
              <span class="ir-b fs-small c-regular">500,000 تومان</span>
            </div>
          </div>

          <a class="btn btn-primary ir-r d-block w-100" href="checkout"
            >ادامه فرایند خرید</a
          >
        </aside>
      </div>
    </section>

		<!-- Footer -->
		<?php include_once "contents/content_footer.php"; ?>

    <!-- Login Modal -->
    <div
      class="modal login-modal fade"
      id="loginModal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="loginModalLabel"
      aria-hidden="true"
    >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- Header -->
          <div class="modal-header border-0 pb-0">
            <h5 class="modal-title k-bl c-dark" id="loginModalLabel">
              ورود
            </h5>
          </div>

          <!-- Body -->
          <div class="modal-body">
            <form action="">
              <!-- Phone Number -->
              <div class="form-group border border-secondary rounded p-2 mb-3">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="phoneNumber"
                  >شماره موبایل</label
                >

                <!-- Input -->
                <input
                  type="tel"
                  pattern="[09][0-9]{9}"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="phoneNumber"
                  placeholder="09120000000"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Password -->
              <div class="form-group border border-secondary rounded p-2 mb-2">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="password"
                  >رمز عبور</label
                >

                <!-- Input -->
                <input
                  type="password"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="password"
                  placeholder="رمز عبور خود را وارد کنید"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Forget Password Link -->
              <a
                class="d-block text-right c-label fs-regular ir-r mb-3 text-decoration-none"
                href="./forget-password.html"
                >بازیابی رمز عبور</a
              >

              <!-- Login Button -->
              <button
                class="btn btn-primary ir-r d-block mb-3 px-5"
                type="submit"
              >
                ورود
              </button>

              <!-- Register Link -->
              <span class="d-block text-right ir-r fs-regular"
                >عضو نیستید؟
                <a class="text-decoration-none" href="register"
                  >ثبت نام کنید</a
                ></span
              >
            </form>
          </div>
        </div>
      </div>
    </div>

		<!-- Scripts -->
		<script src="../assets/js/jquery-3.5.0.min.js"></script>
		<script src="<?= $oPath->asset("client/js/bootstrap.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/popper.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/smoothproducts.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/main.js") ?>"></script>
      <script>
			// Increase Button
			$(".increase-btn").on("click", function () {
			  let input = $(this).siblings("input");
			  let value = input.val();
			  input.val(parseInt(value) + 1);
			});

			// Decrease Button
			$(".decrease-btn").on("click", function () {
			  let input = $(this).siblings("input");
			  let value = input.val();
			  value <= 1 ? input.val(1) : input.val(parseInt(value) - 1);
			});

			$(".q-input").on("change", function () {
			  let value = $(this).val();
			  if (value <= 1) $(this).val(1);
			});

			$(".delete-order").on("click", function () {
			  $(this).parents(".ol-content").prev('hr.separator').remove();
			  $(this).parents(".ol-content").remove();
			});
      </script>
  </body>
</html>

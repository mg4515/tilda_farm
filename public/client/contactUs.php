<?php
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
	$oSiteProDraw=new cSiteProDraw();
?>

<!DOCTYPE html>
<html lang="fa">
   <head>
		<?= 
			$oSiteProDraw->HeadMetaTag([
				'title'=>'ارتباط با ما'
			]); 
		?>

		<!-- Web Font -->
		<!-- <link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet"> -->
		
		<!-- StyleSheet -->
		<?php include_once "contents/content_css.php"; ?>

		<!-- js header Scripts -->
		<?php include_once "contents/content_js_header.php"; ?>	
		
   </head>

   <body class="bg-body">
		<section class="intro">
			<!-- Header -->
			<?php 
				include_once "contents/content_header.php"; 
			?>
		</section>

       <!-- Contact Us -->
      <section class="contact-us container sp-t">
			<!-- Header -->
			<header class="section-header d-flex flex-row justify-content-between position-relative mb-4">
			   <div class="k-bl c-dark text-right title">تماس با ما</div>
			</header>

			<div class="row content">
			   <!-- Form -->
			   <aside class="col-md-5 offset-md-2 mb-5 mb-md-0">
				   <form method="post" onsubmit='return false'>
						<div id='spn_alert' style='width:100%;direction:rtl;text-align: right;'></div>				
					
						<!-- Full Name -->
						<div class="bg-white form-group border border-secondary rounded p-2 mb-3">
							<!-- Lable -->
							<label class="d-block text-right ir-r c-label mb-0 fs-small" for="txt_subject">موضوع</label>

							<!-- Input -->
							<input type="text" class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="txt_subject" placeholder="موضوع پیغام" />

							<!-- Error Message -->
							<small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام ارور</small>
						</div>					
					
						<!-- Full Name -->
						<div class="bg-white form-group border border-secondary rounded p-2 mb-3">
							<!-- Lable -->
							<label class="d-block text-right ir-r c-label mb-0 fs-small" for="txt_name">نام و نام خانوادگی</label>

							<!-- Input -->
							<input type="text" class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="txt_name" placeholder="نام و نام خانوادگی" />

							<!-- Error Message -->
							<small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام ارور</small>
						</div>

						<!-- Email -->
						<div class="bg-white form-group border border-secondary rounded p-2 mb-3" >
							<!-- Lable -->
							<label class="d-block text-right ir-r c-label mb-0 fs-small" for="txt_mail" >ایمیل</label>

							<!-- Input -->
							<input type="email" class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="txt_mail" placeholder="your-email@example.com"/>

							<!-- Error Message -->
							<small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger" >پیام ارور</small>
						</div>
						
						<!-- mobile -->
						<div class="bg-white form-group border border-secondary rounded p-2 mb-3" >
							<!-- Lable -->
							<label class="d-block text-right ir-r c-label mb-0 fs-small" for="txt_mobile" >موبایل</label>

							<!-- Input -->
							<input type="" class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="txt_mobile" placeholder="09xxxxxxxxx"/>

							<!-- Error Message -->
							<small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger" >پیام ارور</small>
						</div>						

						<!-- message -->
						<div class="bg-white form-group border border-secondary rounded p-2 mb-3" >
							<!-- Lable -->
							<label class="d-block text-right ir-r c-label mb-0 fs-small" for="txt_message">پیام شما</label>

							<!-- Textarea -->
							<textarea class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="txt_message" rows="3" placeholder="لطفا نظر یا انتقاد خود را وارد کنید..."></textarea>

						   <!-- Error Message -->
						   <small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام ارور</small>
						</div>

						<!-- sec -->
						<div class="bg-white form-group border border-secondary rounded p-2 mb-3" >
							<!-- Lable -->
							<label class="d-block text-right ir-r c-label mb-0 fs-small" for="txt_message">کد امنیتی</label>

							<!-- text -->
							<input type="text"  id="txt_sec" placeholder="کد موجود در تصویر" style="text-align: center" onkeypress="return runScript(event)" >

							<!-- img -->
							<?php $t=time(); ?>
							<img src="<?= $oPath->public_("client/contents/security/sec_contactUs.php") . "?t={$t}" ?>" id="img_sec" style=""/>
							<i id="btn_refresh" class='fa fa-refresh' onclick="secImageRefresh();"></i><br><br>
							<script>
								function secImageRefresh()
								{
									var d = new Date();
									var t = d.getTime();
									document.getElementById('img_sec').setAttribute("src","<?= $oPath->public_("client/contents/security/sec_contactUs.php") ?>?t=" + t);
								}												
							</script>
						</div>
						                 
						<button id='btn_send' type="submit" class="btn btn-success ir-r d-block px-5" onclick='contactUs_send();'>ارسال</button>
				   </form>
			   </aside>

			   <aside class="col-md-5 d-flex flex-column justify-content-between">
				   <!-- Phone Number -->
				   <div class="connection-way row mb-3 mb-md-0">
						<div class="col-3">
						  <i
							 class="icon-phone mx-auto d-inline-block fs-icon text-center rounded-circle icon ml-3"
						  ></i>
						</div>

						<div class="col-9 d-flex align-items-center">
							<a class="d-block text-right ir-r c-regular text-decoration-none link" href="tel:<?= $oSiteProDraw->mobile() ?>">
								<?= $oSiteProDraw->mobile(false) ?>
							</a>
						</div>
				   </div>

				   <!-- Address -->
				   <div class="connection-way row mb-3 mb-md-0">
						<div class="col-3">
						  <i
							 class="icon-map-marker mx-auto d-inline-block fs-icon text-center rounded-circle icon ml-3"
						  ></i>
						</div>

						<div class="col-9 d-flex align-items-center">
							<a class="d-block text-right ir-r c-regular text-decoration-none link" href="https://goo.gl/maps/bVbpZjeq39ZADuwW6">
								آدرس : <?= $oSiteProDraw->addressCo() ?>
							</a>
						</div>
				   </div>

				   <!-- Instagram Account -->
				   <div class="connection-way row mb-3 mb-md-0">
						<div class="col-3">
						  <i
							 class="icon-instagram mx-auto d-inline-block fs-icon text-center rounded-circle icon ml-3"
						  ></i>
						</div>

						<div class="col-9 d-flex align-items-center">
							<a class="d-block text-right ir-r c-regular text-decoration-none link" href="<?= $oSiteProDraw->instagram() ?>">
								instagram
							</a>
						</div>
				   </div>

				   <!-- Telegram Account -->
				   <div class="connection-way row mb-0">
						<div class="col-3">
						  <i
							 class="icon-telegram mx-auto d-inline-block fs-icon text-center rounded-circle icon ml-3"
						  ></i>
						</div>

						<div class="col-9 d-flex align-items-center">
							<a class="d-block text-right ir-r c-regular text-decoration-none link" href="<?= $oSiteProDraw->telegram() ?>">
								telegram
							</a>
						</div>
				   </div>
			   </aside>
			</div>
      </section>

		<!-- Footer -->
		<?php include_once "contents/content_footer.php"; ?>

    <!-- Login Modal -->
    <div
      class="modal login-modal fade"
      id="loginModal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="loginModalLabel"
      aria-hidden="true"
    >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- Header -->
          <div class="modal-header border-0 pb-0">
            <h5 class="modal-title k-bl c-dark" id="loginModalLabel">
              ورود
            </h5>
          </div>

          <!-- Body -->
          <div class="modal-body">
            <form action="">
              <!-- Phone Number -->
              <div class="form-group border border-secondary rounded p-2 mb-3">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="phoneNumber"
                  >شماره موبایل</label
                >

                <!-- Input -->
                <input
                  type="tel"
                  pattern="[09][0-9]{9}"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="phoneNumber"
                  placeholder="09120000000"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Password -->
              <div class="form-group border border-secondary rounded p-2 mb-2">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="password"
                  >رمز عبور</label
                >

                <!-- Input -->
                <input
                  type="password"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="password"
                  placeholder="رمز عبور خود را وارد کنید"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Forget Password Link -->
              <a
                class="d-block text-right c-label fs-regular ir-r mb-3 text-decoration-none"
                href="./forget-password.html"
                >بازیابی رمز عبور</a
              >

              <!-- Login Button -->
              <button
                class="btn btn-primary ir-r d-block mb-3 px-5"
                type="submit"
              >
                ورود
              </button>

              <!-- Register Link -->
              <span class="d-block text-right ir-r fs-regular"
                >عضو نیستید؟
                <a class="text-decoration-none" href="register"
                  >ثبت نام کنید</a
                ></span
              >
            </form>
          </div>
        </div>
      </div>
    </div>

		<!-- Scripts -->
		<script src="../assets/js/jquery-3.5.0.min.js"></script>
		<script src="<?= $oPath->asset("client/js/bootstrap.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/popper.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/smoothproducts.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/main.js") ?>"></script>
		<script>
			$(".next-btn").on("click", function () {
			  $(".step.step-1, .step-content.step-1").removeClass("active");
			  $(".step.step-2, .step-content.step-2").addClass("active");
			});

			$(".prev-btn").on("click", function () {
			  $(".step.step-1, .step-content.step-1").addClass("active");
			  $(".step.step-2, .step-content.step-2").removeClass("active");
			});
		</script>
   </body>
</html>

<?php
   //include
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
   include_once $oPath->manageDir('sliders_bundle/controller/php/sliders_draw.php');	
   include_once $oPath->manageDir('shop_bundle/controller/php/shop_draw.php');
   include_once $oPath->manageDir('blog_bundle/controller/php/blog_draw.php');
	
	//object
	$oSiteProDraw=new cSiteProDraw();
	$oSlidersDraw=new cSlidersDraw();
	$oShopDraw=new cShopDraw();
	$oBlogDraw=new cBlogDraw();
?>

<!DOCTYPE html>
<html lang="fa">
   <head>
      <?= $oSiteProDraw->HeadMetaTag(); ?>

		<!-- StyleSheet -->
		<?php include_once "contents/content_css.php"; ?>
   </head>

   <body class="bg-body index">
      <section class="intro">

			<!-- Header -->
			<?php 
				include_once "contents/content_header.php"; 
			?>

			<div class="owl-carousel owl-theme intro-carousel position-relative w-100 h-100">
			   <?= $oSlidersDraw->drawForHome() ?>
			</div>
		</section>

      <!-- Latest Products -->
      <section class="container sp-t">
         <header class="section-header d-flex flex-row justify-content-between position-relative mb-3">
				<div class="k-bl c-dark title">جدیدترین محصولات</div>
				<a href="items?byNew" class="more ir-r t-regular c-regular text-decoration-none">موارد بیشتر</a>
				<hr class="position-absolute m-0 w-100" />
         </header>
			<div class="row">
            <?= $oShopDraw->itemsDraw_forIndex('byNew',4) ?>
			</div>
      </section>

      <!-- Best Seller Products -->
      <section class="container sp-t">
         <header class="section-header d-flex flex-row justify-content-between position-relative mb-3">
				<div class="k-bl c-dark title">پرفروش‌ترین محصولات</div>
				<a href="items?bySell" class="more ir-r t-regular c-regular text-decoration-none">موارد بیشتر</a>
				<hr class="position-absolute m-0 w-100" />
         </header>
			<div class="row">
            <?= $oShopDraw->itemsDraw_forIndex('byTopSell',4) ?>
			</div>
      </section>

      <!-- Blog -->
      <section class="container sp-t">
         <!-- Header -->
         <header class="section-header d-flex flex-row justify-content-between position-relative mb-3">
            <div class="k-bl c-dark title">آخرین مطالب</div>
			   <a href="blogs" class="more ir-r t-regular c-regular text-decoration-none">موارد بیشتر</a>
            <hr class="position-absolute m-0 w-100" />
         </header>

         <!-- Content -->
         <div class="row">
            <?= $oBlogDraw->itemsDraw_forIndex(3) ?>
         </div>
      </section>

		<!-- Footer -->
		<?php include_once "contents/content_footer.php"; ?>

      <!-- Login Modal -->
      <div
			class="modal login-modal fade"
			id="loginModal"
			tabindex="-1"
			role="dialog"
			aria-labelledby="loginModalLabel"
			aria-hidden="true"
      >
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<!-- Header -->
					<div class="modal-header border-0 pb-0">
						<h5 class="modal-title k-bl c-dark" id="loginModalLabel">
						  ورود
						</h5>
					</div>

					<!-- Body -->
					<div class="modal-body">
						<form action="">
							<!-- Phone Number -->
							<div class="form-group border border-secondary rounded p-2 mb-3">
								<!-- Lable -->
								<label class="d-block text-right ir-r c-label mb-0 fs-small" for="phoneNumber">شماره موبایل</label>
								
								<!-- Input -->
								<input
									type="tel"
									pattern="[09][0-9]{9}"
									class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
									id="phoneNumber"
									placeholder="09120000000"
								/>

								<!-- Error Message -->
								<small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام ارور</small>
							</div>

							<!-- Password -->
							<div class="form-group border border-secondary rounded p-2 mb-2">
								<!-- Lable -->
								<label class="d-block text-right ir-r c-label mb-0 fs-small" for="password">رمز عبور</label>

								<!-- Input -->
								<input
									type="password"
									class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
									id="password"
									placeholder="رمز عبور خود را وارد کنید"
								/>

								<!-- Error Message -->
								<small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام ارور</small>
							</div>

							<!-- Forget Password Link -->
							<a class="d-block text-right c-label fs-regular ir-r mb-3 text-decoration-none" href="./forget-password.html">بازیابی رمز عبور</a>

							<!-- Login Button -->
							<button class="btn btn-primary ir-r d-block mb-3 px-5" type="submit">ورود</button>

							<!-- Register Link -->
							<span class="d-block text-right ir-r fs-regular">
								عضو نیستید؟
								<a class="text-decoration-none" href="register">ثبت نام کنید</a>
							</span>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- js Scripts -->
		<?php include_once "contents/content_js_footer.php"; ?>   
		<script>
			$(".intro-carousel").owlCarousel({
				rtl: true,
				loop: true,
				margin: 10,
				nav: true,
				dots: false,
				responsive: {
					0: {items: 1},
					600: {items: 1},
					1000: {items: 1},
				},
			});
		</script>		
   </body>
</html>

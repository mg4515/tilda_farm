<?php
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
	$oSiteProDraw=new cSiteProDraw();
?>

<!-- Footer -->
<footer class="pages-footer container-fluid sp-t sm-t bg-footer">
	<div class="row px-3 px-md-5">
	   <!-- Our Company -->
	   <aside class="col-md-3 mb-5 mb-md-0">
			<h3 class="d-block ir-b text-right fs-big c-grey-3 mb-2">شرکت ما</h3>

			<a href="contactUs" class="d-block text-right ir-r fs-regular c-regular text-decoration-none mb-2">تماس با ما</a>
			<a href="about" class="d-block text-right ir-r fs-regular c-regular text-decoration-none mb-2">درباره ما</a>
			<a href="help" class="d-block text-right ir-r fs-regular c-regular text-decoration-none mb-0">رهنما</a>
	   </aside>

	   <!-- Services -->
	   <aside class="col-md-3 mb-5 mb-md-0">
		   <h3 class="d-block ir-b text-right fs-big c-grey-3 mb-2">خدمات</h3>

		   <a href="items" target="_blank" class="d-block text-right ir-r fs-regular c-regular text-decoration-none mb-2">محصولات</a>
		   <a href="#" class="d-block text-right ir-r fs-regular c-regular text-decoration-none mb-0">فروشندگان</a>
	   </aside>

		<aside class="col-md-3 mb-5 mb-md-0">
			<!-- Support -->
			<h3 class="d-block ir-b text-right fs-big c-grey-3 mb-2">حمایت</h3>
			<a href="#" class="d-block text-right ir-r fs-regular c-regular text-decoration-none mb-5">سوالات متداول</a>

			<!-- Social -->
			<h3 class="d-block ir-b text-right fs-big c-grey-3 mb-2">ما را دنبال کنید</h3>

			<!-- Links Box -->
			<div class="social-box clear-fix">
			   <?= $oSiteProDraw->socialDraw_forFooter() ?>
			</div>
		</aside>

	  <!-- Trust Links -->
	  <aside class="col-md-3 mb-5 mb-md-0 trust-links">
		 <a class="d-block float-right trust-item" href="#">
			<img
			  class="d-block w-75 mx-auto"
			  src="<?= $oPath->asset("client/img/trust-1.png") ?>"
			  alt="ای نماد"
			/>
		 </a>

		 <a class="d-block float-right trust-item" href="#">
			<img
			  class="d-block w-75 mx-auto"
			  src="<?= $oPath->asset("client/img/trust-2.png") ?>"
			  alt="ای نماد"
			/>
		 </a>

		 <a class="d-block float-right trust-item" href="#">
			<img
			  class="d-block w-75 mx-auto"
			  src="<?= $oPath->asset("client/img/trust-3.png") ?>"
			  alt="ای نماد"
			/>
		 </a>
	  </aside>
	</div>

	<!-- CopyRight Text -->
	<div class="row py-2 copy-right mt-5 bg-green">
	   <span class="d-block text-center ir-r text-white w-100">
		   <?= $oSiteProDraw->copyRight('text-white text-decoration-none') ?>
		</span>
	</div>
</footer>
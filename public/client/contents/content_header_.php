<?php
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
   include_once $oPath->manageDir('shop_bundle/controller/php/shop_draw.php');
	$oSiteProDraw=new cSiteProDraw();
	$oShopDraw=new cShopDraw();
?>
<header class="header shop">
	<!-- Topbar -->
	<div class="topbar">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-12 col-12">
					<!-- Top Left -->
					<div class="top-left">
						<ul class="list-main">
							<li><i class="ti-headphone-alt"></i><?= $oSiteProDraw->mobile() ?></li>
							<li><i class="ti-email"></i><?= $oSiteProDraw->mail() ?></li>
						</ul>
					</div>
					<!--/ End Top Left -->
				</div>
				<div class="col-lg-8 col-md-12 col-12">
					<!-- Top Right -->
					<div class="right-content">
						<ul class="list-main">
							<li style='display:none;'><i class="ti-location-pin"></i>مکان فروشگاه</li>
							<li><i class="ti-ticket"></i> <a href="tracking">رهگیری خرید</a></li>
							<?php
							if(isset($_SESSION['user_id']))
							{
								echo '
							   <li><i class="ti-user"></i> <a href="user?page=desktop">محیط کاربری</a></li>
							   <li><i class="ti-power-off"></i><a href="javascript:void(0)" onclick="user_logout();">خروج</a></li>
								';								
							}
							else
							{
								echo '
							   <li><i class="ti-lock"></i> <a href="register">ثبت نام بازاریاب</a></li>
							   <li><i class="ti-unlock"></i><a href="login">ورود بازاریاب</a></li>
								';
							}
							?>
						</ul>
					</div>
					<!-- End Top Right -->
				</div>
			</div>
		</div>
	</div>
	<!-- End Topbar -->
	<div class="middle-inner">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-12">
					<!-- Logo -->
					<div class="logo">
						<a href="index.html">
						   <?= $oSiteProDraw->logo() ?>
							<span><?= $oSiteProDraw->title() ?></span>
						</a>
					</div>
					<!--/ End Logo -->
					<!-- Search Form -->
					<div class="search-top">
						<div class="top-search"><a href="#0"><i class="ti-search"></i></a></div>
						<!-- Search Form -->
						<div class="search-top">
							<form class="search-form"  onsubmit='oTools.link("items?searchWord=" + $("#txt_search_").val());return false;'>
								<input type="text" placeholder="جستجو ..." id="txt_search_">
								<button value="search" type="submit"><i class="ti-search"></i></button>
							</form>
						</div>
						<!--/ End Search Form -->
					</div>
					<!--/ End Search Form -->
					<div class="mobile-nav"></div>
				</div>
				<div class="col-lg-6 col-md-6 col-12">
					<div class="search-bar-top">
						<div class="search-bar">
							<select id='slct_searchCat'>
								<option value='0' selected>همه دسته ها</option>
								<?= $oShopDraw->groupOption() ?>
							</select>
							<form onsubmit='oTools.link("items?searchWord=" + $("#txt_search").val() + "&group=" + $("#slct_searchCat").val());return false;'>
								<input name="searchWord" id='txt_search' placeholder="جستجو ..." type="search">
								<button class="btnn" type="submit"><i class="fa fa-search"></i></button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-12">
					<div class="right-bar">
						<ul class="list-main">
							<li style='display:none;'><i class="ti-location-pin"></i>مکان فروشگاه</li>
							<li><i class="ti-ticket"></i> <a href="tracking">رهگیری خرید</a></li>
							<?php
							if(isset($_SESSION['user_id']))
							{
								echo '
							   <li><i class="ti-user"></i> <a href="user?page=desktop">محیط کاربری</a></li>
							   <li><i class="ti-power-off"></i><a href="javascript:void(0)" onclick="user_logout();">خروج</a></li>
								';								
							}
							else
							{
								echo '
							   <li><i class="ti-lock"></i> <a href="register">ثبت نام بازاریاب</a></li>
							   <li><i class="ti-unlock"></i><a href="login">ورود بازاریاب</a></li>
								';
							}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Header Inner -->
	<div class="header-inner">
		<div class="container">
			<div class="cat-nav-head">
				<div class="row">
					<div class="col-lg-9 col-12">
						<div class="menu-area">
							<!-- Main Menu -->
							<nav class="navbar navbar-expand-lg">
								<div class="navbar-collapse">	
									<div class="nav-inner">	
										<ul class="nav main-menu menu navbar-nav">
											<li class="<?= $nav_menu_home_active ?>"><a href="index">خانه</a></li>																							
											<li class="<?= $nav_menu_contactUs_active ?>"><a href="contactUs">ارتباط با ما</a></li>
											<li class="<?= $nav_menu_about_active ?>"><a href="about">درباره ما</a></li>
											<li class="<?= $nav_menu_about_active ?>"><a href="help">راهنما</a></li>
										</ul>
									</div>
								</div>
							</nav>
							<!--/ End Main Menu -->	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<?php
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
   include_once $oPath->manageDir('shop_bundle/controller/php/shop_draw.php');
	$oSiteProDraw=new cSiteProDraw();
	$oShopDraw=new cShopDraw();
?>

<!-- Header -->
<header class="pages-header position-fixed w-100">
	<nav class="navbar w-100 navbar-expand-lg navbar-light px-md-5 py-0">
		<a class="navbar-brand" href="index">
			<?= $oSiteProDraw->logo('',30) ?>
		</a>
		<button
			class="navbar-toggler"
			type="button"
			data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent"
			aria-expanded="false"
			aria-label="Toggle navigation"
		>
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item pr-0 pr-md-4">
					<a class="nav-link ir-r" href="index">خانه</a>
				</li>

				<li class="nav-item dropdown pr-0 pr-md-4">
					<a
						class="nav-link toggle ir-r"
						href="items"
						id="navbarDropdown"
						role="button"
						data-toggle="dropdown"
						aria-haspopup="true"
						aria-expanded="false"
					>
						محصولات
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					   <?= $oShopDraw->groupLink('dropdown-item ir-r text-right') ?>
					</div>
				</li>

				<li class="nav-item pr-0 pr-md-4">
					<a class="nav-link ir-r" href="blogs">وبلاگ</a>
				</li>

				<li class="nav-item pr-0 pr-md-4">
					<a class="nav-link ir-r" href="contactUs">تماس با ما</a>
				</li>
			</ul>
		</div>

		<div class="buttons">
			<!-- Login Button -->
			<?php
			if(isset($_SESSION['user_id']))
			{
				echo '<a href="user?desktop" class="btn btn-primary ir-r d-block float-left login-btn mr-3">محیط کاربری</a>';								
			}
			else
			{
				echo '
				<button
				   class="btn btn-primary ir-r d-block float-left login-btn mr-3"
				   data-toggle="modal"
				   data-target="#loginModal"
				>
				   ورود/ثبت نام
				</button>
				';
			}
			?>			

			<!-- Cart -->
			<a class="d-block float-left nav-link p-0 text-decoration-none mr-3 cart-btn position-relative" href="cart">
				<!-- Cart's Icon -->
				<i class="icon-cart nav-link p-0 icon d-block float-left"></i>

				<!-- Order Counter -->
				<span class="counter ir-r text-white rounded-circle position-absolute text-center">0</span>
			</a>

			<!-- Search Button -->
			<i class="search-btn float-left d-block icon-search nav-link p-0 icon cursor-pointer"></i>
		</div>
	</nav>

	<div class="search-box position-fixed w-100 px-md-5 bg-white">
		<form class="d-block w-100 h-100" action="items">
			<input
			  type="search"
			  class="ir-r text-right form-control shadow-none rounded-0 border-0 d-block float-right h-100 p-0 search-input"
			  placeholder="دنبال چه چیزی می‌گردید؟ برنج، نام فروشنده، کشت اول و..."
			  name="s"
			  id="s"
			/>

			<i class="icon-close cursor-pointer close-btn d-block float-right text-center"></i>
		</form>
	</div>
</header>
<!-- Jquery -->
<script src="<?= $oPath->asset('client/js/jquery-migrate-3.0.0.js') ?>"></script>
<!-- Scripts -->
<script src="<?= $oPath->asset('client/js/jquery-3.5.0.min.js') ?>"></script>
<script src="<?= $oPath->asset('client/js/bootstrap.min.js') ?>"></script>
<script src="<?= $oPath->asset('client/js/popper.min.js') ?>"></script>
<script src="<?= $oPath->asset('client/js/owl.carousel.js') ?>"></script>
<script src="<?= $oPath->asset('client/js/main.js') ?>"></script>

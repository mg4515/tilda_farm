<section class="shop-newsletter section">
	<div class="container">
		<div class="inner-top">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 col-12">
					<!-- Start Newsletter Inner -->
					<div class="inner">
						<h4>رهگیری خرید</h4>
						<p>با <span>کد رهگیری</span> ، خرید های خود را مشاهده نمایید</p>
						<form action="tracking" method="get" target="_blank" class="newsletter-inner">
							<input name="i" placeholder="کد رهگیری" required="" type="">
							<button class="btn">رهگیری</button>
						</form>
					</div>
					<!-- End Newsletter Inner -->
				</div>
			</div>
		</div>
	</div>
</section>
<!--  engine.js -->
<script type='text/javascript'><?php include_once $oPath->engineDir('engine.js.php'); ?></script> 

<!-- Jquery -->
<script type='text/javascript' src="<?= $oPath->asset('default/js/jquery.min.js') ?>"></script>

<!-- compressor -->
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/compressor/jsxcompressor.min.js') ?>"></script>

<!-- default script -->
<script src="<?= $oPath->asset('default/js/script.js') ?>"></script>

<!--  manage ajax -->
<script src="<?= $oPath->manage('contactUs_bundle/controller/js/contactUs_ajax.js') ?>"></script>
<script src="<?= $oPath->manage('blog_bundle/controller/js/blog_ajax.js') ?>"></script>
<script src="<?= $oPath->manage('users_bundle/controller/js/users_ajax.js') ?>"></script>
<script src="<?= $oPath->manage('shop_bundle/controller/js/shop_ajax.js') ?>"></script>

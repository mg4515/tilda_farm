<link rel="stylesheet" href="<?= $oPath->asset('client/css/reset.min.css') ?>">
<link rel="stylesheet" href="<?= $oPath->asset('client/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= $oPath->asset('client/fontIcons/style.css') ?>">
<link rel="stylesheet" href="<?= $oPath->asset('client/fontAwesome/css/font-awesome.min.css') ?>">
<link rel="stylesheet" href="<?= $oPath->asset('client/css/owl.carousel.min.css') ?>">
<link rel="stylesheet" href="<?= $oPath->asset('client/css/owl.theme.default.min.css') ?>">
<link rel="stylesheet" href="<?= $oPath->asset('client/css/main.css') ?>">
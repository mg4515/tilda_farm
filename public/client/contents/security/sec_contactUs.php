<?php
   @session_start();
   include_once $_SESSION["engineRequire"];
   require_once $oPath->manageDir("security_bundle/securityImage/securityImage.php");
   $securityImage = new securityImage();
   $securityImage->font= $oPath->assetDir("default/fonts/Shabnam-Bold-FD.ttf");   
   $securityImage->fontSize=14;
   $securityImage->bgColor(255,255,255);
   $securityImage->color(100,100,100);

   $securityImage->securityImage_create(120,40,4,"sec_contactUs");
?>
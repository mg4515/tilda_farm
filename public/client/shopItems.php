<?php
   //include
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
   include_once $oPath->manageDir('shop_bundle/controller/php/shop_draw.php');
	
	//object
	$oSiteProDraw=new cSiteProDraw();
	$oShopDraw=new cShopDraw();
	
	//request
	$groupId=cDataBase::escape(@$_REQUEST['g']);
	$groupId=str_replace("|"," ",$groupId);
	
	$byVisits=cDataBase::escape(@$_REQUEST['byVisits']); //'' or 0 or 1
	$byNew=cDataBase::escape(@$_REQUEST['byNew']); //'' or 0 or 1
	$bySell=cDataBase::escape(@$_REQUEST['bySell']); //'' or 0 or 1
	$s=cDataBase::escape(@$_REQUEST['s']); //'' or 'word'
	$tag=cDataBase::escape(@$_REQUEST['tag']); //'' or 'tag title'
	$type=cDataBase::escape(@$_REQUEST['type']); 
	$kesht=cDataBase::escape(@$_REQUEST['kesht']); 
	$kood=cDataBase::escape(@$_REQUEST['kood']); 
	$page=cDataBase::escape(@$_REQUEST['page']); 
	if($byVisits==1) $byVisits=true;
	if($byNew==1) $byNew=true;
	if($bySell==1) $bySell=true;
		
	$items=$oShopDraw->itemsDraw([
	   'groupId'=>$groupId,
		'sortByVisits'=>$byVisits,
		'sortByRow'=>$byNew,
		'sortBySell'=>$bySell,
		'searchWord'=>$s,
		'tagTitle'=>$tag,
		'type'=>$type,
		'kesht'=>$kesht,
		'kood'=>$kood,
		'page'=>$page
	]);
	
?>

<!DOCTYPE html>
<html lang="fa">
   <head>
	   <!-- meta & title & ... -->
      <?= $oSiteProDraw->HeadMetaTag('auto','محصولات'); ?>

	   <!-- StyleSheet -->
	   <?php include_once "contents/content_css.php"; ?>
		
	   <!-- js Scripts -->
	   <?php include_once "contents/content_js_header.php"; ?> 		
		<script src="<?= $oPath->asset('client/js/jquery-3.5.0.min.js') ?>"></script>
   </head>

   <body class="bg-body">
		<section class="intro">
			<!-- Header -->
			<?php 
				include_once "contents/content_header.php"; 
			?>
		</section>
    
      <section class="container sp-t products">
			<div class="row">
			   <aside class="col-md-3">
				   <!-- Filters -->
				   <div class="filters">
						<i class="d-block d-lg-none icon-close icon position-absolute close-btn"></i>
						<!-- Header -->
						<span class="filter-btn fs-regular d-none d-md-block text-white bg-primary text-right pr-2 ir-r">فیلترها</span>

						<!-- Categories -->
						<div class="categories">
						   <span class="d-block mb-2 text-right user-select-none fs-small ir-b c-dark">دسته بندی ها</span>
						   <div id='div_groupChek'>
							   <?= $items['groupCode'] ?>
                     </div>
							
						   <hr class="my-3" />

						   <span class="d-block mb-2 text-right user-select-none fs-small ir-b c-dark">زمان کشت</span>

						   <div class="form-check">
							   <input class="form-check-input invisible" type="checkbox" value="" id="item_3" />
							   <label class="check-box-label d-block text-right ml-3 pr-2" for="item_3">
									<!-- Checking -->
									<i class="icon-check d-inline-block rounded-sm"></i>
									<span class="d-inline-block ir-r fs-small">کشت اول</span>
							   </label>
						   </div>

						   <div class="form-check">
							   <input class="form-check-input invisible" type="checkbox" value="" id="item_4" />
							   <label class="check-box-label d-block text-right ml-3 pr-2" for="item_4">
									<!-- Checking -->
									<i class="icon-check d-inline-block rounded-sm"></i>
									<span class="d-inline-block ir-r fs-small">کشت دوم</span>
							   </label>
						  </div>

						  <hr class="my-3" />

						  <span class="d-block mb-2 text-right user-select-none fs-small ir-b c-dark">اندازه</span>

						   <div class="form-check">
							   <input class="form-check-input invisible" type="checkbox" value="" id="item_5" />
							   <label class="check-box-label d-block text-right ml-3 pr-2" for="item_5">
									<!-- Checking -->
									<i class="icon-check d-inline-block rounded-sm"></i>

									<span class="d-inline-block ir-r fs-small">نیم دانه</span>
							   </label>
						   </div>

						   <div class="form-check">
							   <input class="form-check-input invisible" type="checkbox" value="" id="item_6" />
							   <label class="check-box-label d-block text-right ml-3 pr-2" for="item_6">
									<!-- Checking -->
									<i class="icon-check d-inline-block rounded-sm"></i>

									<span class="d-inline-block ir-r fs-small">کامل</span>
							   </label>
						   </div>
						</div>

						<button type="submit" class="btn btn-danger rounded-0 submit-btn ir-r w-100 d-block" onclick='filter();'><i class='icon icon-search'></i>&nbsp;اعمال فیلتر ها</button>
				      <script>
						   function filter()
							{
								var g='';
								var $chks=$('#div_groupChek input[type="checkbox"]');
								for(i=0; i < $chks.length; i++)
								{
									if($chks[i].checked==true)
									{										
										g+=$chks[i].value;
									   if(i < $chks.length-1) g+= "|";
									}
								}
								oTools.link("items?g=" + $chks[0].value);
							}
						</script>
					</div>
			   </aside>

			   <!-- Result -->
			   <aside class="col-md-9 result">
				   <!-- Header -->
				   <header class="header mb-3 d-flex flex-column justify-content-center">
						<div>
						   <h3 class="d-block text-center mb-2 k-bl text-white">محصولات</h3>
						   <!-- Search Box -->
						   <div class="p-search-box mx-auto position-relative">
							   <form action="">
									<!-- Search Input -->
									<input name='s' type="search" placeholder="به دنبال چه محصولی می‌گردید؟ جستجو کنید" class="d-block w-100 bg-white s-input form-control p-0 ir-r shadow-none outline-0 text-right fs-small pr-2"/>

									<!-- Search Button -->
									<button type="submit" class="btn s-btn p-0 position-absolute shadow-none"><i class="icon-search d-block text-center c-regular"></i></button>
							   </form>
						   </div>
						</div>
				   </header>

				   <!-- Filter Button -->
				   <span class="filter-btn fs-regular cursor-pointer d-block d-md-none btn-primary text-center ir-r mb-3">فیلترها</span>

				   <!-- Content -->
				   <div class="row">
					   <?= $items['code'] ?>	
				   </div>

				   <!-- Pagination -->
				   <div class="row d-flex flex-row justify-content-center mt-3">
                  <?= $items['pageCode'] ?>
				   </div>
			   </aside>
			</div>
      </section>

		<!-- Footer -->
		<?php include_once "contents/content_footer.php"; ?>      

      <!-- Login Modal -->
		<div
			class="modal login-modal fade"
			id="loginModal"
			tabindex="-1"
			role="dialog"
			aria-labelledby="loginModalLabel"
			aria-hidden="true"
		>
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- Header -->
          <div class="modal-header border-0 pb-0">
            <h5 class="modal-title k-bl c-dark" id="loginModalLabel">
              ورود
            </h5>
          </div>

          <!-- Body -->
          <div class="modal-body">
            <form action="">
              <!-- Phone Number -->
              <div class="form-group border border-secondary rounded p-2 mb-3">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="phoneNumber"
                  >شماره موبایل</label
                >

                <!-- Input -->
                <input
                  type="tel"
                  pattern="[09][0-9]{9}"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="phoneNumber"
                  placeholder="09120000000"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Password -->
              <div class="form-group border border-secondary rounded p-2 mb-2">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="password"
                  >رمز عبور</label
                >

                <!-- Input -->
                <input
                  type="password"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="password"
                  placeholder="رمز عبور خود را وارد کنید"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Forget Password Link -->
              <a
                class="d-block text-right c-label fs-regular ir-r mb-3 text-decoration-none"
                href="./forget-password.html"
                >بازیابی رمز عبور</a
              >

              <!-- Login Button -->
              <button
                class="btn btn-primary ir-r d-block mb-3 px-5"
                type="submit"
              >
                ورود
              </button>

              <!-- Register Link -->
              <span class="d-block text-right ir-r fs-regular"
                >عضو نیستید؟
                <a class="text-decoration-none" href="register"
                  >ثبت نام کنید</a
                ></span
              >
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery-3.5.0.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/main.js"></script>
    <script></script>
  </body>
</html>

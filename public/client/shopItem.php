<?php
	//include
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
   include_once $oPath->manageDir('shop_bundle/controller/php/shop_draw.php');
	
	//object
	$oSiteProDraw=new cSiteProDraw();
	$oShopDraw=new cShopDraw();
	
	//request
	$id=cDataBase::escape(@$_REQUEST['i']);
	$userMarketerId=cDataBase::escape(@$_REQUEST['uid']);
	
	//item
	$item=@$oShopDraw->item($id);
	if(!$item)
	{
	   header("Location:404");
		exit;	
	}	
	
	//link
	if($userMarketerId)
	{
	   $payLink="pay?i={$id}&uid={$userMarketerId}";
		$_SESSION['shop_userMarketerId']=$userMarketerId;
	}
   else
	   $payLink="pay?i={$id}";
	
?>
<!DOCTYPE html>
<html lang="fa">

<head>
   <?= 
		$oSiteProDraw->HeadMetaTag([
			'title'=>$item->title,
			'description'=>$item->comment,
			'keywords'=>$item->keywords,
			'addingAddress'=>"blog?i={$item->id}"
		]); 
	?>

	<!-- StyleSheet -->
	<?php include_once "contents/content_css.php"; ?>
   <link rel="stylesheet" href="<?= $oPath->asset('client/css/smoothproducts.css') ?>" />
   <link rel="stylesheet" href="<?= $oPath->asset('client/css/starrr.css') ?>" />
	
	<!-- js Scripts -->
	<?php include_once "contents/content_js_header.php"; ?> 
   <script src="<?= $oPath->asset('client/js/jquery-3.5.0.min.js') ?>"></script>
</head>

<body class="bg-body product-details">
	<section class="intro">
		<!-- Header -->
		<?php 
			include_once "contents/content_header.php"; 
		?>
	</section>

	<!-- Intro -->
	<section class="product-intro container sp-t mb-3">
	   <div class="row">
			<!-- Product Image Gallery -->
			<aside class="col-md-4 mb-3 mb-md-0">
				<div class="sp-loading">
					<img src="<?= $oPath->asset('client/img/sp-loading.gif') ?>" alt="">
					<br>
					LOADING IMAGES
				</div>
				<?= $item->images_html ?>
			</aside>

			<!-- Details -->
			<aside class="col-md-8 mb-0 d-flex flex-column justify-content-center">
				<!-- Title -->
				<h3 class="d-block text-center ir-b mb-3 c-dark"><?= $item->title ?></h3>

				<!-- Stars -->
				<div class="stars d-flex flex-row justify-content-center mb-3">
					<i class="c-star ml-1 fs-big icon-star-o"></i>
					<i class="c-star ml-1 fs-big icon-star-o"></i>
					<i class="c-star ml-1 fs-big icon-star"></i>
					<i class="c-star ml-1 fs-big icon-star"></i>
					<i class="c-star ml-0 fs-big icon-star"></i>
				</div>

				<!-- Details -->
				<div class="details mb-3">
					<span class="d-block text-center ir-r fs-regular">محصول <?= $item->city ?></span>
					<span class="d-block text-center ir-r fs-regular">معطر</span>
					<span class="d-block text-center ir-r fs-regular"><?= $item->property_keshtTitle ?></span>				
					<span class="d-block text-center ir-r fs-regular">وزن <?= $item->vaznTitle ?></span>				
				</div>

				<!-- Prices -->
				<div class="prices d-flex flex-row justify-content-center mb-3">
					<span class="ir-r fs-big text-success new-price"><?= $item->price ?></span>
					<span class="ir-r fs-big text-danger last-price text-decoration-line"><?= $item->priceOld ?></span>
				</div>

				<!-- Add to Cart -->
				<div class="add-to-cart d-flex flex-row justify-content-center ml-3">
				   <select class="ir-r custom-select w-25 ml-3">
						<option value="1" selected>1</option>
						<option value="2">2</option>
						<option value="3">3</option>
				   </select>
					<button type="submit" class="btn btn-success ir-r px-4">افزودن به سبد خرید</button>
				</div>
			</aside>
	   </div>
	</section>

   <!-- Product Features -->
   <section class="product-features container sm-t">
      <div class="row">
			<aside class="col-6 col-md-4 col-lg-2 mb-3 mb-lg-0">
				<img class="d-block w-100 mb-3" src="<?= $item->property_saleKeshtImage ?>" alt="">
				<span class="d-block text-center ir-r c-regular mb-0"><?= $item->property_saleKeshtTitle ?></span>
			</aside>

			<aside class="col-6 col-md-4 col-lg-2 mb-3 mb-lg-0">
				<img class="d-block w-100 mb-3" src="<?= $item->property_atrImage ?>" alt="">
				<span class="d-block text-center ir-r c-regular mb-3"><?= $item->property_atrTitle ?></span>
			</aside>

			<aside class="col-6 col-md-4 col-lg-2 mb-3 mb-lg-0">
				<img class="d-block w-100 mb-3" src="<?= $item->property_reyImage ?>" alt="">
				<span class="d-block text-center ir-r c-regular mb-3"><?= $item->property_reyTitle ?></span>
			</aside>

			<aside class="col-6 col-md-4 col-lg-2 mb-3 mb-lg-0">
				<img class="d-block w-100 mb-3" src="<?= $item->property_tamImage ?>" alt="">
				<span class="d-block text-center ir-r c-regular mb-3"><?= $item->property_tamTitle ?></span>
			</aside>

			<aside class="col-6 col-md-4 col-lg-2 mb-3 mb-lg-0">
				<img class="d-block w-100 mb-3" src="<?= $item->property_keshtImage ?>" alt="">
				<span class="d-block text-center ir-r c-regular mb-0"><?= $item->property_keshtTitle ?></span>
			</aside>
      </div>
   </section>

   <!-- About Farmer -->
   <section class="other-products container sm-t">
      <header class="section-header d-flex flex-row justify-content-between position-relative">
         <div class="k-bl c-dark title">درباره‌ی کشاورز این محصول</div>
         <hr class="position-absolute m-0 w-100" />
      </header>

      <div class="row">
			<!-- Farmer Biography -->
			<aside class="col-md-4 mb-3 mb-md-0">
				<div class="farmer-bio position-relative">
					<!-- Farmer's Image -->
					<img class="farmer-img rounded-sm position-absolute" src="<?= $item->seller_image ?>" alt="">

					<!-- Farmer's Name -->
					<span class="d-block text-center ir-b fs-regular c-dark mb-3">آقای <?= $item->seller_title ?></span>

					<!-- Passage -->
					<p class="d-block text-center ir-r fs-regular c-regular">
						<?= $item->seller_comment ?>
					</p>
				</div>
			</aside>

			<!-- Farmer's Products -->
			<aside class="col-md-8">
				<div class="owl-carousel farmer-products owl-theme">
				   <?= $item->mortabet_div ?>
				</div>
			</aside>
      </div>
    </section>

    <!-- Comments -->
    <section class="container comments sm-t">
        <!-- Header -->
        <header class="section-header d-flex flex-row justify-content-between position-relative mb-3">
            <div class="k-bl c-dark title">نظرات</div>
            <hr class="position-absolute m-0 w-100" />
        </header>

        <!-- Content -->
        <hr class="my-3">

        <div class="row">
            <aside class="col-md-5 offset-md-1 mb-3 mb-md-0">
                <span class="d-block ir-r fs-regular text-right c-regular mb-3">علی اسماعیلی</span>
                <p class="d-block ir-r fs-regular text-justify c-regular mb-3">لورم ایپسوم متن ساختگی با تولید سادگی
                    نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و
                    سطرآنچنان که لازم است</p>
                <i class="d-block ir-r fs-small text-right c-grey mb-0 font-italic"> 1398/2/2</i>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <span class="d-block text-right c-regular ir-r m-0">عطر</span>
                <span class="d-block text-right c-regular ir-r m-0">طعم</span>
                <span class="d-block text-right c-regular ir-r m-0">ری کشیدن</span>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>
            </aside>
        </div>

        <hr class="my-3">

        <div class="row">
            <aside class="col-md-5 offset-md-1 mb-3 mb-md-0">
                <span class="d-block ir-r fs-regular text-right c-regular mb-3">علی اسماعیلی</span>
                <p class="d-block ir-r fs-regular text-justify c-regular mb-3">لورم ایپسوم متن ساختگی با تولید سادگی
                    نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و
                    سطرآنچنان که لازم است</p>
                <i class="d-block ir-r fs-small text-right c-grey mb-0 font-italic"> 1398/2/2</i>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <span class="d-block text-right c-regular ir-r m-0">عطر</span>
                <span class="d-block text-right c-regular ir-r m-0">طعم</span>
                <span class="d-block text-right c-regular ir-r m-0">ری کشیدن</span>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>
            </aside>
        </div>

        <hr class="my-3">

        <div class="row">
            <aside class="col-md-5 offset-md-1 mb-3 mb-md-0">
                <span class="d-block ir-r fs-regular text-right c-regular mb-3">علی اسماعیلی</span>
                <p class="d-block ir-r fs-regular text-justify c-regular mb-3">لورم ایپسوم متن ساختگی با تولید سادگی
                    نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و
                    سطرآنچنان که لازم است</p>
                <i class="d-block ir-r fs-small text-right c-grey mb-0 font-italic"> 1398/2/2</i>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <span class="d-block text-right c-regular ir-r m-0">عطر</span>
                <span class="d-block text-right c-regular ir-r m-0">طعم</span>
                <span class="d-block text-right c-regular ir-r m-0">ری کشیدن</span>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>
            </aside>
        </div>

        <hr class="my-3">

        <div class="row">
            <aside class="col-md-5 offset-md-1 mb-3 mb-md-0">
                <span class="d-block ir-r fs-regular text-right c-regular mb-3">علی اسماعیلی</span>
                <p class="d-block ir-r fs-regular text-justify c-regular mb-3">لورم ایپسوم متن ساختگی با تولید سادگی
                    نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و
                    سطرآنچنان که لازم است</p>
                <i class="d-block ir-r fs-small text-right c-grey mb-0 font-italic"> 1398/2/2</i>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <span class="d-block text-right c-regular ir-r m-0">عطر</span>
                <span class="d-block text-right c-regular ir-r m-0">طعم</span>
                <span class="d-block text-right c-regular ir-r m-0">ری کشیدن</span>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>
            </aside>
        </div>

        <hr class="my-3">

        <div class="row">
            <aside class="col-md-5 offset-md-1 mb-3 mb-md-0">
                <span class="d-block ir-r fs-regular text-right c-regular mb-3">علی اسماعیلی</span>
                <p class="d-block ir-r fs-regular text-justify c-regular mb-3">لورم ایپسوم متن ساختگی با تولید سادگی
                    نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و
                    سطرآنچنان که لازم است</p>
                <i class="d-block ir-r fs-small text-right c-grey mb-0 font-italic"> 1398/2/2</i>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <span class="d-block text-right c-regular ir-r m-0">عطر</span>
                <span class="d-block text-right c-regular ir-r m-0">طعم</span>
                <span class="d-block text-right c-regular ir-r m-0">ری کشیدن</span>
            </aside>
            <aside class="col-6 col-md-3 d-flex flex-column justify-content-between">
                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>

                <div class="stars d-flex flex-row justify-content-start">
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star-o"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-1 icon-star"></i>
                    <i class="c-star ml-0 icon-star"></i>
                </div>
            </aside>
        </div>

        <!-- Pagination -->
        <div class="row d-flex flex-row justify-content-center mt-3 pt-3">
            <nav class="navigation" aria-label="Page navigation example" dir="ltr">
                <ul class="pagination">
                    <li class="page-item shadow-none ml-3">
                        <a class="page-link border-0 c-regular ir-r" href="#">صفحه قبل</a>
                    </li>
                    <li class="page-item shadow-none ml-3 active">
                        <a class="page-link border-0 c-regular ir-r" href="#">1</a>
                    </li>
                    <li class="page-item shadow-none ml-3">
                        <a class="page-link border-0 c-regular ir-r" href="#">2</a>
                    </li>
                    <li class="page-item shadow-none ml-3">
                        <a class="page-link border-0 c-regular ir-r" href="#">3</a>
                    </li>
                    <li class="page-item shadow-none">
                        <a class="page-link border-0 c-regular ir-r" href="#">صفحه بعد</a>
                    </li>
                </ul>
            </nav>
        </div>

        <hr class="my-3">

        <!-- Send Comment Form -->
        <span class="ir-b c-dark fs-big text-right d-block mb-3">ارسال نظر</span>

        <form class="w-100" action="#">
            <div class="row">
                <aside class="col-md-6 mb-3 mb-md-0">
                    <!-- Passage -->
                    <div class="bg-white form-group border border-secondary rounded p-2 mb-3">
                        <!-- Lable -->
                        <label class="d-block text-right ir-r c-label mb-0 fs-small" for="passage">نظر شما</label>

                        <!-- Textarea -->
                        <textarea class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="passage"
                            rows="3" placeholder="لطفا نظر را وارد کنید..."></textarea>

                        <!-- Error Message -->
                        <small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام
                            ارور</small>
                    </div>

                </aside>

                <aside class="col-md-6 mb-3 mb-md-0">
                    <div class="row mb-3">
                        <div class="col-6 ir-r text-right fs-regular c-regular">
                            طعم:
                        </div>
                        <div class="col-6">
                            <div class="starrr"></div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-6 ir-r text-right fs-regular c-regular">
                            عطر:
                        </div>
                        <div class="col-6">
                            <div class="starrr"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6 ir-r text-right fs-regular c-regular">
                            ری کشیدن:
                        </div>
                        <div class="col-6">
                            <div class="starrr"></div>
                        </div>
                    </div>
                </aside>

                <aside class="col-12">
                <button type="submit" class="btn btn-success ir-r px-5 d-block">ارسال نظر</button>
                </aside>
            </div>
         </form>
      </section>

		<!-- Footer -->
		<?php include_once "contents/content_footer.php"; ?>

    <!-- Login Modal -->
    <div class="modal login-modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- Header -->
                <div class="modal-header border-0 pb-0">
                    <h5 class="modal-title k-bl c-dark" id="loginModalLabel">
                        ورود
                    </h5>
                </div>

                <!-- Body -->
                <div class="modal-body">
                    <form action="">
                        <!-- Phone Number -->
                        <div class="form-group border border-secondary rounded p-2 mb-3">
                            <!-- Lable -->
                            <label class="d-block text-right ir-r c-label mb-0 fs-small" for="phoneNumber">شماره
                                موبایل</label>

                            <!-- Input -->
                            <input type="tel" pattern="[09][0-9]{9}"
                                class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                                id="phoneNumber" placeholder="09120000000" />

                            <!-- Error Message -->
                            <small id=""
                                class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام
                                ارور</small>
                        </div>

                        <!-- Password -->
                        <div class="form-group border border-secondary rounded p-2 mb-2">
                            <!-- Lable -->
                            <label class="d-block text-right ir-r c-label mb-0 fs-small" for="password">رمز عبور</label>

                            <!-- Input -->
                            <input type="password"
                                class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="password"
                                placeholder="رمز عبور خود را وارد کنید" />

                            <!-- Error Message -->
                            <small id=""
                                class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام
                                ارور</small>
                        </div>

                        <!-- Forget Password Link -->
                        <a class="d-block text-right c-label fs-regular ir-r mb-3 text-decoration-none"
                            href="./forget-password.html">بازیابی رمز عبور</a>

                        <!-- Login Button -->
                        <button class="btn btn-primary ir-r d-block mb-3 px-5" type="submit">
                            ورود
                        </button>

                        <!-- Register Link -->
                        <span class="d-block text-right ir-r fs-regular">عضو نیستید؟
                            <a class="text-decoration-none" href="register">ثبت نام کنید</a></span>
                    </form>
                </div>
            </div>
        </div>
      </div>

		<!-- js Scripts -->
      <script src="<?= $oPath->asset("client/js/jquery-2.1.3.min.js") ?>"></script>
      <script src="<?= $oPath->asset("client/js/bootstrap.min.js") ?>"></script>
      <script src="<?= $oPath->asset("client/js/popper.min.js") ?>"></script>
      <script src="<?= $oPath->asset("client/js/smoothproducts.js") ?>"></script>
      <script src="<?= $oPath->asset("client/js/owl.carousel.js") ?>"></script>
      <script src="<?= $oPath->asset("client/js/starrr.js") ?>"></script>
      <script src="<?= $oPath->asset("client/js/main.js") ?>"></script>
      <script>
        /* wait for images to load */
        $(window).load(function () {
            $('.sp-wrap').smoothproducts();
        });

        // Farmers Products Carousel
        $(".farmer-products").owlCarousel({
            rtl: true,
            loop: true,
            margin: 15,
            nav: false,
            dots: false,
            autoplay: true,
            autoplayTimout: 100,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                },
                767.99: {
                    items: 3,
                },
                1000: {
                    items: 3,
                },
            },
        });

        // Rating
        $('.starrr').starrr();
    </script>
</body>

</html>
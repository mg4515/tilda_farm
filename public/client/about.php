<?php
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
   include_once $oPath->manageDir('about_bundle/controller/php/about_draw.php');
	$oSiteProDraw=new cSiteProDraw();
	$oAboutDraw=new cAboutDraw();
?>
<!DOCTYPE html>
<html lang="fa">
   <head>
		<?= 
			$oSiteProDraw->HeadMetaTag([
				'title'=>'درباره ما'
			]); 
		?>

		<!-- StyleSheet -->
		<?php include_once "contents/content_css.php"; ?>

		<!-- js header Scripts -->
		<?php include_once "contents/content_js_header.php"; ?>	
		
   </head>

   <body class="bg-body">
		<section class="intro">
			<!-- Header -->
			<?php 
				include_once "contents/content_header.php"; 
			?>
		</section>

       <!-- Contact Us -->
      <section class="contact-us container sp-t">
			<!-- Header -->
			<header class="section-header d-flex flex-row justify-content-between position-relative mb-4">
			   <div class="k-bl c-dark text-right title">درباره ما</div>
			</header>

			<!-- About Us -->
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-12">
						<div class="text-right">
							<?= $oAboutDraw->content() ?>
							<hr>
							<div class="button">
								<a href="index" class="btn btn-danger">خانه</a>
								<a href="contactUs" class="btn btn-danger">ارتباط با ما</a>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-12">
						<div class="about-img overlay">
							<img src="asset/client/img/about.jpg" alt="#" style='border-radius: 25px;box-shadow: 0px 0px 10px #aaa;'>
						</div>
					</div>
				</div>
			</div>
      </section>

		<!-- Footer -->
		<?php include_once "contents/content_footer.php"; ?>

    <!-- Login Modal -->
    <div
      class="modal login-modal fade"
      id="loginModal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="loginModalLabel"
      aria-hidden="true"
    >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- Header -->
          <div class="modal-header border-0 pb-0">
            <h5 class="modal-title k-bl c-dark" id="loginModalLabel">
              ورود
            </h5>
          </div>

          <!-- Body -->
          <div class="modal-body">
            <form action="">
              <!-- Phone Number -->
              <div class="form-group border border-secondary rounded p-2 mb-3">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="phoneNumber"
                  >شماره موبایل</label
                >

                <!-- Input -->
                <input
                  type="tel"
                  pattern="[09][0-9]{9}"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="phoneNumber"
                  placeholder="09120000000"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Password -->
              <div class="form-group border border-secondary rounded p-2 mb-2">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="password"
                  >رمز عبور</label
                >

                <!-- Input -->
                <input
                  type="password"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="password"
                  placeholder="رمز عبور خود را وارد کنید"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Forget Password Link -->
              <a
                class="d-block text-right c-label fs-regular ir-r mb-3 text-decoration-none"
                href="./forget-password.html"
                >بازیابی رمز عبور</a
              >

              <!-- Login Button -->
              <button
                class="btn btn-primary ir-r d-block mb-3 px-5"
                type="submit"
              >
                ورود
              </button>

              <!-- Register Link -->
              <span class="d-block text-right ir-r fs-regular"
                >عضو نیستید؟
                <a class="text-decoration-none" href="register"
                  >ثبت نام کنید</a
                ></span
              >
            </form>
          </div>
        </div>
      </div>
    </div>

		<!-- Scripts -->
		<script src="../assets/js/jquery-3.5.0.min.js"></script>
		<script src="<?= $oPath->asset("client/js/bootstrap.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/popper.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/smoothproducts.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/main.js") ?>"></script>
		<script>
			$(".next-btn").on("click", function () {
			  $(".step.step-1, .step-content.step-1").removeClass("active");
			  $(".step.step-2, .step-content.step-2").addClass("active");
			});

			$(".prev-btn").on("click", function () {
			  $(".step.step-1, .step-content.step-1").addClass("active");
			  $(".step.step-2, .step-content.step-2").removeClass("active");
			});
		</script>
   </body>
</html>

<?php
	//include
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
   include_once $oPath->manageDir('blog_bundle/controller/php/blog_draw.php');
	
	//object
	$oSiteProDraw=new cSiteProDraw();
	$oBlogDraw=new cBlogDraw();
	
	//request
	$id=cDataBase::escape(@$_REQUEST['i']);
	if(!$id)
	{
		header("Location: index");
	}
	
	//item
	$item=@$oBlogDraw->item($id);

?>
<!DOCTYPE html>
<html lang="fa">
   <head>
	  <?= 
			$oSiteProDraw->HeadMetaTag([
				'title'=>$item->title,
				'description'=>$item->comment,
				'keywords'=>$item->keywords,
				'addingAddress'=>"blog?i={$item->id}"
			]); 
		?>

		<!-- StyleSheet -->
		<?php include_once "contents/content_css.php"; ?>
		
		<!-- js header Scripts -->
		<?php include_once "contents/content_js_header.php"; ?>	
		
   </head>

   <body class="bg-body blog">
		<section class="intro">
			<!-- Header -->
			<?php 
				include_once "contents/content_header.php"; 
			?>
		</section>

      <section class="blog-details container sm-t">
			<div class="row">
			   <!-- Post Content -->
			   <aside class="col-12">
				   <h3 class="d-block text-right c-dark mb-5 ir-b">
					   <?= $item->title ?>
				   </h3>

				   <img class="d-block w-100 rounded mb-5" src="<?= $item->image ?>" alt="<?= $item->title ?>" />

				   <div style='text-align:right; direction:rtl;'><?= base64_decode($item->content) ?></div>
			   </aside>

			   <aside class="col-12 comments sm-t">
				   <!-- Header -->
				   <header class="section-header d-flex flex-row justify-content-start position-relative mb-3">
					   <div class="k-bl c-dark title">نظرات</div>
				   </header>

				   <div class="row">
					   <aside class="col-md-6">
					      <?= $item->opine_div ?>
					   </aside>
				   </div>

				   <span class="ir-b c-dark fs-big text-right d-block mb-3">ارسال نظر</span>

				   <form class="w-100" onsubmit="return false;">
						<div class="row">
						   <aside class="col-md-6 mb-3 mb-md-0">
							   <div id="spn_alert" style="width:100%;text-align:right" ></div>
							   <!-- Passage -->
							   <div class="bg-white form-group border border-secondary rounded p-2 mb-3">
									<!-- Lable -->
									<label class="d-block text-right ir-r c-label mb-0 fs-small" for="txt_comment">نظر شما</label>
									<!-- Textarea -->
									<textarea class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="txt_comment" rows="3" placeholder="لطفا نظر خود را وارد کنید..."></textarea>
									<!-- Error Message -->
									<small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام ارور</small>
							   </div>
						   </aside>

						   <aside class="col-12">
							   <button id='btn_opineSend' type="submit" class="btn btn-success ir-r px-5 d-block" onclick="blogItems_opineSend(<?= $item->id ?>);" >ارسال نظر</button>
						   </aside>
						</div>
				   </form>
			   </aside>
			</div>
      </section>

		<!-- Footer -->
		<?php include_once "contents/content_footer.php"; ?>

      <!-- Login Modal -->
		<div
			class="modal login-modal fade"
			id="loginModal"
			tabindex="-1"
			role="dialog"
			aria-labelledby="loginModalLabel"
			aria-hidden="true"
		>
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<!-- Header -->
					<div class="modal-header border-0 pb-0">
						<h5 class="modal-title k-bl c-dark" id="loginModalLabel">
						  ورود
						</h5>
					</div>

					<!-- Body -->
					<div class="modal-body">
						<form action="">
							<!-- Phone Number -->
							<div class="form-group border border-secondary rounded p-2 mb-3">
								<!-- Lable -->
								<label class="d-block text-right ir-r c-label mb-0 fs-small" for="phoneNumber">شماره موبایل</label>

								<!-- Input -->
								<input type="tel" pattern="[09][0-9]{9}" class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="phoneNumber" placeholder="09120000000"/>

							   <!-- Error Message -->
							   <small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام ارور</small>
							</div>

						   <!-- Password -->
						   <div class="form-group border border-secondary rounded p-2 mb-2">
							   <!-- Lable -->
							   <label class="d-block text-right ir-r c-label mb-0 fs-small" for="password">رمز عبور</label>

							   <!-- Input -->
							   <input type="password" class="form-control text-right ir-r fs-regular border-0 shadow-none px-0" id="password" placeholder="رمز عبور خود را وارد کنید" />

							   <!-- Error Message -->
							   <small id="" class="form-text error-message d-none text-right fs-small ir-r text-danger">پیام ارور</small>
						   </div>

						   <!-- Forget Password Link -->
						   <a class="d-block text-right c-label fs-regular ir-r mb-3 text-decoration-none" href="./forget-password.html">بازیابی رمز عبور</a>

						   <!-- Login Button -->
						   <button class="btn btn-primary ir-r d-block mb-3 px-5" type="submit">ورود</button>

						   <!-- Register Link -->
						   <span class="d-block text-right ir-r fs-regular">
							   عضو نیستید؟
							   <a class="text-decoration-none" href="register">ثبت نام کنید</a>
							</span>
						</form>
				   </div>
				</div>
			</div>
      </div>

		<!-- Scripts -->
		<script src="../assets/js/jquery-3.5.0.min.js"></script>
		<script src="<?= $oPath->asset("client/js/bootstrap.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/popper.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/smoothproducts.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/main.js") ?>"></script>
	 
      <script>
			$(".next-btn").on("click", function () {
			   $(".step.step-1, .step-content.step-1").removeClass("active");
			   $(".step.step-2, .step-content.step-2").addClass("active");
			});

			$(".prev-btn").on("click", function () {
			   $(".step.step-1, .step-content.step-1").addClass("active");
			   $(".step.step-2, .step-content.step-2").removeClass("active");
			});
      </script>
   </body>
</html>

<?php
	//include
   include_once $oPath->manageDir('site_bundle/controller/php/sitePro_draw.php');
	
	//object
	$oSiteProDraw=new cSiteProDraw();
	
?>

<!DOCTYPE html>
<html lang="fa">
   <head>
		<?= 
			$oSiteProDraw->HeadMetaTag([
				'title'=>'نهایی کردن خرید'
			]); 
		?>

		<!-- StyleSheet -->
		<?php include_once "contents/content_css.php"; ?>

		<!-- js header Scripts -->
		<?php include_once "contents/content_js_header.php"; ?>	
		
   </head>

   <body class="bg-body checkout">
		<section class="intro">
			<!-- Header -->
			<?php 
				include_once "contents/content_header.php"; 
			?>
		</section>

    <section class="container sm-t sp-t">
      <div class="row">
        <aside class="col-lg-8 mb-3 mb-md-0">
          <div class="card border-0 p-3">
            <!-- Choice Address -->
            <div class="choice-address">
              <!-- Title -->
              <span class="ir-b text-right d-block c-dark mb-3"
                >انتخاب آدرس</span
              >

              <!-- First Address -->
              <div class="custom-control custom-radio text-right mb-3">
                <input
                  type="radio"
                  id="firstAddress"
                  name="customRadio"
                  class="custom-control-input"
                />
                <label
                  class="custom-control-label ir-r fs-regular c-regular"
                  for="firstAddress"
                >
                  رشت، گلسار، ابتدای بلوار توحید، رستوران شورکولی.
                </label>
              </div>

              <!-- Second Address -->
              <div class="custom-control custom-radio text-right mb-3">
                <input
                  type="radio"
                  id="secondAddress"
                  name="customRadio"
                  class="custom-control-input"
                />

                <label
                  class="custom-control-label ir-r fs-regular c-regular"
                  for="secondAddress"
                  >رشت، گلسار، نبش خیابان۱۲۳، رستوران رازقی.</label
                >
              </div>

              <!-- Add Address -->
              <div class="add-address">
                <!-- Button -->
                <span
                  class="btn btn-warning add-address-btn ir-r d-block fs-regular cursor-pointer mb-0"
                  >افزودن آدرس</span
                >

                <!-- Forms -->
                <div class="address-forms">
                  <form action="">
                    <div class="row bg-light p-3">
                      <!-- Full Name -->
                      <div class="col-md-6">
                        <div
                          class="form-group border border-secondary rounded p-2 mb-3 bg-white"
                        >
                          <!-- Lable -->
                          <label
                            class="d-block text-right ir-r c-label mb-0 fs-small"
                            for="fullName"
                            >نام و نام خانوادگی
                            <span class="text-danger ir-r">*</span></label
                          >

                          <!-- Input -->
                          <input
                            required
                            type="text"
                            class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                            id="fullName"
                            placeholder="نام و نام خانوادگی"
                          />

                          <!-- Error Message -->
                          <small
                            id=""
                            class="form-text error-message d-none text-right fs-small ir-r text-danger"
                            >پیام ارور</small
                          >
                        </div>
                      </div>

                      <!-- Phone Number -->
                      <div class="col-md-6">
                        <div
                          class="form-group border border-secondary rounded p-2 mb-3 bg-white"
                        >
                          <!-- Lable -->
                          <label
                            class="d-block text-right ir-r c-label mb-0 fs-small"
                            for="phoneNumber"
                            >شماره تماس
                            <span class="text-danger ir-r">*</span></label
                          >

                          <!-- Input -->
                          <input
                            required
                            type="tel"
                            class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                            id="phoneNumber"
                            placeholder="شماره تماس"
                          />

                          <!-- Error Message -->
                          <small
                            id=""
                            class="form-text error-message d-none text-right fs-small ir-r text-danger"
                            >پیام ارور</small
                          >
                        </div>
                      </div>

                      <!-- Province -->
                      <div class="col-md-6">
                        <div
                          class="form-group border border-secondary rounded p-2 mb-3 bg-white"
                        >
                          <!-- Lable -->
                          <label
                            class="d-block text-right ir-r c-label mb-0 fs-small"
                            for="province"
                            >استان
                            <span class="text-danger ir-r">*</span></label
                          >
                          <input
                            placeholder="انتخاب استان"
                            class="form-control ir-r d-block w-100 rounded-0 shadow-none px-0 border-0"
                            list="provinces"
                            name="province"
                            id="province"
                          />

                          <datalist id="provinces">
                            <option value="گیلان"></option>
                            <option value="مازندران"></option>
                            <option value="تهران"></option>
                            <option value="فارس"></option>
                          </datalist>

                          <!-- Error Message -->
                          <small
                            id=""
                            class="form-text error-message d-none text-right fs-small ir-r text-danger"
                            >پیام ارور</small
                          >
                        </div>
                      </div>

                      <!-- City -->
                      <div class="col-md-6">
                        <div
                          class="form-group border border-secondary rounded p-2 mb-3 bg-white"
                        >
                          <!-- Lable -->
                          <label
                            class="d-block text-right ir-r c-label mb-0 fs-small"
                            for="city"
                            >شهر <span class="text-danger ir-r">*</span></label
                          >
                          <input
                            placeholder="انتخاب شهر"
                            class="form-control ir-r d-block w-100 rounded-0 shadow-none px-0 border-0"
                            list="cities"
                            name="city"
                            id="city"
                          />

                          <datalist id="cities">
                            <option value="رشت"></option>
                            <option value="بندر انزلی"></option>
                            <option value="رودبار"></option>
                            <option value="لاهیجان"></option>
                          </datalist>

                          <!-- Error Message -->
                          <small
                            id=""
                            class="form-text error-message d-none text-right fs-small ir-r text-danger"
                            >پیام ارور</small
                          >
                        </div>
                      </div>

                      <!-- Address Details -->
                      <div class="col-12">
                        <div
                          class="form-group border border-secondary rounded p-2 mb-3 bg-white"
                        >
                          <!-- Lable -->
                          <label
                            class="d-block text-right ir-r c-label mb-0 fs-small"
                            for="city"
                            >جزئیات آدرس
                            <span class="text-danger ir-r">*</span></label
                          >

                          <textarea
                            class="ir-r form-control px-0 shadow-none border-0"
                            placeholder="بلوار، خیابان، کوچه، بن بست و..."
                            name=""
                            id=""
                            rows="3"
                          ></textarea>

                          <!-- Error Message -->
                          <small
                            id=""
                            class="form-text error-message d-none text-right fs-small ir-r text-danger"
                            >پیام ارور</small
                          >
                        </div>
                      </div>

                      <!-- ZIP Code -->
                      <div class="col-md-6 offset-md-6">
                        <div
                          class="form-group border border-secondary rounded p-2 mb-3 bg-white"
                        >
                          <!-- Lable -->
                          <label
                            class="d-block text-right ir-r c-label mb-0 fs-small"
                            for="zipCode"
                            >کدپستی
                            <span class="text-danger ir-r">*</span></label
                          >
                          <input
                            type="text"
                            placeholder="انتخاب شهر"
                            class="form-control ir-r d-block w-100 rounded-0 shadow-none px-0 border-0"
                            id="zipCode"
                          />

                          <!-- Error Message -->
                          <small
                            id=""
                            class="form-text error-message d-none text-right fs-small ir-r text-danger"
                            >پیام ارور</small
                          >
                        </div>
                      </div>

                      <button
                        class="btn btn-primary ir-r mr-3 px-5"
                        type="button"
                      >
                        تایید
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </aside>

        <!-- Go to Checkout -->
        <aside class="col-lg-4">
          <div class="card p-3 mb-3">
            <div class="d-flex justify-content-between align-items-center py-3">
              <span class="ir-r fs-small c-regular">قیمت کالا (ها)</span>
              <span class="ir-r fs-small c-regular">500,000 تومان</span>
            </div>

            <hr class="my-0 separator" />

            <div class="d-flex justify-content-between align-items-center py-3">
              <span class="ir-r fs-small c-regular">هزینه ارسال</span>
              <span class="ir-r fs-small c-regular">10,000 تومان</span>
            </div>

            <hr class="my-0 separator" />

            <div class="discount clearfix">
              <input
                placeholder="کد تخفیف"
                class="form-control d-block float-right ir-r text-right shadow-none w-75"
                type="text"
              />
              <button
                type="button"
                class="btn btn-secondary d-block float-right w-25 ir-r shadow-none"
              >
                اعمال
              </button>
            </div>

            <hr class="my-0 separator" />

            <div class="d-flex justify-content-between align-items-center py-3">
              <span class="ir-b fs-small c-regular">مبلغ قابل پرداخت</span>
              <span class="ir-b fs-small text-success">510,000 تومان</span>
            </div>
          </div>

          <a class="btn btn-success ir-r d-block w-100" href="./checkout.html"
            >پرداخت</a
          >
        </aside>
      </div>
    </section>

		<!-- Footer -->
		<?php include_once "contents/content_footer.php"; ?>

    <!-- Login Modal -->
    <div
      class="modal login-modal fade"
      id="loginModal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="loginModalLabel"
      aria-hidden="true"
    >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- Header -->
          <div class="modal-header border-0 pb-0">
            <h5 class="modal-title k-bl c-dark" id="loginModalLabel">
              ورود
            </h5>
          </div>

          <!-- Body -->
          <div class="modal-body">
            <form action="">
              <!-- Phone Number -->
              <div class="form-group border border-secondary rounded p-2 mb-3">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="phoneNumber"
                  >شماره موبایل</label
                >

                <!-- Input -->
                <input
                  type="tel"
                  pattern="[09][0-9]{9}"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="phoneNumber"
                  placeholder="09120000000"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Password -->
              <div class="form-group border border-secondary rounded p-2 mb-2">
                <!-- Lable -->
                <label
                  class="d-block text-right ir-r c-label mb-0 fs-small"
                  for="password"
                  >رمز عبور</label
                >

                <!-- Input -->
                <input
                  type="password"
                  class="form-control text-right ir-r fs-regular border-0 shadow-none px-0"
                  id="password"
                  placeholder="رمز عبور خود را وارد کنید"
                />

                <!-- Error Message -->
                <small
                  id=""
                  class="form-text error-message d-none text-right fs-small ir-r text-danger"
                  >پیام ارور</small
                >
              </div>

              <!-- Forget Password Link -->
              <a
                class="d-block text-right c-label fs-regular ir-r mb-3 text-decoration-none"
                href="./forget-password.html"
                >بازیابی رمز عبور</a
              >

              <!-- Login Button -->
              <button
                class="btn btn-primary ir-r d-block mb-3 px-5"
                type="submit"
              >
                ورود
              </button>

              <!-- Register Link -->
              <span class="d-block text-right ir-r fs-regular"
                >عضو نیستید؟
                <a class="text-decoration-none" href="register"
                  >ثبت نام کنید</a
                ></span
              >
            </form>
          </div>
        </div>
      </div>
    </div>

		<!-- Scripts -->
		<script src="../assets/js/jquery-3.5.0.min.js"></script>
		<script src="<?= $oPath->asset("client/js/bootstrap.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/popper.min.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/smoothproducts.js") ?>"></script>
		<script src="<?= $oPath->asset("client/js/main.js") ?>"></script>
      <script>
			$(".add-address-btn").on("click", function () {
			  $(".address-forms").hasClass("active")
				 ? $(".address-forms").removeClass("active")
				 : $(".address-forms").addClass("active");
			});
      </script>
  </body>
</html>

<?php
   if(!file_exists($oPath->manageDir("about_bundle"))) mkdir($oPath->manageDir("about_bundle"));
   if(!file_exists($oPath->manageDir("about_bundle/data"))) mkdir($oPath->manageDir("about_bundle/data"));
   $content=@file_get_contents($oPath->manageDir("about_bundle/data/content.html"));
	$action=$oPath->manage("about_bundle/controller/php/about.php");
?>

<div class='vSpace-4x'></div>
   <h1><i class='fa fa-pencil'></i>&nbsp;درباره ما</h1>
<div class='vSpace-4x'></div>

<form id="frm" action="<?= $action ?>" method="post" enctype="application/x-www-form-urlencoded">
	<input type="hidden" value="about_update" name="request" />
	<textarea name="txt_content" id="txt_content"><?= $content ?></textarea>
	
	<div class='vSpace-4x'></div>
	<hr>
	<button id="btn" class="btn btn-success">ذخیره</button><br><br>
</form>
<script>
	CKEDITOR.replace('txt_content');
	document.getElementById('btn').onclick= function()
	{
		 CKEDITOR.instances.txt_content.updateElement();
		 var obj = CKEDITOR.instances.txt_content.getData();

		 document.getElementById("frm").submit();
	 };
</script>


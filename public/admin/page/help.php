<?php
   if(!file_exists($oPath->manageDir("help_bundle"))) mkdir($oPath->manageDir("help_bundle"));
   if(!file_exists($oPath->manageDir("help_bundle/data"))) mkdir($oPath->manageDir("help_bundle/data"));
   $content=@file_get_contents($oPath->manageDir("help_bundle/data/content.html"));
	$action=$oPath->manage("help_bundle/controller/php/help.php");
?>

<div class='vSpace-4x'></div>
<h1><i class='fa fa-question-circle'></i>&nbsp;راهنمای سایت</h1>
<div class='vSpace-4x'></div>

<form id="frm" action="<?= $action ?>" method="post" enctype="application/x-www-form-urlencoded">

	<input type="hidden" value="help_update" name="request" />
	<textarea name="txt_content" id="txt_content"><?= $content ?></textarea>
	
	<div class='vSpace-4x'></div>
	<hr>
	<button id="btn" class="btn btn-success">ذخیره</button><br><br>
</form>
<script>
	CKEDITOR.replace('txt_content');
	document.getElementById('btn').onclick= function()
	{
		 CKEDITOR.instances.txt_content.updateElement();
		 var obj = CKEDITOR.instances.txt_content.getData();

		 document.getElementById("frm").submit();
	 };
</script>
<?php
   require_once $oPath->manageDir("kifPool_bundle/model/kifPool_model.php");
   //require_once $oPath->manageDir("products_bundle/model/productsSettings_model.php");
   //require_once $oPath->manageDir("products_bundle/model/productsShop_model.php");
   //require_once $oPath->manageDir("products_bundle/model/productsItem_model.php");
   //require_once $oPath->manageDir("products_bundle/model/productsFactor_model.php");
	
	$oKifPool=new cKifPool();
	//$oProductsShop=new cProductsShop();
	//$oProductsItem=new cProductsItem();
	//$oProductsFactor=new cProductsFactor();

?>
<script>
	Chart.defaults.global.defaultFontFamily = 'WebYekan';
	Chart.defaults.global.defaultFontSize = 12;
</script>

	<div class="padding dir-rtl">
		<!-- ----------------------------------------------------------------- -->
      <?php
			$userId=$_SESSION["user_id"];
			$kifPoolAmount=$oKifPool->kifPool_getAmount($userId);	
			$userRegDate=jdate("H:i:s - Y/m/d",$user->dateRegister);
			$userLogDate=jdate("H:i:s - Y/m/d",$user->dateLogin);
		?>		
		<div class='panel'>
			<div class='panel-header'><i class='fa fa-info'></i>&nbsp;داشبورد</div>
			<div class='panel-body dir-rtl algn-c'>
				<span class='lblCaption status-success'><i class='fa fa-clock-o'></i>&nbsp;تاریخ عضویت</span><span class='lblContent'><?=$userRegDate?></span>
				<span class='lblCaption status-success'><i class='fa fa-clock-o'></i>&nbsp;تاریخ آخرین ورود</span><span class='lblContent'><?=$userLogDate?></span>

				<div class="vSpace-2x" ></div>

				<span class='lblCaption status-info'><i class='fa fa-user'></i>&nbsp;نام کاربری</span><span class='lblContent'><?=$user->userName?></span>
				<span class='lblCaption status-info'>@&nbsp;ایمیل</span><span class='lblContent'><?=$user->email?></span>
				<span class='lblCaption status-info'>نام</span><span class='lblContent'><?=$user->fname?></span>
				<span class='lblCaption status-info'>نام خانوادگی</span><span class='lblContent'><?=$user->lname?></span>
				<span class='lblCaption status-info'><i class='fa fa-location-arrow'></i>&nbsp;نام استان</span><span class='lblContent'><?=$user->state?></span>
				<span class='lblCaption status-info'><i class='fa fa-location-arrow'></i>&nbsp;نام شهر</span><span class='lblContent'><?=$user->city?></span>
				
				<div class="vSpace-2x" ></div>
				
				<span class='lblCaption status-warning'><i class='fa fa-envelope'></i>&nbsp;پیغام های جدید</span><span class='lblContent'>0</span>
				<span class='lblCaption status-warning'><i class='fa fa-bullhorn'></i>&nbsp;اعلان ها</span><span class='lblContent'>0</span>
				<span class='lblCaption status-warning'><i class='fa fa-file'></i>&nbsp;فاکتور های پرداختی</span><span class='lblContent'>0</span>
				<span class='lblCaption status-warning'><i class='fa fa-shopping-bag'></i>&nbsp;کیف پول</span><span class='lblContent'><?=$kifPoolAmount?> تومان</span>
			</div>			
		</div>						
				
		<div class='vSpace'></div>	

	<div class="padding dir-rtl">
		<!-- ----------------------------------------------------------------- -->
      <?php
			//$factorItem=$oProductsFactor->getItemsByUserShopId($userId);
			$itemPayCount=0;
			/*for($i=0;$i < count($factorItem);$i++)
			{
				$itemPayCount+=$factorItem[$i]->count;	
			}*/
			//-
			$itemPayNewCount=0;
			//$factors=$oProductsFactor->getFactors(array("status"=>"0,1,2"));
			/*for($i=0;$i < count($factors);$i++)
			{
				$factorItem=$oProductsFactor->getItemsByUserShopId($userId,$factors[$i]->id);	
				for($ii=0;$ii < count($factorItem);$ii++)
				{
					$itemPayNewCount+=$factorItem[$ii]->count;	
				}
			}*/
			//-
			$itemPayOnlineCount=0;
			/*$factors=$oProductsFactor->getFactors(array("payStatus"=>"1"));
			for($i=0;$i < count($factors);$i++)
			{
				$factorItem=$oProductsFactor->getItemsByUserShopId($userId,$factors[$i]->id);	
				for($ii=0;$ii < count($factorItem);$ii++)
				{
					$itemPayOnlineCount+=$factorItem[$ii]->count;	
				}
			}*/
			//-
			$itemkifPoolCount=0;
			/*$factors=$oProductsFactor->getFactors(array("payStatus"=>"3"));
			for($i=0;$i < count($factors);$i++)
			{
				$factorItem=$oProductsFactor->getItemsByUserShopId($userId,$factors[$i]->id);
				for($ii=0;$ii < count($factorItem);$ii++)
				{
					$itemkifPoolCount+=$factorItem[$ii]->count;	
				}		
			}*/	
			//-
			$itemPayErr=0;
			/*$factors=$oProductsFactor->getFactors(array("payStatus"=>"0"));
			for($i=0;$i < count($factors);$i++)
			{
				$factorItem=$oProductsFactor->getItemsByUserShopId($userId,$factors[$i]->id);	
				for($ii=0;$ii < count($factorItem);$ii++)
				{
					$itemPayErr+=$factorItem[$ii]->count;	
				}			
			}*/

		?>		
		<div class='panel'>
			<div class='panel-header'><i class='fa fa-info'></i>&nbsp;فروش کل</div>
			<div class='panel-body direction'>
					<div class='part part-w-5'>
						<canvas id='canvas_chartsForoosh1' height='200' width='300'></canvas>
					</div>
					<div class='part part-w-5'>
						<button class='btn btn-default'>&nbsp;</button>&nbsp;&nbsp;تعداد کل فروش : <b><?=$itemPayCount?></b><br>
						<button class='btn btn-success'>&nbsp;</button>&nbsp;&nbsp;فروش های جدید : <b><?=$itemPayNewCount?></b><br>
						<button class='btn btn-success'>&nbsp;</button>&nbsp;&nbsp;فروش با پرداخت ناموفق: <b><?=$itemPayErr?></b><br>
					</div>
			</div>
         <script>
				var data = {
					 labels: ['فروش های جدید','فروش آنلاین','فروش با کیف پول','فروش با پرداخت ناموفق'],
					 datasets: [
						  {
								fill: false,
								lineTension: 0.1,
								backgroundColor: "rgba(220,220,220,0.3)",
								borderColor: "rgba(220,220,220,1)",
								pointBackgroundColor: "rgba(220,220,220,1)",
								pointBorderColor: "#fff",
								pointHoverBackgroundColor: "#fff",
								pointHoverBorderColor: "rgba(220,220,220,1)",
								data: [<?=$itemPayCount?>, <?=$itemPayCount?>, <?=$itemPayCount?>,<?=$itemPayCount?>]
						  },
						  {
								backgroundColor: "rgba(0, 175, 240,0.2)",
								borderColor: "rgba(0, 175, 240,1)",
								pointBackgroundColor: "rgba(0, 175, 240,1)",
								pointBorderColor: "#fff",
								pointHoverBackgroundColor: "#fff",
								pointHoverBorderColor: "rgba(0, 175, 240,1)",
								data: [<?=$itemPayNewCount?>,<?=$itemPayOnlineCount?>,<?=$itemkifPoolCount?>,<?=$itemPayErr?>]
						  }
					 ]
				};
				var ctx=document.getElementById('canvas_chartsForoosh1').getContext('2d');
				window.myBar1 = new Chart(ctx, {
					type: "line",
					display:false,
					data: data,
					options: {responsive: true,legend: {display: false}}
				});
         </script>			
		</div>						
				
		<div class='vSpace'></div>
		
<!-- ----------------------------------------------------------------- -->
		<?php
			$today=jdate("Y/m/d",time());
			$yesterday=jdate("Y/m/d",time() - (24*60*60));
			$oldYesterday=jdate("Y/m/d",time() - (48*60*60));
			//-
			$monthNow=time();
			$month=time() - (720*60*60);
			$monthOld=time() - (1440*60*60);
			$monthOlder=time() - (2160*60*60);
			//$payCountMonth1=@count(@$oProductsFactor->getItemsByDate($monthNow,$month,$userId)); //mahy ke dar an hastim ta be emrooz
			//$payCountMonth2=@count(@$oProductsFactor->getItemsByDate($month,$monthOld,$userId));//mah gozashte moshabeh mahy ke dar an hastim
			//$payCountMonth3=@count(@$oProductsFactor->getItemsByDate($monthOld,$monthOlder,$userId));//mah gozashte moshabeh mahy ke dar an hastim

			$pay3MonthCount=$payCountMonth1 + $payCountMonth2 + $payCountMonth3;
			if($pay3MonthCount!=0)
			{
			   $payCountMonth1_per=100/($pay3MonthCount/$payCountMonth1);
			   $payCountMonth2_per=@round(100/($pay3MonthCount/$payCountMonth2),2);
			   $payCountMonth3_per=@round(100/($pay3MonthCount/$payCountMonth3),2);
         }
         else
			{
			   $payCountMonth1_per=0;
			   $payCountMonth2_per=0;
			   $payCountMonth3_per=0;				
			}
			
			$payDate1=jdate("Y/m/d",time());
			$payDate2=jdate("Y/m/d",$month);
			$payDate3=jdate("Y/m/d",$monthOld);		
		?>
		<div class='panel'>
			<div class='panel-header'><i class='fa fa-info'></i>&nbsp;فروش سه ماهه اخیر</div>
			<div class='panel-body direction'>
				<div class='part part-w-5'>
					<canvas id='canvas_chartsForoosh2' height='200' width='300'></canvas>
				</div>					
				<div class='part part-w-5'>
					<button class='btn btn-default'>&nbsp;</button>&nbsp;&nbsp;تعداد فروش سه ماهه اخیر : <b><?=$pay3MonthCount?></b><br>
					<button class='btn btn-warning'>&nbsp;</button>&nbsp;&nbsp;<?=$payDate1?> : <b><?=$payCountMonth1_per?></b><br>
					<button class='btn btn-warning'>&nbsp;</button>&nbsp;&nbsp;<?=$payDate2?> : <b><?=$payCountMonth2_per?></b><br>
					<button class='btn btn-warning'>&nbsp;</button>&nbsp;&nbsp;<?=$payDate3?> : <b><?=$payCountMonth3_per?></b><br>
				</div>		
			</div>
			<script>
				var data = {
					 labels: ['<?=$payDate1?>','<?=$payDate2?>','<?=$payDate3?>'],
					 datasets: [
						  {
								fill: false,
								lineTension: 0.1,
								backgroundColor: "rgba(220,220,220,0.3)",
								borderColor: "rgba(220,220,220,1)",
								pointBackgroundColor: "rgba(220,220,220,1)",
								pointBorderColor: "#fff",
								pointHoverBackgroundColor: "#fff",
								pointHoverBorderColor: "rgba(220,220,220,1)",
								data: [100, 100, 100]
						  },
						  {
								backgroundColor: "rgba(0, 175, 240,0.2)",
								borderColor: "rgba(0, 175, 240,1)",
								pointBackgroundColor: "rgba(0, 175, 240,1)",
								pointBorderColor: "#fff",
								pointHoverBackgroundColor: "#fff",
								pointHoverBorderColor: "rgba(0, 175, 240,1)",
								data: [<?=$payCountMonth1_per?>,<?=$payCountMonth2_per?>,<?=$payCountMonth3_per?>]
						  }
					 ]
				};
				var ctx=document.getElementById('canvas_chartsForoosh2').getContext('2d');
				window.myBar1 = new Chart(ctx, {
					type: "line",
					display:false,
					data: data,
					options: {responsive: true,legend: {display: false}}
				});			
			</script>
		</div>
				
		<div class='vSpace'></div>
						
	</div>
	<div class="vSpace-4x"></div>
</article>

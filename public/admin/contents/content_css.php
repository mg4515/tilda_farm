	<!-- STYLE -->
	<link rel="stylesheet" href="<?= $oPath->asset('default/css/plugins/myStyle_anim.css') ?>" />
	<link rel="stylesheet" href="<?= $oPath->asset('default/css/fontiran.css') ?>" />
   <link rel="stylesheet" href="<?= $oPath->asset('default/css/plugins/font-awesome-4.7.0/css/font-awesome.css') ?>" />
   <link rel="stylesheet" href="<?= $oPath->asset('default/js/plugins/mtree/mtree.css') ?>" />
   <link rel="stylesheet" href="<?= $oPath->asset('default/css/plugins/jsPersianCal/js-persian-cal.css') ?>" />
   <link rel="stylesheet" href="<?= $oPath->asset('default/js/plugins/zTree/them/me/meStyle.css') ?>" >
   <link rel="stylesheet" href="<?= $oPath->asset('default/js/plugins/alert/alert.css') ?>" >
   <link rel="stylesheet" href="<?= $oPath->asset('default/js/plugins/loading-bar/loading-bar.css') ?>" >
   <link rel="stylesheet" href="<?= $oPath->asset('default/js/plugins/notification/notification.css') ?>" >
   <link rel="stylesheet" href="<?= $oPath->asset('default/js/plugins/select2OptionPicker/select2OptionPicker.css') ?>" >
	
	<link rel="stylesheet/less" type="text/css" href="<?= $oPath->asset('default/css/plugins/myStyle.less') ?>" >
	<link rel="stylesheet/less" type="text/css" href="<?= $oPath->asset('admin/css/style.less') ?>" >
	
	<!-- 	
	<link rel="stylesheet" type="text/css" href="<?= $oPath->asset('default/css/plugins/myStyle.css') ?>" >
	<link rel="stylesheet" type="text/css" href="<?= $oPath->asset('admin/css/style.css') ?>" >
	-->
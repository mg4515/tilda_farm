<div class="collapse-auto part-w-1_5-auto">
   <input type="checkbox" id="toggle">
   <label for="toggle" class='part-w-10-important fg-black'></label>		  
   <div class="collapse-auto-content">
		
		<ul class='navBar navBar-right navBar-list-auto navBar-bordered-f-auto navBar-bgColor-auto part-w-10-auto' style='color:#555 !important'>
			<li class='dropdown dropdown-right'>
			   <a class='dropdown-title fg-black'><i class='fa fa-user'></i>&nbsp;<?= $_SESSION['admin_userName'] ?></a>
				<span class='dropdown-icon dropdown-icon-down fg-black'></span>
				<span class='dropdown-content dropdown-content-l-auto'>
				   <a href='javascript:void(0)' onclick='admin_edit(<?= $_SESSION['admin_id'] ?>)'><i class='fa fa-user'></i>مشخصات</a>
					<a href='javascript:void(0)' onclick='admin_logout();'><i class='fa fa-close'></i>خروج</a>
				</span>						
			</li>		   
		</ul>
		
		<div class='hide-auto'>|</div>
		
	   <div onclick="oTools.link('?page=shopItemsOpineNew');">
	      <button class="btn btn-label btn-label-right btn-info btn-custom1">
			   <span id='spn_shopItemsOpine_icon'><i class="fa fa-bell"></i>نظر جدید</span>
	         <span class='btn-label-caption' id='spn_shopItemsOpine'>0</span>
	      </button>
	   </div>		
	   <div onclick="oTools.link('?page=ticket');">
	      <button class="btn btn-label btn-label-right btn-info btn-custom1">
			   <span id='spn_ticket_icon'><i class="fa fa-bell"></i>پشتیبانی</span>
	         <span class='btn-label-caption' id='spn_ticket'>0</span>
	      </button>
	   </div>
	   <div onclick="oTools.link('?page=contactUs');">
	      <button class="btn btn-label btn-label-right btn-info btn-custom1">
			   <span id='spn_contactUs_icon'><i class="fa fa-bell"></i>ارتباط با ما</span>
	         <span class='btn-label-caption' id='spn_contactUs'>0</span>
	      </button>
	   </div>		
	   <div onclick="oTools.link('?page=shopItemsNew');">
	      <button class="btn btn-label btn-label-right btn-info btn-custom1">
			   <span id='spn_shopItems_icon'><i class="fa fa-bell"></i>محصولات جدید</span>
	         <span class='btn-label-caption' id='spn_shopItems'>0</span>
	      </button>
	   </div>				
   </div>
</div>

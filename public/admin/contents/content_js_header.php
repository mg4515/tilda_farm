<script><?php include_once $oPath->engineDir('engine.js.php'); ?></script>

<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/compressor/jsxcompressor.min.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/jquery.min.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/ckeditor/ckeditor.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/myTools.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/myModal.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/myList.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/myTag.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/myTreePage.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/jsPersianCal/js-persian-cal.min.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/jsPersianCity/js-persian-city.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/notification/notification.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/alert/alert.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/loading-bar/loading-bar.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/plugins/select2OptionPicker/select2OptionPicker.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('admin/js/script_matrial.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('admin/js/script.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('default/js/script.js') ?>"></script>

<!-- page script file -->
<script type='text/javascript' src="<?= $oPath->manage('contactUs/controller/js/contactUs.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->asset('admin/js/page/siteMailServer.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->manage('users_bundle/controller/js/users_panel_admin.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->manage('site_bundle/controller/js/sitePro_panel.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->manage('kifPool_bundle/controller/js/kifPool_panel_admin.js') ?>"></script>
<script type='text/javascript' src="<?= $oPath->manage('admin_bundle/controller/js/admin_panel.js') ?>"></script>

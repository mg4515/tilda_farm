<?php
   if(!isset($_SESSION['admin_login']) || @$_SESSION['admin_login']!=true)
	{
	   header("Location:admin/login");
		exit;			
	}
   include_once $oPath->manageDir("admin_bundle/controller/php/admin_panel.php");
	$oAdmin=new cAdmin();
	$admin=$oAdmin->get($_SESSION['admin_id'],true)["admin"];
?>

<!DOCTYPE html>

<html>

<head>
   <meta charset='utf8'>
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
   <title>مدیر - <?= $admin->userName ?></title>

	<!-- STYLE -->
   <?php include_once $oPath->publicDir("admin/contents/content_css.php"); ?>

   <!-- SCRIPT -->
   <?php include_once $oPath->publicDir("admin/contents/content_js_header.php"); ?>
	
</head>

<body id="body" style="">
<!-- headers ////////////////////////////////////////////////////////////-->
	<?php include_once $oPath->publicDir("admin/contents/headers.php"); ?>
	
	<div class='matrial-side-container bg-white row' >
	   <!-- topContainer ////////////////////////////////////////////////////////////-->
		<div class="matrial-side-top dir-rtl bg-default">
			<?php include_once $oPath->publicDir("admin/contents/top.php"); ?>					
		</div>	
	
		<!-- mainContainer //////////////////////////////////////////////////////////-->
		<article class="matrial-side-main bg-whiteDark" style='bottom:0px;'>
			<div class='content-larg dir-rtl' id='layer_content'>
				<?php
					//page maker
					if(isset($_REQUEST["page"]))
					{
						if($oAdmin->isAllowed($_REQUEST["page"]))
						{
							$incRet=@include_once "page/" . $_REQUEST["page"] . ".php";
							if(!$incRet)
							{
								$_SESSION["page_login_info"]="404";
								echo "<script>oTools.link('admin/login');</script>";
								//exit;	
							}			
						}
						else
						{
							$_SESSION["page_login_info"]="404";
							echo "<script>oTools.link('admin/login');</script>";
							//exit;	
						}
					}
					else
					{
						$_SESSION["page_login_info"]="";
						echo "<script>oTools.link('admin/login');</script>";
						//exit;	
					}		
				?>		
			</div>
			<div id='layer_temp'></div>
		</article>

		<!-- rightContainer //////////////////////////////////////////////////////////-->
		<div class="matrial-side-right bg-blackLight" style='bottom:0px;' id='layer-right'>
			<button class='matrial-side-right-handle shadow btn btn-default brd-radius-full' onclick='$("#layer-right").toggleClass("anim-right1");$(".matrial-side-right-back").toggleClass("part-w-0-important");'><i class='fa fa-cog'></i></button>
			<div class='matrial-side-right-close' onclick='$("#layer-right").toggleClass("anim-right1");$(".matrial-side-right-back").toggleClass("part-w-0-important");'></div>
			<div class='matrial-side-right-back' onclick='$("#layer-right").toggleClass("anim-right1");$(".matrial-side-right-back").toggleClass("part-w-0-important");'></div>
			<div class='part-w-10 algn-c' style='background-color:#1d1c27'>
			   <div class="vSpace-3x"></div>
			   <div class="imageCircle-small cur-pointer" style="background-image:url(<?= admin_getImage() ?>);background-color:#fff;" onclick='user_profileDraw(<?= $_SESSION["adminId"] ?>,1);'></div>
				<div class='fg-grayLight'><?= "<span style='font-size:14px;'>" . $_SESSION['admin_fName'] . $_SESSION['admin_lName'] . "</span><br>" . $_SESSION['admin_userName'] ?></div>
				<br>
				<div class="vSpace-2x"></div>
			</div>
			<ul class="dir-rtl">   
				<?php require_once $oPath->publicDir("admin/contents/sideRight.php"); ?>
			</ul>
		</div>
   </div>

<!-- SCRIPT HEADER ////////////////////////////////////////////////////////-->
   <?php include_once $oPath->publicDir("admin/contents/content_js_footer.php"); ?>	
</body>
</html>
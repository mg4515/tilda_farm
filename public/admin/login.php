<?php
   if(@$_SESSION["page_login_info"]=="404")
      $pageInfo="404 . صفحه مورد نظر یافت نشد";
   else
      $pageInfo="";
   //session_unset();
	$t=time();
?>
<!DOCTYPE html>

<html>

<head>
   <title>ورود</title>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- STYLE -->
   <?php include_once $oPath->publicDir("admin/contents/content_css.php"); ?>
   
   <!-- SCRIPT HEADER -->
   <?php include_once $oPath->publicDir("admin/contents/content_js_header.php"); ?>
	
</head>

<body style="background-color: #eee;">
   <div id="layer_loading">
      <div><img src="<?= $oPath->asset('admin/images/loading.gif'); ?>" width="80px" height="80px" alt='loading'></div>
   </div>
   <div class="bg"></div>
   <div id="layer_login">
      <div class='titleBar'>ورود به مدیریت</div>
      <hr>
      <h2 id="h_pageInfo"><?= $pageInfo ?></h2>
      
		<label><i class='fa fa-user'></i>نام کاربری</label>
		<input type="text" id="txt_userName" placeholder="نام کاربری"/><br>
      
		<label><i class='fa fa-key'></i>رمز عبور</label>
		<input type="password" id="txt_password" placeholder="رمز عبور"/>
		
      <span id="spn_loginInfo"></span>
      <hr>
		<div style='padding:20px;background-color:#fff;border-radius:5px;'>
      <img src="<?= $oPath->public_("admin/contents/security/sec_login.php") . "?t={$t}" ?>" id="img_sec" style="width: 220px;height: 40px;"/>
      <i id="btn_refresh" class='fa fa-refresh' onclick="secImageRefresh();"></i><br><br>
      <input type="text"  id="txt_sec" placeholder="کد موجود در تصویر" style="text-align: center" onkeypress="return runScript(event)"/><br>
      <span id="spn_secInfo"></span>
		</div>
      <button id="btn_login" class="btn btn-info" onclick="admin_login();">ورود</button>
   </div>
	
   <!-- SCRIPT FOOTER -->
   <?php include_once $oPath->publicDir("admin/contents/content_js_footer.php"); ?>
	
   <script >
      function secImageRefresh()
      {
         var d = new Date();
         var t = d.getTime();
         document.getElementById('img_sec').setAttribute("src","<?= $oPath->public_("admin/contents/security/sec_login.php") ?>?t=" + t);
      }
      function runScript(e)
      {
         if(e.keyCode==13) admin_login();
      }
      try {
         $("#txt_userName").val(localStorage.modirUserName);
         $("#txt_password").val(localStorage.modirPassword);
      }
      catch(e){} 
   </script>

</body>
</html>
var script_loadingShow=function(){
	div=document.getElementById("layer_loading__");
	if(div==null)
	{
	   div=document.createElement("DIV");
	   div.setAttribute("id","layer_loading__");
		document.body.appendChild(div); 
      div.innerHTML="<div><img src='" + oPath.asset('default/images/loading.gif') + "' width='80px' height='80px' alt='loading'></div>";	
	   div.style.cssText="z-index:1000000;display: none;position: fixed;width:100%;height:100%;left:0px;top:0px;background-color: #000;background-color: rgba(0,0,0,0.6);";
	   div.querySelector("div").style.cssText="padding: 20px;margin:100px auto;width:120px;height: 120px;border-radius: 50%;background-color: #fff;";
	}
	div.style.display="block"; 
}//---------------------------------------------------------------------------
var script_loadingHide=function()
{
	document.querySelector("#layer_loading__").style.display="none";
}//---------------------------------------------------------------------------
function script_alertStatus(elementId,type,message,scroll,icon) //myStyle and bootstarp
{
	if(icon==undefined) icon=false;
	if(scroll==undefined) scroll=true;
	if(icon==false)
	{
		$("#" + elementId).html(message);
		$("#" + elementId).attr("class","");
		$("#" + elementId).attr("class","alert alert-right alert-" + type);
	}
	else if(icon==true)
	{
		if(type=="danger") $("#" + elementId).html("<span class='alert-icon'><i class='fa fa-info-circle'></i></span>" + message);
		else if(type=="info") $("#" + elementId).html("<span class='alert-icon'><i class='fa fa-info'></i></span>" + message);
		else if(type=="warning") $("#" + elementId).html("<span class='alert-icon'><i class='fa fa-warning'></i></span>" + message);
		else if(type=="success") $("#" + elementId).html("<span class='alert-icon'><i class='fa fa-check'></i></span>" + message);
		$("#" + elementId).attr("class","");
		$("#" + elementId).attr("class","alert alert-right alert-" + type);
		if(type=="danger") $("#" + elementId + " .alert-icon").html("<i class='fa fa-info-circle'></i>");
	}
	if(message=="") $("#" + elementId).css("display","none"); else $("#" + elementId).css("display","");
	if(scroll==true)
	{
		document.querySelector("#" + elementId).scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
	}
}//---------------------------------------------------------------------------
function script_alert(message,title,status,autoClose,isButton)
{
	if(title==undefined) title="پیغام سیستم";
	if(status==undefined) status="ok";
	if(status=='success') status="ok";
	if(status=='danger') status="error";
	if(autoClose==undefined || autoClose==true) autoClose=4000;
	if(isButton==undefined || isButton==true)	
		buttons=[{type: 'cancel',text: 'بستن'}];
	else
	   buttons=[];	

	x0p({
		title: title,
		text: message,
		animationType: 'pop',
		type: status,
		buttons: buttons,
		autoClose: autoClose
   });
}//---------------------------------------------------------------------------
function script_confirm(message,title,callbackTrue,callabackFalse)
{
	if(title==undefined) title="پیغام سیستم";	
	x0p(
		{
			title: title,
			text: message,
			type: 'warning',
			buttons: [
				{
					type: 'cancel',
					text: 'خیر'  
				},
				{
					type: 'warning',
					text: 'بله',
					showLoading: true
				}
			]		
		}, 
		function(button, text) 
		{
			if(button=="warning") callbackTrue(); 
			else if(button=="cancel") callbackFale(); 
		}
	);
}//---------------------------------------------------------------------------
function script_confirm2(message,title,callbackTrue,callabackFalse)
{
	if(title==undefined) title="پیغام سیستم";
	iziToast.destroy();
	iziToast.question({
		timeout: 20000,
		close: false,
		overlay: true,
		toastOnce: true,
		id: 'question',
		zindex: 99999,
		title: title,
		message: message,
		animateInside: true,
		position: 'center',
		buttons: [
			['<button><b>بله</b></button>', function (instance, toast) {
				instance.hide(toast, { transitionOut: 'fadeOut' }, 'true');
				callbackTrue();
			}, true],
			['<button>خیر</button>', function (instance, toast) {
				instance.hide(toast, { transitionOut: 'fadeOut' }, 'false');
				callabackFalse
			}]
		],
		onClosing: function(instance, toast, closedBy){

		},
		onClosed: function(instance, toast, closedBy){

		}
	});	
}//---------------------------------------------------------------------------
function script_alert2(message,title,status,autoClose)
{
	if(title==undefined) title="پیغام سیستم";
	if(status==undefined) status="success";
	if(autoClose==undefined || autoClose==true) autoClose=6000; else autoClose=0;
	if(status=='success')
	{
		iziToast.success({
			title: title,
			message: message,
			animateInside: true,
			timeout:autoClose
		});
	}	
	else if(status=='danger')
	{
		iziToast.error({
			title: title,
			message: message,
			animateInside: true,
			timeout:autoClose
		});
	}
	else if(status=='info')
	{
		iziToast.info({
			title: title,
			message: message,
			animateInside: true,
			timeout:autoClose
		});
	}
	else if(status=='warning')
	{
		iziToast.warning({
			title: title,
			message: message,
			animateInside: true,
			timeout:autoClose
		});
	}	
}//---------------------------------------------------------------------------
function script_focus(elementId)
{
	document.querySelector("#" + elementId).scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
	document.querySelector("#" + elementId).focus();
}
   map_init=function(lat,lng){
		try{
			if(typeof lat != "undefined" && typeof lng != "undefined")
			{
				var myLocation = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
			}
			else
				var myLocation = new google.maps.LatLng(29.62561278919156, 52.5585045268862);

			var mapOptions = {
				 zoom: 17,
				 center: myLocation,
				 mapTypeId: google.maps.MapTypeId.ROADMAP
			  }
			  var map = new google.maps.Map(document.getElementById("layer_myMap"), mapOptions);

			  var infowindow = new google.maps.InfoWindow({
				  content: '<div style="direction:rtl;color:#888888;text-align:left;margin:0;font:normal 14px Roboto;padding-right:25px">جابجا کنید</div>'
			  });

			  //MARKER ----------------------
			  marker = new google.maps.Marker({
				 position: myLocation,
				 map: map,
				 draggable:true,
				 animation: google.maps.Animation.DROP
			  });

				google.maps.event.addListener(marker, 'dragend', function()
				{
					geocodePosition(marker.getPosition());
				});
				function geocodePosition(pos)
				{
					//document.getElementById("mapPos").value=pos;
					geocoder = new google.maps.Geocoder();
					geocoder.geocode(
					  {latLng: pos},
					  function(results, status)
					  {
							if (status == google.maps.GeocoderStatus.OK)
							{
								document.getElementById("spn_addressMap").value=results[0].formatted_address;
								//document.getElementById("txt_mapPos").value=results[0].geometry.location;
								document.getElementById("txt_mapPos").value=results[0].geometry.location.lat() + "|" + results[0].geometry.location.lng();
							}
							else
							{
								alert('Cannot determine address at this location.'+status);
							}
					  }
					);

				}


			  // Create the search box and link it to the UI element.
			  var input = document.getElementById('txt_mapSearch');
			  var searchBox = new google.maps.places.SearchBox(input);
			  //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
			  // Bias the SearchBox results towards current map's viewport.
			  map.addListener('bounds_changed', function() {
				 searchBox.setBounds(map.getBounds());
			  });

			  var markers = [];
			  // [START region_getplaces]
			  // Listen for the event fired when the user selects a prediction and retrieve
			  // more details for that place.
			  searchBox.addListener('places_changed', function() {
				 var places = searchBox.getPlaces();

				 if (places.length == 0) {
					return;
				 }

				 // Clear out the old markers.
				 /*markers.forEach(function(marker) {
					marker.setMap(null);
				 });
				 marker.setMap(null);
				 markers = [];*/

				 // For each place, get the icon, name and location.
				 var bounds = new google.maps.LatLngBounds();
				 places.forEach(function(place) {
					var icon = {
					  url: place.icon,
					  size: new google.maps.Size(71, 71),
					  origin: new google.maps.Point(0, 0),
					  anchor: new google.maps.Point(17, 34),
					  scaledSize: new google.maps.Size(25, 25)
					};

					// Create a marker for each place.
					/*markers.push(new google.maps.Marker({
					  map: map,
					  icon: icon,
					  title: place.name,
					  position: place.geometry.location
					}));*/
				  marker.setOptions({
					 position: place.geometry.location,
					 map: map,
					 draggable:true,
					 animation: google.maps.Animation.DROP
				  });
					if (place.geometry.viewport) {
					  // Only geocodes have viewport.
					  bounds.union(place.geometry.viewport);
					} else {
					  bounds.extend(place.geometry.location);
					}
				 });
				 map.fitBounds(bounds);
			  });
			  // [END region_getplaces]





			infowindow.open(map, marker);
		}
		catch(e){}
    }
function myTag(object)
{
	object=object || [];
	if(typeof object != "object") object=[];
	object.elementId=object.elementId || "";
	object.elementId_temp="";
   object.onClick= object.onClick || "";
   object.onDelete= object.onDelete || "";
   object.onInsert= object.onInsert || "";

	obj=document.getElementById(object.elementId);
   try{items=obj.getElementsByTagName("li");}catch(e){items={};}
   for(i=0;i < items.length;i++)
	{
		items[i].id="li_" + object.elementId + "_" + i;		
      if(object.onClick) items[i].onclick=function(){object.onClick(this)};
		items[i].onmouseup=function(){
			this.style.transform="scale(0,0)";
			var me=this;
			setTimeout(function(){
		      me.parentNode.removeChild(me);
			},400);
		};
	}//---------------------------------------------------------------------------------------------
	this.get=function(dataValueName)
	{	
	   var dataValue=[];	
		obj=document.getElementById(object.elementId);
		try{items=obj.getElementsByTagName("li"); }catch(e){items={};}
		for(i=0;i < items.length;i++)
		{
			dataValue.push({
				"dataValue1":items[i].getAttribute("dataValue1"),
				"dataValue2":items[i].getAttribute("dataValue2"),
				"dataValue3":items[i].getAttribute("dataValue3"),
				"dataValue4":items[i].getAttribute("dataValue4"),
				"title":items[i].getAttribute("title"),
				"id":items[i].id,
			});		
		}

		var ret_= dataValue;
		if(dataValueName=="dataValue1" || dataValueName=="dataValue2" || dataValueName=="dataValue3" || dataValueName=="dataValue4")
		{	
			var ret=[];
			for(i=0;i<ret_.length;i++)
			{
				ret.push(ret_[i][dataValueName]);
			}
		}
		else
			ret=ret_;
		return ret;			
   }//---------------------------------------------------------------------------------------------
	this.insert=function(title,dataValue1,dataValue2,dataValue3,dataValue4)
	{
	
	   obj=document.getElementById(object.elementId);
		try{items=obj.getElementsByTagName("li"); }catch(e){items={};}
      if(items.length == undefined)
		{
		   var id="li_" + object.elementId + "_" + 0;
		}
		else
		{
		   var id="li_" + object.elementId + "_" + items.length;	
      }	
		
		var li = document.createElement('LI');
		li.id = id;
		if(title != undefined) 
		{
		   li.setAttribute("title",title);
         li.innerHTML="<span>"+title+"</span>";			
		}		
		if(dataValue1 != undefined) li.setAttribute("dataValue1",dataValue1);		
		if(dataValue2 != undefined) li.setAttribute("dataValue2",dataValue2);		
		if(dataValue3 != undefined) li.setAttribute("dataValue3",dataValue3);		
		if(dataValue4 != undefined) li.setAttribute("dataValue4",dataValue4);
		if(object.onClick) li.onclick=function(){object.onClick(this)};
		li.onmouseup=function(){
			this.style.transform="scale(0,0)";
			var me=this;
			setTimeout(function(){
				me.parentNode.removeChild(me);
			},400);
		};		
		obj.appendChild(li);
      return id;		
	}//---------------------------------------------------------------------------------------------
	this.setByArray=function(arry)
	{
		for(i=0;i < arry.length;i++)
		{
			this.insert(arry[i],'','','','');
		}
	}//---------------------------------------------------------------------------------------------
	this.setByString=function(str,chrDelimiter)
	{
		arry=str.split(chrDelimiter);
		for(i=0;i < arry.length;i++)
		{
			this.insert(arry[i],'','','','');
		}
	}//---------------------------------------------------------------------------------------------
	return this;
}
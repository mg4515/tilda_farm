function myTabs_inits()
{
   var tabs = $('.myTabs');
   tabIndex=-1;
	tabs.each(function()
   {
      tabIndex++;
		var tab = $(this);
		var tabBtns = tab.find('.myTabs-tabBar span');
		var tabPages = tab.find('.myTabs-contents section');

      for(i=0;i < tabBtns.length;i++)
      {
         var tabAddress= tabIndex + "-" + i ;
         tabPages[i].setAttribute("tabAddress",tabAddress);
         tabBtns[i].setAttribute("onclick", "myTab_pageShow('" + tabAddress + "');");
      }
	});
}//---------------------------------------------------------------------------------------
function myTabs_init(myTabId)
{
   //var tab = $('#' + myTabId);
   var tabBtns = $('#' + myTabId + ' .myTabs-tabBar span');
   var tabPages = $('#' + myTabId + ' .myTabs-contents section');

   for(i=0;i < tabBtns.length;i++)
   {
      var tabAddress= i ;
      tabPages[i].setAttribute("tabAddress",tabAddress);
      tabBtns[i].setAttribute("tabAddress",tabAddress);
      tabBtns[i].setAttribute("onclick", "myTab_pageShow('" + myTabId + "','" + tabAddress + "');");
   }
   tabBtns[0].onclick();
}//---------------------------------------------------------------------------------------
var myTab_pageShow=function(myTabId,tabAddress)
{
   var spl=tabAddress.split("-");
   var myTabIndex=spl[0];
   var pageIndex=spl[1];

   var tabBtns = $('#' + myTabId + ' .myTabs-tabBar span');
   var tabBtn = $('#' + myTabId + ' .myTabs-tabBar [tabAddress=' + tabAddress + ']');
   var tabPages = $('#' + myTabId + ' .myTabs-contents section');
   var tabPage = $('#' + myTabId + ' .myTabs-contents [tabAddress=' + tabAddress + ']');

   tabBtns.removeClass("myTabs-tabBar-tabSelect");
   tabPages.css("display","none");

   tabBtn.addClass("myTabs-tabBar-tabSelect");
   tabPage.css("display","block");
}//---------------------------------------------------------------------------------------
var myTab_pageShow_=function(tabAddress)
{
   var spl=tabAddress.split("-");
   var myTabIndex=spl[0];
   var pageIndex=spl[1];
   var myTabs = $('.myTabs');
   tabIndex=-1;
	myTabs.each(function()
   {
      tabIndex++;
      if(tabIndex==tabIndex)
      {
   		var tab = $(this);
         var tabBtns = tab.find('.myTabs-tabBar span');
   		var tabPages = tab.find('.myTabs-contents section');

         tabPages.css("display","none");
         tabPages[pageIndex].style.display="block";

         tabBtns.removeClass("myTabs-tabBar-tabSelect");
      }
	});
}//---------------------------------------------------------------------------------------
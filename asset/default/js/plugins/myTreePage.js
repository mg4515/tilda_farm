function myTreePage(object)
{
	var this_=[];
	var funcTemp="";
	this.dataValue=[];
	object=object || [];
	if(typeof object != "object") object=[];
	object.elementId=object.elementId || "";
	object.elementId_temp="";
	object.onClick= object.onClick || "";

	obj=document.getElementById(object.elementId);
	items=obj.getElementsByTagName("li"); 
	for(i=0;i < items.length;i++)
	{
		if(object.onClick && items[i].getElementsByTagName("ul").length == 0) 
		{
			//event
			if(items[i].getAttribute("event")!="undefined")
			{ 
				items[i].onmousedown=function(){
					dataValue={
						"dataValue1":this.getAttribute("dataValue1"),
						"dataValue2":this.getAttribute("dataValue2"),
						"dataValue3":this.getAttribute("dataValue3"),
						"dataValue4":this.getAttribute("dataValue4"),
						"title":this.getAttribute("title"),
						"selected":this.getAttribute("selected"),
						"id":this.id
					};						
				} 					
				items[i].onclick=function(){object.onClick(dataValue,this_)};
				
				//show selected
				if(items[i].getAttribute("selected")!= null)
				{	
					items[i].setAttribute("class","myTreePage-item-selected");
					if(items[i].parentElement.parentNode.onmousedown!=null)
					{
						var count=document.getElementById(object.elementId).getElementsByTagName("ul").length;
						objEl=items[i].parentElement;
						for(iii=0;iii < count;iii++)
						{
						   if(objEl.id != object.elementId)
							{
							   if(typeof objEl.parentNode.onmousedown == 'function')objEl.parentNode.onmousedown();
								objEl=objEl.parentElement;
							}
                     else
                        break;								
						}
					}
				}				
			}					
		}
		else if(items[i].getElementsByTagName("ul").length > 0)
		{
			items[i].setAttribute("index",i);
			items[i].setAttribute("class","myTreePage-item-parent");
			items[i].id='li_' + object.elementId + '_' + i;
			items[i].getElementsByTagName("ul")[0].id='ul_' + object.elementId + '_' + i;
			items[i].getElementsByTagName("ul")[0].setAttribute("class","myTreePage-hide");              
			//to page					
			items[i].onmousedown=function()
			{
				this.getElementsByTagName("ul")[0].setAttribute("class","myTreePage-show");
				funcTemp=this.onmousedown;
				this.onmousedown="";
				return false;	
			};
			//back page
			items[i].getElementsByTagName("ul")[0].innerHTML = "<li id='li_" + object.elementId + "_back_" + i + "' index='" + i + "' event='undefined' class='myTreePage-item-back'><span>" + items[i].getAttribute("title") + "</span></li>" + items[i].getElementsByTagName("ul")[0].innerHTML;
			document.getElementById('li_' + object.elementId + '_back_' + i).onclick=function()
			{
				document.getElementById('li_' + object.elementId + '_' + this.getAttribute("index")).onmousedown=funcTemp;
				document.getElementById('ul_' + object.elementId + '_' + this.getAttribute("index")).setAttribute("class","myTreePage-hide");
			};				
		}
	}
	this_=this;
	return this;			
}
function myList(object)
{
	var this_=[];
	this.selected=[];
	this.dataValue=[];
	this.selectedIds=[];
	object=object || [];
	if(typeof object != "object") object=[];
	object.elementId=object.elementId || "";
	object.elementId_temp="";
	object.multiSelect= object.multiSelect || false;
   object.onClick= object.onClick || "";
   object.onSelected= object.onSelected || "";

	obj=document.getElementById(object.elementId);
   items=obj.getElementsByTagName("li"); 
   for(i=0;i < items.length;i++)
	{
      if(object.onClick) items[i].onclick=function(){object.onClick(this)};
      if(object.multiSelect==false) 
		{
			items[i].id="li_" + object.elementId + "_" + i;
			items[i].innerHTML+="<input type='radio' name='rdo_" + object.elementId + "' id='rdo_" + object.elementId + "_" + i + "' style='display:none;'>";		
			items[i].onmouseup=function(){
				var radio = this.getElementsByTagName("input")[0];
				if(radio.checked==false)
				{				
					radio.checked=true; 
					this.setAttribute("class","myList-item-selected"); //selected Style
					if(selectedIds != "") document.getElementById(selectedIds).setAttribute("class",""); //unSelected style
					dataValue={
						"dataValue1":this.getAttribute("dataValue1"),
						"dataValue2":this.getAttribute("dataValue2"),
						"dataValue3":this.getAttribute("dataValue3"),
						"dataValue4":this.getAttribute("dataValue4"),
						"title":this.getAttribute("title"),
						"id":this.id
					};
					selectedIds=this.id;
				}
				else 
				{
					radio.checked=false;
					this.setAttribute("class","");
					dataValue="";
					selectedIds="";
				}
				if(object.onSelected)
				{
					object.onSelected(dataValue,this_);							
				}
			};
		}
		else
		{
			items[i].id="li_" + object.elementId + "_" + i;
			items[i].innerHTML+="<input type='checkbox' name='rdo_" + object.elementId + "' id='rdo_" + object.elementId + "_" + i + "' style='display:none;'>";		
			items[i].onmouseup=function(){
				var radio = this.getElementsByTagName("input")[0];
				if(radio.checked==false)
				{				
					radio.checked=true; 
					this.setAttribute("class","myList-item-selected"); //selected Style
					var index=selectedIds.indexOf(this.id);
					if(index <= -1) 
					{
						dataValue.push({
							"dataValue1":this.getAttribute("dataValue1"),
							"dataValue2":this.getAttribute("dataValue2"),
							"dataValue3":this.getAttribute("dataValue3"),
							"dataValue4":this.getAttribute("dataValue4"),
							"title":this.getAttribute("title"),
							"id":this.id
					   });
						selectedIds.push(this.id);
					}
				}
				else 
				{
					radio.checked=false;
					this.setAttribute("class","");
					var index=selectedIds.indexOf(this.id);
					if(index > -1) 
					{
						//delete dataValue[index];
						dataValue.splice(index,1);
						//delete selectedIds[index];
						selectedIds.splice(index,1);
					}
				}
				if(object.onSelected)
				{
					object.onSelected(dataValue,this_);							
				}
			};			
		}
	}
	
	this.getSelected=function(dataValueName)
	{	
	   if(object.multiSelect==false) 
		{
			index=0;
		   var ret= this.dataValue;
			if(ret)
			{
				if(dataValueName=="dataValue1" || dataValueName=="dataValue2" || dataValueName=="dataValue3" || dataValueName=="dataValue4")
					return[ret[dataValueName]];
				else
					return[ret];
			}
			else
				return [];
		}
		else
		{
		   var ret_= this.dataValue;
			if(dataValueName=="dataValue1" || dataValueName=="dataValue2" || dataValueName=="dataValue3" || dataValueName=="dataValue4")
			{	
		      var ret=[];
		      for(i=0;i<ret_.length;i++)
				{
			      ret.push(ret_[i][dataValueName]);
				}
			}
			else
				ret=ret_;
			return ret;			
		}
   }
	this_=this;
	return this;
}
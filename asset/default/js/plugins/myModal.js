//programmer : mohammad gholamy - mg4515@gmail.com
function myModal_init(idElement)
{
   $("#" + idElement).animate({top:"0"},1,function(){$("#" + idElement + ">.myModal-content").animate({opacity:0},1)});
   $("#" + idElement).fadeOut(1,function(){$("#" + idElement + ">.myModal-content").animate({opacity:0},1)});

}
function myModal_show(idElement,content,type,height,width)
{
	//init 
   if(idElement=="#")
	{
		var div = document.createElement('DIV');
		div.id = "layer_myModalTemp";
		div.setAttribute("class","myModal myModal-autoSize");
		div.innerHTML=""+
		"<div class='myModal-container' id='layer_modal_container'>" +
		   "<div class='myModal-topBar'>" +
			   "<i class='fa fa-times' onclick=\"myModal_hide('#');\"></i>" +
			"</div>" +
			"<div class='myModal-content' id='layer_modal_content'></div>" +
			"<div class='myModal-bottomBar' id='layer_modal_bottomBar' style='height:36px'></div>" +
		"</div>";
		document.body.appendChild(div);
		idElement="layer_myModalTemp";
		//document.body.removeChild(a);				
	}
	
	$("#" + idElement).css("visibility","visible");
	$("#" + idElement + ">.myModal-container").css("transform","scale(0,0)");
   if(height!=undefined) $("#" + idElement + ">.myModal-container").css("height",height);
   if(width!=undefined) $("#" + idElement + ">.myModal-container").css("width",width);

   if(type=="success") $("#" + idElement + ">.myModal-container>.myModal-titleBar").attr("class","myModal-titleBar bg-success");
   else if(type=="danger") $("#" + idElement + ">.myModal-container>.myModal-titleBar").attr("class","myModal-titleBar bg-danger");
   else if(type=="info") $("#" + idElement + ">.myModal-container>.myModal-titleBar").attr("class","myModal-titleBar bg-info");
   else if(type=="warning") $("#" + idElement + ">.myModal-container>.myModal-titleBar").attr("class","myModal-titleBar bg-warning");
   
	
	$("#" + idElement).animate({opacity:1},200,function()
	{
	   //$("#" + idElement + ">.myModal-container").animate({transform:scale(1,1)},200);
	   $("#" + idElement + ">.myModal-container").css("transform","scale(1,1)");
	});
   if(content!=undefined) $("#" + idElement + ">.myModal-container>.myModal-content").html(content);
   $("html").css("overflow-y","hidden");
}//---------------------------------------------------------------------------------------
function myModal_hide(idElement)
{
	if(idElement=="#") idElement="layer_myModalTemp";
	if(idElement==undefined) idElement="layer_myModalTemp";
	var hTimer=window.setTimeout(function(){
		$("#" + idElement + ">.myModal-container").css("transform","scale(0,0)");
      $("#" + idElement).animate({opacity:0},200,function()
		{
			$("#" + idElement).css("visibility","hidden");
			$("html").css("overflow-y","auto");
		});
	},200);
	
	if(idElement=="#" || idElement==undefined)
	{
		div=getElementById("layer_myModalTemp");
	   document.body.removeChild(div);
	}
}//---------------------------------------------------------------------------------------
function myModal_hide_(idElement)
{
	var myModal_=new myModal();
	myModal_.hide(idElement);
}//---------------------------------------------------------------------------------------
var myModal=function(object)
{
	object=object || [];
	if(typeof object != "object") object=[];
	object.elementById=object.elementById || "";
	object.elementById_temp="";
	object.title=object.title || "";
	object.status=object.status || "";
	object.showBottomBar= object.showBottomBar || false;
	object.showTopBar=object.showTopBar || true;
	object.showTopBarClose=object.showTopBarClose || true;
	object.topBarContent=object.topBarContent || "";
   object.topBarStyle=object.topBarStyle || "";	
	object.hiddenContent=object.hiddenContent || "";
	object.centerContent=object.centerContent || "";
	object.centerContentById=object.centerContentById || "";
	object.bottomBarContent=object.bottomBarContent || "";
	object.bottomBarStyle=object.bottomBarStyle || "";
	object.width=object.width || undefined;
	object.height=object.height || undefined;
   this.show=function(callback,blnHidden)
	{
		//init 
		if(object.elementById=="")
		{
			object.elementById="layer_myModalTemp" + Date.now();
			object.elementById_temp=object.elementById;
			var innerHtml="";
			var div = document.createElement('DIV');
			div.id = object.elementById;
			div.setAttribute("class","myModal myModal-autoSize");
			innerHtml="<div class='myModal-container' id='layer_modal_container'>";
			   innerHtml+="<div class='myModal-hidden' id='layer_modal_hidden'>" + object.hiddenContent + "</div>";
			   if(object.showTopBar==true)
				{	
				   innerHtml+="<div class='myModal-topBar'>";
					   if(object.showTopBarClose==true) innerHtml+="<i class='fa fa-times' onclick='myModal_hide_(\"" + object.elementById + "\");'></i>";
				      innerHtml+="<span>" + object.title + "</span>";
				      innerHtml+="<span style='" + object.topBarStyle + "'>" + object.topBarContent + "</span>";
				   innerHtml+="</div>";
				}
				innerHtml+="<div class='myModal-content' id='layer_modal_content'></div>";
				if(object.showBottomBar==true) innerHtml+="<div class='myModal-bottomBar' id='layer_modal_bottomBar' style='" + object.bottomBarStyle + "'></div>";
			innerHtml+="</div>";
			div.innerHTML=innerHtml;
			document.body.appendChild(div);

			//document.body.removeChild(a);				
		}
		idElement=object.elementById;		
		if(blnHidden==undefined || blnHidden==false) $("#" + idElement).css("visibility","visible");
		$("#" + idElement + ">.myModal-container").css("transform","scale(0,0)");
		if(object.height!=undefined) 
			$("#" + idElement + ">.myModal-container").css("height",object.height);
		else
		{
         if (window.matchMedia("(max-width: 768px)").matches)			
		      $("#" + idElement + ">.myModal-container").css("height","calc(100%)");	
		   else
				$("#" + idElement + ">.myModal-container").css("height","calc(100% - 101px)");
		}
		if(object.width!=undefined) $("#" + idElement + ">.myModal-container").css("width",object.width);

		if(object.status=="success") $("#" + idElement + ">.myModal-container>.myModal-topBar").attr("class","myModal-topBar bg-success");
		else if(object.status=="danger") $("#" + idElement + ">.myModal-container>.myModal-topBar").attr("class","myModal-topBar bg-danger");
		else if(object.status=="info") $("#" + idElement + ">.myModal-container>.myModal-topBar").attr("class","myModal-topBar bg-info");
		else if(object.status=="warning") $("#" + idElement + ">.myModal-container>.myModal-topBar").attr("class","myModal-topBar bg-warning");
		
		if(object.showBottomBar==true)
		{
		   $("#" + idElement + ">.myModal-container>.myModal-content").css("height","calc(100% - 101px)");	
		   if(object.bottomBarContent != "")
				$("#" + idElement + ">.myModal-container>.myModal-bottomBar").html(object.bottomBarContent);	
		}
		else
		{
		   $("#" + idElement + ">.myModal-container>.myModal-content").css("height","calc(100%)");	
		}
		
		$("#" + idElement).animate({opacity:1},200,function()
		{
			$("#" + idElement + ">.myModal-container").css("transform","scale(1,1)");
			if(callback!=undefined) callback();
		});
		if(object.centerContent!="") $("#" + idElement + ">.myModal-container>.myModal-content").html(object.centerContent);
		$("html").css("overflow-y","hidden");
		$("body").css("overflow-y","hidden");
	}//----------------------------------------------------------------------------------------
	this.hide=function(idElement)
	{
		if(idElement==undefined)
		   var idElement=object.elementById;
		else
			object.elementById_temp=idElement;
		$("#" + idElement + ">.myModal-container").css("transform","scale(0,0)");
		var hTimer=window.setTimeout(function(){
			$("#" + idElement).animate({opacity:0},200,function()
			{
				$("#" + idElement).css("visibility","hidden");
				$("html").css("overflow-y","auto");
				$("body").css("overflow-y","auto");
				if(object.elementById_temp != "")
				{
					div=document.getElementById(object.elementById_temp);
					document.body.removeChild(div);
				}				
			});
		},300);
	}//---------------------------------------------------------------------------------------	
}

// set options before less.js script
less = {
	env: "development",
	async: false,
	fileAsync: false,
	poll: 1000,
	functions: {},
	dumpLineNumbers: "comments",
	relativeUrls: false,
	rootpath: ":/a.com/"
};
function checkedAll()
{
   var count=$("#chk_all").val();
   if(document.getElementById("chk_all").checked==true)
   {
      for(i=1;i <= count ;i++)
      {
         document.getElementById("chk_" + i).checked=true;
      }
		document.getElementById("btn_trash").disabled=false;
   }
   else if(document.getElementById("chk_all").checked==false)
   {
      for(i=1;i <= count ;i++)
      {
         document.getElementById("chk_" + i).checked=false;
			try{document.getElementById("btn_delete").disabled=false;}catch(e){}
      }
		document.getElementById("btn_trash").disabled=true;
   }
}//----------------------------------------------------------------------------
function checkedOne()
{
	document.getElementById("btn_trash").disabled=true;
   var count=$("#chk_all").val();
	for(i=1;i <= count ;i++)
	{
		if(document.getElementById("chk_" + i).checked==true)
		{
		   document.getElementById("btn_trash").disabled=false;
		   try{document.getElementById("btn_delete").disabled=false;}catch(e){}
         break;			
		}
	}
}//----------------------------------------------------------------------------
function text_drawLength(textId,drawId,maxLength,refresh)
{
	document.getElementById(textId).onkeyup = function () { 
		if(document.querySelector('#' + textId).value.length <= maxLength) var color='green'; else var color='red';
	   document.querySelector('#' + drawId).style.color=color;
		document.getElementById(drawId).innerHTML = '<span style=\"color:#ddd\">' + maxLength + ' / </span>' + document.querySelector('#' + textId).value.length;
	};	
	if(refresh==true)
	{
		if(document.querySelector('#' + textId).value.length <= maxLength) var color='green'; else var color='red';
	   document.querySelector('#' + drawId).style.color=color;
		document.getElementById(drawId).innerHTML = '<span style=\"color:#ddd\">' + maxLength + ' / </span>' + document.querySelector('#' + textId).value.length;		
	}
}//---------------------------------------------------------------------------
var filePath_user=oPath.manage("user_bundle/controler/user_http_html.php");
isItemDelImg="";
function user_imgDel(userId)
{
   if(isItemDelImg==false)
   {
      var ret=confirm("تصویر حذف شود ؟");
      if(ret)
      {
         isItemDelImg=true;
         $("#img_user").attr("src",oPath.asset("default/images/user_larg.png"));
         $("#i_userImgDel").attr("class","fa fa-share fa-2x");
      }
   }
   else if(isItemDelImg==true)
   {
      isItemDelImg=false;
      $("#img_user").attr("src",oPath.manage("user_bundle/data/images/user_" + userId + ".jpg"));
      $("#i_userImgDel").attr("class","fa fa-trash fa-2x");
   }
}//-----------------------------------------------------------------------------
function users_search()
{
   searchWord=$("#txt_search").val();
   if(searchWord != "")
   	users_draw("","1",1,searchWord);	
   else
	   users_draw("","",1);	
}//-----------------------------------------------------------------------------
function users_draw(showActive,showSearch,page,searchWord) {
	//try {
	   loading_show();
	   if(searchWord == undefined) searchWord="";
	   $.ajax({
			url: filePath_user,
			data: { "data":oEngine.request({"requestName": "users_draw","showActive":showActive,"showSearch":showSearch,"searchWord":searchWord,"page":page}) },
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
					$("#layer_content").html(spl[1]);
				else
				{
					alert("انجام نشد");
					alert(result);
				}
				loading_hide();
			},
			error: function() {
				alert("خطایی در اتصال رخ داده است");					
				loading_hide();
			}

	   });
	//} catch (e) {
	   //alert("خطای اسکریپت");
	   //loading_hide();
	//}
}//---------------------------------------------------------------------------------------
function users_listDraw(searchWord,selectedIds,multiSelect,page)
{
   try { 
      loading_show();
      $.ajax({
         url: filePath_user,
         data: {"requestName":"users_listDraw", "searchWord":searchWord, "selectedIds":selectedIds, "multiSelect":multiSelect, "page":page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					dialog=new myModal({
						'showBottomBar':true,
						'bottomBarContent': '<div style="padding-right:5px">' + spl[2] + '</div>',
						'centerContent':spl[1]
					});
					dialog.show();
            }
            else
            {
               alert("انجام نشد!.");
            }
            loading_hide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            loading_hide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		loading_hide();
   }
}//-----------------------------------------------------------------------------
function users_listDrawBySearch()
{
	dialog.hide();
   users_listDraw($("#txt_usersSearch"));	
}//-----------------------------------------------------------------------------
function users_treeDraw(showSearch,searchWord,selectId)
{
   try { 
      loading_show();
      $.ajax({
         url: filePath_user,
         data: { "data": oEngine.request({"requestName":"users_treeDraw", "showSearch":showSearch, "searchWord":searchWord, "selectId":selectId}) },
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_temp").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            loading_hide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            loading_hide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		loading_hide();
   }
}//-----------------------------------------------------------------------------
function user_del(userId) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			loading_show();
			if(userId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=userId;
			$.ajax({
				url: filePath_user,
				data: { "data":oEngine.request({"requestName": "user_del","id":id}) },
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						users_draw(spl[1],spl[2],spl[3]);
					}
					else
					{
						alert("انجام نشد");
						alert(result);
					}
					loading_hide();
				},
				error: function() {
					loading_hide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		} catch (e) {
			alert("خطای اسکریپت");
			loading_hide();
		}
    }
}//---------------------------------------------------------------------------------------
function user_active(userId)
{
   try {
      loading_show();
      $.ajax({
         url: filePath_user,
         data: {"data": oEngine.request({"requestName":"user_active","id":userId}) },
         method: "POST",
         success: function(result)
         {
            //alert(result);
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#td_active_" + spl[1]).html(spl[2]);
            }
            else
            {
               alert("انجام نشد");
            }
            loading_hide();
         },
         error: function() {
            loading_hide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		loading_hide();
   }
}//-----------------------------------------------------------------------------
function user_edit(userId) {
	try {
		loading_show();
		isItemDelImg=false;
		$.ajax({
			url: filePath_user,
			data: { "data":oEngine.request({"requestName": "user_edit","id":userId}) },
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
					$("#layer_content").html(spl[1]);
				else
				{
					alert("انجام نشد");
					alert(result);
				}
				loading_hide();
			},
				error: function() {
					loading_hide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			loading_hide();
		}
}//---------------------------------------------------------------------------------------
function user_new(userId) {
	try {
		loading_show();
		isItemDelImg=false;
		$.ajax({
			url: filePath_user,
			data: { "data":oEngine.request({"requestName": "user_new","id":userId}) },
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
					$("#layer_content").html(spl[1]);
				else
				{
					alert("انجام نشد");
					alert(result);
				}
				loading_hide();
			},
				error: function() {
					loading_hide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
	} 
	catch (e) 
	{
		alert("خطای اسکریپت");
		loading_hide();
	}
}//---------------------------------------------------------------------------------------
function user_update(purpose,userId)
{
   try {
      loading_show();
		if(purpose=="new") userName=$("#txt_userName").val(); else userName="";
      var formData = new FormData();
      formData.append('fileImg', $('#fileImg').prop('files')[0]);
      formData.append('data', oEngine.request({
			'imgDel': isItemDelImg,
			"userName":userName,
			"password":$("#txt_password").val(),
			//profile
			'dayCountAllow': $("#txt_dayCountAllow").val(),
			'fname': $("#txt_fname").val(),
			'lname': $("#txt_lname").val(),
			'phone': $("#txt_phone").val(),
			'mobile': $("#txt_mobile").val(),
			'email': $("#txt_email").val(),
			'dateOfBirth': $("#txt_dateOfBirth").val(),
			'fatherName': '',
			'melliCode': '',
			'country': '',
			'state': $("#slct_state").val(),
			'city': $("#slct_city").val(),
			'zipCode': $("#txt_zipCode").val(),
			'address1': $("#txt_address").val(),
			'address2': '',
			'bankName': '',
			'bankHesab': '',
			'bankCart': '',
			'comment': $("#comment").val(),
			'id': userId,
			'purpose': purpose,
			'requestName': "user_update",			
		}));
      $.ajax({
         url: filePath_user,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: formData,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="errType")
            {
               alert("نوع فایل تصویر، غیر مجاز می باشد");
            }
            else if(spl[0]=="errSize")
            {
               alert("سایز فایل تصویر، بیش از حد مجاز است");
            }
            else if(spl[0]=="ok")
            {
               users_draw(spl[1],spl[2],spl[3]);
            }
            else
            {
               alert("انجام نشد");
               alert(result);
            }
            loading_hide();
         },
         error: function() {
            loading_hide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
	}
	catch (e) 
	{
		alert("خطای اسکریپت");
		loading_hide();
	}
}//---------------------------------------------------------------------------------------
function user_profileDraw(userId)
{
   try {
      loading_show();
      $.ajax({
         url: filePath_user,
         data: { "data": oEngine.request({"requestName":"user_profileDraw","id":userId}) },
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
				alert(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					code=''+		
					"<div class='myModal myModal-autoSize' id='layer_modalProfile' >"+
						"<div class='myModal-container' id='layer_modal_container'>"+
							"<div class='myModal-topBar'>"+
								"<i class='fa fa-times' onclick='myModal_hide(\"layer_modalProfile\");'></i>"+
								"<span>" + spl[2] + "</span>"+
							"</div>"+
							"<div class='myModal-content' id='layer_modal_content'>"+
								"<div class='part-w-10 algn-c'>"+
									"<div class='imageCircle' style='background-image:url("+ spl[4] +")'></div>"+
									"<div>"+ spl[3] +"</div>"+
									"<hr>"+
									"<button class='btn btn-image btn-image-right btn-info part-w-5 part-w-10-auto' onclick='user_edit(" + spl[1] + ");myModal_hide(\"layer_modalProfile\");'>"+
										"<span>ویرایش مشخصات</span>"+
										"<i class='btn-image-icon fa fa-pencil'></i>"+
									"</button>"+
									"<div class='vSpace'></div>"+
								"</div>"+
							"</div>"+
						"</div>"+
					"</div>";
               $("#layer_temp").html(code);
					myModal_show('layer_modalProfile');
            }
            else
            {
               alert("انجام نشد!.");
            }
            loading_hide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            loading_hide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		loading_hide();
   }
}//-----------------------------------------------------------------------------
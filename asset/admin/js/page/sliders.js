var filePath_sliders=oPath.manage("sliders_bundle/controler/sliders_http_html.php");
function slider_del(sliderId) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
         loading_show();
         $.ajax({
            url: filePath_sliders,
            data: { "data": oEngine.request({"requestName":"slider_del", "id":sliderId}) },
            method: "POST",
            success: function(result)
            {
					result=oEngine.response(result);
               if(result=="ok")
               {
                  sliders_draw();
               }
               else
               {
                  alert("انجام نشد");
               }
               loading_hide();
            },
            error: function()
            {
					alert("خطایی در اتصال رخ داده است");
               loading_hide();
            }
         });
      }
      catch (e)
      {
			alert("خطای اسکریپت");
         loading_hide();
      }
   }
}//---------------------------------------------------------------------------------------
function slider_update(purpose,sliderId) {
   try {
      loading_show();
      var file_data = $('#file').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);
      form_data.append('data', oEngine.request({
			"title":$("#txt_title").val(),
			"purpose":purpose,
			"id":sliderId,
			"requestName":"slider_update"
		}));
      $.ajax({
         url: filePath_sliders,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            if(result=="ok")
            {
               sliders_draw();
            }
				else
				{
				   alert("انجام نشد");	
				}
            loading_hide();
         },
         error: function()
         {
            loading_hide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		loading_hide();
   }
}//---------------------------------------------------------------------------------------
function slider_edit(sliderId) {
   try {
      loading_show();
      $.ajax({
         url: filePath_sliders,
         data: { "data":oEngine.request({"requestName":"slider_edit", "id":sliderId}) },
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            spl=result.split("[|]");
            if(spl[0]=="ok")
               $("#layer_content").html(spl[1]);
            else
               alert("انجام نشد");
            loading_hide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            loading_hide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		loading_hide();
   }
}//---------------------------------------------------------------------------------------
function slider_new() {
   try {
      loading_show();
      $.ajax({
         url: filePath_sliders,
         data: { "data" : oEngine.request({"requestName":"slider_new"}) },
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            spl=result.split("[|]");
            if(spl[0]=="ok")
               $("#layer_content").html(spl[1]);
            else
               alert("انجام نشد");
            loading_hide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
				loading_hide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		loading_hide();
   }
}//---------------------------------------------------------------------------------------
function sliders_draw() {
   try {
      loading_hide();
      $.ajax({
         url: filePath_sliders,
         data: { "data" : oEngine.request({"requestName":"sliders_draw"}) },
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            spl=result.split("[|]");
            if(spl[0]=="ok")
               $("#layer_content").html(spl[1]);
            else
               alert("انجام نشد");
            loading_hide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
				loading_hide();
         }
      });
   }
   catch (e)
	{
		alert("خطای اسکریپت");
		loading_hide();
   }
}//---------------------------------------------------------------------------------------
var filePath_siteMailServer=oPath.manage("site_bundle/controler/siteMailServer_http_html.php");
function siteMailServer_update() {
    try {
        loading_show();
        var server=$("#txt_server").val();
        var port=$("#txt_port").val();
        var userName=$("#txt_userName").val();
        var password=$("#txt_password").val();
        var from=$("#txt_from").val();
        var active=document.getElementById("chk_active").checked;

        $.ajax({
            url: filePath_siteMailServer,
            data: { "data": oEngine.request({"requestName": "siteMailServer_update","server":server,"port":port,"userName":userName,"password":password,"from":from,"active":active}) },
            method: "POST",
            success: function(result)
            {
					result=oEngine.response(result);
               if(result=="ok")
               {
                  alert("با موفقیت انجام شد");
                  siteMailServer_draw();
					}
					else 
						alert(result);
               loading_hide();
            },
            error: function() {
                alert("خطایی در اتصال رخ داده است");
					 loading_hide();
            }

        });
    } catch (e) {
         alert("خطای اسکریپت");
			loading_hide();
    }
}//---------------------------------------------------------------------------------------
function siteMailServer_draw() {
    try {
        loading_show();
        $.ajax({
            url: filePath_siteMailServer,
            data: { "data": oEngine.request({"requestName": "siteMailServer_draw"}) },
            method: "POST",
            success: function(result)
            {
					result=oEngine.response(result);
					var spl=result.split("[|]");
               if(spl[0]=="ok")
               {					
                  $("#layer_content").html(spl[1]);
					}
					else
						alert(result);
               loading_hide();
            },
            error: function() {
                loading_hide();
                alert("خطایی در اتصال رخ داده است");
            }

        });
    } catch (e) {
         alert("خطای اسکریپت");
			loading_hide();
    }
}//---------------------------------------------------------------------------------------

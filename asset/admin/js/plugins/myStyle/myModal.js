//programmer : mohammad gholamy - mg4515@gmail.com
function myModal_init(idElement)
{
   $("#" + idElement).animate({top:"0"},1,function(){$("#" + idElement + ">.myModal-content").animate({opacity:0},1)});
   $("#" + idElement).fadeOut(1,function(){$("#" + idElement + ">.myModal-content").animate({opacity:0},1)});

}
function myModal_show(idElement,content,type,height,width)
{
	//init 

	$("#" + idElement).css("visibility","visible");
   if(height!=undefined) $("#" + idElement + ">.myModal-container").css("height",height);
   if(width!=undefined) $("#" + idElement + ">.myModal-container").css("width",width);

   if(type=="success") $("#" + idElement + ">.myModal-container>.myModal-titleBar").attr("class","myModal-titleBar bg-success");
   else if(type=="danger") $("#" + idElement + ">.myModal-container>.myModal-titleBar").attr("class","myModal-titleBar bg-danger");
   else if(type=="info") $("#" + idElement + ">.myModal-container>.myModal-titleBar").attr("class","myModal-titleBar bg-info");
   else if(type=="warning") $("#" + idElement + ">.myModal-container>.myModal-titleBar").attr("class","myModal-titleBar bg-warning");
   
	
	$("#" + idElement).animate({opacity:1},200,function()
	{
	   $("#" + idElement + ">.myModal-container").fadeIn(200);
	});
   if(content!=undefined) $("#" + idElement + ">.myModal-container>.myModal-content").html(content);
   $("html").css("overflow-y","hidden");
}//---------------------------------------------------------------------------------------
function myModal_hide(idElement)
{
   $("#" + idElement + ">.myModal-container").fadeOut(200,function()
   {
      $("#" + idElement).animate({opacity:0},200,function(){$("#" + idElement).css("visibility","hidden");});
   });
   $("html").css("overflow-y","auto");
}//---------------------------------------------------------------------------------------
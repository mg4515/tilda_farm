<?php
class cDatabase
{
   public $host = DATABASE_HOST;
   public $userName = DATABASE_USERNAME;
   public $password = DATABASE_PASSWORD;
   public $databaseName = DATABASE_DATABASE_NAME;
   public $dbLink;
   private $countQuery=0;
   public function __construct()
   {
      $this->dbLink=new mysqli($this->host,$this->userName,$this->password,$this->databaseName);
      $this->dbLink->set_charset("utf8");
      $this->dbLink->query("SET NAMES UTF8");
   }//---------------------------------------------------------------------
   public function connect($host,$user,$password,$databaseName)
   {
      $this->dbLink=new mysqli($host,$user,$password,$databaseName);
      return $this->dbLink;
   }//---------------------------------------------------------------------
   static function escape($value,$full=true,$html=true)
   {
		if($html==true)
			$value=htmlspecialchars($value);
      if($full==true)
      {
         $value = str_ireplace(",", '|', $value);
			//---
         $value = str_ireplace("'", '', $value);
			//---
         $value = str_ireplace('"', '', $value);
			//---
         $value = str_ireplace(' union ', '/union/', $value);
         $value = str_ireplace('union ', '/union/', $value);
         $value = str_ireplace(' union', '/union/', $value);
			//---
         $value = str_ireplace(' from ', '/from/', $value);
         $value = str_ireplace('from ', '/from/', $value);
         $value = str_ireplace(' from', '/from/', $value);
			//---
         $value = str_ireplace(' select ', '/select/', $value);
         $value = str_ireplace(' select', '/select/', $value);
         $value = str_ireplace('select ', '/select/', $value);
			//---
         $value = str_ireplace(' insert ', '/insert/', $value);
         $value = str_ireplace(' insert', '/insert/', $value);
         $value = str_ireplace('insert ', '/insert/', $value);
			//---
         $value = str_ireplace(' update ', '/update/', $value);
         $value = str_ireplace(' update', '/update/', $value);
         $value = str_ireplace('update ', '/update/', $value);
			//---
         $value = str_ireplace(' alter ', '/alter/', $value);
         $value = str_ireplace(' alter', '/alter/', $value);
         $value = str_ireplace('alter ', '/alter/', $value);
			//---
         $value = str_ireplace(' create ', '/create/', $value);
         $value = str_ireplace(' create', '/create/', $value);
         $value = str_ireplace('create ', '/create/', $value);
			//---
         $value = str_ireplace(' delete ', '/delete/', $value);
         $value = str_ireplace(' delete', '/delete/', $value);
         $value = str_ireplace('delete ', '/delete/', $value);
			//---
         $value = str_ireplace(' into ', '/into/', $value);
         $value = str_ireplace(' into', '/into/', $value);
         $value = str_ireplace('into ', '/into/', $value);
			//---
         $value = str_ireplace(' outfile ', '/outfile/', $value);
         $value = str_ireplace(' outfile', '/outfile/', $value);
         $value = str_ireplace('outfile ', '/outfile/', $value);
			//---
         $value = str_ireplace(' where ', '/where/', $value);
         $value = str_ireplace(' where', '/where/', $value);
         $value = str_ireplace('where ', '/where/', $value);
			//---
         $value = str_ireplace(' backup ', '/backup/', $value);
         $value = str_ireplace(' backup', '/backup/', $value);
         $value = str_ireplace('backup ', '/backup/', $value);
			//---
         $value = str_ireplace(' index ', '/index/', $value);
         $value = str_ireplace(' index', '/index/', $value);
         $value = str_ireplace('index ', '/index/', $value);
			//---
         $value = str_ireplace(' limit ', '/limit/', $value);
         $value = str_ireplace(' limit', '/limit/', $value);
         $value = str_ireplace('limit ', '/limit/', $value);
			//---
         $value = str_ireplace(' set ', '/set/', $value);
         $value = str_ireplace(' set', '/set/', $value);
         $value = str_ireplace('set ', '/set/', $value);
			//---
         $value = str_ireplace(' values ', '/values/', $value);
         $value = str_ireplace(' values', '/values/', $value);
         $value = str_ireplace('values ', '/values/', $value);
			//---
         $value = str_ireplace(' order ', '/order/', $value);
         $value = str_ireplace(' order', '/order/', $value);
         $value = str_ireplace('order ', '/order/', $value);
			//---
         $value = str_ireplace(' group ', '/group/', $value);
         $value = str_ireplace(' group', '/group/', $value);
         $value = str_ireplace('group ', '/group/', $value);
			//---
         $value = str_ireplace(' -- ', '/--/', $value);
         $value = str_ireplace(' --', '/--/', $value);
         $value = str_ireplace('-- ', '/--/', $value);
			//---
         $value = str_ireplace('/**/', '_/_*_*_/_', $value);
			//---
         $value = str_ireplace('/*', '_/_*_', $value);
         $value = str_ireplace('/*', '_/_*_', $value);
         $value = str_ireplace('/*', '_/_*_', $value);
			//---
         $value = str_ireplace(' = ', '/=/', $value);
         $value = str_ireplace(' =', '/=/', $value);
         $value = str_ireplace('= ', '/=/', $value);
			//---
         $value = str_ireplace(' > ', '/>/', $value);
         $value = str_ireplace(' >', '/>/', $value);
         $value = str_ireplace('> ', '/>/', $value);
			//---
         $value = str_ireplace(' < ', '/>/', $value);
         $value = str_ireplace(' <', '/>/', $value);
         $value = str_ireplace('< ', '/>/', $value);
			//---
         $value = str_ireplace(' null ', '/null/', $value);
         $value = str_ireplace(' null', '/null/', $value);
         $value = str_ireplace('null ', '/null/', $value);
			//---
         $value = str_ireplace(' not ', '/not/', $value);
         $value = str_ireplace(' not', '/not/', $value);
         $value = str_ireplace('not ', '/not/', $value);
			//---
         $value = str_ireplace(' or ', '/or/', $value);
         $value = str_ireplace(' or', '/or/', $value);
         $value = str_ireplace('or ', '/or/', $value);
			//---
         $value = str_ireplace(' xor ', '/xor/', $value);
         $value = str_ireplace(' xor', '/xor/', $value);
         $value = str_ireplace('xor ', '/xor/', $value);
			//---
         $value = str_ireplace(' and ', '/and/', $value);
         $value = str_ireplace(' and', '/and/', $value);
         $value = str_ireplace('and ', '/and/', $value);
			//---
         $value = str_ireplace(' ! ', '/!/', $value);
         $value = str_ireplace(' !', '/!/', $value);
         $value = str_ireplace('! ', '/!/', $value);
			//---
         $value = str_ireplace(' like ', '/like/', $value);
         $value = str_ireplace(' like', '/like/', $value);
         $value = str_ireplace('like ', '/like/', $value);
			//---
         $value = str_ireplace(' regexp ', '/regexp/', $value);
         $value = str_ireplace(' regexp', '/regexp/', $value);
         $value = str_ireplace('regexp ', '/regexp/', $value);
			//---
			$value=htmlspecialchars($value);
      }
		else
		{
         $value = str_ireplace(' union ', '/union/', $value);
         $value = str_ireplace('union ', '/union/', $value);
         $value = str_ireplace(' union', '/union/', $value);
			//---
         $value = str_ireplace(' from ', '/from/', $value);
         $value = str_ireplace('from ', '/from/', $value);
         $value = str_ireplace(' from', '/from/', $value);
			//---
         $value = str_ireplace(' select ', '/select/', $value);
         $value = str_ireplace(' select', '/select/', $value);
         $value = str_ireplace('select ', '/select/', $value);
			//---
         $value = str_ireplace(' insert ', '/insert/', $value);
         $value = str_ireplace(' insert', '/insert/', $value);
         $value = str_ireplace('insert ', '/insert/', $value);
			//---
         $value = str_ireplace(' update ', '/update/', $value);
         $value = str_ireplace(' update', '/update/', $value);
         $value = str_ireplace('update ', '/update/', $value);
			//---
         $value = str_ireplace(' alter ', '/alter/', $value);
         $value = str_ireplace(' alter', '/alter/', $value);
         $value = str_ireplace('alter ', '/alter/', $value);
			//---
         $value = str_ireplace(' create ', '/create/', $value);
         $value = str_ireplace(' create', '/create/', $value);
         $value = str_ireplace('create ', '/create/', $value);
			//---
         $value = str_ireplace(' delete ', '/delete/', $value);
         $value = str_ireplace(' delete', '/delete/', $value);
         $value = str_ireplace('delete ', '/delete/', $value);
			//---
         $value = str_ireplace(' into ', '/into/', $value);
         $value = str_ireplace(' into', '/into/', $value);
         $value = str_ireplace('into ', '/into/', $value);
			//---
         $value = str_ireplace(' outfile ', '/outfile/', $value);
         $value = str_ireplace(' outfile', '/outfile/', $value);
         $value = str_ireplace('outfile ', '/outfile/', $value);
			//---
         $value = str_ireplace(' where ', '/where/', $value);
         $value = str_ireplace(' where', '/where/', $value);
         $value = str_ireplace('where ', '/where/', $value);
			//---
         $value = str_ireplace(' backup ', '/backup/', $value);
         $value = str_ireplace(' backup', '/backup/', $value);
         $value = str_ireplace('backup ', '/backup/', $value);
			//---
         $value = str_ireplace(' index ', '/index/', $value);
         $value = str_ireplace(' index', '/index/', $value);
         $value = str_ireplace('index ', '/index/', $value);
			//---
         $value = str_ireplace(' limit ', '/limit/', $value);
         $value = str_ireplace(' limit', '/limit/', $value);
         $value = str_ireplace('limit ', '/limit/', $value);
			//---
         $value = str_ireplace(' set ', '/set/', $value);
         $value = str_ireplace(' set', '/set/', $value);
         $value = str_ireplace('set ', '/set/', $value);
			//---
         $value = str_ireplace(' values ', '/values/', $value);
         $value = str_ireplace(' values', '/values/', $value);
         $value = str_ireplace('values ', '/values/', $value);
			//---
         $value = str_ireplace(' order ', '/order/', $value);
         $value = str_ireplace(' order', '/order/', $value);
         $value = str_ireplace('order ', '/order/', $value);
			//---
         $value = str_ireplace(' group ', '/group/', $value);
         $value = str_ireplace(' group', '/group/', $value);
         $value = str_ireplace('group ', '/group/', $value);
			//---
         $value = str_ireplace(' -- ', '/--/', $value);
         $value = str_ireplace(' --', '/--/', $value);
         $value = str_ireplace('-- ', '/--/', $value);
			//---
         $value = str_ireplace('/**/', '_/_*_*_/_', $value);
			//---
         $value = str_ireplace('/*', '_/_*_', $value);
         $value = str_ireplace('/*', '_/_*_', $value);
         $value = str_ireplace('/*', '_/_*_', $value);
			//---
         $value = str_ireplace(' = ', '/=/', $value);
         $value = str_ireplace(' =', '/=/', $value);
         $value = str_ireplace('= ', '/=/', $value);
			//---
         $value = str_ireplace(' > ', '/>/', $value);
         $value = str_ireplace(' >', '/>/', $value);
         $value = str_ireplace('> ', '/>/', $value);
			//---
         $value = str_ireplace(' < ', '/>/', $value);
         $value = str_ireplace(' <', '/>/', $value);
         $value = str_ireplace('< ', '/>/', $value);
			//---
         $value = str_ireplace(' null ', '/null/', $value);
         $value = str_ireplace(' null', '/null/', $value);
         $value = str_ireplace('null ', '/null/', $value);
			//---
         $value = str_ireplace(' not ', '/not/', $value);
         $value = str_ireplace(' not', '/not/', $value);
         $value = str_ireplace('not ', '/not/', $value);
			//---
         $value = str_ireplace(' or ', '/or/', $value);
         $value = str_ireplace(' or', '/or/', $value);
         $value = str_ireplace('or ', '/or/', $value);
			//---
         $value = str_ireplace(' xor ', '/xor/', $value);
         $value = str_ireplace(' xor', '/xor/', $value);
         $value = str_ireplace('xor ', '/xor/', $value);
			//---
         $value = str_ireplace(' and ', '/and/', $value);
         $value = str_ireplace(' and', '/and/', $value);
         $value = str_ireplace('and ', '/and/', $value);
			//---
         $value = str_ireplace(' ! ', '/!/', $value);
         $value = str_ireplace(' !', '/!/', $value);
         $value = str_ireplace('! ', '/!/', $value);
			//---
         $value = str_ireplace(' like ', '/like/', $value);
         $value = str_ireplace(' like', '/like/', $value);
         $value = str_ireplace('like ', '/like/', $value);
			//---
         $value = str_ireplace(' regexp ', '/regexp/', $value);
         $value = str_ireplace(' regexp', '/regexp/', $value);
         $value = str_ireplace('regexp ', '/regexp/', $value);
			$value=htmlspecialchars($value);			
		}
      //return $this->dbLink->real_escape_string($value);
      return $value;
   }//---------------------------------------------------------------------
}

class cDatabaseQuery
{
   private $table;
   private $fields=[];
   private $values=[];
   private $where="";
   private $orderBy="";
   private $groupBy="";
   private $limit="";
   private $dbLink;
   private $oMyDb;
	private $query;
   public function __construct($oMyDb)
   {
      $this->dbLink=$oMyDb->dbLink;
      $this->oMyDb=$oMyDb;
   }//---------------------------------------------------------------------
   private function reseter()
	{
      $this->fields=[];
      $this->values=[];
      $this->where='';
      $this->limit= '';
      $this->orderBy='';
      $this->groupBy= '';
	}//---------------------------------------------------------------------
   private function err($dbLink="",$query="",$msg="")
   {
      if($dbLink!="") $dbl=$dbLink; else $dbl=$this->dbLink;
      $errType=array(
         E_ERROR=>'ERROR',
         E_WARNING=>'WARNING',
         E_PARSE=>'PARSE',
         E_NOTICE=>'NOTICE',
         E_CORE_ERROR=>'CORE_ERROR',
         E_CORE_WARNING=>'CORE_WARNING',
         E_COMPILE_ERROR=>'COMPILE_ERROR',
         E_COMPILE_WARNING=>'COMPILE_WARNING',
         E_USER_ERROR=>'USER_ERROR',
         E_USER_WARNING=>'USER_WARNING',
         E_USER_NOTICE=>'USER_NOTICE',
         E_STRICT=>'STRICT',
         E_RECOVERABLE_ERROR=>'RECOVERABLE_ERROR',
         E_DEPRECATED=>'DEPRECATED',
         E_USER_DEPRECATED=>'USER_DEPRECATED'
      );
      $errCode="";
      $debugCode="";
      $debugInfo=debug_backtrace();
      $err=error_get_last();
      for($i=0;$i < count($debugInfo);$i++)
      {
         if($debugInfo[$i]["function"] != "err" && $debugInfo[$i]["class"] != "myDb_query")
         {
            $file=$debugInfo[$i]["file"];
            $class=$debugInfo[$i]["class"];
            $object=$debugInfo[$i]["object"];
            $function=$debugInfo[$i]["function"];
            $line=$debugInfo[$i]["line"];
            if($debugInfo[$i]["type"]== "->") $type="method call";
            else if($debugInfo[$i]["type"]== "::") $type="static method call";
            else $type="function call";
            $debugCode.=
                     "path : " . $file . "<br>\n" .
                     "class : " . $class . "<br>\n" .
                     "function : " . $function . "<br>\n" .
                     "type : " . $type . "<br>\n" .
                     "line : " . $line . "<br>\n" .
                     "<hr>\n\n";
         }
      }
      $type=@$errType[$err["type"]];
      $errCode="ERROR {$type} : <br>\n" .
              "message : " . $err["message"] . "<br>\n" . $msg . "<br>\n";
              if($query!="") $errCode.="query : " . $query . "<br>\n";
              $errCode.="path : " . $err["file"] . "<br>\n" .
              "line : " . $err["line"] . "<br><br><br>\n\n\n" .
              "The codes found : <br><br>\n\n" .
              $debugCode;

/*      $style='background-color:red;color:#000;font-size:20px !important;border:1px solid #000;padding:5px;width:80px';
      $style2='background-color:#000;color:red;font-size:20px !important;border:1px solid #000;padding:5px';
      $strErr.="<tr><td style='{$style}'>type : </td><td style='{$style2}'>" . @$errType[$err["type"]] . "</td></tr>";
      $strErr.="<tr><td style='{$style}'>message : </td><td style='{$style2}'>" . $err["message"] . "</td></tr>";
      $strErr.="<tr><td style='{$style}'>file : </td><td style='{$style2}'>" . $err["file"] . "</td></tr>";
      $strErr.="<tr><td style='{$style}'>line : </td><td style='{$style2}'>" . $err["line"] . "</td></tr>";
      $strErr.="<tr><td style='{$style}'>dbError : </td><td style='{$style2}'>{$dbErrMsg} = " . $dbl->error . "</td></tr>";
      return "<table style='direction:ltr;text-align:left'>" . $strErr . "</table>";*/
      //print_r(debug_backtrace()[1]);
      return @$errCode;
   }//---------------------------------------------------------------------
   private function err_($dbLink="",$query="",$msg="")
   {
      if($dbLink!="") $dbl=$dbLink; else $dbl=$this->dbLink;
      $errCode="";
      $debugCode="";
      $debugInfo=debug_backtrace();
      $err=error_get_last();
      for($i=0;$i < count($debugInfo);$i++)
      {
         if($debugInfo[$i]["function"] != "err" && $debugInfo[$i]["class"] != "myDb_query")
         {
            $file=$debugInfo[$i]["file"];
            $class=$debugInfo[$i]["class"];
            $object=$debugInfo[$i]["object"];
            $function=$debugInfo[$i]["function"];
            $line=$debugInfo[$i]["line"];
            if($debugInfo[$i]["type"]== "->") $type="method call";
            else if($debugInfo[$i]["type"]== "::") $type="static method call";
            else $type="function call";
            $debugCode.=
                     "path : " . $file . "<br>\n" .
                     "class : " . $class . "<br>\n" .
                     "function : " . $function . "<br>\n" .
                     "type : " . $type . "<br>\n" .
                     "line : " . $line . "<br>\n" .
                     "------------------------------------------------------<br>\n\n";
         }
      }
      $type=@$errType[$err["type"]];
              $errCode="The errors found : <br><br>\n\n";
				  if($query!="") $errCode="query : " . $query . "<br>\n";
              $errCode.=$debugCode;
      return @$errCode;
   }//---------------------------------------------------------------------	
   public function escape($value,$full=true,$html=true)
   {
		if($html==true)
			$value=htmlspecialchars($value);
      if($full==true)
      {
         $value = str_ireplace(",", '|', $value);
         $value = str_ireplace("'", '', $value);
         $value = str_ireplace('"', '', $value);
         $value = str_ireplace('union', '/union/', $value);
         $value = str_ireplace('from', '/from/', $value);
         $value = str_ireplace('select', '/select/', $value);
         $value = str_ireplace('insert', '/insert/', $value);
         $value = str_ireplace('update', '/update/', $value);
         $value = str_ireplace('alter', '/alter/', $value);
         $value = str_ireplace('create', '/create/', $value);
         $value = str_ireplace('delete', '/delete/', $value);
         $value = str_ireplace('into', '/into/', $value);
         $value = str_ireplace('outfile', '/outfile/', $value);
         $value = str_ireplace('where', '/where/', $value);
         $value = str_ireplace('backup', '/backup/', $value);
         $value = str_ireplace('index', '/index/', $value);
         $value = str_ireplace('limit', '/limit/', $value);
         $value = str_ireplace('set', '/set/', $value);
         $value = str_ireplace('values', '/values/', $value);
         $value = str_ireplace('order', '/order/', $value);
         $value = str_ireplace('group', '/group/', $value);
         $value = str_ireplace('--', '/--/', $value);
         $value = str_ireplace('/**/', '_/_*_*_/_', $value);
         $value = str_ireplace('/*', '_/_*_', $value);
         $value = str_ireplace('=', '/=/', $value);
         $value = str_ireplace('>', '/>/', $value);
         $value = str_ireplace('<', '/>/', $value);
         $value = str_ireplace('null  ', '/null/', $value);
         $value = str_ireplace('not ', '/not/', $value);
         $value = str_ireplace('or', '/or/', $value);
         $value = str_ireplace('xor', '/xor/', $value);
         $value = str_ireplace('and', '/and/', $value);
         $value = str_ireplace('!', '/!/', $value);
         $value = str_ireplace('like', '/like/', $value);
         $value = str_ireplace('regexp', '/regexp/', $value);
			$value=htmlspecialchars($value);
      }
		else
		{
         $value = str_ireplace(",", '|', $value);
         $value = str_ireplace("'", '', $value);
         $value = str_ireplace('"', '', $value);
         $value = str_ireplace('union', '/union/', $value);
         $value = str_ireplace('from', '/from/', $value);
         $value = str_ireplace('select', '/select/', $value);
         $value = str_ireplace('insert', '/insert/', $value);
         $value = str_ireplace('update', '/update/', $value);
         $value = str_ireplace('alter', '/alter/', $value);
         $value = str_ireplace('create', '/create/', $value);
         $value = str_ireplace('delete', '/delete/', $value);
         $value = str_ireplace('into', '/into/', $value);
         $value = str_ireplace('outfile', '/outfile/', $value);
         $value = str_ireplace('where', '/where/', $value);
         $value = str_ireplace('backup', '/backup/', $value);
         $value = str_ireplace('index', '/index/', $value);
         $value = str_ireplace('limit', '/limit/', $value);
         $value = str_ireplace('set', '/set/', $value);
         $value = str_ireplace('values', '/values/', $value);
         $value = str_ireplace('order', '/order/', $value);
         $value = str_ireplace('group', '/group/', $value);
         $value = str_ireplace('--', '/--/', $value);
         $value = str_ireplace('/**/', '_/_*_*_/_', $value);
         $value = str_ireplace('/*', '_/_*_', $value);
         $value = str_ireplace('=', '/=/', $value);
         $value = str_ireplace('>', '/>/', $value);
         $value = str_ireplace('<', '/>/', $value);
         $value = str_ireplace('!', '/!/', $value);
         $value = str_ireplace('like', '/like/', $value);
         $value = str_ireplace('regexp', '/regexp/', $value);
			$value=htmlspecialchars($value);			
		}
      return mysqli_real_escape_string($this->dbLink,$value);
   }//---------------------------------------------------------------------
   public function table($tableName)
   {
      $this->where="";
      $this->table=$tableName;
      return $this;
   }//---------------------------------------------------------------------
   public function set()
   {
      $arguments=func_get_args();
      $this->fields=[];
      $this->values=[];

      if(count($arguments) == 1)
      {
         $spl=explode(",",$arguments[0]);
         foreach($spl as $val)
         {
            $val=str_ireplace("`","",$val);			
            $valSpl=explode("=",$val);
            
				$fieldName=trim($valSpl[0]);
				$fieldName="`" . $fieldName . "`";
				$this->fields[]=$fieldName;
				
            $fieldValue=$valSpl[1];
            $this->values[]=$fieldValue;
         }
      }
      else
      {
         foreach($arguments as $val)
         {
            $val=str_ireplace("`","",$val);			
            $valSpl=explode("=",$val);
            
				$fieldName=trim($valSpl[0]);
				$fieldName="`" . $fieldName . "`";
				$this->fields[]=$fieldName;
				
            $fieldValue=$valSpl[1];
            $this->values[]=$fieldValue;
         }
      }
      return $this;
   }//---------------------------------------------------------------------	
   public function fields()
   {
      $arguments=func_get_args();
      $this->fields=[];
      $this->values=[];
      if(count($arguments) == 1 && strtolower($arguments[0]) == "count")
      {
			$this->fields[]="COUNT(*)";
      }
      else if(count($arguments) == 1 && $arguments[0] != "*" && count(explode("=",$arguments[0])) > 1 && (count(explode("'",$arguments[0])) != 3 && count(explode('"',$arguments[0])) != 3))
      {
         $spl=explode(",",$arguments[0]);
         foreach($spl as $val)
         {
            $val=str_ireplace("`","",$val);				
            $valSpl=explode("=",$val);
            if(count($valSpl) > 1) $fieldName=$valSpl[0];
            if($fieldName != "*")
            {
               $fieldName="`" . $fieldName . "`";
               $this->values[]=$valSpl[1];
            }
            $this->fields[]=$fieldName;
         }
      }		
      else
      {
         foreach($arguments as $val)
         {
            $val=trim($val,"`");
            if($val!="*") $val="`" . $val . "`";
            $this->fields[]=$val;
         }
      }
      return $this;
   }//---------------------------------------------------------------------
   public function values()
   {
      $arguments=func_get_args();
      $this->values=[];
      if(count($arguments) == 1 && (count(explode("'",$arguments[0])) != 3 && count(explode('"',$arguments[0])) != 3))
      {
         $spl=explode(",",$arguments[0]);
         foreach($spl as $val)
         {
            $this->values[]=$val;
         }
      }
      else
      {
         foreach($arguments as $val)
         {
            $this->values[]=$val;
         }
      }
      return $this;
   }//---------------------------------------------------------------------
   public function where($strWhere)
   {
      $this->where=$strWhere;
      return $this;
   }//---------------------------------------------------------------------
   public function limit($strLimit)
   {
      $this->limit=$strLimit;
      return $this;
   }//---------------------------------------------------------------------
   public function orderBy($strOrderBy)
   {
      $this->orderBy=$strOrderBy;
      return $this;
   }//---------------------------------------------------------------------
   public function groupBy($strGroupBy)
   {
      $this->groupBy=$strGroupBy;
      return $this;
   }//---------------------------------------------------------------------
	//$retType= 'object','assoc'
   public function select($dbLink="",$retType="object")
   {
      if($dbLink!="") $dbl=$dbLink; else $dbl=$this->dbLink;
      $fields=implode(",",$this->fields);
		if($fields=='') $fields='*';
      if($this->where != "") $where="WHERE $this->where"; else $where="";
      if($this->limit != "") $limit="LIMIT $this->limit"; else $limit="";
      if($this->orderBy != "") $orderBy="ORDER BY $this->orderBy"; else $orderBy="";
      if($this->groupBy != "") $groupBy="GROUP BY $this->orderBy"; else $groupBy="";
      $this->query="SELECT {$fields} FROM {$this->table} {$where} {$orderBy} {$groupBy} {$limit}";
		$query=$dbl->query($this->query)or die ($this->err($dbLink,"SELECT {$fields} FROM {$this->table} {$where} {$orderBy} {$groupBy} {$limit}",$dbl->error));
		$ary=[];
      if($retType=="object" || $retType=="obj")
      {
         while($line=$query->fetch_object())
         {
            $ary[]=$line;
         }
      }
      else if($retType=="assoc")
      {
         while($line=$query->fetch_assoc())
         {
            $ary[]=$line;
         }
      }
		$this->reseter();
      return $ary;
   }//---------------------------------------------------------------------
	public function getQuery()
	{
		return $this->query;
	}//---------------------------------------------------------------------
   public function count($dbLink="")
   {
		$this->fields("count");
		$ret=$this->select($dbLink,"assoc");
		return $ret[0]['COUNT(*)'];
   }//---------------------------------------------------------------------	
   public function insert($dbLink="",$regLog=false,$retSelect=false,$selectBy="")
   {
      if($dbLink!="") $dbl=$dbLink; else $dbl=$this->dbLink;
      $fields=implode(",",$this->fields);
      $values=implode(",",$this->values);
      $fields = trim(preg_replace('/\s\s+/', '', $fields));
      if($this->where != "") $where="WHERE $this->where"; else $where="";
      //file_put_contents("test.txt","INSERT INTO {$this->table} ({$fields}) VALUES ({$values}) {$where}");

      $query=$dbl->query("INSERT INTO {$this->table} ({$fields}) VALUES ({$values}) {$where}")or die ($this->err($dbLink,"INSERT INTO {$this->table} ({$fields}) VALUES ({$values}) {$where}",$dbl->error));
      $this->reseter();
		//log file
      if($regLog==true)
      {
         session_start();
         $path=$_SESSION["logPath"] . "runTime_sql.log";
         if(!file_exists($path))
         {
            $f=@fopen($path);
            @fclose($f);
         }
         file_put_contents($path,$query . "[,]",FILE_APPEND);
      }
      //return
      if($retSelect==true)
      {
         $query_=$dbl->query("SELECT * FROM {$this->table} ORDER BY $selectBy DESC LIMIT 1");
         $arr=[];
         while($line=$query_->fetch_object())
         {
            $arr[]=$line;
         }
         return $arr[0];
      }
      else return $query;
   }//---------------------------------------------------------------------
   public function update($dbLink="",$regLog=false)
   {
      if($dbLink!="") $dbl=$dbLink; else $dbl=$this->dbLink;
      $str="";
      for($i=0;$i < count($this->fields);$i++)
      {
         $field = trim(preg_replace('/\s\s+/', '', $this->fields[$i]));
         $str.=$field . "=" . $this->values[$i] . ",";
      }
      $str=rtrim($str,",");
      if($this->where != "") $where="WHERE {$this->where}"; else $where="";

      if(count($this->fields) != count($this->values))
         die ($this->err($dbLink,"UPDATE {$this->table} SET {$str} {$where}","fields unequal values"));

      $query=$dbl->query("UPDATE {$this->table} SET {$str} {$where}") or die ($this->err($dbLink,"UPDATE {$this->table} SET {$str} {$where}",$dbl->error));
      $this->reseter();
		//log file
      if($regLog==true)
      {
         //session_start();
         $path=$_SESSION["logPath"] . "runTime_sql.log";
         if(!file_exists($path))
         {
            $f=@fopen($path);
            @fclose($f);
         }
         file_put_contents($path,$query . "[,]",FILE_APPEND);
      }
      return $query;
   }//---------------------------------------------------------------------
   public function delete($dbLink="",$regLog=false)
   {
      if($dbLink!="") $dbl=$dbLink; else $dbl=$this->dbLink;
      if($this->where != "") $where="WHERE $this->where"; else $where="";
      $query=$dbl->query("DELETE FROM {$this->table} {$where}") or die ($this->err($dbLink,"DELETE FROM {$this->table} {$where}",$dbl->error));
      $this->reseter();
		//log file
      if($regLog==true)
      {
         session_start();
         $path=$_SESSION["logPath"] . "runTime_sql.log";
         if(!file_exists($path))
         {
            $f=@fopen($path);
            @fclose($f);
         }
         file_put_contents($path,$query . "[,]",FILE_APPEND);
      }
      return $query;
   }//---------------------------------------------------------------------
	public function queryEx($queryString,$retFetch=false,$retType="object")
	{
      $dbl=$this->dbLink;
      $query=$dbl->query($queryString) or die ($this->err($dbl,queryString,$dbl->error));	
		if($retFetch==true)
		{
			$arr=[];
			if($retType=="object" || $retType=="obj")
			{
				while($line=$query->fetch_object())
				{
					$arr[]=$line;
				}
			}
			else if($retType=="assoc")
			{
				while($line=$query->fetch_assoc())
				{
					$arr[]=$line;
				}
			}
			return $arr;			
		}
		else
			return $query;
	}
}
?>

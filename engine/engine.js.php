<?php @session_start(); ?>
//config
var RESPONSE_COMPRESS="<?= RESPONSE_COMPRESS ?>";

var cEngine=function() {
   this.response=function(value) {
      if(RESPONSE_COMPRESS==true)
		   return oTools.unCompress(value);
		else
		   return value;      	   
	}//-------------------------------------------------------------------------
	this.request=function(objJson) 
	{
		return JSON.stringify(objJson);   
	}
}

var cPath=function() {
   this.root=function(filePath)
	{
	   return '<?= $oPath->root ?>' + '/' + filePath;
	}//------------------------------------------------------------------------- 
   this.rootDir=function(filePath)
	{
	   return '<?= $oPath->rootDir ?>' + '/' + filePath;
	}//------------------------------------------------------------------------- 	
   this.assetDir=function(filePath)
	{
	   return '<?= $oPath->assetDir ?>' + '/' + filePath;
	}//-------------------------------------------------------------------------  
   this.asset=function(filePath)
	{
	   return '<?= $oPath->asset ?>' + '/' + filePath;
	}//------------------------------------------------------------------------- 	   
   this.publicDir=function(filePath)
	{
	   return '<?= $oPath->publicDir ?>' + '/' + filePath;
	}//------------------------------------------------------------------------- 	
   this.public=function(filePath)
	{
	   return '<?= $oPath->public ?>' + '/' + filePath;
	}//------------------------------------------------------------------------- 
   this.manageDir=function(filePath)
	{
	   return '<?= $oPath->manageDir ?>' + '/' + filePath;
	}//------------------------------------------------------------------------- 
   this.manage=function(filePath)
	{
	   return '<?= $oPath->manage ?>' + '/' + filePath;
	}//------------------------------------------------------------------------- 
   this.engineDir=function(filePath)
	{
	   return '<?= $oPath->engineDir ?>' + '/' + filePath;
	}//------------------------------------------------------------------------- 
   this.engine=function(filePath)
	{
	   return '<?= $oPath->engine ?>' + '/' + filePath;
	}//------------------------------------------------------------------------- 
}

var cTools=function(){
	this.link=function (url,isDownload)
	{
		var a = document.createElement('A');
		a.href = url;
		if(isDownload==true) a.download = url.substr(url.lastIndexOf('/') + 1);
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);			
	}//-------------------------------------------------------------------------
	this.isMail=function(mail){
	   var filter=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	   return filter.test(mail);
	}//-------------------------------------------------------------------------
	this.isMailAddress=function(str) 
	{
		var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;      
		return str.match(pattern);    
	}//-------------------------------------------------------------------------
	this.isNumeric=function(n) 
	{
		return !isNaN(parseFloat(n)) && isFinite(n);  
	}//-------------------------------------------------------------------------	
   this.isFarsiChr=function(value,addAllowedChr,type)//check mykanad meghdare voroody faghat farsi bashad
   {
      var s=value.replace(/^\s+|\s+$/g,"");//trim;
      if(s.length>0)
      {
         var farsi=0;
         if(addAllowedChr==undefined) var Chr="";else Chr=addAllowedChr;
         if(type=="full") var regexp="[\u0600-\u06FF0-9" + Chr + "]"; //all keyboard FA
         else if(type=="only") var regexp="^[ابپتثجچحیخدذرزسشطظعغفقکگلمنوهيئضصءأإؤژيةآۀیي" + Chr + "]";
         else if(type=="standard" || type==undefined)  var regexp="^[0-9ابپتثجچحیخدذرزسشطظعغفقکگلمنوهيئضصءأإؤژيةآۀیي" + Chr + "]";
         var v = new RegExp(regexp);
         for(i=0;i<s.length;i++)
         {
            if (v.test(s.substr(i,1))) farsi++;
         }
         if (farsi < s.length) return(false); else if(farsi == s.length) return(true);
      }
      else if(s.length==0) return (false);
   }//--------------------------------------------------------------------------
   this.isLatinChr=function(value,addAllowedChr,type)//check mykanad meghdare voroody faghat latin va ya adad bashad
   {
      var s=value.replace(/^\s+|\s+$/g,"");//trim;
      if(s.length>0)
      {
         var latin=0;
         if(addAllowedChr==undefined) var Chr="";else Chr=addAllowedChr;
         if(type=="full") var regexp="^[A-Za-z0-9_@$&?, " + Chr + "]";//all keyboard EN
         if(type=="only") var regexp="^[A-Za-z" + Chr + "]";
         if(type=="standard" || type==undefined) var regexp="^[A-Za-z0-9" + Chr + "]";
         var v = new RegExp(regexp);
         for(i=0;i<s.length;i++)
         {
            if (v.test(s.substr(i,1))) latin++;
         }
         if (latin < s.length) return(false); else if(latin == s.length) return(true);
      }
      else if(s.length==0) return (false);
   }//--------------------------------------------------------------------------	
	this.unCompress=function (value)
	{
	   return JXG.Util.UTF8.decode(JXG.decompress(value));
	}//------------------------------------------------------------------------- 
	this.print=function(htmlCode,pathsStyle) //"style1.css,style2.css,...
	{
		var disp_setting="toolbar=no,location=no,directories=no,menubar=no,addressbar=no,resizable=no,copyhistory=no,";
		disp_setting+="scrollbars=yes,status=no,left=0,top=0";
		var content_value =htmlCode;
		var docprint=window.open("","",disp_setting);
		docprint.document.open();
		docprint.document.write("<html><head><title>print</title>");
		if(pathsStyle!=undefined)
		{
		     for(i=0;i < pathsStyle.split(",").length;i++) 
           {
		        docprint.document.write('<link rel="stylesheet" href="' + pathsStyle.split(",")[i] + '" type="text/css">\n');
		     }
       }
		 docprint.document.write('</head><body dir="rtl" onLoad="self.print()"><center>');
		docprint.document.write("<div id='layer' style='font-family:tahoma;font-size:12px;background-color:#fff;'>" + content_value + "</div>" );
		docprint.document.write('</center></body></html>');
		docprint.document.close();
	   docprint.focus();
	}//---------------------------------------------------------------------------
	this.copy=function(text) 
	{
		var textArea = document.createElement("textarea");
		textArea.value = text;

		// Avoid scrolling to bottom
		textArea.style.top = "0";
		textArea.style.left = "0";
		textArea.style.position = "fixed";

		document.body.appendChild(textArea);
		textArea.focus();
		textArea.select();

		try {
			var successful = document.execCommand('copy');
			var msg = successful ? 'successful' : 'unsuccessful';
			console.log('Fallback: Copying text command was ' + msg);
		} catch (err) {
			console.error('Fallback: Oops, unable to copy', err);
		}

		document.body.removeChild(textArea);
	}//---------------------------------------------------------------------------
	this.str_tagFitSend_encode=function(str)
	{
	   var str1=str;
	   for(var i=0;i < str.length;i++)
	   {
	      str1=str1.replace("<","_BT_");//beginTag
	      str1=str1.replace(">","_ET_");//endTag
	      str1=str1.replace('"',"_DS_");//doubleStr
	      str1=str1.replace("'","_SS_");//singleStr
	      str1=str1.replace("[","_BAI_");//brginAryIndex
	      str1=str1.replace("]","_EAI_");//endAryIndex
	      str1=str1.replace("{","_BC_");//beginComment1
	      str1=str1.replace("}","_EC_");//endComment1
	      str1=str1.replace("(","_BA_");//beginArg
	      str1=str1.replace(")","_EA_"); //endArg
	      str1=str1.replace("/","_BS_");//beginSlash
	      str1=str1.replace("\\","_BBS_");//beginBackSlash
	      str1=str1.replace("#","_NC_");//numberChr
	      str1=str1.replace("@","_MC_");//mailChr
	      str1=str1.replace("$","_DC_");//dollarChr
	      str1=str1.replace("&","_AC_");//andChr
	      str1=str1.replace(";","_ECL_");//endCodeLine
	      str1=str1.replace(",","_SA_");//splitArg
	      str1=str1.replace(":","_DD_");//doubleDot
	      str1=str1.replace(".","_DC2_");//dotChr
	      str1=str1.replace("=","_EC2_");//equalChr
	      str1=str1.replace("?=","_QC_");//questionChr
	      str1=str1.replace("?","_QC2_");//questionChr2
	   }
	   return str1;
	}//-----------------------------------------------------------------------------
	this.previewImgFile=function(fileElementId,imgElementId)
	{
		var preview = document.querySelector('#' + imgElementId);
		var file    = document.querySelector('#' + fileElementId).files[0];
		var reader  = new FileReader();

		reader.addEventListener("load", function () {
		 preview.src = reader.result;
		}, false);

		if (file) {
			reader.readAsDataURL(file);
		}
	}//-----------------------------------------------------------------------------	
}

var oEngine=new cEngine();
var oPath=new cPath();
var oTools=new cTools();



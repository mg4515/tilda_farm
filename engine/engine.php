<?php
   /*
   app name : myPhp.
   programmer : mohammad gholamy.
   web : www.office.alexo.ir
   copy right : 
   */
   @session_start();
   $rootMainPage="index.php";
   date_default_timezone_set("Asia/Tehran");
   putenv("Tz=Asia/Tehran");
	
	//include engine_config.php
	if(isset($_SESSION["engine_configRequire"]))
	   include $_SESSION["engine_configRequire"];
	else if(isset($_SESSION["engine.path.engineDir"]))
	   include $_SESSION["engine.path.engineDir"] . "/engine_config.php";		
	else
	{
	   include "engine/engine_config.php";
		if(!isset($CONFIG))
		{
		   //echo "<script>window.location.href='" . $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER["HTTP_HOST"] . "/" . $rootMainPage . "';</script>";
		   header("Location:" . $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER["HTTP_HOST"] . "/" . $rootMainPage);	
		}	
	} 
	   	
   if(PHP_ERROR==true)
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}
	else
	{
		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 0);
		error_reporting(0);		
	}

	//include engine_database.php
	if(isset($_SESSION["engine_dbRequire"]))
	   include $_SESSION["engine_dbRequire"];
	else if(isset($_SESSION["engine.path.engineDir"]))
	   include $_SESSION["engine.path.engineDir"] . "/engine_database.php";
	else
	   include "engine/engine_database.php";

   //include engine_tools.php
	if(isset($_SESSION["engine_toolsRequire"]))
	   include $_SESSION["engine_toolsRequire"];
	else if(isset($_SESSION["engine.path.engineDir"]))
	   include $_SESSION["engine.path.engineDir"] . "/engine_tools.php";
	else
	   include "engine/engine_tools.php";                           	
	
	// START cPath ///////////////////	
	class cPath
	{
      public $assetName="asset";
		public $publicName="public";
		public $manageName="manage";
			
      public $asset;
		public $public;
		public $publicDir;
		public $manage;
		
		public $engine;
		public $engineFile;
		public $engineFileDir;
		public $engineRequire;
		
		public $engine_dbFile;
		public $engine_dbFileDir;
		public $engine_dbRequire;
		
		public $engine_toolsFile;
		public $engine_toolsFileDir;
		public $engine_toolsRequire;

		public $engine_routesFile;
		public $engine_routesFileDir;
		public $engine_routesRequire;

		public $engine_jsFile;
		public $engine_jsFileDir;
		public $engine_jsRequire;
		
		public $engine_configFile;
		public $engine_configFileDir;
		public $engine_configRequire;
				
		public $root;
		public $rootDir;
		
		function path_current($fullPath=true,$typeFull="url")
		{
		   //$pageUrl=$_SERVER["REQUEST_URI"];
		   $pageUrl=$_SERVER["SCRIPT_NAME"];
			$spl=explode("/",$pageUrl);
			$spl[count($spl)-1]="";
			$rootPath=implode($spl,"/");
			$rootPath=trim($rootPath,"/");
			if($fullPath==true) 
			{
				if(!isset($_SERVER['REQUEST_SCHEME']) || $_SERVER['REQUEST_SCHEME']=="")
					$REQUEST_SCHEME="http";
				else
					$REQUEST_SCHEME=$_SERVER['REQUEST_SCHEME'];
			   if($typeFull=="url") $rootPath=$REQUEST_SCHEME . "://" . $_SERVER['HTTP_HOST'] . "/" . $rootPath;			
			   else if($typeFull=="dir") $rootPath=$_SERVER["DOCUMENT_ROOT"] . "/" . $rootPath;			
			}
			return $rootPath;	
		}
		function __construct()
		{
		   if(!isset($_SESSION["engine.path.rootDir"]))
		      $this->rootDir=$this->path_current(true,"dir");
			else
			   $this->rootDir=$_SESSION["engine.path.rootDir"];
			
			//if(!isset($_SESSION["engine.path.root"]))	   
		      $this->root=$this->path_current(true,"url");
			   $this->root=rtrim($this->root,"/");
			//else
			   //$this->root=$_SESSION["engine.path.root"];   	
	
		   $this->assetDir=$this->rootDir . "/" . $this->assetName;	
		   $this->asset=$this->root . "/" . $this->assetName;	
		   
			$this->publicDir=$this->rootDir . "/" . $this->publicName;	
		   $this->public=$this->root . "/" . $this->publicName;	
		   
			$this->manageDir=$this->rootDir . "/" . $this->manageName;	
			$this->manage=$this->root . "/" . $this->manageName;
			
			$this->engineDir=$this->rootDir . "/engine";		
		   $this->engine=$this->root . "/engine";	
		   $this->engineRequire=$this->rootDir . "/engine/engine.php";	
		   $this->engineFile=$this->root . "/engine/engine.php";	
		   $this->engineFileDir=$this->rootDir . "/engine/engine.php";	
			
		   $this->engine_dbRequire=$this->rootDir . "/engine/engine_database.php";	
		   $this->engine_dbFile=$this->rootDir . "/engine/engine_database.php";	
		   $this->engine_dbFileUrl=$this->root . "/engine/engine_database.php";	

		   $this->engine_toolsRequire=$this->rootDir . "/engine/engine_tools.php";	
		   $this->engine_toolsFile=$this->rootDir . "/engine/engine_tools.php";	
		   $this->engine_toolsFileUrl=$this->root . "/engine/engine_tools.php";

		   $this->engine_routesRequire=$this->rootDir . "/engine/engine_routes.php";	
		   $this->engine_routesFile=$this->rootDir . "/engine/engine_routes.php";	
		   $this->engine_routesFileUrl=$this->root . "/engine/engine_routes.php";

		   $this->engine_jsRequire=$this->rootDir . "/engine/engine.js.php";	
		   $this->engine_jsFile=$this->rootDir . "/engine/engine.js.php";	
		   $this->engine_jsFileUrl=$this->root . "/engine/engine.js.php";
			
		   $this->engine_configRequire=$this->rootDir . "/engine/engine_config.php";	
		   $this->engine_configFile=$this->rootDir . "/engine/engine_config.php";	
		   $this->engine_configFileUrl=$this->root . "/engine/engine_config.php";				
		}
		function rootDir($filePath="")
		{
			$retStr=rtrim($this->rootDir,"/");
			$retStr=rtrim($retStr . "/" . $filePath,"/");
			return $retStr;
		}	
		function root($filePath="")
		{
			$retStr=rtrim($this->root,"/");
			$retStr=rtrim($retStr . "/" . $filePath,"/");
			return $retStr;
		}
		
		function assetDir($filePath=""){return ($this->assetDir . "/" . $filePath);}	
		function asset($filePath=""){return ($this->asset . "/" . $filePath);}
		
		function publicDir($filePath=""){return ($this->publicDir . "/" . $filePath);}
		function public_($filePath=""){return ($this->public . "/" . $filePath);}
		
		function manageDir($filePath=""){return ($this->manageDir . "/" . $filePath);}
		function manage($filePath=""){return ($this->manage . "/" . $filePath);	}		
		
		function engineDir($filePath=""){return ($this->engineDir . "/" . $filePath);	}
		function engine($filePath=""){return ($this->engine . "/" . $filePath);}
		
		function current($filePath=""){return ($this->path_current(true,"url") . "/" . $filePath);	}
		function currentDir($filePath=""){return ($this->path_current(true,"dir") . "/" . $filePath);}
	}//----------------------------------------------------------------------------------------------------------------------------
	
	// START cRoute ///////////////////
	class cRoute
	{
		private $result=false;
		var $urlHistory=[];
		function url_($regex, $function,$count="") {
		   $regex=rtrim($regex,"/");
			$itemsStr = str_replace('/', '_|_', $regex);
			$serverUri=$_SERVER['REQUEST_URI'];
			//count filter
			print_r($_REQUEST);
			echo "<hr>";
			
			//is match?
		   $regex_ = str_replace('/', '\/', $regex);
		   $is_match = preg_match('/' . ($regex_) . '$/', rtrim($_SERVER['REQUEST_URI'],"/"), $matches, PREG_OFFSET_CAPTURE);
			if ($is_match) 
			{
			   $items=explode("_|_",$itemsStr); 
			   $function($items); 
			}
		}//-----------------------------------------------------------------------
		function url($url, $function) 
		{
         if(!in_array($url,$this->urlHistory))
			{
				$query=@$_REQUEST["query"];	
            $this->urlHistory[]=$url;				
				if($url=="!") 
				{
					if($_SERVER["REQUEST_URI"]!=$_SERVER["SCRIPT_NAME"] && $_SERVER["REQUEST_URI"]!=$_SERVER["PHP_SELF"] && @$_SERVER["HTTP_X_REQUESTED_WITH"]!='XMLHttpRequest')
					{
						$items="";
						$this->result=true;
						$function($items);
					}
				}
				else if($url=="./" && !isset($_REQUEST["query"]) && $_SERVER["REQUEST_URI"]!=$_SERVER["SCRIPT_NAME"] && $_SERVER["REQUEST_URI"]!=$_SERVER["PHP_SELF"] && !$_SERVER["QUERY_STRING"] && !isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && @$_SERVER["HTTP_X_REQUESTED_WITH"]!='XMLHttpRequest') 
				{
					$url=rtrim($url,"/");
					$query=@$_REQUEST["query"];	
					$items="";
					$function($items);
					exit;
				}
				else
				{
					$url=rtrim($url,"/");
					if($query==$url && strlen($query)>0 && (!isset($_SERVER["HTTP_X_REQUESTED_WITH"]) || @$_SERVER["HTTP_X_REQUESTED_WITH"]!='XMLHttpRequest'))
					{
						$items=explode("/",$_REQUEST["query"]);	
						unset($_REQUEST["query"]);
						$request=$_REQUEST;
						$this->result=true;					
						$function([$items,$request]); 
						exit;
											
					}
				}				
			}
		}//-----------------------------------------------------------------------		
		function urlQ($url, $function) 
		{
			$_SESSION["route_urlCurrent"]=$url;	
			$url=rtrim($url,"/");
			if(isset($_REQUEST["query"]) && @$_REQUEST["query"]==$url)
			{
				$this->result=true;				
				$function(); 
			}	
		}//-----------------------------------------------------------------------
		function urlElse($function) 
		{	
         if($this->result==false && $_SERVER["REQUEST_URI"]!=$_SERVER["SCRIPT_NAME"] && $_SERVER["REQUEST_URI"]!=$_SERVER["PHP_SELF"])
			{			
				$query=@$_REQUEST["query"];	
				$items=explode("/",$query);	
				unset($_REQUEST["query"]);
				$request=$_REQUEST;
            $this->result=true;				
				$function([$items,$request]); 
				//exit;	
			}			
		}//-----------------------------------------------------------------------		

	}//--------------------------------------------------------------------------------------------------------------------------
	
	class cEngine 
	{
		public $path;
		public $db;
		public $dbQuery;
		public $tools;
		public $route;
		function __construct()
		{
		   @session_start();
         
			//objects
		   $this->path=new cPath();
			$_SESSION["engine.path"]= $this->path;	
			if(DATABASE_POWER==true)
			{
			   $this->db=new cDatabase();
		      $this->dbQuery=new cDatabaseQuery($this->db);
				$_SESSION["engine.db"]= $this->db;	
			   $_SESSION["engine.dbQuery"]= $this->dbQuery;	
			}
         $this->tools=new cTools();
         $this->route=new cRoute();
						
			//paths by section
			if(isset($_SESSION["engine.path.root"])) $this->path->root=$_SESSION["engine.path.root"];
			if(isset($_SESSION["engine.path.rootDir"])) $this->path->rootDir=$_SESSION["engine.path.rootDir"];
			
			if(isset($_SESSION["engine.path.assetDir"])) $this->path->assetDir=$_SESSION["engine.path.assetDir"];
			if(isset($_SESSION["engine.path.asset"])) $this->path->asset=$_SESSION["engine.path.asset"];
			if(isset($_SESSION["engine.path.publicDir"])) $this->path->publicDir=$_SESSION["engine.path.publicDir"];
			if(isset($_SESSION["engine.path.public"])) $this->path->public=$_SESSION["engine.path.public"];
			if(isset($_SESSION["engine.path.manageDir"])) $this->path->manageDir=$_SESSION["engine.path.manageDir"];
			if(isset($_SESSION["engine.path.manage"])) $this->path->manage=$_SESSION["engine.path.manage"];
			if(isset($_SESSION["engine.path.engineDir"])) $this->path->engineDir=$_SESSION["engine.path.engineDir"];
			if(isset($_SESSION["engine.path.engine"])) $this->path->engine=$_SESSION["engine.path.engine"];
			
			if(isset($_SESSION["engineRequire"])) $this->path->engineRequire=$_SESSION["engineRequire"];
			if(isset($_SESSION["engineFile"])) $this->path->engineFile=$_SESSION["engineFile"];
			if(isset($_SESSION["engineFileDir"])) $this->path->engineFileDir=$_SESSION["engineFileDir"];
			if(isset($_SESSION["engine.path"])) $this->path=$_SESSION["engine.path"];
		   
			if(isset($_SESSION["engine_dbRequire"])) $this->path->engine_dbRequire=$_SESSION["engine_dbRequire"];
			if(isset($_SESSION["engine_dbFile"])) $this->path->engine_dbFile=$_SESSION["engine_dbFile"];
			if(isset($_SESSION["engine_dbFileDir"])) $this->path->engine_dbFileDir=$_SESSION["engine_dbFileDir"];
			if(isset($_SESSION["engine.db"])) $this->path=$_SESSION["engine.path"];			

			if(isset($_SESSION["engine_configRequire"])) $this->path->engine_configRequire=$_SESSION["engine_configRequire"];
			if(isset($_SESSION["engine_configFile"])) $this->path->engine_configFile=$_SESSION["engine_configFile"];
			if(isset($_SESSION["engine_configFileDir"])) $this->path->engine_configFileDir=$_SESSION["engine_configFileDir"];		
		
		   //database
			/*if(isset($_SESSION["engine.config.db_host"])) $this->db->host=$_SESSION["engine.config.db_host"];
			if(isset($_SESSION["engine.config.db_userName"])) $this->db->userName=$_SESSION["engine.config.db_userName"];
			if(isset($_SESSION["engine.config.db_password"])) $this->db->password=$_SESSION["engine.config.db_password"];
			if(isset($_SESSION["engine.config.db_databaseName"])) $this->db->databaseName=$_SESSION["engine.config.db_databaseName"];*/
		}//--------------------------------------------------------------
		function initOnce()
		{
		   @session_start();
			$_SESSION["engine.path.root"]=$this->path->root;	
			$_SESSION["engine.path.rootDir"]=$this->path->rootDir;	
					
			$_SESSION["engine.path.assetDir"]=$this->path->assetDir;			
			$_SESSION["engine.path.asset"]=$this->path->asset;			
			$_SESSION["engine.path.public"]=$this->path->publicDir;			
			$_SESSION["engine.path.public"]=$this->path->public;			
			$_SESSION["engine.path.manageDir"]=$this->path->manageDir;			
			$_SESSION["engine.path.manage"]=$this->path->manage;			
			$_SESSION["engine.path.engineDir"]=$this->path->engineDir;			
			$_SESSION["engine.path.engine"]=$this->path->engine;
						
			$_SESSION["engineRequire"]=$this->path->engineRequire;			
			$_SESSION["engineFile"]=$this->path->engineFile;			
			$_SESSION["engineFileDir"]=$this->path->engineFileDir;

			$_SESSION["engine_dbRequire"]=$this->path->engine_dbRequire;			
			$_SESSION["engine_dbFile"]=$this->path->engine_dbFile;			
			$_SESSION["engine_dbFileUrl"]=$this->path->engine_dbFileUrl;	
			
			$_SESSION["engine_configRequire"]=$this->path->engine_configRequire;			
			$_SESSION["engine_configFile"]=$this->path->engine_configFile;			
			$_SESSION["engine_configFileUrl"]=$this->path->engine_configFileUrl;

			
		}//--------------------------------------------------------------
		static function response($data="",$compress=true,$convertType='text')
		{
			if($convertType=='json') $data=json_encode($data);
		   if(@RESPONSE_COMPRESS==true && $compress==true)
			{ 
            $oTools=new cTools();
		      echo $oTools->str_compress($data);			
			}
			else
				echo $data;
		}//--------------------------------------------------------------
		static function view($fileName)
		{
		   $fileName=str_replace(".","/",$fileName);
			$oPath=$this->path;
		   return file_get_contents($oPath->publicDir($fileName . ".php"));	
		}//--------------------------------------------------------------
      static function echoBefor($contents)
      {
         @$_SESSION["engine_echoBefor"].=$contents;
		}//--------------------------------------------------------------
      static function echoAfter($contents)
      {
         @$_SESSION["engine_echoAfter"].=$contents;
		}//--------------------------------------------------------------		
	}
	
 	//global function
	function error($errorName,$errorComment,$index=0)
	{
	   $err=debug_backtrace();
		if(count($err) > 0 && isset($err[$index]))
		{
			echo "
			   <div style='border-radius:5px;padding:10px;background-color:#eee;' >
				   <h2 style='color:red'>error : {$errorName}.</h2> 
				   <p>{$errorComment}.</p> 
					<ul>
					   <li><b>file : </b>{$err[$index]['file']}</li>
					   <li><b>line : </b>{$err[$index]['line']}</li>
					</ul>  
				</div>
			";
		}	
	}//-------------------------------------------------------------------------------------------
	function view($fileName)
	{
	   global $oPath;
		$fileType=explode(".",$fileName);
		if($fileType[count($fileType)-1]!="php")
		{
		   if(file_exists($oPath->publicDir($fileName . ".php")))
			{
		      include_once($oPath->publicDir($fileName . ".php"));
			}				
			else
			   die(error("view","not found " . $oPath->publicDir($fileName . ".php"),1));	
		} 	
		else
		{
		   if(file_exists($oPath->publicDir($fileName)))
		      include_once($oPath->publicDir($fileName));	
			else
			   die(error("view","not found " . $oPath->publicDir($fileName),1));			
		}
	}//-------------------------------------------------------------------------------------------
   function error_()
   {
      $errType=array(
         E_ERROR=>'ERROR',
         E_WARNING=>'WARNING',
         E_PARSE=>'PARSE',
         E_NOTICE=>'NOTICE',
         E_CORE_ERROR=>'CORE_ERROR',
         E_CORE_WARNING=>'CORE_WARNING',
         E_COMPILE_ERROR=>'COMPILE_ERROR',
         E_COMPILE_WARNING=>'COMPILE_WARNING',
         E_USER_ERROR=>'USER_ERROR',
         E_USER_WARNING=>'USER_WARNING',
         E_USER_NOTICE=>'USER_NOTICE',
         E_STRICT=>'STRICT',
         E_RECOVERABLE_ERROR=>'RECOVERABLE_ERROR',
         E_DEPRECATED=>'DEPRECATED',
         E_USER_DEPRECATED=>'USER_DEPRECATED'
      );
      $errCode="";
      $debugCode="";
      $debugInfo=debug_backtrace();
      $err=error_get_last();
      for($i=0;$i < count($debugInfo);$i++)
      {
			$file=$debugInfo[$i]["file"];
			$class=$debugInfo[$i]["class"];
			$object=$debugInfo[$i]["object"];
			$function=$debugInfo[$i]["function"];
			$line=$debugInfo[$i]["line"];
			if($debugInfo[$i]["type"]== "->") $type="method call";
			else if($debugInfo[$i]["type"]== "::") $type="static method call";
			else $type="function call";
			$debugCode.=
						"path : " . $file . "<br>\n" .
						"class : " . $class . "<br>\n" .
						"function : " . $function . "<br>\n" .
						"type : " . $type . "<br>\n" .
						"line : " . $line . "<br>\n" .
						"<hr>\n\n";

      }
      $type=@$errType[$err["type"]];
      $errCode="ERROR {$type} : <br>\n" .
              "message : " . $err["message"] . "<br>\n" . $msg . "<br>\n";
              if($query!="") $errCode.="query : " . $query . "<br>\n";
              $errCode.="path : " . $err["file"] . "<br>\n" .
              "line : " . $err["line"] . "<br><br><br>\n\n\n" .
              "The codes found : <br><br>\n\n" .
              $debugCode;
      return @$errCode;
   }//---------------------------------------------------------------------
	
	//global object
	$oEngine=new cEngine();	
	$oPath=$oEngine->path;
	$oTools=$oEngine->tools;
	$oRoute=$oEngine->route;
	if(DATABASE_POWER==true)
	{            
		$oDb=$oEngine->db;
		$oDbq=$oEngine->dbQuery;	
	}

   //init
   if(!isset($_SESSION["engineRequire"]))
	{
	   $oEngine->initOnce();	
	}
	
	//echo (befor) contents
	echo @$_SESSION["engine_echoBefor"];
	@$_SESSION["engine_echoBefor"]="";
	
	//include engine_route.php
	if(isset($_SESSION["engine_routesRequire"]))
		include_once $_SESSION["engine_routesRequire"];
	else if(isset($_SESSION["engine.path.engineDir"]))
		include_once $_SESSION["engine.path.engineDir"] . "/engine_route.php";
	else
		include_once "engine/engine_route.php";
	
	//echo (after) contents
	echo @$_SESSION["engine_echoAfter"];
	@$_SESSION["engine_echoAfter"]="";	
?>

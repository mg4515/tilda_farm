<?php
class cTools
{
   //file system
   static function fs_fileCount($dirPath) //Bdest avardan tedad kol file mojood dar yek foldere moshakhas
   {
      $i=0;
      // Open the folder
      $dir_handle = @opendir($dirPath) or die("Unable to open $dirPath");
      // Loop through the files
      while ($file = readdir($dir_handle))
      {
         if($file == "." || $file == "..")
            continue;
         if(is_file($dirPath . $file))
         {
            $i++;
         }
      }
      closedir($dir_handle);
      return $i;
   }//-----------------------------------------------------------------------		
   static function fs_fileSize($path,$formatSize=false,$remote=false)
   {
		if(!$remote)
         $fs=filesize($path);
      else
		{
			$useHead=true;
			$ch = curl_init($path);
			curl_setopt_array($ch, array(
				CURLOPT_RETURNTRANSFER  => 1,
				CURLOPT_FOLLOWLOCATION  => 1,
				CURLOPT_SSL_VERIFYPEER  => 0,
				CURLOPT_NOBODY          => 1,
			));
			if (false !== $useHead) {
			  curl_setopt($ch, CURLOPT_NOBODY, 1);
			}
			curl_exec($ch);
			// content-length of download (in bytes), read from Content-Length: field
			$clen = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
			curl_close($ch);
			$fs = $clen;
		}
		
      if($formatSize==true)
      {
         if($fs < 1024)
            return $fs . " B";
         if($fs > 1024 && $fs < 1048576)
            return round($fs/1024,2) . " KB";
         if($fs > 1048576 && $fs < 1073741824)
            return round($fs/1024/1024,2) . " MB";
         if($fs > 1073741824 && $fs < 1099511627776)
            return round($fs/1024/1024/1024,2) . " GB";
      }
      else
         return $fs;
   }//---------------------------------------------------------------------------------
   function fs_dirRemove($path)//yek folder ra be hamrahe hameye file ha va folderhayash hazf mykonad
   {
      if (is_dir($path))
      {
         $objects = scandir($path);
         foreach ($objects as $object)
         {
            if ($object != '.' && $object != '..')
            {
               if (filetype($path.'/'.$object) == 'dir')
               {
                  $this->fs_dirRemove($path.'/'.$object);
               }
               else
               {
                  unlink($path.'/'.$object);
               }
            }
         }
         reset($objects);
         rmdir($path);
      }
      else
         unlink($path);
   }//-----------------------------------------------------------------------
   static function fs_dirMake($path,$mode=0777)
   {
      $oldumask = umask(0);
      mkdir($path,0777);
      chmod($path,0777);
      umask($oldumask);
   }//-----------------------------------------------------------------------
   function fs_dirCopy($source, $destination)//yek folder ra dar maghsade morede nazar copy mykonad be hamrahe tamame file ha va folderhayash
   {
      // Simple copy for a file
      //$source=rtrim($source,"/");
      //$source=rtrim($destination,"/");
      if (is_file($source))
      {
         $c = copy($source, $destination);
         chmod($destination, 0777);
         return $c;
      }
      // Make destination directory
      if (!is_dir($destination))
      {
         $oldumask = umask(0);
         mkdir($destination, 0777);
         umask($oldumask);
      }
      // Loop through the folder
      $dir = dir($source);
      while (false !== $entry = $dir->read())
      {
         // Skip pointers
         if ($entry == "." || $entry == "..")
         {
            continue;
         }
         // Deep copy directories
         if ($destination !== "$source/$entry")
         {
            $this->fs_dirCopy("$source/$entry", "$destination/$entry");
         }
      }
      // Clean up
      $dir->close();
      return true;
   }//-----------------------------------------------------------------------
   static function fs_getFileType($path)//noe yek file ra be soorate daghightar barmygardanad
   {
      $spl=explode(".",$path);
      return $spl[count($spl) - 1];
   }//-----------------------------------------------------------------------
   static function fs_getFileName($path,$processingString=false)//name yek file ya folder ra barmygardanad
   {
      $path=rtrim($path,"/");
      $fName="";
      if($processingString==false)
      {
         if(is_file($path))
         {
            $path=explode("/",$path);
            $fileName=$path[count($path)-1];
            $fileName=explode(".",$fileName);
            for($i=0;$i < count($fileName)-1;$i++)
            {
               $fName.=$fileName[$i] . ".";
            }
            return rtrim($fName,".");
         }
         else if(is_dir($path))
         {
            $path=explode("/",$path);
            $fileName=$path[count($path)-1];
            return $fileName;
         }
      }
      else
      {
         $path=explode("/",$path);
         $fileName=$path[count($path)-1];
         $fileName=explode(".",$fileName);
         if(count($fileName) > 1)
         {
            for($i=0;$i < count($fileName)-1;$i++)
            {
               $fName.=$fileName[$i] . ".";
            }
         }
         else $fName=$path[count($path)-1];
         return rtrim($fName,".");
         //return rtrim(join(".",$fileName),".");
      }
   }//-----------------------------------------------------------------------
   static function fs_getPatern($path)//name yek file ra be soorate daghightar barmygardanad
   {
      $path=rtrim($path,"/");
      $path=explode("/",$path);
      $path[count($path)-1]="";
      return rtrim(join("/",$path),"/");
   }//-----------------------------------------------------------------------
   static function fs_getBaseUrl($full=false) //rishe yek address ra barnigardanad
   {
      $site=sprintf("%s://%s%s",
                     isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
                     $_SERVER['SERVER_NAME'],
                     $_SERVER['REQUEST_URI']);
      if($full==false)
      {
         $site=explode("/",$site);
         $site="{$site[0]}//{$site[2]}";
      }
      return $site;
   }//--------------------------------------------------------------------------
   function fs_fileIsImage($path)//if file az noe image bashad true ra barmygardanad
   {
      $f=strtolower($this->fs_getFileType($path));
      if($f=="gif" || $f=="jpg" || $f=="jpeg" || $f=="png")
         return(true);
      else
         return(false);
   }//-----------------------------------------------------------------------
   function fs_rename($path,$newName)//taghire name yek file
   {
      return rename(rtrim($path,"/"),$this->fs_getPatern($path)."/".$newName);
   }//-----------------------------------------------------------------------
	static function fs_createDownload($filePath,$newFileName='',$contentType='multipart/form-data') {
	
	   $fileType=explode('.',$filePath);
		$fileType=$fileType[count($fileType)-1];
		//-----
		if($newFileName=='') $newFileName=time(); 
		//-----
		$newFileName=$newFileName . '.' . $fileType;
		//-----
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.$newFileName);
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filePath));
		header("Content-Type: {$contentType};");
		//-----
		ob_clean();
		flush();
		readfile($filePath);
		exit;	
	}//-----------------------------------------------------------------------  
	function fs_dirToZip($sourcefolder,$zipFileName="")
   {
      $sourcefolder=rtrim($sourcefolder,'/');
      if($zipFileName=="")
      {
         $zipFileName=$this->fs_getFileName($sourcefolder);
         if($zipFileName=="") $zipFileName=$sourcefolder;
         $zipFileName.=".zip";
      }
      $ptern=$this->fs_getPatern($sourcefolder);
      if($ptern != "") $newFileZip="{$ptern}/{$zipFileName}"; else $newFileZip="{$zipFileName}";
      //echo $zipFileName . "<br><br>";
      // Config Vars
      $timeout      = 5000           ; // Default: 5000

      // instantate an iterator (before creating the zip archive, just
      // in case the zip file is created inside the source folder)
      // and traverse the directory to get the file list.
      $dirlist = new RecursiveDirectoryIterator($sourcefolder);
      $filelist = new RecursiveIteratorIterator($dirlist);

      // set script timeout value
      ini_set('max_execution_time', $timeout);

      // instantate object
      $zip = new ZipArchive();

      // create and open the archive
      if ($zip->open("{$newFileZip}", ZipArchive::CREATE) !== TRUE) {
          die ("Could not open archive");
      }

      // add each file in the file list to the archive
      foreach ($filelist as $key=>$value) {
         $fName=$this->fs_getFileName($key,true) . "." . $this->fs_getFileType($key);
         $zip->addFile(realpath($key), $fName) or die ("ERROR: Could not add file: $key");
         //echo "<br>-".$fName."<br>";
      }

      // close the archive
      $zip->close();
      //echo "Archive ". $newFileZip . " created successfully.";
      // And provide download link <a href="http:<.php echo $newFileZip;.>" target="_blank"> Download <.php echo $zipfilename .></a>
      return "{$newFileZip}";
   }//-----------------------------------------------------------------------

   //string
   static function str_compress($str,$useBase64=true)
   {
      if($useBase64==true)
         return base64_encode(gzcompress($str,9));
      else
         return gzcompress($str,9);
   }//-----------------------------------------------------------------------
   static function str_unCompress($str,$useBase64=true)
   {
      if($useBase64==true)
         return gzuncompress(base64_decode($str),9);
      else
         return gzuncompress($str,9);
   }//-----------------------------------------------------------------------
   static function str_tagFitSend_encode($str)
   {
      $str1=$str;
      for($i=0;$i < strlen($str);$i++)
      {
        $str1=str_replace("<","_BT_",$str1);//beginTag
        $str1=str_replace(">","_ET_",$str1);//endTag
        $str1=str_replace('"',"_DS_",$str1);//doubleStr
        $str1=str_replace("'","_SS_",$str1);//singleStr
        $str1=str_replace("[","_BAI_",$str1);//brginAryIndex
        $str1=str_replace("]","_EAI_",$str1);//endAryIndex
        $str1=str_replace("{","_BC_",$str1);//beginComment1
        $str1=str_replace("}","_EC_",$str1);//endComment1
        $str1=str_replace("(","_BA_",$str1);//beginArg
        $str1=str_replace(")","_EA_",$str1); //endArg
        $str1=str_replace("/","_BS_",$str1);//beginSlash
        $str1=str_replace("\\","_BBS_",$str1);//beginBackSlash
        $str1=str_replace("#","_NC_",$str1);//numberChr
        $str1=str_replace("@","_MC_",$str1);//mailChr
        $str1=str_replace("$","_DC_",$str1);//dollarChr
        $str1=str_replace("&","_AC_",$str1);//andChr
        $str1=str_replace(";","_ECL_",$str1);//endCodeLine
        $str1=str_replace(",","_SA_",$str1);//splitArg
        $str1=str_replace(":","_DD_",$str1);//doubleDot
        $str1=str_replace(".","_DC2_",$str1);//dotChr
        $str1=str_replace("=","_EC2_",$str1);//equalChr
        $str1=str_replace("?=","_QC_",$str1);//questionChr
        $str1=str_replace("?","_QC2_",$str1);//questionChr2
      }
      return $str1;
   }//--------------------------------------------------------------------
   static function str_tagFitSend_decode($str)
   {
      for($i=0;$i < strlen($str);$i++)
      {
        $str=str_replace("_BT_","<",$str);
        $str=str_replace("_ET_",">",$str);
        $str=str_replace("_DS_",'"',$str);
        $str=str_replace("_SS_","'",$str);
        $str=str_replace("_BAI_","[",$str);
        $str=str_replace("_EAI_","]",$str);
        $str=str_replace("_BC_","{",$str);
        $str=str_replace("_EC_","}",$str);
        $str=str_replace("_BA_","(",$str);
        $str=str_replace("_EA_",")",$str);
        $str=str_replace("_BS_","/",$str);
        $str=str_replace("_BBS_","\\",$str);
        $str=str_replace("_NC_","#",$str);
        $str=str_replace("_MC_","@",$str);
        $str=str_replace("_DC_","$",$str);
        $str=str_replace("_AC_","&",$str);
        $str=str_replace("_ECL_",";",$str);
        $str=str_replace("_SA_",",",$str);
        $str=str_replace("_DD_",":",$str);
        $str=str_replace("_DC2_",".",$str);
        $str=str_replace("_EC2_","=",$str);
        $str=str_replace("_QC_","?=",$str);
        $str=str_replace("_QC2_","?",$str);
      }
      return $str;
   }//-----------------------------------------------------------------------
   static function str_perIntToEnInt($string)
   {
       $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'); ;
       $num = range(0, 9);
       return str_replace($persian, $num, $string);
   }//--------------------------------------------------------------------------
   static function str_faIntToEnInt($string)
   {
       $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'); ;
       $num = range(0, 9);
       return str_replace($persian, $num, $string);
   }//--------------------------------------------------------------------------
   static function str_goodPersian($string)
   {
      $string=str_ireplace("ك","ک",$string);
      $string=str_ireplace("ي","ی",$string);
      return $string;
   }//--------------------------------------------------------------------------
   
	//misc
	
	/*
	return : [
		countPages,
		sqlLimit,
		pagesStart,
		pagesEnd,
		isPrevious,
		isNext	
	]
	*/
	static function misc_sqlLimit($countTotalItems,$countItems,$countPages,$currentPage)
	{
	   if($countTotalItems <=  $countItems)
		{			
			$countPagesAll=1; //tedade page haye kol
		}
		else
			$countPagesAll=$countTotalItems / $countItems; //tedade page haye kol
		//if($countPagesAll == $countPages) $countPagesAll+=1;
		
		if($countPagesAll < $countPages) $countPages = $countPagesAll;
		if($currentPage > $countPagesAll ) $currentPage=$countPagesAll;
		
		$limitStart=(($currentPage * $countItems) - $countItems) + 1;
		$limitCount=$countItems;
		$btnPageNumber=$currentPage * $countPages;// 1[btn,btn,btn] , 2[btn,{btn},btn] , 3[btn,btn,btn]. javab 2 mishavad.
		
		if($currentPage <= $countPages/2 || $countPagesAll <= $countPages)
		{
		   $btnStart=1;	
		   $btnEnd=$countPages;
			if($btnEnd > $countPagesAll) $btnEnd=$countPagesAll;
		}
		else
		{
		   $btnStart=$currentPage - ($countPages/2) + 1;	
		   $btnEnd=$countPages + ($currentPage - ($countPages/2));
			if($btnEnd > $countPagesAll) $btnEnd=$countPagesAll;		   	
		}
		
		if($btnStart > 1 && $countPagesAll > $countPages) $isPrevious=true; else $isPrevious=false;
		if($btnEnd < $countPagesAll && $countPagesAll > $countPages)   $isNext=true; else $isNext=false;
		return array(
		   "countPages"=>intVal(round($countPagesAll)),
			"sqlLimit"=>($limitStart-1) . ',' . $limitCount,
		   "pagesStart"=>round($btnStart),
		   "pagesEnd"=>round($btnEnd),
		   "isPrevious"=>$isPrevious,
		   "isNext"=>$isNext
		);
	}//--------------------------------------------------------------------
   function misc_getLoopPaging($countPages,$limitShowPage,$selectPageNumber)
   {
      //return start and end items in a page
      $ret=[];
      if($selectPageNumber > $limitShowPage/2)
      {
         $ret["start"]=$selectPageNumber - (($limitShowPage/2)-1);
         $ret["end"]=$ret["start"] + $limitShowPage;
         if($countPages < $ret["end"])
         {
            $step1=($countPages + 1) - $selectPageNumber;
            $step2=$limitShowPage - $step1;
            $step3=$selectPageNumber - $step2;
            $ret["start"]=$step3;
            $ret["end"]=$countPages + 1;
         }
      }
      else
      {
         if($countPages < $limitShowPage)$limitShowPage=$countPages;
         $ret["start"]=1;
         $ret["end"]=$limitShowPage + 1;
      }
      return $ret;
   }//-----------------------------------------------------------------------
   function misc_getLimitPage($itemsCount,$itemsInPage=20,$pageNumber=1)
   {
      //get sql query limit(limitStart,limitEnd) and return page count
      $myPage=$pageNumber;
      $childeCount=$itemsInPage;//tedade item dar har safhe
      $limitPage=($myPage - 1) * $childeCount; //start limit ra moshakhas mikonim.
      $pageCount=$itemsCount / $childeCount; //tedade safehat

      $ret["limitStart"]=$limitPage;
      $ret["limitEnd"]=$childeCount;
      $ret["pageCount"]=$pageCount;
      return $ret;
   }//--------------------------------------------------------------------------
   static function misc_createId($count="full",$retCrypt=false)
   {
      $usec=gettimeofday();$usec=$usec["usec"];
		$idNumber=rand(5000,12000);
      $idNumber=time() + $idNumber;
		if($retCrypt==true) $idNumber=md5($idNumber);
      if($count!="full") $idNumber=substr($idNumber,0,$count);
      return($idNumber);
   }//-----------------------------------------------------------------------
   static function misc_hex2rgb($hex)
   {
      $hex = str_replace("#", "", $hex);

      if(strlen($hex) == 3) {
         $r = hexdec(substr($hex,0,1).substr($hex,0,1));
         $g = hexdec(substr($hex,1,1).substr($hex,1,1));
         $b = hexdec(substr($hex,2,1).substr($hex,2,1));
      }
      else
      {
         $r = hexdec(substr($hex,0,2));
         $g = hexdec(substr($hex,2,2));
         $b = hexdec(substr($hex,4,2));
      }
      $rgb = array($r, $g, $b);
      //return implode(",", $rgb); // returns the rgb values separated by commas
      return $rgb; // returns an array with the rgb values
   }//---------------------------------------------------------------------------------
   static function misc_rgb2hex($r, $g, $b)
   {
      $r = dechex($r);
      if (strlen($r)<2)
      $r = '0'.$r;

      $g = dechex($g);
      if (strlen($g)<2)
      $g = '0'.$g;

      $b = dechex($b);
      if (strlen($b)<2)
      $b = '0'.$b;

      return $r . $g . $b;
   }//---------------------------------------------------------------------
   static function misc_colorBalance($value,$minValue,$maxValue,$arryMinRgb,$arryMaxRgb) //BEDAST AVARDAN RGB YEK MEGHDAR MABEINE MIN VA MAX
   {
      $normalize_base = 2*255;
      $normalized = @(($value - $minValue) / ($maxValue - $minValue)) ;
      $normalize = floor($normalized * $normalize_base);

      $minChange=round($normalize / 2);
      $maxChange=255-$minChange;

      $minR=$arryMinRgb[0] - $minChange;
      $minG=$arryMinRgb[1] - $minChange;
      $minB=$arryMinRgb[2] - $minChange;

      if($minR < 0) $minR=0;
      if($minG < 0) $minG=0;
      if($minB < 0) $minB=0;

      if($minR > 255) $minR=255;
      if($minG > 255) $minG=255;
      if($minB > 255) $minB=255;


      $maxR=$arryMaxRgb[0] - $maxChange;
      $maxG=$arryMaxRgb[1] - $maxChange;
      $maxB=$arryMaxRgb[2] - $maxChange;

      if($maxR < 0) $maxR=0;
      if($maxG < 0) $maxG=0;
      if($maxB < 0) $maxB=0;

      if($maxR > 255) $maxR=255;
      if($maxG > 255) $maxG=255;
      if($maxB > 255) $maxB=255;

      $maxColor_=rgb2hex($maxR,$maxG,$maxB);
      $minColor_=rgb2hex($minR,$minG,$minB);
      $bgColor = dechex(hexdec($maxColor_) + hexdec($minColor_));
      $fgColor = dechex(hexdec($maxColor_) - hexdec($minColor_));
      if(strlen($bgColor) < 6)
      {
         $bgColor=str_repeat("0",6 - strlen($bgColor)) . $bgColor;
      }
      if(strlen($fgColor) < 6)
      {
         $fgColor=str_repeat("0",6 - strlen($fgColor)) . $fgColor;
      }
      return $bgColor;
   }//------------------------------------------------------------------------------------
   static function misc_jsBln2PhpBln($value,$retString=false) //js boolean to php boolean converter
   {
		if($retString==false)
		{
			$value=strtolower($value);
			if($value=="false" || $value=="off" || $value=="0") return false;
			else if($value=="true" || $value=="on" || $value=="1") return true;
			else return false;
		}
		else
		{
			$value=strtolower($value);
			if($value=="false" || $value=="off" || $value=="0") return "0";
			else if($value=="true" || $value=="on" || $value=="1") return "1";
			else return "0";
		}	
   }//------------------------------------------------------------------------------------
	static function misc_isJson($string) 
	{
	   json_decode($string);
	   return (json_last_error() == JSON_ERROR_NONE);
	}//------------------------------------------------------------------------------------
	static function misc_distance($a, $b,$km=true)
	{
		//$a=[lat,lng]
		//$b=[lat,lng]
		list($lat1, $lon1) = $a;
		list($lat2, $lon2) = $b;

		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		if($km==true)
		{
			$km = $miles * 1.6;
			return $km;
		}
		else
		   return $miles;	
	}//------------------------------------------------------------------------------------
	function misc_distanceSort($location,$aryLocations,$latKeyName="lat",$lngKeyName="lng",$km=true)
	{
		//$location=[lat,lng];
		//$aryLocations=[ [lat=>num,lng=>num,otherData],[lat,lng,otherData],[lat,lng,otherData],[lat,lng,otherData],... ]
		
		$aryTemp=[];
		$aryRet=[];
		$index=-1;
		for($i=0;$i < count($aryLocations);$i++)
		{
			$a=$location;
			$b=[$aryLocations[$i][$latKeyName] , $aryLocations[$i][$lngKeyName]];
			$distance=$this->misc_distance($a,$b,$km);
		   $aryTemp[$i]=$distance;			
		}
		asort($aryTemp);
		foreach($aryTemp as $key=>$val)
		{
			$index++;
         $aryLocations[$key]=array_slice($aryLocations[$key], 0, count($aryLocations[$key]), true) + array("distance"=>$val);
		   $aryRet[$index]=$aryLocations[$key];	
		}
		return $aryRet;
	}//------------------------------------------------------------------------------------
	function misc_distanceSort2($location,$aryLocations,$loctionKeyName="location",$latLngChrSplit="|",$km=true)
	{
		//$location=[lat,lng];
		//$aryLocations=[ [location=>"lat|lng",otherData],[location=>"lat|lng",otherData],[location=>"lat|lng",otherData],[location=>"lat|lng",otherData],... ]
		if(is_object($aryLocations[0])) $isObj=true; else $isObj=false;
		$aryTemp=[];
		$aryRet=[];
		$index=-1;
		for($i=0;$i < count($aryLocations);$i++)
		{
			if($isObj) $aryLocations[$i]=(array)$aryLocations[$i];
			$a=$location;
			$latLng=explode($latLngChrSplit,$aryLocations[$i][$loctionKeyName]);
			if(count($latLng) > 1)
			{
				$b=[$latLng[0] , $latLng[1]];
				$distance=$this->misc_distance($a,$b,$km);
				$aryTemp[$i]=$distance;	
			}			
		}
		asort($aryTemp);
		foreach($aryTemp as $key=>$val)
		{
			$index++;
			$aryLocations[$key]=array_slice($aryLocations[$key], 0, count($aryLocations[$key]), true) + array("distance"=>$val);
		   
			if($isObj)
			   $aryRet[$index]=(object)$aryLocations[$key];
			else
				$aryRet[$index]=$aryLocations[$key];
		}
		return $aryRet;
	}//------------------------------------------------------------------------------------
	static function misc_curl($url,$fields_string="")
	{
		$ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
		$headers= array(
			'Accept: application/json',
			'Content-Type: application/x-www-form-urlencoded',
			'charset: utf-8'
		);			
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_USERAGENT, $ua);
		curl_setopt($ch, CURLOPT_COOKIE, 'NID=67=pdjIQN5CUKVn0bRgAlqitBk7WHVivLsbLcr7QOWMn35Pq03N1WMy6kxYBPORtaQUPQrfMK4Yo0vVz8tH97ejX3q7P2lNuPjTOhwqaI2bXCgPGSDKkdFoiYIqXubR0cTJ48hIAaKQqiQi_lpoe6edhMglvOO9ynw; PREF=ID=52aa671013493765:U=0cfb5c96530d04e3:FF=0:LD=en:TM=1370266105:LM=1370341612:GM=1:S=Kcc6KUnZwWfy3cOl; OTZ=1800625_34_34__34_; S=talkgadget=38GaRzFbruDPtFjrghEtRw; SID=DQAAALoAAADHyIbtG3J_u2hwNi4N6UQWgXlwOAQL58VRB_0xQYbDiL2HA5zvefboor5YVmHc8Zt5lcA0LCd2Riv4WsW53ZbNCv8Qu_THhIvtRgdEZfgk26LrKmObye1wU62jESQoNdbapFAfEH_IGHSIA0ZKsZrHiWLGVpujKyUvHHGsZc_XZm4Z4tb2bbYWWYAv02mw2njnf4jiKP2QTxnlnKFK77UvWn4FFcahe-XTk8Jlqblu66AlkTGMZpU0BDlYMValdnU; HSID=A6VT_ZJ0ZSm8NTdFf; SSID=A9_PWUXbZLazoEskE; APISID=RSS_BK5QSEmzBxlS/ApSt2fMy1g36vrYvk; SAPISID=ZIMOP9lJ_E8SLdkL/A32W20hPpwgd5Kg1J');
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

		$result = curl_exec($ch);
		$last = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
		curl_close($ch);
		return [$result,$last];
	}//------------------------------------------------------------------------------------
	static function misc_ip()
	{
		$ip=0;
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else
			$ip = $_SERVER['REMOTE_ADDR'];
		return $ip;		
	}//------------------------------------------------------------------------------------
	static function time_diff1($timeOld,$timeNew)
	{
			$diff=(($timeNew - $timeOld)/60)*60;
			if($diff < 60)
				$dateDiff=round($diff) . ',second';
			else if($diff > 60 && $diff < 3600)
				$dateDiff=round($diff/60) . ',minute';
			else if($diff > 3600 && $diff < 86400)
				$dateDiff=round($diff/60/60) . ',hour';
			else if($diff > 86400 && $diff < 604800)
				$dateDiff=round($diff/60/60/24) . ',day';
			else if($diff > 604800 && $diff < 2592000)
				$dateDiff=round($diff/60/60/24/7) . ',week';			
			else if($diff > 2592000 && $diff < 31104000)
			   $dateDiff=round($diff/60/60/24/30) . ',month';
			else if($diff > 31104000)
			   $dateDiff=round($diff/60/60/24/30/12) . ',year';
      return $dateDiff;			
	}//------------------------------------------------------------------------------------
}
?>
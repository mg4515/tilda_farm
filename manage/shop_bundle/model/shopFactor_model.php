<?php
require_once $oPath->manageDir("shop_bundle/model/shopPriceSettings_model.php");
require_once $oPath->manageDir("kifPool_bundle/model/kifPool_model.php");
require_once $oPath->manageDir("users_bundle/model/users_model.php");
class cShopFactor
{
   private $tbl_shopSubfactor="shopSubfactor";
   private $tbl_shopFactor="shopFactor";
   private $tbl_shopItems="shopItems";

   function factor_getAll($argArray)
   {
      global $oDbq;
		
		$subfactorId=@$argArray["subfactorId"]; //shenase factor asly ya sabad
		$userId=@$argArray["userId"]; //shenase kharidar
		$userSellerId=@$argArray["userSellerId"]; //shenase forooshandeh
		$userMarketerId=@$argArray["userMarketerId"]; //shenase bazaryab
		$payStatus=@$argArray["payStatus"]; //vazeiat pardakht, 0 or 1
		$shopItemId=@$argArray["shopItemId"]; //shenase mahsool
		$checkout=@$argArray["checkout"]; //barrasy shode ya nashode
		$trash=@$argArray["trash"]; //hazf shode ya nashodeh
		$searchWord=@$argArray["searchWord"];
		$dateStart=@$argArray["dateStart"];
		$dateEnd=@$argArray["dateEnd"];
		$sortByRow=@$argArray["sortByRow"] ? $argArray["sortByRow"] : true;
		$temp=@$argArray["temp"];
		$limitStr=@$argArray["limitStr"];		
		
      $where="";
      $payStatus_="";
      $status_="";
      if($subfactorId!="")
      {
         if($where=="") $where="`subfactorId`={$subfactorId}"; else $where.=" AND `subfactorId`={$subfactorId}";
      }		
      if($userId!="")
      {
         if($where=="") $where="`userId`={$userId}"; else $where.=" AND `userId`={$userId}";
      }
      if($userSellerId!="")
      {
         if($where=="") $where="`userSellerId`={$userSellerId}"; else $where.=" AND `userSellerId`={$userSellerId}";
      }
      if($userMarketerId!="")
      {
         if($where=="") $where="`userMarketerId`={$userMarketerId}"; else $where.=" AND `userMarketerId`={$userMarketerId}";
      }
      if($payStatus!="")
      {
         if($where=="") $where="`payStatus`={$payStatus}"; else $where.=" AND `payStatus`={$payStatus}";
      }		
      if($shopItemId!="")
      {
         if($where=="") $where="`shopItemId`={$shopItemId}"; else $where.=" AND `shopItemId`={$shopItemId}";
      }
      if($checkout!="")
      {
         if($where=="") $where="`checkout`={$checkout}"; else $where.=" AND `checkout`={$checkout}";
      }
      if($trash!="")
      {
         if($where=="") $where="`trash`='{$trash}'"; else $where.=" AND `trash`='{$trash}'";
      }
      if($temp!="")
      {
         if($where=="") $where="`temp`='{$temp}'"; else $where.=" AND `temp`='{$temp}'";
      }
      if($searchWord!="")
      {
         if($where=="") $where="(`id` LIKE '%{$searchWord}%' OR `userSellerId` LIKE '%{$searchWord}%' OR `userMarketerId` LIKE '%{$searchWord}%' OR `userId` LIKE '%{$searchWord}%' OR `userMobile` LIKE '%{$searchWord}%' OR `userMail` LIKE '%{$searchWord}%' OR `transactionId` LIKE '%{$searchWord}%' OR `userMobile` LIKE '%{$searchWord}%')"; else $where.=" AND (`id` LIKE '%{$searchWord}%' OR `userSellerId` LIKE '%{$searchWord}%' OR `userMarketerId` LIKE '%{$searchWord}%' OR `userId` LIKE '%{$searchWord}%' OR `userMobile` LIKE '%{$searchWord}%' OR `userMail` LIKE '%{$searchWord}%' OR `transactionId` LIKE '%{$searchWord}%' OR `userFname` LIKE '%{$searchWord}%' OR `userLname` LIKE '%{$searchWord}%' OR `userMobile` LIKE '%{$searchWord}%')";
      }
      if($dateStart!="" && $dateEnd!="")
      {
         if($where=="") $where="(`insertDate` <= {$dateEnd} AND `insertDate` > {$dateStart})"; else $where.=" AND (`insertDate` <= {$dateEnd} AND `insertDate` > {$dateStart})";
      }
		
      if($sortByRow==true)$ret=$oDbq->table($this->tbl_shopFactor)->fields("*")->where($where)->orderBy("`id`");
      else if($sortByRow==false)$ret=$oDbq->table($this->tbl_shopFactor)->fields("*")->where($where);
      if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//--------------------------------------------------------------------------  
   function factor_get($id)
   {
      global $oDbq;
      $ret=@$oDbq->table($this->tbl_shopFactor)->fields("*")->where("`id`='{$id}'")->select()[0];
      return $ret;
   }//--------------------------------------------------------------------------
   function factor_delete($id,$userId="") //dalete factor or factors
   {
      global $oDbq;

      if($userId!="") $where="`userId`={$userId} AND "; else $where="";
      $id=explode(",",rtrim($id,","));
      for($i=0;$i < count($id);$i++)
      {
         $selectedId=$id[$i];
         if($selectedId !="" )
         {
            $where.="(`id`='{$selectedId}')";
            if($i < count($id) - 1) $where.=" OR ";
				//@$this->delItem("",$selectedId);
         }
      }

      $oDbq->table($this->tbl_shopFactor)->fields("*")->where("({$where})")->delete();
   }//------------------------------------------------------------------------------------
   function factor_deleteBySubfactorId($subfactorId='') //hazfe hameye factorha dar yek subfactor
   {
      global $oDbq;
		$oDbq->table($this->tbl_shopFactor)->fields("*")->where("`subfactorId`='{$subfactorId}'")->delete();
   }//------------------------------------------------------------------------------------	
   function factor_deleteByTemp($id='') //hazfe itemhaey ke temp==1 hastand
   {
      global $oDbq;
		if($id=="")
         $oDbq->table($this->tbl_shopFactor)->fields("*")->where("`temp`=1")->delete();
		else
			$oDbq->table($this->tbl_shopFactor)->fields("*")->where("`id`='{$id}' AND `temp`=1")->delete();
   }//------------------------------------------------------------------------------------	
   function factor_trash($id,$trashById=0) //trash factor or factors
   {
      global $oDbq;
      $where="";
      $id=explode(",",rtrim($id,","));
      for($i=0;$i < count($id);$i++)
      {
         $selectedId=$id[$i];
         if($selectedId !="" )
         {
            $where.="(`id`='{$selectedId}')";
            if($i < count($id) - 1) $where.=" OR ";
         }
      }
      
      $oDbq->table($this->tbl_shopFactor)->set("`trash`='1',`trashById`={$trashById}")->where("({$where})")->update();
   }//------------------------------------------------------------------------------------
   function factor_unTrash($id) //unTrash factor or factors
   {
      global $oDbq;

      $where="";
      $id=explode(",",rtrim($id,","));
      for($i=0;$i < count($id);$i++)
      {
         $selectedId=$id[$i];
         if($selectedId !="" )
         {
            $where.="(`id`='{$selectedId}')";
            if($i < count($id) - 1) $where.=" OR ";
         }
      }

      $oDbq->table($this->tbl_shopFactor)->set("`trash`='0'")->where("({$where})")->update();
   }//------------------------------------------------------------------------------------
   function factor_setStatus($id,$value) //0=darHaleBarrasi 2=amadeh ersal 3=ersal shod 4=tahvildade shod 5=laghv shod
   {
      global $oDbq;
      $oDbq->table($this->tbl_shopFactor)->set("`status`='{$value}'")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------
   function factor_setTemp($id,$value) 
   {
      global $oDbq;
      $oDbq->table($this->tbl_shopFactor)->set("`temp`={$value}")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------
   function factor_setTempBySubfactorId($subfactorId,$value) 
   {
      global $oDbq;
      $oDbq->table($this->tbl_shopFactor)->set("`temp`={$value}")->where("`subfactorId`='{$subfactorId}'")->update();
   }//------------------------------------------------------------------------------------	
   function factor_getTemp($id)
   {
      global $oDbq;
      return @$oDbq->table($this->tbl_shopFactor)->fields("`temp`")->where("`id`='{$id}'")->select()[0]->temp;
   }//------------------------------------------------------------------------------------
   function factor_setCount($id,$value='+') 
   {
      global $oDbq;
		if($value=='+' || $value=='-')
         $oDbq->table($this->tbl_shopFactor)->set("`count`= `count`{$value}1 ")->where("`id`='{$id}'")->update(); //+1 or -1
      else  
			$oDbq->table($this->tbl_shopFactor)->set("`count`={$value}")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------	
   function factor_getCount($id)
   {
      global $oDbq;
      return @$oDbq->table($this->tbl_shopFactor)->fields("`count`")->where("`id`='{$id}'")->select()[0]->count;
   }//------------------------------------------------------------------------------------	
   function factor_setCheckout($id,$value) 
   {
      global $oDbq;
      $oDbq->table($this->tbl_shopFactor)->set("`checkout`={$value}")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------
   function factor_setCheckoutBySubfactorId($subfactorId,$value) 
   {
      global $oDbq;
      $oDbq->table($this->tbl_shopFactor)->set("`checkout`={$value}")->where("`subfactorId`='{$subfactorId}'")->update();
   }//------------------------------------------------------------------------------------	
   function factor_getCheckout($id)
   {
      global $oDbq;
      return $oDbq->table($this->tbl_shopFactor)->fields("`checkout`")->where("`id`='{$id}'")->select()[0];
   }//------------------------------------------------------------------------------------	
	function factor_setPayStatus($id,$value)
	{
      global $oDbq;
      $oDbq->table($this->tbl_shopFactor)->set("`payStatus`='{$value}'")->where("`id`='{$id}'")->update();		
	}//------------------------------------------------------------------------------------
	function factor_setPayStatusBySubfactorId($subfactorId,$value)
	{
      global $oDbq;
      $oDbq->table($this->tbl_shopFactor)->set("`payStatus`='{$value}'")->where("`subfactorId`='{$subfactorId}'")->update();		
	}//------------------------------------------------------------------------------------	
   function factor_getPayStatus($id)
   {
      global $oDbq;
      return $oDbq->table($this->tbl_shopFactor)->fields("`payStatus`")->where("`id`='{$id}'")->select()[0];
   }//------------------------------------------------------------------------------------	
	function factor_setTransactionId($id,$value)
	{
      global $oDbq;
      $oDbq->table($this->tbl_shopFactor)->set("`transactionId`='{$value}'")->where("`id`='{$id}'")->update();		
	}//------------------------------------------------------------------------------------
	function factor_setTransactionBysubfactorId($subfactorId,$value)
	{
      global $oDbq;
      $oDbq->table($this->tbl_shopFactor)->set("`transactionId`='{$value}'")->where("`subfactorId`='{$subfactorId}'")->update();		
	}//------------------------------------------------------------------------------------	
   function factor_getUserMarketer($id) //agr factor, bazaryab dashte bashad, object karbar ra barmigardanad
   {
      global $oDbq;
      $userId=@$oDbq->table($this->tbl_shopFactor)->fields("`userMarketerId`")->where("`id`='{$id}'")->select()[0]->userMarketerId;
		if($userId)
		{
			$oUsers=new cUsers();
			$user=@$oUsers->get($userId,'0');
			return $user;
		}
   }//------------------------------------------------------------------------------------
   function factor_getUserSeller($id) //agr factor, forooshande dashte bashad, object karbar ra barmigardanad
   {
      global $oDbq;
      $userId=@$oDbq->table($this->tbl_shopFactor)->fields("`userSellerId`")->where("`id`='{$id}'")->select()[0]->userSellerId;
		if($userId)
		{
			$oUsers=new cUsers();
			$user=@$oUsers->get($userId,'0');
			return $user;
		}
   }//------------------------------------------------------------------------------------	
   function factor_insert($argArray)
   {
		//subfactorId, userMarketerId, payStatus, temp, userId, userIp, user..., shopItemId, shopCouponId
      global $oDbq;

		$insertDate=time();
		
		//id
		$id=@$argArray['id'];
		if(!$id)
		{
			$id=$insertDate + rand(100,1000);
		}
		
		//token
		$token=cTools::misc_createId('full',true);
		
		//shopItem
		$oShopItems=new cShopItems(); 
		$shopItem=$oShopItems->get($argArray['shopItemId']);				
		$userSellerId=$shopItem->userId;
		$shopItemId=$shopItem->id;
		$shopItemTitle=$shopItem->title;
		$shopItemVazn=$shopItem->vazn;
		$shopItemPrice=$shopItem->price;
		$shopItemDiscount=$shopItem->discount;
		
		//coupon
		if($argArray['shopCouponId'] > 0)
		{
			$oShopCoupon=new cShopCoupon();
			$shopCoupon=$oShopCoupon->get($argArray['shopCouponId']);
			$shopCouponDiscount=$shopCoupon->discount;
			$shopCouponMaxDiscount=$shopCoupon->maxDiscount;
			$shopCouponTitle=$shopCoupon->title;
		}
		else
		{
			$shopCouponDiscount=0;
			$shopCouponMaxDiscount=0;
			$shopCouponTitle='';			
		}
		
		//shop settings
		$oShopPriceSettings=new cShopPriceSettings();
		$shopPriceSettings=$oShopPriceSettings->get();
		$shopPricePlus=$shopPriceSettings->pricePlus;
		$shopMarketerCommission=$shopPriceSettings->marketerCommission;
		$shopSiteCommission=$shopPriceSettings->siteCommission;
		$shopPricePlus=$shopPriceSettings->pricePlus;
		
		//check value
		$subfactorId=isset($argArray['subfactorId']) ? $argArray['subfactorId'] : '0';
		$userMarketerId=@$argArray['userMarketerId'] ? $argArray['userMarketerId'] : '0';
		$count=isset($argArray['count']) ? $argArray['count'] : '1';
		$payStatus=isset($argArray['payStatus']) ? $argArray['payStatus'] : '0';
		$temp=isset($argArray['temp']) ? $argArray['temp'] : '0';
		$userIp=isset($argArray['userIp']) ? $argArray['userIp'] : '';
		$userFname=isset($argArray['userFname']) ? $argArray['userFname'] : '';
		$userLname=isset($argArray['userLname']) ? $argArray['userLname'] : '';
		$userMobile=isset($argArray['userMobile']) ? $argArray['userMobile'] : '';
		$userMail=isset($argArray['userMail']) ? $argArray['userMail'] : '';
		$userPhone=isset($argArray['userPhone']) ? $argArray['userPhone'] : '';
		$userMelliCode=isset($argArray['userMelliCode']) ? $argArray['userMelliCode'] : '';
		$userState=isset($argArray['userState']) ? $argArray['userState'] : '';
		$userCity=isset($argArray['userCity']) ? $argArray['userCity'] : '';
		$userAddress=isset($argArray['userAddress']) ? $argArray['userAddress'] : '';
		$userZipCode=isset($argArray['userZipCode']) ? $argArray['userZipCode'] : '';		
		$insertById=isset($argArray['insertById']) ? $argArray['insertById'] : '0';
		
      $oDbq->table($this->tbl_shopFactor)->fields("`id`='{$id}',
																		`subfactorId`='{$subfactorId}',
																		`userSellerId`='{$userSellerId}',
																		`userMarketerId`={$argArray['userMarketerId']},
																		`payStatus`={$payStatus},
																		`temp`={$temp},
																		`count`={$count},
																		`userId`='{$argArray['userId']}',
																		`userIp`='{$userIp}',
																		`userFname`='{$userFname}',
																		`userLname`='{$userLname}',
																		`userMobile`='{$userMobile}',
																		`userMail`='{$userMail}',
																		`userPhone`='{$userPhone}',
																		`userMelliCode`='{$userMelliCode}',
																		`userState`='{$userState}',
																		`userCity`='{$userCity}',
																		`userAddress`='{$userAddress}',
																		`userZipCode`='{$userZipCode}',
																		`shopItemId`='{$shopItemId}',
																		`shopItemTitle`='{$shopItemTitle}',
																		`shopItemVazn`='{$shopItemVazn}',
																		`shopItemPrice`={$shopItemPrice},
																		`shopItemDiscount`={$shopItemDiscount},
																		`shopPricePlus`={$shopPricePlus},
																		`shopMarketerCommission`={$shopMarketerCommission},
																		`shopSiteCommission`={$shopSiteCommission},																		
																		`shopCouponDiscount`={$shopCouponDiscount},
																		`shopCouponMaxDiscount`={$shopCouponMaxDiscount},
																		`shopCouponTitle`='{$shopCouponTitle}',
																		`insertDate`={$insertDate},
																		`insertById`={$insertById},
																		`token`='{$token}'
															")->insert();
      return $id;
   }//------------------------------------------------------------------------------------
   function factor_update($argArray)
   {
		//shopItemTitle, shopItemPrice, shopItemDiscount, shopPricePlus, shopDownloadExpire, userMobile, userMail, comment
      global $oDbq;
		$updateDate=time();
      $oDbq->table($this->tbl_shopFactor)->fields("
																`userMobile`='{$argArray['userMobile']}',
																`userMail`='{$argArray['userMail']}',
																`shopItemTitle`='{$argArray['shopItemTitle']}',
																`shopItemPrice`={$argArray['shopItemPrice']},
																`shopItemDiscount`={$argArray['shopItemDiscount']},
																`shopPricePlus`={$argArray['shopPricePlus']},
																`shopCouponDiscount`={$argArray['shopCouponDiscount']},
																`shopCouponMaxDiscount`={$argArray['shopCouponMaxDiscount']},
																`shopCouponTitle`='{$argArray['shopCouponTitle']}',
																`updateDate`={$updateDate},
																`updateById`={$argArray['updateById']}
															")->where("`id`={$id}")->update();
      return $id;
   }//------------------------------------------------------------------------------------
	function factor_updateBySubfactorId($subfactorId)
	{
      global $oDbq;
		$updateDate=time();
		$oDbq->table("`{$this->tbl_shopFactor}`, `{$this->tbl_shopSubfactor}`")->set("
																											`{$this->tbl_shopFactor}`.`userMobile`=`{$this->tbl_shopSubfactor}`.`userMobile`,
																											`{$this->tbl_shopFactor}`.`userMail`=`{$this->tbl_shopSubfactor}`.`userMail`,
																											`{$this->tbl_shopFactor}`.`shopItemTitle`=`{$this->tbl_shopSubfactor}`.`shopItemTitle`,
																											`{$this->tbl_shopFactor}`.`shopItemPrice`=`{$this->tbl_shopSubfactor}`.`shopItemPrice`,
																											`{$this->tbl_shopFactor}`.`shopItemDiscount`=`{$this->tbl_shopSubfactor}`.`shopItemDiscount`,
																											`{$this->tbl_shopFactor}`.`shopPricePlus`=`{$this->tbl_shopSubfactor}`.`shopPricePlus`,
																											`{$this->tbl_shopFactor}`.`shopCouponDiscount`=`{$this->tbl_shopSubfactor}`.`shopCouponDiscount`,
																											`{$this->tbl_shopFactor}`.`shopCouponMaxDiscount`=`{$this->tbl_shopSubfactor}`.`shopCouponMaxDiscount`,
																											`{$this->tbl_shopFactor}`.`shopCouponTitle`=`{$this->tbl_shopSubfactor}`.`shopCouponTitle`,
																											`updateDate`={$updateDate},
																											`updateById`={$updateById}																											   
																										")->where("`{$this->tbl_shopFactor}`.`subfactorId`={$subfactorId} AND `{$this->tbl_shopSubfactor}`.`id`={$subfactorId}")->update();   
	}//------------------------------------------------------------------------------------		
	function factor_getPrice($id) //mohasebeh gheimat yek factor
   {
      global $oDbq;
      $item=$oDbq->table($this->tbl_shopFactor)->fields("*")->where("`id`='{$id}'")->select()[0];	   
      $price=$item->shopItemPrice;
      $pricePlus=$item->shopPricePlus;
      $discount=$item->shopItemDiscount;
      $couponDiscount=$item->shopCouponDiscount;
      $couponMaxDiscount=$item->shopCouponMaxDiscount;
      
		$price= $price + $pricePlus;
		if($discount > 0) $price=$price - (($discount * $price) / 100); 
		//-----
		if($couponDiscount > 0 )
		{
			$couponDiscount_=($couponDiscount * $price) / 100;
			if($couponDiscount_ > $couponMaxDiscount)
				$price=$price - $couponMaxDiscount;
			else
				$price=$price - $couponDiscount_;
		}
		return $price;
   }//------------------------------------------------------------------------------------
	function factor_getMarketerIncome($userMarketerId,$factorId='') //MIZANE DARAMAD as yek faktor ya hame faktorha, baraye yek bazaryab
   {
      global $oDbq;
		//---
		$where="`payStatus`=1 AND `trash`=0 AND `temp`=0 AND `userMarketerId`={$userMarketerId}";
		if($factorId) $where.=" AND `id`={$factorId}";
      $items=$oDbq->table($this->tbl_shopFactor)->fields("*")->where($where)->select();
      $commission=0;
		foreach($items as $item)
		{
			//price factor
			$price=$item->shopItemPrice; //gheimat mahsool
			$pricePlus=$item->shopPricePlus; //+val or -val
			$discount=$item->shopItemDiscount; //takhfif mahsool
			$couponDiscount=$item->shopCouponDiscount; //takhfif kopin
			$couponMaxDiscount=$item->shopCouponMaxDiscount;
			
			//tanzim gheimat kala
			$price= $price + $pricePlus;
			if($discount > 0) $price=$price - (($discount * $price) / 100); 
			
			//mohasebeh kopin takhfif agar sabt shode bashad va kam kardan an az mablagh kol
			if($couponDiscount > 0 )
			{
				$couponDiscount_=($couponDiscount * $price) / 100;
				if($couponDiscount_ > $couponMaxDiscount)
					$price=$price - $couponMaxDiscount;
				else
					$price=$price - $couponDiscount_;
			}
			
			//Commission
			$commission+=($item->shopMarketerCommission * $price) / 100;
		}			

		return $commission;
   }//------------------------------------------------------------------------------------
	function factor_getSellerIncome($userSellerId,$factorId='') //MIZANE DARAMAD as yek faktor ya hame faktorha, baraye yek forooshandeh
   {
      global $oDbq;
		//---
		$where="`payStatus`=1 AND `trash`=0 AND `temp`=0 AND `userSellerId`={$userSellerId}";
		if($factorId) $where.=" AND `id`={$factorId}";
      $items=$oDbq->table($this->tbl_shopFactor)->fields("*")->where($where)->select();
      $commission=0;
		foreach($items as $item)
		{
			//price factor
			$price=$item->shopItemPrice; //gheimat mahsool
			$pricePlus=$item->shopPricePlus; //+val or -val
			$discount=$item->shopItemDiscount; //takhfif mahsool
			$couponDiscount=$item->shopCouponDiscount; //takhfif kopin
			$couponMaxDiscount=$item->shopCouponMaxDiscount;
			
			//gheimat mahsool
			$price= $price + $pricePlus; // arzan kardan va ya geran kardan. + or -
			
			//emale darsade takhfif bar gheimat
			if($discount > 0) $price=$price - (($discount * $price) / 100); //emale takhfifi
			
			//mohasebeh kopin takhfif agar sabt shode bashad va kam kardan an az mablagh kol
			if($couponDiscount > 0 ) 
			{
				$couponDiscount_=($couponDiscount * $price) / 100; //mablagh takhfif kopin
				if($couponDiscount_ > $couponMaxDiscount)
					$price+=$price - $couponMaxDiscount;
				else
					$price=$price - $couponDiscount_;
			}

			//Subtraction by (marketer Commission)
			//mohasebeh karmozd bazaryab va kam kardan an az mablagh mahsool
			if($item->userMarketerId > 0)
			{
				$marketerCommission= ($item->shopMarketerCommission * $price) / 100;
				$price-= $marketerCommission;
			}
			
			//mohasebeh karmozd webSite
			$shopSiteCommission= (@$item->shopSiteCommission * $price) / 100;
			$price-= $shopSiteCommission;			
			
			//commission
			$commission+= $price;  
		}
		return $commission;
   }//------------------------------------------------------------------------------------
	function factor_getIncome() //MIZANE DARAMAD foroosh (all, marketer, seller, site)
   {
      global $oDbq;
		//---
		$where="`payStatus`=1 AND `trash`=0 AND `temp`=0";
      $items=$oDbq->table($this->tbl_shopFactor)->fields("*")->where($where)->select();
      $marketerCommission=0;
      $sellerCommission=0;
      $siteCommission=0;
		$income=0;
		foreach($items as $item)
		{
			//price factor
			$price=$item->shopItemPrice; //gheimat mahsool
			$pricePlus=$item->shopPricePlus; //+val or -val
			$discount=$item->shopItemDiscount; //takhfif mahsool
			$couponDiscount=$item->shopCouponDiscount; //takhfif kopin
			$couponMaxDiscount=$item->shopCouponMaxDiscount;
			
			//gheimat mahsool
			$price= $price + $pricePlus; // arzan kardan va ya geran kardan. + or -
			
			//emale darsade takhfif bar gheimat
			if($discount > 0) $price=$price - (($discount * $price) / 100); //emale takhfifi
			
			//mohasebeh kopin takhfif agar sabt shode bashad va kam kardan an az mablagh kol
			if($couponDiscount > 0 ) 
			{
				$couponDiscount_=($couponDiscount * $price) / 100; //mablagh takhfif kopin
				if($couponDiscount_ > $couponMaxDiscount)
					$price+=$price - $couponMaxDiscount;
				else
					$price=$price - $couponDiscount_;
			}

			$income+=$price; //daramad az forooshha

			//Subtraction by (marketer Commission)
			//mohasebeh karmozd bazaryab va kam kardan an az mablagh mahsool
			if($item->userMarketerId > 0)
			{
				$marketerCommission= ($item->shopMarketerCommission * $price) / 100;
				$price-= $marketerCommission;
				$marketerCommission+=$price;
			}
			
			//mohasebeh karmozd webSite
			if($item->shopSiteCommission > 0)
			{
				$shopSiteCommission= ($item->shopSiteCommission * $price) / 100;
				$price-= $shopSiteCommission;
				$sellerCommission+=$price;
			}			
		 
			$siteCommission+=$price;
		}
		return [
		   'marketerCommission'=>$marketerCommission,
		   'sellerCommission'=>$sellerCommission,
		   'siteCommission'=>$siteCommission,
		   'income'=>$income
		];
   }//------------------------------------------------------------------------------------	
	function factor_setMarketerIncome($factorId) //bad az pardakht movafagh yek faktor, agar factor, bazaryab dashte bashad, poorsant bazryab be kif pool karbar variz myshavad
	{
		$userMarketer=$this->factor_getUserMarketer($factorId); //bedast avardan userId bazaryab agar vojood dashte bashe 
		if($userMarketer)
		{
			$income=$this->factor_getMarketerIncome($userMarketer->id,$factorId);
			$oKifPool=new cKifPool();
			$oKifPool->deposit($userMarketer->id,$income,"پورسانت بازاریابی از فاکتور {$factorId}");
		   return $userMarketer; //object karbar, baraye ersale sms ya mail ya ...
		}		
	}//------------------------------------------------------------------------------------
	function factor_setSellerIncome($factorId) //bad az pardakht movafagh yek faktor, agar factor, forooshande dashte bashad, poorsant bazryab be kif pool karbar variz myshavad
	{
		$userSeller=$this->factor_getUserSeller($factorId); //bedast avardan userId forooshande agar vojood dashte bashe 
		if($userSeller)
		{
			$income=$this->factor_getSellerIncome($userSeller->id,$factorId);
			$oKifPool=new cKifPool();
			$oKifPool->deposit($userSeller->id,$this->factor_getSellerIncome($userSeller->id,$factorId),"پورسانت فروش از فاکتور {$factorId}");
		   return $userSeller; //object karbar, baraye ersale sms ya mail ya ...
		}		
	}//------------------------------------------------------------------------------------


   function subfactor_getAll($argArray)
   {
      global $oDbq;
		$userId=isset($argArray["userId"]) ? $argArray["userId"]:""; //shenase kharidar
		$userSellerId=isset($argArray["userSellerId"]) ? $argArray["userSellerId"]:""; //shenase forooshandegan
		$userMarketerId=isset($argArray["userMarketerId"]) ? $argArray["userMarketerId"]:""; //shenase bazaryab
		$payStatus=isset($argArray["payStatus"]) ? $argArray["payStatus"]:"";
		$status=isset($argArray["status"]) ? $argArray["status"]:"";
		$trash=isset($argArray["trash"]) ? $argArray["trash"]:"";
		$temp=isset($argArray["temp"]) ? $argArray["temp"]:"";
		$sortByRow=isset($argArray["sortByRow"]) ? $argArray["sortByRow"]:true;
		$searchWord=isset($argArray["searchWord"]) ? $argArray["searchWord"]:"";
		$limitStr=isset($argArray["limitStr"]) ? $argArray["limitStr"]:"";		
		
      $where="";
      $payStatus_="";
      $status_="";
      if($userId!="")
      {
         if($where=="") $where="`userId`='{$userId}'"; else $where.=" AND `userId`='{$userId}'";
      }
      if($userSellerId!="")
      {
         if($where=="") $where="`userSellerId` LIKE '%{$userSellerId}%'"; else $where.=" AND `userSellerId` LIKE '%{$userSellerId}%'";
      }
      if($userMarketerId!="")
      {
         if($where=="") $where="`userMarketerId`='{$userMarketerId}'"; else $where.=" AND `userMarketerId`='{$userMarketerId}'";
      }
      if($payStatus!="")
      {
         if($where=="") $where="`payStatus`={$payStatus}"; else $where.=" AND `payStatus`={$payStatus}";
      }
      if($status!="")
      {
         $statusArr=explode(",",$status);
         for($i=0;$i < count($statusArr);$i++)
         {
            if($status_=="") $status_="`status`='{$statusArr[$i]}'"; else $status_.=" OR `status`='{$statusArr[$i]}'";
         }
         if($where=="") $where="({$status_})"; else $where.=" AND ({$status_})";
      }
      if($trash!="")
      {
         if($where=="") $where="`trash`='{$trash}'"; else $where.=" AND `trash`='{$trash}'";
      }
      if($temp!="")
      {
         if($where=="") $where="`temp`='{$temp}'"; else $where.=" AND `temp`='{$temp}'";
      }
      if($searchWord!="")
      {
         if($where=="") $where="(`id` LIKE '%{$searchWord}%' OR `userId` LIKE '%{$searchWord}%' OR `payId` LIKE '%{$searchWord}%' OR `userFname` LIKE '%{$searchWord}%' OR `userLname` LIKE '%{$searchWord}%' OR `userMobile` LIKE '%{$searchWord}%')"; else $where.=" AND (`id` LIKE '%{$searchWord}%' OR `userId` LIKE '%{$searchWord}%' OR `payId` LIKE '%{$searchWord}%' OR `userFname` LIKE '%{$searchWord}%' OR `userLname` LIKE '%{$searchWord}%' OR `userMobile` LIKE '%{$searchWord}%')";
      }
		
      if($sortByRow==true)$ret=$oDbq->table($this->tbl_shopSubfactor)->fields("*")->where($where)->orderBy("`id`");
      else if($sortByRow==false)$ret=$oDbq->table($this->tbl_shopSubfactor)->fields("*")->where($where);
      if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//--------------------------------------------------------------------------
   function subfactor_getAll2($argArray) //get all and ['factor']
   {
      global $oDbq;
      $items=(array)$this->subfactor_getAll($argArray);
		$subfactor=[];
		$count=count($items);
		for($i=0;$i < $count;$i++)
		{
			$factor=$this->factor_get(['subfactorId'=>$item[$i]['id']]);
			$items[$i]['factor']=$factor;
		}
		return (object)$items;
   }//--------------------------------------------------------------------------
   function subfactor_delete($id,$userId="") //dalete yek subfactor ya chand subfactors.(id1, id2, ...)
   {
      global $oDbq;

      if($userId!="") $where="`userId`={$userId} AND "; else $where="";
      $id=explode(",",rtrim($id,","));
      for($i=0;$i < count($id);$i++)
      {
         $selectedId=$id[$i];
         if($selectedId !="" )
         {
            $where.="(`id`='{$selectedId}')";
            if($i < count($id) - 1) $where.=" OR ";
				//@$this->delItem("",$selectedId);
         }
      }

      $oDbq->table($this->tbl_shopSubfactor)->fields("*")->where("({$where})")->delete();
   }//------------------------------------------------------------------------------------
   function subfactor_trash($id,$trashById=0) //trash yek subfactor or chand subfactors(id1, id2, ...)
   {
      global $oDbq;
      $where="";
      $id=explode(",",rtrim($id,","));
      for($i=0;$i < count($id);$i++)
      {
         $selectedId=$id[$i];
         if($selectedId !="" )
         {
            $where.="(`id`='{$selectedId}')";
            if($i < count($id) - 1) $where.=" OR ";
         }
      }
      
      $oDbq->table($this->tbl_shopSubfactor)->set("`trash`='1',`trashById`={$trashById}")->where("({$where})")->update();
   }//------------------------------------------------------------------------------------
   function subfactor_unTrash($id) //unTrash yek subfactor or chand subfactors(id1, id2, ...)
   {
      global $oDbq;

      $where="";
      $id=explode(",",rtrim($id,","));
      for($i=0;$i < count($id);$i++)
      {
         $selectedId=$id[$i];
         if($selectedId !="" )
         {
            $where.="(`id`='{$selectedId}')";
            if($i < count($id) - 1) $where.=" OR ";
         }
      }

      $oDbq->table($this->tbl_shopSubfactor)->set("`trash`='0'")->where("({$where})")->update();
   }//------------------------------------------------------------------------------------
   function subfactor_setStatus($id,$value) //0=darHaleBarrasi 2=amadeh ersal 3=ersal shod 4=tahvildade shod 5=laghv shod
   {
      global $oDbq;
      $oDbq->table($this->tbl_shopSubfactor)->set("`status`='{$value}'")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------
   function subfactor_setTemp($id,$value) 
   {
      global $oDbq;
      $oDbq->table($this->tbl_shopSubfactor)->set("`temp`={$value}")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------
   function subfactor_getTemp($id)
   {
      global $oDbq;
      return @$oDbq->table($this->tbl_shopSubfactor)->fields("`temp`")->where("`id`='{$id}'")->select()[0]->temp;
   }//------------------------------------------------------------------------------------
   function subfactor_setUserSellerId($id,$value='')
   {
      global $oDbq;
		if($value!='')
		{
			$oDbq->table($this->tbl_shopSubfactor)->set("`userSellerId`='{$value}'")->where("`id`='{$id}'")->update();
		}
		else
		{
			$arrayUserSellerId=[];
			$factorItems=$this->factor_getAll(['trash'=>'0', 'subfactorId'=>$id]);
			foreach($factorItems as $factor)
			{
				$arrayUserSellerId[$factor->userSellerId]=$factor->userSellerId;
			}
			$value=implode('|',$arrayUserSellerId);
			$oDbq->table($this->tbl_shopSubfactor)->set("`userSellerId`='{$value}'")->where("`id`='{$id}'")->update();
		}
   }//------------------------------------------------------------------------------------	
   function subfactor_insert($argArray)
	{
      global $oDbq;
		$id=time();
		$insertById=isset($argArray['insertById']) ? $argArray['insertById'] : 0;
		$insertDate=$id;
		$userMarketerId=isset($argArray['userMarketerId']) ? $argArray['userMarketerId'] : 0;
		$payStatus=isset($argArray['payStatus']) ? $argArray['payStatus'] : 0;
		$userFname=isset($argArray['userFname']) ? $argArray['userFname'] : '';
		$userLname=isset($argArray['userLname']) ? $argArray['userLname'] : '';
		$userMobile=isset($argArray['userMobile']) ? $argArray['userMobile'] : '';
		$userMail=isset($argArray['userMail']) ? $argArray['userMail'] : '';
		$userPhone=isset($argArray['userPhone']) ? $argArray['userPhone'] : '';
		$userMelliCode=isset($argArray['userMelliCode']) ? $argArray['userMelliCode'] : '';
		$userState=isset($argArray['userState']) ? $argArray['userState'] : '';
		$userCity=isset($argArray['userCity']) ? $argArray['userCity'] : '';
		$userAddress=isset($argArray['userAddress']) ? $argArray['userAddress'] : '';
		$userZipCode=isset($argArray['userZipCode']) ? $argArray['userZipCode'] : '';
		$comment=isset($argArray['comment']) ? $argArray['comment'] : '';
		$commentAdmin=isset($argArray['commentAdmin']) ? $argArray['commentAdmin'] : '';
      $temp=isset($argArray['temp']) ? $argArray['temp'] : 0;
		
		$oDbq->table($this->tbl_shopSubfactor)->set("
																	`id`={$id},
																	`userMarketerId`={$argArray['userMarketerId']},
																	`userId`={$argArray['userId']},	
																	`userFname`='{$userFname}',
																	`userLname`='{$userLname}',
																	`userMobile`='{$userMobile}',
																	`userMail`='{$userMail}',
																	`userPhone`='{$userPhone}',
																	`userMelliCode`='{$userMelliCode}',
																	`userState`='{$userState}',
																	`userCity`='{$userCity}',
																	`userAddress`='{$userAddress}',
																	`userZipCode`='{$userZipCode}',
																	`comment`='{$comment}',
																	`commentAdmin`='{$commentAdmin}',
																	`insertDate`='{$insertDate}',
																	`insertById`='{$insertById}',
																	`payStatus`={$payStatus},
																	`temp`='{$temp}'
														      ")->insert();
		return $id;
	}//--------------------------------------------------------------------------		
   function subfactor_update($argArray)
	{
      global $oDbq;
		$updateById=isset($argArray['updateById']) ? $argArray['updateById'] : 0;
		$updateDate=time();
		$userMarketerId=isset($argArray['userMarketerId']) ? $argArray['userMarketerId'] : 0;
		$userIp=isset($argArray['userIp']) ? $argArray['userIp'] : '';
		$userFname=isset($argArray['userFname']) ? $argArray['userFname'] : '';
		$userLname=isset($argArray['userLname']) ? $argArray['userLname'] : '';
		$userMobile=isset($argArray['userMobile']) ? $argArray['userMobile'] : '';
		$userMail=isset($argArray['userMail']) ? $argArray['userMail'] : '';
		$userPhone=isset($argArray['userPhone']) ? $argArray['userPhone'] : '';
		$userMelliCode=isset($argArray['userMelliCode']) ? $argArray['userMelliCode'] : '';
		$userState=isset($argArray['userState']) ? $argArray['userState'] : '';
		$userCity=isset($argArray['userCity']) ? $argArray['userCity'] : '';
		$userAddress=isset($argArray['userAddress']) ? $argArray['userAddress'] : '';
		$userZipCode=isset($argArray['userZipCode']) ? $argArray['userZipCode'] : '';
		$comment=isset($argArray['comment']) ? $argArray['comment'] : '';
		$commentAdmin=isset($argArray['commentAdmin']) ? $argArray['commentAdmin'] : '';

		$oDbq->table($this->tbl_shopSubfactor)->set("
																`userMarketerId`={$argArray['userMarketerId']},
																`userIp`='{$userIp}',
																`userFname`='{$userFname}',
																`userLname`='{$userLname}',
																`userMobile`='{$userMobile}',
																`userMail`='{$userMail}',
																`userPhone`='{$userPhone}',
																`userMelliCode`='{$userMelliCode}',
																`userState`='{$userState}',
																`userCity`='{$userCity}',
																`userAddress`='{$userAddress}',
																`userZipCode`='{$userZipCode}',
																`comment`='{$comment}',
																`commentAdmin`='{$commentAdmin}',
																`updateDate`='{$updateDate}',
																`updateById`='{$updateById}'
														   ")->where("`id`={$argArray['id']}")->update();
		return $id;
	}//--------------------------------------------------------------------------	
   function basket_exists($userId) //agar subfactor ba temp 1 va shenase karbar, vojood dashte bashad, shense an bargasht dade myshavad. dar gheir insoorat, 1 subfactor insert myshavad va id an bargasht dade myshavad.
	{
		$items=@$this->subfactor_getAll(['trash'=>'0', 'temp'=>'1', 'userId'=>$userId]);
		if(!$items)
		{
			$id=$this->subfactor_insert(['temp'=>'1', 'userId'=>$userId]);
			return $id;
		}
      else
         return $items[0]->id;			
	}//--------------------------------------------------------------------------
   function basket_factorInsert($argArray) //afzoodan yek factor mahsool be sabad(subfactor).
	{
      $subfactorId=$this->basket_exists($argArray['user_id']);
		$this->factor_insert(['temp'=>'1', 'subfactorId'=>$subfactorId, 'shopItemId'=>$argArray['shopItemId'], 'count'=>$argArray['count']]);
	}//--------------------------------------------------------------------------
   function basket_factorDelete($factorId) //hazfe yek factor mahsool az sabad(subfactor).
	{
      $this->factor_delete($factorId);
	}//--------------------------------------------------------------------------
   function basket_delete($userId) //hazfe hameye factorha az sabad(subfactor).
	{
		$subfactorId=basket_exists($userId);
		$this->subfactor_delete($subfactorId);
		$this->factor_deleteBySubfactorId($subfactorId);
	}//--------------------------------------------------------------------------
	function basket_toSubfactor() //tabdil sabad be subfactor be jahat pardakht va ... . (set temp=0)
	{
		$subfactorId=basket_exists($userId);
		$this->subfactor_setTemp($subfactorId, '0');
		$this->factor_setTempBySubfactorId($subfactorId, '0');
	}//--------------------------------------------------------------------------
}

?>
<?php

class cShopPriceSettings
{
   public $tbl_shopPriceSettings="shopPriceSettings";
   function get()
   {
      global $oDbq;
		$ret=@$oDbq->table($this->tbl_shopPriceSettings)->select()[0];
		if(!$ret)
		{
		   $oDbq->table($this->tbl_shopPriceSettings)->set("`pricePlus`=0")->insert();
			$ret=$oDbq->table($this->tbl_shopPriceSettings)->select()[0];
      }	
		return $ret;
   }//--------------------------------------------------------------------------
   function update ($pricePlus, $siteCommission, $marketerCommission)
	{
		global $oDbq;
      $oDbq->table($this->tbl_shopPriceSettings)->set("`pricePlus`={$pricePlus}, `siteCommission`={$siteCommission}, `marketerCommission`={$marketerCommission}")->update();	   
	}//-------------------------------------------------------------------------- 
}

?>
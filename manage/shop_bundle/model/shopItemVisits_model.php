<?php
   class cProductsItemVisits
   {
		private $tbl_filmItemVisits="filmItemVisits";
		private $tbl_filmItems="filmItems";
      public function ip()
      {
      	if (getenv('HTTP_CLIENT_IP')) {
      		$ip = getenv('HTTP_CLIENT_IP');
      	} else {
      		$ip = $_SERVER['REMOTE_ADDR'];
      	} return $ip;
      }//------------------------------------------------------------------------------------
      public function visits($id)
      {
			global $oDbq;
         $ip=$this->ip();
         $today=time();
         $yesterday=time() - (24 * 60 * 60);
         //$ret=$db->table("catItemsVisits")->fields("`ip`")->where("`ip`='$ip' AND `date` > $yesterday")->select();
         $ret=$oDbq->table($this->tbl_filmItemVisits)->fields("`ip`")->where("`ip`='{$ip}' AND `id`={$id} AND `date` > {$yesterday}")->select();
         if(count($ret) == 0)
         {
            $oDbq->table($this->tbl_filmItemVisits)->set("`id`={$id},`ip`='{$ip}',`date`={$today}")->insert();
            $count=$oDbq->table($this->tbl_filmItems)->fields("*")->where("`id`={$id}")->select()[0]->visits;
            $count++;
            $oDbq->table($this->tbl_filmItems)->set("`visits`={$count}")->where("`id`={$id}")->update();
         }
      }//-----------------------------------------------------------------------------------
      public function delOld($id)
      {
         global $oDbq;
         $yesterday=time() - (24 * 60 * 60);
         $oldYesterday=time() - (48 * 60 * 60);
         $oDbq->table($this->tbl_filmItemVisits)->where("`id`={$id} AND `date` <= {$oldYesterday}")->delete();
      }//------------------------------------------------------------------------------------
      public function getToday($id)
      {
         global $oDbq;
         $yesterday=time() - (24 * 60 * 60);
         $ret=$oDbq->table($this->tbl_filmItemVisits)->fields("*")->where("`id`={$id} AND `date` > {$yesterday}")->select();
         return $ret;
      }//------------------------------------------------------------------------------------
      public function getYesterday($id)
      {
         global $oDbq;
         $yesterday=time() - (24 * 60 * 60);
         $oldeYesterday=time() - (48 * 60 * 60);
         $ret=$oDbq->table($this->tbl_filmItemVisits)->fields("*")->where("`id`={$id} AND `date` <= {$yesterday} AND `date` > {$oldeYesterday}")->select();
         return $ret;
      }//------------------------------------------------------------------------------------
      public function countAllByIp($id)
      {
         global $oDbq;
         $ret=$oDbq->table($this->tbl_filmItemVisits)->fields("*")->where("`id`={$id}")->select();
         return count($ret);
      }//------------------------------------------------------------------------------------
      public function countAll($id)
      {
         global $oDbq;
         $count=$oDbq->table($this->tbl_filmItems)->fields("`visits`")->where("`id`={$id}")->select()[0]->visits;
         return $count;
      }//------------------------------------------------------------------------------------
   }
?>
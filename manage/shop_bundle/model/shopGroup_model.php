<?php

class cShopGroup
{
   public $tbl_shopGroup="shopGroup";
	
	private $ids="";
   function getAll($sortByRow=true,$parentId="",$active="",$limitStr="")
   {
      global $oDbq;
      $where="";
      if(intval($parentId) > -1 && strlen($parentId) > 0)
      {
         if($where=="") $where="`parentId`='{$parentId}'"; else $where.=" AND `parentId`='{$parentId}'";
      }
      if($active!="")
      {
         if($where=="") $where="`active`='{$active}'"; else $where.=" AND `active`='{$active}'";
      }
      if($sortByRow==true)$ret=$oDbq->table($this->tbl_shopGroup)->fields("*")->where($where)->orderBy("`row`");
      else if($sortByRow==false)$ret=$oDbq->table($this->tbl_shopGroup)->fields("*")->where($where);
      if($limitStr!=="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//--------------------------------------------------------------------------
   function get($id)
   {
      global $oDbq;
		$ret=@$oDbq->table($this->tbl_shopGroup)->fields("*")->where("`id`='{$id}'")->select()[0];
		return $ret;
   }//--------------------------------------------------------------------------
   function existsId($id)
   {
      global $oDbq;
      return count($oDbq->table($this->tbl_shopGroup)->fields("*")->where("`id`='{$id}'")->select());
   }//--------------------------------------------------------------------------	
	function eachId($id)
	{
		global $oDbq;
	   $id_=@$oDbq->table($this->tbl_shopGroup)->fields("`id`")->where("`parentId`='{$id}'")->select()[0]->id;
		if($id_)
		{
			$this->ids.=$id_ . ",";
			$this->eachId($id_);	
		}
		$ret=$this->ids;
		//$this->ids="";
      return rtrim($ret,",");		
	}//--------------------------------------------------------------------------
   function hasChilds($id)
   {
      global $oDbq;
      $ret=$oDbq->table($this->tbl_shopGroup)->fields("*")->where("`parentId`='{$id}'")->select();
      if(count($ret) > 0) return true; else return false; 
   }//--------------------------------------------------------------------------	
   function countChilds($id)
   {
      global $oDbq;
      $ret=$oDbq->table($this->tbl_shopGroup)->fields("*")->where("`parentId`='{$id}'")->select();
      return count($ret);
   }//--------------------------------------------------------------------------	
   function countChildItems($id)
   {
      global $oDbq;
      $ret=$oDbq->table($this->itemsTbl)->fields("*")->where("`otherSids` LIKE '%{$id}%' AND `active`='1'")->select();
      return count($ret);
   }//--------------------------------------------------------------------------	
   function delete($ids)
   {
      global $oDbq;
		$ids=rtrim($ids,",");
		$ids=explode(",",$ids);
		for($i=0;$i < count($ids);$i++)
		{
			$id=$ids[$i];
			$oDbq->table($this->tbl_shopGroup)->where("`id`='{$id}'")->delete();
		}
      return $ids; //return array		
   }//--------------------------------------------------------------------------
   function getChildsId($id)  //list hame menuha be soorate derakhty
   {
      $items=$this->gets(true,$id);
      $code="";
      if(count($items) > 0) 
      {
         foreach($items as $item)
         {
				$code.=$item->id . ",";
				$code.=$this->getChildsId($item->id);
         }
         return $code;
      }
      else
         return "";
   }//------------------------------------------------------------------------------------
	function setActive($id,$value)
	{
	  global $oDbq;
      $oDbq->table($this->tbl_shopGroup)->set("`active`={$value}")->where("id={$id}")->update(); 		
	}//--------------------------------------------------------------------------
    function insert ($array)
	{
		global $oDbq;
      $oDbq->table($this->tbl_shopGroup)->set("`id`={$array['id']}, `parentId`={$array['parentId']}, `title`='{$array['title']}', `comment`='{$array['comment']}', `row`={$array['row']}")->insert();	   
	}//--------------------------------------------------------------------------
   function update ($array)
	{
		global $oDbq;
      $oDbq->table($this->tbl_shopGroup)->set("`title`='{$array['title']}', `comment`='{$array['comment']}', `row`={$array['row']}")->where("`id`={$array['id']}")->update();	   
	}//-------------------------------------------------------------------------- 
}

?>
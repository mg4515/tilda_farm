<?php

class cShopProperty
{ 
   public $tbl_shopProperty="shopProperty";
   public $tbl_shopItemProperty="shopItemProperty";
   function getAll($limitStr="")
   {
      global $oDbq;
      $where="";
      $ret=$oDbq->table($this->tbl_shopProperty)->where($where);
      if($limitStr!=="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//--------------------------------------------------------------------------
   function get($id)
   {
      global $oDbq;
		$ret=@$oDbq->table($this->tbl_shopProperty)->fields("*")->where("`id`='{$id}'")->select()[0];
		return $ret;
   }//--------------------------------------------------------------------------
   function isWorkType1($id)
   {
      global $oDbq;
      $item=(array)$this->get($id);
		$str='';
		if($item->type==1)
		{
			for($i=1; $i <= 5; $i++)
			{
				$str.=$item["option{$i}Title"];
			}
			if($str != '') return true; else return false;
		}
      else
			return false;
   }//--------------------------------------------------------------------------	
   function delete($ids)
   {
      global $oDbq;
		$ids=rtrim($ids,",");
		$ids=explode(",",$ids);
		for($i=0;$i < count($ids);$i++)
		{
			$id=$ids[$i];
			$oDbq->table($this->tbl_shopProperty)->where("`id`='{$id}'")->delete();
		}
      return $ids; //return array		
   }//--------------------------------------------------------------------------
    function insert ($argArray)
	{
		global $oDbq;
		$id=isset($argArray['id']) ? $argArray['id'] : time();
		$option1Title=isset($argArray['option1Title']) ? $argArray['option1Title'] : '';
		$option2Title=isset($argArray['option2Title']) ? $argArray['option2Title'] : '';
		$option3Title=isset($argArray['option3Title']) ? $argArray['option3Title'] : '';
		$option4Title=isset($argArray['option4Title']) ? $argArray['option4Title'] : '';
		$option5Title=isset($argArray['option5Title']) ? $argArray['option5Title'] : '';
      $oDbq->table($this->tbl_shopProperty)->set("
																   `id`={$id},
																	`type`={$argArray['type']},
																	`title`='{$argArray['title']}',
																	`option1Title`='{$option1Title}',
																	`option2Title`='{$option2Title}',
																	`option3Title`='{$option3Title}',
																	`option4Title`='{$option4Title}',
																	`option5Title`='{$option5Title}'
																")->insert();	   
	}//--------------------------------------------------------------------------
   function update ($argArray)
	{
		global $oDbq;
		$option1Title=isset($argArray['option1Title']) ? $argArray['option1Title'] : '';
		$option2Title=isset($argArray['option2Title']) ? $argArray['option2Title'] : '';
		$option3Title=isset($argArray['option3Title']) ? $argArray['option3Title'] : '';
		$option4Title=isset($argArray['option4Title']) ? $argArray['option4Title'] : '';
		$option5Title=isset($argArray['option5Title']) ? $argArray['option5Title'] : '';
      $oDbq->table($this->tbl_shopProperty)->set("
																	`type`={$argArray['type']},
																	`title`='{$argArray['title']}',
																	`option1Title`='{$option1Title}',
																	`option2Title`='{$option2Title}',
																	`option3Title`='{$option3Title}',
																	`option4Title`='{$option4Title}',
																	`option5Title`='{$option5Title}'
																")->where("`id`={$argArray['id']}")->update();	    
	}//-------------------------------------------------------------------------- 
}

?>
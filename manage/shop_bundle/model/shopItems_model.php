<?php
include_once $oPath->manageDir('shop_bundle/model/shopPriceSettings_model.php');
class cShopItems
{
   private $tbl_shopItems="shopItems";
   private $tbl_shopPublication="shopPublication";
   private $tbl_shopItemLikes="shopItemLikes";
   private $tbl_shopItemOpine="shopItemOpine";
	
	function getAll($argArray)
   {
		global $oDbq;
		
		$userId=isset($argArray["userId"]) ? $argArray["userId"] : "";
		$groupId=isset($argArray["groupId"]) ? $argArray["groupId"] : "";
		$sortByRow=isset($argArray["sortByRow"]) ? $argArray["sortByRow"] : true;
		$sortBySell=isset($argArray["sortBySell"]) ? $argArray["sortBySell"] : true;
		$sortByVisits=isset($argArray["sortByVisits"]) ? $argArray["sortByVisits"] : false;
		$searchWord=isset($argArray["searchWord"]) ? $argArray["searchWord"] : "";
		$tagTitle=isset($argArray["tagTitle"]) ? $argArray["tagTitle"] : "";
		$isSpecial=isset($argArray["isSpecial"]) ? $argArray["isSpecial"] : "";
		$active=isset($argArray["active"]) ? $argArray["active"] : ""; //0, 1
		$confirm=isset($argArray["confirm"]) ? $argArray["confirm"] : ""; //0 dar entezar, 1 taeed shode, 2 taeed nashode
		$trash=isset($argArray["trash"]) ? $argArray["trash"] : "";
		//---
		$vazn=isset($argArray["vazn"]) ? $argArray["vazn"] : ""; //1:5kg , 2:10kg
		$type=isset($argArray["type"]) ? $argArray["type"] : ""; //1:dane boland , 2:dane shekasteh
		$kesht=isset($argArray["kesht"]) ? $argArray["kesht"] : ""; //kesht avval ya dovvom ya donoj. 1 ya 2 ya 3
		$kood=isset($argArray["kood"]) ? $argArray["kood"] : ""; //noe kood. organik ya heivany ya shimiyaey. 1 ya 2 ya 3
		//---
		$limitStr=isset($argArray["limitStr"]) ? $argArray["limitStr"] : "";	

      $where="";
      if($userId!=="")
      {
         if($where=="") $where="`userId`='{$userId}'"; else $where.=" AND `userId`='{$userId}'";
      }
      if($groupId!=="")
      {
         if($where=="") $where="`groupId` LIKE '%{$groupId}%'"; else $where.=" AND `groupId` LIKE '%{$groupId}%'";
      }
		if($searchWord !== "")
		{
			if($where=="") 
				$where="(`title` LIKE '%{$searchWord}%' OR `comment` LIKE '%{$searchWord}%' OR `keywords` LIKE '%{$searchWord}%' OR `city` LIKE '%{$searchWord}%')";
		   else
				$where.=" AND (`title` LIKE '%{$searchWord}%' OR `comment` LIKE '%{$searchWord}%' OR `keywords` LIKE '%{$searchWord}%' OR `city` LIKE '%{$searchWord}%')";
		}
		if($tagTitle !== "")
		{
			if($where=="") 
				$where="(`keywords` LIKE '%{$tagTitle}%')";
		   else
				$where.=" AND (`keywords` LIKE '%{$tagTitle}%')";
		}		
      if($active!=="")
      {
         if($where=="") $where="`active`='{$active}'"; else $where.=" AND `active`='{$active}'";
      }
      if($confirm!="")
      {
         if($where=="") $where="`confirm`='{$confirm}'"; else $where.=" AND `confirm`='{$confirm}'";
      }
      if($trash!=="")
      {
         if($where=="") $where="`trash`='{$trash}'"; else $where.=" AND `trash`='{$trash}'";
      }	

      //---
      if($vazn!=="")
      {
         if($where=="") $where="`vazn`='{$vazn}'"; else $vazn.=" AND `vazn`='{$vazn}'";
      }
      if($type!=="")
      {
         if($where=="") $where="`type`='{$type}'"; else $type.=" AND `type`='{$type}'";
      }
      if($kesht!=="")
      {
         if($where=="") $where="`kesht`='{$kesht}'"; else $kesht.=" AND `kesht`='{$kesht}'";
      }
      if($kood!=="")
      {
         if($where=="") $where="`kood`='{$kood}'"; else $kood.=" AND `kood`='{$kood}'";
      }	
		//---
		
      if($isSpecial!=="")
      {
         if($where=="") $where="`isSpecial`='{$isSpecial}'"; else $where.=" AND `isSpecial`='{$isSpecial}'";
      }				
		
      if($sortByRow==true)$ret=$oDbq->table($this->tbl_shopItems)->fields("*")->where($where)->orderBy("`id` DESC");
		else if($sortBySell==true) $ret=$oDbq->table($this->tbl_shopItems)->fields("*")->where($where)->orderBy("`countPay` DESC");      
		else if($sortByVisits==true) $ret=$oDbq->table($this->tbl_shopItems)->fields("*")->where($where)->orderBy("`visits` DESC");      
		else $ret=$oDbq->table($this->tbl_shopItems)->fields("*")->where($where);  
      
		if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//--------------------------------------------------------------------------
   function getByGroupId($groupId,$active='',$confirm='',$trash='') //=
   {
      global $oDbq;
		$where="`groupId`={$groupId}";
		if($active!=='')
			if($where=='') $where="`active`={$active}"; else $where.=" AND `active`={$active}";
		if($confirm!=='')
			if($where=='') $where="`confirm`={$confirm}"; else $where.=" AND `confirm`={$confirm}";
		if($trash!=='')
			if($where=='') $where="`trash`={$trash}"; else $where.=" AND `trash`={$trash}";
		$ret=$oDbq->table($this->tbl_shopItems)->fields("*")->where($where)->select();
      return $ret;
   }//--------------------------------------------------------------------------
   function getByGroupId2($groupId,$active='',$confirm='',$trash='') //like
   {
      global $oDbq;
		$where="`groupId` LIKE '%{$groupId}%'";
		if($active!=='')
			if($where=='') $where="`active`={$active}"; else $where.=" AND `active`={$active}";
		if($confirm!=='')
			if($where=='') $where="`confirm`={$confirm}"; else $where.=" AND `confirm`={$confirm}";
		if($trash!=='')
			if($where=='') $where="`trash`={$trash}"; else $where.=" AND `trash`={$trash}";
		$ret=$oDbq->table($this->tbl_shopItems)->fields("*")->where($where)->select();
      return $ret;
   }//--------------------------------------------------------------------------
   function getByUserId($userId,$active='',$confirm='',$trash='')
   {
      global $oDbq;
		$where="`userId`={$userId}";
		if($active!=='')
			if($where=='') $where="`active`={$active}"; else $where.=" AND `active`={$active}";
		if($confirm!=='')
			if($where=='') $where="`confirm`={$confirm}"; else $where.=" AND `confirm`={$confirm}";
		if($trash=='')
			if($where!=='') $where="`trash`={$trash}"; else $where.=" AND `trash`={$trash}";
		$ret=$oDbq->table($this->tbl_shopItems)->fields("*")->where($where)->select();
      return $ret;
   }//--------------------------------------------------------------------------		
	function get($id,$active='',$confirm='',$trash='')
   {
      global $oDbq;
		$where="`id`={$id}";
		if($active!=='')
			if($where=='') $where="`active`={$active}"; else $where.=" AND `active`={$active}";
		if($confirm!=='')
			if($where=='') $where="`confirm`={$confirm}"; else $where.=" AND `confirm`={$confirm}";
		if($trash!=='')
			if($where=='') $where="`trash`={$trash}"; else $where.=" AND `trash`={$trash}";
      $ret=@$oDbq->table($this->tbl_shopItems)->fields("*")->where($where)->select()[0];
      return $ret;
   }//--------------------------------------------------------------------------
	function get2($id,$usePricePlus=true,$active='',$confirm='',$trash='')
   {
      global $oDbq;
		$where="`id`={$id}";
		if($active!=='')
			if($where=='') $where="`active`={$active}"; else $where.=" AND `active`={$active}";
		if($confirm!=='')
			if($where=='') $where="`confirm`={$confirm}"; else $where.=" AND `confirm`={$confirm}";
		if($trash!=='')
			if($where=='') $where="`trash`={$trash}"; else $where.=" AND `trash`={$trash}";
      $ret=@$oDbq->table($this->tbl_shopItems)->fields("*")->where($where)->select('','assoc')[0];
		if($ret)
		{
			//price settings
			$oShopPriceSettings=new cShopPriceSettings();
			$shopPriceSettings=$oShopPriceSettings->get();
			
			//price settings 'pricePlus'
			if($usePricePlus==true)
			{
				$ret['price']+= $shopPriceSettings->pricePlus;
				if($ret['price'] < 0) $ret['price']=0;
			}
			
			//discount
			if($ret['notDiscount']=='1')
			{
				$ret['allowDiscount']=false;
				$ret['isDiscount']=false;
				$ret['priceByDiscount']= $ret['price'];
			}
			else if($ret['discount']=='0')
			{
				$ret['allowDiscount']=true;
				$ret['isDiscount']=false;
				$ret['priceByDiscount']= $ret['price'];
			}
			else
			{
				$ret['allowDiscount']=true;
				$ret['isDiscount']=true;
				if($ret['price'] > 0)
				   $ret['priceByDiscount']= $ret['price'] - (($ret['discount'] * $ret['price']) / 100);
				else
					$ret['priceByDiscount']= $ret['price'];
			}
			
			//isFree
			if($ret['price'] > 0 && $ret['isFree'] == '0')
				$ret['isFree']=false;
			else 
			{
				$ret['isFree']=true;	
				$ret['priceByDiscount']=0;
				$ret['allowDiscount']=false;
				$ret['isDiscount']=false;
			}

		   //price format
			$ret['price_str']=number_format($ret['price']);
			$ret['priceByDiscount_str']=number_format($ret['priceByDiscount']);

			//marketer Commission
			if($ret['isFree']==false)
			{
				//
			   $ret['marketerCommission']=($shopPriceSettings->marketerCommission * $ret['price']) / 100;
			   $ret['marketerCommission_str']=number_format($ret['marketerCommission']);
			   //sahme forooshande
				$ret['siteCommission']=($shopPriceSettings->siteCommission * $ret['price']) / 100;
			   $ret['siteCommission_str']=number_format($ret['siteCommission']);
				//sahme forooshande, agar mahsool be vasile bazaryab be foroosh beresad
				$ret['siteCommission_byMarketer']= ($shopPriceSettings->siteCommission * ($ret['price'] - $ret['marketerCommission'])) / 100;				
				$ret['siteCommission_byMarketer_str']= number_format($ret['siteCommission_byMarketer']);			
			}
			else
			{
			   $ret['marketerCommission']=0;
			   $ret['marketerCommission_str']=0;
			   $ret['siteCommission']=0;
			   $ret['siteCommission_str']=0;		
			   $ret['siteCommission_byMarketer']=0;				
			   $ret['siteCommission_byMarketer_str']=0;				
			}
		}
      return (object)$ret;
   }//--------------------------------------------------------------------------
	function calc($objItem, $usePricePlus=true)
   {
      global $oDbq;
		$ret=(array)$objItem;
		if($ret)
		{
			//price settings
			$oShopPriceSettings=new cShopPriceSettings();
			$shopPriceSettings=$oShopPriceSettings->get();
			
			//price settings 'pricePlus'
			if($usePricePlus==true)
			{
				$ret['price']+= $shopPriceSettings->pricePlus;
				if($ret['price'] < 0) $ret['price']=0;
			}
			
			//discount
			if($ret['discount']=='0')
			{
				$ret['allowDiscount']=true;
				$ret['isDiscount']=false;
				$ret['priceByDiscount']= $ret['price'];
			}
			else
			{
				$ret['allowDiscount']=true;
				$ret['isDiscount']=true;
				if($ret['price'] > 0)
				   $ret['priceByDiscount']= $ret['price'] - (($ret['discount'] * $ret['price']) / 100);
				else
					$ret['priceByDiscount']= $ret['price'];
			}
			
			//isFree
			if($ret['price'] > 0)
				$ret['isFree']=false;
			else 
			{
				$ret['isFree']=true;	
				$ret['priceByDiscount']=0;
				$ret['allowDiscount']=false;
				$ret['isDiscount']=false;
			}

		   //price format
			$ret['price_str']=number_format($ret['price']);
			$ret['priceByDiscount_str']=number_format($ret['priceByDiscount']);

			//marketer Commission
			if($ret['isFree']==false)
			{
				//
			   $ret['marketerCommission']=($shopPriceSettings->marketerCommission * $ret['price']) / 100;
			   $ret['marketerCommission_str']=number_format($ret['marketerCommission']);
			   //sahme forooshande
				$ret['siteCommission']=($shopPriceSettings->siteCommission * $ret['price']) / 100;
			   $ret['siteCommission_str']=number_format($ret['siteCommission']);
				//sahme forooshande, agar mahsool be vasile bazaryab be foroosh beresad
				$ret['siteCommission_byMarketer']= ($shopPriceSettings->siteCommission * ($ret['price'] - $ret['marketerCommission'])) / 100;				
				$ret['siteCommission_byMarketer_str']= number_format($ret['siteCommission_byMarketer']);			
			}
			else
			{
			   $ret['marketerCommission']=0;
			   $ret['marketerCommission_str']=0;
			   $ret['siteCommission']=0;
			   $ret['siteCommission_str']=0;		
			   $ret['siteCommission_byMarketer']=0;				
			   $ret['siteCommission_byMarketer_str']=0;				
			}
			
			//titleFa
			//if($ret['titleBefor']) $ret['titleFa']= $ret['titleBefor'] . ' ' . $ret['titleFa'];
			
		}
      return (object)$ret;
   }//--------------------------------------------------------------------------
	function delete($id,$userId="")
	{
		global $oDbq;
		if($userId!="") $where="`userId`={$userId} AND "; else $where="";
		$where2="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				$where2.="(`itemId`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
				if($i < count($ids) - 1) $where2.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_shopItems)->fields("*")->where("({$where})")->delete();
	   if($where != "" && $where2 != "") $oDbq->table($this->tbl_shopItemOpine)->fields("*")->where("({$where2})")->delete();
		if($where != "" && $where2 != "") $oDbq->table($this->tbl_shopItemLikes)->fields("*")->where("({$where2})")->delete();		
		return $ids;		
	}//---------------------------------------------------------------------------
	function trash($id,$userId="")
	{
		global $oDbq;
		if($userId!="") $where="`userId`={$userId} AND "; else $where="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_shopItems)->set("`trash`=1")->where("({$where})")->update();
		return $ids;		
	}//---------------------------------------------------------------------------
	function unTrash($id,$userId="")
	{
		global $oDbq;
		if($userId!="") $where="`userId`={$userId} AND "; else $where="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_shopItems)->set("`trash`=0")->where("({$where})")->update();
		return $ids;		
	}//---------------------------------------------------------------------------		
	public function getActive($id)
	{
		global $oDbq;
		$ret=@$oDbq->table($this->tbl_shopItems)->fields("`active`")->where("`id`={$id}")->select()[0]->active;
		if($ret=="1")
			return true;
		else
			return false;
	}//---------------------------------------------------------------------------------
	public function setActive($id,$value)
	{
		global $oDbq;
		$oDbq->table($this->tbl_shopItems)->set("`active`={$value}")->where("`id`={$id}")->update();
	}//---------------------------------------------------------------------------------
	public function getConfirm($id)
	{
		global $oDbq;
		$ret=@$oDbq->table($this->tbl_shopItems)->fields("`confirm`")->where("`id`={$id}")->select()[0]->confirm;
		if($ret=="1")
			return true;
		else if($ret=="2")
			return false;
	}//---------------------------------------------------------------------------------
	public function setConfirm($id,$value)
	{
		global $oDbq;
		$oDbq->table($this->tbl_shopItems)->set("`confirm`={$value}")->where("`id`={$id}")->update();
	}//---------------------------------------------------------------------------------
	public function getConfirmComment($id)
	{
		global $oDbq;
		$ret=@$oDbq->table($this->tbl_shopItems)->fields("`confirmComment`")->where("`id`={$id}")->select()[0]->confirmComment;
		if($ret=="1")
			return true;
		else
			return false;
	}//---------------------------------------------------------------------------------
	public function setConfirmComment($id,$value)
	{
		global $oDbq;
		$oDbq->table($this->tbl_shopItems)->set("`confirmComment`='{$value}'")->where("`id`={$id}")->update();
	}//---------------------------------------------------------------------------------
	public function getTrash($id)
	{
		global $oDbq;
		$ret=@$oDbq->table($this->tbl_shopItems)->fields("`trash`")->where("`id`={$id}")->select()[0]->active;
		if($ret=="1")
			return true;
		else
			return false;
	}//---------------------------------------------------------------------------------
	public function setTrash($id,$value)
	{
		global $oDbq;
		$oDbq->table($this->tbl_shopItems)->set("`trash`={$value}")->where("`id`={$id}")->update();
	}//---------------------------------------------------------------------------------
	public function isFree($id)
	{
		global $oDbq;
		$ret=@$oDbq->table($this->tbl_shopItems)->fields("`price`")->where("`id`={$id}")->select()[0];
		if($ret->price > 0)
			return false;
		else 
			return true;
	}//---------------------------------------------------------------------------------
   function setVisits($id,$value)
   {
      global $oDbq;
		if($value=='+') $set="`visits`=`visits` + 1";
		else if($value=='-') $set="`visits`=`visits` - 1";
		else $set="`visits`='{$value}'";
		$oDbq->table($this->tbl_shopItems)->set($set)->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------	
   function getVisits($id)
   {
      global $oDbq;
		return @$oDbq->table($this->tbl_shopItems)->fields("`visits`")->where("`id`='{$id}'")->select()[0]->visits;
   }//------------------------------------------------------------------------------------		
	function insert($array)
	{
      global $oDbq;
		$id=@$array['id'] ? $array['id'] : time();
		$userId=@$array['userId'] ? $array['userId'] : 0;
		$discount=@$array['discount'] ? $array['discount'] : 0;	
		//---
		$vazn=@$array['vazn'] ? $array['vazn'] : 1; //5 or 10 kg.
		$type=@$array['type'] ? $array['type'] : 1; //1: dane boland 2:dane shekasteh.
		$kesht=@$array['kesht'] ? $array['kesht'] : 1; //kesht avval ya dovvom ya donoj. value: 1 ya 2 ya 3.
		$kood=@$array['kood'] ? $array['kood'] : 1; //noe kood. organik ya heivany ya shimiyaey. value: 1 ya 2 ya 3
		$atr=@$array['atr'] ? $array['atr'] : 1; //booye atr. 1 ta 10
		$tam=@$array['tam'] ? $array['tam'] : 1; //tam va mazzeh. 1 ta 10
		$rey=@$array['rey'] ? $array['rey'] : 1; //rey keshidan. 1 ta 10
		$saleKesht=@$array['saleKesht'] ? $array['saleKesht'] : 399; //sale kesht. 1399 ya 99
		//---
		$isSpecial=@$array['isSpecial'] ? $array['isSpecial'] : 0;
		$confirm=@$array['confirm'] ? $array['confirm'] : 1;		
		$insertById=@$array['insertById'] ? $array['insertById'] : 0;
		$insertDate=time();
      $oDbq->table($this->tbl_shopItems)->set("`id`={$id},
		                                       `userId`={$userId},
		                                       `groupId`='{$array['groupId']}',
		                                       `title`='{$array['title']}',												
		                                       `city`='{$array['city']}',										
		                                       `count`='{$array['count']}',										
															`comment`='{$array['comment']}',												
															`keywords`='{$array['keywords']}',												
															`price`='{$array['price']}',																							
															`discount`='{$discount}',
		                                       `vazn`={$vazn},
		                                       `type`={$type},															
															`kesht`={$kesht},												
															`kood`={$kood},												
															`atr`={$atr},												
															`tam`={$tam},												
															`rey`={$rey},												
															`saleKesht`='{$saleKesht}',												
															`isSpecial`={$isSpecial},
		                                       `confirm`={$confirm},
		                                       `insertById`={$insertById},
		                                       `insertDate`={$insertDate}
		                                      ")->insert();	
      return $id;		
	}//--------------------------------------------------------------------------
   function update($array)
	{
      global $oDbq;
		$discount=@$array['discount'] ? $array['discount'] : 0;
		$isSpecial=@$array['isSpecial'] ? $array['isSpecial'] : 0;	
		//---
		$vazn=@$array['vazn'] ? $array['vazn'] : 1; //5 or 10 kg.
		$type=@$array['type'] ? $array['type'] : 1; //1: dane boland 2:dane shekasteh.
		$kesht=@$array['kesht'] ? $array['kesht'] : 1; //kesht avval ya dovvom ya donoj. value: 1 ya 2 ya 3.
		$kood=@$array['kood'] ? $array['kood'] : 1; //noe kood. organik ya heivany ya shimiyaey. value: 1 ya 2 ya 3
		$atr=@$array['atr'] ? $array['atr'] : 1; //booye atr. 1 ta 10
		$tam=@$array['tam'] ? $array['tam'] : 1; //tam va mazzeh. 1 ta 10
		$rey=@$array['rey'] ? $array['rey'] : 1; //rey keshidan. 1 ta 10
		$saleKesht=@$array['saleKesht'] ? $array['saleKesht'] : 1; //sale kesht. 1399 ya 99
		//---		
		$updateById=@$array['updateById'] ? $array['updateById'] : 0;
		$updateDate=time();
      $oDbq->table($this->tbl_shopItems)->set("`groupId`='{$array['groupId']}',
																`title`='{$array['title']}',												
																`city`='{$array['city']}',												
																`count`={$array['count']},												
																`comment`='{$array['comment']}',												
																`keywords`='{$array['keywords']}',												
																`price`='{$array['price']}',																							
																`discount`='{$discount}',
																`vazn`={$vazn},												
																`type`={$vazn},												
																`kesht`={$kesht},												
																`kood`={$kood},												
																`atr`={$atr},												
																`tam`={$tam},												
																`rey`={$rey},												
																`saleKesht`='{$saleKesht}',																
																`isSpecial`={$isSpecial},
																`updateById`={$updateById},
																`updateDate`={$updateDate}
															")->where("`id`={$array['id']}")->update();	
	}//--------------------------------------------------------------------------
   function updateProperties($array)
	{
      global $oDbq;
		$kesht=@$array['kesht'] ? $array['kesht'] : 1; //kesht avval ya dovvom ya donoj. value: 1 ya 2 ya 3.
		$kood=@$array['kood'] ? $array['kood'] : 1; //noe kood. organik ya heivany ya shimiyaey. value: 1 ya 2 ya 3
		$atr=@$array['atr'] ? $array['atr'] : 1; //booye atr. 1 ta 10
		$tam=@$array['tam'] ? $array['tam'] : 1; //tam va mazzeh. 1 ta 10
		$rey=@$array['rey'] ? $array['rey'] : 1; //rey keshidan. 1 ta 10
		$saleKesht=@$array['saleKesht'] ? $array['saleKesht'] : 1; //sale kesht. 1399 ya 99
		//---		
		$updateById=@$array['updateById'] ? $array['updateById'] : 0;
		$updateDate=time();
      $oDbq->table($this->tbl_shopItems)->set("`kesht`={$kesht},												
															  `kood`={$kood},												
															  `atr`={$atr},												
															  `tam`={$tam},												
															  `rey`={$rey},												
															  `saleKesht`='{$saleKesht}',																
															  `isSpecial`={$isSpecial},
															  `updateById`={$updateById},
															  `updateDate`={$updateDate}
															")->where("`id`={$array['id']}")->update();	
	}//--------------------------------------------------------------------------	
	public function likes_getAll($itemId,$userId="",$searchWord="",$limitStr="")
	{
		global $oDbq;
		$whereSearch="";
		if($searchWord !== "")
		{
			$oUsers=new cUser();
			$users=$oUsers->gets("",$searchWord);
			foreach($users as $user)
			{
				if($whereSearch != "") $whereSearch.=" OR ";
			   $whereSearch.="`userId`={$user->id}";	
			}
			if($whereSearch!="") $whereSearch=" AND ({$whereSearch})";
		}
		if($userId!=="")
		   $ret=$oDbq->table($this->tbl_shopItemLikes)->fields("*")->where("`itemId`={$itemId} AND `userId`={$userId}{$whereSearch}");
		else
		   $ret=$oDbq->table($this->tbl_shopItemLikes)->fields("*")->where("`itemId`={$itemId}{$whereSearch}");
	   if($limitStr!="") return $ret->limit($limitStr)->select();
		else return $ret->select();
	}//---------------------------------------------------------------------------------
   function likes_delete($id,$itemId="")
   {
      global $oDbq;
		if($itemId!=="")$where="`itemId`={$itemId} AND "; else $where="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}
		$oDbq->table($this->tbl_shopItemLikes)->fields("*")->where("({$where})")->delete();
   }//--------------------------------------------------------------------------
   function likes_deleteByUserId($userId)
   {
      global $oDbq;
		$oDbq->table($this->tbl_shopItemLikes)->where("`userId`='{$userId}'")->delete();
   }//--------------------------------------------------------------------------
   function likes_deleteByItemId($itemId)
   {
      global $oDbq;
		$oDbq->table($this->tbl_shopItemLikes)->where("`itemId`='{$itemId}'")->delete();
   }//--------------------------------------------------------------------------	
	function likes_check($itemId,$userId)
	{
	   global $oDbq;
   	$ret=@$oDbq->table($this->tbl_shopItemLikes)->fields("*")->where("`itemId`='{$itemId}' AND `userId`='{$userId}'")->select()[0];
		if($ret) return $ret->id;
	}//--------------------------------------------------------------------------
   function likes_insert($itemId,$userId)
	{
      global $oDbq;
		if(!$this->likes_check($itemId,$userId))
		{
			$id=time();
			$oDbq->table($this->tbl_shopItemLikes)->fields("`id`={$id},
																`itemId`='{$itemId}',
																`userId`='{$userId}'
															  ")->insert();	
      }														  
	}//--------------------------------------------------------------------------		

   function opine_getAll($itemId="",$confirm="",$checkOut="",$limitStr="")
   {
      global $oDbq;
      $where="";
      if($itemId!=="")
      {
         if($where=="") $where="`itemId` = '{$itemId}'"; else $where.=" AND `itemId`='{$itemId}'";
      }
      if($confirm!=="")
      {
         if($where=="") $where="`confirm`='{$confirm}'"; else $where.=" AND `confirm`='{$confirm}'";
      }
      if($checkOut!=="")
      {
         if($where=="") $where="`checkOut`='{$checkOut}'"; else $where.=" AND `checkOut`='{$checkOut}'";
      }
		
      $ret=$oDbq->table($this->tbl_shopItemOpine)->fields("*")->where($where);
      if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//------------------------------------------------------------------------------------	
   function opine_getByUserId($userId="",$confirm="",$checkOut="",$limitStr="")
   {
      global $oDbq;
      $where="";
      if($userId!="")
      {
         if($where=="") $where="`userId` = '{$userId}'"; else $where.=" AND `userId`='{$userId}'";
      }
      if($confirm!="")
      {
         if($where=="") $where="`confirm`='{$confirm}'"; else $where.=" AND `confirm`='{$confirm}'";
      }
      if($checkOut!="")
      {
         if($where=="") $where="`checkOut`='{$checkOut}'"; else $where.=" AND `checkOut`='{$checkOut}'";
      }
		
      $ret=$oDbq->table($this->tbl_shopItemOpine)->fields("*")->where($where);
      if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//------------------------------------------------------------------------------------	
   function opine_get($id,$itemId="")
   {
      global $oDbq;
		if($itemId!="") $where=" AND `itemId`={$itemId}"; else $where="";
		return $oDbq->table($this->tbl_shopItemOpine)->fields("*")->where("`id`={$id}{$where}")->select()[0];
   
	}//------------------------------------------------------------------------------------   
	function opine_delete($id)
   {
      global $oDbq;
		$where="";
		$ids=explode(",",rtrim($id,","));
		$ids2=[];
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
				$ids2[]=$selectedId;
			}
		}
		if($where) $oDbq->table($this->tbl_shopItemOpine)->fields("*")->where("({$where})")->delete();
		return $ids2;
   }//------------------------------------------------------------------------------------
	function opine_deleteByItemId($itemId,$retIds=true)
	{
		global $oDbq;
		if($retIds)
		   $ret=$oDbq->table($this->tbl_shopItemOpine)->fields('`id`')->where("`itemId`={$itemId}")->select();
	   else
			$ret=[];
		$oDbq->table($this->tbl_shopItemOpine)->where("`itemId`={$itemId}")->delete();
	   return $ret;
	}//------------------------------------------------------------------------------------
   function opine_insert($userId,$itemId)
   {
      global $oDbq;
		$id=time();
		$oDbq->table($this->tbl_shopItemOpine)->set("`id`='{$id}',`itemId`='{$itemId}',`userId`='{$userId}'")->insert();
		return $id;
   }//------------------------------------------------------------------------------------
   function opine_setConfirm($id,$value)
   {
      global $oDbq;
		$oDbq->table($this->tbl_shopItemOpine)->set("`confirm`='{$value}'")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------	
	function opine_getConfirm($id)
	{
		global $oDbq;
		$ret=@$oDbq->table($this->tbl_shopItemOpine)->fields("`confirm`")->where("`id`={$id}")->select()[0];
		if($ret)
		{
			if($ret->confirm=="1")
				return true;
			else
				return false;
		}
		else
		   return false;
	}//---------------------------------------------------------------------------------	
   function opine_setCheckOut($id,$value)
   {
      global $oDbq;
		$oDbq->table($this->tbl_shopItemOpine)->set("`checkOut`='{$value}'")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------ 
}

?>
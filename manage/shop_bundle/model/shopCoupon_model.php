<?php
require_once $oPath->manageDir("shop_bundle/model/shopPriceSettings_model.php");
require_once $oPath->manageDir("shop_bundle/model/shopItems_model.php");
class cShopCoupon
{
   private $tbl_shopCoupon="shopCoupon";
	
	function getAll($argArray)
   {
		global $oDbq;
		
		$sortByDate=isset($argArray["sortByDate"]) ? $argArray["sortByDate"] : false;
		$searchWord=isset($argArray["searchWord"]) ? $argArray["searchWord"] : "";
		$limitStr=isset($argArray["limitStr"]) ? $argArray["searchWord"] : "";	

      $where="";	
		if($searchWord !== "")
		{
			if($where=="") 
				$where="(`title` LIKE '%{$searchWord}%' OR `discount` LIKE '%{$searchWord}%')";
		   else
				$where=" AND (`title` LIKE '%{$searchWord}%' OR `discount` LIKE '%{$searchWord}%')";
		}				
		
      if($sortByDate==true)$ret=$oDbq->table($this->tbl_shopCoupon)->fields("*")->where($where)->orderBy("`startDate` DESC");
		else $ret=$oDbq->table($this->tbl_shopCoupon)->fields("*")->where($where)->orderBy("`id` DESC");  
      
		if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//--------------------------------------------------------------------------	
	function get($id)
   {
      global $oDbq;
      $ret=@$oDbq->table($this->tbl_shopCoupon)->fields("*")->where("`id`={$id}")->select()[0];
      return $ret;
   }//--------------------------------------------------------------------------
	function getByTitle($title)
   {
      global $oDbq;
      $ret=@$oDbq->table($this->tbl_shopCoupon)->fields("*")->where("`title`='{$title}'")->select()[0];
      return $ret;
   }//--------------------------------------------------------------------------	
	function delete($id)
	{
		global $oDbq;
		$ids=explode(",",rtrim($id,","));
		$where='';
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_shopCoupon)->fields("*")->where("({$where})")->delete();	
		return $ids;		
	}//---------------------------------------------------------------------------	
   function insert($array)
	{
      global $oDbq;
		$id=time();
		$insertById=isset($array['insertById']) ? $array['insertById'] : 0;
		$insertDate=time();
      $oDbq->table($this->tbl_shopCoupon)->set("`id`={$id},
		                                       `title`='{$array['title']}',
															`discount`={$array['discount']},
		                                       `maxDiscount`={$array['maxDiscount']},
		                                       `startDate`={$array['startDate']},
		                                       `endDate`={$array['endDate']},
															`comment`='{$array['comment']}',
		                                       `insertById`={$insertById},
		                                       `insertDate`={$insertDate}
		                                    ")->insert();	
      return $id;		
	}//--------------------------------------------------------------------------
   function update($array)
	{
      global $oDbq;
		$updateById=isset($array['updateById']) ? $array['updateById'] : 0;
		$updateDate=time();
      $oDbq->table($this->tbl_shopCoupon)->set("
															`discount`={$array['discount']},
		                                       `maxDiscount`={$array['maxDiscount']},
		                                       `startDate`={$array['startDate']},
		                                       `endDate`={$array['endDate']},
															`comment`='{$array['comment']}',	
															`updateById`={$updateById},
															`updateDate`={$updateDate}
														")->where("`id`={$array['id']}")->update();	
	}//--------------------------------------------------------------------------
	function isAllow($title, $shopItemId)
	{
		$item=@$this->getByTitle($title);
		if(!$item) return false;
		//---
		if($item->endDate < time()) return false; //expiration status
		//---
		if($item->discount == 0) return false;
		//---
		$oShopItems=new cShopItems();
		$shopItem= $oShopItems->get2($shopItemId);
		$price=$shopItem->price;
		$couponDiscount=($item->discount * $price) / 100;
		if($couponDiscount > $item->maxDiscount)
			$price=$price - $item->maxDiscount;
		else
			$price=$price - $couponDiscount;		
		return ['id'=>$item->id ,'price'=>$price];
	}
}

?>
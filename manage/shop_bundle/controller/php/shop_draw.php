<?php
   include_once $oPath->manageDir("shop_bundle/model/shopGroup_model.php");
   include_once $oPath->manageDir("shop_bundle/model/shopItems_model.php");
   include_once $oPath->manageDir("shop_bundle/model/shopProperty_model.php");
   include_once $oPath->manageDir("shop_bundle/model/shopFactor_model.php");
   include_once $oPath->manageDir("users_bundle/model/users_model.php");
   include_once $oPath->manageDir("jdf.php");

	class cShopDraw
	{
		//for index, or other
		function groupLink($class)
		{
			global $oPath;
			$oShopGroup=new cShopGroup();
			$ret=$oShopGroup->getAll(true,"","1");
			$count=count($ret);
			$code='';
			for($i=0;$i < $count;$i++)
			{
				$code.="<a class='{$class}' href='items?g={$ret[$i]->id}'>" . $ret[$i]->title . "</a>";
			}
			return $code;
		}//------------------------------------------------------------------------------------		
		
		//for index
		function itemsDraw_forIndex($type='byNew',$count=4) 
		{
			global $oPath;
			$oShopItems=new cShopItems();
			$oShopGroup=new cShopGroup();
			$oUsers=new cUsers();
			if($type=='byNew')
			   $ret=$oShopItems->getAll(['sortByRow'=>true, 'active'=>1, 'confirm'=>1, 'trash'=>0, 'limitStr'=>$count]);
			else if($type=='byTopSell')   
				$ret=$oShopItems->getAll(['sortBySell'=>true, 'active'=>1, 'confirm'=>1, 'trash'=>0, 'limitStr'=>$count]);
			
			$count=count($ret);
			$code='';
			for($i=0;$i < $count;$i++)
			{
				$item=$oShopItems->calc($ret[$i]);
			
				//image info
				if(file_exists($oPath->manageDir("shop_bundle/data/images/item0_{$item->id}.jpg")))
				{
					$img="<img class='default-img' src='" . $oPath->manage("shop_bundle/data/images/item0_{$item->id}.jpg") ."' alt='{$item->title}' style='' />";
				   $imgSrc=$oPath->manage("shop_bundle/data/images/item0_{$item->id}.jpg");
				}
				else
				{
					$img="";
					$imgSrc=$oPath->asset("default/images/noImage.gif");
				}
				
				//user or seller
				if($item->userId)
				{
					$user=$oUsers->get($item->userId);
					$userTitle=@$user->fName . ' ' . @$user->lName;
					if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$user->id}.jpg")))
						$userImgSrc=$oPath->manage("users_bundle/data/images/user_{$user->id}.jpg");
					else
						$userImgSrc=$oPath->asset("default/images/user_larg.png");
				}
				//group info
				$group=@$oShopGroup->get($item->groupId)->title;						

            //price
				$priceStatus='';
				if($item->discount > 0)
				{
					$priceOld=$item->price_str . ' تومان';
					$price = $item->priceByDiscount_str . ' تومان';
				}
				else
				{
					$priceOld='';
					$price = $item->price_str . ' تومان';					
				}

				//like
				$likeCount=count($oShopItems->likes_getAll($item->id));
				if(isset($_SESSION['user_id']))
				{
					$likeId=$oShopItems->likes_check($item->id,@$_SESSION['user_id']);
					if($likeId) $heart='icon-heart'; else $heart='icon-heart-o'; 
				}
				else
					$heart='icon-heart-o';
				
				//visits count
				$visitCount=$item->visits;
				
				$code.="
			   <aside class='col-md-3 mb-3 mb-md-0'>
				   <div class='card border-0 overflow-hidden product-card'>
						<!-- Header -->
						<div class='card-header bg-transparent border-0 p-0 position-relative background' style='background-image:url({$imgSrc});'>
						   <!-- Product Image -->
						   <img src='{$imgSrc}' class='card-img-top' alt='{$item->title}' />

						   <!-- Add to Favorites -->
						   <button class='add-favorites btn position-absolute shadow-none' type='button' >
							   <i class='icon-heart-o text-white'></i>
						   </button>

						   <!-- Product Seller -->
						   <div class='product-seller px-2 d-flex flex-row align-items-end position-absolute'>
							   <img class='d-block float-right rounded-sm seller-img' src='{$userImgSrc}' alt='{$userTitle}' />
							   <span class='d-block float-right seller-name text-right ir-b c-dark fs-small pr-2'>{$userTitle}</span>
						   </div>

						   <a href='items?s={$item->city}' class='text-decoration-none ir-r text-white text-center fs-small position-absolute product-location'>{$item->city}</a>
						</div>
						<div class='card-body px-2 pb-2'>
						   <a class='text-decoration-none' href='item?i={$item->id}'>
							   <!-- Title -->
							   <h5 class='card-title ir-r c-dark fs-regular text-right'>{$item->title}</h5>
								
							   <!-- Price -->
							   <div class='price clearfix w-100'>
								   <p class='currently-price d-block float-right ir-r ml-2 text-success'>{$price}</p>
								   <p class='last-price d-block float-right ir-r text-decoration-line text-danger'>{$priceOld}</p>
							   </div>

								<!-- Stars -->
								<div class='stars d-flex flex-row justify-content-start'>
								   <i class='c-star ml-1 icon-star-o'></i>
									<i class='c-star ml-1 icon-star-o'></i>
									<i class='c-star ml-1 icon-star'></i>
									<i class='c-star ml-1 icon-star'></i>
									<i class='c-star ml-0 icon-star'></i>
								</div>
						   </a>
						</div>
				   </div>
			   </aside>			
				";
			}
			return $code;
		}//------------------------------------------------------------------------------------

      public function item($id)
		{
			global $oPath;
			$oShopItems=new cShopItems();
			$oShopGroup=new cShopGroup();
			$oShopProperty=new cShopProperty();
			$oUsers=new cUsers();
			
			//item
			$item=$oShopItems->get($id,1);
			$item=(array)$oShopItems->calc($item);
			if($item)
			{
				//image info -----
				$imgLarg=[];
				$imgThumb=[];
				$imgDir=[];
				if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Larg_{$item['id']}.jpg")))
					$item['image']=$oPath->manage("shop_bundle/data/images/item0Larg_{$item['id']}.jpg");
				else
					$item['image']=$oPath->asset("default/images/noImage.gif");
				
				for($i=0; $i<=5; $i++)
				{
					$imgLarg[]=$oPath->manage("shop_bundle/data/images/item{$i}Larg_{$item['id']}.jpg");
					$imgThumb[]=$oPath->manage("shop_bundle/data/images/item{$i}Thumb_{$item['id']}.jpg");
					$imgDir[]=$oPath->manageDir("shop_bundle/data/images/item{$i}Thumb_{$item['id']}.jpg");
				}
				$count=count($imgThumb);
				$imagesImg='';
				for($i=0;$i < $count; $i++)
				{
					if(file_exists($imgDir[$i]))
					{
						$imagesImg.="<a href='{$imgThumb[$i]}'><img src='{$imgLarg[$i]}' alt=''></a>";
					}
				}
				$item['images_html']="
				<div class='sp-wrap w-100 border-0 p-0 m-0 bg-transparent'>
					{$imagesImg}
				</div>
				";
				
				//group or groups -----
				$groupId=explode('|', $item['groupId'])[0];
				$group=$oShopGroup->get($groupId);
				$item['groupTitle']=$group->title;
				
            //price
				if($item['discount'] > 0)
				{
					$priceOld=$item['price_str'] . ' تومان';
					$price = $item['priceByDiscount_str'] . ' تومان';
				}
				else
				{
					$priceOld='';
					$price = $item['price_str'] . ' تومان';					
				}
				$item['price']=$price;
				$item['priceOld']=$priceOld;
					
				//vazn
				$arryVaznTitle=[1=>'5 کیلو', 2=>'10 کیلو'];
				$item['vaznTitle']= $arryVaznTitle[$item['vazn']];					
					
				//property
				   //kesht
				   $arryKeshtTitle=[1=>'کشت اول', 2=>'کشت دوم', 3=>'دونوج'];
					$arryKeshtImage=[
						1=>$oPath->manage('shop_bundle/data/images/properties/kesht_aval.svg'),
						2=>$oPath->manage('shop_bundle/data/images/properties/kesht_dovom.svg'),
						3=>$oPath->manage('shop_bundle/data/images/properties/kesht_donoj.svg')
					];
				   $item['property_keshtTitle']= $arryKeshtTitle[$item['kesht']];
				   $item['property_keshtImage']= $arryKeshtImage[$item['kesht']];
					//type
					$arryTypeTitle=[1=>'دانه بلند', 2=>'دانه شکسته'];
					$item['property_typeTitle']= $arryTypeTitle[$item['type']];
					//tam
					$tamTitle="طعم";
				   $item['property_tamTitle']= "{$tamTitle} : {$item['type']}/10";
				   $item['property_tamImage']= $oPath->manage('shop_bundle/data/images/properties/tam.svg');
					//rey
					$reyTitle="ری کشیدن";
				   $item['property_reyTitle']= "{$tamTitle} : {$item['type']}/10";
				   $item['property_reyImage']= $oPath->manage('shop_bundle/data/images/properties/tam.svg');
					//atr
					$atrTitle="عطر";
				   $item['property_atrTitle']= "{$atrTitle} : {$item['atr']}/10";
				   $item['property_atrImage']= $oPath->manage('shop_bundle/data/images/properties/atr.svg');
					//saleKesht
					$saleKeshtTitle="برداشت";
				   $item['property_saleKeshtTitle']= "{$saleKeshtTitle} {$item['saleKesht']}";
				   $item['property_saleKeshtImage']= $oPath->manage('shop_bundle/data/images/properties/saleKesht.svg');						
				
				//tags -----
				$keywords=explode('|',$item['keywords']);
				$count=count($keywords);
				$tags='';
				for($i=0;$i < $count;$i++)
				{
					$tagTitle=str_replace(' ','_',$keywords[$i]);
					$tags.="<li><a href='items?tag={$tagTitle}'>{$keywords[$i]}</a></li>";
				}
				$item['tags_li']=$tags;
				
				//opine -----
				$opine=$oShopItems->opine_getAll($item['id'],1);
				$count=count($opine);
				$opineDiv='';
				for($i=0;$i < $count;$i++)
				{
					$user=$oUsers->get($opine[$i]->userId);
					if($user)
					{
						if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$user->id}.jpg")))
							$userImage=$oPath->manage("users_bundle/data/images/user_{$user->id}.jpg");
						else
							$userImage=$oPath->asset("default/images/user_larg.png");
						$opineDate=jDate('Y/m/d',$opine[$i]->id);
						$opineContent=@file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemOpineContent_{$opine[$i]->id}.html"));
						$opineAnswer=@file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$opine[$i]->id}.html"));
						if($opineAnswer) $opineAnswer='<hr>پاسخ :<br>' . $opineAnswer;
						$opineDiv.="
						<div class='single-comment'>
							<img src='{$userImage}' alt='#'>
							<div class='content'>
								<h4>{$user->fName} {$user->lName}<span>{$opineDate}</span></h4>
								<p>{$opineContent}{$opineAnswer}</p>
							</div>
						</div>
						";
					}
				}
				$item['opineCount']=$count;
				$item['opine_div']=$opineDiv;
				
				//user
				if($item['userId'] > 0)
				{
					$user=$oUsers->get($item['userId']);
					$item['seller_title']=$user->fName . ' ' . $user->lName;
					$item['seller_comment']=$user->description;
					if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$user->id}.jpg")))
						$item['seller_image']=$oPath->manage("users_bundle/data/images/user_{$user->id}.jpg");
					else
						$item['seller_image']=$oPath->asset("default/images/user_larg.png");
				}
				else
				{
					$item['seller_title']=$user->fName . ' ' . $user->lName;
					$item['seller_comment']=$user->description;
					$item['seller_image']=$oPath->asset("default/images/user_larg.png");					
				}				
				
				//mortabet ha -----
				$mortabetha=$oShopItems->getAll(['trash'=>0, 'active'=>1, 'confirm'=>1, 'limitStr'=>10]);
				$count=count($mortabetha);
				$mortabetDiv='';
				for($i=0;$i < $count;$i++)
				{
					$mortabet=$mortabetha[$i];
					if($mortabet->id != $item['id']) //check exists
					{
						//IMAGE
						if(file_exists($oPath->manageDir("shop_bundle/data/images/item0_{$mortabet->id}.jpg")))
							$mortabetImg=$oPath->manage("shop_bundle/data/images/item0_{$mortabet->id}.jpg");
						else
							$mortabetImg=$oPath->asset("default/images/noImage.gif");
						
						//user or seller
						$user=@$oUsers->get($mortabet->userId);
						$mortabetUserTitle=@$user->fName . ' ' . @$user->lName;
						if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$user->id}.jpg")))
							$mortabetUserImgSrc=$oPath->manage("users_bundle/data/images/user_{$user->id}.jpg");
						else
							$mortabetUserImgSrc=$oPath->asset("default/images/user_larg.png");
						
						//price
						if($mortabet->discount > 0)
						{
							$mortabetPriceOld=$mortabet->price_str . ' تومان';
							$mortabetPrice = $mortabet->priceByDiscount_str . ' تومان';
						}
						else
						{
							$mortabetPriceOld='';
							$mortabetPrice = $mortabet->price_str . ' تومان';					
						}						
						
						$mortabetDate=jDate("Y/m/d",$mortabet->id);
						$mortabetDiv.="
						<div class='item'>
							<div class='card border-0 overflow-hidden product-card'>
								<!-- Header -->
								<div class='card-header bg-transparent border-0 p-0 position-relative'>
									<!-- Product Image -->
									<img src='{$mortabetImg}' class='card-img-top' alt='برنج' />

									<!-- Add to Favorites -->
									<button class='add-favorites btn position-absolute shadow-none' type='button'>
										<i class='icon-heart-o text-white'></i>
									</button>

									<!-- Product Seller -->
									<div class='product-seller px-2 d-flex flex-row align-items-end position-absolute'>
										<img class='d-block float-right rounded-sm seller-img' src='{$mortabetUserImgSrc}' alt='{$mortabetUserTitle}' />
										<span class='d-block float-right seller-name text-right ir-b c-dark fs-small pr-2'>{$mortabetUserTitle}</span>
									</div>

									<a href='./products.html' class='text-decoration-none ir-r text-white text-center fs-small position-absolute product-location'>{$mortabet->city}</a>
								</div>
								<div class='card-body px-2 pb-2'>
									<a class='text-decoration-none' href='item?i={$mortabet->id}'>
										<!-- Title -->
										<h5 class='card-title ir-r c-dark fs-regular text-right'>{$mortabet->title}</h5>
										<!-- Price -->
										<div class='price clearfix w-100'>
											<p class='currently-price d-block float-right ir-r ml-2 text-success'>{$mortabetPrice}</p>
											<p class='last-price d-block float-right ir-r text-decoration-line text-danger'>{$mortabetPriceOld}</p>
										</div>

										<!-- Stars -->
										<div class='stars d-flex flex-row justify-content-start'>
											<i class='c-star ml-1 icon-star-o'></i>
											<i class='c-star ml-1 icon-star-o'></i>
											<i class='c-star ml-1 icon-star'></i>
											<i class='c-star ml-1 icon-star'></i>
											<i class='c-star ml-0 icon-star'></i>
										</div>
									</a>
								</div>
							</div>
						</div>
						";
					}
				}
				$item['mortabet_div']=$mortabetDiv;
				
				//visist
				$oShopItems->setVisits($item['id'],'+');
				
				//likeCount
				$likeCount=count($oShopItems->likes_getAll($item['id']));
				if(isset($_SESSION['user_id']))
				{
					$likeId=$oShopItems->likes_check($item['id'],$_SESSION['user_id']);
					if($likeId) $heart='icon-heart'; else $heart='icon-heart-o'; 
				}
				else
					$heart='icon-heart-o';
				$item['like_a']="<a href='javascript:void(0)' onclick='shopItems_like({$item['id']});'><span id='spn_like_{$item['id']}'><i id='i_like_{$item['id']}' class='{$heart}'></i></span><span id='spn_likeCount_{$item['id']}'>({$likeCount})</span></a>";
				$item['likeCount']=$likeCount;
				
				//other -----
				$item['date']=jDate("Y/m/d",$item['id']);
         }
			return (object)$item;	
		}//------------------------------------------------------------------------------------
		public function itemsDraw($argArray)
		{
			$groupId=isset($argArray["groupId"]) ? $argArray["groupId"] : "";
			$sortByRow=isset($argArray["sortByRow"]) ? $argArray["sortByRow"] : true;
			$sortBySell=isset($argArray["sortBySell"]) ? $argArray["sortBySell"] : true;
			$sortByVisits=isset($argArray["sortByVisits"]) ? $argArray["sortByVisits"] : false;
			$searchWord=isset($argArray["searchWord"]) ? $argArray["searchWord"] : "";
			$tagTitle=isset($argArray["tagTitle"]) ? $argArray["tagTitle"] : "";
			$type=isset($argArray["type"]) ? $argArray["type"] : ""; //1:dane boland , 2:dane shekasteh
			$kesht=isset($argArray["kesht"]) ? $argArray["kesht"] : ""; //kesht avval ya dovvom ya donoj. 1 ya 2 ya 3
			$kood=isset($argArray["kood"]) ? $argArray["kood"] : ""; //noe kood. organik ya heivany ya shimiyaey. 1 ya 2 ya 3
			$page=isset($argArray["page"]) ? $argArray["page"] : 1; //noe kood. organik ya heivany ya shimiyaey. 1 ya 2 ya 3
			$argArray["active"]=1;
			$argArray["confirm"]=1;
			$argArray["trash"]='0';
			
			global $oPath;
			$oShopItems=new cShopItems();
			$oShopGroup=new cShopGroup();
			$oUsers=new cUsers();
			
			$link="items?group={$groupId}&byVisits={$sortByVisits}&byNew={$sortByRow}&bySell={$sortBySell}&s={$searchWord}";
			
			if($groupId==0) $groupId='';
			$items=$oShopItems->getAll($argArray);
			
			//pages
			$count=count($items);
			if($page < 1) $page=1;
			$pages=cTools::misc_sqlLimit($count,10,10,$page);
			$btnPageCode="";
			if($pages["pagesEnd"] > 1)
			{
				for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
				{
					if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
						$btnPageCode.="<li class='page-item shadow-none ml-3 active'><a class='page-link border-0 c-regular ir-r' href='{$link}&page={$i}'>{$i}</a></li>";
					else
						$btnPageCode.="<li class='page-item shadow-none ml-3'><a class='page-link border-0 c-regular ir-r' href='#'>{$i}</a></li>";
				}
				$back=$page - 1;
				$next=$page + 1;
				if($pages['isPrevious']) $btnPageCode="<li class='page-item shadow-none ml-3'><a class='page-link border-0 c-regular ir-r' href='{$link}&page={$back}'><i class='fa fa-angle-left'></i></a></li>" . $btnPageCode;
				if($pages['isNext']) $btnPageCode=$btnPageCode . "<li class='page-item shadow-none'><a class='page-link border-0 c-regular ir-r' href='{$link}&page={$next}'><i class='fa fa-angle-left'></i></a></li>";
			}
			$items=$oShopItems->getAll(['groupId'=>$groupId, 'sortByVisits'=>$sortByVisits, 'sortByRow'=>$sortByRow, 'sortBySell'=>$sortBySell, 'searchWord'=>$searchWord, 'tagTitle'=>$tagTitle, 'active'=>'1', 'confirm'=>'1', 'trash'=>'0', 'limitStr'=>$pages['sqlLimit']]);
		   $count=count($items);
			$code='';
			for($i=0;$i < $count;$i++)
			{
				$item=$oShopItems->calc($items[$i]);
				
				//image info
				if(file_exists($oPath->manageDir("shop_bundle/data/images/item0_{$item->id}.jpg")))
				{
					$img="<img class='default-img' src='" . $oPath->manage("shop_bundle/data/images/item0_{$item->id}.jpg") ."' alt='{$item->title}' style='' />";
				   $imgSrc=$oPath->manage("shop_bundle/data/images/item0_{$item->id}.jpg");
				}
				else
				{
					$img="";
					$imgSrc=$oPath->asset("default/images/noImage.gif");
				}
				
				//user or seller
				if($item->userId)
				{
					$user=$oUsers->get($item->userId);
					$userTitle=@$user->fName . ' ' . @$user->lName;
					if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$user->id}.jpg")))
						$userImgSrc=$oPath->manage("users_bundle/data/images/user_{$user->id}.jpg");
					else
						$userImgSrc=$oPath->asset("default/images/user_larg.png");
				}
				else
				{
					$userTitle='';
					$userImgSrc=$oPath->asset("default/images/user_larg.png");
				}
				//group info
				$group=@$oShopGroup->get($item->groupId)->title;						

            //price
				$priceStatus='';
				if($item->discount > 0)
				{
					$priceOld=$item->price_str . ' تومان';
					$price = $item->priceByDiscount_str . ' تومان';
				}
				else
				{
					$priceOld='';
					$price = $item->price_str . ' تومان';					
				}
            
				//like
				$likeCount=count($oShopItems->likes_getAll($item->id));
				if(isset($_SESSION['user_id']))
				{
					$likeId=$oShopItems->likes_check($item->id,@$_SESSION['user_id']);
					if($likeId) $heart='icon-heart'; else $heart='icon-heart-o'; 
				}
				else
					$heart='icon-heart-o';
				
				//visits count
				$visitCount=$item->visits;
				
				$code.="
			   <aside class='col-md-3 mb-3 mb-md-0'>
				   <div class='card border-0 overflow-hidden product-card'>
						<!-- Header -->
						<div class='card-header bg-transparent border-0 p-0 position-relative background' style='background-image:url({$imgSrc});'>
						   <!-- Product Image -->
						   <img src='{$imgSrc}' class='card-img-top' alt='{$item->title}' />

						   <!-- Add to Favorites -->
						   <button class='add-favorites btn position-absolute shadow-none' type='button' >
							   <i class='icon-heart-o text-white'></i>
						   </button>

						   <!-- Product Seller -->
						   <div class='product-seller px-2 d-flex flex-row align-items-end position-absolute'>
							   <img class='d-block float-right rounded-sm seller-img' src='{$userImgSrc}' alt='{$userTitle}' />
							   <span class='d-block float-right seller-name text-right ir-b c-dark fs-small pr-2'>{$userTitle}</span>
						   </div>

						   <a href='items?s={$item->city}' class='text-decoration-none ir-r text-white text-center fs-small position-absolute product-location'>{$item->city}</a>
						</div>
						<div class='card-body px-2 pb-2'>
						   <a class='text-decoration-none' href='item?i={$item->id}'>
							   <!-- Title -->
							   <h5 class='card-title ir-r c-dark fs-regular text-right'>{$item->title}</h5>
								
							   <!-- Price -->
							   <div class='price clearfix w-100'>
								   <p class='currently-price d-block float-right ir-r ml-2 text-success'>{$price}</p>
								   <p class='last-price d-block float-right ir-r text-decoration-line text-danger'>{$priceOld}</p>
							   </div>

								<!-- Stars -->
								<div class='stars d-flex flex-row justify-content-start'>
								   <i class='c-star ml-1 icon-star-o'></i>
									<i class='c-star ml-1 icon-star-o'></i>
									<i class='c-star ml-1 icon-star'></i>
									<i class='c-star ml-1 icon-star'></i>
									<i class='c-star ml-0 icon-star'></i>
								</div>
						   </a>
						</div>
				   </div>
			   </aside>			
				";
			}
	
	      if($code == '') $code="<h3 style='text-align:center;color:#ccc;'>موردی وجود ندارد</h3>";
			
			//page button
			$pageCode="
			<nav class='navigation' aria-label='Page navigation example' dir='ltr'>
				<ul class='pagination'>	
					{$btnPageCode}
				</ul>
			</nav>
			";
			
			//group
			$ret=$oShopGroup->getAll(true,"","1");
			$count=count($ret);
			$groupCode='';
			$gId=explode('|', $groupId);
			for($i=0;$i < $count;$i++)
			{
				
				if(in_array($ret[$i]->id, $gId)) $checked='checked'; else $checked='';
				$groupCode.="
				<div class='form-check'>
					<input class='form-check-input invisible' type='checkbox' value='{$ret[$i]->id}' id='item_{$i}' {$checked}>
					<label class='check-box-label d-block text-right ml-3 pr-2' for='item_{$i}' >
						<!-- Checking -->
						<i class='icon-check d-inline-block rounded-sm'></i>
						<span class='d-inline-block ir-r fs-small'>{$ret[$i]->title}</span>
					</label>
			   </div>				
				";
			}			
			
			return ['code'=>$code, 'pageCode'=>$pageCode, 'groupCode'=>$groupCode];			
		}//----------------------------------------------------------------------------------------------------------------------------------------
		public function items($groupId='',$sortByVisits='',$sortByRow='',$searchWord='', $tagTitle='',$page=1)
		{
			global $oPath;
			$oShopItems=new cShopItems();
			$oShopGroup=new cShopGroup();
			
			$link="items?group={$groupId}&visits={$sortByVisits}&byNew={$sortByRow}&searchWord={$searchWord}";
			
			if($groupId==0) $groupId='';
			$items=$oShopItems->getAll(['groupId'=>$groupId, 'sortByVisits'=>$sortByVisits, 'sortByRow'=>$sortByRow, 'searchWord'=>$searchWord, 'tagTitle'=>$tagTitle, 'active'=>1, 'confirm'=>1, 'trash'=>0]);
			
			//pages
			$count=count($items);
			if($page < 1) $page=1;
			$pages=cTools::misc_sqlLimit($count,10,10,$page);
			$btnPageCode="";
			for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn active'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn' href='{$link}&page={$i}' >{$i}</a>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages['isPrevious']) $btnPageCode="<a href='{$link}&page={$back}' class='btn'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages['isNext']) $btnPageCode=$btnPageCode . "<a href='{$link}&page={$next}' class='btn'><i class='fa fa-angle-left'></i></a>";						
			
			$items=$oShopItems->getAll(['groupId'=>$groupId, 'sortByVisits'=>$sortByVisits, 'sortByRow'=>$sortByRow, 'searchWord'=>$searchWord, 'tagTitle'=>$tagTitle, 'active'=>'1', 'confirm'=>'1', 'trash'=>'0', 'limitStr'=>$pages['sqlLimit']]);
		   $count=count($items);
			$code='';
			for($i=0;$i < $count;$i++)
			{
				$item=$oShopItems->calc($items[$i]);
			
				//image info
				if(file_exists($oPath->manageDir("shop_bundle/data/images/item0_{$item->id}.jpg")))
				{
					$img="<img class='default-img' src='" . $oPath->manage("shop_bundle/data/images/item0_{$item->id}.jpg") ."' alt='{$item->titleFa}' style='' />";
				   $imgSrc=$oPath->manage("shop_bundle/data/images/item0_{$item->id}.jpg");
				}
				else
				{
					$img="";
					$imgSrc=$oPath->asset("default/images/noImage.gif");
				}
				
				//comment
				$comment=explode("\n",$item->property);
				$comment=@$comment[0] . '، ' . @$comment[1];
				if($comment) $comment= mb_substr($comment,0,50) . ' ...';
				
				//group info
				$group=@$oShopGroup->get($item->groupId)->title;						

            //price
				$priceStatus='';
				if($item->discount)
				{
					$priceOld=$item->price_str . ' تومان';
					$price = $item->priceByDiscount_str . ' تومان';
				}
				else
				{
					$priceOld='';
					$price = $item->price_str . ' تومان';					
				}
				
				//like
				$likeCount=count($oShopItems->likes_getAll($item->id));
				if(isset($_SESSION['user_id']))
				{
					$likeId=$oShopItems->likes_check($item->id,$_SESSION['user_id']);
					if($likeId) $heart='icon-heart'; else $heart='icon-heart-o'; 
				}
				else
					$heart='icon-heart-o';
				
				//visits count
				$visitCount=$item->visits;
				
				$code.="
				<div class='col-lg-4 col-md-6 col-12'>
					<div class='single-list'>
						<div class='row'>
							<div class='col-lg-6 col-md-6 col-12'>
								<div class='list-image overlay' style='background-image:url({$imgSrc});'>
								   {$priceStatus}
									{$img}
									<a href='item?i={$item->id}' class='buy'><i class='fa fa-shopping-bag'></i></a>
								</div>
							</div>
							<div class='col-lg-6 col-md-6 col-12 no-padding'>
								<div class='content'>
									<h4 class='title'><a href='item?i={$item->id}'>{$item->titleFa}</a></h4>";
									if($item->type=='0' && $item->titleEn) 
										$code.="<h4 class='title' style='direction:rtl;'><a href='item?i={$item->id}'>{$item->titleEn}</a></h3>";
									$code.="
									<p class='comment'>{$comment}</p>
									<div class='list-price'>
										<span class='price-old'>{$priceOld}</span>
										<span class=''>{$price}</span>
									</div>
									<div class='list-action'>
										<a title='{$typeStatus}' href='items?t={$typeStatus}'><per>{$typeStatus}</per><span>نوع محصول</span></a>
										<a title='علاقه مندی' href='javascript:void(0)' onclick='shopItems_like({$item->id});'><i id='i_like_{$item->id}' class='icon-heart-o'></i><per id='spn_like_{$item->id}'>{$likeCount}</per><span>افزودن به علاقه مندی</span></a>
										<a title='تعداد بازدید' href='javascript:void(0)'><i class='fa fa-eye'></i><per>{$visitCount}</per><span>تعداد بازدید</span></a>
									</div>									
								</div>
							</div>
						</div>
					</div>
				</div>							
				";				
			}
			if($count == 0) $code='<h3 class="col-12" style="text-align:center;color:#ddd;font-size: 32px;"><br><br><i class="fa fa-info-circle fa-2x"></i>&nbsp;خالی است</h3>';
			
			//page button
			$code.="
			<div class='btnPages col-12'>
			   <div class='container'>
					<div class='row'>		
						{$btnPageCode}
					</div>
				</div>
			</div>
			";
			
			$title='';
			if($searchWord && $groupId)
				$title="جستجو '{$searchWord}' - " . 'دسته ' . @$oShopGroup->get($groupId)->title;	
			else if($searchWord)
				$title="جستجو '{$searchWord}'";	
			else if($tagTitle)
				$title="محصولات '{$searchWord}'";					
			else if($groupId)
				$title.='دسته ' . @$oShopGroup->get($groupId)->title;
			else if($sortByVisits)
				$title='محصولات پربازدید';
			else
				$title='محصولات';
			
			return ['items_div'=>$code, 'title'=>$title];
		}//------------------------------------------------------------------------------------
		
		function factorHtml($factorId)
		{
			global $oPath;
			//---
		   $oShopFactor=new cShopFactor();
		   $oShopItems=new cShopItems();
			$shopFactor=$oShopFactor->get($factorId);
			if($shopFactor)
			{
				//factorId
				$lbl_factorId="<label>فاکتور : <b>{$factorId}<b></label><br>";
				//date
				$lbl_date='<label>تاریخ : <b>' . jDate('Y/m/d h:i:s', $shopFactor->insertDate) . '</b></label><br>';
				//shopItem object
			   $shopItem=$oShopItems->get($shopFactor->shopItemId);
            //---
				if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$shopFactor->shopItemId}.jpg")))
					$img_shopItem="<img class='brd-radius-8' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$shopFactor->shopItemId}.jpg?t=" . time() ) ."' style='width:160px' />";
				else
					$img_shopItem="<img class='brd-radius-8' src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";
				//---
				$filePath1=$oPath->manageDir("shop_bundle/data/{$shopItem->fileName1}");
				$filePath2=$oPath->manageDir("shop_bundle/data/{$shopItem->fileName2}");
				//---
				$lbl_download='';
				$lbl_fileDownloadExpire='';
				if($shopFactor->payStatus > 0)
				{
					if(($shopItem->fileName1 && file_exists($filePath1)) || $shopItem->fileUrl1)
						$lbl_download.="<label><a href='dl?i={$factorId}&n=1'><i class='fa fa-download'></i>&nbsp;دانلود فایل 1</a></label><br>";
					if(($shopItem->fileName2 && file_exists($filePath1)) || $shopItem->fileUrl2)
						$lbl_download.="<label><a href='dl?i={$factorId}&n=2'><i class='fa fa-download'></i>&nbsp;دانلود فایل 2</a></label><br>";				
					//---
					if($shopFactor->shopDownloadExpire < time())
						$lbl_fileDownloadExpire="<label class='alert alert-danger'>پایان اعتبار</label><br>";
					else
					{
						$days=round(($shopFactor->shopDownloadExpire - time()) /60/60/24);
						$lbl_fileDownloadExpire="<label class='alert alert-success' >اعتبار {$days} روز</label><br>";
					}
				}
				//shopItem price and discount
				$shopItemPrice=$shopFactor->shopItemPrice + $shopFactor->shopPricePlus;
				if($shopFactor->shopItemDiscount)
				{
					$lbl_shopItemPrice="<label><s>" . number_format($shopItemPrice) . " تومان</s></label><br>"; 
					$lbl_shopItemPrice.="<label>" . number_format($shopItemPrice - (($shopItemPrice * $shopFactor->shopItemDiscount))/100) . " تومان</label><br>"; 
				}
				else
				{
					$lbl_shopItemPrice="<label>" . number_format($shopItemPrice) . " تومان</label><br>"; 
				}
				//factor info
				if($shopFactor->payStatus == 0)
					$lbl_payStatus='<h4 class="alert alert-danger">ناموفق</h4><br>';
				else
					$lbl_payStatus='<h4 class="alert alert-success">موفق</h4><br>';

				$lbl_price='<label>مبلغ : <b>' . number_format($oShopFactor->getPrice($factorId)) . ' تومان</b></label><br>';
				$lbl_transactionId="<label>تراکنش : <b>{$shopFactor->transactionId}</b></label><br>";
            $html="
				   {$lbl_payStatus}
				   <h4>مشخصات</h4><br>
					{$lbl_factorId}
					{$lbl_transactionId}
					{$lbl_date}
					{$lbl_price}
					{$lbl_fileDownloadExpire}
					<hr>
					<h4>محصول</h4><br>
					{$img_shopItem}<br>
					<label>{$shopFactor->shopItemTitle}</label><br>
					{$lbl_shopItemPrice}
				";	
            if($lbl_download) $html.="<hr><h3>فایل ها</h3><br>{$lbl_download}";
            return "
				   <div id='layer_factor' style='direction:rtl;text-align:right;padding:15px;background-color:#fff;'>
					   {$html}
						<br></br>
						<hr>
						<div style='text-align:center;'><button class='btn' onclick='oTools.print($(\"#layer_factor\").html());'><i class='fa fa-print'></i>&nbsp;چاپ</button></div>
					</div>
				";				
			}
			else
			{
				return "
				   <div class='alert alert-danger' style='text-align:center;direction:rtl;'>
					   <i class='fa fa-info'></i>&nbsp;یافت نشد
					</div>
				";
			}
		}//------------------------------------------------------------------------------------
		function factorFileDownload($factorId, $fileNumber) //$fileNumber: file1 or 1,file2 or 2
		{
			global $oPath;
		   $oShopFactor=new cShopFactor();
			$shopFactor=$oShopFactor->get($factorId);
			if($shopFactor)
			{
				if($shopFactor->shopDownloadExpire==-1 || $shopFactor->shopDownloadExpire > time())
				{
					$oShopItems=new cShopItems();
					$shopItem=$oShopItems->get($shopFactor->shopItemId);
					//---
					$filePath1=$oPath->manageDir("shop_bundle/data/{$shopItem->fileName1}");
					$filePath2=$oPath->manageDir("shop_bundle/data/{$shopItem->fileName2}");
					//---
					if($fileNumber==1 || $fileNumber=='file1')
					{
						if($shopItem->fileName1 && file_exists($filePath1))
						{
							$oShopFactor->setShopItemDownload1($factorId,'+');
							cTools::fs_createDownload($filePath1,"{$factorId}_{$fileNumber}");
						}
						else if($shopItem->fileUrl1)
						{
							$oShopFactor->setShopItemDownload1($factorId,'+');
							echo "<script type='javascript'>oTools.link('{$shopItem->fileUrl1}');</script>";
						}						
					}
					else if($fileNumber==2 || $fileNumber=='file2')
					{
						if($shopItem->fileName2 && file_exists($filePath2))
						{
							$oShopFactor->setShopItemDownload2($factorId,'+');
							cTools::fs_createDownload($filePath2,"{$factorId}_{$fileNumber}");
						}
						else if($shopItem->fileUrl2)
						{
							$oShopFactor->setShopItemDownload2($factorId,'+');
							echo "<script type='javascript'>oTools.link('{$shopItem->fileUrl2}');</script>";
						}							
					}
				}
				else
					echo 'Expire';
			}
		}//------------------------------------------------------------------------------------
	}
?>
<?php
   @session_start();

	//includes
	include_once $_SESSION["engineRequire"]; //engine.php
	require_once $oPath->manageDir("shop_bundle/model/shopCoupon_model.php");
	require_once $oPath->manageDir("admin_bundle/model/admin_model.php");
	require_once $oPath->manageDir("jdf.php");

	//objects
	$oShopCoupon=new cShopCoupon();
	$oAdmin=new cAdmin();
	
	//request
	$request=@$_REQUEST["requestName"];
	
	if($request=="shopCoupon_draw")
   {
      $searchWord=cDataBase::escape(@$_REQUEST["searchWord"]);
      $page=isset($_REQUEST["page"]) ? cDataBase::escape($_REQUEST["page"]) : 1;
		
		
		//auto session
		if($searchWord==-1) $searchWord=@$_SESSION["shopCoupon_searchWord"];
      if($page==-1) $page=$_SESSION["shopCoupon_page"];
		
		//init session for auto session
      $_SESSION["shopCoupon_searchWord"]=$searchWord;
      $_SESSION["shopCoupon_page"]=$page;		
		
		//START PAGE ...................................
      $arg=array(
			"searchWord"=>$searchWord,
			"limitStr"=>""
		);		
      $items=$oShopCoupon->getAll($arg);
      $count=count($items);
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
		for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
		{
			if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
				$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
			else
				$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='shopCoupon_draw(\"searchWord\":-1,\"page\":{$i});'>{$i}</a>";
		}
      $back=$page - 1;
      $next=$page + 1;
      if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='shopCoupon_draw(\"searchWord\":-1,\"page\":{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='shopCoupon_draw(\"searchWord\":-1,\"page\":{$next});'><i class='fa fa-angle-left'></i></a>";		
		
      $arg=array(
			"searchWord"=>$searchWord,
			"limitStr"=>$pages['sqlLimit']
		);	      
		$items=$oShopCoupon->getAll($arg);
		
      //END PAGE ...................................
		
      $codeTr="";
      $i=0;

		foreach($items as $item)
		{
			$i++;
			$itemId=$item->id;
			
			//maxDiscount
			$maxDiscount=number_format($item->maxDiscount,0,',','.');
			
			//startDate
			$startDate=jDate('Y/m/d',$item->startDate);

			//endDate
			$endDate=jDate('Y/m/d',$item->endDate);
			
			//expiration Status
			if($item->endDate < time())
				$expirationStatus="<label class='lbl fg-danger'>به پایان رسیده</label>";
			else if($item->startDate > time())
			{
				$days=(time() - $item->startDate) /60/60/24;
				$expirationStatus="<label class='lbl fg-warning'>{$days} روز تا زمان شروع</label>";
			}
			else
			{
				$days=($item->endDate - $item->startDate) /60/60/24;
				$expirationStatus="<label class='lbl fg-success'>{$days} روز اعتبار دارد</label>";				
			}
			
			$codeTr.="
			<tr>";
				if(count($items) > 1) $codeTr.="<td style='text-align:center;min-width: 10px;'><input type='checkbox' id='chk_{$i}' onclick='checkedOne();' value='{$item->id}' /></td>";
				$codeTr.="
				<td>{$item->title}</td>
				<td>{$item->discount} %</td>
				<td>{$maxDiscount} تومان</td>
				<td>
				   <label>{$startDate}</span>
					<br>
					تا
					<br>
					<span>{$endDate}</span>
					<hr>
					{$expirationStatus}
				</td>
				<td>
					<button class='btn btn-danger' onclick='shopCoupon_del({$item->id});'><i class='fa fa-remove'></i></button>
					<button class='btn btn-info' onclick='shopCoupon_edit({$item->id});'><i class='fa fa-pencil'></i></button>
				</td>
			</tr>";
		}
      if($codeTr)
		{
			$chkCount=$i;
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;کوپن های تخفیف</h1>
			<div class='vSpace-4x'></div>
			
			<button type='button' class='btn btn-success' onclick='shopCoupon_new();'><i class='fa fa-plus'></i>&nbsp;افزودن</button>
			<button id='btn_trash' class='btn btn-danger' onclick='shopCoupon_del(\"selected\")' disabled><i class='fa fa-remove'></i></button>
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopCoupon_draw($(\"#txt_search\").val(),1);'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			<br>{$btnPageCode}<br><br>
			<div class='vScrollBar-auto'>
				<table class='tbl tbl-center tbl-bordered tbl-hover'>
					<tr>";
						if(count($items) > 1) $code.="<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>";
						$code.="
						<th>عنوان</th>
						<th>درصد تخفیف</th>
						<th>سقف تخفیف</th>
						<th>زمان اعتبار</th>
						<th>عملیات</th>
					</tr>
					{$codeTr}
				</table>
			<div>
			<br>{$btnPageCode}<br> 
			<div class='vSpace-4x'></div>";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;کوپن های تخفیف</h1>
			<div class='vSpace-4x'></div>

			<button type='button' class='btn btn-success' onclick='shopCoupon_new();'><i class='fa fa-plus'></i>&nbsp;افزودن</button>

			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopCoupon_draw($(\"#txt_search\").val(),1);'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'></div>";		
		}
		$oEngine->response("ok[|]" . $code);
   }//------------------------------------------------------------------------------------	
   else if($request=="shopCoupon_del")
   {
      $itemId=$oDb->escape($_REQUEST["id"],false);
      $oShopCoupon->delete($itemId);   
		$oEngine->response('ok');
   }//------------------------------------------------------------------------------------
   else if($request=="shopCoupon_edit")
   {
      $id=$oDb->escape($_REQUEST["id"]);
      $item=$oShopCoupon->get($id);

		//startDate
		$startDate=jDate('Y/m/d',$item->startDate);

		//endDate
		$endDate=jDate('Y/m/d',$item->endDate);
			
      $code="
		   <div class='vSpace-4x'></div>
		   <h1><i class='fa fa-pencil'></i>&nbsp;ویرایش کوپن تخفیف ({$item->title})</h1>
			<div class='vSpace-4x'></div>
			
			<div class='form'>
			   <label><i class='fa fa-circle'></i>عنوان<span class='fg-red'>*</span></label>
				<input type='text' id='txt_title' value='{$item->title}' disabled>
            
			   <label><i class='fa fa-circle'></i>درصد تخفیف<span class='fg-red'>*</span></label>
				<input type='text' id='txt_discount' value='{$item->discount}' class='dir-ltr' required> %				

			   <label><i class='fa fa-circle'></i>سقف تخفیف<span class='fg-red'>*</span></label>
				<input type='text' id='txt_maxDiscount' value='{$item->maxDiscount}' class='dir-ltr' required> تومان
				
			   <label><i class='fa fa-circle'></i>تاریخ شروع. مثال : 1396/2/5<span class='fg-red'>*</span></label>
				<input type='text' id='txt_startDate' value='{$startDate}' class='pdate dir-ltr' required>

			   <label><i class='fa fa-circle'></i>تاریخ پایان. مثال : 1396/2/5<span class='fg-red'>*</span></label>
				<input type='text' id='txt_endDate' value='{$endDate}' class='pdate dir-ltr' required>	

			   <label><i class='fa fa-circle'></i>توضیحات</label>
				<textarea id='txt_comment'>{$item->comment}</textarea>					
			</div>
			
			<div class='vSpace-4x'></div>
			<button class='btn btn-default' onclick='shopCoupon_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>				
			<button class='btn btn-success' onclick='shopCoupon_update(\"edit\",{$item->id});'>ذخيره</button>
         <div class='vSpace-4x'></div>
      ";

      $oEngine->response("ok[|]{$code}");	
   }//------------------------------------------------------------------------------------
   else if($request=="shopCoupon_new")
   {

		//startDate
		$startDate=jDate('Y/m/d',time());

		//endDate
		$endDate=jDate('Y/m/d',time());
			
      $code="
		   <div class='vSpace-4x'></div>
		   <h1><i class='fa fa-pencil'></i>&nbsp;کوپن تخفیف جدید</h1>
			<div class='vSpace-4x'></div>
			
			<div class='form'>
			   <label><i class='fa fa-circle'></i>عنوان<span class='fg-red'>*</span></label>
				<input type='text' id='txt_title' required>
            

			   <label><i class='fa fa-circle'></i>درصد تخفیف<span class='fg-red'>*</span></label>
				<input type='text' id='txt_discount' class='dir-ltr' required> %				

			   <label><i class='fa fa-circle'></i>سقف تخفیف<span class='fg-red'>*</span></label>
				<input type='text' id='txt_maxDiscount' class='dir-ltr' required> تومان
				
			   <label><i class='fa fa-circle'></i>تاریخ شروع. مثال : 1396/2/5<span class='fg-red'>*</span></label>
				<input type='text' id='txt_startDate' value='{$startDate}' class='pdate dir-ltr' required>

			   <label><i class='fa fa-circle'></i>تاریخ پایان. مثال : 1396/2/5<span class='fg-red'>*</span></label>
				<input type='text' id='txt_endDate' value='{$endDate}' class='pdate dir-ltr' required>					
			
			   <label><i class='fa fa-circle'></i>توضیحات</label>
				<textarea id='txt_comment'></textarea>			
			</div>
			
			<div class='vSpace-4x'></div>
			<button class='btn btn-default' onclick='shopCoupon_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>				
			<button class='btn btn-success' onclick='shopCoupon_update(\"new\",0);'>ذخيره</button>
         <div class='vSpace-4x'></div>
      ";

      $oEngine->response("ok[|]{$code}");	
   }//------------------------------------------------------------------------------------
   else if($request=="shopCoupon_update")
   {
		$purpose=cDataBase::escape($_REQUEST["purpose"]);
      $array=array();
		
		$array["title"]=cDataBase::escape(@$_REQUEST["title"]);
      if($oShopCoupon->getByTitle($array["title"]) && $purpose=='new')
		{
			$oEngine->response("!exists[|]{$array['title']}");	
			exit;
		}
		
		//date
		$startDate=cDataBase::escape($_REQUEST['startDate']);
		$endDate=cDataBase::escape($_REQUEST['endDate']);
		
      $array['discount']=(int)cDataBase::escape($_REQUEST['discount']);		
      $array['maxDiscount']=(int)
		cDataBase::escape($_REQUEST['maxDiscount']);		
      $array['startDate']=jalali_to_unix_timestamp($startDate);		
      $array['endDate']=jalali_to_unix_timestamp($endDate);		
      $array['comment']=cDataBase::escape($_REQUEST['comment']);	

		if($purpose=="edit")
		{
			$array['id']=cDataBase::escape($_REQUEST['id']);	
			$array['createById']=$_SESSION['admin_id'];
         $oShopCoupon->update($array);
		}
		else if($purpose=="new")
		{
			$array['insertById']=$_SESSION['admin_id'];
         $oShopCoupon->insert($array);
		}

      $oEngine->response("ok[|]{$purpose}");	
   }//------------------------------------------------------------------------------------
?>
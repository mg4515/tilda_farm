<?php
   @session_start();

	//includes
	require_once $_SESSION["engineRequire"]; //engine.php
	require_once $oPath->manageDir("shop_bundle/model/shopProperty_model.php");
	require_once $oPath->manageDir("simpleImage.php");

	//objects
	$oShopProperty=new cShopProperty();
	
	//var
	$array_typeTitle=['امتیازی 1 تا 10', 'گزینه های مختلف'];
	
	//request
	$request=@$_REQUEST["requestName"];
	
	//start requestName process
	if($request=="shopProperty_draw")
   {
      $items=$oShopProperty->getAll();
      $codeTr="";
      $i=0;
      foreach($items as $item)
      {
         $i++;
			$typeTitle=$array_typeTitle[$item->type];
         $codeTr.="
         <tr>
            <td style='text-align:center;padding:10px;width:100px;'>
					{$item->title}
				</td>
				<td>{$typeTitle}</td>
            <td>
					<button class='btn btn-block-auto btn-danger' onclick='shopProperty_del({$item->id});'><i class='fa fa-trash-o'></i></button>
					<button class='btn btn-block-auto btn-info' onclick='shopProperty_edit({$item->id});'><i class='fa fa-pencil'></i></button>
				</td>
         </tr>";
      }
		
		if($codeTr)
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;ویژگی ها</h1>
			<div class='vSpace-4x'></div>
			
			<button type='button' class='btn btn-success' onclick='shopProperty_new();'><i class='fa fa-plus'></i>&nbsp;افزودن</button>				
			<div class='vSpace'></div>		
			<table class='tbl tbl-right tbl-bordered tbl-hover'>
				<tr>
					<th>عنوان</th>
					<th>نوع</th>
					<th></th>
				</tr>
				{$codeTr}
			</table>
			<div class='vSpace-4x'></div>
			";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;ویژگی ها</h1>
			<div class='vSpace-4x'></div>
			<button type='button' class='btn btn-success' onclick='shopProperty_new();'><i class='fa fa-plus'></i>&nbsp;افزودن</button>				
			<div class='vSpace'></div>					
			<hr>			
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'></div>
			";
		}
		cEngine::response("ok[|]" . $code);
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopProperty_del")
   {
      $id=cDataBase::escape(@$_REQUEST["id"]);
      
		//shopProperty
		$oShopProperty->delete($id);
		@unlink($oPath->manageDir("shop_bundle/data/images/propertyOptn1_{$id}.jpg"));
		@unlink($oPath->manageDir("shop_bundle/data/images/propertyOptn2_{$id}.jpg"));
		@unlink($oPath->manageDir("shop_bundle/data/images/propertyOptn3_{$id}.jpg"));
		@unlink($oPath->manageDir("shop_bundle/data/images/propertyOptn4_{$id}.jpg"));
		@unlink($oPath->manageDir("shop_bundle/data/images/propertyOptn5_{$id}.jpg"));
		
		//shopItemProperty
		//$oShopItemProperty->deleteByProprtyId($id);
		
		cEngine::response("ok[|]{$parentId}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopProperty_edit")
   {
      $id=cDataBase::escape($_REQUEST["id"]);
      $item=$oShopProperty->get($id);

      $t=time();                                                             
      if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn1_{$id}.jpg")))
         $imgOption1="<img id='img_option1' src='". $oPath->manage("shop_bundle/data/images/propertyOptn1_{$id}.jpg?t={$t}") ."' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel1' class='fa fa-trash fa-2x' onclick='shopProperty_imgDel({$id},1);' style='cursor:pointer;color:red;'></i>";
      else
         $imgOption1="<img id='img_option1' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn2_{$id}.jpg")))
         $imgOption2="<img id='img_option2' src='". $oPath->manage("shop_bundle/data/images/propertyOptn2_{$id}.jpg?t={$t}") ."' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel2' class='fa fa-trash fa-2x' onclick='shopProperty_imgDel({$id},2);' style='cursor:pointer;color:red;'></i>";
      else
         $imgOption2="<img id='img_option2' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn3_{$id}.jpg")))
         $imgOption3="<img id='img_option3' src='". $oPath->manage("shop_bundle/data/images/propertyOptn3_{$id}.jpg?t={$t}") ."' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel3' class='fa fa-trash fa-2x' onclick='shopProperty_imgDel({$id},3);' style='cursor:pointer;color:red;'></i>";
      else
         $imgOption3="<img id='img_option3' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn4_{$id}.jpg")))
         $imgOption4="<img id='img_option4' src='". $oPath->manage("shop_bundle/data/images/propertyOptn4_{$id}.jpg?t={$t}") ."' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel4' class='fa fa-trash fa-2x' onclick='shopProperty_imgDel({$id},4);' style='cursor:pointer;color:red;'></i>";
      else
         $imgOption4="<img id='img_option4' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn5_{$id}.jpg")))
         $imgOption5="<img id='img_option4' src='". $oPath->manage("shop_bundle/data/images/propertyOptn5_{$id}.jpg?t={$t}") ."' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel5' class='fa fa-trash fa-2x' onclick='shopProperty_imgDel({$id},5);' style='cursor:pointer;color:red;'></i>";
      else
         $imgOption5="<img id='img_option4' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
		
      //type
		$optn_type='';
		for($i=0;$i < count($array_typeTitle); $i++)
		{
			if($item->type==$i) $selected='selected'; else $selected='';
			$optn_type.="<option value='{$i}' {$selected}>{$array_typeTitle[$i]}</option>";
		}
		
		if($item->type=='0') 
			$class_display='hide';
		else
			$class_display='show';
		
      $code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;ویرایش ویژگی ({$item->title})</h1>
		<div class='vSpace-4x'></div>
		
		<div class='panel'>
		   <div class='panel-body form'>
				<label>عنوان</label>
				<input type='text' id='txt_title' value='{$item->title}' >

				<label>نوع ویژگی</label>
				<select id='slct_type' onclick='if(this.value==0) $(\"#div_options\").attr(\"class\",\"hide\"); else $(\"#div_options\").attr(\"class\",\"show\");'>{$optn_type}</select>
				
				<div id='div_options' class='{$class_display}'>
				   <div class='vSpace-4x'></div>
				   <h3>گزینه های مختلف</h3>
					<p>گزینه هایی که اطلاعات آن تکمیل می شود، گزینه های فعال در نظر گرفته می شوند</p>
					
					<div class='vSpace-2x'></div>
					
					
					<label>عنوان گزینه 1</label>
					<input type='text' id='txt_option1Title' value='{$item->option1Title}'>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 1</label>
					{$imgOption1}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option1Img' onchange='oTools.previewImgFile(\"file_option1Img\",\"img_option1\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>						
					
					<hr>
					
					<label>عنوان گزینه 2</label>
					<input type='text' id='txt_option2Title' value='{$item->option2Title}'>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 2</label>
					{$imgOption2}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option2Img' onchange='oTools.previewImgFile(\"file_option2Img\",\"img_option2\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>						
					
					<hr>
					
					<label>عنوان گزینه 3</label>
					<input type='text' id='txt_option3Title' value='{$item->option3Title}'>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 3</label>
					{$imgOption3}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option3Img' onchange='oTools.previewImgFile(\"file_option3Img\",\"img_option3\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>						
					
					<hr>
					
					<label>عنوان گزینه 4</label>
					<input type='text' id='txt_option4Title' value='{$item->option4Title}'>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 4</label>
					{$imgOption4}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option4Img' onchange='oTools.previewImgFile(\"file_option4Img\",\"img_option4\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>											
					
					<hr>
					
					<label>عنوان گزینه 5</label>
					<input type='text' id='txt_option5Title' value='{$item->option5Title}'>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 5</label>
					{$imgOption4}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option5Img' onchange='oTools.previewImgFile(\"file_option5Img\",\"img_option5\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>					
				</div>

				<div class='vSpace-4x'></div>			
				<hr>
				<button class='btn btn-default' onclick='shopProperty_draw();'><i class='fa fa-arrow-right'></i></button>				
				<button class='btn btn-success' onclick='shopProperty_update(\"edit\",{$item->id});'>ذخيره</button>
			
			</div>
		</div>
		
		<div class='vSpace-4x'></div>
      ";

      cEngine::response("ok[|]{$code}");
      exit;		
   }//------------------------------------------------------------------------------------
   else if($request=="shopProperty_new")
   { 
      //image
      $imgOption1="<img id='img_option1' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
      $imgOption2="<img id='img_option2' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
      $imgOption3="<img id='img_option3' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
      $imgOption4="<img id='img_option4' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
      $imgOption5="<img id='img_option5' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";

      //type
		$optn_type='';
		for($i=0;$i < count($array_typeTitle); $i++)
		{
			if($i==0) $selected='selected'; else $selected='';
			$optn_type.="<option value='{$i}' {$selected}>{$array_typeTitle[$i]}</option>";
		}
		$class_display='hide';
      $code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-plus'></i>&nbsp;ویژگی جدید</h1>
		<div class='vSpace-4x'></div>
		
		<div class='panel'>
		   <div class='panel-body form'>
				<label>عنوان</label>
				<input type='text' id='txt_title' value='' >

				<label>نوع ویژگی</label>
				<select id='slct_type' onclick='if(this.value==0) $(\"#div_options\").attr(\"class\",\"hide\"); else $(\"#div_options\").attr(\"class\",\"show\");'>{$optn_type}</select>
				
				<div id='div_options' class='{$class_display}'>
				   <div class='vSpace-4x'></div>
				   <h3>گزینه های مختلف</h3>
					<p>گزینه هایی که اطلاعات آن تکمیل می شود، گزینه های فعال در نظر گرفته می شوند</p>
					
					<div class='vSpace-2x'></div>
					
					<label>عنوان گزینه 1</label>
					<input type='text' id='txt_option1Title' value=''>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 1</label>
					{$imgOption1}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option1Img' onchange='oTools.previewImgFile(\"file_option1Img\",\"img_option1\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>						
					
					<hr>
					
					<label>عنوان گزینه 2</label>
					<input type='text' id='txt_option2Title' value=''>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 2</label>
					{$imgOption2}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option2Img' onchange='oTools.previewImgFile(\"file_option2Img\",\"img_option2\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>						
					
					<hr>
					
					<label>عنوان گزینه 3</label>
					<input type='text' id='txt_option3Title' value=''>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 3</label>
					{$imgOption3}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option3Img' onchange='oTools.previewImgFile(\"file_option3Img\",\"img_option3\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>						
					
					<hr>
					
					<label>عنوان گزینه 4</label>
					<input type='text' id='txt_option4Title' value=''>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 4</label>
					{$imgOption4}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option4Img' onchange='oTools.previewImgFile(\"file_option4Img\",\"img_option4\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>											
					
					<hr>
					
					<label>عنوان گزینه 5</label>
					<input type='text' id='txt_option5Title' value=''>
					<!-- -->
					<label><i class='fa fa-circle'></i>تصویر گزینه 5</label>
					{$imgOption4}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_option5Img' onchange='oTools.previewImgFile(\"file_option5Img\",\"img_option5\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>					
				</div>

				<div class='vSpace-4x'></div>			
				<hr>
				<button class='btn btn-default' onclick='shopProperty_draw();'><i class='fa fa-arrow-right'></i></button>				
				<button class='btn btn-success' onclick='shopProperty_update(\"new\",0);'>ذخيره</button>
			
			</div>
		</div>
		
		<div class='vSpace-4x'></div>
      ";

      cEngine::response("ok[|]{$code}");
      exit;		
   }//------------------------------------------------------------------------------------
   else if($request=="shopProperty_update")
   {
		$purpose=cDataBase::escape($_REQUEST["purpose"]);
		
		if($purpose=="edit")
		   $id=cDataBase::escape($_REQUEST["id"]);
	   else
			$id=time();
		
      $array=array();	
      $array["id"]=$id;
      $array["purpose"]=$purpose;
      $array["title"]=cDataBase::escape($_REQUEST["title"]);
      $array["type"]=cDataBase::escape($_REQUEST["type"]);
      $array["option1Title"]=cDataBase::escape($_REQUEST["option1Title"]);
      $array["option2Title"]=cDataBase::escape($_REQUEST["option2Title"]);
      $array["option3Title"]=cDataBase::escape($_REQUEST["option3Title"]);
      $array["option4Title"]=cDataBase::escape($_REQUEST["option4Title"]);
      $array["option5Title"]=cDataBase::escape($_REQUEST["option5Title"]);
      
		if($array["purpose"]=="edit")
         $ret=$oShopProperty->update($array);
		else if($array["purpose"]=="new")
         $ret=$oShopProperty->insert($array);
		
      //folders
      if(!file_exists($oPath->manageDir("shop_bundle/data"))) mkdir($oPath->manageDir("shop_bundle/data"));
      if(!file_exists($oPath->manageDir("shop_bundle/data/images"))) mkdir($oPath->manageDir("shop_bundle/data/images"));
		
		//image delete and save
		for($i=1; $i<=5; $i++)
		{
			$imgDel=$oDb->escape($_REQUEST["imgDel{$i}"]);
			if($imgDel==1 || $imgDel=="true") @unlink($oPath->manageDir("shop_bundle/data/images/propertyOptn{$i}_{$id}.jpg"));
		   //---
			if(isset($_FILES["file_option{$i}Img"]))
			{
				if($oTools->fs_fileIsImage($_FILES["file_option{$i}Img"]["name"]))
				{
					//$img = new SimpleImage();
					//$img->load($_FILES["file_option{$i}Img"]['tmp_name'])
					//->fit_to_width(250)
					//->save($oPath->manageDir("shop_bundle/data/images/propertyOptn{$i}_{$id}.jpg"),100);
					copy($_FILES["file_option{$i}Img"]['tmp_name'], $oPath->manageDir("shop_bundle/data/images/propertyOptn{$i}_{$id}.jpg"));
					@unlink($_FILES["file_option{$i}Img"]["tmp_name"]);
				}
				else
				{
					cEngine::response("errType[|]{$i}");
					exit;
				}
			}
		}
		
      cEngine::response("ok");
      exit;		
   }//------------------------------------------------------------------------------------
?>
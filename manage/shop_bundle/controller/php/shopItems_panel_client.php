<?php
   /*header("Content-Type: application/json; charset=UTF-8");*/
   session_start();

	//includes
	include_once $_SESSION["engineRequire"]; //engine.php
	require_once $oPath->manageDir("shop_bundle/model/shopItems_model.php");
	require_once $oPath->manageDir("shop_bundle/model/shopItemsOther_model.php");
	require_once $oPath->manageDir("shop_bundle/model/shopGroup_model.php");
	require_once $oPath->manageDir("shop_bundle/model/shopPublication_model.php");
	require_once $oPath->manageDir("shop_bundle/controller/php/shop_tools.php");
	require_once $oPath->manageDir("admin_bundle/model/admin_model.php");
	require_once $oPath->manageDir("users_bundle/model/users_model.php");
	require_once $oPath->manageDir("jdf.php");
	require_once $oPath->manageDir("simpleImage.php");

	//objects
	$oShopItems=new cShopItems();
	$oShopItemsOther=new cShopItemsOther();
	$oShopGroup=new cShopGroup();
	$oShopPublication=new cShopPublication();
	$oShopTools=new cShopTools();
	$oAdmin=new cAdmin();
	$oUsers=new cUsers();

   //var
	$arryTypeTitle=[''=>'محصولات',0=>'مقالات',1=>'پایان نامه'];
	$arryTypeFilter=[''=>'',0=>'مقالات',1=>'پایان نامه'];
	$arryConfirmFilter=[''=>'', 0=>'در انتظار تایید', 1=>'تایید شده', 2=>'تایید نشده'];
	
	//request
	$request=@$_REQUEST["requestName"];
	
	if($request=="shopItems_draw")
   {
		$type=isset($_REQUEST["type"]) ? cDataBase::escape($_REQUEST["type"]) : '0'; //0,maghalat or 1,payanname
      $userId=isset($_REQUEST["userId"]) ? cDataBase::escape($_REQUEST["userId"]) : '';
      $confirm=cDataBase::escape(@$_REQUEST["confirm"]);
      $trash=isset($_REQUEST["trash"]) ? cDataBase::escape($_REQUEST["trash"]) : '0';
      $searchWord=cDataBase::escape(@$_REQUEST["searchWord"]);
      $page=isset($_REQUEST["page"]) ? cDataBase::escape($_REQUEST["page"]) : 1;
		
		//auto session
		if($type==-1) $type=@$_SESSION["shopItems_type"];
		if($userId==-1) $userId=$_SESSION["shopItems_userId"];
		if($confirm==-1) $confirm=$_SESSION["shopItems_confirm"];
		if($searchWord==-1) $searchWord=@$_SESSION["shopItems_searchWord"];
      if($page==-1) $page=$_SESSION["shopItems_page"];
		
		//init session for auto session
		$_SESSION["shopItems_type"]=$type;
		$_SESSION["shopItems_userId"]=$userId;
		$_SESSION["shopItems_confirm"]=$confirm;
      $_SESSION["shopItems_searchWord"]=$searchWord;
      $_SESSION["shopItems_page"]=$page;
		
		//btn new
		if($type=='0')
		   $btnNew="<button type='button' class='btn btn-success' onclick='shopItems_new0();'><i class='fa fa-plus'></i>&nbsp;مقاله جدید</button>";
		else if($type=='1')   
			$btnNew="<button type='button' class='btn btn-success' onclick='shopItems_new1();'><i class='fa fa-plus'></i>&nbsp;پایان نامه جدید</button>";
		else
		{
			$btnNew="
			<button type='button' class='btn btn-success' onclick='shopItems_new0();'><i class='fa fa-plus'></i>&nbsp;مقاله جدید</button>
			<button type='button' class='btn btn-success' onclick='shopItems_new1();'><i class='fa fa-plus'></i>&nbsp;پایان نامه جدید</button>
			";
		}
		$title='محصولات';
		
		//by user
		if($userId != $_SESSION['user_id']) $userId='';
		$slctTypeFilter='';		
		if($userId > 0 && $userId != '') 
		{
			//filter confirm
			foreach($arryTypeFilter as $key=>$val)
			{
				if($type==$key && $type!=='')	$selected='selected'; else $selected='';
				$slctTypeFilter.="<option value='{$key}' {$selected}>{$val}</option>";
			}	
		   $slctTypeFilter="<select id='slct_confirmFilter' onchange='shopItems_draw(\"auto\",this.value);'>{$slctTypeFilter}</select>";				
			
			//title by confirm
			$title=$arryConfirmFilter[$confirm];
		}
		
		//title+
		if($title)
		   $title= @$arryTypeTitle[$type] . '، ' . $title;
	   else
		   $title= @$arryTypeTitle[$type];
		
		//START PAGE ...................................
		if($page < 1) $page=1;
      $arg=array(
			"userId"=>$userId,
			"type"=>$type,
			"sortByRow"=>true,
			"active"=>1,
			"confirm"=>$confirm,
			"trash"=>0,
			"searchWord"=>$searchWord,
			"limitStr"=>""
		);		
      $items=$oShopItems->getAll($arg);
      $count=count($items);
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
		
		if($pages["pagesEnd"] > 1)
		{
			for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='shopItems_draw({\"userId\":-1,\"confirm\":-1,\"searchWord\":-1,\"page\":{$i}});'>{$i}</a>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='shopItems_draw({\"userId\":-1,\"confirm\":-1,\"searchWord\":-1,\"page\":{$back}});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='shopItems_draw({\"userId\":-1,\"confirm\":-1,\"searchWord\":-1,\"page\":{$next}});'><i class='fa fa-angle-left'></i></a>";		
		}
      $arg=array(
			"userId"=>$userId,
			"type"=>$type,
			"sortByRow"=>true,
			"active"=>1,
			"confirm"=>$confirm,
			"trash"=>0,
			"searchWord"=>$searchWord,
			"limitStr"=>$pages['sqlLimit']
		);	      
		$items=$oShopItems->getAll($arg);
		
      //END PAGE ...................................
		
      $codeTr="";
      $i=0;

		foreach($items as $item)
		{
			$item=$oShopItems->calc($item); //calc price and discount
			if($userId == '' && $item->isFree==true) break;
			
			$itemId=$item->id;
			$countLikes=count($oShopItems->likes_getAll($itemId));
			$btnLikesDisable= $countLikes ? "" : "disabled";

			$countOpine_new=count($oShopItems->opine_getAll($item->id,"","0"));
			$countOpine_confirm=count($oShopItems->opine_getAll($item->id,"1","1"));
			$countOpine_notConfirm=count($oShopItems->opine_getAll($item->id,"0","1"));
			$btnOpineNewDisable= $countOpine_new ? "" : "disabled";
			$btnOpineConfirmDisable= $countOpine_confirm ? "" : "disabled";
			$btnOpineNotConfirmDisable= $countOpine_notConfirm ? "" : "disabled";
			
			//price
			if($item->isFree==true)
				$price='رایگان';
			else
			{
				//price
				$price=$item->price_str . ' تومان';
	
				//discount
				if($item->priceByDiscount_str != '') 
				{
					$priceByDiscount= "{$item->priceByDiscount_str} تومان<br>({$item->discount}%)";
					$price= "<span class='fg-danger'>{$price}</span>" . "<hr><span class='fg-success'>{$priceByDiscount}</span>";
				}	
			}
			
			//special
			if($item->isSpecial=='1') $htmlSpecial='<i class="fa fa-star fg-warning"></i>'; else $htmlSpecial='';
			
			//image info
			if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$item->id}.jpg")))
				$html_img="<img class='brd-radius-8' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$item->id}.jpg?t=" . time() ) ."' style='width:86px' />";
			else
				$html_img="<img class='brd-radius-8' src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";
			
			//group info
			$html_group='';
         $groups=explode('|',$item->groupId);
			$count=count($groups);
			for($i=0;$i < $count;$i++)
			{
				$groupId=$groups[$i];
				if($group=@$oShopGroup->get($groupId))
					$html_group.="<span>{$group->title}</span>، ";
			}
			$html_group=rtrim($html_group,"، ");
			
			//copy link & comment for marketer
			$content=@file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContent_{$item->id}.html"));
			$link=$oPath->root() . "/item?i={$itemId}&uid={$_SESSION['user_id']}";
			$html_copy="<textarea id='txt_content' class='hide'>{$content}</textarea>";
			$html_copy.="<button class='btn btn-info btn-block' onclick='oTools.copy($(\"#txt_content\").val());script_alert2(\"\",\"کپی شد\",\"success\");' >کپی توضیحات محصول</button>";
			$html_copy.="<button class='btn btn-info btn-block' onclick='oTools.copy(\"{$link}\");script_alert2(\"\",\"کپی شد\",\"success\");' >کپی لینک محصول</button>";
			
			$codeTr.="
			<tr>
				<td style='padding:10px;min-width:120px;'>
					{$html_img}<br>
					{$item->titleFa}";
					if($userId)
					{
						$codeTr.="
						<hr>
						<i class='fa fa-heart'> {$countLikes}</i>
						&nbsp;&nbsp;
						<i class='fa fa-eye'> {$item->visits}</i>
						&nbsp;&nbsp;
						<i class='fa fa-shopping-cart'> {$item->countPay}</i>
						";
					}
				$codeTr.="
				</td>
				<td style='padding:10px;min-width:80px;'>{$item->id}</td>
				<td style='padding:10px;min-width:80px;'>{$html_group}</td>
				<td style='padding:10px;min-width:100px;'>{$price}</td>
				<td style='padding:10px;min-width:100px;'>{$item->marketerCommission_str} تومان</td>";
				if($userId) $codeTr.="<td>{$item->siteCommission_str} تومان</td>";
				if($userId) $codeTr.="<td>{$item->siteCommission_byMarketer_str} تومان</td>";
			   $codeTr.="<td style='padding:10px;'>{$html_copy}</td>";
				if($userId) $codeTr.="<td><button class='btn btn-warning' onclick='shopItems_edit{$item->type}({$item->id});'><i class='fa fa-pencil'></i></button></td>";
			$codeTr.="
			</tr>";
		}
      if($codeTr)
		{
			$chkCount=$i;
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>
			
			{$btnNew}
			{$slctTypeFilter}
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopItems_draw(\"auto\",undefined,$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>

			<div class='vSpace'></div>
			
			<div class='vScrollBar-auto'>
				<table class='tbl tbl-center tbl-bordered tbl-hover'>
					<tr>";
						if(count($items) > 1) $code.="<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>";
						$code.="
						<th></th>
						<th>شناسه محصول</th>
						<th>دسته بندی ها</th>
						<th>قیمت</th>";
						if($userId) $code.="<th>پورسانت بازار یاب</th>"; else $code.="<th>پورسانت شما</th>";
						if($userId) $code.="<th>پورسانت شما در فروش مستقیم</th>";
						if($userId) $code.="<th>پورسانت شما در فروش بازاریاب</th>";
						$code.="<th>کپی</th>";
						if($userId) $code.="<th>ویرایش</th>";
					$code.="
					</tr>
					{$codeTr}
				</table>
			<div>
			<br>{$btnPageCode}<br> 
			<div class='vSpace-4x'></div>";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>

			{$btnNew}
			{$slctTypeFilter}
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopItems_draw(\"auto\",undefined,$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'></div>";		
		}
		cEngine::response("ok[|]" . $code);
		exit;
   }//------------------------------------------------------------------------------------	
   else if($request=="shopItems_del")
   {
      $itemId=$oDb->escape($_REQUEST["id"],false);
      $ids=$oShopItems->delete($itemId);
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId)
			{
				//images
				for($ii=0;$ii <= 5;$ii++)
				{
					@unlink ($oPath->manageDir("shop_bundle/data/images/item{$ii}_{$selectedId}.jpg"));
					@unlink ($oPath->manageDir("shop_bundle/data/images/item{$ii}Larg_{$selectedId}.jpg"));
					@unlink ($oPath->manageDir("shop_bundle/data/images/item{$ii}Thumb_{$selectedId}.jpg"));
				}
				@unlink ($oPath->manageDir("shop_bundle/data/images/itemQr_{$selectedId}.jpg"));
				//attaches
				$item=$oShopItems->get($selectedId);
				@unlink($oPath->manageDir("shop_bundle/data/{$item->fileName1}"));
				@unlink($oPath->manageDir("shop_bundle/data/{$item->fileName2}"));
				@unlink($oPath->manageDir("shop_bundle/data/{$item->attachName1}"));
				@unlink($oPath->manageDir("shop_bundle/data/{$item->attachName1}"));
				//contents
				@unlink($oPath->manageDir("shop_bundle/data/contents/otherProperties_{$selectedId}.json"));
				@unlink($oPath->manageDir("shop_bundle/data/contents/itemContent_{$selectedId}.html"));
				@unlink($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$selectedId}.html"));
				@unlink($oPath->manageDir("shop_bundle/data/contents/itemContentSampleEn_{$selectedId}.html"));		
				//opines
				$opines=$oShopItems->opine_deleteByItemId($selectedId);
				$count=count($opines);
				for($ii=0;$ii < $count;$ii++)
				{
					@unlink ($oPath->manageDir("shop_bundle/data/contents/opineAnswer_{$opines[$ii]->id}.html"));
				   @unlink ($oPath->manageDir("shop_bundle/data/contents/opineContent_{$opines[$ii]->id}.html"));
				}
				//likes
				$oShopItems->likes_deleteByItemId($selectedId);		
			}
		}      
		cEngine::response('ok');
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_trash")
   {
      $itemId=$oDb->escape($_REQUEST["id"],false);
      $ids=$oShopItems->trash($itemId);      
		cEngine::response('ok');
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_unTrash")
   {
      $itemId=$oDb->escape($_REQUEST["id"],false);
      $ids=$oShopItems->unTrash($itemId);      
		cEngine::response('ok');
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_edit0") //type 1 or maghalat
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $item=$oShopItems->get($itemId);
      
		//images
		$t=time();
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$itemId}.jpg")))
         $img0="<img id='img_item0' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel0' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},0);' style='cursor:pointer;color:red;'></i>";
      else
         $img0="<img id='img_item0' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item1Thumb_{$itemId}.jpg")))
         $img1="<img id='img_item1' src='" . $oPath->manage("shop_bundle/data/images/item1Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel1' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},1);' style='cursor:pointer;color:red;'></i>";
      else
         $img1="<img id='img_item1' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item2Thumb_{$itemId}.jpg")))
         $img2="<img id='img_item2' src='" . $oPath->manage("shop_bundle/data/images/item2Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel2' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},2);' style='cursor:pointer;color:red;'></i>";
      else
         $img2="<img id='img_item2' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item3Thumb_{$itemId}.jpg")))
         $img3="<img id='img_item3' src='" . $oPath->manage("shop_bundle/data/images/item3Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel3' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},3);' style='cursor:pointer;color:red;'></i>";
      else
         $img3="<img id='img_item3' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item4Thumb_{$itemId}.jpg")))
         $img4="<img id='img_item4' src='" . $oPath->manage("shop_bundle/data/images/item4Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel4' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},4);' style='cursor:pointer;color:red;'></i>";
      else
         $img4="<img id='img_item4' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item5Thumb_{$itemId}.jpg")))
         $img5="<img id='img_item5' src='" . $oPath->manage("shop_bundle/data/images/item5Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel4' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},5);' style='cursor:pointer;color:red;'></i>";
      else
         $img5="<img id='img_item5' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
		
		//attach
		$fileLlink1='';
		if($item->fileName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->fileName1}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->fileName1}");
			$fileLlink1="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->fileName1}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		//---
		$fileLlink2='';
		if($item->fileName2 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->fileName2}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->fileName2}");
			$fileLlink2="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->fileName2}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}	
		//---
		$fileFreeLlink1='';
		if($item->attachName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->attachName1}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->attachName1}");
			$fileFreeLlink1="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->attachName1}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		//---
		$fileFreeLlink2='';
		if($item->attachName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->attachName2}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->attachName2}");
			$fileFreeLlink2="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->attachName2}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		
      //content
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html")))
         $content=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html"));
      else 
			$content="";
		
      //sample content farsi
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$itemId}.html")))
         $contentSampleFa=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$itemId}.html"));
      else 
			$contentSampleFa="";

      //sample content english
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContentSampleEn_{$itemId}.html")))
         $contentSampleEn=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContentSampleEn_{$itemId}.html"));
      else 
			$contentSampleEn="";		
				
		//group
		$html_group=$oShopTools->group_chksDraw(0,explode('|',$item->groupId));

      //publication
		$publication=$oShopPublication->getAll();
		$count=count($publication);
		$publication_html='';
		$publicationName_value=$item->publicationName;
		for($i=0;$i < $count;$i++)
		{
			if($item->publicationName == $publication[$i]->title) 
			{
				$selected='selected';
				$publicationName_value='';				
			}
			else 
				$selected='';
			$publication_html.="<option value='{$publication[$i]->title}' {$selected}>{$publication[$i]->title}</option>";
		}
		$publication_html="<select id='slct_publicationName' onchange='if(this.value==0) $(\"#txt_publicationName\").css(\"opacity\",1); else $(\"#txt_publicationName\").css(\"opacity\",0.3);'><option value='0'></option>{$publication_html}</select>";

      //mortabet ha, listLinked
		$listLinked=str_replace('|',',',$item->listLinked);

      //json
		$json=str_replace('|',',',$item->json);
      $json=htmlspecialchars_decode($json);		
		
		//other checkbox
      if($item->active=="1") $chk_active="checked='checked'"; else $chk_active="";		
      if($item->isSpecial=="1") $chk_isSpecial="checked='checked'"; else $chk_isSpecial="";			
      if($item->isSource=="1") $chk_isSource="checked='checked'"; else $chk_isSource="";			
      if($item->isFree=="1") $chk_isFree="checked='checked'"; else $chk_isFree="";			
      if($item->notDiscount=="1") $chk_notDiscount="checked='checked'"; else $chk_notDiscount="";	
		
		//other properties
		$otherProperties=@file_get_contents($oPath->manageDir("shop_bundle/data/contents/otherProperties_{$item->id}.json"));
		$otherProperties=@json_decode($otherProperties,true);
		$itemsOther=$oShopItemsOther->getAll(1,true);
		$count=count($itemsOther);
		$itemsOther_code="";
		for($i=0;$i < $count;$i++)
		{
			$itemOther=$itemsOther[$i];
			$value=@$otherProperties[$itemOther->id];
			$itemsOther_code.="
			<label>{$itemOther->title}</label>
			<input type='text' id='txt_otherValue{$i}' value='{$value}'>
			<input type='hidden' id='txt_otherId{$i}' value='{$itemOther->id}'>
			";
		}
		$itemsOther_code.="<input type='hidden' id='txt_otherCount' value='{$count}'>";
		
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil-square-o'></i>&nbsp;ویرایش مقاله {$item->titleFa}</h1>		
		<div class='vSpace-4x'></div>
 
		<div class='form form-w5 dir-rtl algn-r'>
			<div class='part-r part-w-5'>
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>پیشوند عنوان</label>				
							<input type='text' id='txt_titleBefor'	value='{$item->titleBefor}'>
							<label>عنوان</label>				
							<input type='text' id='txt_titleFa'	value='{$item->titleFa}' required><span>*</span>
			  
							<div class='vSpace'></div>
			  
							<label>عنوان انگلیسی</label>				
							<input type='text' id='txt_titleEn'	value='{$item->titleEn}' class='dir-ltr'>
							
							<div class='vSpace'></div>
			  
							<label>تعداد صفحات فارسی</label>				
							<input type='number' id='txt_countPageFa'	value='{$item->countPageFa}' class='dir-ltr'>
							
							<div class='vSpace'></div>
			  
							<label>تعداد صفحات انگلیسی</label>				
							<input type='number' id='txt_countPageEn'	value='{$item->countPageEn}' class='dir-ltr'>
							
							<div class='vSpace'></div>
			  
							<label>مشخصات محصول. در هر خط یک ویژگی وارد نمایید</label>				
							<textarea type='text' id='txt_property'>{$item->property}</textarea>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>شرح محصول</label>
							<textarea id='txt_content'>{$content}</textarea>
							<script>CKEDITOR.replace('txt_content');</script>
							
							<div class='vSpace'></div>
							
							<label>نمونه متن فارسی پروژه</label>
							<textarea id='txt_contentSampleFa'>{$contentSampleFa}</textarea>
							<script>CKEDITOR.replace('txt_contentSampleFa');</script>
							
							<div class='vSpace'></div>
							
							<label>نمونه متن انگلیسی پروژه</label>
							<textarea id='txt_contentSampleEn'>{$contentSampleEn}</textarea>
							<script>CKEDITOR.replace('txt_contentSampleEn');</script>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>فهرست فارسی. هر فهرست را در یک خط وارد نمایید</label>				
							<textarea type='text' id='txt_listFa'>{$item->listFa}</textarea>
							
							<label>فهرست انگلیسی. هر فهرست را در یک خط وارد نمایید</label>				
							<textarea type='text' id='txt_listEn' class='dir-ltr'>{$item->listEn}</textarea>

                     <label><input type='checkbox' id='chk_isSource' {$chk_isSource}>&nbsp;پروژه منبع دارد</label>							
						</div>
					</div>
				</div>

				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>محصولات مرتبط</h3>				
							<label>بر اساس id مطالب.</label>				
							<input type='text' id='txt_listLinked' class='dir-ltr' value='{$listLinked}'>						
						</div>
					</div>
				</div>				
			</div>
			
			<!-- ---------- -->
			
			<div class='part part-w-5'>
				<div class='part part-w-10 content-small margin-b'>			
					<div class='panel'>
						<div class='panel-body'>
							<label>قیمت محصول</label>				
							<input type='text' id='txt_price' value='{$item->price}' class='dir-ltr'>&nbsp;تومان

							<label>تخفیف</label>				
							<input type='text' id='txt_discount' value='{$item->discount}' class='dir-ltr'>&nbsp;%
							
							<label><input type='checkbox' id='chk_isFree' {$chk_isFree}>&nbsp;محصول رایگان شود</label>
							<label><input type='checkbox' id='chk_notDiscount' {$chk_notDiscount}>&nbsp;محصول قابل تخفیف نیست</label>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>آپلود فایل های دانلود بعد از خرید</h3>
							<label>نوع فایل های مجاز : zip,rar</label>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attach1' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل 1</span>
								</label>	
								{$fileLlink1}			
							</div>
							<div class='vSpace'></div>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attach2' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل 2</span>
								</label>	
								{$fileLlink2}				
							</div>
							<hr>
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<label>می توانید بجای آپلود فایل، از لینک مستقیم استفاده کنید</label>
							
							<div class='vSpace'></div>
							
							<label>لینک فایل 1</label>
							<input type='text' id='txt_fileUrl1' class='dir-ltr' value='{$item->fileUrl1}'> 
							
							<label>لینک فایل 2</label>
							<input type='text' id='txt_fileUrl2' class='dir-ltr' value='{$item->fileUrl2}'> 							
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body' style='height:220px;overflow-y:auto;'>
						   <h3>دسته بندی <span class='fg-red'>*</span></h3>
							<label>دسته بندی های مورد نظر خود را انتخاب نمایید</label>
							<div id='layer_group' class='ul ul-right ul-tree'>{$html_group}</div>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>برچسب ها</h3>
							<label>کلمه مورد نظر را وارد نمایید و بر روی دکمه + کلیک کنید</label>
							<input type='hidden' id='txt_tags' value='{$item->keywords}'>
							<div class='input-control input-control-right part-w-10'>
								<span class='input-control-button fa fa-plus bg-info' onclick='oMyTag.insert($(\"#txt_tag\").val(),$(\"#txt_tag\").val());$(\"#txt_tag\").val(\"\");'></span>
								<input type='text' id='txt_tag' class='dir-rtl-i algn-r-i'> 							
							</div>
							<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
							<script>
								oMyTag=myTag({'elementId':'ul_toTags'});
								oMyTag.setByString('{$item->keywords}','|');
							</script>
						</div>
					</div>
				</div>
            <!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>تصویر شاخص محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label></label></td></tr>
								<tr><td>{$img0}</td></tr>
								<tr><td>
									<label class='lbl-file  lbl-file-right'>
										<input type='file' id='file_imgItem0' onchange='oTools.previewImgFile(\"file_imgItem0\",\"img_item0\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<hr>
							<h3>تصاویر گالری محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 1</label></td></tr>
								<tr><td>{$img1}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem1' onchange='oTools.previewImgFile(\"file_imgItem1\",\"img_item1\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 2</label></td></tr>
								<tr><td>{$img2}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem2' onchange='oTools.previewImgFile(\"file_imgItem2\",\"img_item2\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 3</label></td></tr>
								<tr><td>{$img3}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem3' onchange='oTools.previewImgFile(\"file_imgItem3\",\"img_item3\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 4</label></td></tr>
								<tr><td>{$img4}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem4' onchange='oTools.previewImgFile(\"file_imgItem4\",\"img_item4\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>	
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 5</label></td></tr>
								<tr><td>{$img5}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem5' onchange='oTools.previewImgFile(\"file_imgItem5\",\"img_item5\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body' style='height:220px;overflow-y:auto;'>
						   <h3>فایل های قابل دانلود قبل ااز خرید</h3>
							<label>نوع فایل های مجاز : doc,pdf</label>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attachFree1' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل pdf</span>
								</label>	
								<label class='algn-r'>{$fileFreeLlink1}</label>			
							</div>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attachFree2' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل word</span>
								</label>	
								<label class='algn-r'>{$fileFreeLlink2}</label>				
							</div>
							<hr>
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<label>می توانید بجای آپلود فایل، از لینک مستقیم استفاده کنید</label>
							<div class='vSpace'></div>

							<label>انتخاب فایل pdf</label>
							<input type='text' id='txt_attachUrl1' class='dir-ltr' value='{$item->attachUrl1}'> 

							<label>انتخاب فایل word</label>
							<input type='text' id='txt_attachUrl2' class='dir-ltr' value='{$item->attachUrl2}'>							
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>جزئیات مقاله</h3>
							
							<label>doi</label>
							<input type='text' id='txt_doi' value='{$item->doi}' class='dir-ltr'>
							
							<label>json</label>
							<textarea id='txt_json' class='dir-ltr'>{$json}</textarea>

							<label>نام نشریه</label>
							{$publication_html}
							<div>یا</div>
							<input type='text' id='txt_publicationName' value='{$publicationName_value}' class='dir-ltr'>
							
							<div class='vSpace'></div>
							
							<label>دانشگاه</label>
							<input type='text' id='txt_university' value='{$item->university}'>

							<label>مجله</label>
							<input type='text' id='txt_gazette' value='{$item->gazette}' class='dir-ltr'>

							<label>سال انتشار، مثال: 1396</label>
							<input type='text' id='txt_publicationYear' value='{$item->publicationYear}' class='dir-ltr'>

							<label>لینک مرجع</label>
							<input type='text' id='txt_marjaLink' value='{$item->marjaLink}' class='dir-ltr'>							
						</div>
					</div>
				</div>
				<!-- ----- -->";
				if($itemsOther_code) $code.="
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>خصوصیات متفرقه</h3>
							{$itemsOther_code}
						</div>
					</div>
				</div>
			</div>			
		</div>
		
		<div class='part part-w-10'>
		   <div class='vSpace-4x'></div>
			<hr>
			<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
			<button class='btn btn-success' onclick='shopItems_update(\"edit\",{$item->id},0);'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>			
			<div class='vSpace-4x'></div>
		</div>		
		";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_edit1") //type1 payan nameh
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $item=$oShopItems->get($itemId);
      
		//images
		$t=time();
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$itemId}.jpg")))
         $img0="<img id='img_item0' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel0' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},0);' style='cursor:pointer;color:red;'></i>";
      else
         $img0="<img id='img_item0' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item1Thumb_{$itemId}.jpg")))
         $img1="<img id='img_item1' src='" . $oPath->manage("shop_bundle/data/images/item1Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel1' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},1);' style='cursor:pointer;color:red;'></i>";
      else
         $img1="<img id='img_item1' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item2Thumb_{$itemId}.jpg")))
         $img2="<img id='img_item2' src='" . $oPath->manage("shop_bundle/data/images/item2Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel2' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},2);' style='cursor:pointer;color:red;'></i>";
      else
         $img2="<img id='img_item2' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item3Thumb_{$itemId}.jpg")))
         $img3="<img id='img_item3' src='" . $oPath->manage("shop_bundle/data/images/item3Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel3' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},3);' style='cursor:pointer;color:red;'></i>";
      else
         $img3="<img id='img_item3' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item4Thumb_{$itemId}.jpg")))
         $img4="<img id='img_item4' src='" . $oPath->manage("shop_bundle/data/images/item4Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel4' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},4);' style='cursor:pointer;color:red;'></i>";
      else
         $img4="<img id='img_item4' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item5Thumb_{$itemId}.jpg")))
         $img5="<img id='img_item5' src='" . $oPath->manage("shop_bundle/data/images/item5Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel4' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},5);' style='cursor:pointer;color:red;'></i>";
      else
         $img5="<img id='img_item5' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
		
		//attach
		$fileLlink1='';
		if($item->fileName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->fileName1}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->fileName1}");
			$fileLlink1="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->fileName1}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		//---
		$fileLlink2='';
		if($item->fileName2 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->fileName2}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->fileName2}");
			$fileLlink2="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->fileName2}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		//---
		$fileFreeLlink1='';
		if($item->attachName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->attachName1}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->attachName1}");
			$fileFreeLlink1="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->attachName1}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		//---
		$fileFreeLlink2='';
		if($item->attachName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->attachName2}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->attachName2}");
			$fileFreeLlink2="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->attachName2}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		
      //content
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html")))
         $content=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html"));
      else 
			$content="";

      //sample content farsi
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$itemId}.html")))
         $contentSampleFa=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$itemId}.html"));
      else 
			$contentSampleFa="";

      //sample content english
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContentSampleEn_{$itemId}.html")))
         $contentSampleEn=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContentSampleEn_{$itemId}.html"));
      else 
			$contentSampleEn="";		
	
		//group
		$html_group=$oShopTools->group_chksDraw(0,explode('|',$item->groupId));

      //publication
		$publication=$oShopPublication->getAll();
		$count=count($publication);
		$publication_html='';
		$publicationName_value=$item->publicationName;
		for($i=0;$i < $count;$i++)
		{
			if($item->publicationName == $publication[$i]->title) 
			{
				$selected='selected';
				$publicationName_value='';				
			}
			else 
				$selected='';
			$publication_html.="<option value='{$publication[$i]->title}' {$selected}>{$publication[$i]->title}</option>";
		}
		$publication_html="<select id='slct_publicationName' onchange='if(this.value==0) $(\"#txt_publicationName\").css(\"opacity\",1); else $(\"#txt_publicationName\").css(\"opacity\",0.3);'><option value='0'></option>{$publication_html}</select>";

      //mortabet ha, listLinked
		$listLinked=str_replace('|',',',$item->listLinked);

      //json
		$json=str_replace('|',',',$item->json);
      $json=htmlspecialchars_decode($json);		
		
		//other checkbox
      if($item->active=="1") $chk_active="checked='checked'"; else $chk_active="";		
      if($item->isSpecial=="1") $chk_isSpecial="checked='checked'"; else $chk_isSpecial="";			
      if($item->isSource=="1") $chk_isSource="checked='checked'"; else $chk_isSource="";			
      if($item->isFree=="1") $chk_isFree="checked='checked'"; else $chk_isFree="";			
      if($item->notDiscount=="1") $chk_notDiscount="checked='checked'"; else $chk_notDiscount="";	
		
		//other properties
		$otherProperties=@file_get_contents($oPath->manageDir("shop_bundle/data/contents/otherProperties_{$item->id}.json"));
		$otherProperties=@json_decode($otherProperties,true);
		$otherProperties=isset($otherProperties) ? $otherProperties : [];
		$itemsOther=@$oShopItemsOther->getAll(1,true);
		$count=count($itemsOther);
		$itemsOther_code="";
		for($i=0;$i < $count;$i++)
		{
			$itemOther=$itemsOther[$i];
			$value=@$otherProperties[$itemOther->id];
			$itemsOther_code.="
			<label>{$itemOther->title}</label>
			<input type='text' id='txt_otherValue{$i}' value='{$value}'>
			<input type='hidden' id='txt_otherId{$i}' value='{$itemOther->id}'>
			";
		}
		$itemsOther_code.="<input type='hidden' id='txt_otherCount' value='{$count}'>";
		
      $code= "
		<div class='vSpace-4x'></div>
		<div class='title-h1 dir-rtl algn-c'><i class='fa fa-pencil-square-o'></i>&nbsp;ویرایش پایان نامه {$item->titleFa}</div>		
		<div class='vSpace-4x'></div>
 
		<div class='form form-w5 dir-rtl algn-r'>
			<div class='part-r part-w-5'>
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>پیشوند عنوان</label>				
							<input type='text' id='txt_titleBefor'	value='{$item->titleBefor}'>
							<label>عنوان</label>				
							<input type='text' id='txt_titleFa'	value='{$item->titleFa}' required><span>*</span>
							
							<div class='vSpace'></div>
			  
							<label>تعداد صفحات</label>				
							<input type='number' id='txt_countPageFa'	value='{$item->countPageFa}' class='dir-ltr'>
							
							<div class='vSpace'></div>
			  
							<label>مشخصات محصول. در هر خط یک ویژگی وارد نمایید</label>				
							<textarea type='text' id='txt_property'>{$item->property}</textarea>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>شرح محصول</label>
							<textarea id='txt_content'>{$content}</textarea>
							<script>CKEDITOR.replace('txt_content');</script>
							
							<div class='vSpace'></div>
							
							<label>نمونه متن پروژه</label>
							<textarea id='txt_contentSampleFa'>{$contentSampleFa}</textarea>
							<script>CKEDITOR.replace('txt_contentSampleFa');</script>
							
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>فهرست فارسی. هر فهرست را در یک خط وارد نمایید</label>				
							<textarea type='text' id='txt_listFa'>{$item->listFa}</textarea>
						
                     <label><input type='checkbox' id='chk_isSource' {$chk_isSource}>&nbsp;پروژه منبع دارد</label>							
						</div>
					</div>
				</div>

				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>محصولات مرتبط</h3>				
							<label>بر اساس id مطالب.</label>				
							<input type='text' id='txt_listLinked' class='dir-ltr' value='{$listLinked}'>						
						</div>
					</div>
				</div>				
			</div>
			
			<!-- ---------- -->
			
			<div class='part part-w-5'>
				<div class='part part-w-10 content-small margin-b'>			
					<div class='panel'>
						<div class='panel-body'>
							<label>قیمت محصول</label>				
							<input type='text' id='txt_price' value='{$item->price}' class='dir-ltr'>&nbsp;تومان

							<label>تخفیف</label>				
							<input type='text' id='txt_discount' value='{$item->discount}' class='dir-ltr'>&nbsp;%
							
							<label><input type='checkbox' id='chk_isFree' {$chk_isFree}>&nbsp;محصول رایگان شود</label>
							<label><input type='checkbox' id='chk_notDiscount' {$chk_notDiscount}>&nbsp;محصول قابل تخفیف نیست</label>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>آپلود فایل های دانلود بعد از خرید</h3>
							<label>نوع فایل های مجاز : zip,rar</label>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attach1' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل 1</span>
								</label>	
								{$fileLlink1}			
							</div>
							<div class='vSpace'></div>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attach2' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل 2</span>
								</label>	
								{$fileLlink2}				
							</div>
							<hr>
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<label>می توانید بجای آپلود فایل، از لینک مستقیم استفاده کنید</label>
							
							<div class='vSpace'></div>
							
							<label>لینک فایل 1</label>
							<input type='text' id='txt_fileUrl1' class='dir-ltr' value='{$item->fileUrl1}'> 
							
							<label>لینک فایل 2</label>
							<input type='text' id='txt_fileUrl2' class='dir-ltr' value='{$item->fileUrl2}'> 							
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body' style='height:220px;overflow-y:auto;'>
						   <h3>دسته بندی <span class='fg-red'>*</span></h3>
							<label>دسته بندی های مورد نظر خود را انتخاب نمایید</label>
							<div id='layer_group' class='ul ul-right ul-tree'>{$html_group}</div>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>برچسب ها</h3>
							<label>کلمه مورد نظر را وارد نمایید و بر روی دکمه + کلیک کنید</label>
							<input type='hidden' id='txt_tags' value='{$item->keywords}'>
							<div class='input-control input-control-right part-w-10'>
								<span class='input-control-button fa fa-plus bg-info' onclick='oMyTag.insert($(\"#txt_tag\").val(),$(\"#txt_tag\").val());$(\"#txt_tag\").val(\"\");'></span>
								<input type='text' id='txt_tag' class='dir-rtl-i algn-r-i'> 							
							</div>
							<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
							<script>
								oMyTag=myTag({'elementId':'ul_toTags'});
								oMyTag.setByString('{$item->keywords}','|');
							</script>
						</div>
					</div>
				</div>
            <!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>تصویر شاخص محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label></label></td></tr>
								<tr><td>{$img0}</td></tr>
								<tr><td>
									<label class='lbl-file  lbl-file-right'>
										<input type='file' id='file_imgItem0' onchange='oTools.previewImgFile(\"file_imgItem0\",\"img_item0\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<hr>
							<h3>تصاویر گالری محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 1</label></td></tr>
								<tr><td>{$img1}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem1' onchange='oTools.previewImgFile(\"file_imgItem1\",\"img_item1\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 2</label></td></tr>
								<tr><td>{$img2}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem2' onchange='oTools.previewImgFile(\"file_imgItem2\",\"img_item2\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 3</label></td></tr>
								<tr><td>{$img3}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem3' onchange='oTools.previewImgFile(\"file_imgItem3\",\"img_item3\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 4</label></td></tr>
								<tr><td>{$img4}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem4' onchange='oTools.previewImgFile(\"file_imgItem4\",\"img_item4\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>	
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 5</label></td></tr>
								<tr><td>{$img5}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem5' onchange='oTools.previewImgFile(\"file_imgItem5\",\"img_item5\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body' style='height:220px;overflow-y:auto;'>
						   <h3>فایل های قابل دانلود قبل از خرید</h3>
							<label>نوع فایل های مجاز : doc,pdf</label>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attachFree1' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل pdf</span>
								</label>	
								<label class='algn-r'>{$fileFreeLlink1}</label>			
							</div>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attachFree2' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل word</span>
								</label>	
								<label class='algn-r'>{$fileFreeLlink2}</label>				
							</div>
							<hr>
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<label>می توانید بجای آپلود فایل، از لینک مستقیم استفاده کنید</label>
							<div class='vSpace'></div>

							<label>انتخاب فایل pdf</label>
							<input type='text' id='txt_attachUrl1' class='dir-ltr' value='{$item->attachUrl1}'> 

							<label>انتخاب فایل word</label>
							<input type='text' id='txt_attachUrl2' class='dir-ltr' value='{$item->attachUrl2}'>							
						</div>
					</div>
				</div>				
				<!-- ----- -->";
				if($itemsOther_code) $code.="
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>خصوصیات متفرقه</h3>
							{$itemsOther_code}
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class='part part-w-10'>
			<div class='vSpace-4x'></div>
			<hr>
			<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
			<button class='btn btn-success' onclick='shopItems_update(\"edit\",{$item->id},1);'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>			
			<div class='vSpace-4x'></div>
		</div>			
		";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_new0") //maghaleh
   {
		$backEvent=@$_REQUEST['backEvent'];
		if(!$backEvent) 
			$backEvent='shopItems_draw("auto");';
		else
			$backEvent='oTools.link("' . $backEvent . '");';
		
		//images
		$t=time();

      $img0="<img id='img_item0' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img1="<img id='img_item1' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img2="<img id='img_item2' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img3="<img id='img_item3' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img4="<img id='img_item4' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img5="<img id='img_item5' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
										
      //content
		$content="";
		
      //sample content farsi
		$contentSampleFa="";

      //sample content english
		$contentSampleEn="";		
				
		//group
		$html_group=$oShopTools->group_chksDraw(0,[]);

      //publication
		$publication=$oShopPublication->getAll();
		$count=count($publication);
		$selected='';
		$publication_html='';
		for($i=0;$i < $count;$i++)
		{
			$publication_html.="<option value='{$publication[$i]->title}' {$selected}>{$publication[$i]->title}</option>";
		}
		$publication_html="<select id='slct_publicationName' onchang='if(this.value!=0) $(\"#txt_publicatioName\").css(\"opacity\",0.3); else $(\"#txt_publicationName\").css(\"opacity\",1);'><option value='0'></option>{$publication_html}</select>";
		$publicationName_value='';

      //mortabet ha, listLinked
		$listLinked='';
		
		//other checkbox
      $chk_active="";		
      $chk_isSpecial="";			
      $chk_isSource="";			
      $chk_isFree="";			
      $chk_notDiscount="";	
		
		//other properties
		$itemsOther=$oShopItemsOther->getAll(1,true);
		$count=count($itemsOther);
		$itemsOther_code="";
		for($i=0;$i < $count;$i++)
		{
			$itemOther=$itemsOther[$i];
			$itemsOther_code.="
			<label>{$itemOther->title}</label>
			<input type='text' id='txt_otherValue{$i}' value=''>
			<input type='hidden' id='txt_otherId{$i}' value='{$itemOther->id}'>
			";
		}
		$itemsOther_code.="<input type='hidden' id='txt_otherCount' value='{$count}'>";
		
		
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-plus'></i>&nbsp;مقاله جدید</h1>		
		<div class='vSpace-4x'></div>
 
 
		<div class='form form-w5 dir-rtl algn-r'>
			<div class='part-r part-w-5'>
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>پیشوند عنوان</label>				
							<input type='text' id='txt_titleBefor'	value=''>
							<label>عنوان</label>				
							<input type='text' id='txt_titleFa'	value='' required><span>*</span>
			  
							<div class='vSpace'></div>
			  
							<label>عنوان انگلیسی</label>				
							<input type='text' id='txt_titleEn'	value='' class='dir-ltr'>
							
							<div class='vSpace'></div>
			  
							<label>تعداد صفحات فارسی</label>				
							<input type='number' id='txt_countPageFa'	value='' class='dir-ltr'>
							
							<div class='vSpace'></div>
			  
							<label>تعداد صفحات انگلیسی</label>				
							<input type='number' id='txt_countPageEn'	value='' class='dir-ltr'>
							
							<div class='vSpace'></div>
			  
							<label>مشخصات محصول. در هر خط یک ویژگی وارد نمایید</label>				
							<textarea type='text' id='txt_property'></textarea>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>شرح محصول</label>
							<textarea id='txt_content'>{$content}</textarea>
							<script>CKEDITOR.replace('txt_content');</script>
							
							<div class='vSpace'></div>
							
							<label>نمونه متن فارسی پروژه</label>
							<textarea id='txt_contentSampleFa'>{$contentSampleFa}</textarea>
							<script>CKEDITOR.replace('txt_contentSampleFa');</script>
							
							<div class='vSpace'></div>
							
							<label>نمونه متن انگلیسی پروژه</label>
							<textarea id='txt_contentSampleEn'>{$contentSampleEn}</textarea>
							<script>CKEDITOR.replace('txt_contentSampleEn');</script>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>فهرست فارسی. هر فهرست را در یک خط وارد نمایید</label>				
							<textarea type='text' id='txt_listFa'></textarea>
							
							<label>فهرست انگلیسی. هر فهرست را در یک خط وارد نمایید</label>				
							<textarea type='text' id='txt_listEn' class='dir-ltr'></textarea>

                     <label><input type='checkbox' id='chk_isSource' {$chk_isSource}>&nbsp;پروژه منبع دارد</label>							
						</div>
					</div>
				</div>

				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>محصولات مرتبط</h3>				
							<label>بر اساس id مطالب.</label>				
							<input type='text' id='txt_listLinked' class='dir-ltr'>						
						</div>
					</div>
				</div>				
			</div>
			
			<!-- ---------- -->
			
			<div class='part part-w-5'>
				<div class='part part-w-10 content-small margin-b'>			
					<div class='panel'>
						<div class='panel-body'>
							<label>قیمت محصول</label>				
							<input type='text' id='txt_price' value='' class='dir-ltr'>&nbsp;تومان

							<label>تخفیف</label>				
							<input type='text' id='txt_discount' value=''>&nbsp;%
							
							<label><input type='checkbox' id='chk_isFree' {$chk_isFree}>&nbsp;محصول رایگان شود</label>
							<label><input type='checkbox' id='chk_notDiscount' {$chk_notDiscount}>&nbsp;محصول قابل تخفیف نیست</label>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>آپلود فایل های دانلود بعد از خرید</h3>
							<label>نوع فایل های مجاز : zip,rar</label>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attach1' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل 1</span>
								</label>			
							</div>
							<div class='vSpace'></div>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attach2' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل 2</span>
								</label>				
							</div>
							<hr>
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<label>می توانید بجای آپلود فایل، از لینک مستقیم استفاده کنید</label>
							<div class='vSpace'></div>
							
							<label>لینک فایل 1</label>
							<input type='text' id='txt_fileUrl1' class='dir-ltr' value=''> 

							<label>لینک فایل 2</label>
							<input type='text' id='txt_fileUrl2' class='dir-ltr' value=''> 							
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body' style='height:220px;overflow-y:auto;'>
						   <h3>دسته بندی <span class='fg-red'>*</span></h3>
							<label>دسته بندی های مورد نظر خود را انتخاب نمایید</label>
							<div id='layer_group' class='ul ul-right ul-tree'>{$html_group}</div>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>برچسب ها</h3>
							<label>کلمه مورد نظر را وارد نمایید و بر روی دکمه + کلیک کنید</label>
							<div class='input-control input-control-right part-w-10'>
								<span class='input-control-button fa fa-plus bg-info' onclick='oMyTag.insert($(\"#txt_tag\").val(),$(\"#txt_tag\").val());$(\"#txt_tag\").val(\"\");'></span>
								<input type='text' id='txt_tag' class='dir-rtl-i algn-r-i'> 							
							</div>
							<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
							<script>oMyTag=myTag({'elementId':'ul_toTags'});</script>	
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>تصویر شاخص محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label></label></td></tr>
								<tr><td>{$img0}</td></tr>
								<tr><td>
									<label class='lbl-file  lbl-file-right'>
										<input type='file' id='file_imgItem0' onchange='oTools.previewImgFile(\"file_imgItem0\",\"img_item0\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<hr>
							<h3>تصاویر گالری محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 1</label></td></tr>
								<tr><td>{$img1}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem1' onchange='oTools.previewImgFile(\"file_imgItem1\",\"img_item1\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 2</label></td></tr>
								<tr><td>{$img2}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem2' onchange='oTools.previewImgFile(\"file_imgItem2\",\"img_item2\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 3</label></td></tr>
								<tr><td>{$img3}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem3' onchange='oTools.previewImgFile(\"file_imgItem3\",\"img_item3\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 4</label></td></tr>
								<tr><td>{$img4}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem4' onchange='oTools.previewImgFile(\"file_imgItem4\",\"img_item4\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>	
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 5</label></td></tr>
								<tr><td>{$img5}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem5' onchange='oTools.previewImgFile(\"file_imgItem5\",\"img_item5\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>فایل های قابل دانلود قبل از خرید</h3>
							<label>نوع فایل های مجاز : doc,pdf</label>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attachFree1' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل pdf</span>
								</label>	
								<label class='dir-ltr algn-r'></label>				
							</div>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attachFree2' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل word</span>
								</label>	
								<label class='dir-ltr algn-r'></label>				
							</div>
							<hr>
							<h3>لینک فایل های دانلود قبل از خرید</h3>
							<label>می توانید بجای آپلود فایل، از لینک مستقیم استفاده کنید</label>
							<div class='vSpace'></div>

							<label>انتخاب فایل pdf</label>
							<input type='text' id='txt_attachUrl1' class='dir-ltr' value=''> 

							<label>انتخاب فایل word</label>
							<input type='text' id='txt_attachUrl2' class='dir-ltr' value=''>							
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>جزئیات مقاله</h3>
							
							<label>doi</label>
							<input type='text' id='txt_doi' value='' class='dir-ltr'>
							
							<label>json</label>
							<textarea id='txt_json' class='dir-ltr'></textarea>

							<label>نام نشریه</label>
							{$publication_html}
							<div>یا</div>
							<input type='text' id='txt_publicationName' value='{$publicationName_value}' class='dir-ltr'>
							
							<div class='vSpace'></div>
							
							<label>دانشگاه</label>
							<input type='text' id='txt_university' value=''>

							<label>مجله</label>
							<input type='text' id='txt_gazette' value='' class='dir-ltr'>

							<label>سال انتشار</label>
							<input type='text' id='txt_publicationYear' value='' class='dir-ltr'>

							<label>لینک مرجع</label>
							<input type='text' id='txt_marjaLink' value='' class='dir-ltr'>							
						</div>
					</div>
				</div>
				<!-- ----- -->";
				if($itemsOther_code) $code.="
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>خصوصیات متفرقه</h3>
							{$itemsOther_code}
						</div>
					</div>
				</div>				
			</div>			
		</div>
		
		<div class='part part-w-10'>
			<div class='vSpace-4x'></div>
			<hr>
			<button class='btn btn-default' onclick='{$backEvent}'><i class='fa fa-arrow-right'></i></button>
			<button class='btn btn-success' onclick='shopItems_update(\"new\",0,0);'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>			
			<div class='vSpace-4x'></div>
		</div>		
		";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_new1") //payan nameh
   {
		$backEvent=@$_REQUEST['backEvent'];
		if(!$backEvent) 
			$backEvent='shopItems_draw("auto");';
		else
			$backEvent='oTools.link("' . $backEvent . '");';
		
		//images
		$t=time();

      $img0="<img id='img_item0' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img1="<img id='img_item1' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img2="<img id='img_item2' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img3="<img id='img_item3' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img4="<img id='img_item4' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img5="<img id='img_item5' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
										
      //content
		$content="";
		
      //sample content farsi
		$contentSampleFa="";

      //sample content english
		$contentSampleEn="";		
				
		//group
		$html_group=$oShopTools->group_chksDraw(0,[]);

      //publication
		$publication=$oShopPublication->getAll();
		$count=count($publication);
		$selected='';
		$publication_html='';
		for($i=0;$i < $count;$i++)
		{
			$publication_html.="<option value='{$publication[$i]->title}' {$selected}>{$publication[$i]->title}</option>";
		}
		$publication_html="<select id='slct_publicationName' onchang='if(this.value!=0) $(\"#txt_publicatioName\").css(\"opacity\",0.3); else $(\"#txt_publicationName\").css(\"opacity\",1);'><option value='0'></option>{$publication_html}</select>";
		$publicationName_value='';

      //mortabet ha, listLinked
		$listLinked='';
		
		//other checkbox
      $chk_active="";		
      $chk_isSpecial="";			
      $chk_isSource="";			
      $chk_isFree="";			
      $chk_notDiscount="";	
		
		//other properties
		$itemsOther=$oShopItemsOther->getAll(1,true);
		$count=count($itemsOther);
		$itemsOther_code="";
		for($i=0;$i < $count;$i++)
		{
			$itemOther=$itemsOther[$i];
			$itemsOther_code.="
			<label>{$itemOther->title}</label>
			<input type='text' id='txt_otherValue{$i}' value=''>
			<input type='hidden' id='txt_otherId{$i}' value='{$itemOther->id}'>
			";
		}
		$itemsOther_code.="<input type='hidden' id='txt_otherCount' value='{$count}'>";
		
		
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-plus'></i>&nbsp;پایان نامه جدید</h1>		
		<div class='vSpace-4x'></div>
 
 
		<div class='form form-w5 dir-rtl algn-r'>
			<div class='part-r part-w-5'>
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>پیشوند عنوان</label>				
							<input type='text' id='txt_titleBefor'	value=''>
							<label>عنوان</label>				
							<input type='text' id='txt_titleFa'	value='' required><span>*</span>
			  
							<div class='vSpace'></div>
			  
							<label>تعداد صفحات</label>				
							<input type='number' id='txt_countPageFa'	value='' class='dir-ltr'>

							<div class='vSpace'></div>
			  
							<label>مشخصات محصول. در هر خط یک ویژگی وارد نمایید</label>				
							<textarea type='text' id='txt_property'></textarea>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>شرح محصول</label>
							<textarea id='txt_content'>{$content}</textarea>
							<script>CKEDITOR.replace('txt_content');</script>
							
							<div class='vSpace'></div>
							
							<label>نمونه متن پروژه</label>
							<textarea id='txt_contentSampleFa'>{$contentSampleFa}</textarea>
							<script>CKEDITOR.replace('txt_contentSampleFa');</script>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>فهرست. هر فهرست را در یک خط وارد نمایید</label>				
							<textarea type='text' id='txt_listFa'></textarea>
							
                     <label><input type='checkbox' id='chk_isSource' {$chk_isSource}>&nbsp;پروژه منبع دارد</label>							
						</div>
					</div>
				</div>

				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>محصولات مرتبط</h3>				
							<label>بر اساس id مطالب.</label>				
							<input type='text' id='txt_listLinked' class='dir-ltr'>						
						</div>
					</div>
				</div>				
			</div>
			
			<!-- ---------- -->
			
			<div class='part part-w-5'>
				<div class='part part-w-10 content-small margin-b'>			
					<div class='panel'>
						<div class='panel-body'>
							<label>قیمت محصول</label>				
							<input type='text' id='txt_price' value='' class='dir-ltr'>&nbsp;تومان

							<label>تخفیف</label>				
							<input type='text' id='txt_discount' value=''>&nbsp;%
							
							<label><input type='checkbox' id='chk_isFree' {$chk_isFree}>&nbsp;محصول رایگان شود</label>
							<label><input type='checkbox' id='chk_notDiscount' {$chk_notDiscount}>&nbsp;محصول قابل تخفیف نیست</label>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>آپلود فایل های دانلود بعد از خرید</h3>
							<label>نوع فایل های مجاز : zip,rar</label>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attach1' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل 1</span>
								</label>			
							</div>
							<div class='vSpace'></div>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attach2' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل 2</span>
								</label>				
							</div>
							<hr>
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<label>می توانید بجای آپلود فایل، از لینک مستقیم استفاده کنید</label>
							<div class='vSpace'></div>
							
							<label>لینک فایل 1</label>
							<input type='text' id='txt_fileUrl1' class='dir-ltr' value=''> 

							<label>لینک فایل 2</label>
							<input type='text' id='txt_fileUrl2' class='dir-ltr' value=''> 							
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body' style='height:220px;overflow-y:auto;'>
						   <h3>دسته بندی <span class='fg-red'>*</span></h3>
							<label>دسته بندی های مورد نظر خود را انتخاب نمایید</label>
							<div id='layer_group' class='ul ul-right ul-tree'>{$html_group}</div>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>برچسب ها</h3>
							<label>کلمه مورد نظر را وارد نمایید و بر روی دکمه + کلیک کنید</label>
							<div class='input-control input-control-right part-w-10'>
								<span class='input-control-button fa fa-plus bg-info' onclick='oMyTag.insert($(\"#txt_tag\").val(),$(\"#txt_tag\").val());$(\"#txt_tag\").val(\"\");'></span>
								<input type='text' id='txt_tag' class='dir-rtl-i algn-r-i'> 							
							</div>
							<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
							<script>oMyTag=myTag({'elementId':'ul_toTags'});</script>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>تصویر شاخص محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label></label></td></tr>
								<tr><td>{$img0}</td></tr>
								<tr><td>
									<label class='lbl-file  lbl-file-right'>
										<input type='file' id='file_imgItem0' onchange='oTools.previewImgFile(\"file_imgItem0\",\"img_item0\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<hr>
							<h3>تصاویر گالری محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 1</label></td></tr>
								<tr><td>{$img1}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem1' onchange='oTools.previewImgFile(\"file_imgItem1\",\"img_item1\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 2</label></td></tr>
								<tr><td>{$img2}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem2' onchange='oTools.previewImgFile(\"file_imgItem2\",\"img_item2\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 3</label></td></tr>
								<tr><td>{$img3}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem3' onchange='oTools.previewImgFile(\"file_imgItem3\",\"img_item3\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 4</label></td></tr>
								<tr><td>{$img4}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem4' onchange='oTools.previewImgFile(\"file_imgItem4\",\"img_item4\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>	
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 5</label></td></tr>
								<tr><td>{$img5}</td></tr>
								<tr><td>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='file_imgItem5' onchange='oTools.previewImgFile(\"file_imgItem5\",\"img_item5\");' required >
										<span class='lbl-file-icon'></span>
										<span>انتخاب فایل</span>
									</label>					
								</td></tr>
							</table>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>فایل های قابل دانلود قبل از خرید</h3>
							<label>نوع فایل های مجاز : doc,pdf</label>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attachFree1' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل pdf</span>
								</label>	
								<label class='dir-ltr algn-r'></label>				
							</div>
							<div>
								<label class='lbl-file  lbl-file-right'>
									<input type='file' id='file_attachFree2' onchange='' required >
									<span class='lbl-file-icon'></span>
									<span>انتخاب فایل word</span>
								</label>	
								<label class='dir-ltr algn-r'></label>				
							</div>
							<hr>
							<h3>لینک فایل های دانلود قبل از خرید</h3>
							<label>می توانید بجای آپلود فایل، از لینک مستقیم استفاده کنید</label>
							<div class='vSpace'></div>

							<label>انتخاب فایل pdf</label>
							<input type='text' id='txt_attachUrl1' class='dir-ltr' value=''> 

							<label>انتخاب فایل word</label>
							<input type='text' id='txt_attachUrl2' class='dir-ltr' value=''>							
						</div>
					</div>
				</div>
				<!-- ----- -->";
				if($itemsOther_code) $code.="
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>خصوصیات متفرقه</h3>
							{$itemsOther_code}
						</div>
					</div>
				</div>				
			</div>			
		</div>
		
		<div class='part part-w-10'>
		   <div class='vSpace-4x'></div>
			<hr>
			<button class='btn btn-default' onclick='{$backEvent}'><i class='fa fa-arrow-right'></i></button>
			<button class='btn btn-success' onclick='shopItems_update(\"new\",0,1);'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>			
			<div class='vSpace-4x'></div>
		</div>	
		";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_update")
   {
      if($_REQUEST["purpose"]=="edit")
		{
         $id=cDataBase::escape($_REQUEST["id"]);
         $item=$oShopItems->get($id);
		}			
      else
         $id=substr(time(),5);

		//fields
      $array=[];
      $array["purpose"]=cDataBase::escape($_REQUEST["purpose"]);
      $array["id"]=$id;
      $array["userId"]=$_SESSION['user_id'];
      $array["groupId"]=cDataBase::escape(rtrim($_REQUEST["groupId"],','));	//groupId1,groupId2,id3,... conver to groupId1|groupId2|groupId3|...	
      $array["titleBefor"]=cDataBase::escape(@$_REQUEST["titleBefor"]);
      $array["titleFa"]=cDataBase::escape(@$_REQUEST["titleFa"]);
      $array["titleEn"]=cDataBase::escape(@$_REQUEST["titleEn"]);
      $array["countPageFa"]=(int)cDataBase::escape(@$_REQUEST["countPageFa"]);
      $array["countPageEn"]=(int)cDataBase::escape(@$_REQUEST["countPageEn"]);
      $array["property"]=cDataBase::escape(@$_REQUEST["property"]);
      $array["listFa"]=cDataBase::escape(@$_REQUEST["listFa"]);
      $array["listEn"]=cDataBase::escape(@$_REQUEST["listEn"]);
      $array["isSource"]=cTools::misc_jsBln2PhpBln(cDataBase::escape(@$_REQUEST["isSource"])); //checkBox
      $array["listLinked"]=cDataBase::escape(rtrim(@$_REQUEST["listLinked"],',')); //id1,id2,id3,... conver to id1|id2|id3|...
      $array["publicationName"]=cDataBase::escape(@$_REQUEST["publicationName"]);
      $array["comment"]=cDataBase::escape(@$_REQUEST["comment"]);
      $array["keywords"]=cDataBase::escape(rtrim(@$_REQUEST["keywords"],',')); //tag1,tag2,tag3,... conver to tag1|tag2|tag3|...
      $array["price"]=(int)cTools::str_faIntToEnInt(cDataBase::escape(@$_REQUEST["price"]));
      $array["isFree"]=cTools::misc_jsBln2PhpBln(cDataBase::escape(@$_REQUEST["isFree"])); //checkBox
      $array["discount"]=cTools::str_faIntToEnInt(cDataBase::escape(@$_REQUEST["discount"]));      
		$array["notDiscount"]=cTools::misc_jsBln2PhpBln(cDataBase::escape(@$_REQUEST["notDiscount"])); //checkBox
		$array["doi"]=cDataBase::escape(@$_REQUEST["doi"]);
		$array["json"]=cDataBase::escape(@$_REQUEST["json"]);
		$array["university"]=cDataBase::escape(@$_REQUEST["university"]); //daneshgah
		$array["gazette"]=cDataBase::escape(@$_REQUEST["gazette"]); //majalleh
		$array["publicationYear"]=cDataBase::escape(@$_REQUEST["publicationYear"]); //sale enteshar
		$array["marjaLink"]=cDataBase::escape(@$_REQUEST["marjaLink"]); //link marja
      $array["isSpecial"]=(int)$oTools->misc_jsBln2PhpBln(cDataBase::escape(@$_REQUEST["isSpecial"]));
		$array["fileUrl1"]=cDataBase::escape(@$_REQUEST["fileUrl1"]); 
		$array["fileUrl2"]=cDataBase::escape(@$_REQUEST["fileUrl2"]); 
		$array["attachUrl1"]=cDataBase::escape(@$_REQUEST["attachUrl1"]); 
		$array["attachUrl2"]=cDataBase::escape(@$_REQUEST["attachUrl2"]); 
		
		//otherProperties
		$otherProperties=@$_REQUEST["otherProperties"];
		file_put_contents($oPath->manageDir("shop_bundle/data/contents/otherProperties_{$id}.json"),$otherProperties);
		
		//contents
		if(!file_exists($oPath->manageDir("shop_bundle/data"))) mkdir($oPath->manageDir("shop_bundle/data"));
		if(!file_exists($oPath->manageDir("shop_bundle/data/contents"))) mkdir($oPath->manageDir("shop_bundle/data/contents"));
		//---		
		$content=$oTools->str_tagFitSend_decode($_REQUEST["content"]);
		file_put_contents($oPath->manageDir("shop_bundle/data/contents/itemContent_{$id}.html"),$content);
		//---
		$contentSampleFa=$oTools->str_tagFitSend_decode($_REQUEST["contentSampleFa"]);
		file_put_contents($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$id}.html"),$contentSampleFa);
		//---
		$contentSampleEn=$oTools->str_tagFitSend_decode($_REQUEST["contentSampleEn"]);
		file_put_contents($oPath->manageDir("shop_bundle/data/contents/itemContentSampleEn_{$id}.html"),$contentSampleEn);
		
      //image delete (edit)
		for($i=0;$i <=5;$i++)
		{
			$imgDel=cDataBase::escape(@$_REQUEST["imgDel{$i}"]);
			if($imgDel==1 || $imgDel=="true") 
			{
				@unlink($oPath->manageDir("shop_bundle/data/images/item{$i}_{$id}.jpg"));
				@unlink($oPath->manageDir("shop_bundle/data/images/item{$i}Larg_{$id}.jpg"));
				@unlink($oPath->manageDir("shop_bundle/data/images/item{$i}Thumb_{$id}.jpg"));
			}
		}
		
		//image new
		for($i=0;$i <= 5;$i++)
		{
			if(isset($_FILES["file_imgItem{$i}"]))
			{
				if(!$oTools->fs_fileIsImage($_FILES["file_imgItem{$i}"]["name"]))
				{
					cEngine::response("!imageType[|]{$i}");
					exit;
				}
				if($oTools->fs_fileSize($_FILES["file_imgItem{$i}"]["tmp_name"]) > 15728640) //15mb
				{
					cEngine::response("!imageSize[|]{$i}");
					exit;
				}				

				$img = new SimpleImage();
				$img->load($_FILES["file_imgItem{$i}"]['tmp_name'])
				->fit_to_width(370)
				->save($oPath->manageDir("shop_bundle/data/images/item{$i}_{$id}.jpg"),100);
				//--
				$img->load($_FILES["file_imgItem{$i}"]['tmp_name'])
				->fit_to_width(600)
				->save($oPath->manageDir("shop_bundle/data/images/item{$i}Larg_{$id}.jpg"),100);
				//--
				$img->load($_FILES["file_imgItem{$i}"]['tmp_name'])
				->fit_to_width(120)
				->save($oPath->manageDir("shop_bundle/data/images/item{$i}Thumb_{$id}.jpg"),100);				
				unlink($_FILES["file_imgItem{$i}"]['tmp_name']);
			}
		}		
		
		//file 1,2 , for after pay
		if($_REQUEST["purpose"]=="edit") $fileName_=[1=>$item->fileName1,2=>$item->fileName2]; else $fileName_=[1=>'',2=>''];
		for($i=1;$i <= count($fileName_);$i++)
		{
			if(@$_FILES["file_attach{$i}"]["error"] === 0)
			{
				//file
				$extension= pathinfo($_FILES["file_attach{$i}"]['name'], PATHINFO_EXTENSION);
				$fileName=$_FILES["file_attach{$i}"]['name'];
				$fileType=explode(".",$extension);
				$fileType=$fileType[count($fileType)-1];
				$fileType=strtolower($fileType);

				if(filesize($_FILES["file_attach{$i}"]['tmp_name']) > 52428800) //50mb
				{
					cEngine::response("!attachSize[|]{$i}");
					unlink($_FILES["file_attach{$i}"]['tmp_name']);
					exit;
				}
				if($fileType=="zip" || $fileType=="rar")
				{
					$fileName=cDataBase::escape(str_replace(' ','_',$fileName),false);
					$fileName=rand(100000,10000000) . '_' . $fileName;
					if(file_exists($oPath->manageDir("shop_bundle/data/{$fileName}")))
						$fileName=rand(100000,10000000) . '_' . $fileName;
					copy($_FILES["file_attach{$i}"]['tmp_name'],$oPath->manageDir("shop_bundle/data/{$fileName}"));
					unlink($_FILES["file_attach{$i}"]['tmp_name']);
					$fileName_[$i]=$fileName;
				}
				else
				{
					cEngine::response("!attachType[|]{$i}");
					unlink($_FILES["file_attach{$i}"]['tmp_name']);
					exit;					
				}
			}
		}

		//attach 1,2 , free
		if($_REQUEST["purpose"]=="edit") $fileNameFree=[1=>$item->attachName1,2=>$item->attachName2]; else $fileNameFree=[1=>'',2=>''];
		$fileNameFree_type=[1=>'pdf', 2=>'doc'];
		$fileNameFree_name=[1=>'pdf', 2=>'word'];
		for($i=1;$i <= count($fileNameFree);$i++)
		{
			if(@$_FILES["file_attachFree{$i}"]["error"] === 0)
			{
				//file
				$extension= pathinfo($_FILES["file_attachFree{$i}"]['name'], PATHINFO_EXTENSION);
				$fileName=$_FILES["file_attachFree{$i}"]['name'];
				$fileType=explode(".",$extension);
				$fileType=$fileType[count($fileType)-1];
				$fileType=strtolower($fileType);

				if(filesize($_FILES["file_attachFree{$i}"]['tmp_name']) > 52428800) //50mb
				{
					cEngine::response("!attachFreeSize[|]{$fileNameFree_name[$i]}[|]{$i}");
					unlink($_FILES["file_attachFree{$i}"]['tmp_name']);
					exit;
				}
				if($fileType==$fileNameFree_type[$i])
				{
					$fileName=cDataBase::escape(str_replace(' ','',$fileName),false);
					$fileName=rand(100000,10000000) . '_' . $fileName;
					if(file_exists($oPath->manageDir("shop_bundle/data/{$fileName}")))
						$fileName=rand(100000,10000000) . '_' . $fileName;
					copy($_FILES["file_attachFree{$i}"]['tmp_name'],$oPath->manageDir("shop_bundle/data/{$fileName}"));
					unlink($_FILES["file_attachFree{$i}"]['tmp_name']);
					$fileNameFree[$i]=$fileName;
				}
				else
				{
					cEngine::response("!attachFreeType[|]{$fileNameFree_name[$i]}");
					unlink($_FILES["file_attachFree{$i}"]['tmp_name']);
					exit;					
				}
				
			}			
		}

      if($array["purpose"]=="edit")
		{
			$array["updateById"]=0;
			$array["fileName1"]=$fileName_[1];
			$array["fileName2"]=$fileName_[2];
			$array["attachName1"]=$fileNameFree[1];
			$array["attachName2"]=$fileNameFree[2];
         $ret=$oShopItems->update($array);
			$oShopItems->setConfirm($id,0);
		}
      else if($array["purpose"]=="new")
		{
			$array["type"]=cDataBase::escape(@$_REQUEST["type"]); //0 maghalat or 1 payannameh
			$array["insertById"]=0;
			$array["fileName1"]=$fileName_[1];
			$array["fileName2"]=$fileName_[2];
			$array["attachName1"]=$fileNameFree[1];
			$array["attachName2"]=$fileNameFree[2];
         $ret=$oShopItems->insert($array);
			$oShopItems->setConfirm($id,0);
		}
		
      cEngine::response("ok[|]{$array['purpose']}[|]{$_SESSION['user_id']}");
		//QRcode::png($oShopItems->getUrl($id),$oPath->manageDir("shop_bundle/data/images/itemQr_{$id}.jpg"),4,6,false);
		//file_put_contents($oPath->publicDir("client/sitemap.xml"),$oShopItems->siteMapUpdate());
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_viewForConfirm0") //darkhast namayesh mahsool (maghaleh), jahat barrasy
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $item=$oShopItems->get($itemId);
      
		//images
		$t=time();
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$itemId}.jpg")))
         $img0="<img id='img_item0' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img0="<img id='img_item0' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item1Thumb_{$itemId}.jpg")))
         $img1="<img id='img_item1' src='" . $oPath->manage("shop_bundle/data/images/item1Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img1="<img id='img_item1' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item2Thumb_{$itemId}.jpg")))
         $img2="<img id='img_item2' src='" . $oPath->manage("shop_bundle/data/images/item2Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img2="<img id='img_item2' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item3Thumb_{$itemId}.jpg")))
         $img3="<img id='img_item3' src='" . $oPath->manage("shop_bundle/data/images/item3Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img3="<img id='img_item3' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item4Thumb_{$itemId}.jpg")))
         $img4="<img id='img_item4' src='" . $oPath->manage("shop_bundle/data/images/item4Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img4="<img id='img_item4' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item5Thumb_{$itemId}.jpg")))
         $img5="<img id='img_item5' src='" . $oPath->manage("shop_bundle/data/images/item5Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img5="<img id='img_item5' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
		
		//attach
		$fileLlink1='';
		if($item->fileName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->fileName1}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->fileName1}");
			$fileLlink1="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->fileName1}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		//---
		$fileLlink2='';
		if($item->fileName2 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->fileName2}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->fileName2}");
			$fileLlink2="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->fileName2}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}	
		//---
		$fileFreeLlink1='';
		if($item->attachName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->attachName1}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->attachName1}");
			$fileFreeLlink1="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->attachName1}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		//---
		$fileFreeLlink2='';
		if($item->attachName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->attachName2}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->attachName2}");
			$fileFreeLlink2="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->attachName2}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		
      //content
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html")))
         $content=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html"));
      else 
			$content="";
		
      //sample content farsi
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$itemId}.html")))
         $contentSampleFa=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$itemId}.html"));
      else 
			$contentSampleFa="";

      //sample content english
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContentSampleEn_{$itemId}.html")))
         $contentSampleEn=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContentSampleEn_{$itemId}.html"));
      else 
			$contentSampleEn="";		
		
		//group
		$html_group=$oShopTools->group_treeDraw(0,explode('|',$item->groupId));

      //publication
		$publication=$oShopPublication->getAll();
		$count=count($publication);
		$publication_html='';
		$publicationName_value=$item->publicationName;
		for($i=0;$i < $count;$i++)
		{
			if($item->publicationName == $publication[$i]->title) 
			{
				$selected='selected';
				$publicationName_value='';				
			}
			else 
				$selected='';
			$publication_html.="<option value='{$publication[$i]->title}' {$selected}>{$publication[$i]->title}</option>";
		}
		$publication_html="<select id='slct_publicationName' onchange='if(this.value==0) $(\"#txt_publicationName\").css(\"opacity\",1); else $(\"#txt_publicationName\").css(\"opacity\",0.3);'><option value='0'></option>{$publication_html}</select>";

      //mortabet ha, listLinked
		$listLinked=str_replace('|',',',$item->listLinked);

      //json
		$json=str_replace('|',',',$item->json);
      $json=htmlspecialchars_decode($json);		
		
		//other checkbox
      if($item->active=="1") $chk_active="checked='checked'"; else $chk_active="";		
      if($item->isSpecial=="1") $chk_isSpecial="checked='checked'"; else $chk_isSpecial="";			
      if($item->isSource=="1") $chk_isSource="checked='checked'"; else $chk_isSource="";			
      if($item->isFree=="1") $chk_isFree="checked='checked'"; else $chk_isFree="";			
      if($item->notDiscount=="1") $chk_notDiscount="checked='checked'"; else $chk_notDiscount="";	
		
		//list property,listFa,listEn
		$property=str_replace("\n","<br>",$item->property);
		$listFa=str_replace("\n","<br>",$item->listFa);
		$listEn=str_replace("\n","<br>",$item->listEn);
		
		//other properties
		$otherProperties=@file_get_contents($oPath->manageDir("shop_bundle/data/contents/otherProperties_{$item->id}.json"));
		$otherProperties=@json_decode($otherProperties,true);
		$itemsOther=$oShopItemsOther->getAll(1,true);
		$count=count($itemsOther);
		$itemsOther_code="";
		for($i=0;$i < $count;$i++)
		{
			$itemOther=$itemsOther[$i];
			$value=@$otherProperties[$itemOther->id];
			$itemsOther_code.="
			<label>{$itemOther->title} : <b>{$value}</b></label>
			";
		}
		
      $code= "
		<div class='vSpace-4x'></div>
		<div class='title-h1 dir-rtl algn-c'><i class='fa fa-pencil-square-o'></i>&nbsp;بررسی مقاله {$item->titleFa}</div>		
		<div class='vSpace-4x'></div>
 
		<div class='form form-w5 dir-rtl algn-r'>
			<div class='part-r part-w-5'>
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>پیشوند عنوان : <b>{$item->titleBefor}</b></label>				
							
							<label>عنوان : <b>{$item->titleFa}</b></label>				
			  
							<label>عنوان انگلیسی : <b>{$item->titleEn}</b></label>				
							
							<label>تعداد صفحات فارسی : <b>{$item->countPageFa}</b></label>				
						
							<label>تعداد صفحات انگلیسی : <b>{$item->countPageEn}</b></label>				
			            
							<hr>
							
							<label>مشخصات محصول</label>				
							<span>{$property}</span>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>شرح محصول</label>
							<div>{$content}</div>
							<hr>
							
							<div class='vSpace'></div>
							
							<label>نمونه متن فارسی پروژه</label>
							<div>{$contentSampleFa}</div>
							<hr>
							
							<div class='vSpace'></div>
							
							<label>نمونه متن انگلیسی پروژه</label>
							<div>{$contentSampleEn}</div>
							<hr>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>فهرست فارسی</label>				
							<span>{$listFa}</span>
							<hr>
							
							<label>فهرست انگلیسی</label>				
							<span>{$listEn}</span>

                     <label><input type='checkbox' id='chk_isSource' {$chk_isSource} disabled>&nbsp;پروژه منبع دارد</label>							
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>محصولات مرتبط</h3>				
							<label>بر اساس id مطالب.</label>				
							<input type='text' id='txt_listLinked' class='dir-ltr' value='{$listLinked}' disabled>						
						</div>
					</div>
				</div>
			<!-- ----- -->";
			if($itemsOther_code) $code.="
			<div class='part part-w-10 content-small margin-b'>
				<div class='panel'>
					<div class='panel-body'>
						<h3>خصوصیات متفرقه</h3>
						{$itemsOther_code}
					</div>
				</div>
			</div>				
			</div>

			<!-- ---------- -->
			
			<div class='part part-w-5'>
				<div class='part part-w-10 content-small margin-b'>			
					<div class='panel'>
						<div class='panel-body'>
							<label>قیمت محصول : <b>{$item->price} تومان</b></label>				

							<label>تخفیف : <b>{$item->discount} %</b></label>				
							
							<label><input type='checkbox' id='chk_isFree' {$chk_isFree} disabled>&nbsp;محصول رایگان شود</label>
							<label><input type='checkbox' id='chk_notDiscount' {$chk_notDiscount} disabled>&nbsp;محصول قابل تخفیف نیست</label>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>آپلود فایل های دانلود بعد از خرید</h3>
							<div>
								{$fileLlink1}			
							</div>
							<div class='vSpace'></div>
							<div>
								{$fileLlink2}				
							</div>
							
							<hr>
							
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<div class='vSpace'></div>
							
							<label>لینک فایل 1</label>
							<a href='{$item->fileUrl1}' target='_blank'>{$item->fileUrl1}</a> 
							
							<label>لینک فایل 2</label>
							<a href='{$item->fileUrl2}' target='_blank'>{$item->fileUrl2}</a>						
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body' style='height:220px;overflow-y:auto;'>
						   <h3>دسته بندی <span class='fg-red'>*</span></h3>
							<div id='layer_group' class='ul ul-right ul-tree'>{$html_group}</div>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>برچسب ها</h3>
							<input type='hidden' id='txt_tags' value='{$item->keywords}'>
							<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
							<script>
								oMyTag=myTag({'elementId':'ul_toTags'});
								oMyTag.setByString('{$item->keywords}','|');
							</script>
						</div>
					</div>
				</div>
            <!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>تصویر شاخص محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label></label></td></tr>
								<tr><td>{$img0}</td></tr>
							</table>
							<hr>
							<h3>تصاویر گالری محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 1</label></td></tr>
								<tr><td>{$img1}</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 2</label></td></tr>
								<tr><td>{$img2}</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 3</label></td></tr>
								<tr><td>{$img3}</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 4</label></td></tr>
								<tr><td>{$img4}</td></tr>
							</table>	
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 5</label></td></tr>
								<tr><td>{$img5}</td></tr>
							</table>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>فایل های قابل دانلود قبل از خرید</h3>
							<label>نوع فایل های مجاز : doc,pdf</label>
							<div>
								<label>فایل pdf : </label>
								<label class='algn-r'>{$fileFreeLlink1}</label>			
							</div>
							<div>
								<label>فایل word : </label>
								<label class='algn-r'>{$fileFreeLlink2}</label>				
							</div>
							<hr>
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<div class='vSpace'></div>

							<label>فایل pdf : </label>
							<label>{$item->attachUrl1}</label>

							<label>فایل word : </label>
							<label>{$item->attachUrl2}</label>						
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>جزئیات مقاله</h3>
							
							<label>doi : </label>
							<label><b>{$item->doi}</b></label>
							
							<label>json : </label>
							<textarea id='txt_json' class='dir-ltr' disabled>{$json}</textarea>

							<label>نام نشریه : <b>{$item->publicationName}</b></label>
							
							<label>دانشگاه : <b>{$item->university}</b></label>

							<label>مجله : <b>{$item->gazette}</b></label>

							<label>سال انتشار : <b>{$item->publicationYear}</b></label>

							<label>لینک مرجع : </label>
							<label><b>{$item->marjaLink}</b></label>						
						</div>
					</div>
				</div>
				
			</div>
         
         <div class='part part-w-10'>
			   <div class='vSpace-4x'></div>
			   <label>توضیحات، در صورت تایید نکردن</label>
				<textarea id='txt_confirmComment'>{$item->confirmComment}</textarea>
			   <hr>
				<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
				<button class='btn btn-success' onclick='shopItems_setConfirm({$item->id},1);'>تایید کردن</button>			
				<button class='btn btn-danger' onclick='shopItems_setConfirm({$item->id},2);'>تایید نکردن</button>			
			   <div class='vSpace-4x'></div>
			</div>			
		</div>";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_viewForConfirm1") //darkhast namayesh mahsool (payan nameh), jahat barrasy
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $item=$oShopItems->get($itemId);
      
		//images
		$t=time();
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$itemId}.jpg")))
         $img0="<img id='img_item0' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img0="<img id='img_item0' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item1Thumb_{$itemId}.jpg")))
         $img1="<img id='img_item1' src='" . $oPath->manage("shop_bundle/data/images/item1Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img1="<img id='img_item1' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item2Thumb_{$itemId}.jpg")))
         $img2="<img id='img_item2' src='" . $oPath->manage("shop_bundle/data/images/item2Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img2="<img id='img_item2' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item3Thumb_{$itemId}.jpg")))
         $img3="<img id='img_item3' src='" . $oPath->manage("shop_bundle/data/images/item3Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img3="<img id='img_item3' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item4Thumb_{$itemId}.jpg")))
         $img4="<img id='img_item4' src='" . $oPath->manage("shop_bundle/data/images/item4Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img4="<img id='img_item4' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item5Thumb_{$itemId}.jpg")))
         $img5="<img id='img_item5' src='" . $oPath->manage("shop_bundle/data/images/item5Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img5="<img id='img_item5' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
		
		//attach
		$fileLlink1='';
		if($item->fileName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->fileName1}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->fileName1}");
			$fileLlink1="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->fileName1}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		//---
		$fileLlink2='';
		if($item->fileName2 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->fileName2}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->fileName2}");
			$fileLlink2="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->fileName2}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}	
		//---
		$fileFreeLlink1='';
		if($item->attachName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->attachName1}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->attachName1}");
			$fileFreeLlink1="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->attachName1}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		//---
		$fileFreeLlink2='';
		if($item->attachName1 != '' && file_exists($oPath->manageDir("shop_bundle/data/{$item->attachName2}")))
		{
			$path=$oPath->manage("shop_bundle/data/subtitle/{$item->attachName2}");
			$fileFreeLlink2="<div><span class='lbl dir-ltr'><a href='{$path}' target='_blank' class='fg-info'>{$item->attachName2}</a>&nbsp;<i class='fa fa-download'></i></span></div>";
		}
		
      //content
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html")))
         $content=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html"));
      else 
			$content="";
		
      //sample content farsi
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$itemId}.html")))
         $contentSampleFa=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContentSampleFa_{$itemId}.html"));
      else 
			$contentSampleFa="";
		
		//group
		$html_group=$oShopTools->group_treeDraw(0,explode('|',$item->groupId));

      //publication
		$publication=$oShopPublication->getAll();
		$count=count($publication);
		$publication_html='';
		$publicationName_value=$item->publicationName;
		for($i=0;$i < $count;$i++)
		{
			if($item->publicationName == $publication[$i]->title) 
			{
				$selected='selected';
				$publicationName_value='';				
			}
			else 
				$selected='';
			$publication_html.="<option value='{$publication[$i]->title}' {$selected}>{$publication[$i]->title}</option>";
		}
		$publication_html="<select id='slct_publicationName' onchange='if(this.value==0) $(\"#txt_publicationName\").css(\"opacity\",1); else $(\"#txt_publicationName\").css(\"opacity\",0.3);'><option value='0'></option>{$publication_html}</select>";

      //mortabet ha, listLinked
		$listLinked=str_replace('|',',',$item->listLinked);

      //json
		$json=str_replace('|',',',$item->json);
      $json=htmlspecialchars_decode($json);		
		
		//other checkbox
      if($item->active=="1") $chk_active="checked='checked'"; else $chk_active="";		
      if($item->isSpecial=="1") $chk_isSpecial="checked='checked'"; else $chk_isSpecial="";			
      if($item->isSource=="1") $chk_isSource="checked='checked'"; else $chk_isSource="";			
      if($item->isFree=="1") $chk_isFree="checked='checked'"; else $chk_isFree="";			
      if($item->notDiscount=="1") $chk_notDiscount="checked='checked'"; else $chk_notDiscount="";	
		
		//list property,listFa,listEn
		$property=str_replace("\n","<br>",$item->property);
		$listFa=str_replace("\n","<br>",$item->listFa);
		
		//other properties
		$otherProperties=@file_get_contents($oPath->manageDir("shop_bundle/data/contents/otherProperties_{$item->id}.json"));
		$otherProperties=@json_decode($otherProperties,true);
		$itemsOther=$oShopItemsOther->getAll(1,true);
		$count=count($itemsOther);
		$itemsOther_code="";
		for($i=0;$i < $count;$i++)
		{
			$itemOther=$itemsOther[$i];
			$value=@$otherProperties[$itemOther->id];
			$itemsOther_code.="
			<label>{$itemOther->title} : <b>{$value}</b></label>
			";
		}
		
      $code= "
		<div class='vSpace-4x'></div>
		<div class='title-h1 dir-rtl algn-c'><i class='fa fa-pencil-square-o'></i>&nbsp;بررسی مقاله {$item->titleFa}</div>		
		<div class='vSpace-4x'></div>
 
		<div class='form form-w5 dir-rtl algn-r'>
			<div class='part-r part-w-5'>
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>پیشوند عنوان : <b>{$item->titleBefor}</b></label>				
							<label>عنوان : <b>{$item->titleFa}</b></label>							
							<label>تعداد صفحات : <b>{$item->countPageFa}</b></label>						
							<hr>
							<label>مشخصات محصول</label>				
							<span>{$property}</span>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>شرح محصول</label>
							<div>{$content}</div>
							<hr>
							
							<div class='vSpace'></div>
							
							<label>نمونه متن پروژه</label>
							<div>{$contentSampleFa}</div>
							<hr>
						</div>
					</div>
				</div>
				
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<label>فهرست فارسی</label>				
							<span>{$listFa}</span>
							<hr>
                     <label><input type='checkbox' id='chk_isSource' {$chk_isSource} disabled>&nbsp;پروژه منبع دارد</label>							
						</div>
					</div>
				</div>
				<!-- ----- -->";
				if($itemsOther_code) $code.="
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>خصوصیات متفرقه</h3>
							{$itemsOther_code}
						</div>
					</div>
				</div>				
			</div>

			<!-- ---------- -->
			
			<div class='part part-w-5'>
				<div class='part part-w-10 content-small margin-b'>			
					<div class='panel'>
						<div class='panel-body'>
							<label>قیمت محصول : <b>{$item->price} تومان</b></label>				
							<label>تخفیف : <b>{$item->discount} %</b></label>				
							<label><input type='checkbox' id='chk_isFree' {$chk_isFree} disabled>&nbsp;محصول رایگان شود</label>
							<label><input type='checkbox' id='chk_notDiscount' {$chk_notDiscount} disabled>&nbsp;محصول قابل تخفیف نیست</label>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>آپلود فایل های دانلود بعد از خرید</h3>
							<div>
								{$fileLlink1}			
							</div>
							<div class='vSpace'></div>
							<div>
								{$fileLlink2}				
							</div>
							
							<hr>
							
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<div class='vSpace'></div>
							
							<label>لینک فایل 1</label>
							<a href='{$item->fileUrl1}' target='_blank'>{$item->fileUrl1}</a> 
							
							<label>لینک فایل 2</label>
							<a href='{$item->fileUrl2}' target='_blank'>{$item->fileUrl2}</a>						
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body' style='height:220px;overflow-y:auto;'>
						   <h3>دسته بندی <span class='fg-red'>*</span></h3>
							<div id='layer_group' class='ul ul-right ul-tree'>{$html_group}</div>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>برچسب ها</h3>
							<input type='hidden' id='txt_tags' value='{$item->keywords}'>
							<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
							<script>
								oMyTag=myTag({'elementId':'ul_toTags'});
								oMyTag.setByString('{$item->keywords}','|');
							</script>
						</div>
					</div>
				</div>
            <!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
							<h3>تصویر شاخص محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label></label></td></tr>
								<tr><td>{$img0}</td></tr>
							</table>
							<hr>
							<h3>تصاویر گالری محصول</h3>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 1</label></td></tr>
								<tr><td>{$img1}</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 2</label></td></tr>
								<tr><td>{$img2}</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 3</label></td></tr>
								<tr><td>{$img3}</td></tr>
							</table>
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 4</label></td></tr>
								<tr><td>{$img4}</td></tr>
							</table>	
							<table class='display-inlineBlock'>
								<tr><td><label>تصویر شماره 5</label></td></tr>
								<tr><td>{$img5}</td></tr>
							</table>
						</div>
					</div>
				</div>
				<!-- ----- -->
				<div class='part part-w-10 content-small margin-b'>
					<div class='panel'>
						<div class='panel-body'>
						   <h3>فایل های قابل دانلود قبل از خرید</h3>
							<label>نوع فایل های مجاز : doc,pdf</label>
							<div>
								<label>فایل pdf : </label>
								<label class='algn-r'>{$fileFreeLlink1}</label>			
							</div>
							<div>
								<label>فایل word : </label>
								<label class='algn-r'>{$fileFreeLlink2}</label>				
							</div>
							<hr>
							<h3>لینک فایل های دانلود بعد از خرید</h3>
							<div class='vSpace'></div>

							<label>فایل pdf : </label>
							<label>{$item->attachUrl1}</label>

							<label>فایل word : </label>
							<label>{$item->attachUrl2}</label>						
						</div>
					</div>
				</div>

			</div>
         
         <div class='part part-w-10'>
			   <div class='vSpace-4x'></div>
			   <label>توضیحات، در صورت تایید نکردن</label>
				<textarea id='txt_confirmComment'>{$item->confirmComment}</textarea>
			   <hr>
				<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
				<button class='btn btn-success' onclick='shopItems_setConfirm({$item->id},1);'>تایید کردن</button>			
				<button class='btn btn-danger' onclick='shopItems_setConfirm({$item->id},2);'>تایید نکردن</button>			
			   <div class='vSpace-4x'></div>
			</div>			
		</div>";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
	else if($request=="shopItems_setConfirm")
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $confirmValue=$oDb->escape($_REQUEST["confirmValue"]);
      $confirmComment=$oDb->escape($_REQUEST["confirmComment"]);
      $oShopItems->setConfirm($itemId,$confirmValue);
      $oShopItems->setConfirmComment($itemId,$confirmComment);
      cEngine::response("ok[|]{$confirmValue}");
		exit;
   }//------------------------------------------------------------------------------------ 
	else if($request=="shopItems_confirm") //change confirm to unConfirm and reverse
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $ret=$oShopItems->getConfirm($itemId);
		if($ret==true)
		{
		   $oShopItems->setConfirm($itemId,"2");					
			$code="<button class='btn btn-block-auto fg-danger' onclick='shopItems_confirm({$itemId});'><i class='fa fa-undo'></i>&nbsp;تایید نشده</button>";
		}
		else
		{
			$oShopItems->setConfirm($itemId,"1");			
			$code="<button class='btn btn-block-auto fg-success' onclick='shopItems_confirm({$itemId});'><i class='fa fa-undo'></i>&nbsp;تایید شده</button>";
		}
      cEngine::response("ok[|]{$itemId}[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------  	
	else if($request=="shopItems_active_") //change active to unActive and reverse
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $ret=$oShopItems->getActive($itemId);
		if($ret==true)
		{
		   $oShopItems->setActive($itemId,"0");					
			$code="<button class='btn btn-danger' onclick='shopItems_active_({$itemId});'><i class='fa fa-undo'></i>&nbsp;غیر فعال</button>";
		}
		else
		{
			$oShopItems->setActive($itemId,"1");			
			$code="<button class='btn btn-success' onclick='shopItems_active_({$itemId});'><i class='fa fa-undo'></i>&nbsp;فعال</button>";
		}	
      cEngine::response("ok[|]{$itemId}[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------   		
	
	//START likes
	else if($request=="shopItems_likesDraw")
   {
		$itemId=$oDb->escape($_REQUEST["itemId"]);
      $showSearch=$oDb->escape($_REQUEST["showSearch"]); //if "" bashad, emal namishavad.dar gheire insoorate agar 1 bashad, bar asase jostejo searchWord, select mishavad
      $searchWord=@htmlisSpecialchars($oDb->escape($_REQUEST["searchWord"]));
      $page=isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;		
		$item=$oShopItems->get($itemId);
      if($showSearch=="1")
      {
         if($searchWord)
            $_SESSION["shopItems_likesSearchWord"]=$searchWord;
         else
            $searchWord=@$_SESSION["shopItems_likesSearchWord"];
      }
      else
         $searchWord="";
		$_SESSION["shopItems_likesPage"]=$page;
		$_SESSION["shopItems_likesShowSearch"]=$showSearch;
		
      //get session for back item
      $backItem="{
			\"shopId\":\"{$_SESSION['shopItems_shopId']}\",
			\"showIsSpecial\":\"{$_SESSION['shopItems_showIsSpecial']}\",
			\"showActive\":\"{$_SESSION['shopItems_showActive']}\",
			\"showSearch\":\"{$_SESSION['shopItems_showSearch']}\",
			\"showCheckOut\":\"{$_SESSION['shopItems_showCheckOut']}\",
			\"checkCount\":\"{$_SESSION['shopItems_checkCount']}\",
			\"page\":\"{$_SESSION['shopItems_selectPage']}\"
		}";
		
      //START PAGE ...................................
      $items=$oShopItems->getsLikes($itemId,"",$searchWord);	
		
      $itemCount=count($items);
      $limitPage=$oTools->misc_getLimitPage($itemCount,30,$page);
      $limitStart=$limitPage["limitStart"];
      $limitEnd=$limitPage["limitEnd"];
      $pageCount=$limitPage["pageCount"];
      $loop=$oTools->misc_getLoopPaging($pageCount,10,$page);
      $start=$loop["start"];
      $end=$loop["end"];
      $btnPageCode="";
      for($i=$start;$i < $end;$i++)
      {
         if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
            $btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
         else
            $btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='shopItems_likesDraw({$itemId},{$showSearch},{$i});'>{$i}</a>";
      }
      $back=$page - 1;
      $next=$page + 1;
      if($page > 1) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='shopItems_likesDraw({$itemId},{$showSearch},{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($page < $pageCount) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='shopItems_likesDraw({$itemId},{$showSearch},{$next});'><i class='fa fa-angle-left'></i></a>";
 
		$items=$oShopItems->getsLikes($itemId,"",$searchWord,"{$limitStart},{$limitEnd}");
      //END PAGE ...................................

		if(file_exists($oPath->manageDir("shop_bundle/data/images/item_{$itemId}.jpg")))
			$itemImg="<img src='" . $oPath->manage("shop_bundle/data/images/item_{$itemId}.jpg?t=" . time() ) ."' style='width:64px' />";
		else
			$itemImg="<img src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";
		
		
      $codeTr="";
      $i=0;
      foreach($items as $item)
      {
         $i++;
			$user=$oUsers->get($item->userId);
			$userLink="<a onclick='user_profileDraw({$user->id});'>{$user->userName}<br>{$user->fname} {$user->lname}</a>";
         if(file_exists($oPath->manageDir("user_bundle/data/images/user_{$user->id}.jpg")))
            $userImg="<div class='chatOnline img' style='background-image:url(" . $oPath->manage("user_bundle/data/images/user_{$user->id}.jpg") . ");'></div>";
         else
            $userImg="<div class='chatOnline img' style='background-image:url(" . $oPath->asset("default/images/user_larg.png") . ");border-radius:0%' ></div>";
         
     			
         $codeTr.="
         <tr>
			   <td style='text-align:center;width: 10px;'><input type='checkbox' id='chk_{$i}' value='{$item->id}' /></td>
				<td style='width:130px'>{$userImg}<br>{$userLink}</td>
            <td><button class='btn btn-danger' onclick='shopItems_likeDel({$itemId},{$item->id});'><i class='fa fa-trash-o'></i></button></td>
         </tr>";
      }
		$chkCount=$i;
      $code= "
      <div class='panel panel-info'>
         <div class='panel-titleBar'>پسندیدگان محصول {$item->title}</div>
			<div class='panel-header'>
			<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
			&nbsp;|&nbsp;
			<button class='btn btn-danger' onclick='shopItems_likeDel({$itemId},\"selected\");'><i class='fa fa-trash-o'></i></button>
			&nbsp;|&nbsp;
			<button class='btn' onclick='shopItems_likesDraw({$itemId},1,{$page});'><i class='fa fa-search'></i></button>
			<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/><br>		
			</div>			
         <div class='panel-body'>
				<br>{$btnPageCode}<br><br>
            <table class='tbl tbl-center tbl-bordered tbl-hover'>
				   <tr>
					   <th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>
					   <th>کاربر</th>
					   <th>عملیات</th>
					</tr>
				   {$codeTr}
            </table>
				<br>{$btnPageCode}<br>
         </div>
		</div>
		<br><br>
      ";
		cEngine::response("ok[|]" . $code);
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_likeDel")
   {
      $itemId=$oDb->escape($_REQUEST["itemId"]);
      $likeId=$oDb->escape($_REQUEST["id"],false);
		
      //get session for back
      $showSearch=$_SESSION["shopItems_likesShowSearch"];
      $page=$_SESSION["shopItems_likesPage"];

		
      $ids=$oShopItems->deleteLikes($likeId);    
		cEngine::response("ok[|]{$itemId}[|]{$showSearch}[|]{$page}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_opineDraw")
   {
		$itemId=$oDb->escape($_REQUEST["itemId"]);
		$confirm=$oDb->escape($_REQUEST["confirm"]);
		$checkOut=$oDb->escape($_REQUEST["checkOut"]);
		$page=isset($_REQUEST["page"]) ? cDataBase::escape($_REQUEST["page"]) : 1;
		
		//auto
		if($page=="-1") $page=$_SESSION["shopItemsOpine_page"];
		if($itemId=="-1") $itemId=$_SESSION["shopItemsOpine_itemId"];
		if($confirm=="-1") $confirm=$_SESSION["shopItemsOpine_confirm"];
		if($checkOut=="-1") $checkOut=$_SESSION["shopItemsOpine_checkOut"];
		
		//save
		$_SESSION["shopItemsOpine_page"]=$page;
		$_SESSION["shopItemsOpine_itemId"]=$itemId;
		$_SESSION["shopItemsOpine_confirm"]=$confirm;
		$_SESSION["shopItemsOpine_checkOut"]=$checkOut;
      
      //START PAGE ...................................
      $items=$oShopItems->opine_getAll($itemId,$confirm,$checkOut);
      $count=count($items);
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
		for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
		{
			if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
				$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
			else
				$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='shopItems_opineDraw(-1,-1,-1,{$i});'>{$i}</a>";
		}
      $back=$page - 1;
      $next=$page + 1;
      if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='shopItems_opineDraw(-1,-1,-1,{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='-1,-1,-1,{$next});'><i class='fa fa-angle-left'></i></a>";		      
		$items=$oShopItems->opine_getAll($itemId,$confirm,$checkOut,$pages['sqlLimit']);		
		//END PAGE ...................................
		
		if($itemId)
		{
			$shopItem=@$oShopItems->get($itemId);
			if($shopItem) $title=$shopItem->title; else $title='جدید';
		}
		else
		{
			$shopItem='';
			$title='جدید';
		}
		
		$i=(($page-1)*3);
		$codeTr="";
      foreach($items as $item)
      {
         $i++;
		
			if(!$shopItem) $shopItem=$oShopItems->get($itemId);
			
         if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$item->id}.html")))
            $status='<span style="color: #009900">پاسخ داده شده</span>';
         else
            $status='<span style="color: #990000">پاسخ داده نشده</span>';
			
         if($item->confirm=="0") $confirm="<button class='btn btn-danger' onclick='shopItems_opineConfirm({$item->id});'>تایید نشده</button>";
         elseif($item->confirm=="1") $confirm="<button class='btn btn-success' onclick='shopItems_opineConfirm({$item->id});'>تایید شده</button>";			

			$user=@$oUsers->get($item->userId);
			$user=@"<a href='javascript:void(0)' onclick='user_profileDraw({$user->id});'>{$user->userName}</a>";
			
			if(file_exists($oPath->manageDir("shop_bundle/data/images/item0_{$shopItem->id}.jpg")))
				$shopItemImg="<img src='" . $oPath->manage("shop_bundle/data/images/item0_{$shopItem->id}.jpg") . "' style='border-radius:8px;width:120px'>";
			else
				$shopItemImg="<img src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
         $shopItemTitle=$shopItemImg . "<br>" . $shopItem->title;
			
         $date=jdate("Y/m/d h:t:i",$item->id);
         if($item->checkOut=="0") $style="style='font-weight: bolder;color:#000;font-size:17px;'"; else $style="color:#666";
         $codeTr.="
			<tr>";
				if(count($items) > 1) $codeTr.="<td style='text-align:center;width: 10px;'><input type='checkbox' id='chk_{$i}' onclick='checkedOne();' value='{$item->id}' /></td>";
				$codeTr.="
				<td {$style}>{$shopItemTitle}</td>
				<td {$style}>{$user}</td>
				<td {$style}>{$status}</td>
				<td {$style}>{$date}</td>
				<td {$style}>
				   <span id='td_active_{$item->id}'>{$confirm}</span>
				   <button class='btn btn-danger' onclick='shopItems_opineDel({$item->id});'><i class='fa fa-trash-o'></i></button>
				   <button class='btn btn-info' onclick='shopItems_opineView({$item->id});'><i class='fa fa-eye'></i></button>
				</td>
			</tr>";
      }
		$chkCount=$i;
		if($codeTr)
		{
			$code= "
				<div class='vSpace-4x'></div>
				<h1><i class='fa fa-list'></i>&nbsp;نظرات {$title}</h1>
				<div class='vSpace-4x'></div>";
				if($itemId) $code.="<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>";
			if(count($items) > 1) $code.="<button id='btn_trash' class='btn btn-danger' onclick='shopItems_opineDel(\"selected\");' disabled><i class='fa fa-trash-o'></i></button>";			
			$code.= "
			<div class='vSpace'></div>
			{$btnPageCode}			
			<table class='tbl tbl-striped tbl-bordered tbl-hover' style='direction:rtl'>
				<tr>";
					if(count($items) > 1) $code.="<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>";
					$code.="
					<th class='text-right'>موضوع</th>
					<th class='text-right'>کاربر</th>
					<th class='text-right'>وضعیت</th>
					<th class='text-right'>تاریخ</th>
					<th class='text-right'></th>
				</tr>
				{$codeTr}
			</table>
			{$btnPageCode}
			<br><br>";
		}
		else
		{
			$code= "
				<div class='vSpace-4x'></div>
				<h1><i class='fa fa-list'></i>&nbsp;نظرات {$title}</h1>
				<div class='vSpace-4x'></div>";
				if($itemId) $code.="<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>";
			$code.= "
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'>";			
		}
		cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
	else if($request=="shopItems_opineNotification")
   {
		$count=count($oShopItems->getOpineAll("","","0"));
		if(!isset($_SESSION["productItems_opineNewCount"])) 
			$_SESSION["productItems_opineNewCount"]=$count;
		else if($_SESSION["productItems_opineNewCount"] > $count)
		{
		   $_SESSION["productItems_opineNewCount"]=$count;	
		}
		if($_SESSION["productItems_opineNewCount"] != $count)
		{
			$isNew="new";
		}
		else
		{
		   $isNew="";	
		}
		$_SESSION["productItems_opineNewCount"]=$count;
      cEngine::response("ok[|]{$count}[|]{$isNew}");
		exit;
   }//------------------------------------------------------------------------------	
   else if($request=="shopItems_opineDel")
   {
      $ids=cDataBase::escape($_REQUEST["ids"],false);
      $ids=$oShopItems->opine_delete($ids);
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				@unlink ($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$selectedId}.html"));
				@unlink ($oPath->manageDir("shop_bundle/data/contents/itemOpineContent_{$selectedId}.html"));
			}
		} 		
		
      cEngine::response("ok");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_opineConfirm")
   {
      $id=$oDb->escape($_REQUEST["id"]);
      $ret=$oShopItems->opine_getConfirm($id);
		if($ret==true)
		{
		   $oShopItems->opine_setConfirm($id,"0");	
			$code="<button class='btn btn-danger' onclick='shopItems_opineConfirm({$id});'>تایید نشده</button>";
		}
		else
		{
			$oShopItems->opine_setConfirm($id,"1");	
			$code="<button class='btn btn-success' onclick='shopItems_opineConfirm({$id});'>تایید شده</button>";
		}
      cEngine::response("ok[|]{$id}[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------	
   else if($request=="shopItems_opineView")
   {
      $opineId=$oDb->escape($_REQUEST["id"]);
      $item=$oShopItems->opine_get($opineId);
		$oShopItems->opine_setCheckOut($opineId,"1");
      
		//content and answer
		$content=@file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemOpineContent_{$opineId}.html"));
		if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$opineId}.html")))
		{	
         $answerContent=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$opineId}.html"));
		   $status="<span class='fg-success'>پاسخ داده شده</span>";
		}
		else
      {			
			$answerContent="";
			$status="<span class='fg-warning'>پاسخ داده نشده</span>";
		}
		
		if($item->confirm=="0") $optnConfirm="<option value='1'>تایید شده</option><option value='0' selected='selected'>تایید نشده</option>"; 
		else if($item->confirm=="1") $optnConfirm="<option value='1' selected='selected'>تایید شده</option><option value='0'>تایید نشده</option>"; 

		$user=$oUsers->get($item->userId);
		if(file_exists($oPath->manageDir("user_bundle/data/images/user_{$item->userId}.jpg")))
			$userImage="<div class='chatOnline img' style='background-image:url(" . $oPath->manage("user_bundle/data/images/user_{$item->userId}.jpg") . ");width:64px;height:64px;'></div>";
		else
			$userImage="<div class='chatOnline img' style='background-image:url(" . $oPath->asset("default/images/user_larg.png") . ");border-radius:0%' ></div>";
				
		$date=jdate("Y/m/d h:t:i",$item->id);
      $code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-eye'></i>&nbsp;نظر</h1>
		<div class='vSpace-4x'></div>
		<div class='dir-rtl'>
			<table class='tbl tbl-bordered'>
				<tr>
					<th>ایجاد شده</th>
					<th>وضعیت پاسخ</th>
					<th>وضعیت نمایش</th>
				</tr>
				<tr>
					<td>{$date}</td>
					<td>{$status}</td>
					<td><select id='slct_confirm' style='width:120px;'>{$optnConfirm}</select></td>
				</tr>								   
			</table>
		
			<div class='vSpace-2x'></div>
						
			<!-- opine content -->
			<div class='panel'>
				<div class='panel-titleBar'>نظر کاربر</div>
				<div class='panel-header'>
					<table>
						<tr>
							<td class='hide-auto'>{$userImage}</td>
							<td class='hide-auto'>&nbsp;|&nbsp;</td>
							<td><i class='fa fa-user'></i>&nbsp;<span class='title-h4'>{$user->userName}</span></td>
							<td>&nbsp;|&nbsp;</td>
							<td><i class='fa fa-calendar'></i>&nbsp;{$date}</td>
						</tr>
					</table>
				</div>
				<div class='panel-body'>
					{$content}  
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<hr>
			<div class='title-h3'><i class='fa fa-tag'></i>&nbspارسال پاسخ</div>
			<div class='vSpace-2x'></div>
			
			<div class='form dir-rtl algn-r'>
				<label>متن پاسخ</label>
				<textarea id='txt_answer'>{$answerContent}</textarea>
			</div>						
		</div>
				
		<div class='vSpace-4x'></div>
		<hr>
		<button class='btn btn-default' onclick='shopItems_opineDraw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
		<button class='btn btn-success' onclick='shopItems_opineAnswer({$opineId});'>ارسال</button>		
		<div class='vSpace-4x'></div>
      ";
		cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_opineAnswer")
   {
      $opineId=cDataBase::escape($_REQUEST["id"]);
      $confirm=cDataBase::escape($_REQUEST["confirm"]);
		$answerContent=$oTools->str_tagFitSend_decode($_REQUEST["answerContent"]);
		//save ansver
		if(!file_exists($oPath->manageDir("shop_bundle/data"))) mkdir($oPath->manageDir("shop_bundle/data"));
		if(!file_exists($oPath->manageDir("shop_bundle/data/contents"))) mkdir($oPath->manageDir("shop_bundle/data/contents"));
      file_put_contents($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$opineId}.html"),$answerContent);
      //set confirm
		$oShopItems->opine_setConfirm($opineId,$confirm);
		cEngine::response("ok");
		exit;
   }//------------------------------------------------------------------------------	
?>
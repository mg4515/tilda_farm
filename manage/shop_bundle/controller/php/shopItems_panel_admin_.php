<?php
   /*header("Content-Type: application/json; charset=UTF-8");*/
   session_start();

	//includes
	include_once $_SESSION["engineRequire"]; //engine.php
	require_once $oPath->manageDir("shop_bundle/model/shopPriceSettings_model.php");
	require_once $oPath->manageDir("shop_bundle/model/shopItems_model.php");
	require_once $oPath->manageDir("shop_bundle/model/shopGroup_model.php");
	require_once $oPath->manageDir("shop_bundle/model/shopProperty_model.php");
	require_once $oPath->manageDir("shop_bundle/controller/php/shop_tools.php");
	require_once $oPath->manageDir("admin_bundle/model/admin_model.php");
	require_once $oPath->manageDir("users_bundle/model/users_model.php");
	require_once $oPath->manageDir("jdf.php");
	require_once $oPath->manageDir("simpleImage.php");

	//objects
	$oShopPriceSettings=new cShopPriceSettings();
	$oShopItems=new cShopItems();
	$oShopGroup=new cShopGroup();
	$oShopProperty=new cShopProperty();
	$oShopTools=new cShopTools();
	$oAdmin=new cAdmin();
	$oUsers=new cUsers();

   //var
	$arryConfirmFilter=[0=>'در انتظار تایید',1=>'تایید شده', 2=>'تایید نشده'];
	$arryProperty0Title=[1=>'1', 2=>'2', 3=>'3', 4=>'4', 5=>'5', 6=>'6', 7=>'7', 8=>'8', 9=>'9', 10=>'10'];
	$arryVaznTitle=[1=>'5 کیلو', 2=>'10 کیلو'];

	//request
	$request=@$_REQUEST["requestName"];
	
	if($request=="shopItems_draw")
   {
		$backName=cDataBase::escape(@$_REQUEST["backName"]); //requestName for back
      $userId=@$_REQUEST["userId"] ? cDataBase::escape($_REQUEST["userId"]) : '';
      $active=cDataBase::escape(@$_REQUEST["active"]);
      $confirm=isset($_REQUEST["confirm"]) ? cDataBase::escape($_REQUEST["confirm"]) : '';
      $trash=@$_REQUEST["trash"] ? cDataBase::escape($_REQUEST["trash"]) : '0';
		$isSpecial=cDataBase::escape(@$_REQUEST["isSpecial"]); 
      $searchWord=cDataBase::escape(@$_REQUEST["searchWord"]);
      $page=@$_REQUEST["page"] ? cDataBase::escape($_REQUEST["page"]) : 1;
		
		//auto session
		if($backName==-1) $backName=$_SESSION["shopItems_backName"];
		if($userId==-1) $userId=$_SESSION["shopItems_userId"];
		if($active==-1) $active=$_SESSION["shopItems_active"];
		if($confirm==-1) $confirm=$_SESSION["shopItems_confirm"];
		if($trash==-1) $trash=$_SESSION["shopItems_trash"];
      if($isSpecial==-1) $isSpecial=$_SESSION["shopItems_isSpecial"];
		if($searchWord==-1) $searchWord=@$_SESSION["shopItems_searchWord"];
      if($page==-1) $page=$_SESSION["shopItems_page"];
		
		//init session for auto session
		$_SESSION["shopItems_backName"]=$backName;
		$_SESSION["shopItems_userId"]=$userId;
		$_SESSION["shopItems_active"]=$active;
		$_SESSION["shopItems_confirm"]=$confirm;
		$_SESSION["shopItems_trash"]=$trash;
      $_SESSION["shopItems_isSpecial"]=$isSpecial;
      $_SESSION["shopItems_searchWord"]=$searchWord;
      $_SESSION["shopItems_page"]=$page;
		
		//title by trash, btn trash, btn new
		if($trash==1)
		{
         $title='محصولات حذف شده';
			$btnDeleteAll="<button id='btn_trash' class='btn btn-danger' onclick='shopItems_del(\"selected\")' disabled><i class='fa fa-remove'></i></button>";
			$btnNew='';
			$slctConfirmFilter='';
		}
		else
		{
		   $btnNew="<button type='button' class='btn btn-success' onclick='shopItems_new();'><i class='fa fa-plus'></i>&nbsp;محصول جدید</button>";
		   $title='محصولات';
			$btnDeleteAll="<button id='btn_trash' class='btn btn-danger' onclick='shopItems_trash(\"selected\")' disabled><i class='fa fa-trash-o'></i></button>";
			//filter
			$slctConfirmFilter='';
			foreach($arryConfirmFilter as $key=>$val)
			{
				if($confirm==$key && $confirm!=='')	$selected='selected'; else $selected='';
				$slctConfirmFilter.="<option value='{$key}' {$selected}>{$val}</option>";
			}
		   $slctConfirmFilter="<select id='slct_confirmFilter' onchange='shopItems_draw(\"auto\",this.value);'>{$slctConfirmFilter}</select>";		
		}		
		
		//title by confirm
		if($confirm !== "") 
		{
			$title.=' ' . $arryConfirmFilter[$confirm];
		}
		
		//by user
		if($userId > 0 && $userId != '') 
		{
			$title='(<i class="fa fa-user"></i>&nbsp;' . $oUsers->get($userId)->userName . ')';
		}
		
		//back button
		$btnBack='';
		if($backName)
		{
			$btnBack="<button class='btn btn-default' onclick='{$backName}(\"auto\");'><i class='fa fa-arrow-right'></i></button>&nbsp;|&nbsp;";
		}
		
		//START PAGE ...................................
      $items=$oShopItems->getAll([
			"userId"=>$userId,
			"sortByRow"=>true,
			"active"=>$active,
			"confirm"=>$confirm,
			"trash"=>$trash,
			"searchWord"=>$searchWord,
			"isSpecial"=>$isSpecial,
			"limitStr"=>""
		]);
      $count=count($items);
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
		if($pages["pagesEnd"] > 1)
		{
			for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='shopItems_draw(\"auto\",undefined,undefined,{$i});'>{$i}</a>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='shopItems_draw(\"auto\",undefined,undefined,{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='shopItems_draw(\"auto\",undefined,undefined,{$next});'><i class='fa fa-angle-left'></i></a>";		
		}
      $items=$oShopItems->getAll([
			"userId"=>$userId,
			"sortByRow"=>true,
			"active"=>$active,
			"confirm"=>$confirm,
			"trash"=>$trash,
			"searchWord"=>$searchWord,
			"isSpecial"=>$isSpecial,
			"limitStr"=>$pages['sqlLimit']
		]);		
      //END PAGE ...................................
		
      $codeTr="";
      $i=0;

		foreach($items as $item)
		{
			$i++;
			$itemId=$item->id;
			$countLikes=count($oShopItems->likes_getAll($itemId));
			$btnLikesDisable= $countLikes ? "" : "disabled";

			$countOpine_new=count($oShopItems->opine_getAll($item->id,"","0"));
			$countOpine_confirm=count($oShopItems->opine_getAll($item->id,"1","1"));
			$countOpine_notConfirm=count($oShopItems->opine_getAll($item->id,"0","1"));
			$btnOpineNewDisable= $countOpine_new ? "" : "disabled";
			$btnOpineConfirmDisable= $countOpine_confirm ? "" : "disabled";
			$btnOpineNotConfirmDisable= $countOpine_notConfirm ? "" : "disabled";
			
			//price
			if($item->price==0)
				$spnPrice='رایگان';
			else if($item->price > 0)
			{
				$spnPrice='<span>' . number_format($item->price) . ' تومان</span>';
				if($item->discount > 0) 
				{
					$priceByDiscount= number_format($item->price - (($item->discount * $item->price)/100));
					$spnPrice= "<span class='fg-danger'>{$spnPrice}</span>" . "<hr><span class='fg-success'>{$priceByDiscount} تومان<br>({$item->discount}%)</span>";
				}	
			}
				
			//user
			if($item->userId > 0) 
			{
				$user=$oUsers->get($item->userId);
				$html_user='کاربر<hr>' . "<a href='javascript:void(0);' onclick='user_profileDraw({$user->id});'>{$user->userName}</a>";
			}
			else if($item->insertById > 0) 
				$html_user='مدیر<hr>' . $oAdmin->get($item->insertById)['admin']->userName;
			
			//active
			if($item->active==0) $btnActive="<button class='btn btn-block-auto btn-danger' onclick='shopItems_active_({$itemId});'><i class='fa fa-undo'></i>&nbsp;غیر فعال</button>";
			else if($item->active=="1") $btnActive="<button class='btn btn-block-auto btn-success' onclick='shopItems_active_({$itemId});'><i class='fa fa-undo'></i>&nbsp;فعال</button>";	

			//confirm
			$bgColor='';
			$divQualityLevel='';
			if($item->confirm==0) //darentezare taeed
			{
				$btnConfirm="<span>در انتظار تایید</span><br><button class='btn btn-block-auto' onclick='shopItems_viewForConfirm({$itemId});'><i class='fa fa-eye'></i>&nbsp;بررسی جهت تایید</button>";	
			   $qualityLevel=100 / (30/($item->atr + $item->tam + $item->rey));
				$qualitySvg=$oPath->asset('default/images/svg/5star.svg');
				$divQualityLevel="
				<div id='layer_qualityLevel_{$i}' class='ldBar label-center' style='width:86;height:86px;'></div>
            <script>
				   setTimeout(function(){
						var bar1 = new ldBar('#layer_qualityLevel_{$i}',{
							'preset':'circle',
							'stroke':'#2196f3',
							'stroke-width':'5',
							'stroke-trail-width':'3',
							'stroke-linecap':'round',
							'precision':'1',
							'duration':'1',
							'transition-in':'1',
							'value':'{$qualityLevel}'
						});
					},1000);
				</script>				
				";
			}
			else if($item->confirm==1)
			{
				$btnConfirm="<button class='btn btn-block-auto fg-success' onclick='shopItems_confirm({$itemId});'><i class='fa fa-refresh'></i>&nbsp;تایید شده</button>";	
			}
			else if($item->confirm==2) 
			{
				$btnConfirm="<button class='btn btn-block-auto fg-danger' onclick='shopItems_confirm({$itemId});'><i class='fa fa-refresh'></i>&nbsp;تایید نشده</button>";
			}
			
			//special
			if($item->isSpecial=='1') $iSpecial='<i class="fa fa-star fg-warning"></i>'; else $iSpecial='';
			
			//trash or delete
			if($item->trash==0)
			{
				$btnDelete="<button class='btn btn-block-auto btn-danger' onclick='shopItems_trash({$item->id});'><i class='fa fa-trash-o'></i></button>";
		      $btnUnTrash='';
		   }
			else
			{
				$btnDelete="<button class='btn btn-block-auto btn-danger' onclick='shopItems_del({$item->id});'><i class='fa fa-remove' title='حذف کامل'></i></button>";
				$btnUnTrash="<button class='btn btn-block-auto btn-danger' onclick='shopItems_unTrash({$item->id});' title='بازیابی'><i class='fa fa-undo'></i></button>";
			}
			
			//image info
			if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$item->id}.jpg")))
				$imgImage="<img class='brd-radius-8' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$item->id}.jpg?t=" . time() ) ."' style='width:86px' />";
			else
				$imgImage="<img class='brd-radius-8' src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";
			
			//group info
			$html_group='';
         $groups=explode('|',$item->groupId);
			$count=count($groups);
			for($ii=0;$ii < $count;$ii++)
			{
				$groupId=$groups[$ii];
				if($group=@$oShopGroup->get($groupId))
					$html_group.="<span>{$group->title}</span>، ";
			}
			$html_group=rtrim($html_group,"، ");
			
			$codeTr.="
			<tr>";
				if(count($items) > 1) $codeTr.="<td style='text-align:center;min-width: 10px;'><input type='checkbox' id='chk_{$i}' onclick='checkedOne();' value='{$item->id}' /></td>";
				$codeTr.="
				<td style='padding:10px;min-width:138px;'>
					{$imgImage}<br>
					{$item->title}
					<hr>
					<i class='fa fa-heart'> {$countLikes}</i>
					&nbsp;&nbsp;
					<i class='fa fa-eye'> {$item->visits}</i>
					&nbsp;&nbsp;
					<i class='fa fa-shopping-cart'> {$item->countPay}</i>
				</td>
				<td style='padding:10px;min-width:100px;'>{$html_user}</td>
				<td style='padding:10px;min-width:100px;'>{$spnPrice}</td>
				<td style='padding:10px;min-width:80px;'  class='hide'>{$html_group}</td>";
				if($divQualityLevel)
					$codeTr.="<td style='min-width:86px;max-width:86px;'>{$divQualityLevel}</td>";
				else
				{
					$codeTr.="
					<td style='padding:10px;min-width:180px;'>
						<button class='btn btn-block btn-label btn-info' onclick='shopItems_opineDraw({$item->id},\"\",\"0\",1);' {$btnOpineNewDisable}>
							<span>نظرات جدید</span>
							<span class='btn-label-caption'>{$countOpine_new}</span>                 
						</button>
						<button class='btn btn-block btn-label btn-info' onclick='shopItems_opineDraw({$item->id},\"1\",\"1\",1);' {$btnOpineConfirmDisable}>
							<span>نظرات تایید شده</span>
							<span class='btn-label-caption'>{$countOpine_confirm}</span>                  
						</button>
						<button class='btn btn-block btn-label btn-info' onclick='shopItems_opineDraw({$item->id},\"0\",\"1\",1);' {$btnOpineNotConfirmDisable}>
							<span>نظرات تایید نشده</span>
							<span class='btn-label-caption'>{$countOpine_notConfirm}</span>                  
						</button>					
					</td>";
				}
				$codeTr.="
				<td style='padding:10px;min-width:120px;' class='hide-auto'><span id='td_confirm_{$item->id}'>{$btnConfirm}</span></td>
				<td style='paddin:10px'>
				   <div class='vSpace'></div>
					<div id='td_confirm2_{$item->id}' class='show-auto'>{$btnConfirm}</div>
					<div class='vSpace'></div>
					<span id='td_active_{$item->id}'>{$btnActive}</span>
					{$btnDelete}
					{$btnUnTrash}
					<button class='btn btn-block-auto btn-info' onclick='shopItems_edit({$item->id});'><i class='fa fa-pencil'></i></button>
					<button class='btn btn-label btn-warning hide-i' onclick='shopItems_likesDraw({$item->id},0);' {$btnLikesDisable}>
						<span>مورد پسندها</span>
						<span class='btn-label-caption'>{$countLikes}</span>                  
					</button>
				</td>
			</tr>";
		}
      if($codeTr)
		{
			$chkCount=$i;
			$code= "
			<div class='vSpace-4x'></div>
			<h1>{$btnBack}<i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>
			
			{$btnNew}
			{$btnDeleteAll}
			{$slctConfirmFilter}
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopItems_draw(\"auto\",undefined,$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='hScrollBar-auto'>
				<table class='tbl tbl-center tbl-bordered tbl-hover'>
					<tr>";
						if(count($items) > 1) $code.="<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>";
						$code.="
						<th></th>
						<th>ایجاد کننده</th>
						<th>قیمت</th>
						<th class='hide'>دسته بندی ها</th>";
						if($divQualityLevel)
						   $code.="<th>سطح کیفیت</th>";
						else
						   $code.="<th>نظرات</th>";
						$code.="
						<th class='hide-auto'>تایید شده/نشده</th>
						<th>عملیات</th>
					</tr>
					{$codeTr}
				</table>
			<div>
			<br>{$btnPageCode}<br> 
			<div class='vSpace-4x'></div>";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1>{$btnBack}<i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>

			{$btnNew}
			{$btnDeleteAll}
			{$slctConfirmFilter}
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopItems_draw(\"auto\",undefined,$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'></div>";		
		}
		cEngine::response("ok[|]" . $code);
		exit;
   }//------------------------------------------------------------------------------------	
   else if($request=="shopItems_del")
   {
      $itemId=$oDb->escape($_REQUEST["id"],false);
      $ids=$oShopItems->delete($itemId);
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId)
			{
				//images
				for($ii=0;$ii <= 5;$ii++)
				{
					@unlink ($oPath->manageDir("shop_bundle/data/images/item{$ii}_{$selectedId}.jpg"));
					@unlink ($oPath->manageDir("shop_bundle/data/images/item{$ii}Larg_{$selectedId}.jpg"));
					@unlink ($oPath->manageDir("shop_bundle/data/images/item{$ii}Thumb_{$selectedId}.jpg"));
				}
				@unlink ($oPath->manageDir("shop_bundle/data/images/itemQr_{$selectedId}.jpg"));

				//contents
				@unlink($oPath->manageDir("shop_bundle/data/contents/item_{$selectedId}.html"));
		
				//opines
				$opines=$oShopItems->opine_deleteByItemId($selectedId);
				$count=count($opines);
				for($ii=0;$ii < $count;$ii++)
				{
					@unlink ($oPath->manageDir("shop_bundle/data/contents/opineAnswer_{$opines[$ii]->id}.html"));
				   @unlink ($oPath->manageDir("shop_bundle/data/contents/opineContent_{$opines[$ii]->id}.html"));
				}
				//likes
				$oShopItems->likes_deleteByItemId($selectedId);		
			}
		}      
		cEngine::response('ok');
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_trash")
   {
      $itemId=$oDb->escape($_REQUEST["id"],false);
      $ids=$oShopItems->trash($itemId);      
		cEngine::response('ok');
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_unTrash")
   {
      $itemId=$oDb->escape($_REQUEST["id"],false);
      $ids=$oShopItems->unTrash($itemId);      
		cEngine::response('ok');
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_edit") //namayesh form virayesh yek mahsool
   {
		//priceSettings
		$priceSettings=$oShopPriceSettings->get();
		$pricePlus=$priceSettings->pricePlus; //+ or -
		$siteCommission=$priceSettings->siteCommission; //karmozd forooshgah
		$marketerCommission=$priceSettings->marketerCommission; //karmozd bazaryab		
		
		//shopItem
      $itemId=$oDb->escape($_REQUEST["id"]);
      $item=$oShopItems->get($itemId);
      
		//images
		$t=time();
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$itemId}.jpg")))
         $img0="<img id='img_item0' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel0' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},0);' style='cursor:pointer;color:red;'></i>";
      else
         $img0="<img id='img_item0' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item1Thumb_{$itemId}.jpg")))
         $img1="<img id='img_item1' src='" . $oPath->manage("shop_bundle/data/images/item1Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel1' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},1);' style='cursor:pointer;color:red;'></i>";
      else
         $img1="<img id='img_item1' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item2Thumb_{$itemId}.jpg")))
         $img2="<img id='img_item2' src='" . $oPath->manage("shop_bundle/data/images/item2Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel2' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},2);' style='cursor:pointer;color:red;'></i>";
      else
         $img2="<img id='img_item2' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item3Thumb_{$itemId}.jpg")))
         $img3="<img id='img_item3' src='" . $oPath->manage("shop_bundle/data/images/item3Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel3' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},3);' style='cursor:pointer;color:red;'></i>";
      else
         $img3="<img id='img_item3' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item4Thumb_{$itemId}.jpg")))
         $img4="<img id='img_item4' src='" . $oPath->manage("shop_bundle/data/images/item4Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel4' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},4);' style='cursor:pointer;color:red;'></i>";
      else
         $img4="<img id='img_item4' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item5Thumb_{$itemId}.jpg")))
         $img5="<img id='img_item5' src='" . $oPath->manage("shop_bundle/data/images/item5Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel4' class='fa fa-trash fa-2x' onclick='shopItems_imgDel({$itemId},5);' style='cursor:pointer;color:red;'></i>";
      else
         $img5="<img id='img_item5' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
		
      //content
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html")))
         $content=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html"));
      else 
			$content="";
	
		//group
		$html_group=$oShopTools->group_chksDraw(0,explode('|',$item->groupId));

		//select vazn
		$slctVazn='';
		for($i=1; $i <= count($arryVaznTitle);$i++)
		{
			if($i==$item->vazn) $selected='selected'; else $selected='';
			$slctVazn.="<option value='{$i}' {$selected}>{$arryVaznTitle[$i]}</option>";
		}
		$slctVazn="<select id='slct_vazn'>{$slctVazn}</select>";
		
		//select type
		$slctType='';
		for($i=1; $i <= count($arryTypeTitle);$i++)
		{
			if($i==$item->type) $selected='selected'; else $selected='';
			$slctType.="<option value='{$i}' {$selected} data-img-src='{$arryTypeImage[$i]}'>{$arryTypeTitle[$i]}</option>";
		}
		$slctType="<select id='slct_type'>{$slctType}</select>";
		$slctType.="<script>$('#slct_type').select2OptionPicker();</script>";
		
		//select kesht
		$slctKesht='';
		for($i=1; $i <= count($arryKeshtTitle);$i++)
		{
			if($i==$item->kesht) $selected='selected'; else $selected='';
			$slctKesht.="<option value='{$i}' {$selected} data-img-src='{$arryKeshtImage[$i]}'>{$arryKeshtTitle[$i]}</option>";
		}
		$slctKesht="<select id='slct_kesht'>{$slctKesht}</select>";
		$slctKesht.="<script>$('#slct_kesht').select2OptionPicker();</script>";
		
		//select kood
		$slctKood='';
		for($i=1; $i <= count($arryKoodTitle);$i++)
		{
			if($i==$item->kood) $selected='selected'; else $selected='';
			$slctKood.="<option value='{$i}' {$selected} data-img-src='{$arryKoodImage[$i]}'>{$arryKoodTitle[$i]}</option>";
		}
		$slctKood="<select id='slct_kood'>{$slctKood}</select>";
		$slctKood.="<script>$('#slct_kood').select2OptionPicker();</script>";

		//select atr
		$slctAtr='';
		for($i=1; $i <= count($arryAtrTitle);$i++)
		{
			if($i==$item->atr) $selected='selected'; else $selected='';
			$slctAtr.="<option value='{$i}' {$selected}>{$arryAtrTitle[$i]}</option>";
		}
		$slctAtr="<select id='slct_atr'>{$slctAtr}</select>";
		//$slctAtr.="<script>$('#slct_atr').select2OptionPicker();</script>";

		//select tam
		$slctTam='';
		for($i=1; $i <= count($arryTamTitle);$i++)
		{
			if($i==$item->tam) $selected='selected'; else $selected='';
			$slctTam.="<option value='{$i}' {$selected}>{$arryTamTitle[$i]}</option>";
		}
		$slctTam="<select id='slct_tam'>{$slctTam}</select>";
		//$slctTam.="<script>$('#slct_tam').select2OptionPicker();</script>";

		//select rey
		$slctRey='';
		for($i=1; $i <= count($arryReyTitle);$i++)
		{
			if($i==$item->rey) $selected='selected'; else $selected='';
			$slctRey.="<option value='{$i}' {$selected}>{$arryReyTitle[$i]}</option>";
		}
		$slctRey="<select id='slct_rey'>{$slctRey}</select>";
		//$slctRey.="<script>$('#slct_rey').select2OptionPicker();</script>";

		//select SaleKesht
		$slctSaleKesht='';
		for($i=1; $i <= count($arrySaleKeshtTitleTitle);$i++)
		{
			if($i==$item->saleKesht) $selected='selected'; else $selected='';
			$slctSaleKesht.="<option value='{$i}' {$selected}>{$arrySaleKeshtTitleTitle[$i]}</option>";
		}
		$slctSaleKesht="<select id='slct_saleKesht'>{$slctSaleKesht}</select>";
		$slctSaleKesht.="<script>$('#slct_saleKesht').select2OptionPicker();</script>";
		
		//other checkbox
      if($item->active=="1") $chk_active="checked='checked'"; else $chk_active="";		
      if($item->isSpecial=="1") $chk_isSpecial="checked='checked'"; else $chk_isSpecial="";					

		//properies
		$html_properties='';
		
		
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil-square-o'></i>&nbsp;ویرایش محصول {$item->title}</h1>		
		<div class='vSpace-4x'></div>
 
		<div class='form'>

			<div class='panel'>
				<div class='panel-body'>
					<h3>مشخصات محصول</h3>
					<label>نام محصول</label>				
					<input type='text' id='txt_title'	value='{$item->title}' required><span>*</span>
					
					<div class='vSpace'></div>
	  
					<label>شهر</label>				
					<input type='text' id='txt_city'	value='{$item->city}' required><span>*</span>
					
					<div class='vSpace'></div>
	  
					<label>وزن</label>				
					{$slctVazn}

					<label>موجودی انبار</label>
               <label>به منظور نامحدود شدن موجودی انبار، از مقدار -1 استفاده نمایید</label>					
					<input type='text' id='txt_count' value='{$item->count}' class='dir-ltr' required><span>*</span>						
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>قیمت محصول</h3>
					
					<div class='row form form-w10'>
					   <div class='part part-r part-w-4'>
							<label>قیمت محصول، تومان</label>				
							<input type='number' id='txt_price' value='{$item->price}' class='dir-ltr' onkeyup='shopItems_priceInfo({$pricePlus}, {$siteCommission}, {$marketerCommission});' required><span>*</span>
						
							<label>تخفیف، درصد</label>				
							<input type='text' id='txt_discount' value='{$item->discount}' class='dir-ltr' onkeyup='shopItems_priceInfo({$pricePlus}, {$siteCommission}, {$marketerCommission});' >					
					   </div>
						<div class='part part-w-6'>
						   <label class='algn-c bg-trans part-w-10' id='lbl_priceInfo'></label>
						</div>
						<script>
						   shopItems_priceInfo({$pricePlus}, {$siteCommission}, {$marketerCommission});
						</script>
					</div>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>توضیحات محصول</h3>
					
					<label><i class='fa fa-circle'></i>کلمات کلیدی</label>
					<label>کلمه مورد نظر را وارد نمایید و بر روی دکمه + کلیک کنید</label>
					<input type='hidden' id='txt_tags' value='{$item->keywords}'>
					<div class='input-control input-control-right part-w-10'>
						<span class='input-control-button fa fa-plus bg-info' onclick='oMyTag.insert($(\"#txt_tag\").val(),$(\"#txt_tag\").val());$(\"#txt_tag\").val(\"\");'></span>
						<input type='text' id='txt_tag' class='dir-rtl-i algn-r-i'> 							
					</div>
					<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
					<script>
						oMyTag=myTag({'elementId':'ul_toTags'});
						oMyTag.setByString('{$item->keywords}','|');
					</script>
					
					<label><i class='fa fa-circle'></i>توضیحات کوتاه</label>
					<textarea id='txt_comment'>{$item->comment}</textarea>
					
					<label><i class='fa fa-circle'></i>شرح محصول</label>
					<textarea id='txt_content'>{$content}</textarea>
					<script>CKEDITOR.replace('txt_content');</script>
				</div>
			</div>					
			
			<div class='vSpace'></div>

			<div class='panel'>
				<div class='panel-body' style='height:220px;overflow-y:auto;'>
					<h3>دسته بندی <span class='fg-red'>*</span></h3>
					<label>دسته بندی های مورد نظر خود را انتخاب نمایید</label>
					<div id='layer_group' class='ul ul-right ul-tree'>{$html_group}</div>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>تصویر شاخص محصول</h3>
					<table class='display-inlineBlock'>
						<tr><td><label></label></td></tr>
						<tr><td>{$img0}</td></tr>
						<tr><td>
							<label class='lbl-file  lbl-file-right'>
								<input type='file' id='file_imgItem0' onchange='oTools.previewImgFile(\"file_imgItem0\",\"img_item0\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
					<hr>
					<h3>تصاویر گالری محصول</h3>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 1</label></td></tr>
						<tr><td>{$img1}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem1' onchange='oTools.previewImgFile(\"file_imgItem1\",\"img_item1\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 2</label></td></tr>
						<tr><td>{$img2}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem2' onchange='oTools.previewImgFile(\"file_imgItem2\",\"img_item2\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 3</label></td></tr>
						<tr><td>{$img3}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem3' onchange='oTools.previewImgFile(\"file_imgItem3\",\"img_item3\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 4</label></td></tr>
						<tr><td>{$img4}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem4' onchange='oTools.previewImgFile(\"file_imgItem4\",\"img_item4\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>	
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 5</label></td></tr>
						<tr><td>{$img5}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem5' onchange='oTools.previewImgFile(\"file_imgItem5\",\"img_item5\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>ویژگی های محصول</h3>
					
					<label><i class='fa fa-circle'></i>نوع محصول</label>				
					<div class='row'>{$slctType}</div>
					
					<div class='vSpace'></div>
					
					<label><i class='fa fa-circle'></i>کشت</label>				
					<div class='row'>{$slctKesht}</div>
					
					<div class='vSpace'></div>
	  
					<label><i class='fa fa-circle'></i>کود</label>				
					<div class='row'>{$slctKood}</div>
					
					<div class='vSpace'></div>
					
					<label><i class='fa fa-circle'></i>عطر، امتیاز این ویژگی از یک تا ده</label>				
					<div class='row'>{$slctAtr}</div>
					
					<div class='vSpace'></div>
					
					<label><i class='fa fa-circle'></i>طعم، امتیاز این ویژگی را از یک تا ده انتخاب نمایید</label>				
					<div class='row'>{$slctTam}</div>
					
					<div class='vSpace'></div>
					
					<label><i class='fa fa-circle'></i>ری کشیدن، امتیاز این ویژگی را از یک تا ده انتخاب نمایید</label>				
					<div class='row'>{$slctRey}</div>
					
					<div class='vSpace'></div>
					
					<label><i class='fa fa-circle'></i>سال کشت</label>				
					<div class='row'>{$slctSaleKesht}</div>								
				</div>
			</div>
					
		</div>
		
		<div class='vSpace-4x'></div>
		<hr>
		<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
		<button class='btn btn-success' onclick='shopItems_update(\"edit\",{$item->id});'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>			
		<div class='vSpace-4x'></div>	
		";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_new") //ersale form
   {
		//priceSettings
		$priceSettings=$oShopPriceSettings->get();
		$pricePlus=$priceSettings->pricePlus; //+ or -
		$siteCommission=$priceSettings->siteCommission; //karmozd forooshgah
		$marketerCommission=$priceSettings->marketerCommission; //karmozd bazaryab
		
		//images
		$t=time();
      $img0="<img id='img_item0' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img1="<img id='img_item1' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img2="<img id='img_item2' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img3="<img id='img_item3' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img4="<img id='img_item4' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      $img5="<img id='img_item5' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
										
      //content
		$content="";
		
		//group
		$html_group=$oShopTools->group_chksDraw(0,[]);

      //property
		$property=$oShopProperty->getAll();
		$count=count($property);
		$html_property='';
		$script_property='';
		for($i=0; $i < $count;$i++)
		{
			$option1Img='';$option2Img='';$option3Img='';$option4Img='';$option5Img='';
			if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn1_{$property[$i]->id}.jpg"))) $option1Img=$oPath->manageDir("shop_bundle/data/images/propertyOptn1_{$property[$i]->id}.jpg");				
			if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn2_{$property[$i]->id}.jpg"))) $option2Img=$oPath->manageDir("shop_bundle/data/images/propertyOptn2_{$property[$i]->id}.jpg");				
			if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn3_{$property[$i]->id}.jpg"))) $option3Img=$oPath->manageDir("shop_bundle/data/images/propertyOptn3_{$property[$i]->id}.jpg");				
			if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn4_{$property[$i]->id}.jpg"))) $option4Img=$oPath->manageDir("shop_bundle/data/images/propertyOptn4_{$property[$i]->id}.jpg");				
			if(file_exists($oPath->manageDir("shop_bundle/data/images/propertyOptn5_{$property[$i]->id}.jpg"))) $option5Img=$oPath->manageDir("shop_bundle/data/images/propertyOptn5_{$property[$i]->id}.jpg");				
         $html_property.="<label><input type='checkbox' id='chk_property_{$property[$i]->id}' data-id='{$property[$i]->id}' data-title='{$property[$i]->title}' data-type='{$property[$i]->type}' data-option1='{$property[$i]->option1Title}' data-option2='{$property[$i]->option2Title}' data-option3='{$property[$i]->option3Title}' data-option4='{$property[$i]->option4Title}' data-option5='{$property[$i]->option5Title}' checked>&nbsp;{$property[$i]->title}</label>";
		   $script_property.="{
			   'id':'{$property[$i]->id}',
			   'title':'{$property[$i]->title}',
			   'type':'{$property[$i]->type}',
			   'option1Title':'{$property[$i]->option1Title}',
			   'option2Title':'{$property[$i]->option2Title}',
			   'option3Title':'{$property[$i]->option3Title}',
			   'option4Title':'{$property[$i]->option4Title}',
			   'option5Title':'{$property[$i]->option5Title}',
				'option1Img':'{$option1Img}',
				'option2Img':'{$option2Img}',
				'option3Img':'{$option3Img}',
				'option4Img':'{$option4Img}',
				'option5Img':'{$option5Img}',
			   'optionSelectIndex':'1',
			   'selected':true,
				'isUse':false
			},";
		}
		$script_property="<script>var arryProperty=[" . rtrim($script_property,',') . "]</script>";
		
		//select vazn
		$slctVazn='';
		for($i=1; $i <= count($arryVaznTitle);$i++)
		{
			if($i==1) $selected='selected'; else $selected='';
			$slctVazn.="<option value='{$i}' {$selected}>{$arryVaznTitle[$i]}</option>";
		}
		$slctVazn="<select id='slct_vazn'>{$slctVazn}</select>";
		
		//other checkbox
      $chk_active="";		
      $chk_isSpecial="";					
		
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil-square-o'></i>&nbsp;محصول جدید</h1>		
		<div class='vSpace-4x'></div>
 
		<div class='form'>
		   <div class='panel'>
			   <div class='panel-body'>
					<input type='hidden' id='txt_userId'>
					<button class='btn btn-info btn-image btn-image-right' onclick='shop_toListDraw();'>
						<span>انتخاب کشاورز</span>
						<i class='btn-image-icon fa fa-user-plus'></i>
					</button>
               <span class='lbl' id='spn_user'>کشاورزی انتخاب نشده است</span>					
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>مشخصات محصول</h3>
					<label>نام محصول</label>				
					<input type='text' id='txt_title' value='' required><span>*</span>
					
					<div class='vSpace'></div>
	  
					<label>شهر</label>				
					<input type='text' id='txt_city' value='' required><span>*</span>
					
					<div class='vSpace'></div>

					<label>وزن</label>				
					{$slctVazn}

					<div class='vSpace'></div>
	  
					<label>موجودی انبار</label>
               <label>به منظور نامحدود شدن موجودی انبار، از مقدار -1 استفاده نمایید</label>					
					<input type='text' id='txt_count' value='' class='dir-ltr' required><span>*</span>					
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>قیمت محصول</h3>
					
					<div class='row form form-w10'>
					   <div class='part part-r part-w-6'>
							<label>قیمت محصول، تومان</label>				
							<input type='number' id='txt_price' value='' class='dir-ltr' onkeyup='shopItems_priceInfo({$pricePlus}, {$siteCommission}, {$marketerCommission});' required><span>*</span>
						
							<label>تخفیف، درصد</label>				
							<input type='text' id='txt_discount' value='0' class='dir-ltr' onkeyup='shopItems_priceInfo({$pricePlus}, {$siteCommission}, {$marketerCommission});' >					
					   </div>
						<div class='part part-w-4'>
						   <label class='algn-c bg-trans part-w-10' id='lbl_priceInfo'></label>
						</div>
					</div>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>توضیحات محصول</h3>
					
					<label><i class='fa fa-circle'></i>کلمات کلیدی</label>
					<label>کلمه مورد نظر را وارد نمایید و بر روی دکمه + کلیک کنید</label>
					<input type='hidden' id='txt_tags' value=''>
					<div class='input-control input-control-right part-w-10'>
						<span class='input-control-button fa fa-plus bg-info' onclick='oMyTag.insert($(\"#txt_tag\").val(),$(\"#txt_tag\").val());$(\"#txt_tag\").val(\"\");'></span>
						<input type='text' id='txt_tag' class='dir-rtl-i algn-r-i'> 							
					</div>
					<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
					<script>
						oMyTag=myTag({'elementId':'ul_toTags'});
					</script>
					
					<label><i class='fa fa-circle'></i>توضیحات کوتاه</label>
					<textarea id='txt_comment'></textarea>
					
					<label><i class='fa fa-circle'></i>شرح محصول</label>
					<textarea id='txt_content'></textarea>
					<script>CKEDITOR.replace('txt_content');</script>
				</div>
			</div>					
	
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body' style='height:220px;overflow-y:auto;'>
					<h3>دسته بندی <span class='fg-red'>*</span></h3>
					<label>دسته بندی های مورد نظر خود را انتخاب نمایید</label>
					<div id='layer_group' class='ul ul-right ul-tree'>{$html_group}</div>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>تصویر شاخص محصول</h3>
					<table class='display-inlineBlock'>
						<tr><td><label></label></td></tr>
						<tr><td>{$img0}</td></tr>
						<tr><td>
							<label class='lbl-file  lbl-file-right'>
								<input type='file' id='file_imgItem0' onchange='oTools.previewImgFile(\"file_imgItem0\",\"img_item0\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
					<hr>
					<h3>تصاویر گالری محصول</h3>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 1</label></td></tr>
						<tr><td>{$img1}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem1' onchange='oTools.previewImgFile(\"file_imgItem1\",\"img_item1\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 2</label></td></tr>
						<tr><td>{$img2}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem2' onchange='oTools.previewImgFile(\"file_imgItem2\",\"img_item2\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 3</label></td></tr>
						<tr><td>{$img3}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem3' onchange='oTools.previewImgFile(\"file_imgItem3\",\"img_item3\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 4</label></td></tr>
						<tr><td>{$img4}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem4' onchange='oTools.previewImgFile(\"file_imgItem4\",\"img_item4\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>	
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 5</label></td></tr>
						<tr><td>{$img5}</td></tr>
						<tr><td>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='file_imgItem5' onchange='oTools.previewImgFile(\"file_imgItem5\",\"img_item5\");' required >
								<span class='lbl-file-icon'></span>
								<span>انتخاب فایل</span>
							</label>					
						</td></tr>
					</table>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>ویژگی های محصول</h3>
					<button class='btn btn-info' onclick='shopProperty_dialog();'><i class='fa fa-plus'></i>&nbsp;افزودن ویژگی ها</button>
					<div id='div_propertySelector' class='hide'>
					   <p>ویژگی های مورد نظر را فعال نمایید</p>
						{$html_property}
					</div>
					{$script_property}
					<script>shopProperty_dialog(true);</script>
					<div id='div_propertyDraw'></div>
				</div>
			</div>
									
		</div>
		

		<div class='vSpace-4x'></div>
		<hr>
		<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
		<button class='btn btn-success' onclick='shopItems_update(\"new\",0);'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>			
		<div class='vSpace-4x'></div>
		
		";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_update") //zakhire maghadire form
   {
      if($_REQUEST["purpose"]=="edit")
		{
         $id=cDataBase::escape($_REQUEST["id"]);
         $item=$oShopItems->get($id);
		}			
      else
         $id=time();		

		//fields
      $array=[];
      $array["purpose"]=cDataBase::escape($_REQUEST["purpose"]);
      $array["id"]=$id;
      $array["userId"]=cDataBase::escape(@$_REQUEST["userId"]);
      $array["groupId"]=cDataBase::escape(rtrim($_REQUEST["groupId"],','));	//groupId1,groupId2,id3,... conver to groupId1|groupId2|groupId3|...	
      $array["title"]=cDataBase::escape(@$_REQUEST["title"]);
      $array["city"]=cDataBase::escape(@$_REQUEST["city"]);
      $array["count"]=cDataBase::escape(@$_REQUEST["count"]);
      $array["comment"]=cDataBase::escape(@$_REQUEST["comment"]);
      $array["keywords"]=cDataBase::escape(rtrim(@$_REQUEST["keywords"],',')); //tag1,tag2,tag3,... conver to tag1|tag2|tag3|...
      $array["price"]=cTools::str_faIntToEnInt(cDataBase::escape(@$_REQUEST["price"]));
      $array["discount"]=cTools::str_faIntToEnInt(cDataBase::escape(@$_REQUEST["discount"]));      
		$array["isSpecial"]=(int)$oTools->misc_jsBln2PhpBln(cDataBase::escape(@$_REQUEST["isSpecial"]));
		//---
		$array["vazn"]=cDataBase::escape(@$_REQUEST["vazn"]);
		$array["type"]=cDataBase::escape(@$_REQUEST["type"]);
		$array["kesht"]=cDataBase::escape(@$_REQUEST["kesht"]);
		$array["kood"]=cDataBase::escape(@$_REQUEST["kood"]);
		$array["atr"]=cDataBase::escape(@$_REQUEST["atr"]);
		$array["tam"]=cDataBase::escape(@$_REQUEST["tam"]);
		$array["rey"]=cDataBase::escape(@$_REQUEST["rey"]);
		$array["saleKesht"]=cDataBase::escape(@$_REQUEST["saleKesht"]);
		
		//contents
		if(!file_exists($oPath->manageDir("shop_bundle/data"))) mkdir($oPath->manageDir("shop_bundle/data"));
		if(!file_exists($oPath->manageDir("shop_bundle/data/contents"))) mkdir($oPath->manageDir("shop_bundle/data/contents"));
		//---		
		$content=$oTools->str_tagFitSend_decode($_REQUEST["content"]);
		file_put_contents($oPath->manageDir("shop_bundle/data/contents/itemContent_{$id}.html"),$content);
		
      //image delete (edit)
		for($i=0;$i <=5;$i++)
		{
			$imgDel=cDataBase::escape(@$_REQUEST["imgDel{$i}"]);
			if($imgDel==1 || $imgDel=="true") 
			{
				@unlink($oPath->manageDir("shop_bundle/data/images/item{$i}_{$id}.jpg"));
				@unlink($oPath->manageDir("shop_bundle/data/images/item{$i}Larg_{$id}.jpg"));
				@unlink($oPath->manageDir("shop_bundle/data/images/item{$i}Thumb_{$id}.jpg"));
			}
		}
		
		//image new
		for($i=0;$i <= 5;$i++)
		{
			if(isset($_FILES["file_imgItem{$i}"]))
			{
				if(!$oTools->fs_fileIsImage($_FILES["file_imgItem{$i}"]["name"]))
				{
					cEngine::response("!imageType[|]{$i}");
					exit;
				}
				if($oTools->fs_fileSize($_FILES["file_imgItem{$i}"]["tmp_name"]) > 15728640) //15mb
				{
					cEngine::response("!imageSize[|]{$i}");
					exit;
				}				

				$img = new SimpleImage();
				$img->load($_FILES["file_imgItem{$i}"]['tmp_name'])
				->fit_to_width(370)
				->save($oPath->manageDir("shop_bundle/data/images/item{$i}_{$id}.jpg"),100);
				//--
				$img->load($_FILES["file_imgItem{$i}"]['tmp_name'])
				->fit_to_width(600)
				->save($oPath->manageDir("shop_bundle/data/images/item{$i}Larg_{$id}.jpg"),100);
				//--
				$img->load($_FILES["file_imgItem{$i}"]['tmp_name'])
				->fit_to_width(120)
				->save($oPath->manageDir("shop_bundle/data/images/item{$i}Thumb_{$id}.jpg"),100);				
				unlink($_FILES["file_imgItem{$i}"]['tmp_name']);
			}
		}		

      if($array["purpose"]=="edit")
		{
			$array["updateById"]=$_SESSION['admin_id'];
         $ret=$oShopItems->update($array);
		}
      else if($array["purpose"]=="new")
		{
			$array["insertById"]=$_SESSION['admin_id'];
         $ret=$oShopItems->insert($array);
		}
		
      cEngine::response("ok[|]{$array['purpose']}");
		//QRcode::png($oShopItems->getUrl($id),$oPath->manageDir("shop_bundle/data/images/itemQr_{$id}.jpg"),4,6,false);
		//file_put_contents($oPath->publicDir("client/sitemap.xml"),$oShopItems->siteMapUpdate());
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_viewForConfirm") //darkhast namayesh mahsool , jahat barrasy
   {
		//priceSettings
		$priceSettings=$oShopPriceSettings->get();
		$pricePlus=$priceSettings->pricePlus; //+ or -
		$siteCommission=$priceSettings->siteCommission; //karmozd forooshgah
		$marketerCommission=$priceSettings->marketerCommission; //karmozd bazaryab		
		
		//shopItem
      $itemId=$oDb->escape($_REQUEST["id"]);
      $item=$oShopItems->get($itemId);
      
		//images
		$t=time();
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$itemId}.jpg")))
         $img0="<img id='img_item0' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img0="<img id='img_item0' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item1Thumb_{$itemId}.jpg")))
         $img1="<img id='img_item1' src='" . $oPath->manage("shop_bundle/data/images/item1Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img1="<img id='img_item1' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item2Thumb_{$itemId}.jpg")))
         $img2="<img id='img_item2' src='" . $oPath->manage("shop_bundle/data/images/item2Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img2="<img id='img_item2' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item3Thumb_{$itemId}.jpg")))
         $img3="<img id='img_item3' src='" . $oPath->manage("shop_bundle/data/images/item3Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img3="<img id='img_item3' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item4Thumb_{$itemId}.jpg")))
         $img4="<img id='img_item4' src='" . $oPath->manage("shop_bundle/data/images/item4Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img4="<img id='img_item4' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      //---
      if(file_exists($oPath->manageDir("shop_bundle/data/images/item5Thumb_{$itemId}.jpg")))
         $img5="<img id='img_item5' src='" . $oPath->manage("shop_bundle/data/images/item5Thumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>";
      else
         $img5="<img id='img_item5' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
				
      //content
      if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html")))
         $content=file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemContent_{$itemId}.html"));
      else 
			$content="";
	
		//price
		$price=number_format($item->price);
		
		//group
		$html_group=$oShopTools->group_treeDraw(0,explode('|',$item->groupId));

		//select vazn
		$vazn=$arryVaznTitle[$item->vazn];

		//select type
		$imgType="<img src='{$arryTypeImage[$item->type]}'>";

		//select kesht
		$imgKesht="<img src='{$arryKeshtImage[$item->kesht]}'>";

		//select kood
		$imgKood="<img src='{$arryKoodImage[$item->kood]}'>";

		//select atr
		$atr=$arryAtrTitle[$item->atr];

		//select tam
		$tam=$arryTamTitle[$item->tam];

		//select rey
		$rey=$arryReyTitle[$item->rey];

		//select SaleKesht
		$saleKesht=$arrySaleKeshtTitleTitle[$item->saleKesht];

		//other checkbox
      if($item->active=="1") $chk_active="checked='checked'"; else $chk_active="";		
      if($item->isSpecial=="1") $chk_isSpecial="checked='checked'"; else $chk_isSpecial="";					

      $code= "
		<div class='vSpace-4x'></div>
		<div class='title-h1 dir-rtl algn-c'><i class='fa fa-pencil-square-o'></i>&nbsp;بررسی محصول {$item->title}</div>		
		<div class='vSpace-4x'></div>
 
		<div class='form'>

			<div class='panel'>
				<div class='panel-body'>
					<h3>مشخصات محصول</h3>
					<label><b>نام محصول :</b> {$item->title}</label>
					<label><b>شهر :</b> {$item->city}</label>		
					<label><b>وزن :</b> {$vazn}</label>	
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>قیمت محصول</h3>
					
					<div class='row form form-w10' style='display: flex;'>
					   <div class='part part-r part-w-3' style='border-left:1px solid #eee;'>
							<input type='hidden' id='txt_price' value='{$item->price}'>
							<input type='hidden' id='txt_discount' value='{$item->discount}'>
							<label><b>قیمت محصول :</b> {$price} تومان</label>				
							<label><b>تخفیف :</b> {$item->discount} ٪</label>							
					   </div>
						<div class='part part-w-7'>
						   <label class='algn-c bg-trans part-w-10' id='lbl_priceInfo'></label>
						</div>
						<script>
						   shopItems_priceInfo({$pricePlus}, {$siteCommission}, {$marketerCommission});
						</script>
					</div>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>توضیحات محصول</h3>
					
					<label><i class='fa fa-circle'></i><b>کلمات کلیدی</b></label>
					<input type='hidden' id='txt_tags' value='{$item->keywords}'>
					<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
					<script>
						oMyTag=myTag({'elementId':'ul_toTags'});
						oMyTag.setByString('{$item->keywords}','|');
					</script>
					<hr>
					
					<label><i class='fa fa-circle'></i><b>توضیحات کوتاه</b></label>
					<div>{$item->comment}</div>
					<hr>
					
					<label><i class='fa fa-circle'></i><b>شرح محصول</b></label>
					<div>{$content}</div>
					
				</div>
			</div>					
			
			<div class='vSpace'></div>

			<div class='panel'>
				<div class='panel-body' style='height:220px;overflow-y:auto;'>
					<h3>دسته بندی <span class='fg-red'>*</span></h3>
					<div id='layer_group' class='ul ul-right ul-tree'>{$html_group}</div>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>تصویر شاخص محصول</h3>
					<table class='display-inlineBlock'>
						<tr><td><label></label></td></tr>
						<tr><td>{$img0}</td></tr>
					</table>
					<hr>
					<h3>تصاویر گالری محصول</h3>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 1</label></td></tr>
						<tr><td>{$img1}</td></tr>
					</table>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 2</label></td></tr>
						<tr><td>{$img2}</td></tr>
					</table>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 3</label></td></tr>
						<tr><td>{$img3}</td></tr>
					</table>
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 4</label></td></tr>
						<tr><td>{$img4}</td></tr>
					</table>	
					<table class='display-inlineBlock'>
						<tr><td><label>تصویر شماره 5</label></td></tr>
						<tr><td>{$img5}</td></tr>
					</table>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
				<div class='panel-body'>
					<h3>ویژگی های محصول</h3>
					<table class='tbl tbl-center tbl-bordered'>
						<tr>
							<th>نوع محصول</th>
							<th>کشت</th>
							<th>کود</th>
							<th>عطر</th>
							<th>طعم</th>
							<th>ری کشیدن</th>
							<th>سال کشت</th>
						</tr>
						<tr>
							<td>{$imgType}</td>
							<td>{$imgKesht}</td>
							<td>{$imgKood}</td>
							<td><b>{$atr}</b> از 10</td>
							<td><b>{$tam}</b> از 10</td>
							<td><b>{$rey}</b> از 10</td>
							<td><b>{$saleKesht}</b></td>
						</tr>	
               </table>					
				</div>
			</div>
		
         <div class='part part-w-10'>
			   <div class='vSpace-4x'></div>
			   <label>توضیحات، در صورت تایید نکردن</label>
				<textarea id='txt_confirmComment'>{$item->confirmComment}</textarea>
			   <hr>
				<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
				<button class='btn btn-success' onclick='shopItems_setConfirm({$item->id},1);'>تایید کردن</button>			
				<button class='btn btn-danger' onclick='shopItems_setConfirm({$item->id},2);'>تایید نکردن</button>			
			   <div class='vSpace-4x'></div>
			</div>		
		</div>";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
	else if($request=="shopItems_setConfirm")
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $confirmValue=$oDb->escape($_REQUEST["confirmValue"]);
      $confirmComment=$oDb->escape($_REQUEST["confirmComment"]);
      $oShopItems->setConfirm($itemId,$confirmValue);
      $oShopItems->setConfirmComment($itemId,$confirmComment);
      cEngine::response("ok[|]{$confirmValue}");
		exit;
   }//------------------------------------------------------------------------------------ 
	else if($request=="shopItems_confirm") //change confirm to unConfirm and reverse
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $ret=$oShopItems->getConfirm($itemId);
		if($ret==true)
		{
		   $oShopItems->setConfirm($itemId,"2");					
			$code="<button class='btn btn-block-auto fg-danger' onclick='shopItems_confirm({$itemId});'><i class='fa fa-undo'></i>&nbsp;تایید نشده</button>";
		}
		else
		{
			$oShopItems->setConfirm($itemId,"1");			
			$code="<button class='btn btn-block-auto fg-success' onclick='shopItems_confirm({$itemId});'><i class='fa fa-undo'></i>&nbsp;تایید شده</button>";
		}
      cEngine::response("ok[|]{$itemId}[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------  	
	else if($request=="shopItems_active_") //change active to unActive and reverse
   {
      $itemId=$oDb->escape($_REQUEST["id"]);
      $ret=$oShopItems->getActive($itemId);
		if($ret==true)
		{
		   $oShopItems->setActive($itemId,"0");					
			$code="<button class='btn btn-danger' onclick='shopItems_active_({$itemId});'><i class='fa fa-undo'></i>&nbsp;غیر فعال</button>";
		}
		else
		{
			$oShopItems->setActive($itemId,"1");			
			$code="<button class='btn btn-success' onclick='shopItems_active_({$itemId});'><i class='fa fa-undo'></i>&nbsp;فعال</button>";
		}	
      cEngine::response("ok[|]{$itemId}[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------   		
	
	//START likes
	else if($request=="shopItems_likesDraw")
   {
		$itemId=$oDb->escape($_REQUEST["itemId"]);
      $showSearch=$oDb->escape($_REQUEST["showSearch"]); //if "" bashad, emal namishavad.dar gheire insoorate agar 1 bashad, bar asase jostejo searchWord, select mishavad
      $searchWord=@htmlisSpecialchars($oDb->escape($_REQUEST["searchWord"]));
      $page=isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;		
		$item=$oShopItems->get($itemId);
      if($showSearch=="1")
      {
         if($searchWord)
            $_SESSION["shopItems_likesSearchWord"]=$searchWord;
         else
            $searchWord=@$_SESSION["shopItems_likesSearchWord"];
      }
      else
         $searchWord="";
		$_SESSION["shopItems_likesPage"]=$page;
		$_SESSION["shopItems_likesShowSearch"]=$showSearch;
		
      //get session for back item
      $backItem="{
			\"shopId\":\"{$_SESSION['shopItems_shopId']}\",
			\"showIsSpecial\":\"{$_SESSION['shopItems_showIsSpecial']}\",
			\"showActive\":\"{$_SESSION['shopItems_showActive']}\",
			\"showSearch\":\"{$_SESSION['shopItems_showSearch']}\",
			\"showCheckOut\":\"{$_SESSION['shopItems_showCheckOut']}\",
			\"checkCount\":\"{$_SESSION['shopItems_checkCount']}\",
			\"page\":\"{$_SESSION['shopItems_selectPage']}\"
		}";
		
      //START PAGE ...................................
      $items=$oShopItems->getsLikes($itemId,"",$searchWord);	
		
      $itemCount=count($items);
      $limitPage=$oTools->misc_getLimitPage($itemCount,30,$page);
      $limitStart=$limitPage["limitStart"];
      $limitEnd=$limitPage["limitEnd"];
      $pageCount=$limitPage["pageCount"];
      $loop=$oTools->misc_getLoopPaging($pageCount,10,$page);
      $start=$loop["start"];
      $end=$loop["end"];
      $btnPageCode="";
      for($i=$start;$i < $end;$i++)
      {
         if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
            $btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
         else
            $btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='shopItems_likesDraw({$itemId},{$showSearch},{$i});'>{$i}</a>";
      }
      $back=$page - 1;
      $next=$page + 1;
      if($page > 1) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='shopItems_likesDraw({$itemId},{$showSearch},{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($page < $pageCount) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='shopItems_likesDraw({$itemId},{$showSearch},{$next});'><i class='fa fa-angle-left'></i></a>";
 
		$items=$oShopItems->getsLikes($itemId,"",$searchWord,"{$limitStart},{$limitEnd}");
      //END PAGE ...................................

		if(file_exists($oPath->manageDir("shop_bundle/data/images/item_{$itemId}.jpg")))
			$itemImg="<img src='" . $oPath->manage("shop_bundle/data/images/item_{$itemId}.jpg?t=" . time() ) ."' style='width:64px' />";
		else
			$itemImg="<img src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";
		
		
      $codeTr="";
      $i=0;
      foreach($items as $item)
      {
         $i++;
			$user=$oUsers->get($item->userId);
			$userLink="<a onclick='user_profileDraw({$user->id});'>{$user->userName}<br>{$user->fname} {$user->lname}</a>";
         if(file_exists($oPath->manageDir("user_bundle/data/images/user_{$user->id}.jpg")))
            $userImg="<div class='chatOnline img' style='background-image:url(" . $oPath->manage("user_bundle/data/images/user_{$user->id}.jpg") . ");'></div>";
         else
            $userImg="<div class='chatOnline img' style='background-image:url(" . $oPath->asset("default/images/user_larg.png") . ");border-radius:0%' ></div>";
         
     			
         $codeTr.="
         <tr>
			   <td style='text-align:center;width: 10px;'><input type='checkbox' id='chk_{$i}' value='{$item->id}' /></td>
				<td style='width:130px'>{$userImg}<br>{$userLink}</td>
            <td><button class='btn btn-danger' onclick='shopItems_likeDel({$itemId},{$item->id});'><i class='fa fa-trash-o'></i></button></td>
         </tr>";
      }
		$chkCount=$i;
      $code= "
      <div class='panel panel-info'>
         <div class='panel-titleBar'>پسندیدگان محصول {$item->title}</div>
			<div class='panel-header'>
			<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
			&nbsp;|&nbsp;
			<button class='btn btn-danger' onclick='shopItems_likeDel({$itemId},\"selected\");'><i class='fa fa-trash-o'></i></button>
			&nbsp;|&nbsp;
			<button class='btn' onclick='shopItems_likesDraw({$itemId},1,{$page});'><i class='fa fa-search'></i></button>
			<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/><br>		
			</div>			
         <div class='panel-body'>
				<br>{$btnPageCode}<br><br>
            <table class='tbl tbl-center tbl-bordered tbl-hover'>
				   <tr>
					   <th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>
					   <th>کاربر</th>
					   <th>عملیات</th>
					</tr>
				   {$codeTr}
            </table>
				<br>{$btnPageCode}<br>
         </div>
		</div>
		<br><br>
      ";
		cEngine::response("ok[|]" . $code);
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_likeDel")
   {
      $itemId=$oDb->escape($_REQUEST["itemId"]);
      $likeId=$oDb->escape($_REQUEST["id"],false);
		
      //get session for back
      $showSearch=$_SESSION["shopItems_likesShowSearch"];
      $page=$_SESSION["shopItems_likesPage"];

		
      $ids=$oShopItems->deleteLikes($likeId);    
		cEngine::response("ok[|]{$itemId}[|]{$showSearch}[|]{$page}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_opineDraw")
   {
		$itemId=$oDb->escape($_REQUEST["itemId"]);
		$confirm=$oDb->escape($_REQUEST["confirm"]);
		$checkOut=$oDb->escape($_REQUEST["checkOut"]);
		$page=@$_REQUEST["page"] ? cDataBase::escape($_REQUEST["page"]) : 1;
		
		//auto
		if($page=="-1") $page=$_SESSION["shopItemsOpine_page"];
		if($itemId=="-1") $itemId=$_SESSION["shopItemsOpine_itemId"];
		if($confirm=="-1") $confirm=$_SESSION["shopItemsOpine_confirm"];
		if($checkOut=="-1") $checkOut=$_SESSION["shopItemsOpine_checkOut"];
		
		//save
		$_SESSION["shopItemsOpine_page"]=$page;
		$_SESSION["shopItemsOpine_itemId"]=$itemId;
		$_SESSION["shopItemsOpine_confirm"]=$confirm;
		$_SESSION["shopItemsOpine_checkOut"]=$checkOut;
      
      //START PAGE ...................................
      $items=$oShopItems->opine_getAll($itemId,$confirm,$checkOut);
      $count=count($items);
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
		if($pages["pagesEnd"] > 1)
		{
			for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='shopItems_opineDraw(-1,-1,-1,{$i});'>{$i}</a>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='shopItems_opineDraw(-1,-1,-1,{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='-1,-1,-1,{$next});'><i class='fa fa-angle-left'></i></a>";		      
		}
		$items=$oShopItems->opine_getAll($itemId,$confirm,$checkOut,$pages['sqlLimit']);		
		//END PAGE ...................................
		
		if($itemId)
		{
			$shopItem=@$oShopItems->get($itemId);
			if($shopItem) $title=$shopItem->title; else $title='جدید';
		}
		else
		{
			$shopItem='';
			$title='جدید';
		}
		
		$i=(($page-1)*3);
		$codeTr="";
      foreach($items as $item)
      {
         $i++;
		
			if(!$shopItem) $shopItem=$oShopItems->get($item->itemId);
			
         if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$item->id}.html")))
            $status='<span style="color: #009900">پاسخ داده شده</span>';
         else
            $status='<span style="color: #990000">پاسخ داده نشده</span>';
			
         if($item->confirm=="0") $confirm="<button class='btn btn-danger' onclick='shopItems_opineConfirm({$item->id});'>تایید نشده</button>";
         elseif($item->confirm=="1") $confirm="<button class='btn btn-success' onclick='shopItems_opineConfirm({$item->id});'>تایید شده</button>";			

			$user=@$oUsers->get($item->userId);
			$user=@"<a href='javascript:void(0)' onclick='user_profileDraw({$user->id});'>{$user->userName}</a>";
			
			//image info
			if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$shopItem->id}.jpg")))
				$shopItemImg="<img class='brd-radius-8' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$shopItem->id}.jpg?t=" . time() ) ."' style='width:86px' />";
			else
				$shopItemImg="<img class='brd-radius-8' src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";			
			$shopItemTitle=$shopItemImg . "<br>" . $shopItem->titleFa;
			
         $date=jdate("Y/m/d h:t:i",$item->id);
         if($item->checkOut=="0") $style="style='font-weight: bolder;color:#000;font-size:17px;'"; else $style="color:#666";
         $codeTr.="
			<tr>";
				if(count($items) > 1) $codeTr.="<td style='text-align:center;width: 10px;'><input type='checkbox' id='chk_{$i}' onclick='checkedOne();' value='{$item->id}' /></td>";
				$codeTr.="
				<td {$style}>{$shopItemTitle}</td>
				<td {$style}>{$user}</td>
				<td {$style}>{$status}</td>
				<td {$style}>{$date}</td>
				<td {$style}>
				   <span id='td_active_{$item->id}'>{$confirm}</span>
				   <button class='btn btn-danger' onclick='shopItems_opineDel({$item->id});'><i class='fa fa-trash-o'></i></button>
				   <button class='btn btn-info' onclick='shopItems_opineView({$item->id});'><i class='fa fa-eye'></i></button>
				</td>
			</tr>";
      }
		$chkCount=$i;
		if($codeTr)
		{
			$code= "
				<div class='vSpace-4x'></div>
				<h1><i class='fa fa-list'></i>&nbsp;نظرات {$title}</h1>
				<div class='vSpace-4x'></div>";
				if($itemId) $code.="<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>";
			if(count($items) > 1) $code.="<button id='btn_trash' class='btn btn-danger' onclick='shopItems_opineDel(\"selected\");' disabled><i class='fa fa-trash-o'></i></button>";			
			$code.= "
			<div class='vSpace'></div>	
			<table class='tbl tbl-striped tbl-bordered tbl-hover' style='direction:rtl'>
				<tr>";
					if(count($items) > 1) $code.="<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>";
					$code.="
					<th class='text-right'>موضوع</th>
					<th class='text-right'>کاربر</th>
					<th class='text-right'>وضعیت</th>
					<th class='text-right'>تاریخ</th>
					<th class='text-right'></th>
				</tr>
				{$codeTr}
			</table>
			{$btnPageCode}
			<br><br>";
		}
		else
		{
			$code= "
				<div class='vSpace-4x'></div>
				<h1><i class='fa fa-list'></i>&nbsp;نظرات {$title}</h1>
				<div class='vSpace-4x'></div>";
				if($itemId) $code.="<button class='btn btn-default' onclick='shopItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>";
			$code.= "
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'>";			
		}
		cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
	else if($request=="shopItems_opineNotification")
   {
		$count=count($oShopItems->getOpineAll("","","0"));
		if(!isset($_SESSION["productItems_opineNewCount"])) 
			$_SESSION["productItems_opineNewCount"]=$count;
		else if($_SESSION["productItems_opineNewCount"] > $count)
		{
		   $_SESSION["productItems_opineNewCount"]=$count;	
		}
		if($_SESSION["productItems_opineNewCount"] != $count)
		{
			$isNew="new";
		}
		else
		{
		   $isNew="";	
		}
		$_SESSION["productItems_opineNewCount"]=$count;
      cEngine::response("ok[|]{$count}[|]{$isNew}");
		exit;
   }//------------------------------------------------------------------------------	
   else if($request=="shopItems_opineDel")
   {
      $ids=cDataBase::escape($_REQUEST["ids"],false);
      $ids=$oShopItems->opine_delete($ids);
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				@unlink ($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$selectedId}.html"));
				@unlink ($oPath->manageDir("shop_bundle/data/contents/itemOpineContent_{$selectedId}.html"));
			}
		} 		
		
      cEngine::response("ok");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_opineConfirm")
   {
      $id=$oDb->escape($_REQUEST["id"]);
      $ret=$oShopItems->opine_getConfirm($id);
		if($ret==true)
		{
		   $oShopItems->opine_setConfirm($id,"0");	
			$code="<button class='btn btn-danger' onclick='shopItems_opineConfirm({$id});'>تایید نشده</button>";
		}
		else
		{
			$oShopItems->opine_setConfirm($id,"1");	
			$code="<button class='btn btn-success' onclick='shopItems_opineConfirm({$id});'>تایید شده</button>";
		}
      cEngine::response("ok[|]{$id}[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------	
   else if($request=="shopItems_opineView")
   {
      $opineId=$oDb->escape($_REQUEST["id"]);
      $item=$oShopItems->opine_get($opineId);
		$oShopItems->opine_setCheckOut($opineId,"1");
      
		//content and answer
		$content=@file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemOpineContent_{$opineId}.html"));
		if(file_exists($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$opineId}.html")))
		{	
         $answerContent="<b>پاسخ : </b>" . file_get_contents($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$opineId}.html"));
		   $status="<span class='fg-success'>پاسخ داده شده</span>";
		}
		else
      {			
			$answerContent="";
			$status="<span class='fg-warning'>پاسخ داده نشده</span>";
		}
		
		if($item->confirm=="0") $optnConfirm="<option value='1'>تایید شده</option><option value='0' selected='selected'>تایید نشده</option>"; 
		else if($item->confirm=="1") $optnConfirm="<option value='1' selected='selected'>تایید شده</option><option value='0'>تایید نشده</option>"; 

		$user=$oUsers->get($item->userId);
		if(file_exists($oPath->manageDir("user_bundle/data/images/user_{$item->userId}.jpg")))
			$userImage="<div class='chatOnline img' style='background-image:url(" . $oPath->manage("user_bundle/data/images/user_{$item->userId}.jpg") . ");width:64px;height:64px;'></div>";
		else
			$userImage="<div class='chatOnline img' style='background-image:url(" . $oPath->asset("default/images/user_larg.png") . ");border-radius:0%' ></div>";
				
		$date=jdate("Y/m/d h:t:i",$item->id);
      $code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-eye'></i>&nbsp;نظر</h1>
		<div class='vSpace-4x'></div>
		<div class='dir-rtl'>
			<table class='tbl tbl-bordered'>
				<tr>
					<th>ایجاد شده</th>
					<th>وضعیت پاسخ</th>
					<th>وضعیت نمایش</th>
				</tr>
				<tr>
					<td>{$date}</td>
					<td>{$status}</td>
					<td><select id='slct_confirm' style='width:120px;'>{$optnConfirm}</select></td>
				</tr>								   
			</table>
		
			<div class='vSpace-2x'></div>
						
			<!-- opine content -->
			<div class='panel'>
				<div class='panel-titleBar'>نظر کاربر</div>
				<div class='panel-header'>
					<table>
						<tr>
							<td class='hide-auto'>{$userImage}</td>
							<td class='hide-auto'>&nbsp;|&nbsp;</td>
							<td><span>{$user->userName}</span></td>
							<td>&nbsp;|&nbsp;</td>
							<td><i class='fa fa-calendar'></i>&nbsp;{$date}</td>
						</tr>
					</table>
				</div>
				<div class='panel-body'>
					{$content}  
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<!-- opine answer Content -->
			
			<div class='panel'>
				<div class='panel-titleBar'>پاسخ به نظر کاربر</div>
				<div class='panel-header'>
               {$answerContent}
				</div>			
			   <div class='panel-body'>
					<div class='form dir-rtl algn-r'>
						<label>متن پاسخ</label>
						<textarea id='txt_answer'></textarea>
					</div>				
				</div>
			</div>			
		</div>
				
		<div class='vSpace-4x'></div>
		<hr>
		<button class='btn btn-default' onclick='shopItems_opineDraw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
		<button class='btn btn-success' onclick='shopItems_opineAnswer({$opineId});'>ارسال</button>		
		<div class='vSpace-4x'></div>
      ";
		cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_opineAnswer")
   {
      $opineId=cDataBase::escape($_REQUEST["id"]);
      $confirm=cDataBase::escape($_REQUEST["confirm"]);
		$answerContent=$oTools->str_tagFitSend_decode($_REQUEST["answerContent"]);
		//save ansver
		if(!file_exists($oPath->manageDir("shop_bundle/data"))) mkdir($oPath->manageDir("shop_bundle/data"));
		if(!file_exists($oPath->manageDir("shop_bundle/data/contents"))) mkdir($oPath->manageDir("shop_bundle/data/contents"));
      file_put_contents($oPath->manageDir("shop_bundle/data/contents/itemOpineAnswer_{$opineId}.html"),$answerContent);
      //set confirm
		$oShopItems->opine_setConfirm($opineId,$confirm);
		cEngine::response("ok");
		exit;
   }//------------------------------------------------------------------------------	
?>
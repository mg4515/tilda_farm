<?php
   @session_start();
	
	//include
	require_once $_SESSION["engineRequire"];//engine.php
	require_once $oPath->manageDir("shop_bundle/model/shopItems_model.php");
	require_once $oPath->manageDir("shop_bundle/model/shopCoupon_model.php");
	require_once $oPath->manageDir("shop_bundle/model/shopFactor_model.php");
	require_once $oPath->manageDir("users_bundle/model/users_model.php");
	require_once $oPath->manageDir("pay_bundle/model/pay_model.php");
	require_once $oPath->manageDir("jdf.php");
	
	//object
   $oShopItems=new cShopItems();
   $oShopCoupon=new cShopCoupon();
   $oShopFactor=new cShopFactor();
   $oUsers=new cUsers();
   $oPay=new cPay();
	
	//request name
   $request=@$_REQUEST["requestName"];

   if($request=="shopItems_opineSend")
   {
		if(!isset($_SESSION['user_id'])){$oEngine->response("!login");exit;}
		$comment=cDataBase::escape($_REQUEST["comment"]);
		$filmItemId=cDataBase::escape($_REQUEST["filmItemId"]);
		$userId=$_SESSION['user_id'];
      $opineId=$oShopItems->opine_insert($userId,$filmItemId);
      file_put_contents($oPath->manageDir("film_bundle/data/contents/itemOpineContent_{$opineId}.html"),$comment);
		$oEngine->response("ok");		
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_attachDownload")
   {
		$shopItemId=cDataBase::escape($_REQUEST["shopItemId"]);
		$attachType=cDataBase::escape($_REQUEST["attachType"]);
		$shopItem=$oShopItems->get($shopItemId);
      if($attachType=='file1')
		{
			$oShopItems->setAttachCountDownload1($shopItemId,'+');
			$path=$oPath->manage("shop_bundle/data/{$shopItem->attachName1}");
		}
      if($attachType=='file2')
		{
			$oShopItems->setAttachCountDownload2($shopItemId,'+');
			$path=$oPath->manage("shop_bundle/data/{$shopItem->attachName2}");
		}
      if($attachType=='url1')
		{
			$oShopItems->setAttachUrlCountDownload1($shopItemId,'+');
			$path=$oPath->manage("shop_bundle/data/{$shopItem->attachUrl1}");
		}
      if($attachType=='url2')
		{
			$oShopItems->setAttachUrlCountDownload2($shopItemId,'+');
			$path=$oPath->manage("shop_bundle/data/{$shopItem->attachUrl2}");
		}
		
      
		$oEngine->response("ok[|]{$path}[|]{$attachType}");		
   }//------------------------------------------------------------------------------------ 
   else if($request=="shopItems_like")
   {
		$shopItemId=cDataBase::escape($_REQUEST["shopItemId"]);
		if(!isset($_SESSION['user_id'])){cEngine::response("!login[|]{$shopItemId}");exit;}
      $userId=$_SESSION["user_id"];
		$likeId=$oShopItems->likes_check($shopItemId,$userId);
		if($likeId)
		{
			$oShopItems->likes_delete($likeId);
			$isLike=0;
		}
		else
		{
			$oShopItems->likes_insert($filmItemId,$userId);
			$isLike=1;
		}
		$likeCount=count($oShopItems->likes_getAll($filmItemId));
		$oEngine->response("ok[|]{$filmItemId}[|]{$isLike}[|]{$likeCount}");		
   }//------------------------------------------------------------------------------------
   else if($request=="shopItems_pay")
   {
      @session_start();
      $itemId=cDataBase::escape($_REQUEST['itemId']);
      $userMail=cDataBase::escape(@$_REQUEST['userMail']);
      $userMobile=cDataBase::escape(@$_REQUEST['userMobile']);
      $userMarketerId=isset($_REQUEST['userMarketerId']) ? cDataBase::escape(@$_REQUEST['userMarketerId']) : 0;
      $coupon=cDataBase::escape(@$_REQUEST['coupon']);
      $payTypeId=cDataBase::escape(@$_REQUEST['payTypeId']);
		//---
		$userId=isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
		
		//shop item
		$shopItem=$oShopItems->get($itemId);
		
		//check allow coupon
		if($coupon)
		{
			$coupon=$oShopCoupon->isAllow();
			if(!$coupon)
			{
				cEngine::response('!coupon');
				exit;
			}
         else
			{
				$price=$coupon['price'];
				$shopCouponId=$coupon['id'];
			}
		}
		else
		{
			$shopItem_=$oShopItems->calc($shopItem);
			$price=$shopItem_->price;
			$shopCouponId=0;
		}
      
		
		if($price > 0)
		{
			$orderId=substr(time(),5); //factor id
         $amount=$price;
			$description='product ' . $shopItem->id;
			$callBackUrl= $oPath->root("cb"); //http://www.mydomin.com/cb
			$otherData='';
         $payRet=$oPay->start($payTypeId,$orderId,$amount,$description,$callBackUrl,$otherData='');
			
			if($payRet)
			{
				//register db
				$oShopFactor->insert([
					'id'=>$orderId,
					'userMarketerId'=>$userMarketerId,
					'userId'=>$userId,
					'userIp'=>cTools::misc_ip(),
					'userMobile'=>$userMobile,
					'userMail'=>$userMail,
					'shopItemId'=>$shopItem->id,
					'shopCouponId'=>$shopCouponId
				]);
				
				//response
				cEngine::response("ok[|]{$price}[|]{$payRet}");
			}
			else
				cEngine::response("err");
		}
		else
			cEngine::response("err");
   }//------------------------------------------------------------------------------
  	
?>
<?php
   @session_start();
	
	//request
	include_once $_SESSION["engineRequire"];//engine.php
	require_once $oPath->manageDir("shop_bundle/model/shopFactor_model.php");	
	require_once $oPath->manageDir("shop_bundle/model/shopItems_model.php");	
	require_once $oPath->manageDir("users_bundle/model/users_model.php");	
	require_once $oPath->manageDir("jdf.php");

	//object
	$oShopFactor=new cShopFactor();
	$oShopItems=new cShopItems();
	$oUsers=new cUsers();
	
   //var
	$arryPayStatusFilter=[''=>'', 0=>'تراکنش های ناموفق', 1=>'تراکنش های موفق'];	
	
	//request
   $request=@$_REQUEST["requestName"];

	if($request=="shopFactor_draw")
   {
		$backName=cDataBase::escape(@$_REQUEST["backName"]);
      $payStatus=cDataBase::escape(@$_REQUEST["payStatus"]);
      $userId=cDataBase::escape(@$_REQUEST["userId"]);
      $userSellerId=cDataBase::escape(@$_REQUEST["userSellerId"]);
      $userMarketerId=cDataBase::escape(@$_REQUEST["userMarketerId"]);
      $checkout=cDataBase::escape(@$_REQUEST["checkout"]);
		$searchWord=cDataBase::escape(@$_REQUEST["searchWord"]);
		$dateStart=cDataBase::escape(@$_REQUEST["dateStart"]);
		$dateEnd=cDataBase::escape(@$_REQUEST["dateEnd"]);
		$page=@$_REQUEST["page"] ? cDataBase::escape($_REQUEST["page"]) : 1;
		
		//auto by session
		if($backName==-1) $backName=@$_SESSION["shopItems_backName"];
      if($payStatus==-1) $payStatus=$_SESSION["shopFactor_payStatus"];
      if($userId==-1) $userId=$_SESSION["shopFactor_userId"];
      if($userSellerId==-1) $userSellerId=$_SESSION["shopFactor_userSellerId"];
      if($userMarketerId==-1) $userMarketerId=$_SESSION["shopFactor_userMarketerId"];
      if($checkout==-1) $checkout=@$_SESSION["shopFactor_checkout"];
		if($searchWord==-1) $searchWord=$_SESSION["shopFactor_searchWord"];
		if($dateStart==-1) $dateStart=$_SESSION["shopFactor_dateStart"];
		if($dateEnd==-1) $dateEnd=$_SESSION["shopFactor_dateEnd"];
		if($page==-1) $page=$_SESSION["shopFactor_page"];
      
		//init session
		$_SESSION["shopItems_backName"]=$backName;
      $_SESSION["shopFactor_payStatus"]=$payStatus;
      $_SESSION["shopFactor_userId"]=$userId;
      $_SESSION["shopFactor_userSellerId"]=$userSellerId;
      $_SESSION["shopFactor_userMarketerId"]=$userMarketerId;
      $_SESSION["shopFactor_checkout"]=$checkout;
		$_SESSION["shopFactor_searchWord"]=$searchWord;
		$_SESSION["shopFactor_dateStart"]=$dateStart;
		$_SESSION["shopFactor_dateEnd"]=$dateEnd;
		$_SESSION["shopFactor_page"]=$page;
			
		//filter payStatus
		$slctPayStatusFilter='';
		foreach($arryPayStatusFilter as $key=>$val)
		{
			if($payStatus==$key && $payStatus!=='') $selected='selected'; else $selected='';
			$slctPayStatusFilter.="<option value='{$key}' {$selected}>{$val}</option>";
		}
		$slctPayStatusFilter="<select id='slct_confirmFilter' onchange='shopFactor_draw(\"auto\",this.value,undefined);'>{$slctPayStatusFilter}</select>";	
		//---
		if($payStatus=='0')
			$title='تراکنش های خرید ناموفق';
		else if($payStatus > 0)
			$title='تراکنش های خرید موفق';
		else if($checkout=='1')
			$title='تراکنش های خرید جدید';
		else
			$title='تراکنش های خرید';		

		//by user Seller
		$lblSellerIncome='';
		if($userSellerId > 0) 
		{
			$title='فروش های من';
			//seller income
			$sellerIncome=$oShopFactor->getSellerIncome($userSellerId);
			$sellerIncome=number_format($sellerIncome,0,',','.');
			$lblSellerIncome="<label class='lbl fg-success'>پورسانت : {$sellerIncome} تومان</label>";
		}

		//by user Seller
		$lblMarketerIncome='';
		if($userMarketerId > 0) 
		{
			$title='بازاریابی های من';
			//Marketer income
			$marketerIncome=$oShopFactor->getMarketerIncome($userMarketerId);
			$marketerIncome=number_format($marketerIncome,0,',','.');
			$lblMarketerIncome="<label class='lbl fg-success'>پورسانت : {$marketerIncome} تومان</label>";		
		}
		
		//title+ by search word
		if($searchWord) $title.= " - جستجو ({$searchWord})";
		
		//back button
		$btnBack='';
		if($backName)
		{
			$btnBack="<button class='btn btn-default' onclick='{$backName}(\"auto\");'><i class='fa fa-arrow-right'></i></button>&nbsp;|&nbsp;";
		}
		
		//START PAGE ...................................
      $arg=array(
			"payStatus"=>$payStatus,
			"userId"=>$userId,
			"userSellerId"=>$userSellerId,
			"userMarketerId"=>$userMarketerId,
			"checkout"=>$checkout,
			"trash"=>'0',
			"searchWord"=>$searchWord,
			"dateStart"=>$dateStart,
			"dateEnd"=>$dateEnd,
			"limitStr"=>""
		);		
      $items=$oShopFactor->getAll($arg);
      $count=count($items);
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
		for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
		{
			if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
				$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
			else
				$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='shopFactor_draw({\"payStatus\":-1,\"checkout\":-1,\"trash\":-1,\"searchWord\":-1,\"page\":{$i}});'>{$i}</a>";
		}
      $back=$page - 1;
      $next=$page + 1;
      if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='shopFactor_draw({\"payStatus\":-1,\"checkout\":-1,\"trash\":-1,\"searchWord\":-1,\"page\":{$back}});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='shopFactor_draw({\"payStatus\":-1,\"checkout\":-1,\"trash\":-1,\"searchWord\":-1,\"page\":{$next}});'><i class='fa fa-angle-left'></i></a>";		
      if($pages["pagesEnd"] < 2 ) $btnPageCode='';
      $arg=array(
			"payStatus"=>$payStatus,
			"userSellerId"=>$userSellerId,
			"userMarketerId"=>$userMarketerId,
			"checkout"=>$checkout,
			"trash"=>'0',
			"searchWord"=>$searchWord,
			"dateStart"=>$dateStart,
			"dateEnd"=>$dateEnd,
			"limitStr"=>$pages['sqlLimit']
		);	      
		$items=$oShopFactor->getAll($arg);
		//END PAGE ...................................
		
      $codeTr="";
      $i=0;
		foreach($items as $item)
		{
			//payStatus
			$lbl_payStatus='';
			if($item->payStatus=='0') 
				$lbl_payStatus="<label class='lbl fg-danger'>تراکنش ناموفق</label>";
			else if($item->payStatus == '1') 
				$lbl_payStatus="<label class='lbl fg-success'>تراکنش موفق</label>";
			
			//shopItem info
			if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$item->shopItemId}.jpg")))
				$img_shopItem="<img class='brd-radius-8' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$item->shopItemId}.jpg?t=" . time() ) ."' style='width:64px' />";
			else
				$img_shopItem="<img class='brd-radius-8' src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";
			
			//price
			$price=number_format($oShopFactor->getPrice($item->id)) . ' تومان';
		 
			//files for download
			$shopItem=$oShopItems->get($item->shopItemId);
			$filePath1=$oPath->manageDir("shop_bundle/data/{$shopItem->fileName1}");
			$filePath2=$oPath->manageDir("shop_bundle/data/{$shopItem->fileName2}");		
			$div_download='';
			if(($shopItem->fileName1 && file_exists($filePath1)) || $shopItem->fileUrl1)
			{
				$div_download.="
				<div>
					<button class='btn btn-small dir-ltr'>
						<a href='dl?i={$item->id}&n=1' target='_blank' class='fg-info'>دانلود فایل 1</a>
						&nbsp;
						<i class='fa fa-download'></i>
					</button>
				</div>
				";
			}
			if(($shopItem->fileName2 && file_exists($filePath1)) || $shopItem->fileUrl2)
			{
				$div_download.="
				<div>
					<button class='btn btn-small dir-ltr'>
						<a href='dl?i={$item->id}&n=2' target='_blank' class='fg-info'>دانلود فایل 2</a>
						&nbsp;
						<i class='fa fa-download'></i>
					</button>
				</div>
				";
			}			
			//---
			if($item->shopDownloadExpire < time())
				$spn_downloadExpire="<span class='lbl fg-danger'>پایان اعتبار</span>";
			else
			{
				$days=round(($item->shopDownloadExpire - time()) /60/60/24);
				$spn_downloadExpire="<span class='lbl fg-success'>اعتبار {$days} روز</span>";
			}
			//---
			$div_download=$div_download . "<hr>" . $spn_downloadExpire;
			if($item->payStatus==0) $div_download='<div><i class="fa fa-info-circle fa-2x fg-grayLight"></i></div>';
			
			//date
			$date=jDate('Y/m/d h:i:s', $item->insertDate);
			
			$i++;
			$codeTr.="
			<tr>";
				if(count($items) > 1) $codeTr.="<td style='text-align:center;width: 10px;'><input type='checkbox' id='chk_{$i}' onclick='checkedOne();' value='{$item->id}' /></td>";
				$codeTr.="
				<td>{$item->id}<br>{$lbl_payStatus}</td>
				<td>{$date}</td>
				<td style='padding:10px;width:100px;'>
					{$img_shopItem}
					<br>
					{$item->shopItemTitle}
				</td>";
				if(!$userMarketerId)
				{
					$codeTr.="
					<td>
						{$div_download}
					</td>";
				}
				$codeTr.="
				<td>
					<button class='btn btn-info' onclick='shopFactor_view({$item->id},\"shopFactor_draw\",{$userId});'><i class='fa fa-eye'></i></button>
				</td>
			</tr>";
		}
      if($codeTr)
		{
			$chkCount=$i;
			$code= "
			<div class='vSpace-4x'></div>
			<h1>{$btnBack}<i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>
			
			{$slctPayStatusFilter}
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopFactor_draw(\"auto\",$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			
			<div class='vSpace'></div>
			
			<table class='tbl tbl-center tbl-bordered tbl-hover'>
				<tr>";
					if(count($items) > 1) $code.="<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>";
					$code.="
					<th></th>
					<th>تاریخ</th>
					<th>محصول</th>";
					if(!$userMarketerId)
					{
						$code.="
						<th>
							فایل های دانلود
						</th>";
					}
					$code.="
					<th>عملیات</th>
				</tr>
				{$codeTr}
			</table>
			<br>{$btnPageCode}<br> 
			<div class='vSpace-4x'></div>";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1>{$btnBack}<i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>

			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopFactor_draw(\"auto\",undefined,$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input typetext' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'></div>";		
		}
		$oEngine->response("ok[|]" . $code);
   }//------------------------------------------------------------------------------------	
   else if($request=="shopFactor_del")
   {
      $itemId=cDataBase::escape($_REQUEST["id"],false);
      $ids=$shopFactor->delete($itemId);      
		$oEngine->response('ok');
   }//------------------------------------------------------------------------------------
   else if($request=="shopFactor_trash")
   {
      $itemId=cDataBase::escape($_REQUEST["id"],false);
      $ids=$shopFactor->trash($itemId);      
		$oEngine->response('ok');
   }//------------------------------------------------------------------------------------
   else if($request=="shopFactor_unTrash")
   {
      $itemId=cDataBase::escape($_REQUEST["id"],false);
      $ids=$shopFactor->unTrash($itemId);      
		$oEngine->response('ok');
   }//------------------------------------------------------------------------------------
	else if($request=="shopFactor_userDraw") //list kharidaran
   {
		$searchWord=cDataBase::escape(@$_REQUEST["searchWord"]);
		$dateStart=cDataBase::escape(@$_REQUEST["dateStart"]);
		$dateEnd=cDataBase::escape(@$_REQUEST["dateStart"]);
		$page=isset($_REQUEST["page"]) ? cDataBase::escape($_REQUEST["page"]) : 1;
		
		//auto session
		if($searchWord==-1) $searchWord=$_SESSION["shopFactor_searchWord"];
		if($dateStart==-1) $dateStart=$_SESSION["shopFactor_dateStart"];
		if($dateEnd==-1) $dateEnd=$_SESSION["shopFactor_dateEnd"];
		if($page==-1) $page=$_SESSION["shopFactor_page"];
      
		//init session
		$_SESSION["shopFactor_searchWord"]=$searchWord;
		$_SESSION["shopFactor_dateStart"]=$dateStart;
		$_SESSION["shopFactor_dateEnd"]=$dateEnd;
		$_SESSION["shopFactor_page"]=$page;
		
		//title
		$title='خریداران';

		//title+ by search word
		if($searchWord) $title.= " - جستجو ({$searchWord})";
		
		//START PAGE ...................................
      $arg=array(
			"payStatus"=>1,
			"trash"=>0,
			"searchWord"=>$searchWord,
			"dateStart"=>$dateStart,
			"dateEnd"=>$dateEnd,
			"limitStr"=>""
		);		
      $items=$oShopFactor->getAll($arg);
      $count=count($items);
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
		for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
		{
			if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
				$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
			else
				$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='shopFactor_usersDraw({\"searchWord\":-1,\"page\":{$i}});'>{$i}</a>";
		}
      $back=$page - 1;
      $next=$page + 1;
      if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='shopFactor_usersDraw({\"searchWord\":-1,\"page\":{$back}});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='shopFactor_usersDraw({\"searchWord\":-1,\"page\":{$next}});'><i class='fa fa-angle-left'></i></a>";		

      $arg=array(
			"payStatus"=>1,
			"trash"=>0,
			"searchWord"=>$searchWord,
			"dateStart"=>$dateStart,
			"dateEnd"=>$dateEnd,
			"limitStr"=>$pages['sqlLimit']
		);	      
		$items=$oShopFactor->getAll($arg);
		//END PAGE ...................................
		
      $codeTr="";
      $i=0;
		foreach($items as $item)
		{
			//user, kharidaran
			if($item->userId > 0)
				$user=@$oUsers->get($item->userId);
			else
				$user='';
			if($user) 
			{
				if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$user->id}.jpg")))
					$img_user="<div class='chatOnline img' style='background-image:url(" . $oPath->manage("users_bundle/data/images/user_{$user->id}.jpg") . ");'></div>";
				else
					$img_user="<div class='chatOnline img' style='background-image:url(" . $oPath->asset("default/images/user_larg.png") . ");border-radius:0%' ></div>";
				
				$html_user= $img_user . "<label class='lbl fg-success' onclick='oTools.link(\"admin?page=userEdit?i={$user->id}\")'>{$user->userName}</label>";
			}
			else
			{
				$img_user="<div class='chatOnline img' style='background-image:url(" . $oPath->asset("default/images/user_larg.png") . ");border-radius:0%' ></div>";
				$html_user= $img_user . "<label class='lbl fg-danger'>میهمان</label>";				
			}
			
			//date
			$date=jDate('Y/m/d h:i:s', $item->insertDate);
			
			$i++;
			$codeTr.="
			<tr>
				<td>{$html_user}</td>
				<td>
					<label>{$item->userIp}</label>
					<label>{$item->userMobile}</label>
					<label>{$item->userMail}</label>
				</td>
				<td>{$date}</td>
				<td>
					<button class='btn btn-info' onclick='shopFactor_view({$item->id},\"shopFactor_userDraw\",\"\");'><i class='fa fa-eye'></i></button>
				</td>
			</tr>";
		}
      if($codeTr)
		{
			$chkCount=$i;
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopFactor_draw(\"auto\",$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			
			<br>{$btnPageCode}<br><br>
			<table class='tbl tbl-center tbl-bordered tbl-hover'>
				<tr>
					<th>کاربر</th>
					<th>مشخصات</th>
					<th>تاریخ</th>
					<th></th>
				</tr>
				{$codeTr}
			</table>
			<br>{$btnPageCode}<br> 
			<div class='vSpace-4x'></div>";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>

			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='shopFactor_draw(\"auto\",undefined,$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input typetext' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'></div>";		
		}
		$oEngine->response("ok[|]" . $code);
   }//------------------------------------------------------------------------------------		
	if($request=="shopFactor_view") //list kharidaran
   {
		$id=cDataBase::escape($_REQUEST["id"]);
		$userId=cDataBase::escape(@$_REQUEST["userId"]);
		$backName=cDataBase::escape($_REQUEST["backName"],false);
		$item=$oShopFactor->get($id);
		
		//shopItem info
		if(file_exists($oPath->manageDir("shop_bundle/data/images/item0Thumb_{$item->shopItemId}.jpg")))
			$img_shopItem="<img class='brd-radius-8' src='" . $oPath->manage("shop_bundle/data/images/item0Thumb_{$item->shopItemId}.jpg?t=" . time() ) ."' style='width:160px' />";
		else
			$img_shopItem="<img class='brd-radius-8' src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";
		
		//files for download
		$shopItem=$oShopItems->get($item->shopItemId);
		$filePath1=$oPath->manageDir("shop_bundle/data/{$shopItem->fileName1}");
		$filePath2=$oPath->manageDir("shop_bundle/data/{$shopItem->fileName2}");		
		$div_download='';
		if(($shopItem->fileName1 && file_exists($filePath1)) || $shopItem->fileUrl1)
		{
			$div_download.="
			<div>
				<a href='dl?i={$item->id}&n=1' class='fg-info'><i class='fa fa-download'></i>&nbsp;دانلود فایل 1</a>
				&nbsp;|&nbsp;
				<span class='lbl lbl-small'><i class='fa fa-download'></i>&nbsp;دفعات دانلود : {$item->shopItemDownload1}</span>
			</div>
			";
		}
		if(($shopItem->fileName2 && file_exists($filePath1)) || $shopItem->fileUrl2)
		{
			$div_download.="
			<div>
				<a href='dl?i={$item->id}&n=2' class='fg-info'><i class='fa fa-download'></i>&nbsp;دانلود فایل 2</a>
				&nbsp;|&nbsp;
				<span class='lbl lbl-small'><i class='fa fa-download'></i>&nbsp;دفعات دانلود : {$item->shopItemDownload2}</span>
			</div>
			";
		}	
		//---
		if($item->shopDownloadExpire < time())
			$fileDownloadExpire="<span class='lbl fg-danger'>پایان اعتبار</span>";
		else
		{
			$days=round(($item->shopDownloadExpire - time()) /60/60/24);
			$fileDownloadExpire="<span class='lbl fg-success'>اعتبار {$days} روز</span>";
		}		
		
		//user, kharidaran
		if($item->userId > 0)
		{
			$user=@$oUsers->get($item->userId);
			$html_user= "<span class='lbl fg-success' onclick='oTools.link(\"admin?page=userEdit?i={$user->id}\")'>{$user->userName}</span>";
		}
		else
			$html_user= "<span class='lbl fg-danger'>میهمان</span>";	
							
		//user, forooshandeh
		if($item->userSellerId > 0)
		{
			$user=@$oUsers->get($item->userSellerId);
			$html_userSeller="<span class='lbl fg-success' onclick='oTools.link(\"admin?page=userEdit?i={$user->id}\")'>{$user->userName}</span>";
		}
		else
			$html_userSeller= "<span class='lbl fg-danger'>مدیریت</span>";	
		
		//user, bazaryab
		if($item->userMarketerId > 0)
		{
			$user=@$oUsers->get($item->userMarketerId);
			$html_userMarketer="<span class='lbl fg-success' onclick='oTools.link(\"admin?page=userEdit?i={$user->id}\")'>{$user->userName}</span>";
		}
		else
			$html_userMarketer= "<span class='lbl fg-danger'>بدون بازاریاب</span>";			
		
		//price
		$priceTotal=number_format($oShopFactor->getPrice($item->id)); //mohasebh shode
		//---
      $price=$item->shopItemPrice;
      $pricePlus=$item->shopPricePlus;
      $discount=$item->shopItemDiscount;
      $couponDiscount=$item->shopCouponDiscount;
      $couponMaxDiscount=$item->shopCouponMaxDiscount;	
      $couponTitle=$item->shopCouponTitle;
		$html_couponDiscount='';
		if($item->shopCouponDiscount > 0)
		{
			$price= $price + $pricePlus;
			if($discount > 0) $price=$price - (($discount * $price) / 100);
         //---
			$couponDiscount_=($couponDiscount * $price) / 100;
			if($couponDiscount_ > $couponMaxDiscount)
				$couponDiscount=number_format($couponMaxDiscount) . ' تومان';
			else
				$couponDiscount=$couponDiscount_ . '%';
			$html_couponDiscount="
			   <label><i class='fa fa-circle'></i>کوپن تخفیف</label>
				<span>{$couponTitle} | {$couponDiscount}</span>
			";
		}
		if($userId) $price= number_format($price + $pricePlus); else $price=number_format($price);
		$pricePlus=number_format($pricePlus);
		
		//date
		$date=jDate('Y/m/d h:i:s', $item->insertDate);		
		
		//status
		if($item->payStatus == 0)
			$html_payStatus='<span class="lbl fg-danger">تراکنش ناموفق</span>';
		else
			$html_payStatus='<span class="lbl fg-success">تراکنش موفق</span>';		
		
      //btn print
		$cssStyle=$oPath->asset('default/css/plugins/myStyle.css');
		$cssStyle.= ',' . $oPath->asset('user/css/style.css');
		$btn_print="<button class='btn btn-info' onclick='oTools.print($(\"#layer_content\").html(),\"{$cssStyle}\");'><i class='fa fa-print'></i>&nbsp;چاپ</button>";
				
		$code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-eye'></i>&nbspفاکتور {$item->id}</h1>
		<div class='vSpace-4x'></div>
		
		<div class='content' id='layer_content'>
		   <div class='panel'>
			   <div class='panel-body'>
				   <h3>مشخصات</h3>
					<label><i class='fa fa-circle'></i>وضعیت : <b>{$html_payStatus}</b></label>
					<label><i class='fa fa-circle'></i>شماره فاکتور : <b>{$item->id}</b></label>
					<label><i class='fa fa-circle'></i>تاریخ : <b>{$date}</b></label>
					<label class='hide'><i class='fa fa-circle'></i>خریدار : <b>{$html_user}</b></label>
					<label class='hide'><i class='fa fa-circle'></i>فروشنده : <b>{$html_userSeller}</b></label>
					<label class='hide'><i class='fa fa-circle'></i>بازاریاب : <b>{$html_userMarketer}</b></label>
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<div class='panel'>
			   <div class='panel-body'>
				   <h3>محصول</h3>
					{$img_shopItem}<br>
					{$item->shopItemTitle}
					<hr>
					{$div_download}
					{$fileDownloadExpire}
				</div>
			</div>
			
			<div class='vSpace'></div>
			
		   <div class='panel'>
			   <div class='panel-body'>
				   <h3>مبلغ فاکتور</h3>
					<label><i class='fa fa-circle'></i>مبلغ کل : <b>{$priceTotal}</b>&nbsp;تومان</label>
					<hr>
					<label><i class='fa fa-circle'></i>قیمت محصول : <b>{$price}</b>&nbsp;تومان</label>
					<label><i class='fa fa-circle'></i>تخفیف محصول : <b>{$discount}</b>&nbsp;%</label>
					";
					if(!$userId) $code.="<label><i class='fa fa-circle'></i>کاهش یا افزایش : <span class='dir-ltr'><b style='display: inline-block;'>{$pricePlus}</b></span>&nbsp;تومان</label>";
					$code.="
					{$html_couponDiscount}
				</div>
			</div>
		</div>
		
      <div class='vSpace-4x'></div>		
		<hr>
		<button class='btn btn-default' onclick='{$backName}(\"auto\");'><i class='fa fa-arrow-right'></i></button>
		{$btn_print}
		<div class='vSpace-4x'></div>
      ";
      cEngine::response("ok[|]{$code}");
	}//------------------------------------------------------------------------------------
	else if($request=="shopFactor_notification")
   {
		$count=count($shopFactor->getOpineAll("","","0"));
		if(!isset($_SESSION["shopFactor_opineNewCount"])) 
			$_SESSION["shopFactor_opineNewCount"]=$count;
		else if($_SESSION["shopFactor_opineNewCount"] > $count)
		{
		   $_SESSION["shopFactor_opineNewCount"]=$count;	
		}
		if($_SESSION["shopFactor_opineNewCount"] != $count)
		{
			$isNew="new";
		}
		else
		{
		   $isNew="";	
		}
		$_SESSION["shopFactor_opineNewCount"]=$count;
      $oEngine->response("ok[|]{$count}[|]{$isNew}");
   }//------------------------------------------------------------------------------	
?>
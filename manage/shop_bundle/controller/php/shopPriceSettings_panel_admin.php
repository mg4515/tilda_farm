<?php
   @session_start();
	
	//request
	include_once $_SESSION["engineRequire"];//engine.php
	require_once $oPath->manageDir("shop_bundle/model/shopPriceSettings_model.php");		

	//object
	$oShopPriceSettings=new cShopPriceSettings();	

	//request
   $request=$_REQUEST["requestName"];
	
   if($request=="shopPriceSettings_edit")
   {
      $item=$oShopPriceSettings->get();
		
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil-square-o'></i>&nbsp;تنظیمات قیمت</h1>		
		<div class='vSpace-4x'></div>
		
		<div class='panel'>
			<div class='panel-body form'>
				<label><i class='fa fa-circle'></i>قیمت پویا</label>
				<label>مقدار قیمت پویا، با تمام قیمت های محصولات جمع یا تفریق می شود</label>
				<label>از این امکان می توان برای ارزان کردن و یا گران کردن همه محصولات، استفاده کرد</label>
				<label>برای کاهش قیمت محصولات، از اعداد منفی استفاده نمایید</label>
				<input type='text' id='txt_pricePlus' value='{$item->pricePlus}' class='dir-ltr' required>&nbsp;تومان
				<hr>
				
				<label><i class='fa fa-circle'></i>درصد پورسانت برای فروشندگان</label>
				<input type='text' id='txt_siteCommission' value='{$item->siteCommission}' class='dir-ltr' required>&nbsp;%
				
				<label><i class='fa fa-circle'></i>درصد پورسانت برای بازاریابان</label>
				<input type='text' id='txt_marketerCommission' value='{$item->marketerCommission}' class='dir-ltr' required>&nbsp;%			
				
				<hr>
				<button class='btn btn-success' onclick='shopPriceSettings_update();'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
			</div>
		</div>
		
		<div class='vSpace-4x'></div>
      ";
      cEngine::response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($request=="shopPriceSettings_update")
   {
      $pricePlus=cDataBase::escape($_REQUEST['pricePlus']);
      $siteCommission=cDataBase::escape($_REQUEST['siteCommission']);
      $marketerCommission=cDataBase::escape($_REQUEST['marketerCommission']);
      $ret=$oShopPriceSettings->update($pricePlus, $siteCommission, $marketerCommission);
      cEngine::response("ok");
   }//------------------------------------------------------------------------------------
?>
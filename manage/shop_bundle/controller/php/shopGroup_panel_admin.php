<?php
   @session_start();

	//includes
	include_once $_SESSION["engineRequire"]; //engine.php
	require_once $oPath->manageDir("shop_bundle/model/shopGroup_model.php");

	//objects
	$oShopGroup=new cShopGroup();
	
	//request
	$request=@$_REQUEST["requestName"];
	
	//START GROUP
	if($request=="shopGroup_draw")
   {
		$parentId=cDataBase::escape(@$_REQUEST["parentId"]);
      
		//init session
		if($parentId=="-1") 
			$parentId=$_SESSION["shopGroup_parentId"]; //auto
		else
		   $_SESSION["shopGroup_parentId"]=$parentId;	
		
		if($parentId > 0)
		{
			$oldParent=$oShopGroup->get($parentId);
			$oldParentTitle=" - زیر دسته های دسته '" . $oldParent->title . "'";
		}
		else
			$oldParentTitle="";
      $items=$oShopGroup->getAll(true,$parentId);
		
      $codeTr="";
      $i=0;
      foreach($items as $item)
      {
         $i++;
         $groupId=$item->id;
			$countChilds=$oShopGroup->countChilds($groupId);
         
			if($item->active=="0") $active="<button class='btn btn-block-auto btn-danger' onclick='shopGroup_active({$groupId});'><i class='fa fa-undo'></i>&nbsp;غیر فعال</button>";
         elseif($item->active=="1") $active="<button class='btn btn-block-auto btn-success' onclick='shopGroup_active({$groupId});'><i class='fa fa-undo'></i>&nbsp;فعال</button>";
			
			if(file_exists($oPath->manageDir("shop_bundle/data/images/group_{$groupId}.jpg")))
				$img="<img src='" . $oPath->manage("shop_bundle/data/images/group_{$groupId}.jpg?t=" . time() ) ."' style='width:64px' />";
			else
				$img="<img src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";

         $codeTr.="
         <tr>
            <td style='text-align:center;padding:10px;width:100px;'>
				   {$img}
					<br>
					{$item->title}
					<hr>
					مکان {$item->row}
				</td>";
			if($parentId == 0) 
			{
			   $codeTr.="				
				<td>
               <button class='btn btn-label btn-label-right btn-warning' onclick='shopGroup_draw({$groupId});'>
                  <span>زیر دسته ها</span>
                  <span class='btn-label-caption'>{$countChilds}</span>
               </button>				
				</td>";
			}
			$codeTr.="
            <td>
					<span id='td_active_{$groupId}'>{$active}</span>
					<button class='btn btn-block-auto btn-danger' onclick='shopGroup_del({$parentId},{$groupId});'><i class='fa fa-trash-o'></i></button>
					<button class='btn btn-block-auto btn-info' onclick='shopGroup_edit({$parentId},{$groupId});'><i class='fa fa-pencil'></i></button>
				</td>
         </tr>";
      }
		
		if($codeTr)
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;دسته بندی ها{$oldParentTitle}</h1>
			<div class='vSpace-4x'></div>
			";
			if($parentId > 0) $code.="<button class='btn' onclick='shopGroup_draw({$oldParent->parentId})'><i class='fa fa-arrow-right'></i></button>";
			$code.="<button type='button' class='btn btn-success' onclick='shopGroup_new({$parentId});'><i class='fa fa-plus'></i>&nbsp;افزودن</button>				
			<div class='vSpace'></div>		
			<table class='tbl tbl-right tbl-bordered tbl-hover'>
				<tr>
					<th>دسته</th>";
					if($parentId == 0) $code.="<th></th>";
					$code.="
					<th>عملیات</th>
				</tr>
				{$codeTr}
			</table>
			<div class='vSpace-4x'></div>
			";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;دسته بندی ها{$oldParentTitle}</h1>
			<div class='vSpace-4x'></div>
			";
			if($parentId > 0) $code.="<button class='btn' onclick='shopGroup_draw({$oldParent->parentId})'><i class='fa fa-arrow-right'></i></button>";
			$code.="<button type='button' class='btn btn-success' onclick='shopGroup_new({$parentId});'><i class='fa fa-plus'></i>&nbsp;افزودن</button>				
			<hr>			
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'></div>
			";
		}
		cEngine::response("ok[|]" . $code);
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopGroup_del")
   {
		$parentId=cDataBase::escape(@$_REQUEST["parentId"]);
      $groupId=cDataBase::escape(@$_REQUEST["id"]);
      
		$oShopGroup->delete($groupId);
		@unlink($oPath->manageDir("shop_bundle/data/images/group_{$groupId}.jpg"));
		
		//chids delete
		$childsId=$oShopGroup->eachId($groupId);
		$idsArray=$oShopGroup->delete($childsId);
		for($i=0;$i < count($idsArray);$i++)
		{
			$childId=$idsArray[$i];
			@unlink($oPath->manageDir("shop_bundle/data/images/group_{$childId}.jpg"));
		}
		
		cEngine::response("ok[|]{$parentId}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="shopGroup_edit")
   {
		$parentId=isset($_REQUEST["parentId"]) ? cDataBase::escape($_REQUEST["parentId"]) : 0;
      $groupId=cDataBase::escape($_REQUEST["id"]);
      $item=$oShopGroup->get($groupId);

      $t=time();                                                             
      if(file_exists($oPath->manageDir("shop_bundle/data/images/group_{$groupId}.jpg")))
         $img="<img id='img_item' src='". $oPath->manage("shop_bundle/data/images/group_{$groupId}.jpg?t={$t}") ."' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel' class='fa fa-trash fa-2x' onclick='shopGroup_imgDel({$groupId});' style='cursor:pointer;color:red;'></i>";
      else
         $img="<img id='img_item' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
		
      $code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;ویرایش دسته ({$item->title})</h1>
		<div class='vSpace-4x'></div>
		
		<div class='form'>
			<label><i class='fa fa-circle'></i>اولویت مکانی</label>
			<label>مرتب سازی در نمایش دسته بندی ها، بر اساس این مقدار انجام می شود.</label>
			<label>عدد صفر، مقدار پیش فرض است و مکان دسته بندی را به صورت خودکار انتخاب می کند</label>
			<input type='number' id='txt_row' value='{$item->row}' class='dir-ltr' >			  
		
			<label><i class='fa fa-circle'></i>عنوان</label>
			<input type='text' id='txt_title' value='{$item->title}' >

			<label><i class='fa fa-circle'></i>توضیحات</label>
			<textarea id='txt_comment' class='form-control'>{$item->comment}</textarea>
			
			<label><i class='fa fa-circle'></i>تصویر دسته</label>
			{$img}
			<br>
			<label class='lbl-file lbl-file-right'>
				<input type='file' id='fileImg' onchange='oTools.previewImgFile(\"fileImg\",\"img_item\");' required/>
				<span class='lbl-file-icon'></span>
				<span>انتخاب تصویر</span>
			</label>
		
		</div>
		
		<div class='vSpace-4x'></div>			
		<hr>
		<button class='btn btn-default' onclick='shopGroup_draw({$parentId});'><i class='fa fa-arrow-right'></i></button>				
		<button class='btn btn-success' onclick='shopGroup_update(\"edit\",{$parentId},{$groupId});'>ذخيره</button>
		<div class='vSpace-4x'></div>
      ";

      cEngine::response("ok[|]{$code}");
      exit;		
   }//------------------------------------------------------------------------------------
   else if($request=="shopGroup_new")
   {
		$parentId=isset($_REQUEST["parentId"]) ? cDataBase::escape($_REQUEST["parentId"]) : 0;
      $img="<img id='img_item' src='". $oPath->asset("default/images/noImage.gif") ."' style='border-radius:8px;width:120px'>";
		
      $code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;دسته جدید</h1>
		<div class='vSpace-4x'></div>
		
		<div class='form'>
			<label><i class='fa fa-circle'></i>اولویت مکانی</label>
			<label>مرتب سازی در نمایش دسته بندی ها، بر اساس این مقدار انجام می شود.</label>
			<label>عدد صفر، مقدار پیش فرض است و مکان دسته بندی را به صورت خودکار انتخاب می کند</label>
			<input type='number' id='txt_row' value='0' class='dir-ltr'>			  
		
			<label><i class='fa fa-circle'></i>عنوان</label>
			<input type='text' id='txt_title' >

			<label><i class='fa fa-circle'></i>توضیحات</label>
			<textarea id='txt_comment' class='form-control'></textarea>
			
			<label><i class='fa fa-circle'></i>تصویر دسته</label>
			{$img}
			<br>
			<label class='lbl-file lbl-file-right'>
				<input type='file' id='fileImg' onchange='oTools.previewImgFile(\"fileImg\",\"img_item\");' required/>
				<span class='lbl-file-icon'></span>
				<span>انتخاب تصویر</span>
			</label>
		
		</div>
		
		<div class='vSpace-4x'></div>			
		<hr>
		<button class='btn btn-default' onclick='shopGroup_draw({$parentId});'><i class='fa fa-arrow-right'></i></button>				
		<button class='btn btn-success' onclick='shopGroup_update(\"new\",{$parentId},0);'>ذخيره</button>
		<div class='vSpace-4x'></div>
      ";

      cEngine::response("ok[|]{$code}");
      exit;		
   }//------------------------------------------------------------------------------------
   else if($request=="shopGroup_update")
   {
		$purpose=cDataBase::escape($_REQUEST["purpose"]);
		
		if($purpose=="edit")
		   $id=cDataBase::escape($_REQUEST["id"]);
	   else
			$id=time();
		
      $array=array();
      $array["purpose"]=cDataBase::escape($_REQUEST["purpose"]);
      $array["parentId"]=cDataBase::escape($_REQUEST["parentId"]);		
      $array["id"]=$id;
      $array["title"]=cDataBase::escape($_REQUEST["title"]);
      $array["comment"]=cDataBase::escape($_REQUEST["comment"]);
      $array["row"]=cTools::str_perIntToEnInt(cDataBase::escape($_REQUEST["row"]));
      
		if($array["purpose"]=="edit")
         $ret=$oShopGroup->update($array);
		else if($array["purpose"]=="new")
         $ret=$oShopGroup->insert($array);
		
      //folders
      if(!file_exists($oPath->manageDir("shop_bundle/data"))) mkdir($oPath->manageDir("shop_bundle/data"));
      if(!file_exists($oPath->manageDir("shop_bundle/data/images"))) mkdir($oPath->manageDir("shop_bundle/data/images"));
		$imgDel=$oDb->escape($_REQUEST["imgDel"]);
      if($imgDel==1 || $imgDel=="true") @unlink($oPath->manageDir("shop_bundle/data/images/group_{$id}.jpg"));
		
      if(count($_FILES) > 0 && isset($_FILES["file"]))
      {
         if($oTools->fs_fileIsImage($_FILES["file"]["name"]))
         {
            @copy($_FILES["file"]["tmp_name"],$oPath->manageDir("shop_bundle/data/images/group_{$id}.jpg"));
            @unlink($_FILES["file"]["tmp_name"]);
         }
      }
	  
		$parentId=$oDb->escape($_REQUEST["parentId"]);
      cEngine::response("ok[|]{$parentId}");
      exit;		
   }//------------------------------------------------------------------------------------
   else if($request=="shopGroup_active")
   {
      $groupId=$oDb->escape($_REQUEST["id"]);
      $item=$oShopGroup->get($groupId);
		
      if($item->active=="0")
      {
         $code="<button class='btn btn-success' onclick='shopGroup_active({$groupId});'>فعال</button>";
			$oShopGroup->setActive($groupId,"1");
      }
      elseif($item->active=="1")
      {
         $code="<button class='btn btn-danger' onclick='shopGroup_active({$groupId});'>غیر فعال</button>";
         $oShopGroup->setActive($groupId,"0");
      }
		
		cEngine::response("ok[|]{$groupId}[|]{$code}");	
      exit;		
   }//--------------------------------------------------------------------------
?>
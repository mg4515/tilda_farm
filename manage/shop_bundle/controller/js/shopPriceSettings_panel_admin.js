var url_ShopPriceSettings=oPath.manage("shop_bundle/controller/php/shopPriceSettings_panel_admin.php");
function shopPriceSettings_edit()
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_ShopPriceSettings,
         data: {"requestName":"shopPriceSettings_edit"},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopPriceSettings_update()
{
   try {
		err=0;
		if($('#txt_siteCommission').val()=='' || $('#txt_siteCommission').val() < 0)
		{
			script_alert2("","درصد پورسانت فروشندگان را مشخص نمایید","danger");
			script_focus("txt_siteCommission");
			err=true;				
		}
		else if($('#txt_marketerCommission').val()=='' || $('#txt_marketerCommission').val() < 0)
		{
			script_alert2("","درصد پورسانت بازاریابان را مشخص نمایید","danger");
			script_focus("txt_marketerCommission");
			err=true;				
		}
		if(err==false)
		{
			script_loadingShow();
			$.ajax({
				url: url_ShopPriceSettings,
				data: {'requestName':'shopPriceSettings_update', 'pricePlus':$('#txt_pricePlus').val(), 'siteCommission':$('#txt_siteCommission').val(), 'marketerCommission':$('#txt_marketerCommission').val() },
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split('[|]');
					if(spl[0]=="ok")
					{
						shopPriceSettings_edit();
						script_alert2('','با موفقیت ذخیره گردید','success');
					}
					else
					{
						alert('انجام نشد!.');
					}
					script_loadingHide();
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
		}
   } 
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
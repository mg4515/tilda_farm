var url_shopGroup=oPath.manage("shop_bundle/controller/php/shopGroup_panel_admin.php");
var isImageDelete=false;
var oMyModal=[];

function shopGroup_imgDel(groupId)
{
   if(isImageDelete==false)
   {
      script_confirm2("تصویر حذف شود ؟",'',function()
      {
         isImageDelete=true;
         $("#img_item").attr("src",oPath.asset("default/images/noImage.gif"));
         $("#i_itemImgDel").attr("class","fa fa-share fa-2x");
      });
   }
   else if(isImageDelete==true)
   {
      isImageDelete=false;
      $("#img_item").attr("src",oPath.manage("shop_bundle/data/images/group_" + groupId + ".jpg"));
      $("#i_itemImgDel").attr("class","fa fa-trash fa-2x");
   }
}//-----------------------------------------------------------------------------w
function shopGroup_draw(parentId)
{
	isImageDelete=false;
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopGroup,
         data: {"requestName":"shopGroup_draw", "parentId":parentId},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopGroup_del(parentId,id)
{
   script_confirm2('',"به صورت کلی از سیستم حذف شود ؟",function()
   {
      try {
         script_loadingShow();
         $.ajax({
            url: url_shopGroup,
            data: {"requestName":"shopGroup_del", "parentId":parentId, "id":id},
            method: "POST",
            success: function(result)
            {
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  shopGroup_draw(spl[1]);
               }
               else
               {
                  alert("انجام نشد");
                  alert(result);
               }
               script_loadingHide();
            },
            error: function() {
               script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
            }
         });
      } catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
      }
   });
}//-----------------------------------------------------------------------------
function shopGroup_active(id)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopGroup,
         data: {"requestName":"shopGroup_active","id":id},
         method: "POST",
         success: function(result)
         {
            //alert(result);
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#td_active_" + spl[1]).html(spl[2]);
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopGroup_new(parentId)
{
   try {
        script_loadingShow();
        $.ajax({
            url: url_shopGroup,
            data: {"requestName":"shopGroup_new", "parentId":parentId},
            method: "POST",
            success: function(result)
            {
               //alert(result);
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  $("#layer_content").html(spl[1]);
               }
               else
               {
                  alert("انجام نشد!.");
               }
               script_loadingHide();
            },
            error: function() {
					alert("خطایی در اتصال رخ داده است");
               script_loadingHide();
            }
        });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopGroup_edit(parentId,id)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopGroup,
         data: {"requestName":"shopGroup_edit", "parentId":parentId, "id":id },
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopGroup_update(purpose,parentId,id)
{
   try {
      script_loadingShow();
      var file_data = $('#fileImg').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);
		form_data.append("parentId",parentId);
		form_data.append("id",id);
		form_data.append("title",$("#txt_title").val());
		form_data.append("comment",$("#txt_comment").val());
		form_data.append("row",$("#txt_row").val());
		form_data.append("requestName","shopGroup_update");
		form_data.append('imgDel',isImageDelete);
		form_data.append("purpose",purpose);

      $.ajax({
         url: url_shopGroup,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               shopGroup_draw(spl[1]);
					script_alert2('','با موفقیت ذخیره گردید',"success");
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }

      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
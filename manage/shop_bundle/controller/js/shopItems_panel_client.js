var url_shopItems=oPath.manage("shop_bundle/controller/php/shopItems_panel_client.php");
var isImageDelete=[false,false,false,false,false,false];
oMyTag=[];

function shopItems_imgDel(itemId,index)
{
   if(isImageDelete[index]==false)
   {
		script_confirm2("تصویر حذف شود ؟",'',function(){
			isImageDelete[index]=true;
			$("#img_item" + index).attr("src",oPath.asset("default/images/noImage.gif"));
			$("#i_itemImgDel" + index).attr("class","fa fa-share fa-2x");
		});
   }
   else if(isImageDelete[index]==true)
   {
      isImageDelete[index]=false;
      $("#img_item" + index).attr("src",oPath.manage("shop_bundle/data/images/item" + index + "Thumb_" + itemId + ".jpg"));
      $("#i_itemImgDel" + index).attr("class","fa fa-trash fa-2x");
   }
}//-----------------------------------------------------------------------------
function shopItems_draw(obj,type_,searchWord_) //{userId, type, page, confirm, searchWord}
{
	isImageDelete=false;
	if(obj=="auto")
	{
		obj={ 
			"userId":-1,
			"type":-1,
			"page":-1,
			"confirm":-1,
			"searchWord":-1 	
	   }
	}
	if(type_ != undefined) obj.type=type_;
	if(searchWord_ != undefined) obj.searchWord=searchWord_;
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopItems,
         data: {"requestName":"shopItems_draw", "userId":obj.userId, "type":obj.type, "page":obj.page, "confirm":obj.confirm, "searchWord":obj.searchWord},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopItems_edit0(itemId) {
	try {
		script_loadingShow();
		isImageDelete=[false,false,false,false,false,false];
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_edit0","id":itemId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------
function shopItems_edit1(itemId) {
	try {
		script_loadingShow();
		isImageDelete=[false,false,false,false,false,false];
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_edit1","id":itemId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------
function shopItems_new0(backEvent) {
	try {
		script_loadingShow();
		if(backEvent == undefined) backEvent='';
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_new0", "backEvent": backEvent},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{	
					$("#layer_content").html(spl[1]);				
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() 
			{
				script_loadingHide();
				alert("خطایی در اتصال رخ داده است");
			}
		});
	} 
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function shopItems_new1(backEvent) {
	try {
		script_loadingShow();
		if(backEvent == undefined) backEvent='';
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_new1", "backEvent": backEvent},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{	
					$("#layer_content").html(spl[1]);					
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() 
			{
				script_loadingHide();
				alert("خطایی در اتصال رخ داده است");
			}
		});
	} 
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function shopItems_update(purpose,itemId,type)
{
   //try {
		err=false;
		if($("#txt_titleFa").val()=="")
		{
			script_alert2('',"تکمیل گزینه های ستاره دار الزامی است","danger");
			script_focus("txt_titleFa");
			err=true;			
		}			
		//---
		layer_group=$('#layer_group input[type="checkbox"]');
		groupId='';
      for($i=0;$i < layer_group.length;$i++)
		{
			if(layer_group[$i].checked==true) groupId+= layer_group[$i].value + ",";
		}
		if(!groupId && err==false)
		{
			script_alert2('',"دسته بندی ها را مشخص نمایید","danger");
			script_focus("layer_group");
			err=true;			
		}	

		if(err==false)
		{
			script_loadingShow();
			var formData = new FormData();
			//---
			var content = CKEDITOR.instances.txt_content.getData();
			content.replace(/(?:\\[rn])+/g, "<br>");
			content=oTools.str_tagFitSend_encode(content);
			//---
			var contentSampleFa = CKEDITOR.instances.txt_contentSampleFa.getData(); 
			contentSampleFa.replace(/(?:\\[rn])+/g, "<br>");
			contentSampleFa=oTools.str_tagFitSend_encode(contentSampleFa);
			//---
			if($("#txt_contentSampleEn").val())
			{
				var contentSampleEn = CKEDITOR.instances.txt_contentSampleEn.getData(); 
				contentSampleEn.replace(/(?:\\[rn])+/g, "<br>");
				contentSampleEn=oTools.str_tagFitSend_encode(contentSampleEn);
         }
			else
				contentSampleEn='';
         //---
			strGenreIds='';
			chkGenre=$('#layer_genre input[type="checkbox"]');
			for(i=0;i < chkGenre.length;i++)
			{
				if(chkGenre[i].checked==true) strGenreIds+= chkGenre[i].value + ','; 
			}
			//---
			tags=oMyTag.get();
			keywords='';
			console.log(tags);
			for($i=0;$i < tags.length;$i++)
			{
				keywords+=tags[$i].title + ',';
			}
			//---
			if($("#slct_publicationName").val() != 0) 
				publicationName=$("#slct_publicationName").val();
			else
				publicationName=$("#txt_publicationName").val(); 
			//---
			otherCount=$("#txt_otherCount").val();
			otherProperties='';
			for(i=0;i < otherCount;i++)
			{
				otherId=$("#txt_otherId" + i).val();
				otherTitle=$("#txt_otherTitle" + i).val(); 
				otherValue=$("#txt_otherValue" + i).val(); 
				otherProperties+='"' + otherId + '"' + ':"' + otherValue + '"';
            if(i<(otherCount-1)) otherProperties+=",";				
			}
			otherProperties='{' + otherProperties + '}';
			//---
			var file_attach1 = $('#file_attach1').prop('files')[0];
			var file_attach2 = $('#file_attach2').prop('files')[0];
			var file_attachFree1 = $('#file_attachFree1').prop('files')[0];
			var file_attachFree2 = $('#file_attachFree2').prop('files')[0];
			var file_imgItem0 = $('#file_imgItem0').prop('files')[0];
			var file_imgItem1 = $('#file_imgItem1').prop('files')[0];
			var file_imgItem2 = $('#file_imgItem2').prop('files')[0];
			var file_imgItem3 = $('#file_imgItem3').prop('files')[0];
			var file_imgItem4 = $('#file_imgItem4').prop('files')[0];
			var file_imgItem5 = $('#file_imgItem5').prop('files')[0];
			//---
			formData.append('imgDel0',isImageDelete[0]);
			formData.append('imgDel1',isImageDelete[1]);
			formData.append('imgDel2',isImageDelete[2]);
			formData.append('imgDel3',isImageDelete[3]);
			formData.append('imgDel4',isImageDelete[4]);
			formData.append('imgDel5',isImageDelete[5]);
			formData.append('groupId',groupId);
			formData.append('titleBefor',$("#txt_titleBefor").val()); 
			formData.append('titleFa',$("#txt_titleFa").val());
			formData.append('titleEn',$("#txt_titleEn").val());
			formData.append('countPageFa',$("#txt_countPageFa").val());
			formData.append('countPageEn',$("#txt_countPageEn").val());
			formData.append('property',$("#txt_property").val());
			formData.append('listFa',$("#txt_listFa").val());
			formData.append('listEn',$("#txt_listEn").val());
			formData.append('isSource',document.getElementById("chk_isSource").checked);
			formData.append('listLinked',$("#txt_listLinked").val());
			formData.append('publicationName',publicationName);
			formData.append('keywords',keywords);
			formData.append('comment','');
			formData.append('price',$("#txt_price").val()); 
			formData.append('isFree',document.getElementById("chk_isFree").checked);
			formData.append('discount',$("#txt_discount").val());
			formData.append('notDiscount',document.getElementById("chk_notDiscount").checked);
			formData.append('doi',$("#txt_doi").val());
			formData.append('json',$("#txt_json").val());
			formData.append('university',$("#txt_university").val());
			formData.append('gazette',$("#txt_gazette").val());
			formData.append('publicationYear',$("#txt_publicationYear").val());
			formData.append('marjaLink',$("#txt_marjaLink").val());
			formData.append('otherProperties',otherProperties);	
			formData.append('content',content);	
			formData.append('contentSampleFa',contentSampleFa);	
			formData.append('contentSampleEn',contentSampleEn);	
			formData.append('file_attach1',file_attach1);	
			formData.append('file_attach2',file_attach2);	
			formData.append('file_attachFree1',file_attachFree1);	
			formData.append('file_attachFree2',file_attachFree2);	
			formData.append('file_imgItem0',file_imgItem0);	
			formData.append('file_imgItem1',file_imgItem1);	
			formData.append('file_imgItem2',file_imgItem2);	
			formData.append('file_imgItem3',file_imgItem3);	
			formData.append('file_imgItem4',file_imgItem4);	
			formData.append('file_imgItem5',file_imgItem5);	
			formData.append('id',itemId);
			formData.append('type',type);
			formData.append('purpose',purpose);
			formData.append('requestName',"shopItems_update");			
			//---
			$.ajax({
				url: url_shopItems,
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
					if(spl[0]=="!imageType")
					{
						if(spl[1]=="0") var num="اصلی"; else var num=spl[1];
						script_alert2('',"نوع تصویر  " + num + "، غیر مجاز می باشد","danger");
						script_focus("img_item" + spl[1]);
					}
					else if(spl[0]=="!imageSize")
					{
						if(spl[1]=="0") var num="اصلی"; else var num=spl[1];
						script_alert2('',"سایز فایل تصویر " + num + "، بیش از حد مجاز است","danger");
					}
					else if(spl[0]=="!attachType")
					{
						if(spl[1]=="0") var num="اصلی"; else var num=spl[1];
						script_alert2('',"نوع فایل " + num + "، غیر مجاز می باشد","danger");
					}
					else if(spl[0]=="!attachSize")
					{
						if(spl[1]=="0") var num="اصلی"; else var num=spl[1];
						script_alert2('',"سایز فایل " + num + "، بیش از حد مجاز است","danger");
					}
					else if(spl[0]=="!attachFreeType")
					{
						if(spl[1]=="0") var num="اصلی"; else var num=spl[1];
						script_alert2('',"نوع فایل " + num + " نمی باشد","danger");
					}
					else if(spl[0]=="!attachFreeSize")
					{
						if(spl[1]=="0") var num="اصلی"; else var num=spl[1];
						script_alert2('',"سایز فایل " + num + "، بیش از حد مجاز است","danger");
					}
					else if(spl[0]=="ok")
					{
						purpose=spl[1];
						userId=spl[2];
						shopItems_draw({'userId':userId, 'type':'', 'page':1, 'searchWord':'', 'confirm':0});

						if(purpose=="new") 
							message="مقاله جدید، با موفقیت ذخیره گردید و بعد از تایید، انتشار خواهد یافت";
						else
							message="مقاله با موفقیت ویرایش گردید و بعد از تایید، انتشار خواهد یافت";
						script_alert2('',message,"success");
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		}
	//}
	//catch (e) 
	//{
		//alert("خطای اسکریپت");
		//script_loadingHide();
	//}
}//---------------------------------------------------------------------------------------

//services 
function shopItems_opineNotification() {
	try {
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_opineNotification"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
				   $("#spn_shopItemsOpine").html(spl[1]);
					if(spl[2]=="new") beep(3);
				}
			}
		});
	} 
	catch (e) {}
}//---------------------------------------------------------------------------------------
services_add("shopItems_opineNotification",function(){shopItems_opineNotification();});

var url_shopFactor=oPath.manage("shop_bundle/controller/php/shopFactor_panel_admin.php");

function shopFactor_draw(obj,payStatus,searchWord,dateStart,dateEnd) //{payStatus, checkout, trash, searchWord, dateStart, dateEnd, page}
{
	if(obj=="auto")
	{
		obj={
			"backName":-1,
			"payStatus":-1,
			"userSellerId":-1,
			"userMarketerId":-1,			
			"checkout":-1,
			"trash":-1,
			"searchWord":-1,
			"dateStart":-1,
			"dateEnd":-1,
			"page":-1
	   }
	}
	if(payStatus != undefined) obj.payStatus=payStatus;
	if(searchWord != undefined) obj.searchWord=searchWord;
	if(dateStart != undefined) obj.dateStart=dateStart;
	if(dateEnd != undefined) obj.dateEnd=dateEnd;
	
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopFactor,
         data: {"requestName":"shopFactor_draw", "backName":obj.backName, "payStatus":obj.payStatus, "userSellerId":obj.userSellerId, "userMarketerId":obj.userMarketerId, "checkout":obj.checkout, "trash":obj.trash, "searchWord":obj.searchWord, "dateStart":obj.dateStart, "dateEnd":obj.dateEnd, "page":obj.page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopFactor_del(itemId) {
	script_confirm2("حذف شود ؟",'',function()
	{
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_shopFactor,
					data: {"requestName": "shopFactor_del","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							shopFactor_draw('auto');
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
		}		
	})
}//---------------------------------------------------------------------------------------
function shopFactor_trash(itemId) {
	script_confirm2('',"موقتا حذف شود؟",function()
	{
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_shopFactor,
					data: {"requestName": "shopFactor_trash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							shopFactor_draw('auto');
							script_alert2('',"با موفقیت حذف شد","success");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
		}		
	})
}//---------------------------------------------------------------------------------------
function shopFactor_unTrash(itemId) {
	script_confirm2('',"بازیابی شود؟",function()
	{
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_shopFactor,
					data: {"requestName": "shopFactor_unTrash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							shopFactor_draw('auto');
							script_alert2('',"با موفقیت بازیابی شد","success");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
		}		
	});
}//---------------------------------------------------------------------------------------
function shopFactor_userDraw(obj,searchWord,dateStart,dateEnd) //{searchWord, dateStart, dateEnd, page} 
{
	if(obj=="auto")
	{
		obj={ 
			"searchWord":-1,
			"dateStart":-1,
			"dateEnd":-1,
			"page":-1
	   }
	}
	if(searchWord != undefined) obj.searchWord=searchWord;
	if(dateStart != undefined) obj.dateStart=dateStart;
	if(dateEnd != undefined) obj.dateEnd=dateEnd;
	
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopFactor,
         data: {"requestName":"shopFactor_userDraw", "searchWord":obj.searchWord, "dateStart":obj.dateStart, "dateEnd":obj.dateEnd, "page":obj.page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopFactor_view(id,backName) {
	try {
		script_loadingShow();
		isImageDelete=false;
		$.ajax({
			url: url_shopFactor,
			data: {"requestName": "shopFactor_view", "id":id, "backName":backName},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------

//services 
function shopFactor_notification() {
	try {
		$.ajax({
			url: url_shopFactor,
			data: {"requestName": "shopFactor_notification"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
				   $("#spn_filmItemsOpine").html(spl[1]);
					if(spl[2]=="new") beep(3);
				}
			}
		});
	} 
	catch (e) {}
}//---------------------------------------------------------------------------------------
services_add("shopFactor_notification",function(){shopFactor_notification();});

var url_shopProperty=oPath.manage("shop_bundle/controller/php/shopProperty_panel_admin.php");
var isImageDelete=[false,false,false,false,false,false];
var oMyModal=[];

function shopProperty_imgDel(propertyId,index)
{
   if(isImageDelete[index]==false)
   {
		script_confirm2("تصویر حذف شود ؟",'',function(){
			isImageDelete[index]=true;
			$("#img_option" + index).attr("src",oPath.asset("default/images/noImage.gif"));
			$("#i_itemImgDel" + index).attr("class","fa fa-share fa-2x");
		});
   }
   else if(isImageDelete[index]==true)
   {
      isImageDelete[index]=false;
      $("#img_option" + index).attr("src",oPath.manage("shop_bundle/data/images/propertyOptn" + index + "_" + itemId + ".jpg"));
      $("#i_itemImgDel" + index).attr("class","fa fa-trash fa-2x");
   }
}//-----------------------------------------------------------------------------
function shopProperty_draw()
{
	isImageDelete=[false, false, false, false, false];
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopProperty,
         data: {"requestName":"shopProperty_draw"},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopProperty_del(id)
{
   script_confirm2('',"به صورت کلی از سیستم حذف شود ؟",function()
   {
      try {
         script_loadingShow();
         $.ajax({
            url: url_shopProperty,
            data: {"requestName":"shopProperty_del", "id":id},
            method: "POST",
            success: function(result)
            {
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  shopGroup_draw(spl[1]);
               }
               else
               {
                  alert("انجام نشد");
                  alert(result);
               }
               script_loadingHide();
            },
            error: function() {
               script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
            }
         });
      } catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
      }
   });
}//-----------------------------------------------------------------------------
function shopProperty_new()
{
   try {
        script_loadingShow();
        $.ajax({
            url: url_shopProperty,
            data: {"requestName":"shopProperty_new"},
            method: "POST",
            success: function(result)
            {
               //alert(result);
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  $("#layer_content").html(spl[1]);
               }
               else
               {
                  alert("انجام نشد!.");
               }
               script_loadingHide();
            },
            error: function() {
					alert("خطایی در اتصال رخ داده است");
               script_loadingHide();
            }
        });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopProperty_edit(id)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopProperty,
         data: {"requestName":"shopProperty_edit", "id":id },
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopProperty_update(purpose,id)
{
   //try {
      script_loadingShow();
      var form_data = new FormData();
		form_data.append("id",id);
		form_data.append("title",$("#txt_title").val());
		for(i=1; i <= 5; i++)
		{
			var file_option = $('#file_option' + i + 'Img').prop('files')[0];
			form_data.append('file_option' + i + 'Img', file_option);
			form_data.append("option" + i + "Title",$("#txt_option" + i + "Title").val());
			form_data.append('imgDel' + i,isImageDelete[i]);
		}
		form_data.append("type", document.querySelector("#slct_type").value);
		form_data.append("requestName","shopProperty_update");
		form_data.append("purpose",purpose);

      $.ajax({
         url: url_shopProperty,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
				alert(result);
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               shopProperty_draw();
					script_alert2('','با موفقیت ذخیره گردید',"success");
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }

      });
   //}
   //catch (e)
   //{
		//alert("خطای اسکریپت");
		//script_loadingHide();
   //}
}//-----------------------------------------------------------------------------
var url_shopItems=oPath.manage("shop_bundle/controller/php/shopItems_panel_admin.php");
var isImageDelete=[false,false,false,false,false,false];
oMyTag=[];

function shopItems_priceInfo(pricePlus, siteCommission, marketerCommission)
{
	pricePlus=parseInt(pricePlus);
	siteCommission=parseInt(siteCommission);
	marketerCommission=parseInt(marketerCommission);
	price=parseInt($('#txt_price').val()); 
	discount=parseInt($('#txt_discount').val());
	
	//gheimate kala
	if(price + pricePlus > 0) price=price + pricePlus;
	
	//emale darsade takhfif
	if(discount > 0) price= price - ((discount * price) / 100);
	
   //sahme forooshandeh va sahme site
	priceForSite1= (siteCommission * price) / 100;
	
	console.log(price);
	console.log(siteCommission);
	console.log(marketerCommission);
	
	priceForSaller1= price - priceForSite1;
   
	//sahme forooshandeh va sahme site va bazaryab
	priceForMarketer= (marketerCommission * price) / 100;
	price_= price - priceForMarketer;
   priceForSite2= (siteCommission * price_) / 100;
	priceForSaller2= price_ - priceForSite2;
	
	//commission
	siteCommission1=siteCommission;
	siteCommission2=Math.round(100 / (price / priceForSite2));
	
	//formating
	if(priceForSite1 == 0) priceForSite1='رایگان'; else priceForSite1=(new Intl.NumberFormat().format(priceForSite1)) + ' تومان';
	priceForSaller1=(new Intl.NumberFormat().format(priceForSaller1)) + ' تومان';
	//---
	if(priceForSite2 == 0) priceForSite2='رایگان'; else priceForSite2=(new Intl.NumberFormat().format(priceForSite2)) + ' تومان';	
	priceForSaller2=(new Intl.NumberFormat().format(priceForSaller2)) + ' تومان';
	if(priceForMarketer == 0) priceForMarketer='رایگان'; else priceForMarketer=(new Intl.NumberFormat().format(priceForMarketer)) + ' تومان';	
	
	//START HTML CODE
	html='<h4>فروش مستقیم</h4>';
	html+='<div><b>سهم شما</b> : ' + priceForSaller1 + '</div>';
	html+='<div><b>کارمزد سایت</b> : ' + priceForSite1 + ' (' + siteCommission1 + '%)</div>';
	html+='<hr>';
	html+='<h4>فروش بواسطه بازاریاب</h4>';
	html+='<div><b>سهم شما</b> : ' + priceForSaller2 + '</div>';
	html+='<div><b>کارمزد بازاریاب</b> : ' + priceForMarketer + ' (' + marketerCommission + '%)</div>';
	html+='<div><b>کارمزد سایت</b> : ' + priceForSite2 + ' (' + siteCommission2 + '%)</div>';
	
	$('#lbl_priceInfo').html(html);
}//-----------------------------------------------------------------------------
function shop_toListDraw(searchWord,selectedIds,multiSelect,page) //users list
{
   //try { 
      script_loadingShow();
      $.ajax({
         url: url_users,
         data: {"requestName":"users_listDraw", "searchWord":searchWord, "selectedIds":selectedIds, "multiSelect":multiSelect, "page":page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					var codeCenter=spl[1];
					var searchWord=spl[2];
					dialog=new myModal({
						'showBottomBar':true,
						'topBarStyle': "float:left;font-size:12px;",
						'title': "کشاورزان",
						'centerContent': "" +						
						   "<div style='margin-top:32px;position:relative'>" + 						
							   codeCenter + 
							"</div>"+
							"<div class='input-control input-control-right' style='position:fixed;height: 46px;margin-top: 45px;top:0px;left:5px;right:0px;padding:7px 10px 0px 10px;background-color:rgba(255,255,255,0.9);'>" + 
							   "<div class='input-control input-control-right'>" + 
								   "<span class='fa fa-search input-control-button' onclick='shop_toListDrawSearch();'></span>" +
								   "<input type='text' id='txt_usersSearch' style='background-color:#fff' value='" + searchWord + "' placeholder='جستجو ...'>"+						
							   "</div>"+
							"</div>",
						'bottomBarContent': "<div style='padding-right:5px'><button id='btn_myListOk' class='btn btn-success' onclick='var myListUser=oMyList.getSelected()[0];$(\"#txt_userId\").val(myListUser.dataValue1);$(\"#spn_user\").attr(\"class\",\"lbl lbl-success\");$(\"#spn_user\").html(myListUser.dataValue3 + myListUser.dataValue4 + \" - \" + myListUser.dataValue2);dialog.hide();'>تایید</button></div>",						
					});
					dialog.show();
					oMyList=myList({
						"elementId":"ul_usersList",
						"multiSelect":false,
						"onSelected":function(a,b)
						{
							if(b.getSelected().length > 0)
							{
								document.getElementById("btn_myListOk").disabled=false;
								document.getElementById("btn_myListOk").innerHTML="تایید";
							}
							else
							{
								document.getElementById("btn_myListOk").disabled=true;
							   document.getElementById("btn_myListOk").innerHTML="تایید";
						   }
						}
					});
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   //}
   //catch (e)
   //{
		//alert("خطای اسکریپت");
		//script_loadingHide();
   //}
}//-----------------------------------------------------------------------------
function shop_toListDrawSearch() //to
{
	dialog.hide();
	var searchWord=$("#txt_usersSearch").val();
	shop_toListDraw(searchWord,"","","");
}//-----------------------------------------------------------------------------
function shopItems_imgDel(itemId,index)
{
   if(isImageDelete[index]==false)
   {
		script_confirm2("تصویر حذف شود ؟",'',function(){
			isImageDelete[index]=true;
			$("#img_item" + index).attr("src",oPath.asset("default/images/noImage.gif"));
			$("#i_itemImgDel" + index).attr("class","fa fa-share fa-2x");
		});
   }
   else if(isImageDelete[index]==true)
   {
      isImageDelete[index]=false;
      $("#img_item" + index).attr("src",oPath.manage("shop_bundle/data/images/item" + index + "Thumb_" + itemId + ".jpg"));
      $("#i_itemImgDel" + index).attr("class","fa fa-trash fa-2x");
   }
}//-----------------------------------------------------------------------------
function shopItems_draw(obj,confirm_,searchWord_,page_) //{type, isSpecial, active, trash, page, confirm, searchWord}
{
	isImageDelete=false;
	if(obj=="auto")
	{
		obj={ 
			"backName":-1,
			"userId":-1,
			"type":-1,
			"isSpecial":-1,
			"page":-1,
			"active":-1,
			"trash":-1,
			"confirm":-1,
			"searchWord":-1 	
	   }
	}
	if(confirm_ != undefined) obj.confirm=confirm_;
	if(searchWord_ != undefined) obj.searchWord=searchWord_;
	if(page_ != undefined) obj.page=page_;
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopItems,
         data: {"requestName":"shopItems_draw", "backName":obj.backName, "userId":obj.userId, "type":obj.type, "isSpecial":obj.isSpecial, "page":obj.page, "active":obj.active, "trash":obj.trash, "confirm":obj.confirm, "searchWord":obj.searchWord},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopItems_del(itemId) {
	script_confirm2("حذف شود ؟",'',function()
	{
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_shopItems,
					data: {"requestName": "shopItems_del","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							//{isSpecial,active,page,searchWord}
							shopItems_draw('auto');
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
		}		
	})
}//---------------------------------------------------------------------------------------
function shopItems_trash(itemId) {
	script_confirm2('',"موقتا حذف شود؟",function()
	{
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_shopItems,
					data: {"requestName": "shopItems_trash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							//{isSpecial,active,page,searchWord}
							shopItems_draw('auto');
							script_alert2('',"با موفقیت حذف شد","success");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
		}		
	})
}//---------------------------------------------------------------------------------------
function shopItems_unTrash(itemId) {
	script_confirm2('',"بازیابی شود؟",function()
	{
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_shopItems,
					data: {"requestName": "shopItems_unTrash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							//{isSpecial,active,page,searchWord}
							shopItems_draw('auto');
							script_alert2('',"با موفقیت بازیابی شد","success");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
		}		
	});
}//---------------------------------------------------------------------------------------
function shopItems_active_(itemId) //change active to unActive and reverse
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopItems,
         data: {"requestName":"shopItems_active_","id":itemId},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					$("#td_active_" + spl[1]).html(spl[2]);     	
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopItems_active(itemId,active)
{
   try {
		err=false;
		if(err==false)
		{
			script_loadingShow();
			$.ajax({
				url: url_shopItems,
				data: {"requestName": "shopItems_active","id":itemId,"active":active},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					if(result=="ok")
					{
					   shopItems_draw("auto");
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		}
	}
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function shopItems_edit(itemId) {
	try {
		script_loadingShow();
		isImageDelete=[false,false,false,false,false,false];
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_edit","id":itemId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------
function shopItems_new() {
	try {
		script_loadingShow();
		isImageDelete=[false,false,false,false,false,false];
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_new"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{	
					$("#layer_content").html(spl[1]);				
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() 
			{
				script_loadingHide();
				alert("خطایی در اتصال رخ داده است");
			}
		});
	} 
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function shopItems_update(purpose,itemId,type)
{
   //try {
		err=false;
		if($("#txt_title").val()=="")
		{
			script_alert2('',"نام محصول را وارد نمایید","danger");
			script_focus("txt_title");
			err=true;			
		}
		else if($("#txt_city").val()=="")
		{
			script_alert2('',"شهر مورد نظر را وارد نمایید","danger");
			script_focus("txt_city");
			err=true;			
		}
		else if($("#txt_count").val()=="")
		{
			script_alert2('',"مقدار موجودی انبار را وارد نمایید","danger");
			script_focus("txt_city");
			err=true;			
		}		
		else if($("#txt_price").val()=="")
		{
			script_alert2('',"قیمت محصول را وارد نمایید","danger");
			script_focus("txt_price");
			err=true;			
		}
		//---		
		layer_group=$('#layer_group input[type="checkbox"]');
		groupId='';
      for($i=0;$i < layer_group.length;$i++)
		{
			if(layer_group[$i].checked==true) groupId+= layer_group[$i].value + ",";
		}
		if(!groupId && err==false)
		{
			script_alert2('',"دسته بندی ها را مشخص نمایید","danger");
			script_focus("layer_group");
			err=true;			
		}	

		if(err==false)
		{
			script_loadingShow();
			var formData = new FormData();
			//---
			var content = CKEDITOR.instances.txt_content.getData();
			content.replace(/(?:\\[rn])+/g, "<br>");
			content=oTools.str_tagFitSend_encode(content);
			//---
			tags=oMyTag.get();
			keywords='';
			console.log(tags);
			for($i=0;$i < tags.length;$i++)
			{
				keywords+=tags[$i].title + ',';
			}
			//---
			var file_imgItem0 = $('#file_imgItem0').prop('files')[0];
			var file_imgItem1 = $('#file_imgItem1').prop('files')[0];
			var file_imgItem2 = $('#file_imgItem2').prop('files')[0];
			var file_imgItem3 = $('#file_imgItem3').prop('files')[0];
			var file_imgItem4 = $('#file_imgItem4').prop('files')[0];
			var file_imgItem5 = $('#file_imgItem5').prop('files')[0];
			//---
			formData.append('imgDel0',isImageDelete[0]);
			formData.append('imgDel1',isImageDelete[1]);
			formData.append('imgDel2',isImageDelete[2]);
			formData.append('imgDel3',isImageDelete[3]);
			formData.append('imgDel4',isImageDelete[4]);
			formData.append('imgDel5',isImageDelete[5]);
			formData.append('userId',$("#txt_userId").val());
			formData.append('groupId',groupId);
			formData.append('title',$("#txt_title").val()); 
			formData.append('city',$("#txt_city").val());
			formData.append('count',$("#txt_count").val());
			formData.append('keywords',keywords);
			formData.append('comment',$("#txt_comment").val());
			formData.append('price',$("#txt_price").val()); 
			formData.append('discount',$("#txt_discount").val());
			formData.append('vazn',$("#slct_vazn").val());
			formData.append('type',$("#slct_type").val());
			formData.append('kesht',$("#slct_kesht").val());
			formData.append('kood',$("#slct_kood").val());
			formData.append('atr',$("#slct_atr").val());
			formData.append('tam',$("#slct_tam").val());
			formData.append('rey',$("#slct_rey").val());
			formData.append('saleKesht',$("#slct_saleKesht").val());	
			formData.append('content',content);	
			formData.append('file_imgItem0',file_imgItem0);	
			formData.append('file_imgItem1',file_imgItem1);	
			formData.append('file_imgItem2',file_imgItem2);	
			formData.append('file_imgItem3',file_imgItem3);	
			formData.append('file_imgItem4',file_imgItem4);	
			formData.append('file_imgItem5',file_imgItem5);	
			formData.append('id',itemId);
			formData.append('purpose',purpose);
			formData.append('requestName',"shopItems_update");			
			//---
			$.ajax({
				url: url_shopItems,
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						purpose=spl[1];
						shopItems_draw('auto');

						if(purpose=="new") 
							message="محصول جدید، با موفقیت ذخیره گردید";
						else
							message="محصول با موفقیت ویرایش گردید";
						script_alert2('',message,"success");
					}			
					else if(spl[0]=="!imageType")
					{
						if(spl[1]=="0") var num="اصلی"; else var num=spl[1];
						script_alert2('',"نوع تصویر  " + num + "، غیر مجاز می باشد","danger");
						script_focus("img_item" + spl[1]);
					}
					else if(spl[0]=="!imageSize")
					{
						if(spl[1]=="0") var num="اصلی"; else var num=spl[1];
						script_alert2('',"سایز فایل تصویر " + num + "، بیش از حد مجاز است","danger");
					}
					else
					{
						alert("انجام نشد");
						alert(result);
					}
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		}
	//}
	//catch (e) 
	//{
		//alert("خطای اسکریپت");
		//script_loadingHide();
	//}
}//---------------------------------------------------------------------------------------
function shopItems_viewForConfirm(itemId) //darkhast namayesh mahsool , jahat barrasy
{
	try {
		script_loadingShow();
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_viewForConfirm","id":itemId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
	} 
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function shopItems_setConfirm(itemId,confirmValue)
{
	try {
		script_loadingShow();
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_setConfirm","id":itemId, "confirmValue":confirmValue, "confirmComment":$("#txt_confirmComment").val()},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					confirmValue=spl[1];
					shopItems_draw('auto');
					if(confirmValue=="1") 
						message="محصول تایید شد";
					else if(confirmValue=="2") 
						message="محصول تایید نشد";
					script_alert2('',message,"success");
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
	} 
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function shopItems_confirm(itemId) //change confirm to unConfirm and reverse
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopItems,
         data: {"requestName":"shopItems_confirm","id":itemId},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					$("#td_confirm_" + spl[1]).html(spl[2]);     	
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopItems_likesDraw(itemId,showSearch,page)
{
	searchWord="";
	if(showSearch==undefined) 
		showSearch="";
   else if(showSearch==1 || showSearch=="1" || showSearch==true) 
	{
      searchWord=$('#txt_search').val();
	}
	if(page == undefined || page <=0 ) page=1;
	
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopItems,
         data: {"requestName":"shopItems_likesDraw", "itemId":itemId, "showSearch":showSearch, "searchWord":searchWord, "page":page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopItems_likeDel(itemId,likeId) {
	var ret=confirm("حذف شود ؟.");
	if(ret)
	{
		try {
			if(likeId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=likeId;
			if(id)
			{
				script_loadingShow();
				$.ajax({
					url: url_shopItems,
					data: {"requestName": "shopItems_likeDel","itemId":itemId,"id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						var spl=result.split("[|]");
						if(spl[0]=="ok")
						{
							//{shopId,showSearch,page}
							shopItems_likesDraw(spl[1],spl[2],spl[3]);
						}
						else
						{
							alert("انجام نشد");
							alert(result);
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
	}
}//---------------------------------------------------------------------------------------
function shopItems_opineDraw(itemId,confirm,checkOut,page)
{
	if(itemId=="auto")
	{
		itemId=-1;
		confirm=-1;
		checkOut=-1;
		page=-1;
	}

   try {
      script_loadingShow();
      $.ajax({
         url: url_shopItems,
         data: {"requestName":"shopItems_opineDraw", "itemId":itemId, "confirm":confirm, "checkOut":checkOut, "page":page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopItems_opineDel(opineId) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			if(opineId=="selected") //entekhab chand checkbox
			{
				ids="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) ids+=chkbox.value + ",";
				}
			}
			else
				ids=opineId;
			if(ids!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_shopItems,
					data: {"requestName": "shopItems_opineDel","ids":ids},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							shopItems_opineDraw("auto");
						}
						else
						{
							alert("انجام نشد");
							alert(result);
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
    }
}//---------------------------------------------------------------------------------------
function shopItems_opineConfirm(opineId)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopItems,
         data: {"requestName":"shopItems_opineConfirm","id":opineId},
         method: "POST",
         success: function(result)
         {
            //alert(result);
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#td_active_" + spl[1]).html(spl[2]);
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopItems_opineView(opineId) {
	try {
		script_loadingShow();
		$.ajax({
			url: url_shopItems,
			data: {"requestName": "shopItems_opineView","id":opineId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
					alert(result);
				}
				script_loadingHide();
			},
			error: function() 
			{
				script_loadingHide();
				alert("خطایی در اتصال رخ داده است");
			}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------
function shopItems_opineAnswer(opineId)
{
   try {
		err=false;
		if($("#txt_answer").val()=="")
		{
			script_alert2('','متن پاسخ خالی است','danger');
			document.querySelector("#txt_answer").scrollIntoView({ behavior: 'smooth' });
			document.getElementById("txt_answer").focus();
         err=true;			
		}
		if(err==false)
		{
			script_loadingShow();
			var formData = new FormData();
			formData.append('answerContent',$("#txt_answer").val());
			formData.append('confirm',$("#slct_confirm").val());
			formData.append('id',opineId);
			formData.append('requestName',"shopItems_opineAnswer");			

			$.ajax({
				url: url_shopItems,
				dataType: 'text',  // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
               if(result=="ok")
					{
						script_alert2('','با موفقیت انجام شد','success');
					   shopItems_opineDraw("auto");
					}
					else
					{
						alert("انجام نشد");
						alert(result);
					}
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		}
	}
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------



var url_shop=oPath.manage("shop_bundle/controller/php/shop_ajax.php");
isItemDelImg=false;
function shopItems_opineSend(shopItemId)
{
	try {
		err=false;
		$("#spn_alert").html("");
		if($("#txt_comment").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا متن پیغام خود را وارد نمایید");
         err=true;			
		}
      else if(err==false)
		{
			$('#btn_opineSend').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
				url: url_shop,
				data: {"requestName": "shopItems_opineSend","shopItemId":shopItemId,"comment":$("#txt_comment").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
               if(result=="!login")
						script_alertStatus("spn_alert","danger","برای درج نظر، لطفا وارد شوید و یا ثبت نام نمایید");			
               else if (result=="ok")
						script_alertStatus("spn_alert","success","نظر شما با موفقیت ارسال شد و پس از تایید، منتشر خواهد شد");					
               else
                  alert("انجام نشد");
					$('#btn_opineSend').html('ارسال');
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					$('#btn_opineSend').html('ارسال');
				}
			});
		}
	}
	catch (e) 
	{
	   alert("خطای اسکریپت");
	   $('#btn_opineSend').html('ارسال');
	}
}//---------------------------------------------------------------------------------------

function shopItems_attachDownload(shopItemId,attachType)// attachType : file1, file2, urll1, url2
{
	try {
		$('#spn_' + attachType).html('<i class="fa fa-circle-o-notch fa-spin"></i>');
		$.ajax({
			url: url_shop,
			data: {"requestName": "shopItems_attachDownload","shopItemId":shopItemId,"attachType":attachType},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				alert(result);
				var spl=result.split('[|]');
				if(spl[0]=="ok") oTools.link(spl[1],true);
				$('#spn_' + spl[2]).html('<i class="fa fa-download"></i>');
			},
			error: function() {
				alert("خطایی در اتصال رخ داده است");					
				script_loadingHide();
			}
		});
	} catch (e) {
	   alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function shopItems_like(shopItemId)
{
	try {
		$('#spn_like_' + shopItemId).html('<i class="fa fa-circle-o-notch fa-spin"></i>');
		$.ajax({
			url: url_shop,
			data: {"requestName": "shopItems_like","shopItemId":shopItemId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="!login")
				{
					alert("لطفا ابتدا وارد شوید و یا ثبت نام نمایید");	
					$('#spn_like_' + spl[1]).html('<i id="i_like" class="fa fa-heart-o"></i>');
				}					
				else if (spl[0]=="ok")
				{
					if(spl[2]==1)
						$('#spn_like_' + spl[1]).html('<i id="i_like" class="fa fa-heart"></i>');
					else
						$('#spn_like_' + spl[1]).html('<i id="i_like" class="fa fa-heart-o"></i>');			
				   $("#spn_likeCount_" + spl[1]).html("(" + spl[3] + ")");
				}
				else
				{
					alert("انجام نشد");
				}
			},
			error: function() {
				alert("خطایی در اتصال رخ داده است");					
			}
		});
	} 
	catch (e) 
	{
	   alert("خطای اسکریپت");
	}
}//---------------------------------------------------------------------------------------
function shopItems_pay(itemId)
{
	try {
		err=false;
		$("#spn_alert").html("");
		if(!oTools.isMail($("#txt_userMail").val()))
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل خود را به صورت صحیح وارد نمایید")
         err=true;		
		}
		else if($("#txt_userMobile").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا شماره موبایل خود را وارد نمایید")
         err=true;							
		}
      else if(err==false)
		{
			$('#btn_pay').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
				url: url_shop,
				data: {"requestName": "shopItems_pay", "itemId": itemId, "userMail":$("#txt_userMail").val(), "userMobil":$("#txt_userMobile").val(), "coupon":$("#txt_coupon").val(), "payTypeId":$("#slct_payType").val(), "userMarketerId":$("#txt_userMarketerId").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
               if(spl[0]=="ok")
               {
						script_alertStatus("spn_alert","success","مبلغ قابل پرداخت : " + spl[1] + " تومان" + "، در حال انتقال به درگاه پرداخت");
						setTimeout(function(){
							oTools.link(spl[2]);
						}, 5000);
               }
               else if (spl[0]=="!coupon")
					{
						script_alertStatus("spn_alert","danger","کوپن وارد شده، نامعتبر است");
						$('#btn_pay').html('خرید و پرداخت آنلاین');						
               }
               else if (spl[0]=="err")
					{
						script_alertStatus("spn_alert","danger","اتصال به درگاه پرداخت، امکان پذیر نیست. لطفا دوباره امتحان کنید");					
						$('#btn_pay').html('خرید و پرداخت آنلاین');
					}
					else
               {
                  alert("انجام نشد");
               }
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					$('#btn_pay').html('خرید و پرداخت آنلاین');
				}

			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
		$('#btn_pay').html('خرید و پرداخت آنلاین');
	}
}//---------------------------------------------------------------------------------------
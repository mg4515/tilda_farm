var url_shopItemsOther=oPath.manage("shop_bundle/controller/php/shopItemsOther_panel_admin.php");
function shopItemsOther_edit()
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopItemsOther,
         data: {"requestName":"shopItemsOther_edit"},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } 
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopItemsOther_update()
{
   try {
      script_loadingShow();
		count=$("#txt_count").val();
		items='';
		for(i=0;i < count;i++)
		{
			id=$("#txt_id" + i).val();
			title=$("#txt_title" + i).val();
			active=document.querySelector("#chk_active" + i).checked; 
			items+=id + '#' + title + '#' + active + ',';
		}
      var form_data = new FormData();
		form_data.append("items",items);
		form_data.append("requestName","shopItemsOther_update");

      $.ajax({
         url: url_shopItemsOther,
         dataType: 'text',
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            if(result=="ok")
            {
               shopItemsOther_edit();
					script_alert2('',"با موفقیت ذخیره گردید","success");
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
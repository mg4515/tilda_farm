var url_shopPublication=oPath.manage("shop_bundle/controller/php/shopPublication_panel_admin.php");
function shopPublication_draw(parentId)  //darkhast namayesh list nashryeha
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopPublication,
         data: {"requestName":"shopPublication_draw"},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopPublication_del(id) //darkhast hazf
{
   script_confirm2('',"به صورت کلی از سیستم حذف شود ؟",function()
   {
      try {
         script_loadingShow();
         $.ajax({
            url: url_shopPublication,
            data: {"requestName":"shopPublication_del", "id":id},
            method: "POST",
            success: function(result)
            {
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  shopPublication_draw();
               }
               else
               {
                  alert("انجام نشد");
               }
               script_loadingHide();
            },
            error: function() {
               script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
            }
         });
      } catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
      }
   });
}//-----------------------------------------------------------------------------
function shopPublication_new() //darkhast namayesh form jadid
{
   try {
        script_loadingShow();
        $.ajax({
            url: url_shopPublication,
            data: {"requestName":"shopPublication_new"},
            method: "POST",
            success: function(result)
            {
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  $("#layer_content").html(spl[1]);
               }
               else
               {
                  alert("انجام نشد!.");
                  alert(result);
               }
               script_loadingHide();
            },
            error: function() {
					alert("خطایی در اتصال رخ داده است");
               script_loadingHide();
            }
        });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopPublication_edit(id)  //darkhast namayesh form virayesh
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopPublication,
         data: {"requestName":"shopPublication_edit", "id":id },
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopPublication_update(purpose,id) //darkhast zakhire form virayesh ya jaddid
{
   try {
      script_loadingShow();
      var form_data = new FormData();
		form_data.append("id",id);
		form_data.append("title",$("#txt_title").val());
		form_data.append("requestName","shopPublication_update");
		form_data.append("purpose",purpose);

      $.ajax({
         url: url_shopPublication,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               shopPublication_draw(spl[1]);
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }

      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
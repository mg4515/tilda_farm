var url_shopCoupon=oPath.manage("shop_bundle/controller/php/shopCoupon_panel_admin.php");
var isImageDelete=false;
var oMyModal=[];

function shopCoupon_draw(searchWord,page)
{
   try {
      script_loadingShow();
		if(searchWord=='auto')
		{
			searchWord= -1;
			page= -1;
		}
      $.ajax({
         url: url_shopCoupon,
         data: {"requestName":"shopCoupon_draw", "searchWord":searchWord, "page":page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopCoupon_del(itemId)
{
   script_confirm2('',"به صورت کلی از سیستم حذف شود ؟",function()
   {
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_shopCoupon,
					data: {"requestName":"shopCoupon_del", "id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						var spl=result.split("[|]");
						if(spl[0]=="ok")
						{
							shopCoupon_draw("auto");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
      } catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
      }
   });
}//-----------------------------------------------------------------------------
function shopCoupon_new()
{
   try {
        script_loadingShow();
        $.ajax({
            url: url_shopCoupon,
            data: {"requestName":"shopCoupon_new"},
            method: "POST",
            success: function(result)
            {
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  $("#layer_content").html(spl[1]);
               }
               else
               {
                  alert("انجام نشد!.");
               }
               script_loadingHide();
            },
            error: function() {
					alert("خطایی در اتصال رخ داده است");
               script_loadingHide();
            }
        });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopCoupon_edit(id)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_shopCoupon,
         data: {"requestName":"shopCoupon_edit", "id":id },
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function shopCoupon_update(purpose,id)
{
   try {
		err=false;		
		if($("#txt_discount").val()=="")
		{
			script_alert2('',"تکمیل گزینه های ستاره دار الزامی است","danger");
			script_focus("txt_discount");
			err=true;			
		}
		else if($("#txt_maxDiscount").val()=="")
		{
			script_alert2('',"تکمیل گزینه های ستاره دار الزامی است","danger");
			script_focus("txt_maxDiscount");
			err=true;			
		}
		else if($("#txt_startDate").val()=="")
		{
			script_alert2('',"تکمیل گزینه های ستاره دار الزامی است","danger");
			script_focus("txt_startDate");
			err=true;			
		}	
		else if($("#txt_endDate").val()=="")
		{
			script_alert2('',"تکمیل گزینه های ستاره دار الزامی است","danger");
			script_focus("txt_endDate");
			err=true;			
		}			
		if(err==false)
		{
			script_loadingShow();
			
			var form_data = new FormData();
			form_data.append("id",id);
			form_data.append("title",$("#txt_title").val());
			form_data.append("discount",$("#txt_discount").val());
			form_data.append("maxDiscount",$("#txt_maxDiscount").val());
			form_data.append("startDate",$("#txt_startDate").val());
			form_data.append("endDate",$("#txt_endDate").val());
			form_data.append("comment",$("#txt_comment").val());
			form_data.append("requestName","shopCoupon_update");
			form_data.append("purpose",purpose);

			$.ajax({
				url: url_shopCoupon,
				dataType: 'text',  // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="!exists")
					{
						title=spl[1];
						script_alert2('',"کوپن تخفیف با عنوان " + title + "، قبلا ایجاد شده است","danger");
						script_focus("txt_title");
					}					
					else if(spl[0]=="ok")
					{
						purpose=spl[1];
						shopCoupon_draw('auto');

						if(purpose=="new") 
							message="کوپن تخفیف  جدید، با موفقیت ذخیره گردید";
						else
							message="کوپن تخفیف با موفقیت ویرایش گردید";
						script_alert2('',message,"success");						
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}

			});
		}
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
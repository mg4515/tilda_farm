var url_sitePro=oPath.manage("site_bundle/controller/php/sitePro_panel.php");
function sitePro_update() {
	try {
		script_loadingShow();

		var file_icon = $('#file_icon').prop('files')[0];
		var file_logo = $('#file_logo').prop('files')[0];
		var file_logo2 = $('#file_logo2').prop('files')[0];
		var form_data = new FormData();
		form_data.append('file_icon', file_icon);
		form_data.append('file_logo', file_logo);
		form_data.append('file_logo2', file_logo2);
		form_data.append("requestName","sitePro_update");
		form_data.append("webSiteAddress",$("#txt_webSiteAddress").val());
		form_data.append("title",$("#txt_title").val());
		form_data.append('comment',$("#txt_comment").val());
		form_data.append('keywords',$("#txt_keywords").val());
		form_data.append('copyRight',$("#txt_copyRight").val());
		  
		form_data.append('addressCo',$("#txt_addressCo").val());
		form_data.append('tell1',$("#txt_tell1").val());
		form_data.append('tell2',$("#txt_tell2").val());
		form_data.append('tell3',$("#txt_tell3").val());
		form_data.append('mailAdmin',$("#txt_mailAdmin").val());
		form_data.append('mailAds',$("#txt_mailAds").val());
		form_data.append('mailInfo',$("#txt_mailInfo").val());
		form_data.append('mailNoreplay',$("#txt_mailNoreplay").val());
		  
		form_data.append('facebook',$("#txt_facebook").val());
		form_data.append('googlePlus',$("#txt_googlePlus").val());
		form_data.append('twitter',$("#txt_twitter").val());
		form_data.append('instagram',$("#txt_instagram").val());
		form_data.append('telegram',$("#txt_telegram").val());

	   $.ajax({
			url: url_sitePro,
			dataType: 'text',  // what to expect back from the PHP script, if anything
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				if(result=="ok")
				{
					sitePro_edit();
					script_alert2('','با موفقیت انجام شد','success');
				}
				else
					alert(result);
				script_loadingHide();
			},
			error: function() {
				 script_loadingHide();
				 alert("خطایی در اتصال رخ داده است");
			}

	   });
	} catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function sitePro_edit() {
    try {
        script_loadingShow();
        $.ajax({
            url: url_sitePro,
            data: {"requestName":"sitePro_edit"},
            method: "POST",
            success: function(result)
            {
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  $("#layer_content").html(spl[1]);
               }
               script_loadingHide();
            },
            error: function() {
               script_loadingHide();
               alert("خطایی در اتصال رخ داده است");
            }

        });
    } catch (e) {
         alert("خطای اسکریپت");
			script_loadingHide();
    }
}//---------------------------------------------------------------------------------------

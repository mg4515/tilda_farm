<?php
   include_once $oPath->manageDir("site_bundle/model/sitePro_model.php");
	class cSiteProDraw
	{
		function keywords_draw1($limitStr="",$keywords="") 
		{
			global $oPath;
			$oSitePro=new cSitePro();
			if($keywords=="") $keywords=$oSitePro->getKeywords();
			$items=explode("|",$keywords);
			$countItems=count($items);
			$code="";
			for($i=0;$i < $countItems;$i++)
			{
				$code= $code . "<li><a href='home' >{$items[$i]}</a></li>";		
			}
			return $code;
		}//------------------------------------------------------------------------------------
		function HeadMetaTag($properties="auto",$plusTitle='')
		{
			global $oPath;
			$oSitePro=new cSitePro();
			if($properties=="auto")
			{
				$title=$oSitePro->getTitle();
				$description=$oSitePro->getComment();
				$keywords=$oSitePro->getKeywords();
				$addingAddress='';
			}
			else
			{
				$title=@$properties["title"] . ' - ' . $oSitePro->getTitle();
				$description=@$properties["description"];
				$keywords=@$properties["keywords"];
				$addingAddress=@$properties["addingAddress"];
			}
			if($plusTitle) $title=$plusTitle . ' - ' . $title;
			
			//metaTag and title
			$head="
			<meta charset=\"utf-8\">
			<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
			<meta name=\"copyright\" content=\"\">
			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">			
			<title>{$title}</title>
			<meta name=\"keywords\" content=\"{$keywords}\" />
			<meta name=\"description\" content=\"{$description}\" />
			<meta name=\"robots\" content=\"noodp,noydir\"/>
			<link rel=\"icon\" type=\"image/png\" href=\"{$oSitePro->getIcon()}\">
			";			
			
			//Open Graph
			$pageOg="
			<meta property=\"og:locale\" content=\"fa_IR\" />
			<meta property=\"og:type\" content=\"article\" />
			<meta property=\"og:title\" content=\"{$title}\" />
			<meta property=\"og:description\" content=\"{$description}\" />
			<meta property=\"og:url\" content=\"{$oPath->root()}/{$addingAddress}\" />
			<meta property=\"og:site_name\" content=\"{$title}\" />";
			
			//canonical
			$canonical="<link rel='canonical' href='{$oPath->root()}/{$addingAddress}' />\n";
			
			return  $head . $pageOg . $canonical;
		}//------------------------------------------------------------------------------------
		public function copyRight($cssClass='')
		{
			global $oPath;
			$oSitePro=new cSitePro();
			return "<a href='{$oPath->root()}' target='_blank' class='{$cssClass}'>{$oSitePro->getCopyRight()}</a>";
		}//------------------------------------------------------------------------------------
		public function logo($width='', $height='')
		{
			global $oPath;
			$oSitePro=new cSitePro();
			if($width) $width="width=\"{$width}\""; else $width='';
			if($height) $height="height=\"{$height}\""; else $height='';
			return "<img src='{$oSitePro->getLogo()}' {$width} {$height}>";
		}//------------------------------------------------------------------------------------
		public function logo2($width='', $height='')
		{
			global $oPath;
			$oSitePro=new cSitePro();
			if($width) $width="width=\"{$width}\""; else $width='';
			if($height) $height="height=\"{$height}\""; else $height='';
			return "<img src='{$oSitePro->getLogo2()}' {$width} {$height}>";
		}//------------------------------------------------------------------------------------		
		public function mobile($isLink=true)
		{
			global $oPath;
			$oSitePro=new cSitePro();
			if($isLink==true)
			   return "<a href='tel:{$oSitePro->getTell1()}' target='_blank'>{$oSitePro->getTell1()}</a>";
			else
			   return $oSitePro->getTell1();
		}//------------------------------------------------------------------------------------	
		public function mail($isLink=true)
		{
			global $oPath;
			$oSitePro=new cSitePro();
			if($isLink==true)
			   return "<a href='mailto:{$oSitePro->getMailInfo()}' target='_blank'>{$oSitePro->getMailInfo()}</a>";
			else   
				return $oSitePro->getMailInfo();
		}//------------------------------------------------------------------------------------	
		public function addressCo()
		{
			global $oPath;
			$oSitePro=new cSitePro();
			return $oSitePro->getAddressCo();
		}//------------------------------------------------------------------------------------
		public function instagram ()
		{
			global $oPath;
			$oSitePro=new cSitePro();	
         return $oSitePro->getInstagram();	
		}//------------------------------------------------------------------------------------
		public function telegram ()
		{
			global $oPath;
			$oSitePro=new cSitePro();	
         return $oSitePro->getTelegram();	
		}//------------------------------------------------------------------------------------		
      public function socialDraw_forFooter()
		{
			global $oPath;
			$oSitePro=new cSitePro();	
			$code='';
         if($oSitePro->getInstagram()) $code.="<a href=\"{$oSitePro->getInstagram()}\" class='d-block float-right text-decoration-none ml-3'><i class=\"icon-instagram c-regular fs-icon\"></i></a>";
         if($oSitePro->getTelegram()) $code.="<a href=\"{$oSitePro->getTelegram()}\" class='d-block float-right text-decoration-none ml-3'><i class=\"icon-telegram c-regular fs-icon\"></i></a>";
         if($oSitePro->getTwitter()) $code.="<a href=\"{$oSitePro->getTwitter()}\" class='d-block float-right text-decoration-none ml-3'><i class=\"icon-twitter c-regular fs-icon\"></i></a>";
         if($oSitePro->getFacebook()) $code.="<a href=\"{$oSitePro->getFacebook()}\" class='d-block float-right text-decoration-none ml-3'><i class=\"icon-facebook c-regular fs-icon\"></i></a>";
			return $code;
		}//------------------------------------------------------------------------------------
		public function title($length=0)
		{
			global $oPath;
			$oSitePro=new cSitePro();
			if($length > 0) 
				return mb_substr($oSitePro->getTitle(),0,$length,'utf8');
			else
				return $oSitePro->getTitle(false);
		}//------------------------------------------------------------------------------------			
		public function description($length=0)
		{
			global $oPath;
			$oSitePro=new cSitePro();
			if($length > 0) 
				return mb_substr($oSitePro->getComment(false),0,$length,'utf8');
			else
				return $oSitePro->getComment(false);
		}//------------------------------------------------------------------------------------		
	}
?>
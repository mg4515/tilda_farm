<?php
   error_reporting(E_ALL);
   session_start();

	//engine.php
	include_once $_SESSION["engineRequire"];
	require_once $oPath->manageDir('site_bundle/model/siteMailServer_model.php');

   $oSiteMailServer=new cSiteMailServer();
	
	//request check
	if(isset($_REQUEST["data"])) 
	{
		$data=$_REQUEST["data"];
		if(!$oTools->misc_isJson($data))
		{
			$oEngine->response("error[|]The json format is invalid"); //ettelaate daryafty json namybashad
			exit;			
		}
      else
      {
         $data=json_decode($_REQUEST["data"],true);
      }
	}
	else
	{
		$oEngine->response("error[|]The data could not be found");
		exit;
	}
   if(isset($data["requestName"])) $request=$data["requestName"]; else $request=""; //request name
	
	
	//start request process
   if($request=="siteMailServer_draw")
   {
      $item=$oSiteMailServer->get(1);
      if($item->active=="1") $checked=" checked='checked'"; else $checked="";
      $code= "
         <table class='tbl tbl-right form'>
            <tr>
               <td style='width:100px;'><input type='checkbox' id='chk_active'{$checked}>&nbsp;فعال</td>
               <td></td>
            </tr>
            <tr>
               <td>آدرس سرور ارسال کننده : </td>
               <td><input type='text' id='txt_server' value='{$item->server}' style='direction:ltr'></td>
            </tr>
            <tr>
               <td>شماره پرت : </td>
               <td><input type='text' id='txt_port' value='{$item->port}' style='direction:ltr'></td>
            </tr>
            <tr>
               <td>نام کاربری : </td>
               <td><input type='text' id='txt_userName' value='{$item->userName}' style='direction:ltr'></td>
            </tr>
            <tr>
               <td>رمز عبور : </td>
               <td><input type='password' id='txt_password' value='{$item->password}' style='direction:ltr'></td>
            </tr>
            <tr>
               <td>آدرس ایمیل : </td>
               <td><input type='text' id='txt_from' value='{$item->from}' style='direction:ltr'></td>
            </tr>
         </table>
         <hr>
         <button class='btn btn-success' onclick='siteMailServer_update();'>ذخيره</button>
      ";
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($request=="siteMailServer_update")
   {
		$array=array(
      "server"=>htmlspecialchars($oDb->escape($data["server"],false)),
      "port"=>htmlspecialchars($oDb->escape($data["port"],false)),
      "userName"=>htmlspecialchars($oDb->escape($data["userName"],false)),
      "password"=>htmlspecialchars($oDb->escape($data["password"],false)),
      "from"=>htmlspecialchars($oDb->escape($data["from"],false)),
      "active"=>$oTools->misc_jsBln2PhpBln(htmlspecialchars($oDb->escape($data["active"])),true)
		);
      $oSiteMailServer->update($array);
      $oEngine->response("ok");
   }//--------------------------------------------------------------------------

?>
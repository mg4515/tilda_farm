<?php
   @session_start();
	
	//include
	include_once $_SESSION["engineRequire"];//engine.php
	require_once $oPath->manageDir('site_bundle/model/sitePro_model.php');

   //object
   $oSitePro=new cSitePro();
	
	//request
	$requestName=@$_REQUEST["requestName"];

   if($requestName=="sitePro_edit")
   {
      $item=$oSitePro->get(); 
		
      if(!file_exists($oPath->manageDir("site_bundle/data"))) mkdir($oPath->manageDir("site_bundle/data"));
      
		if(file_exists($oPath->manageDir("site_bundle/data/logo.png")))
         $logo=$oPath->manage("site_bundle/data/logo.png");  
      else
			$logo=$oPath->asset("admin/images/logo.png");
      
      if(file_exists($oPath->manageDir("site_bundle/data/logo2.png")))
         $logo2=$oPath->manage("site_bundle/data/logo2.png");  
      else
			$logo2=$oPath->asset("admin/images/logo.png");
		
		if(file_exists($oPath->manageDir("site_bundle/data/icon.png")))
         $icon=$oPath->manage("site_bundle/data/icon.png"); 
      else
         $icon=$oPath->asset("admin/images/icon.png");
      $code= "
		   <div class='vSpace-4x'></div>
         <h1><i class='fa fa-pencil-square-o'></i>&nbsp;ویرایش خصوصیات سایت</h1>
			<div class='vSpace-4x'></div>
			
         <div class='panel'>
            <div class='panel-body form'>
               <label>آدرس وب سایت</label>
               <input type='text' id='txt_webSiteAddress' value='{$item->webSiteAddress}' class='dir-ltr'>

               <label>عنوان سایت</label>
               <input type='text' id='txt_title' value='{$item->title}'>

               <label>توضیحات سایت</label>
               <textarea id='txt_comment' >{$item->comment}</textarea>

               <label>کلمات کلیدی سایت</label>
               <textarea id='txt_keywords' >{$item->keywords}</textarea>

               <label>متن کپی رایت</label>
               <input type='text' id='txt_copyRight' value='{$item->copyright}'>
			
				   <hr>
				
               <label>شمایل سایت</label>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_icon' onchange='oTools.previewImgFile(\"file_icon\",\"img_icon\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب فایل</span>
					</label>
					<img src='{$icon}' width='32px' id='img_icon'/>

               <label>آرم سایت : </label>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_logo' onchange='oTools.previewImgFile(\"file_logo\",\"img_logo\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب فایل</span>
					</label>
					<img src='{$logo}' width='32px' id='img_logo'/>
						
					&nbsp;|&nbsp;
						
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='file_logo2' onchange='oTools.previewImgFile(\"file_logo2\",\"img_logo2\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب فایل</span>
					</label>
					<img src='{$logo2}' width='32px' id='img_logo2'/>						
		
               <hr>

               <label>آدرس شرکت</label>
               <textarea id='txt_addressCo' >{$item->addressCo}</textarea>

               <label>تلفن تماس 1</label>
               <input type='text' id='txt_tell1' value='{$item->tell1}' class='dir-ltr'>

               <label>تلفن تماس 2</label>
               <input type='text' id='txt_tell2' value='{$item->tell2}' class='dir-ltr'>

               <label>تلفن تماس 3</label>
               <input type='text' id='txt_tell3' value='{$item->tell3}' class='dir-ltr'>

					<hr>

               <label>ایمیل مدیریت</label>
               <input type='text' id='txt_mailAdmin' value='{$item->mailAdmin}' class='dir-ltr' >

               <label>ایمیل تبلیغات</label>
               <input type='text' id='txt_mailAds' value='$item->mailAds' class='dir-ltr'>

               <label>ایمیل اطلاع رسانی</label>
               <input type='text' id='txt_mailInfo' value='$item->mailInfo' class='dir-ltr'>

               <label>ایمیل ارسال های سیستم</label>
               <input type='text' id='txt_mailNoreplay' value='$item->mailNoreplay' class='dir-ltr'>

					<hr>

               <label>آدرس صفحه در فیسبوک</label>
               <textarea id='txt_facebook' class='form-control text-left dir-ltr'>$item->facebook</textarea>

               <label>آدرس صفحه در گوگل پلاس</label>
               <textarea id='txt_googlePlus' class='form-control text-left dir-ltr'>$item->googlePlus</textarea>

               <label>آدرس صفحه در تویتر : </label>
               <textarea id='txt_twitter' class='form-control text-left dir-ltr'>$item->twitter</textarea>

               <label>آدرس صفحه در اینستگرام</label>
               <textarea id='txt_instagram' class='form-control text-left dir-ltr'>$item->instagram</textarea>

               <label>آدرس کانال تلگرام</label>
               <textarea id='txt_telegram' class='form-control text-left dir-ltr'>$item->telegram</textarea>			
					
					<hr>
					
					<button class='btn btn-success' onclick='sitePro_update();'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>				
				</div>
			</div>
			<div class='vSpace-4x'></div>
      ";
		
      $oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="sitePro_update")
   {
      $array=[];
      if(count($_FILES) > 0)
      {
         if(isset($_FILES['file_icon']))
         {
            $extension = pathinfo($_FILES['file_icon']['name'], PATHINFO_EXTENSION);
            $fileType=explode(".",$extension);
            $fileType=$fileType[count($fileType)-1];
            if($fileType=="jpg" || $fileType=="gif" || $fileType=="png")
            {
               copy($_FILES['file_icon']['tmp_name'],$oPath->manageDir("site_bundle/data/icon.png"));
               unlink($_FILES['file_icon']['tmp_name']);
            }
         }
         if(isset($_FILES['file_logo']))
         {
            $extension = pathinfo($_FILES['file_logo']['name'], PATHINFO_EXTENSION);
            $fileType=explode(".",$extension);
            $fileType=$fileType[count($fileType)-1];
            if($fileType=="jpg" || $fileType=="gif" || $fileType=="png")
            {
               copy($_FILES['file_logo']['tmp_name'],$oPath->manageDir("site_bundle/data/logo.png"));
               unlink($_FILES['file_logo']['tmp_name']);
            }
         }
         if(isset($_FILES['file_logo2']))
         {
            $extension = pathinfo($_FILES['file_logo2']['name'], PATHINFO_EXTENSION);
            $fileType=explode(".",$extension);
            $fileType=$fileType[count($fileType)-1];
            if($fileType=="jpg" || $fileType=="gif" || $fileType=="png")
            {
               copy($_FILES['file_logo2']['tmp_name'],$oPath->manageDir("site_bundle/data/logo2.png"));
               unlink($_FILES['file_logo2']['tmp_name']);
            }
         }
      }
      $array['webSiteAddress']=$oDb->escape($_REQUEST["webSiteAddress"],false);
      $array['title']=$oDb->escape($_REQUEST["title"],false);
      $array['comment']=$oDb->escape($_REQUEST["comment"],false);
      $array['keywords']=$oDb->escape($_REQUEST["keywords"],false);
      $array['copyRight']=$oDb->escape($_REQUEST["copyRight"],false);
      $array['copyRightLink']=$array['webSiteAddress'];		
      //-----
      $array['addressCo']=$oDb->escape($_REQUEST["addressCo"],false);
      $array['tell1']=$oDb->escape($_REQUEST["tell1"],false);
      $array['tell2']=$oDb->escape($_REQUEST["tell2"],false);
      $array['tell3']=$oDb->escape($_REQUEST["tell3"],false);
      //------
      $array['mailAdmin']=$oDb->escape($_REQUEST["mailAdmin"],false);
      $array['mailAds']=$oDb->escape($_REQUEST["mailAds"],false);
      $array['mailInfo']=$oDb->escape($_REQUEST["mailInfo"],false);
      $array['mailNoreplay']=$oDb->escape($_REQUEST["mailNoreplay"],false);
      //------
      $array['facebook']=$oDb->escape($_REQUEST["facebook"],false);
      $array['googlePlus']=$oDb->escape($_REQUEST["googlePlus"],false);
      $array['twitter']=$oDb->escape($_REQUEST["twitter"],false);
      $array['instagram']=$oDb->escape($_REQUEST["instagram"],false);
      $array['telegram']=$oDb->escape($_REQUEST["telegram"],false);

      $oSitePro->update($array);
      $oEngine->response("ok");
   }//--------------------------------------------------------------------------

?>
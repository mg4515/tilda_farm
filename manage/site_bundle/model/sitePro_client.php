<?php
class sitePro
{
   private $retSitePro;
   public $webSiteAddress="http://www.behnovin.ir";
   private $title;
   private $keywords;
   private $comment;
   private $logo;
   private $icon;
   private $copyright;
   private $copyrightLink;
   //----------
   private $addressCo;
   private $tell1;
   private $tell2;
   private $tell3;
   //----------
   private $facebook;
   private $googlePlus;
   private $twitter;
   private $instagram;

   function sitePro($startPath="")
   {
      $db=new DbMng();
      $ret=$db->table("sitePro")->fields("*")->select()[0];
      $this->title=$ret->title;
      $this->keywords=$ret->keywords;
      $this->comment=$ret->comment;
      $this->copyright=$ret->copyright;
      $this->copyrightLink=$ret->copyrightLink;
      //--------
      $this->addressCo=$ret->addressCo;
      $this->tell1=$ret->tell1;
      $this->tell2=$ret->tell2;
      $this->tell3=$ret->tell3;
      //---------
      $this->facebook=$ret->facebook;
      $this->googlePlus=$ret->googlePlus;
      $this->twitter=$ret->twitter;
      $this->instagram=$ret->instagram;

      if(file_exists($startPath . "Manage/siteBundle/data/logo.png"))
         $this->logo=$startPath ."Manage/siteBundle/data/logo.png";
      else
         $this->logo=$startPath ."Asset/img/noImage_larg.gif";

      if(file_exists($startPath . "Manage/siteBundle/data/icon.png"))
         $icon=$startPath . "Manage/siteBundle/data/icon.png";
      else
         $icon=$startPath . "Asset/img/noImage_larg.gif";
   }//--------------------------------------------------------------------------
   function reset($startPath="")
   {
      $db=new DbMng();
      $ret=$db->table("sitePro")->fields("*")->select()[0];
      $this->title=$ret->title;
      $this->keyWords=$ret->keywords;
      $this->comment=$ret->comment;

      if(file_exists($startPath . "Manage/siteBundle/data/logo.png"))
         $this->logo=$startPath ."Manage/siteBundle/data/logo.png";
      else
         $this->logo=$startPath ."Asset/img/noImage_larg.gif";

      if(file_exists($startPath . "Manage/siteBundle/data/icon.png"))
         $icon=$startPath . "Manage/siteBundle/data/icon.png";
      else
         $icon=$startPath . "Asset/img/noImage_larg.gif";
   }//--------------------------------------------------------------------------
   function getTitle($standard=true)
   {
      if($standard==true)
         return mb_substr($this->title,0,69,"utf8");
      else
         return $this->title;
   }//--------------------------------------------------------------------------
   function setTitle($value)
   {
      $this->title=$value;
   }//--------------------------------------------------------------------------
   function getComment($standard=true)
   {
      if($standard==true)
         return mb_substr($this->comment,0,154,"utf8");
      else
         return $this->comment;
   }//--------------------------------------------------------------------------
   function setComment($value)
   {
      $this->comment=$value;
   }//--------------------------------------------------------------------------
   function getKeywords($standard=true)
   {
      if($standard==true)
         return mb_substr($this->keywords,0,69,"utf8");
      else
         return $this->keywords;
   }//--------------------------------------------------------------------------
   function setKeywords($value)
   {
      $this->keywords=$value;
   }//--------------------------------------------------------------------------
   function getCopyright()
   {
      return $this->copyright;
   }//--------------------------------------------------------------------------
   function setCopyright($value)
   {
      $this->copyright=$value;
   }//--------------------------------------------------------------------------	
   function getCopyrightLink()
   {
      return $this->copyrightLink;
   }//--------------------------------------------------------------------------
   function setCopyrightLink($value)
   {
      $this->copyrightLink=$value;
   }//--------------------------------------------------------------------------		
   function getIcon()
   {
      return $this->icon;
   }//--------------------------------------------------------------------------
   function setIcon($value)
   {
      $this->icon=$value;
   }//--------------------------------------------------------------------------
   function getLogo()
   {
      return $this->logo;
   }//--------------------------------------------------------------------------
   function setLogo($value)
   {
      $this->logo=$value;
   }//--------------------------------------------------------------------------
   function getAddressCo()
   {
      return $this->addressCo;
   }//--------------------------------------------------------------------------
   function setAddressCo($value)
   {
      $this->AddressCo=$value;
   }//--------------------------------------------------------------------------
   function getTell1()
   {
      return $this->tell1;
   }//--------------------------------------------------------------------------
   function setTell1($value)
   {
      $this->tell1=$value;
   }//--------------------------------------------------------------------------
   function getTell2()
   {
      return $this->tell2;
   }//--------------------------------------------------------------------------
   function setTell2($value)
   {
      $this->tell2=$value;
   }//--------------------------------------------------------------------------
   function getTell3()
   {
      return $this->tell3;
   }//--------------------------------------------------------------------------
   function setTell3($value)
   {
      $this->tell3=$value;
   }//--------------------------------------------------------------------------
   function getFacebook()
   {
      return $this->facebook;
   }//--------------------------------------------------------------------------
   function setFacebook($value)
   {
      $this->facebook=$value;
   }//--------------------------------------------------------------------------
    function getGooglePlus()
   {
      return $this->googlePlus;
   }//--------------------------------------------------------------------------
   function setGooglePlus($value)
   {
      $this->googlePlus=$value;
   }//--------------------------------------------------------------------------
    function getTwitter()
   {
      return $this->twitter;
   }//--------------------------------------------------------------------------
   function setTwitter($value)
   {
      $this->twitter=$value;
   }//--------------------------------------------------------------------------
    function getInstagram()
   {
      return $this->instagram;
   }//--------------------------------------------------------------------------
   function setInstagram($value)
   {
      $this->instagram=$value;
   }//--------------------------------------------------------------------------
   function getHeadSeo()
   {
      $pageMetaTagCanonical="<link rel='canonical' href='{$this->webSiteAddress}' />\n";
      //Open Graph
   $pageOg="   <meta name=\"robots\" content=\"noodp,noydir\"/>
   <meta property=\"og:locale\" content=\"fa_IR\" />
   <meta property=\"og:type\" content=\"article\" />
   <meta property=\"og:title\" content=\"{$this->title}\" />
   <meta property=\"og:description\" content=\"{$this->comment}\" />
   <meta property=\"og:url\" content=\"{$this->webSiteAddress}\" />
   <meta property=\"og:site_name\" content=\"{$this->title}\" />";
      return  $pageOg . $pageMetaTagCanonical;
   }//--------------------------------------------------------------------------
   function getHeadSeo_all()
   {
   $head="<title>{$this->getTitle()}</title>
   <meta name=\"keywords\" content=\"{$this->getKeywords()}\" />
   <meta name=\"description\" content=\"{$this->getComment()}\" />
   <meta name=\"robots\" content=\"noodp,noydir\"/>";
    //Open Graph
   $pageOg="
   <meta property=\"og:locale\" content=\"fa_IR\" />
   <meta property=\"og:type\" content=\"article\" />
   <meta property=\"og:title\" content=\"{$this->title}\" />
   <meta property=\"og:description\" content=\"{$this->comment}\" />
   <meta property=\"og:url\" content=\"{$this->webSiteAddress}\" />
   <meta property=\"og:site_name\" content=\"{$this->title}\" />\n";
      return $head . $pageOg . "   <link rel='canonical' href='{$this->webSiteAddress}' />";
   }//--------------------------------------------------------------------------
}
$oSitePro=new sitePro();
?>
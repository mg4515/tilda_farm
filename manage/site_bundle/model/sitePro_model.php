<?php
class cSitePro
{
   private $table="sitePro";
   private $retSitePro;
   public $title;
   public $keywords;
   public $comment;
   public $copyRight;
   public $copyRightLink;
   public $logo;
   public $logo2;
   public $icon;
   //----------
   public $addressCo;
   public $tell1;
   public $tell2;
   public $tell3;
   //----------
   public $mailInfo;
   public $mailAds;
   public $mailNoreplay;
   public $mailAdmin;
   //----------
   public $facebook;
   public $googlePlus;
   public $twitter;
   public $instagram;
   public $telegram;

   function __construct($startPath="")
   {
      global $oDbq;
		global $oPath;
      $ret=$oDbq->table($this->table)->fields("*")->select()[0];
      $this->webSiteAddress=$ret->webSiteAddress;
      $this->title=$ret->title;
      $this->keywords=$ret->keywords;
      $this->comment=$ret->comment;
      $this->copyRight=$ret->copyright;
      $this->copyRightLink=$ret->copyrightLink;
      //--------
      $this->addressCo=$ret->addressCo;
      $this->tell1=$ret->tell1;
      $this->tell2=$ret->tell2;
      $this->tell3=$ret->tell3;
      //---------
      $this->mailAdmin=$ret->mailAdmin;
      $this->mailAds=$ret->mailAds;
      $this->mailInfo=$ret->mailInfo;
      $this->mailNoreplay=$ret->mailNoreplay;
      //---------
      $this->facebook=$ret->facebook;
      $this->googlePlus=$ret->googlePlus;
      $this->twitter=$ret->twitter;
      $this->instagram=$ret->instagram;
      $this->telegram=$ret->telegram;

      if(file_exists($oPath->manageDir("site_bundle/data/logo.png")))
         $this->logo=$oPath->manage("site_bundle/data/logo.png");
      else
         $this->logo=$oPath->asset("default/images/noImage.gif");

      if(file_exists($oPath->manageDir("site_bundle/data/logo2.png")))
         $this->logo2=$oPath->manage("site_bundle/data/logo2.png");
      else
         $this->logo2=$oPath->asset("default/images/noImage.gif");
		
      if(file_exists($oPath->manageDir("site_bundle/data/icon.png")))
         $this->icon=$oPath->manage("site_bundle/data/icon.png");
      else
         $this->icon=$oPath->asset("default/images/noImage.gif");
   }//--------------------------------------------------------------------------
   function get()
   {
      global $oDbq;
      $item=$oDbq->table($this->table)->fields("*")->where("`id`='1'")->select()[0];
      return $item;
   }//-----------------------------------------------------------------------
   function reset($startPath="")
   {
      global $oDbq;
		global $oPath;
      $ret=$oDbq->table($this->table)->fields("*")->select()[0];
      $this->title=$ret->title;
      $this->keyWords=$ret->keywords;
      $this->comment=$ret->comment;
      $this->comment=$ret->comment;

      if(file_exists($oPath->manageDir("site_bundle/data/logo.png")))
         $this->logo=$oPath->manage("site_bundle/data/logo.png");
      else
         $this->logo=$oPath->asset("admin/images/logo.png");

      if(file_exists($oPath->manageDir("site_bundle/data/icon.png")))
         $this->icon=$oPath->manage("site_bundle/data/icon.png");
      else
         $this->icon=$oPath->asset("admin/images/icon.png");
   }//--------------------------------------------------------------------------
   function getWebSiteAddress()
   {
      return $this->webSiteAddress;
   }//--------------------------------------------------------------------------
   function setWebSiteAddress($value)
   {
      $this->webSiteAddress=$value;
   }//--------------------------------------------------------------------------   
   function getTitle($standard=true)
   {
      if($standard==true)
         return mb_substr($this->title,0,70,"utf8");
      else
         return $this->title;
   }//--------------------------------------------------------------------------
   function setTitle($value)
   {
      $this->title=$value;
   }//--------------------------------------------------------------------------
   function getComment($standard=true)
   {
      if($standard==true)
         return mb_substr($this->comment,0,170,"utf8");
      else
         return $this->comment;
   }//--------------------------------------------------------------------------
   function setComment($value)
   {
      $this->comment=$value;
   }//--------------------------------------------------------------------------
   function getKeywords($standard=true)
   {
      if($standard==true)
         return mb_substr($this->keywords,0,69,"utf8");
      else
         return $this->keywords;
   }//--------------------------------------------------------------------------
   function setKeywords($value)
   {
      $this->keywords=$value;
   }//--------------------------------------------------------------------------
   function getCopyRight()
   {
      return $this->copyRight;
   }//--------------------------------------------------------------------------
   function setCopyRight($value)
   {
      $this->copyRight=$value;
   }//--------------------------------------------------------------------------
   function setCopyRightLink($value)
   {
      $this->copyRightLink=$value;
   }//--------------------------------------------------------------------------	
   function getCopyRightLink()
   {
      return $this->copyRightLink;
   }//--------------------------------------------------------------------------	
   function getIcon()
   {
      if($this->icon=="")
      {
         if(file_exists("Manage/siteBundle/data/logo.png"))
            $logo="Manage/siteBundle/data/logo.png";
         else
            $logo="Asset/img/noImage_larg.gif";
         return $logo;
      }
      else
         return $this->icon;
   }//--------------------------------------------------------------------------
   function setIcon($value)
   {
      $this->icon=$value;
   }//--------------------------------------------------------------------------
   function getLogo()
   {
      return $this->logo;
   }//--------------------------------------------------------------------------
   function setLogo($value)
   {
      $this->logo=$value;
   }//--------------------------------------------------------------------------
   function getLogo2()
   {
      return $this->logo2;
   }//--------------------------------------------------------------------------
   function setLogo2($value)
   {
      $this->logo2=$value;
   }//--------------------------------------------------------------------------	
   function getAddressCo()
   {
      return $this->addressCo;
   }//--------------------------------------------------------------------------
   function setAddressCo($value)
   {
      $this->AddressCo=$value;
   }//--------------------------------------------------------------------------
   function getTell1()
   {
      return $this->tell1;
   }//--------------------------------------------------------------------------
   function setTell1($value)
   {
      $this->tell1=$value;
   }//--------------------------------------------------------------------------
   function getTell2()
   {
      return $this->tell2;
   }//--------------------------------------------------------------------------
   function setTell2($value)
   {
      $this->tell2=$value;
   }//--------------------------------------------------------------------------
   function getTell3()
   {
      return $this->tell3;
   }//--------------------------------------------------------------------------
   function setTell3($value)
   {
      $this->tell3=$value;
   }//--------------------------------------------------------------------------
   function getMailInfo()
   {
      return $this->mailInfo;
   }//--------------------------------------------------------------------------
   function setMailInfo($value)
   {
      $this->mailInfo=$value;
   }//--------------------------------------------------------------------------
   function getMailAds()
   {
      return $this->mailAds;
   }//--------------------------------------------------------------------------
   function setMailAds($value)
   {
      $this->mailAds=$value;
   }//--------------------------------------------------------------------------
   function getMailNoreplay()
   {
      return $this->mailNoreplay;
   }//--------------------------------------------------------------------------
   function setMailNoreplay($value)
   {
      $this->mailNoreplay=$value;
   }//--------------------------------------------------------------------------
   function getMailAdmin()
   {
      return $this->mailAdmin;
   }//--------------------------------------------------------------------------
   function setMailAdmin($value)
   {
      $this->mailAdmin=$value;
   }//--------------------------------------------------------------------------
   function getFacebook()
   {
      return $this->facebook;
   }//--------------------------------------------------------------------------
   function setFacebook($value)
   {
      $this->facebook=$value;
   }//--------------------------------------------------------------------------
    function getGooglePlus()
   {
      return $this->googlePlus;
   }//--------------------------------------------------------------------------
   function setGooglePlus($value)
   {
      $this->googlePlus=$value;
   }//--------------------------------------------------------------------------
    function getTwitter()
   {
      return $this->twitter;
   }//--------------------------------------------------------------------------
   function setTwitter($value)
   {
      $this->twitter=$value;
   }//--------------------------------------------------------------------------
    function getInstagram()
   {
      return $this->instagram;
   }//--------------------------------------------------------------------------
   function setInstagram($value)
   {
      $this->instagram=$value;
   }//--------------------------------------------------------------------------
    function getTelegram()
   {
      return $this->telegram;
   }//--------------------------------------------------------------------------
   function setTelegram($value)
   {
      $this->telegram=$value;
   }//--------------------------------------------------------------------------
   function update($array)
   {
      global $oDbq;
      $oDbq->table($this->table)->fields("`webSiteAddress`","`title`","`comment`","`keywords`","`copyRight`","`copyRightLink`","`addressCo`","`tell1`","`tell2`","`tell3`","`mailAdmin`","`mailAds`","`mailInfo`","`mailNoreplay`","`facebook`","`googlePlus`","`twitter`","`instagram`","`telegram`")
                           ->values("'{$array['webSiteAddress']}'","'{$array['title']}'","'{$array['comment']}'","'{$array['keywords']}'","'{$array['copyRight']}'","'{$array['copyRightLink']}'","'{$array['addressCo']}'","'{$array['tell1']}'","'{$array['tell2']}'","'{$array['tell3']}'","'{$array['mailAdmin']}'","'{$array['mailAds']}'","'{$array['mailInfo']}'","'{$array['mailNoreplay']}'","'{$array['facebook']}'","'{$array['googlePlus']}'","'{$array['twitter']}'","'{$array['instagram']}'","'{$array['telegram']}'")->where("`id`=1")->update(false);
   }//--------------------------------------------------------------------------
}
$oSitePro=new cSitePro();
?>
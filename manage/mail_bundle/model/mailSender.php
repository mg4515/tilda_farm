<?php
   //require_once '../PHPMailer/PHPMailerAutoload.php';
   function mail_send($to,$subject,$message,$mailFrom="")
   {
      $db=new myDb();
      $dbq=new myDb_query();
      $item=$dbq->table("siteMailServer")->fields("*")->select()[0];
      if($item->active=="1")
      {
         $server=$item->server;
         $port=$item->port;
         $userName=$item->userName;
         $password=$item->password;
         $from=$item->from;

         $mail = new PHPMailer(); // create a new object
         $mail->IsSMTP(); // enable SMTP
         $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
         $mail->SMTPAuth = true; // authentication enabled
         $mail->Host = $server; //smtp.gmail.com
         //$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
         $mail->Port = $port; // or 587 or 465
         $mail->IsHTML(true);
         $mail->Username = $userName;
         $mail->Password = $password;
         $mail->setFrom($from, 'Behnovin team');//Set who the message is to be sent from
         $mail->CharSet = 'UTF-8';
         $mail->Subject = '=?utf-8?B?'.base64_encode($subject).'?=';
         $mail->addAddress('music4030@hotmail.com', 'music4030.ir');//Set who the message is to be sent to
         $mail->msgHTML("<div style='font-family: Segoe UI Light,Segoe UI Web Light,Segoe UI Web Regular,Segoe UI,Segoe UI Symbol,HelveticaNeue-Light,Helvetica Neue,Arial,sans-serif;direction:rtl;text-align:right'>" . $message . "</div>");
         $mail->AltBody = $message;
         if(!$mail->Send()) {
             return 0 ;
          } else {
             return 1;
          }
      }
      else
      {
         $header = "To: {$to}" . "\r\n";    //girandeh
         $header .= "From: $mailFrom" . "\r\n"; //ersal konandeh
         $header .= "MIME-Version: 1.0"."\r\n";
         $header .= "Content-Type: text/html; charset=UTF-8"."\r\n";
         $header .= "Content-Transfer-Encoding: 8bit"."\r\n";
         $header .= "X-Mailer: PHP v".phpversion();
         $ret=mail($to,'=?utf-8?B?'.base64_encode($subject).'?=', $message, $header)or die(555);
      }
   }

?>
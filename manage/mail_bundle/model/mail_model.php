<?php
   include_once $oPath->manageDir('mail_bundle/PHPMailer/PHPMailerAutoload.php');
   include_once $oPath->manageDir('site_bundle/model/sitePro_model.php');
   class cMail
   {
   	public $table='siteMailServer';
      function get($id,$checkActive=false)
      {
         global $oDbq;

         if($checkActive==true) $where="active=1 AND "; else $where="";

         $item=$oDbq->table($this->table)->fields("*")->where("{$where}id={$id}")->select()[0];
         return $item;
      }//-----------------------------------------------------------------------
	   function getByIdName($idName,$checkActive=false)
      {
         global $oDbq;

         if($checkActive==true) $where="active=1 AND "; else $where="";
         $item=$oDbq->table($this->table)->fields("*")->where("{$where}idName='{$idName}'")->select()[0];
         return $item;
      }//-----------------------------------------------------------------------	
      function getAll($checkActive=false)
      {
         global $oDbq;
         if($checkActive==true) $where="active=1"; else $where="0=0";
         $items=$oDbq->table($this->table)->fields("*")->where("{$where}")->select();
         return $items;
      }//-----------------------------------------------------------------------
      function send($to,$subject,$message,$idName="")
      {
			global $oDbq, $oPath;
         $oSitePro=new cSitePro();
         $siteTitle=$oSitePro->getTitle();
         $siteLogo=$oSitePro->getLogo();
			$siteLink=$oPath->root();
         $mailFrom=$oSitePro->getMailInfo();
         
			$messageCode="
         <div style='font-family:tahoma !important'>
         <div style='direction:rtl;font-size: 14px;height:64px;padding:10px;background-color : #66CCFF;border-radius: 5px 5px 0px 0px;background-image:url({$siteLogo});background-repeat: no-repeat;background-position: 5px center;'>
            <div style='color:#fff;font-size:14px;width: 65%;'>{$siteTitle}</div>
         </div>";
         $messageCode.="<div style='direction:rtl;font-size: 14px;padding:25px;background-color :#EEEEEE'>{$message}</div>";
         $messageCode.="
         <div style='direction:ltr;text-align:center;font-size: 14px;height:120px;padding:10px;background-color : #555555;color:#fff;border-radius: 0px 0px 5px 5px;'>
         <p style='direction:rtl;color:#fff'>۷ روز هفته، ۲۴ ساعته پاسخگوی شما هستیم</p>";
            if($oSitePro->getTell1()) $messageCode.="<span style='color:#fff'>{$oSitePro->getTell1()}</span>";
            if($oSitePro->getTell2()) $messageCode.="&nbsp;,&nbsp;<span style='color:#fff'>{$oSitePro->getTell2()}</span>";
            if($oSitePro->getTell3()) $messageCode.="&nbsp;,&nbsp;<span style='color:#fff'>{$oSitePro->getTell3()}</span>";
         $messageCode.="
               <br><br>
               {$siteLink}
            </div>
         </div>";

			if($idName != '') 
				$item=$this->getByIdName($idName);
			else
            $item=$this->getAll(true)[0];
         if($item->active=="1")
         {
            $server=$item->server;
            $port=$item->port;
            $userName=$item->userName;
            $password=$item->password;
            $from=$item->from;

            $mail = new PHPMailer(); // create a new object
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth = true; // authentication enabled
				$mail->SMTPOptions = array (
				'ssl' => array(
					'verify_peer'  => false,
					'verify_peer_name'  => false,
					'allow_self_signed' => true));
				$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = $server; //smtp.gmail.com
            $mail->Port = $port; // or 587 or 465
            $mail->IsHTML(true);
            $mail->Username = $userName;
            $mail->Password = $password;
            $mail->setFrom($from, $siteTitle);//Set who the message is to be sent from
            $mail->CharSet = 'UTF-8';
            $mail->Subject = '=?utf-8?B?'.base64_encode($subject).'?=';
            $mail->addAddress($to);//Set who the message is to be sent to
            //$mail->msgHTML("<div style='font-family: Segoe UI Light,Segoe UI Web Light,Segoe UI Web Regular,Segoe UI,Segoe UI Symbol,HelveticaNeue-Light,Helvetica Neue,Arial,sans-serif;direction:rtl;text-align:right'>" . $message . "</div>");
            $mail->msgHTML($messageCode);
            //$mail->AltBody = $messageCode;
            if(!$mail->Send()) 
				{
               return 0 ;
            } 
				else 
				{
               return 1;
            }
         }
         else
         {
            $message="<div style='font-family: Segoe UI Light,Segoe UI Web Light,Segoe UI Web Regular,Segoe UI,Segoe UI Symbol,HelveticaNeue-Light,Helvetica Neue,Arial,sans-serif;direction:rtl;text-align:right'>" . $message . "</div>";
            $header = "To: {$to}" . "\r\n";    //girandeh
            $header .= "From: {$mailFrom}" . "\r\n"; //ersal konandeh
            $header .= "MIME-Version: 1.0"."\r\n";
            $header .= "Content-Type: text/html; charset=UTF-8"."\r\n";
            $header .= "Content-Transfer-Encoding: 8bit"."\r\n";
            $header .= "X-Mailer: PHP v".phpversion();
            $ret=mail($to,'=?utf-8?B?'.base64_encode($subject).'?=', $message, $header)or die(555);
         }
      }
   }
?>
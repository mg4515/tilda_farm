<?php
	//include
	include_once $_SESSION["engineRequire"];
	include_once $oPath->manageDir("admin_bundle/model/admin_model.php");
	include_once $oPath->manageDir("admin_bundle/model/adminMenu_model.php");
	
   //object	
	$oAdmin=new cAdmin();
	$oAdminMenu=new cAdminMenu(); 	
	
	//function
	function admin_checkLogin($return=false,$redirect="")
	{
		if($return==true)
		{
			if(!isset($_SESSION['admin_login']) || @!$_SESSION['admin_login'])
				return false;	
			else
				return true;
		}
		else
		{
			if(!isset($_SESSION['admin_login']) || @!$_SESSION['admin_login'])
			{
				header("Location:{$redirect}");
				exit;			
			}			
		}
	}//--------------------------------------------------------------------------
   function admin_menuChksDraw($parentId=0,$ArryAccess) //list hame menuha be soorate check box
   {
		global $oAdminMenu;	
		
      //list menuhaye ghabel dastras
      $items=$oAdminMenu->getByParentId($parentId);
      $code="";
      if(count($items) > 0)
      {
         foreach($items as $item)
         {
            if(strlen(array_search($item->id,$ArryAccess)) > 0 ) $checked=" checked='checked'"; else $checked="";
            if((count($ArryAccess) == 1 && $ArryAccess[0]=="") || count($ArryAccess) == 0) $checked=" checked='checked'";
            if ($parentId == 0) $class="has-sub"; else $class="";
            $code.="
            <li class='{$class}'><input type='checkbox' id='chk_{$item->id}' value='{$item->parentId}'{$checked} onchange='userAccessLevel_changedChk({$item->id});'>&nbsp;&nbsp;{$item->text}
               <ul style='list-style-type: none;'>";
                  $code.=admin_menuChksDraw($item->id,$ArryAccess);
                  $code.="
               </ul>
            </li>
            ";
         }
         return $code;
      }
      else
         return "";
   }//--------------------------------------------------------------------------
   function admin_menuListDraw($parentId=0,$ArryAccess,$pageActive="")  //list hame menuha be soorate derakhty
   {
      $oAdminMenu=new cAdminMenu();		
      $items=$oAdminMenu->getByParentId($parentId,1);
      $code="";
      if(count($items) > 0) 
      {
         $code.= "<ul class='mtree dark' >";
         foreach($items as $item)
         {
            if(array_search($item->id,$ArryAccess) || $_SESSION['admin_generalManager']==1)
            {
					$idCounter="spn_{$item->idName}";
               if($item->url=="")
               { 
                  $code.="<li id='li_{$item->id}' class='nav-second-level'>
                          <a href='#'>
                             <i class='mtree-icon {$item->fontIcon_class}'></i>
                             {$item->text}
									  <span class='mtree-counter' id='{$idCounter}'></span>
                          </a>";
                      $code.=admin_menuListDraw($item->id,$ArryAccess,$pageActive);
                  $code.="</li>";
               }
               else
               {
						if($item->url == $pageActive) $activeClass='active'; else $activeClass='';
                  $code.="
                  <li id='li_{$pageActive}'>
                     <a href='javascript:void(0)' onclick='oTools.link(\"?page={$item->url}\");' id='{$item->url}' class='{$activeClass}' >
							   <i class='mtree-icon {$item->fontIcon_class}' ></i>
								{$item->text}
								<span class='mtree-counter' id='{$idCounter}'></span>
							</a>                    
						</li>
						";
						if($item->url == $pageActive) 
						{
							$oldParentId=@$oAdminMenu->get($item->parentId)->parentId;
						   cEngine::echoAfter("
							<script>
							   try{
							      $('#li_{$oldParentId}').removeClass('mtree-closed');
							      $('#li_{$oldParentId}').addClass('mtree-open');
							      $('#li_{$oldParentId} ul:first').css({'display':'block','height':'auto'});
								}catch(e){}	
								
							   $('#li_{$item->parentId}').removeClass('mtree-closed');
							   $('#li_{$item->parentId}').addClass('mtree-open');
							   $('#li_{$item->parentId} ul:first').css({'display':'block','height':'auto'});
							</script>");	
						}
               }
            }
         }
         $code.="</ul>";
         return $code;
      }
      else
         return "";
   }//------------------------------------------------------------------------------------
   function admin_getImage()
	{
		global $oPath;
		if(@$_SESSION["admin_login"]==true)
		{
			$userId=$_SESSION["admin_id"];
			if(file_exists($oPath->manageDir("admin_bundle/data/images/user_{$userId}.jpg")))
				return $oPath->manage("admin_bundle/data/images/user_{$userId}.jpg");
			else
				return $oPath->asset("default/images/admin_larg.png");
		}			
	}	
?>
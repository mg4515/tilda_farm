<?php
   @session_start();
	
	//includes
	require_once $_SESSION["engineRequire"]; //engine.php
	include_once $oPath->manageDir("admin_bundle/model/admin_model.php");
	include_once $oPath->manageDir("admin_bundle/model/adminMenu_model.php");
	include_once $oPath->manageDir("admin_bundle/controller/php/admin_func.php");

	//objects
	$oAdmin=new cAdmin();
	$oMenu=new cAdminMenu();   
   
	//request check
	$request=@$_REQUEST["requestName"];
	
	//start request check
	if($request=="admin_draw") //chap list modiran
   {
		$trash=isset($_REQUEST['trash']) ? $oDbq->escape($_REQUEST['trash']) : '0';
		$active=$oDbq->escape($_REQUEST['active']);
		
		//auto load arg
		if($trash==-1) $trash=$_SESSION['admin_draw_trash'];
		if($active==-1) $active=$_SESSION['admin_draw_active'];
		
		//auto save arg
      $_SESSION['admin_draw_trash']=$trash;
      $_SESSION['admin_draw_active']=$active;
		
		if($active) $title='فعال';
		else if($trash) $title='حذف شده';
		else $title='غیر فعال';
		
      $items=$oAdmin->getAll(true,$active,$trash);
      $code="";
      foreach($items as $item)
      {
         $id=$item->id;
			
			//active
			if($oAdmin->isAllowed('mnu_adminAccessActive',false))
			{
            if($item->active=="0") $active="<button class='btn btn-danger' onclick='admin_active($item->id);'>غیر فعال</button>";
            elseif($item->active=="1") $active="<button class='btn btn-success' onclick='admin_active($item->id);'>فعال</button>";
         }
			else
			{
            if($item->active=="0") $active="<span class='lbl lbl-danger'>غیر فعال</span>";
            elseif($item->active=="1") $active="<span class='lbl lbl-danger'>فعال</span>";
			}

			//edit
			if($oAdmin->isAllowed('mnu_adminAccessEdit',false))
            $btnEdit="<button class='btn btn-info' onclick='admin_edit({$item->id});'><i class='fa fa-pencil'></i></button>";
			else
            $btnEdit="";

			//trash
			if($oAdmin->isAllowed('mnu_adminAccessTrash',false) && $item->generalManager=='0')
            $btnTrash="<button class='btn btn-danger' onclick='admin_trash({$item->id});'><i class='fa fa-trash-o'></i></button>";
			else
            $btnTrash="";			
			
			if($item->userName == $_SESSION["admin_userName"])
			{
			   $styleMe="";
				$infoMe="<i class='chatOnline-infoMe'></i>";
			}	
         else 
			{
				$styleMe=""; 
				$infoMe="";
			}
			
         if(file_exists($oPath->manageDir("admin_bundle/data/images/user_{$id}.jpg")))
            $img="<div class='chatOnline img' style='{$styleMe};background-image:url(". $oPath->manage("admin_bundle/data/images/user_{$id}.jpg?t=" . time() ) .");'>{$infoMe}</div>";
         else
            $img="<div class='chatOnline img' style='{$styleMe};background-image:url(". $oPath->asset("default/images/noImage.gif") .");border-radius:0%' >{$infoMe}</div>";

         $code.="
         <tr>
            <td style='text-align:center;width: 64px;padding:5px;'>{$img}&nbsp;{$item->userName}</td>
            <td class='algn-r-important'>
               <span id='td_active_{$item->id}'>{$active}</span>
					{$btnTrash}
					{$btnEdit}
            </td>
         </tr>";
      }
		
		//new
		if($oAdmin->isAllowed('mnu_adminAccessInsert',false))
			$btnNew="<button type='button' class='btn btn-success' onclick='admin_new();'><i class='fa fa-plus'></i>&nbsp;مدیر جدید</button>";
		else
			$btnNew="";
			
		if(count($items)==0) $code='<h3 class="fg-grayLight"><i class="fa fa-info"></i>&nbsp;خالی است</h3>';
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-users'></i>&nbsp;مدیران&nbsp;{$title}</h1>
		<div class='vSpace-4x'></div>

		<div>
			{$btnNew}	
		</div>
		<br>
		<table class='tbl tbl-bordered tbl-hover'>
			{$code}
		</table>

		<div class='vSpace-4x'></div>
      ";
		$oEngine->response("ok[|]" . $code);
   }//------------------------------------------------------------------------------------
   else if($request=="admin_trash")
   {
		if($oAdmin->isAllowed('mnu_adminAccessTrash',false))
		{
			$adminId=$oDb->escape($_REQUEST["id"]);
			$oAdmin->trash($adminId);
			$oEngine->response('ok');
		}
		else
		{
			$oEngine->response('err');
		}
   }//------------------------------------------------------------------------------------
   else if($request=="admin_unTrash")
   {
		if($oAdmin->isAllowed('mnu_adminAccessTrash',false))
		{
			$adminId=$oDb->escape($_REQUEST["id"]);
			$oAdmin->unTrash($adminId);
			$oEngine->response('ok');
		}
		else
			$oEngine->response('err');
   }//------------------------------------------------------------------------------------
   else if($request=="admin_edit")
   {
		if($oAdmin->isAllowed('mnu_adminAccessEdit',false))
		{
			$adminId=$oDb->escape($_REQUEST["id"]);
			$ret=$oAdmin->get($adminId,true);
			$itemAdmin=$ret["admin"];
			$itemProfile=$ret["profile"];
			$ArryAccess=explode("|",$itemAdmin->accessList);

			$menuChecks=admin_menuChksDraw(0,$ArryAccess); //list hame menuha besoorate checkBox

			$t=time();
			//ADMIN IMAGE
			if(!file_exists($oPath->manageDir("admin_bundle/data"))) mkdir($oPath->manageDir("admin_bundle/data"));
			if(!file_exists($oPath->manageDir("admin_bundle/data/images"))) mkdir($oPath->manageDir("admin_bundle/data/images"));

			if(file_exists($oPath->manageDir("admin_bundle/data/images/user_{$adminId}.jpg")))
				$img="<img id='img_admin' src='". $oPath->manage("admin_bundle/data/images/user_{$adminId}.jpg?t={$t}") ."' style='border-radius:8px;width:120px'>&nbsp;<i id='i_adminImgDel' class='fa fa-trash fa-2x' onclick='admin_imgDel({$adminId});' style='cursor:pointer;color:red;'></i>";
			else
				$img="<img id='img_admin' src='". $oPath->asset("admin/images/admin_larg.png") ."' style='border-radius:8px;width:120px'>";

			$code="
				<div class='vSpace-4x'></div>
			   <h1>
				   <button class='btn btn-default' onclick='admin_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
				   &nbsp;|&nbsp;
					<i class='fa fa-pencil'></i>&nbsp;ویرایش مدیر ({$itemAdmin->userName})
				</h1>
				<div class='vSpace-4x'></div>
				
				<table class='tbl tbl-right form'>
					<tr>
						<td style='width:100px;'>نام کاربری :</td>
						<td><input type='text' id='txt_userName' value='$itemAdmin->userName' class='form-control'></td>
					</tr>
					<tr>
						<td>رمز عبور :</td>
						<td><input type='text' id='txt_password' value='' class='form-control'></td>
					</tr>
				</table>
				<hr>
				<table>
					<tr>
						<td style='width:100px;'>سطح دسترسی&nbsp;:&nbsp;</td>
						<td>";
							//if($accessLevels_comboBox) $code.="شیوه دسترسی بر اساس : {$accessLevels_comboBox}&nbsp;<br><br>";
							$code.="<button class='btn btn-info' onclick='listTog();'>ویرایش سطح دسترسی</button>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div id='layer_accessLevelList' style='text-align:right' class='ul ul-right ul-tree'>{$menuChecks}</div>
						</td>
					<tr>
				</table>
				<hr>
				<table class='tbl tbl-right form'>
					<tr>
						<td style='width:100px;'>نام :</td>
						<td><input type='text' id='txt_fname' value='{$itemProfile->fName}' class='form-control'></td>
					</tr>
					<tr>
						<td>نام خانوادگی :</td>
						<td><input type='text' id='txt_lname' value='{$itemProfile->lName}' class='form-control'></td>
					</tr>
					<tr>
						<td>موبایل :</td>
						<td><input type='text' id='txt_phone' value='{$itemProfile->phone}' class='form-control'></td>
					</tr>
					<tr>
						<td>آدرس :</td>
						<td><textarea id='txt_address' class='form-control'>{$itemProfile->address}</textarea></td>
					</tr>
				</table>
				<hr>
				<table class='tbl form'>
					<tr>
						<td style='width:180px;'>
							{$img}
							<br>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='fileImg' onchange='oTools.previewImgFile(\"fileImg\",\"img_admin\");' required/>
								<span class='lbl-file-icon'></span>
								<span>انتخاب تصویر</span>
							</label>
						</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<button class='btn btn-default' onclick='admin_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>				
				<button class='btn btn-success' onclick='admin_update(\"edit\",{$adminId});'>ذخيره</button>
				<br><br><br>
				<script type='text/javascript'>
					$('#layer_accessLevelList').toggle('slow');
					function listTog()
					{
						$('#layer_accessLevelList').toggle('slow');
					}
				</script>
			";

			$oEngine->response("ok[|]{$code}");
		}
		else
         $oEngine->response('err');	
   }//------------------------------------------------------------------------------------
   else if($request=="admin_new")
   {
		if($oAdmin->isAllowed('mnu_adminAccessInsert',false))
		{
			$ArryAccess=[];
			$menuChecks=admin_menuChksDraw(0,$ArryAccess); //list hame menuha besoorate checkBox

			//ADMIN IMAGE
			if(!file_exists($oPath->manageDir("admin_bundle/data"))) mkdir($oPath->manageDir("admin_bundle/data"));
			if(!file_exists($oPath->manageDir("admin_bundle/data/images"))) mkdir($oPath->manageDir("admin_bundle/data/images"));
			$img="<img id='img_admin' src='". $oPath->asset("admin/images/admin_larg.png") ."' style='border-radius:8px;width:120px'>";

			$code="
				<div class='vSpace-4x'></div>
				<h1>
				   <button class='btn btn-default' onclick='admin_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
				   &nbsp;|&nbsp;
					<i class='fa fa-plus'></i>&nbsp;مدیر جدید
				</h1>
				<hr>
				<div class='vSpace-4x'></div>
				
				<table class='tbl tbl-right form'>
					<tr>
						<td style='width:100px;'>نام کاربری :</td>
						<td><input type='text' id='txt_userName' class='form-control'></td>
					</tr>
					<tr>
						<td>رمز عبور :</td>
						<td><input type='text' id='txt_password' value='' class='form-control'></td>
					</tr>
				</table>
				<hr>
				<table>
					<tr>
						<td style='width:100px;'>سطح دسترسی&nbsp;:&nbsp;</td>
						<td>";
							//if($accessLevels_comboBox) $code.="شیوه دسترسی بر اساس : {$accessLevels_comboBox}&nbsp;<br><br>";
							$code.="<button class='btn btn-info' onclick='listTog();'>ویرایش سطح دسترسی</button>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div id='layer_accessLevelList' style='list-style-type: none;text-align:right'>{$menuChecks}</div>
						</td>
					<tr>
				</table>
				<hr>
				<table class='tbl tbl-right form'>
					<tr>
						<td style='width:100px;'>نام :</td>
						<td><input type='text' id='txt_fname' class='form-control'></td>
					</tr>
					<tr>
						<td>نام خانوادگی :</td>
						<td><input type='text' id='txt_lname' class='form-control'></td>
					</tr>
					<tr>
						<td>موبایل :</td>
						<td><input type='text' id='txt_phone' class='form-control'></td>
					</tr>
					<tr>
						<td>آدرس :</td>
						<td><textarea id='txt_address' class='form-control'></textarea></td>
					</tr>
				</table>
				<hr>
				<table class='tbl form'>
					<tr>
						<td style='width:180px;'>
							{$img}
							<br>
							<label class='lbl-file lbl-file-right'>
								<input type='file' id='fileImg' onchange='oTools.previewImgFile(\"fileImg\",\"img_admin\");' required/>
								<span class='lbl-file-icon'></span>
								<span>انتخاب تصویر</span>
							</label>
						</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<button class='btn btn-default' onclick='admin_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>				
				<button class='btn btn-success' onclick='admin_update(\"new\",0);'>ذخيره</button>
				<br><br><br>
				<script type='text/javascript'>
					$('#layer_accessLevelList').toggle('slow');
					function listTog()
					{
						$('#layer_accessLevelList').toggle('slow');
					}
				</script>
			";

			$oEngine->response("ok[|]{$code}");
		}
		else
         $oEngine->response('err');
   }//------------------------------------------------------------------------------------
   else if($request=="admin_update")
   {
		$purpose=$oDb->escape($_REQUEST["purpose"]);
		
		if($purpose=="edit")
		   $adminId=$oDb->escape($_REQUEST["id"]);
	   else
			$adminId=time();
		
      $array=array();
      $array["purpose"]=$oDb->escape($_REQUEST["purpose"]);
      $array["id"]=$adminId;
      $array["userName"]=$oDb->escape($_REQUEST["userName"],false);
      $array["password"]=$oDb->escape($_REQUEST["password"]);
      $array["accessId"]=$oDb->escape($_REQUEST["accessId"]);
      $array["accessList"]=$oDb->escape($_REQUEST["accessList"]);
      $array["loginTime"]=0;
      $array["fName"]=$oDb->escape($_REQUEST["fName"],false);
      $array["lName"]=$oDb->escape($_REQUEST["lName"],false);
      $array["phone"]=$oDb->escape($_REQUEST["phone"]);
      $array["address"]=$oDb->escape($_REQUEST["address"]);
      
		if($array['purpose']=='new' && $oAdmin->isAllowed('mnu_adminAccessInsert',false))
         $oAdmin->insert($array);
		else if($array['purpose']=='edit' && $oAdmin->isAllowed('mnu_adminAccessEdit',false))
			$oAdmin->update($array);
		
		$imgDel=$oDb->escape($_REQUEST["imgDel"]);
      if($imgDel==1 || $imgDel=="true") @unlink($oPath->manageDir("admin_bundle/data/images/user_{$adminId}.jpg"));
		
      if(count($_FILES) > 0)
      {
         if($oTools->fs_fileIsImage($_FILES["file"]["name"]))
         {
            copy($_FILES["file"]["tmp_name"],$oPath->manageDir("admin_bundle/data/images/user_{$adminId}.jpg"));
            @unlink($_FILES["file"]["tmp_name"]);
         }
      }
      $oEngine->response("ok");		
   }//------------------------------------------------------------------------------------
   else if($request=="admin_active")
   {
		if($oAdmin->isAllowed('mnu_adminAccessActive',false))
		{
			$adminId=$oDb->escape($_REQUEST["id"]);
			if($oAdmin->getActive($adminId)=="0")
			{
				$code="<button class='btn btn-success' onclick='admin_active({$id});'>فعال</button>";
				$oAdmin->setActive($adminId,'1');
			}
			else if($oAdmin->getActive($adminId)=="1")
			{
				$code="<button class='btn btn-danger' onclick='admin_active({$id});'>غیر فعال</button>";
				$oAdmin->setActive($adminId,'0');
			}
			$oEngine->response("ok[|]{$adminId}[|]{$code}");	
		}		
		else
		   $oEngine->response("err");		
   }//--------------------------------------------------------------------------
   else if($request=="admin_login")
   { 
      $userName=cTools::str_faIntToEnInt(cDataBase::escape($_REQUEST["userName"],false));
      $password=cTools::str_faIntToEnInt(cDataBase::escape($_REQUEST["password"],false));
      $sec=cTools::str_faIntToEnInt($_REQUEST["sec"]);

      if(!isset($_SESSION["sec_login"]) || $sec!=$_SESSION["sec_login"])
      { 
         $oEngine->response("errSec");		
         exit;
      }
      $oAdminMenu=new cAdminMenu();
      $ret=$oAdmin->login($userName,$password);
      if($ret=='ok')
      {
			$url=@$oAdminMenu->getByPageindex()->url;
         if($url)
			   $oEngine->response("ok[|]{$url}");
         else
            $oEngine->response("home");
         $oAdminMenu->runFirstCode();
			
		   //LOG
		   $d=date("Y/m/d h:i:s");
		   file_put_contents($oPath->publicDir("admin/loginLog.txt"),"<log type='adminLogin' action='login' date='{$d}' admin={$_SESSION['admin_id']} ></log>\n",FILE_APPEND);			
      }
      else
		   $oEngine->response($ret);
		
   }//--------------------------------------------------------------------------
   else if($request=="admin_logout")
   {
		$id=$_SESSION['admin_id'];
      $oAdmin->logout();
		$oEngine->response("ok");
		
		//LOG
		$d=date("Y/m/d h:i:s");
		file_put_contents($oPath->publicDir("admin/loginLog.txt"),"<log type='adminLogin' action='logout' date='{$d}' admin={$id} ></log>\n",FILE_APPEND);			
   }//------------------------------------------------------------------------------------
	else if($request=="admin_notification_counter")
	{
		if(@$_SESSION['admin_id'])
		{
			//include
			include_once $oPath->manageDir("shop_bundle/model/shopItems_model.php");
			include_once $oPath->manageDir("kifPool_bundle/model/kifPool_model.php");
			include_once $oPath->manageDir("ticket_bundle/model/ticket_model.php");
		   include_once $oPath->manageDir("contactUs_bundle/model/contactUs_model.php");
				
			//objects
			$oShopItems=new cShopItems();
			$oKifPool=new cKifPool();		
			$oTicket=new cTicket();	
			$oContactUs=new cContactUs();
			
			//var
			$id_mnu_shop='spn_mnu_shop';
			$id_mnu_shopItemsNew='spn_mnu_shopItemsNew';
			$id_mnu_shopItemsOpine='spn_mnu_shopItemsOpine'; //darkhast taeed maghale
			//---
			$id_shopItemsOpine='spn_shopItemsOpine'; //darkhast taeed maghale
			$id_shopItems='spn_shopItems'; //darkhast taeed mahsool

			//---
			$id_mnu_kifPool='spn_mnu_kifPool';
			$id_mnu_kifPoolWithdrawa='spn_mnu_kifPoolWithdrawa';	
			//---
			$id_mnu_ticket='spn_mnu_ticket';
			$id_mnu_ticketMsg='spn_mnu_ticketMsg';			
			$id_ticket='spn_ticket';
			//---
			$id_mnu_sitePages='spn_mnu_sitePages';
		   $id_mnu_contactUs='spn_mnu_contactUs';	
			$id_contactUs='spn_contactUs';
			
			$str='';
			
			//shop
			$count=count($oShopItems->getAll(['confirm'=>'0', 'active'=>'1', 'trash'=>'0']));
			if($count > 0) $str.="{$id_mnu_shopItemsNew}:{$count}[|]";
			if($count > 0) $str.="{$id_shopItems}:{$count}[|]";
			if($count > 0) $str.="{$id_mnu_shop}:<i class='fa fa-info'></i>[|]";				
			//---
			$count=count($oShopItems->opine_getAll('','','0'));
			$str.="{$id_shopItemsOpine}:{$count}[|]";
			if($count > 0) $str.="{$id_mnu_shopItemsOpine}:{$count}[|]";
			if($count > 0) $str.="{$id_mnu_shop}:<i class='fa fa-info'></i>[|]";

			//kifPool
			$count=count($oKifPool->transaction_getAll(['type'=>'out', 'status'=>'suspended', 'purpose'=>'withdrawal', 'trash'=>'0']));
			if($count > 0) $str.="{$id_mnu_kifPoolWithdrawa}:{$count}[|]";
			if($count > 0) $str.="{$id_mnu_kifPool}:<i class='fa fa-info'></i>[|]";

			//ticket
			$count=count($oTicket->getAllByCheckout($_SESSION['admin_id']));
			if($count > 0) $str.="{$id_ticket}:{$count}[|]";
			if($count > 0) $str.="{$id_mnu_ticketMsg}:{$count}[|]";
			if($count > 0) $str.="{$id_mnu_ticket}:<i class='fa fa-info'></i>[|]";
			
			
			//contactUs
			$count=$oContactUs->getCountNew();
			if($count > 0) $str.="{$id_contactUs}:{$count}[|]";
			if($count > 0) $str.="{$id_mnu_contactUs}:{$count}[|]";
			if($count > 0) $str.="{$id_mnu_sitePages}:<i class='fa fa-info'></i>[|]";
		
			$str=rtrim($str,'[|]');
			cEngine::response("ok[|]{$str}");
		}
		else
			cEngine::response("err");
	}	
?>
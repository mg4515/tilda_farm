var url_admin=oPath.manage('admin_bundle/controller/php/admin_panel.php');
var isAdminDelImg=false;
function userAccessLevel_changedChk(value)
{
   try
   {
      var checked=$("#chk_" + value).prop("checked");
      $("#tr_chks input[value='" + value + "']").parent("li").find('input').prop("checked",checked);
      $("#layer_accessLevelList input[value='" + value + "']").parent("li").find('input').prop("checked",checked);
   }
   catch(e)
   {
      //
   }
}//-----------------------------------------------------------------------------
function admin_login()
{
	try{
		if (localStorage.adminUserName==undefined) localStorage.setItem('adminUserName','');
      var userName=$('#txt_userName').val();
		var password=$('#txt_password').val();
		var sec=$('#txt_sec').val();
		script_loadingShow();
		$.ajax({
			url: url_admin,
			data: {'requestName':'admin_login', 'userName':userName, 'password':password, 'sec':sec},
			method: 'POST',
			success: function(result)
			{
			   result=oEngine.response(result);
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               localStorage.adminUserName=$("#txt_userName").val();
               oTools.link(oPath.root("admin?page=" + spl[1]));
            }				
            else if(spl[0]=="errLogin")
            {
               $("#spn_loginInfo").html("نام کاربری یا رمز عبور اشتباه است&nbsp;<i class='fa fa-info'></i>");
               $("#spn_secInfo").html("");
               //script_loadingHide();
            }
            else if(spl[0]=="errSec")
            {
               $("#spn_secInfo").html("با تصویر مطابقت ندارد&nbsp;<i class='fa fa-info'></i>");
               $("#spn_loginInfo").html("");
            }
				script_loadingHide();
			},
			error: function()
			{
			   alert('خطایی در اتصال رخ داده است');
				script_loadingHide();
			}
		});
	}
	catch(e)
	{
	   alert('خطایی رخ داده است');
      script_loadingHide();		
	}
}//-----------------------------------------------------------------------------
	function admin_logout()
	{
		try {
			script_loadingShow();
			$.ajax({
				url: url_admin,
				data: {"requestName": "admin_logout"},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					if(result=="ok")
					{
						oTools.link("admin/login");
					}					
					else
					{
						alert("انجام نشد");
						alert(resulte);
					}
					script_loadingHide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					script_loadingHide();
				}

			});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
	}//---------------------------------------------------------------------------------------
function admin_draw(active,trash)
{
	isAdminDelImg=false;
   try {
		if(active=='auto')
		{
			trash="-1";
			active="-1";			
		}
		if(trash==undefined) var trash="0";
		if(active==undefined) var active="";
      script_loadingShow();
      $.ajax({
         url: url_admin,
         data: {"requestName":"admin_draw", "trash":trash, "active":active},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function admin_imgDel(adminId)
{
   if(isAdminDelImg==false)
   {
      var ret=confirm("تصویر حذف شود ؟");
      if(ret)
      {
         isAdminDelImg=true;
         $("#img_admin").attr("src",oPath.asset("admin/images/admin_larg.png"));
         $("#i_adminImgDel").attr("class","fa fa-share fa-2x");
      }
   }
   else if(isAdminDelImg==true)
   {
      isAdminDelImg=false;
      $("#img_admin").attr("src",oPath.manage("admin_bundle/data/images/user_" + adminId + ".jpg"));
      $("#i_adminImgDel").attr("class","fa fa-trash fa-2x");
   }
}//-----------------------------------------------------------------------------
function admin_del(adminId)
{
   var ret=confirm("به صورت کلی از سیستم حذف شود ؟.");
   if(ret)
   {
      try {
         script_loadingShow();
         $.ajax({
            url: url_admin,
            data: {"requestName":"admin_del","id":adminId},
            method: "POST",
            success: function(result)
            {
               //alert(result);
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  admin_draw();
               }
               else
               {
                  alert("انجام نشد");
                  alert(result);
               }
               script_loadingHide();
            },
            error: function() {
               script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
            }
         });
      } catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
      }
   }
}//-----------------------------------------------------------------------------
function admin_trash(adminId)
{
   var ret=confirm("به صورت کلی از سیستم حذف شود ؟.");
   if(ret)
   {
      try {
         script_loadingShow();
         $.ajax({
            url: url_admin,
            data: {"requestName":"admin_trash","id":adminId},
            method: "POST",
            success: function(result)
            {
               //alert(result);
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  admin_draw();
               }
               else
               {
                  alert("انجام نشد");
                  alert(result);
               }
               script_loadingHide();
            },
            error: function() {
               script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
            }
         });
      } catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
      }
   }
}//-----------------------------------------------------------------------------
function admin_active(adminId)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_admin,
         data: {"requestName":"admin_active","id":adminId},
         method: "POST",
         success: function(result)
         {
            //alert(result);
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#td_active_" + spl[1]).html(spl[2]);
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function admin_new()
{
   try {
        script_loadingShow();
        $.ajax({
            url: url_admin,
            data: {"requestName":"admin_new"},
            method: "POST",
            success: function(result)
            {
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  $("#layer_content").html(spl[1]);
               }
               else
               {
                  alert("انجام نشد!.");
                  alert(result);
               }
               script_loadingHide();
            },
            error: function() {
					alert("خطایی در اتصال رخ داده است");
               script_loadingHide();
            }
        });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function admin_edit(adminId)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_admin,
         data: {"requestName":"admin_edit", "id":adminId },
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
               alert(result);
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function admin_update(purpose,adminId)
{
   //try {
      script_loadingShow();
		
      var accessList="";
      var checks=$("#layer_accessLevelList input");
      for(i=0;i < checks.length;i++)
      {
         if(checks[i].checked==true)
         {
            accessList+=checks[i].id.split("_")[1];
            if(i < checks.length-1) accessList+="|";
         }
      }
		
      var file_data = $('#fileImg').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);
		form_data.append("id",adminId);
		form_data.append("requestName","admin_update");
		form_data.append("userName",$("#txt_userName").val());
		form_data.append("password",$("#txt_password").val());
		form_data.append("accessId",0);
		form_data.append("fName",$("#txt_fname").val());
		form_data.append("lName",$("#txt_lname").val());
		form_data.append("phone",$("#txt_phone").val());
		form_data.append("address",$("#txt_address").val());	
		form_data.append("accessList",accessList);
		form_data.append("imgDel",isAdminDelImg);
		form_data.append("purpose",purpose);

      $.ajax({
         url: url_admin,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            if(result=="ok")
            {
               admin_draw();
            }
            else
            {
               alert("انجام نشد");
               alert(result);
            }
            script_loadingHide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }

      });
   //}
   //catch (e)
   //{
		//alert("خطای اسکریپت");
		//script_loadingHide();
   //}
}//-----------------------------------------------------------------------------
function admin_selectAccessList(accessId)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_admin,
         data: {"requestName": "ajax_userModir_selectAccessList","accessId":accessId},
         method: "POST",
         success: function(result)
         {
            //alert(result);
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_accessLevelList").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
               alert(result);
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function admin_notification_counter()
{
   try {
      $.ajax({
         url: url_admin,
         data: {"requestName": "admin_notification_counter"},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					for(i=1; i < spl.length; i++)
					{
						spl2=spl[i].split(':');
						elmId=spl2[0];
						console.log(elmId);
						elmVal=spl2[1];
						console.log(elmId);
						$("#" + elmId).html(elmVal);
						if($("#" + elmId + "_icon i"))
						{
							if(elmVal > 0) 
								$("#" + elmId + "_icon i").css({'color':'#f0a30a', 'transform':'scale(1.1,1.1)'});
							else
								$("#" + elmId + "_icon i").css({'color':'#000', 'transform':'scale(1,1)'});
						}
						document.querySelector("#" + elmId).style.opacity=1;
					}
					setTimeout(function(){
						admin_notification_counter();
					},30000);
            }
            else
            {
					setTimeout(function(){
						admin_notification_counter();
					},30000);
					//console.log('admin_notification_counter error');
            }
         },
         error: function() {}
      });
   } 
	catch (e) {}
}//-----------------------------------------------------------------------------

//service run
admin_notification_counter();
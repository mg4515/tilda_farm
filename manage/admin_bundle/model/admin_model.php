<?php
   class cAdmin {
		private $tbl_admin="admin";
		private $tbl_usersProfile="usersProfile";
		function getALL($sortByRow=true,$active="",$trash="",$limitStr="") //select all admin
		{
			global $oDbq;
			$where="";
			if($active != "")
			{
			   if($where=="") $where="`active`={$active}"; else $where.=" AND `active`={$active}";	
			}
			if($trash != "")
			{
			   if($where=="") $where="`trash`={$trash}"; else $where.=" AND `trash`={$trash}";	
			}			
			if($sortByRow==true) $order="`id` DESC"; else $order="";	
			$ret=$oDbq->table($this->tbl_admin)->fields("*")->where($where)->limit($limitStr)->orderBy($order)->select();
         return $ret;
		}//--------------------------------------------------------------------------	
		function get($id) //select one admin by id
		{
			$ret=[];
			global $oDbq;
			$item=$oDbq->table($this->tbl_admin)->fields("*")->where("`id`='{$id}'")->select()[0];
			$itemProfile=@$oDbq->table($this->tbl_usersProfile)->fields("*")->where("`userId`='{$item->id}' AND `userName`='{$item->userName}' AND `userType`='admin'")->select()[0];
			
			if(!$itemProfile)
			{
				$oDbq->table($this->tbl_usersProfile)->set("`userId`={$id},`userName`='{$item->userName}',`userType`='admin'")->insert();				
			   $itemProfile=@$oDbq->table($this->tbl_usersProfile)->fields("*")->where("`userId`='{$id}' AND `userType`='admin'")->select()[0];
			}
			
			$ret["admin"]=$item;
			$ret["profile"]=$itemProfile;
         return $ret;
		}//--------------------------------------------------------------------------	
		public function get2($userNameId,$trash="")
		{
			global $oDbq,$oPath;
			$where='';
			if($trash!='')
			{
				$where=" AND `admin`.`trash`={$trash}";
			}		
			$ret=@$oDbq->table("`{$this->tbl_admin}`,`{$this->tbl_usersProfile}`")->fields('*')->where("(`admin`.`id`='{$userNameId}' AND `usersProfile`.`userId`='{$userNameId}'{$where}) OR (`admin`.`userName`='{$userNameId}' AND `usersProfile`.`userName`='{$userNameId}'{$where})")->select('','assoc')[0];
			if($ret)
			{
				if(file_exists($oPath->manageDir("users_bundle/data/images/{$ret['id']}.png")))
					$img=$oPath->manage("admin_bundle/data/images/{$ret['id']}.png");
				else
					$img='';
				$ret['image']=$img;
				$ret=(object)$ret;
			}
			return $ret;
		}//------------------------------------------------------------------------------------------------------
		public function getGeneralManager()
		{
			$ret=[];
			global $oDbq;
			$item=$oDbq->table($this->tbl_admin)->fields("*")->where("`generalManager`=1")->select()[0];
			$itemProfile=@$oDbq->table($this->tbl_usersProfile)->fields("*")->where("`userId`='{$item->id}' AND `userName`='{$item->userName}' AND `userType`='admin'")->select()[0];
			
			if(!$itemProfile)
			{
				$oDbq->table($this->tbl_usersProfile)->set("`userId`={$item->id},`userName`='{$item->userName}',`userType`='admin'")->insert();				
			   $itemProfile=@$oDbq->table($this->tbl_usersProfile)->fields("*")->where("`userId`='{$item->id}' AND `userType`='admin'")->select()[0];
			}
			
			$ret["admin"]=$item;
			$ret["profile"]=$itemProfile;
         return $ret;
		}//------------------------------------------------------------------------------------------------------		
		function delete($id) //delete one admin by id
		{
			global $oDbq;
			$oDbq->table($this->tbl_admin)->where("`id`={$id}")->delete();
			$oDbq->table($this->tbl_usersProfile)->where("`userId`={$id}")->delete();
		}//--------------------------------------------------------------------------
		function trash($id) //trash one admin by id 
		{
			global $oDbq;
			$oDbq->table($this->tbl_admin)->set("`trash`='1'")->where("`id`={$id}")->update();
		}//--------------------------------------------------------------------------
		function unTrash($id) //unTrash one admin by id 
		{
			global $oDbq;
			$oDbq->table($this->tbl_admin)->set("`trash`='0'")->where("`id`={$id}")->update();
		}//--------------------------------------------------------------------------		
      function login($userName,$password)
		{
			global $oDbq;
			//echo password_hash("123456",PASSWORD_BCRYPT) . "   ";
         
			$ret=$oDbq->table($this->tbl_admin)->fields('*')->where("`active`='1' AND `trash`=0 AND `userName`='{$userName}'")->select();
			if(count($ret) > 0)
			{
			   $verify=password_verify($password,$ret[0]->password);	
				if($verify)
				{
					$retProfile=$oDbq->table($this->tbl_usersProfile)->fields('*')->where("`userId`='{$ret[0]->id}'")->select();
				   $_SESSION["admin_login"]=true;
					$_SESSION["admin_userName"]=$ret[0]->userName;
					$_SESSION["admin_id"]=$ret[0]->id;
					$_SESSION['admin_generalManager']=$ret[0]->generalManager;
					$_SESSION["admin_fName"]=$retProfile[0]->fName;
					$_SESSION["admin_lName"]=$retProfile[0]->lName;
					$_SESSION["admin_mobile"]=$retProfile[0]->mobile;
					$_SESSION["admin_mail"]=$retProfile[0]->mail;
					return 'ok';
				}
				else
					return '!password';
			}
			else
				return '!userName';
			
		}//-----------------------------------------------------------------------------------------------------------
		function logout()
		{
			@session_start();
			$_SESSION["admin_login"]=false;
			unset($_SESSION["admin_userName"],
					$_SESSION["admin_id"],
					$_SESSION["admin_fName"],
					$_SESSION["admin_lName"],
					$_SESSION["admin_tell"],
					$_SESSION["admin_mail"]
					);
		}//-----------------------------------------------------------------------		
      function insert($arryArg)   //insert one admin and profile
		{
         global $oDbq;
			$id=time();
			$password=password_hash($arryArg['password'],PASSWORD_BCRYPT);
			$oDbq->table($this->tbl_admin)->set("`id`={$id},
													 `userName`='{$arryArg['userName']}',
													 `password`='{$password}',
													 `accessList`='{$arryArg['accessList']}'
													")->insert();
			$oDbq->table($this->tbl_usersProfile)->set("`id`={$id},
													       `userType`='admin',
													       `userId`='{$id}',
															 `fName`='{$arryArg['fName']}',
															 `lName`='{$arryArg['lName']}',
															 `phone`='{$arryArg['phone']}',
															 `address`='{$arryArg['address']}'
													")->insert();
			
		}//-----------------------------------------------------------------------	
      function update($arryArg)  //update one admin and profile by id 
		{
         global $oDbq;
			$id=$arryArg['id'];
			if($arryArg['password'])
			{
			   $password=password_hash($arryArg['password'],PASSWORD_BCRYPT);
            $oDbq->table($this->tbl_admin)->set("`password`='{$password}'")->where("`id`={$id}")->update();				
			}
			if(isset($arryArg['userName']))
			{
            if($arryArg['userName']) $oDbq->table($this->tbl_admin)->set("`userName`='{$arryArg['userName']}'")->where("`id`={$id}")->update();				
			}
			$oDbq->table($this->tbl_admin)->set("`accessList`='{$arryArg['accessList']}'")->where("`id`={$id}")->update();
			$oDbq->table($this->tbl_usersProfile)->set("`fName`='{$arryArg['fName']}',
															       `lName`='{$arryArg['lName']}',
															       `phone`='{$arryArg['phone']}',
															       `address`='{$arryArg['address']}'
													           ")->where("`userId`={$id}")->update();
			
		}//-----------------------------------------------------------------------	
		function setActive($id,$value) //set active=$value
		{
			global $oDbq;
			$oDbq->table($this->tbl_admin)->set("`active`='{$value}'")->where("`id`={$id}")->update();
		}//--------------------------------------------------------------------------
		function getActive($id)
		{
			global $oDbq;
			return @$oDbq->table($this->tbl_admin)->fields("*")->where("`id`={$id}")->select()[0]->active;
		}//--------------------------------------------------------------------------
		function getAccessList($id)
		{
			global $oDbq;
			return explode('|',@$oDbq->table($this->tbl_admin)->fields("*")->where("`id`={$id}")->select()[0]->accessList);
		}//--------------------------------------------------------------------------
		public function isAllowed($urlOrIdname,$byUrl=true) //check login and pageAllow
		{
			@session_start();
			global $oDbq;
			if(isset($_SESSION['admin_login']))
			{
				if($_SESSION['admin_login']==true)
				{
					if($_SESSION['admin_generalManager']==1) 
						return true;
					else
					{
						$oAdminMenu=new cAdminMenu();
						$admin=$oDbq->table($this->tbl_admin)->fields("`accessList`")->where("`id`={$_SESSION['admin_id']}")->select()[0];
						$accessLiastArry=explode('|',$admin->accessList);
						if($byUrl==true)
						   return $oAdminMenu->checkAllowByUrl($urlOrIdname,$accessLiastArry);
						else
						   return $oAdminMenu->checkAllowByIdname($urlOrIdname,$accessLiastArry);	
					}
				}
				else
					return false;
			}
			else
				return false;
		}//--------------------------------------------------------------------------		
   }
?>
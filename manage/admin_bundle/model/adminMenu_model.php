<?php
include_once $oPath->manageDir("admin_bundle/model/admin_model.php");
class cAdminMenu
{
   private $tbl_adminMenu="adminMenu"; //Accesslevel
   function getAll($sortByRow=true,$active="1",$limitStr="")
   {
      global $oDb;
      global $oDbq;
      $where="";
      if($active!="")
      {
         if($where=="") $where="`active`='{$active}'"; else $where.=" AND `active`='{$active}'";
      }
		if($sortByRow==true) $order="`id`"; else $order="`row`";
		$ret=$oDbq->table($this->tbl_adminMenu)->fields('*')->where($where)->limit($limitStr)->orderBy($order)->select();
      return $ret;
   }//--------------------------------------------------------------------------
   function get($id)
   {
      global $oDb;
      global $oDbq;
      $ret=$oDbq->table($this->tbl_adminMenu)->fields("*")->where("`id`='{$id}'")->select()[0];
      return $ret;
   }//--------------------------------------------------------------------------
   function getListArray()
   {
      $menus=$this->getAll(false);
      foreach($menus as $menu)
      {
         $ret[]=$menu->id;
      }
      return $ret;
   }//---------------------------------------------------------------------------
   function getByParentId($parentId=0,$visible="")
   {
      global $oDbq;
      $where="";
      if($visible!=='')
      {
         if($where=='') $where="`visible`='{$visible}' AND "; else $where.=" AND `visible`='{$visible}'";
      }
		$ret=$oDbq->table($this->tbl_adminMenu)->fields('*')->where("{$where}`trash`=0 AND `active`=1 AND `parentId`='{$parentId}'")->orderby("row")->select();		
      return $ret;
   }//--------------------------------------------------------------------------
   public function getByUrl($url)
   {
      if($url != "login")
      {
			global $oDbq;
         $ret=@$oDbq->fields('*')->table($this->tbl_adminMenu)->where("`trash`=0 AND `active`=1 AND `url`='{$url}'")->select()[0];
         return $ret;
      }
   }//--------------------------------------------------------------------------
   public function getByIdname($idName)
   {
		global $oDbq;
		$ret=@$oDbq->fields('*')->table($this->tbl_adminMenu)->where("`trash`=0 AND `active`=1 AND `idName`='{$idName}'")->select()[0];
		return $ret;
   }//--------------------------------------------------------------------------
   public function getByPageindex()
   {
		global $oDbq;
		$ret=@$oDbq->fields('*')->table($this->tbl_adminMenu)->where("`trash`=0 AND `active`=1 AND `pageIndex`='1'")->select()[0];
		return $ret;
   }//--------------------------------------------------------------------------
   function delete($id)
   {
      global $oDb;
      global $oDbq;
      $oDbq->table($this->tbl_adminMenu)->where("`id`={$id}")->delete();
   }//--------------------------------------------------------------------------
	public function checkAllowByUrl($url,$accessListArry) //check pageAllow by url
	{
		@session_start();
		global $oDbq;
		$id=@$oDbq->fields('*')->table($this->tbl_adminMenu)->where("`trash`=0 AND `active`=1 AND `url`='{$url}'")->select()[0]->id;
		if(!$id) return false;
		else
		{
			if(strlen(array_search($id,$accessListArry)) > 0)
				return true;
			else
				return false;
		}
	}//--------------------------------------------------------------------------	
	public function checkAllowByIdName($idName,$adminId) //check pageAllow by idName
	{
		global $oDbq;
		$oAdmin=new cAdmin();
		$admin=$oAdmin->get($adminId)['admin'];
		if($admin->generalManager==1) 
			return true;
		else
		{
			$menu=@$oDbq->fields('*')->table($this->tbl_adminMenu)->where("`trash`=0 AND `active`=1 AND `idName`='{$idName}'")->select();
			if(!$menu) 
				return false;
			else
			{
				if(strlen(array_search($menu[0]->id,$admin->accessListArry)) > 0)
					return true;
				else
					return false;
			}
		}
	}//--------------------------------------------------------------------------	
   function runFirstCode()
   {
      $menus=$this->getAll();
      $oAdmin=new cAdmin();
      for($i=0;$i < count($menus);$i++)
      {
         $page=$menus[$i]->url;
         if($oAdmin->isAllowed($page))
            @include_once "../firstCode/" . $page . ".php";
      }
   }//------------------------------------------------------------------------------------
}

?>
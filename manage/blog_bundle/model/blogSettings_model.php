<?php

class cBlogSettings
{
   public $tbl_blogSettings="blogSettings";
   function get()
   {
      global $oDbq;
		$ret=@$oDbq->table($this->tbl_blogSettings)->select()[0];
		if(!$ret)
		{
		   $oDbq->table($this->tbl_blogSettings)->set("`activeOpine`=1")->insert();
			$ret=$oDbq->table($this->tbl_blogSettings)->select()[0];
      }	
		return $ret;
   }//--------------------------------------------------------------------------
   function update ($activeOpine, $showDate)
	{
		global $oDbq;
      $oDbq->table($this->tbl_blogSettings)->set("`activeOpine`={$activeOpine}, `showDate`={$showDate}")->update();	   
	}//-------------------------------------------------------------------------- 
}

?>
<?php

class cBlogGroup
{
   public $tbl_blogGroup="blogGroup";
	
	private $ids="";
   function getAll($sortByRow=true,$parentId="",$active="",$limitStr="")
   {
      global $oDbq;
      $where="";
      if(intval($parentId) > -1 && strlen($parentId) > 0)
      {
         if($where=="") $where="`parentId`='{$parentId}'"; else $where.=" AND `parentId`='{$parentId}'";
      }
      if($active!="")
      {
         if($where=="") $where="`active`='{$active}'"; else $where.=" AND `active`='{$active}'";
      }
      if($sortByRow==true)$ret=$oDbq->table($this->tbl_blogGroup)->fields("*")->where($where)->orderBy("`row`");
      else if($sortByRow==false)$ret=$oDbq->table($this->tbl_blogGroup)->fields("*")->where($where);
      if($limitStr!=="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//--------------------------------------------------------------------------
   function get($id)
   {
      global $oDbq;
		$ret=@$oDbq->table($this->tbl_blogGroup)->fields("*")->where("`id`='{$id}'")->select()[0];
		return $ret;
   }//--------------------------------------------------------------------------
   function existsId($id)
   {
      global $oDbq;
      return count($oDbq->table($this->tbl_blogGroup)->fields("*")->where("`id`='{$id}'")->select());
   }//--------------------------------------------------------------------------	
	function eachId($id)
	{
		global $oDbq;
	   $id_=@$oDbq->table($this->tbl_blogGroup)->fields("`id`")->where("`parentId`='{$id}'")->select()[0]->id;
		if($id_)
		{
			$this->ids.=$id_ . ",";
			$this->eachId($id_);	
		}
		$ret=$this->ids;
		//$this->ids="";
      return rtrim($ret,",");		
	}//--------------------------------------------------------------------------
   function hasChilds($id)
   {
      global $oDbq;
      $ret=$oDbq->table($this->tbl_blogGroup)->fields("*")->where("`parentId`='{$id}'")->select();
      if(count($ret) > 0) return true; else return false; 
   }//--------------------------------------------------------------------------	
   function countChilds($id)
   {
      global $oDbq;
      $ret=$oDbq->table($this->tbl_blogGroup)->fields("*")->where("`parentId`='{$id}'")->select();
      return count($ret);
   }//--------------------------------------------------------------------------	
   function countChildItems($id)
   {
      global $oDbq;
      $ret=$oDbq->table($this->itemsTbl)->fields("*")->where("`otherSids` LIKE '%{$id}%' AND `active`='1'")->select();
      return count($ret);
   }//--------------------------------------------------------------------------	
   function delete($ids)
   {
      global $oDbq;
		$ids=rtrim($ids,",");
		$ids=explode(",",$ids);
		for($i=0;$i < count($ids);$i++)
		{
			$id=$ids[$i];
			$oDbq->table($this->tbl_blogGroup)->where("`id`='{$id}'")->delete();
		}
      return $ids; //return array		
   }//--------------------------------------------------------------------------
   function getChildsId($id)  //list hame menuha be soorate derakhty
   {
      $items=$this->gets(true,$id);
      $code="";
      if(count($items) > 0) 
      {
         foreach($items as $item)
         {
				$code.=$item->id . ",";
				$code.=$this->getChildsId($item->id);
         }
         return $code;
      }
      else
         return "";
   }//------------------------------------------------------------------------------------
	function setActive($id,$value)
	{
	  global $oDbq;
      $oDbq->table($this->tbl_blogGroup)->fields("`active`")->values($value)->where("id=$id")->update(); 		
	}//--------------------------------------------------------------------------
    function insert ($array)
	{
		global $oDbq;
	   $id=$array["groupId"];
	   $parentId=$array["parentId"];
	   $title=$array["title"];
	   $comment=$array["comment"];
      $oDbq->table($this->tbl_blogGroup)->set("`id`={$id}, `parentId`={$parentId}, `title`='{$title}', `comment`='{$comment}'")->insert();	   
	}//--------------------------------------------------------------------------
   function update ($array)
	{
		global $oDbq;
	   $id=$array["groupId"];
	   $parentId=$array["parentId"];
	   $title=$array["title"];
	   $comment=$array["comment"];
      $oDbq->table($this->tbl_blogGroup)->set("`title`='{$title}', `comment`='{$comment}'`")->where("`id`={$id}")->update();	   
	}//-------------------------------------------------------------------------- 
}

?>
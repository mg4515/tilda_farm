<?php
class cBlogItems
{
   private $tbl_blogItems="blogItems";
   private $tbl_blogItemOpine="blogItemOpine";
	
	function getAll($argArray)
   {
		global $oDbq;
		
		$groupId=@$argArray["groupId"] ? $argArray["groupId"] : "";
		$sortByRow=@$argArray["sortByRow"] ? $argArray["sortByRow"] : true;
		$sortByVisits=@$argArray["sortByVisits"] ? $argArray["sortByVisits"] : false;
		$searchWord=@$argArray["searchWord"] ? $argArray["searchWord"] : "";
		$active=@$argArray["active"] ? $argArray["active"] : "";
		$trash=@$argArray["trash"] ? $argArray["trash"] : "";
		$limitStr=@$argArray["limitStr"] ? $argArray["limitStr"] : "";	

      $where="";
      if($groupId!=="")
      {
         if($where=="") $where="`groupId` LIKE '%{$groupId}%'"; else $where.=" AND `groupId` LIKE '%{$groupId}%'";
      }	
		if($searchWord !== "")
		{
			if($where=="") 
				$where="(`title` LIKE '%{$searchWord}%' OR `comment` LIKE '%{$searchWord}%' OR `keywords` LIKE '%{$searchWord}%')";
		   else
				$where.=" AND (`title` LIKE '%{$searchWord}%' OR `comment` LIKE '%{$searchWord}%' OR `keywords` LIKE '%{$searchWord}%')";
		}
      if($active!=="")
      {
         if($where=="") $where="`active`='{$active}'"; else $where.=" AND `active`='{$active}'";
      }
      if($trash!=="")
      {
         if($where=="") $where="`trash`='{$trash}'"; else $where.=" AND `trash`='{$trash}'";
      }						
		
      if($sortByRow==true)$ret=$oDbq->table($this->tbl_blogItems)->fields("*")->where($where)->orderBy("`id` DESC");
		else if($sortByVisits==true) $ret=$oDbq->table($this->tbl_blogItems)->fields("*")->where($where)->orderBy("`visits` DESC");      
		else $ret=$oDbq->table($this->tbl_blogItems)->fields("*")->where($where)->orderBy("`id` DESC");  
      
		if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//--------------------------------------------------------------------------
   function getByGroupId($groupId,$active='',$trash='') //=
   {
      global $oDbq;
		$where="`groupId`={$groupId}";
		if($active!=='')
			if($where=='') $where="`active`={$active}"; else $where.=" AND `active`={$active}";
		if($trash!=='')
			if($where=='') $where="`trash`={$trash}"; else $where.=" AND `trash`={$trash}";
		$ret=$oDbq->table($this->tbl_blogItems)->fields("*")->where($where)->select();
      return $ret;
   }//--------------------------------------------------------------------------
   function getByGroupId2($groupId,$active='',$trash='') //like
   {
      global $oDbq;
		$where="`groupId` LIKE '%{$groupId}%'";
		if($active!=='')
			if($where=='') $where="`active`={$active}"; else $where.=" AND `active`={$active}";
		if($trash!=='')
			if($where=='') $where="`trash`={$trash}"; else $where.=" AND `trash`={$trash}";
		$ret=$oDbq->table($this->tbl_blogItems)->fields("*")->where($where)->select();
      return $ret;
   }//--------------------------------------------------------------------------	
	function get($id,$active='',$trash='')
   {
      global $oDbq;
		$where="`id`={$id}";
		if($active!=='')
			if($where=='') $where="`active`={$active}"; else $where.=" AND `active`={$active}";
		if($trash!=='')
			if($where=='') $where="`trash`={$trash}"; else $where.=" AND `trash`={$trash}";
      $ret=@$oDbq->table($this->tbl_blogItems)->fields("*")->where($where)->select()[0];
      return $ret;
   }//--------------------------------------------------------------------------
	function delete($id,$userId="")
	{
		global $oDbq;
		if($userId!="") $where="`userId`={$userId} AND "; else $where="";
		$where2="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				$where2.="(`itemId`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
				if($i < count($ids) - 1) $where2.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_blogItems)->fields("*")->where("({$where})")->delete();
	   if($where != "" && $where2 != "") $oDbq->table($this->tbl_blogItemOpine)->fields("*")->where("({$where2})")->delete();		
		return $ids;		
	}//---------------------------------------------------------------------------
	function trash($id,$userId="")
	{
		global $oDbq;
		if($userId!="") $where="`userId`={$userId} AND "; else $where="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_blogItems)->set("`trash`=1")->where("({$where})")->update();
		return $ids;		
	}//---------------------------------------------------------------------------
	function unTrash($id,$userId="")
	{
		global $oDbq;
		if($userId!="") $where="`userId`={$userId} AND "; else $where="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_blogItems)->set("`trash`=0")->where("({$where})")->update();
		return $ids;		
	}//---------------------------------------------------------------------------		
	public function getActive($id)
	{
		global $oDbq;
		$ret=@$oDbq->table($this->tbl_blogItems)->fields("`active`")->where("`id`={$id}")->select()[0]->active;
		if($ret=="1")
			return true;
		else
			return false;
	}//---------------------------------------------------------------------------------
	public function setActive($id,$value)
	{
		global $oDbq;
		$oDbq->table($this->tbl_blogItems)->set("`active`={$value}")->where("`id`={$id}")->update();
	}//---------------------------------------------------------------------------------
	public function getTrash($id)
	{
		global $oDbq;
		$ret=@$oDbq->table($this->tbl_blogItems)->fields("`trash`")->where("`id`={$id}")->select()[0]->active;
		if($ret=="1")
			return true;
		else
			return false;
	}//---------------------------------------------------------------------------------
	public function setTrash($id,$value)
	{
		global $oDbq;
		$oDbq->table($this->tbl_blogItems)->set("`trash`={$value}")->where("`id`={$id}")->update();
	}//---------------------------------------------------------------------------------	
   function setVisits($id,$value)
   {
      global $oDbq;
		if($value=='+') $set="`visits`=`visits` + 1";
		else if($value=='-') $set="`visits`=`visits` - 1";
		else $set="`visits`='{$value}'";
		$oDbq->table($this->tbl_blogItems)->set($set)->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------	
   function getVisits($id)
   {
      global $oDbq;
		return @$oDbq->table($this->tbl_blogItems)->fields("`visits`")->where("`id`='{$id}'")->select()[0]->visits;
   }//------------------------------------------------------------------------------------
   function getDateIfAllow($itemId) //tarikhe ijade matlab bar asase timestamp, if showDate==true then
	{
		$oBlogSettings=new cBlogSettings();
		$blogSettings=$oBlogSettings->get();
		if($blogSettings->showDate=='1')
		{
			$item=$this->get($itemId);
         return $item->insertDate;
		}
      else
         return -1;			
	}//--------------------------------------------------------------------------		
   function insert($array)
	{
      global $oDbq;
		$id=time();
		$insertById=(int)@$array['insertById'] ? $array['insertById'] : 0;
		$insertDate=time();
      $oDbq->table($this->tbl_blogItems)->set("`id`={$id},
		                                       `groupId`='{$array['groupId']}',
		                                       `title`='{$array['title']}',												
															`comment`='{$array['comment']}',												
															`keywords`='{$array['keywords']}',												
															`activeOpine`='{$array['activeOpine']}',												
		                                       `insertById`={$insertById},
		                                       `insertDate`={$insertDate}
		                                      ")->insert();	
      return $id;		
	}//--------------------------------------------------------------------------
   function update($array)
	{
      global $oDbq;
		$updateById=(int)@$array['updateById'] ? $array['updateById'] : 0;
		$updateDate=time();
      $oDbq->table($this->tbl_blogItems)->set(" `groupId`='{$array['groupId']}',
															   `title`='{$array['title']}',											
																`comment`='{$array['comment']}',												
																`keywords`='{$array['keywords']}',
																`activeOpine`='{$array['activeOpine']}',
																`updateById`={$updateById},
																`updateDate`={$updateDate}
															")->where("`id`={$array['id']}")->update();	
	}//--------------------------------------------------------------------------		
   function isActiveOpine($itemId) //nazardehy faal ast ya kheir?
	{
		$oBlogSettings=new cBlogSettings();
		$blogSettings=$oBlogSettings->get();
		$item=$this->get($itemId);
		if($blogSettings->activeOpine=='1' || ($blogSettings->activeOpine=='3' && $item->activeOpine=='1'))
         return true;
      else
         return false;			
	}//--------------------------------------------------------------------------	
   function opine_getAll($itemId="",$confirm="",$checkOut="",$limitStr="")
   {
      global $oDbq;
      $where="";
      if($itemId!=="")
      {
         if($where=="") $where="`itemId` = '{$itemId}'"; else $where.=" AND `itemId`='{$itemId}'";
      }
      if($confirm!=="")
      {
         if($where=="") $where="`confirm`='{$confirm}'"; else $where.=" AND `confirm`='{$confirm}'";
      }
      if($checkOut!=="")
      {
         if($where=="") $where="`checkOut`='{$checkOut}'"; else $where.=" AND `checkOut`='{$checkOut}'";
      }
		
      $ret=$oDbq->table($this->tbl_blogItemOpine)->fields("*")->where($where);
      if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//------------------------------------------------------------------------------------	
   function opine_getByUserId($userId="",$confirm="",$checkOut="",$limitStr="")
   {
      global $oDbq;
      $where="";
      if($userId!="")
      {
         if($where=="") $where="`userId` = '{$userId}'"; else $where.=" AND `userId`='{$userId}'";
      }
      if($confirm!="")
      {
         if($where=="") $where="`confirm`='{$confirm}'"; else $where.=" AND `confirm`='{$confirm}'";
      }
      if($checkOut!="")
      {
         if($where=="") $where="`checkOut`='{$checkOut}'"; else $where.=" AND `checkOut`='{$checkOut}'";
      }
		
      $ret=$oDbq->table($this->tbl_blogItemOpine)->fields("*")->where($where);
      if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//------------------------------------------------------------------------------------	
   function opine_get($id,$itemId="")
   {
      global $oDbq;
		if($itemId!="") $where=" AND `itemId`={$itemId}"; else $where="";
		return $oDbq->table($this->tbl_blogItemOpine)->fields("*")->where("`id`={$id}{$where}")->select()[0];
   
	}//------------------------------------------------------------------------------------   
	function opine_delete($id)
   {
      global $oDbq;
		$where="";
		$ids=explode(",",rtrim($id,","));
		$ids2=[];
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
				$ids2[]=$selectedId;
			}
		}
		if($where) $oDbq->table($this->tbl_blogItemOpine)->fields("*")->where("({$where})")->delete();
		return $ids2;
   }//------------------------------------------------------------------------------------
	function opine_deleteByItemId($itemId,$retIds=true)
	{
		global $oDbq;
		if($retIds)
		   $ret=$oDbq->table($this->tbl_blogItemOpine)->fields('`id`')->where("`itemId`={$itemId}")->select();
	   else
			$ret=[];
		$oDbq->table($this->tbl_blogItemOpine)->where("`itemId`={$itemId}")->delete();
	   return $ret;
	}//------------------------------------------------------------------------------------
   function opine_insert($userId,$itemId)
   {
      global $oDbq;
		$id=time();
		$oDbq->table($this->tbl_blogItemOpine)->set("`id`='{$id}',`itemId`='{$itemId}',`userId`='{$userId}'")->insert();
		return $id;
   }//------------------------------------------------------------------------------------
   function opine_setConfirm($id,$value)
   {
      global $oDbq;
		$oDbq->table($this->tbl_blogItemOpine)->set("`confirm`='{$value}'")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------	
	function opine_getConfirm($id)
	{
		global $oDbq;
		$ret=@$oDbq->table($this->tbl_blogItemOpine)->fields("`confirm`")->where("`id`={$id}")->select()[0];
		if($ret)
		{
			if($ret->confirm=="1")
				return true;
			else
				return false;
		}
		else
		   return false;
	}//---------------------------------------------------------------------------------	
   function opine_setCheckOut($id,$value)
   {
      global $oDbq;
		$oDbq->table($this->tbl_blogItemOpine)->set("`checkOut`='{$value}'")->where("`id`='{$id}'")->update();
   }//------------------------------------------------------------------------------------ 
}

?>
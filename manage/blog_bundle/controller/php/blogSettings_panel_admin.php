<?php
   @session_start();
	
	//request
	include_once $_SESSION["engineRequire"];//engine.php
	require_once $oPath->manageDir("blog_bundle/model/blogSettings_model.php");		

	//object
	$oBlogSetting=new cBlogSettings();	

   //var
	$arryActiveOpine=[1=>'فعال', 2=>'غیر فعال', 3=>'بر اساس تنظیمات مطالب'];	
	
	//request
   $request=$_REQUEST["requestName"];
	
   if($request=="blogSettings_edit")
   {
      $item=$oBlogSetting->get();
 
		//activeOpine
      if($item->activeOpine=="1") $chk_activeOpine="checked='checked'"; else $chk_activeOpine="";		
		$slct_activeOpine='';
		foreach($arryActiveOpine as $key=>$val)
		{
			if($item->activeOpine==$key) $selected='selected'; else $selected='';
			$slct_activeOpine.="<option value='{$key}' {$selected}>{$val}</option>";
		}
		$slct_activeOpine="<select id='slct_activeOpine'>{$slct_activeOpine}</select>";

      //showDate
		if($item->showDate == 1) $chk_showDate='checked'; else $chk_showDate='';
		
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil-square-o'></i>&nbsp;تنظیمات وبلاگ</h1>		
		<div class='vSpace-4x'></div>
		     
		<div class='form dir-rtl algn-r content'>
			<label><i class='fa fa-circle'></i>امکان ثبت نظر</label>
			{$slct_activeOpine}
			
			<div class='vSpace'></div>
			
			<label>
			   <i class='fa fa-circle'></i>
				<input type='checkbox' id='chk_showDate' {$chk_showDate}>
				نمایش تاریخ مطالب در سایت
			</label>
			
      </div>
		
      <div class='vSpace-4x'></div>
		
		<hr>
		<button class='btn btn-success' onclick='blogSettings_update();'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
		
		<div class='vSpace-4x'></div>
      ";
      cEngine::response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($request=="blogSettings_update")
   {
      $activeOpine=cDataBase::escape($_REQUEST['activeOpine']);
      $showDate=cTools::misc_jsBln2PhpBln(cDataBase::escape($_REQUEST['showDate']),true);
      $ret=$oBlogSetting->update($activeOpine, $showDate);
      cEngine::response("ok");
   }//------------------------------------------------------------------------------------
?>
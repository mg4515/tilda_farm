<?php
   include_once $oPath->manageDir("blog_bundle/model/blogGroup_model.php");
   include_once $oPath->manageDir("blog_bundle/model/blogItems_model.php");
   include_once $oPath->manageDir("blog_bundle/model/blogSettings_model.php");
   include_once $oPath->manageDir("users_bundle/model/users_model.php");
   include_once $oPath->manageDir("jdf.php");

	class cBlogDraw
	{
		function groupLi()
		{
			global $oPath;
			$oBlogGroup=new cBlogGroup();
			$ret=$oBlogGroup->getAll("","","1");
			$count=count($ret);
			$code='';
			for($i=0;$i < $count;$i++)
			{
				$code.="<li value='{$ret[$i]->id}'><a href='blogs?group={$ret[$i]->id}'>{$ret[$i]->title}</a></li>";
			}
			return $code;
		}//------------------------------------------------------------------------------------		
		
		//for index
		function itemsDraw_forIndex($count=3) 
		{
			global $oPath;
			$oBlogItems=new cBlogItems();
			$ret=$oBlogItems->getAll(['sortByRow'=>true, 'active'=>1, 'limitStr'=>$count]);
			$count=count($ret);
			$code='';
			for($i=0;$i < $count;$i++)
			{
				$item=$ret[$i];
			
				//image info
				if(file_exists($oPath->manageDir("blog_bundle/data/images/item_{$item->id}.jpg")))
				{
					$img="<img class='default-img' src='" . $oPath->manage("blog_bundle/data/images/item_{$item->id}.jpg") ."' alt='{$item->title}' style='' />";
				   $imgSrc=$oPath->manage("blog_bundle/data/images/item_{$item->id}.jpg");
				}
				else
				{
					$img="";
					$imgSrc=$oPath->asset("default/images/noImage.gif");
				}
				
				$description=mb_substr($item->comment,0,200) . ' ...';
				
				//date
				$date=jDate("Y/m/d", $item->insertDate);
				
				$code.="
				<aside class='col-md-4 mb-3 mb-md-0'>
				   <div class='card border-0 h-100'>
						<a class='text-decoration-none' href='blog?i={$item->id}'>
						   <div class='row no-gutters'>
							   <div class='col-md-4'>
									<!-- Post Image -->
									<img src='{$imgSrc}' class='card-img' alt='{$item->title}' />
							   </div>
							   <!-- Texts -->
							   <div class='col-md-8 d-flex align-items-center'>
									<div class='card-body p-2'>
									   <!-- Title -->
									   <h5 class='card-title ir-b c-dark d-block mb-2 fs-small text-right'>{$item->title}</h5>
									   <p class='card-text d-block ir-r fs-small c-regular mb-2 text-right'>
										   {$description}
									   </p>
									   <small class='text-muted ir-r fs-small text-right mb-0 d-block font-italic'>{$date}</small>
									</div>
							   </div>
						   </div>
						</a>
				   </div>
				</aside>			
				";
			}
			return $code;
		}//------------------------------------------------------------------------------------ 
      public function item($id)
		{
			global $oPath;
			$oBlogItems=new cBlogItems();
			$oBlogGroup=new cBlogGroup();
			$oBlogSettings=new cBlogSettings(); 
			$oUsers=new cUsers();
			
			//item
			$item=(array)$oBlogItems->get($id);
			if($item)
			{
				//image info -----
				if(file_exists($oPath->manageDir("blog_bundle/data/images/itemLarg_{$item['id']}.jpg")))
					$item['image']=$oPath->manage("blog_bundle/data/images/itemLarg_{$item['id']}.jpg");
				else
					$item['image']=$oPath->asset("default/images/noImage.gif");
				
				//group or groups -----
				$groupId=explode('|', $item['groupId'])[0];
				$group=$oBlogGroup->get($groupId);
				$item['groupTitle']=$group->title;

				//tags -----
				$keywords=explode('،',$item['keywords']);
				$count=count($keywords);
				$tags='';
				for($i=0;$i < $count;$i++)
				{
					$tagTitle=str_replace(' ','_',$keywords[$i]);
					$tags.="<li><a href='blog?i={$item['id']}&t={$tagTitle}'>{$keywords[$i]}</a></li>";
				}
				$item['tags_li']=$tags;
				
				//opine -----
				$blogSettings=$oBlogSettings->get();
				if($oBlogItems->isActiveOpine($item['id']))
				{
					$opine=$oBlogItems->opine_getAll($item['id'],1);
					$count=count($opine);
					$opineDiv='';
					for($i=0;$i < $count;$i++)
					{
						$user=$oUsers->get($opine[$i]->userId);
						if($user)
						{
							if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$user->id}.jpg")))
								$userImage=$oPath->manage("users_bundle/data/images/user_{$user->id}.jpg");
							else
								$userImage=$oPath->asset("default/images/user_larg.png");
							$opineDate=jDate('Y/m/d',$opine[$i]->id);
							$opineContent=file_get_contents($oPath->manageDir("blog_bundle/data/contents/itemOpineContent_{$opine[$i]->id}.html"));
							$opineAnswer=@file_get_contents($oPath->manageDir("blog_bundle/data/contents/itemOpineAnswer_{$opine[$i]->id}.html"));
							if($opineAnswer) $opineAnswer='<hr class="my-3" />پاسخ :<br>' . $opineAnswer;
							$opineDiv.="
					      <div class='row'>
						      <span class='d-block ir-r fs-regular text-right c-regular mb-3'>{$user->fName} {$user->lName}</span>
								<p class='d-block ir-r fs-regular text-justify c-regular mb-3'>
									{$opineContent}{$opineAnswer}
								</p>
						      <i class='d-block ir-r fs-small text-right c-grey mb-0 font-italic'>{$opineDate}</i>
					      </div>							
							<hr class='my-3' />
							";
						}
					}
					$item['opineCount']=$count;
					$item['opine_div']=$opineDiv;
				}
				else
				{
					$item['opineCount']=-1;
					$item['opine_div']='';
				}
				
				//mortabet ha -----
				$mortabet=$oBlogItems->getAll(['groupId'=>$item['groupId'], 'active'=>1, 'limitStr'=>4]);
				$count=count($mortabet);
				$mortabetDiv='';
				for($i=0;$i<$count;$i++)
				{
					if($mortabet[$i]->id != $item['id']) //check exists
					{
						if(file_exists($oPath->manageDir("blog_bundle/data/images/itemThumb_{$mortabet[$i]->id}.jpg")))
							$mortabetImg=$oPath->manage("blog_bundle/data/images/itemThumb_{$mortabet[$i]->id}.jpg");
						else
							$mortabetImg=$oPath->asset("default/images/noImage.gif");
						$mortabetDate=jDate("Y/m/d",$mortabet[$i]->id);
						if($oBlogItems->isActiveOpine($mortabet[$i]->id))
						   $mortabetOpine=count($oBlogItems->opine_getAll($mortabet[$i]->id,1));
						else
							$mortabetOpine=-1;
						$mortabetDiv.="
						<div class='single-post'>
							<div class='image'>
								<img src='{$mortabetImg}' alt='{$mortabet[$i]->title}'>
							</div>
							<div class='content'>
								<h5><a href='blog?i={$mortabet[$i]->id}'>{$mortabet[$i]->title}</a></h5>
								<ul class='comment'>
									<li><i class='fa fa-calendar' aria-hidden='true'></i>{$mortabetDate}</li>";
									if($mortabetOpine > -1)
										$mortabetDiv.="
									   <li><i class='fa fa-commenting-o' aria-hidden='true'></i>{$mortabetOpine}</li>";
								$mortabetDiv.="
								</ul>
							</div>
						</div>					
						";
					}
				}
				$item['mortabet_div']=$mortabetDiv;
				
				//visist
				$oBlogItems->setVisits($item['id'],'+');
				
				//date -----
				$date=$oBlogItems->getDateIfAllow($item['id']);
				if($date != -1)
				   $item['date']=jDate("Y/m/d",$date);
				else
					$item['date']='';
				
				$item['content']=base64_encode(file_get_contents($oPath->manageDir("blog_bundle/data/contents/item_{$item['id']}.html")));
			}
			return (object)$item;	
		}//------------------------------------------------------------------------------------
		function items($groupId='',$searchWord='',$page=1) 
		{
			//500 x 750
			global $oPath;
			$oBlogItems=new cBlogItems();
			$oBlogGroup=new cBlogGroup();
			
			$items=$oBlogItems->getAll(['sortByRow'=>true, 'active'=>1, 'groupId'=>$groupId, 'searchWord'=>$searchWord]);
			
			//pages
			$link="blogs?group={$groupId}&searchWord={$searchWord}";
			$count=count($items);
			if($page < 1) $page=1;
			$pages=cTools::misc_sqlLimit($count,10,10,$page);
			$btnPageCode="";
			for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn active'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn' href='{$link}&page={$i}' >{$i}</a>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages['isPrevious']) $btnPageCode="<a href='{$link}&page={$back}' class='btn'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages['isNext']) $btnPageCode=$btnPageCode . "<a href='{$link}&page={$next}' class='btn'><i class='fa fa-angle-left'></i></a>";						
			
			$items=$oBlogItems->getAll(['sortByRow'=>true, 'active'=>1, 'groupId'=>$groupId, 'searchWord'=>$searchWord, 'limitStr'=>$pages['sqlLimit']]);
		   $count=count($items);
			$code='';
			for($i=0;$i < $count;$i++)
			{
				$item=$items[$i];
			
				//image info
				if(file_exists($oPath->manageDir("blog_bundle/data/images/item_{$item->id}.jpg")))
				{
					$img="<img class='default-img' src='" . $oPath->manage("blog_bundle/data/images/item_{$item->id}.jpg") ."' alt='{$item->title}' style='' />";
				   $imgSrc=$oPath->manage("blog_bundle/data/images/item_{$item->id}.jpg");
				}
				else
				{
					$img="";
					$imgSrc=$oPath->asset("default/images/noImage.gif");
				}
				
				//date
				$date=jDate("Y/m/d", $item->insertDate);
				
				$code.="
				<div class='col-lg-4 col-md-6 col-12'>
					<div class='shop-single-blog'>
						<div class='img' style='background-image:url({$imgSrc});'>
						   {$img}
						</div>
						<div class='content'>
							<p class='date'>{$date}</p>
							<a href='blog?i={$item->id}' class='title'>{$item->title}</a>
							<a href='blog?i={$item->id}' class='more-btn'>نمایش</a>
						</div>
					</div>
				</div>			
				";
			}
			
			if($count == 0) $code='<h3 class="col-12" style="text-align:center;color:#ddd;font-size: 32px;"><br><br><i class="fa fa-info-circle fa-2x"></i>&nbsp;خالی است</h3>';
			$code.="
			<div class='btnPages col-12'>
			   <div class='container'>
					<div class='row'>		
						{$btnPageCode}
					</div>
				</div>
			</div>
			";
			
			$title='مطالب وبلاگ';
			if($searchWord && $groupId)
				$title.=" - جستجو '{$searchWord}' - " . 'دسته ' . @$oBlogGroup->get($groupId)->title;	
			else if($searchWord)
				$title.=" - جستجو '{$searchWord}'";				
			else if($groupId)
				$title.=' - دسته ' . @$oBlogGroup->get($groupId)->title;
			
			return ['items_div'=>$code, 'title'=>$title];
		}//------------------------------------------------------------------------------------ 		
	}
?>
<?php
   @session_start();

	//includes
	include_once $_SESSION["engineRequire"]; //engine.php
	require_once $oPath->manageDir("blog_bundle/model/blogGroup_model.php");

	//objects
	$oBlogGroup=new cBlogGroup();
	
	//request
	$request=@$_REQUEST["requestName"];
	
	//START GROUP
	if($request=="blogGroup_draw")
   {
		$parentId=cDataBase::escape(@$_REQUEST["parentId"]);
      
		//init session
		if($parentId=="-1") 
			$parentId=$_SESSION["blogGroup_parentId"]; //auto
		else
		   $_SESSION["blogGroup_parentId"]=$parentId;	
		
		if($parentId > 0)
		{
			$oldParent=$oBlogGroup->get($parentId);
			$oldParentTitle=" - زیر دسته های دسته '" . $oldParent->title . "'";
		}
		else
			$oldParentTitle="";
      $items=$oBlogGroup->getAll(true,$parentId);
		
      $codeTr="";
      $i=0;
      foreach($items as $item)
      {
         $i++;
         $groupId=$item->id;
			$countChilds=$oBlogGroup->countChilds($groupId);
         
			if($item->active=="0") $active="<button class='btn btn-danger' onclick='blogGroup_active({$groupId});'>غیر فعال</button>";
         elseif($item->active=="1") $active="<button class='btn btn-success' onclick='blogGroup_active({$groupId});'>فعال</button>";
			
         $codeTr.="
         <tr>
            <td style='text-align:center;padding:10px;width:100px;'>{$item->title}</td>
            <td>
					<span id='td_active_{$groupId}'>{$active}</span>
					<button class='btn btn-danger' onclick='blogGroup_del({$parentId},{$groupId});'><i class='fa fa-trash-o'></i></button>
					<button class='btn btn-info' onclick='blogGroup_edit({$parentId},{$groupId});'><i class='fa fa-pencil'></i></button>
         ";
			if($parentId == 0) 
			$codeTr.="
					&nbsp;|&nbsp;
               <button class='btn btn-label btn-label-right btn-warning' onclick='blogGroup_draw({$groupId});'>
                  <span>زیر دسته ها</span>
                  <span class='btn-label-caption'>{$countChilds}</span>
               </button>			
				</td>
         </tr>";
      }
		
		if($codeTr)
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;دسته بندی ها{$oldParentTitle}</h1>
			<div class='vSpace-4x'></div>
			";
			if($parentId > 0) $code.="<button class='btn' onclick='blogGroup_draw({$oldParent->parentId})'><i class='fa fa-arrow-right'></i></button>";
			$code.="<button type='button' class='btn btn-success hide' onclick='blogGroup_new({$parentId});'><i class='fa fa-plus'></i>&nbsp;افزودن</button>				
			<div class='vSpace'></div>		
			<table class='tbl tbl-right tbl-bordered tbl-hover'>
				<tr>
					<th>دسته</th>
					<th>عملیات</th>
				</tr>
				{$codeTr}
			</table>
			<div class='vSpace-4x'></div>
			";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;دسته بندی ها{$oldParentTitle}</h1>
			<div class='vSpace-4x'></div>
			<hr>
			";
			if($parentId > 0) $code.="<button class='btn' onclick='blogGroup_draw({$oldParent->parentId})'><i class='fa fa-arrow-right'></i></button>";
			$code.="<button type='button' class='btn btn-success' onclick='blogGroup_new({$parentId});'><i class='fa fa-plus'></i>&nbsp;افزودن</button>				
			<hr>			
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'></div>
			";
		}
		$oEngine->response("ok[|]" . $code);
   }//------------------------------------------------------------------------------------
   else if($request=="blogGroup_del")
   {
		$parentId=cDataBase::escape(@$_REQUEST["parentId"]);
      $groupId=cDataBase::escape(@$_REQUEST["id"]);
      
		$oBlogGroup->delete($groupId);
		
		//chids delete
		$childsId=$oBlogGroup->eachId($groupId);
		$idsArray=$oBlogGroup->delete($childsId);
		
		$oEngine->response("ok[|]{$parentId}");
   }//------------------------------------------------------------------------------------
   else if($request=="blogGroup_edit")
   {
		$parentId=cDataBase::escape(@$_REQUEST["parentId"]);
      $groupId=cDataBase::escape(@$_REQUEST["id"]);
      $item=$oBlogGroup->get($groupId);                                                           

		$code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;ویرایش دسته ({$item->title})</h1>
		<div class='vSpace-4x'></div>
		
		<div class='form'>
			<label>عنوان :</label>
			<input type='text' id='txt_title' value='{$item->title}' >

			<label>توضیحات :</label>
			<textarea id='txt_comment' class='form-control'>{$item->comment}</textarea>			
		</div>
      <div class='vSpace-4x'></div>		
		<hr>
		<button class='btn btn-default' onclick='blogGroup_draw({$parentId});'><i class='fa fa-arrow-right'></i></button>				
		<button class='btn btn-success' onclick='blogGroup_update(\"edit\",{$parentId},{$groupId});'>ذخيره</button>
		<div class='vSpace-4x'></div>
      ";

      $oEngine->response("ok[|]{$code}");	
   }//------------------------------------------------------------------------------------
   else if($request=="blogGroup_new")
   {
		$parentId=cDataBase::escape($_REQUEST["parentId"]);                                                        

		$code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;ویرایش دسته ({$item->title})</h1>
		<div class='vSpace-4x'></div>
		
		<div class='form'>
			<label>عنوان :</label>
			<input type='text' id='txt_title' value='' >

			<label>توضیحات :</label>
			<textarea id='txt_comment' class='form-control'></textarea>			
		</div>
      <div class='vSpace-4x'></div>		
		<hr>
		<button class='btn btn-default' onclick='blogGroup_draw({$parentId});'><i class='fa fa-arrow-right'></i></button>				
		<button class='btn btn-success' onclick='blogGroup_update(\"new\",{$parentId},0);'>ذخيره</button>
		<div class='vSpace-4x'></div>
      ";

      $oEngine->response("ok[|]{$code}");		
   }//------------------------------------------------------------------------------------
   else if($request=="blogGroup_update")
   {
		$purpose=$oDb->escape($_REQUEST["purpose"]);
		
		if($purpose=="edit")
		   $groupId=cDataBase::escape($_REQUEST["id"]);
	   else
			$groupId=time();
		
      $array=array();
      $array["purpose"]=cDataBase::escape($_REQUEST["purpose"]);
      $array["parentId"]=cDataBase::escape($_REQUEST["parentId"]);		
      $array["groupId"]=$groupId;
      $array["title"]=cDataBase::escape($_REQUEST["title"]);
      $array["comment"]=cDataBase::escape($_REQUEST["comment"]);
      
		if($array["purpose"]=="edit")
         $ret=$oBlogGroup->update($array);
		else if($array["purpose"]=="new")
         $ret=$oBlogGroup->insert($array);
	  
      $oEngine->response("ok[|]{$array["parentId"]}");		
   }//------------------------------------------------------------------------------------
   else if($request=="blogGroup_active")
   {
      $groupId=$oDb->escape($_REQUEST["id"]);
      $item=$oBlogGroup->get($groupId);
		
      if($item->active=="0")
      {
         $code="<button class='btn btn-success' onclick='blogGroup_active({$groupId});'><i class='fa fa-undo'></i>&nbsp;فعال</button>";
			$oBlogGroup->setActive($groupId,"1");
      }
      elseif($item->active=="1")
      {
         $code="<button class='btn btn-danger' onclick='blogGroup_active({$groupId});'><i class='fa fa-undo'></i>&nbsp;غیر فعال</button>";
         $oBlogGroup->setActive($groupId,"0");
      }
		
		$oEngine->response("ok[|]{$groupId}[|]{$code}");		
   }//--------------------------------------------------------------------------
?>
<?php
	require_once $oPath->manageDir("blog_bundle/model/blogGroup_model.php");
	class cBlogTools
	{
		function group_chksDraw($parentId=0,$arryId) //list hame group ha be soorate check box
		{
			$oBlogGroup=new cBlogGroup();
			
			//list menuhaye ghabel dastras
			$items=$oBlogGroup->getAll(false,$parentId,1);
			$code="";
			if(count($items) > 0)
			{
				foreach($items as $item)
				{
					if(in_array($item->id,$arryId)) $checked=" checked='checked'"; else $checked="";
					$code.="
					<li>
					   <input type='checkbox' id='chk_{$item->id}' value='{$item->id}'{$checked}>&nbsp;&nbsp;{$item->title}
						<ul>";
							$code.=$this->group_chksDraw($item->id,$arryId);
							$code.="
						</ul>
					</li>
					";
				}
				return $code;
			}
			else
				return "";
		}//--------------------------------------------------------------------------
		function group_treeDraw($parentId=0,$arryId) //list hame group ha be soorate check box
		{
			$oBlogGroup=new cBlogGroup();
			
			//list menuhaye ghabel dastras
			$items=$oBlogGroup->getAll(false,$parentId,1);
			$code="";
			if(count($items) > 0)
			{
				foreach($items as $item)
				{
					if(in_array($item->id,$arryId)) $checked="fa fa-check"; else $checked="";
					$code.="
					<li>
					  <i class='{$checked}'></i>&nbsp;{$item->title}
						<ul>";
							$code.=$this->group_treeDraw($item->id,$arryId);
							$code.="
						</ul>
					</li>
					";
				}
				return $code;
			}
			else
				return "";
		}//--------------------------------------------------------------------------		
	}
?>
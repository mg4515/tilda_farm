<?php
   @session_start();
	
	//request
	include_once $_SESSION["engineRequire"];//engine.php
	require_once $oPath->manageDir("blog_bundle/model/blogItems_model.php");	
	require_once $oPath->manageDir("blog_bundle/model/blogGroup_model.php");	
	require_once $oPath->manageDir("blog_bundle/controller/php/blog_tools.php");	
	//require_once $oPath->manageDir("site_bundle/model/sitePro_model.php");
	//require_once $oPath->manageDir("mail_bundle/model/mail_model.php");
	require_once $oPath->manageDir("jdf.php");
	require_once $oPath->manageDir("simpleImage.php");
	require_once $oPath->manageDir("qrcode_bundle/qrlib.php");

	//object
	$oBlogItems=new cBlogItems();
	$oBlogGroup=new cBlogGroup();
	$oBlogTools=new cBlogTools();
	//$oSitePro=new cSitePro();
	//$oMail=new cMail();
	
   //var
	$arryActiveFilter=[''=>'', 0=>'غیر فعال ها', 1=>'فعال ها'];		
	
	//request
   $request=$_REQUEST["requestName"];

	if($request=="blogItems_draw")
   {
      $searchWord=cDataBase::escape(@$_REQUEST["searchWord"]);
      $page=isset($_REQUEST["page"]) ? cDataBase::escape($_REQUEST["page"]) : 1;
      $active=cDataBase::escape(@$_REQUEST["active"]);
      $trash=isset($_REQUEST["trash"]) ? cDataBase::escape($_REQUEST["trash"]) : 0;
		
		//auto session
      if($searchWord==-1) $searchWord=$_SESSION["blogItems_searchWord"];
      if($page==-1) $page=$_SESSION["blogItems_page"];
      if($active==-1) $active=@$_SESSION["blogItems_active"];
      if($trash==-1) $trash=$_SESSION["blogItems_trash"];
      
		//init session
      $_SESSION["blogItems_searchWord"]=$searchWord;
      $_SESSION["blogItems_page"]=$page;
      $_SESSION["blogItems_active"]=$active;
      $_SESSION["blogItems_trash"]=$trash;
		
		//button new
		$btnNew="<button type='button' class='btn btn-success' onclick='blogItems_new();'><i class='fa fa-plus'></i>&nbsp;افزودن</button>";
		
		//title
		$title='وبلاگ، مطالب';
		
		//title by trash, btn trash
		if($trash==1)
		{			
         $title='وبلاگ، مطالب حذف شده';
			//delete all
			$btnDeleteAll="<button id='btn_trash' class='btn btn-danger' onclick='blogItems_del(\"selected\")' disabled><i class='fa fa-remove'></i></button>";
			$btnNew='';
			$slctActiveFilter='';
		}
		else
		{
			//trash all
			$btnDeleteAll="<button id='btn_trash' class='btn btn-danger' onclick='blogItems_trash(\"selected\")' disabled><i class='fa fa-trash-o'></i></button>";
			//filter active
			$slctActiveFilter='';
			foreach($arryActiveFilter as $key=>$val)
			{
				if($active==$key && $active!=='') $selected='selected'; else $selected='';
				$slctActiveFilter.="<option value='{$key}' {$selected}>{$val}</option>";
			}
		   $slctActiveFilter="<select id='slct_confirmFilter' onchange='blogItems_draw(\"auto\",this.value,undefined);'>{$slctActiveFilter}</select>";		
			if($active===0) 
				$title='وبلاگ، مطالب غیر فعال';
			else if($active==1)
				$title='وبلاگ، مطالب فعال';
			
		}		
		
		//title+ by search word
		if($searchWord) $title.= " - جستجو ({$searchWord})";
		
		//START PAGE ...................................
      $arg=array(
			"trash"=>$trash,
			"active"=>$active,
			"searchWord"=>$searchWord,
			"limitStr"=>""
		);		
      $items=$oBlogItems->getAll($arg);
      $count=count($items);
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
		if($pages["pagesEnd"] > 1)
		{
			for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='blogItems_draw({\"active\":-1,\"trash\":-1,\"searchWord\":-1,\"page\":{$i}});'>{$i}</a>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='blogItems_draw({\"active\":-1,\"trash\":-1,\"searchWord\":-1,\"page\":{$back}});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='blogItems_draw({\"active\":-1,\"trash\":-1,\"searchWord\":-1,\"page\":{$next}});'><i class='fa fa-angle-left'></i></a>";		
		}
      $arg=array(
			"trash"=>$trash,
			"active"=>$active,
			"searchWord"=>$searchWord,
			"limitStr"=>$pages['sqlLimit']
		);	      
		$items=$oBlogItems->getAll($arg);
		//END PAGE ...................................
		
      $codeTr="";
      $i=0;
		foreach($items as $item)
		{
			$itemId=$item->id;
			
			$countOpine_new=count($oBlogItems->opine_getAll($item->id,"","0"));
			$countOpine_confirm=count($oBlogItems->opine_getAll($item->id,"1","1"));
			$countOpine_notConfirm=count($oBlogItems->opine_getAll($item->id,"0","1"));
			$btnOpineNewDisable= $countOpine_new ? "" : "disabled";
			$btnOpineConfirmDisable= $countOpine_confirm ? "" : "disabled";
			$btnOpineNotConfirmDisable= $countOpine_notConfirm ? "" : "disabled";
			
			//active
			if($item->active==0) $activeBtn="<button class='btn btn-danger' onclick='blogItems_active({$itemId});'><i class='fa fa-undo'></i>&nbsp;غیر فعال</button>";
			else if($item->active=="1") $activeBtn="<button class='btn btn-success' onclick='blogItems_active({$itemId});'><i class='fa fa-undo'></i>&nbsp;فعال</button>";	

			//trash or delete
			if($item->trash==0)
			{
				$btnDelete="<button class='btn btn-danger' onclick='blogItems_trash({$item->id});'><i class='fa fa-trash-o'></i></button>";
		      $btnUnTrash='';
		   }
			else
			{
				$btnDelete="<button class='btn btn-danger' onclick='blogItems_del({$item->id});' title='حذف'><i class='fa fa-remove' title='حذف کامل'></i></button>";
				$btnUnTrash="<button class='btn btn-warning' onclick='blogItems_unTrash({$item->id});' title='بازیابی'><i class='fa fa-undo'></i></button>";
			}
			
			//image info
			if(file_exists($oPath->manageDir("blog_bundle/data/images/itemThumb_{$item->id}.jpg")))
				$html_img="<img class='brd-radius-8' src='" . $oPath->manage("blog_bundle/data/images/itemThumb_{$item->id}.jpg?t=" . time() ) ."' style='width:86px' />";
			else
				$html_img="<img class='brd-radius-8' src='" . $oPath->asset("default/images/noImage.gif") . "' style='width:64px;' />";
			
			//group info
			$html_group='';
         $groups=explode('|',$item->groupId);
			$count=count($groups);
			for($ii=0;$ii < $count;$ii++)
			{
				$groupId=$groups[$ii];
				if($group=@$oBlogGroup->get($groupId))
					$html_group.="<span>{$group->title}</span>, ";
			}
			$html_group=rtrim($html_group,', ');
			
			$i++;
			$codeTr.="
			<tr>";
				if(count($items) > 1) $codeTr.="<td style='text-align:center;width: 10px;'><input type='checkbox' id='chk_{$i}' onclick='checkedOne();' value='{$item->id}' /></td>";
				$codeTr.="
				<td style='padding:10px;width:100px;'>
					{$html_img}<br>
					{$item->title}
					<hr>
					<i class='fa fa-eye'> {$item->visits}</i>
				</td>
				<td>{$html_group}</td>
				<td>
					<button class='btn btn-label btn-info' onclick='blogItems_opineDraw({$item->id},\"\",\"0\",1);' {$btnOpineNewDisable}>
						<span>نظرات جدید</span>
						<span class='btn-label-caption'>{$countOpine_new}</span>                 
					</button>
					<button class='btn btn-label btn-success' onclick='blogItems_opineDraw({$item->id},\"1\",\"1\",1);' {$btnOpineConfirmDisable}>
						<span>نظرات تایید شده</span>
						<span class='btn-label-caption'>{$countOpine_confirm}</span>                  
					</button>
					<button class='btn btn-label btn-warning' onclick='blogItems_opineDraw({$item->id},\"0\",\"1\",1);' {$btnOpineNotConfirmDisable}>
						<span>نظرات تایید نشده</span>
						<span class='btn-label-caption'>{$countOpine_notConfirm}</span>                  
					</button>					
				</td>
				<td>
					<span id='td_active_{$item->id}'>{$activeBtn}</span>
					{$btnDelete}
					{$btnUnTrash}
					<button class='btn btn-info' onclick='blogItems_edit({$item->id});'><i class='fa fa-pencil'></i></button>
				</td>
			</tr>";
		}
      if($codeTr)
		{
			$chkCount=$i;
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>
			
			{$btnNew}
			{$btnDeleteAll}
			{$slctActiveFilter}
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='blogItems_draw(\"auto\",undefined,$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			
			<div class='vSpace'></div>
			
			<table class='tbl tbl-center tbl-bordered tbl-hover'>
				<tr>";
					if(count($items) > 1) $code.="<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>";
					$code.="
					<th></th>
					<th>دسته بندی</th>
					<th>نظرات</th>
					<th>عملیات</th>
				</tr>
				{$codeTr}
			</table>
			<br>{$btnPageCode}<br> 
			<div class='vSpace-4x'></div>";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$title}</h1>
			<div class='vSpace-4x'></div>

			{$btnNew}
			{$btnDeleteAll}
			{$slctActiveFilter}
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='blogItems_draw(\"auto\",undefined,$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'></div>";		
		}
		$oEngine->response("ok[|]" . $code);
   }//------------------------------------------------------------------------------------	
   else if($request=="blogItems_del")
   {
      $itemId=cDataBase::escape($_REQUEST["id"],false);
      $ids=$oBlogItems->delete($itemId);
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId)
			{
				//images
				@unlink ($oPath->manageDir("blog_bundle/data/images/item_{$selectedId}.jpg"));
				@unlink ($oPath->manageDir("blog_bundle/data/images/itemLarg_{$selectedId}.jpg"));
				@unlink ($oPath->manageDir("blog_bundle/data/images/itemThumb_{$selectedId}.jpg"));
				@unlink ($oPath->manageDir("blog_bundle/data/images/itemQr_{$selectedId}.jpg"));
				//contents
				@unlink($oPath->manageDir("blog_bundle/data/contents/item_{$selectedId}.html"));
				//opines
				$opines=$oBlogItems->opine_deleteByItemId($selectedId);
				$count=count($opines);
				for($ii=0;$ii < $count;$ii++)
				{
					@unlink ($oPath->manageDir("blog_bundle/data/contents/opineAnswer_{$opines[$ii]->id}.html"));
				   @unlink ($oPath->manageDir("blog_bundle/data/contents/opineContent_{$opines[$ii]->id}.html"));
				}	
			}
		}      
		$oEngine->response('ok');
   }//------------------------------------------------------------------------------------
   else if($request=="blogItems_trash")
   {
      $itemId=cDataBase::escape($_REQUEST["id"],false);
      $ids=$oBlogItems->trash($itemId);      
		$oEngine->response('ok');
   }//------------------------------------------------------------------------------------
   else if($request=="blogItems_unTrash")
   {
      $itemId=cDataBase::escape($_REQUEST["id"],false);
      $ids=$oBlogItems->unTrash($itemId);      
		$oEngine->response('ok');
   }//------------------------------------------------------------------------------------
	else if($request=="blogItems_active") //change active to unActive and reverse
   {
      $itemId=cDataBase::escape($_REQUEST["id"]);
      $ret=$oBlogItems->getActive($itemId);
		if($ret==true)
		{
		   $oBlogItems->setActive($itemId,"0");					
			$code="<button class='btn btn-danger' onclick='blogItems_active({$itemId});'><i class='fa fa-undo'></i>&nbsp;غیر فعال</button>";
		}
		else
		{
			$oBlogItems->setActive($itemId,"1");			
			$code="<button class='btn btn-success' onclick='blogItems_active({$itemId});'><i class='fa fa-undo'></i>&nbsp;فعال</button>";
		}	
      $oEngine->response("ok[|]{$itemId}[|]{$code}");
   }//------------------------------------------------------------------------------------  
   else if($request=="blogItems_edit")
   {
      $itemId=cDataBase::escape($_REQUEST["id"]);
      $item=$oBlogItems->get($itemId);
 
      //images
		$t=time();
      if(!file_exists($oPath->manageDir("blog_bundle/data"))) mkdir($oPath->manageDir("blog_bundle/data"));
      if(!file_exists($oPath->manageDir("blog_bundle/data/images"))) mkdir($oPath->manageDir("blog_bundle/data/images"));
      //---
      if(file_exists($oPath->manageDir("blog_bundle/data/images/itemThumb_{$itemId}.jpg")))
         $html_img="<img id='img_item' src='" . $oPath->manage("blog_bundle/data/images/itemThumb_{$itemId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_itemImgDel' class='fa fa-trash fa-2x' onclick='blogItems_imgDel({$itemId},0);' style='cursor:pointer;color:red;'></i>";
      else
         $html_img="<img id='img_item' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      
		//conetent
      if(file_exists($oPath->manageDir("blog_bundle/data/contents/item_{$itemId}.html")))
         $content=file_get_contents($oPath->manageDir("blog_bundle/data/contents/item_{$itemId}.html"));
      else 
			$content="";
		
		//group
		$html_group=@$oBlogTools->group_chksDraw(0,explode('|',$item->groupId));
		
		//activeOpine
      if($item->activeOpine=="1") $chk_activeOpine="checked='checked'"; else $chk_activeOpine="";		
	
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil-square-o'></i>&nbsp;ویرایش مطلب {$item->title}</h1>		
		<div class='vSpace-4x'></div>
		     
		<div class='form dir-rtl algn-r content'>
			<label><i class='fa fa-circle'></i>عنوان <b class='fg-danger'>*</b></label>
			<div id='layer_titleCounter'></div>
			<input type='text' id='txt_title' value='{$item->title}' />
			<script>text_drawLength('txt_title','layer_titleCounter',70,true);</script>
			
			<hr>
			
			<label><i class='fa fa-circle'></i>دسته بندی <span class='fg-red'>*</span></label>
			<label>دسته بندی های مورد نظر خود را انتخاب نمایید</label>			
			<div style='height:220px;overflow-y:auto;background-color:#eee;'>
				<div id='layer_group' class='ul ul-right ul-tree padding'>{$html_group}</div>
			</div>			
			
			<hr>
				
			<label><i class='fa fa-circle'></i>برچسب ها</label>
			<label>کلمه مورد نظر را وارد نمایید و بر روی دکمه + کلیک کنید</label>
			<input type='hidden' id='txt_tags' value='{$item->keywords}'>
			<div class='input-control input-control-right part-w-10'>
				<span class='input-control-button fa fa-plus bg-info' onclick='oMyTag.insert($(\"#txt_tag\").val(),$(\"#txt_tag\").val());$(\"#txt_tag\").val(\"\");'></span>
				<input type='text' id='txt_tag' class='dir-rtl-i algn-r-i'> 							
			</div>
			<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
			<script>
				oMyTag=myTag({'elementId':'ul_toTags'});
				oMyTag.setByString('{$item->keywords}','|');
			</script>
			
			<hr>
			
			<label><i class='fa fa-circle'></i>توضیحات کوتاه<b class='fg-danger'></b></label>
			<div id='layer_commentCounter'></div>
			<textarea id='txt_comment' placeholder='توضیحات کوتاه' >{$item->comment}</textarea>
			<script>text_drawLength('txt_comment','layer_commentCounter',170,true);</script>			
			
			<hr>

			<label><i class='fa fa-circle'></i>متن اصلی</label>
			<textarea id='txt_content'>{$content}</textarea>
			<script>CKEDITOR.replace('txt_content');</script>
			
			<hr>
			
         <label><i class='fa fa-circle'></i>تصویر</label>			
			<table class='display-inlineBlock'>
				<tr><td>{$html_img}</td></tr>
				<tr><td>
					<label class='lbl-file  lbl-file-right'>
						<input type='file' id='file_imgItem' onchange='oTools.previewImgFile(\"file_imgItem\",\"img_item\");' required >
						<span class='lbl-file-icon'></span>
						<span>انتخاب فایل</span>
					</label>					
				</td></tr>
			</table>    

			<hr>

			<label><i class='fa fa-circle'></i><input type='checkbox' id='chk_activeOpine' {$chk_activeOpine}>&nbsp;امکان نظر دهی کاربران</label>			
      </div>
		
      <div class='vSpace-4x'></div>		
		<hr>
		<button class='btn btn-default' onclick='blogItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
		<button class='btn btn-success' onclick='blogItems_update(\"edit\",{$itemId});'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
		<div class='vSpace-4x'></div>
      ";
      cEngine::response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($request=="blogItems_new")
   {

      //images
		$t=time();
      if(!file_exists($oPath->manageDir("blog_bundle/data"))) mkdir($oPath->manageDir("blog_bundle/data"));
      if(!file_exists($oPath->manageDir("blog_bundle/data/images"))) mkdir($oPath->manageDir("blog_bundle/data/images"));
      //---
      $html_img="<img id='img_item' src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
      
		//conetent
		$content="";
		
		//group
		$html_group=@$oBlogTools->group_chksDraw(0,[]);
		
		//activeOpine
      $chk_activeOpine="";		
	
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-plus'></i>&nbsp;مطلب جدید</h1>		
		<div class='vSpace-4x'></div>
		
		<div class='form dir-rtl algn-r content'>
			<label><i class='fa fa-circle'></i>عنوان <b class='fg-danger'>*</b></label>
			<div id='layer_titleCounter'></div>
			<input type='text' id='txt_title' />
			<script>text_drawLength('txt_title','layer_titleCounter',70,true);</script>
			
			<hr>
			
			<label><i class='fa fa-circle'></i>دسته بندی <span class='fg-red'>*</span></label>
			<label>دسته بندی های مورد نظر خود را انتخاب نمایید</label>			
			<div style='height:220px;overflow-y:auto;background-color:#eee;'>
				<div id='layer_group' class='ul ul-right ul-tree padding'>{$html_group}</div>
			</div>			
			
			<hr>
			
			<label><i class='fa fa-circle'></i>برچسب ها</label>
			<label>کلمه مورد نظر را وارد نمایید و بر روی دکمه + کلیک کنید</label>
			<input type='hidden' id='txt_tags'>
			<div class='input-control input-control-right'>
				<span class='input-control-button fa fa-plus bg-info' onclick='oMyTag.insert($(\"#txt_tag\").val(),$(\"#txt_tag\").val());$(\"#txt_tag\").val(\"\");'></span>
				<input type='text' id='txt_tag' class='dir-rtl-i algn-r-i'> 							
			</div>
			<ul id='ul_toTags' class='myTag myTag-right fg-gray padding'></ul>
			<script>
				oMyTag=myTag({'elementId':'ul_toTags'});
			</script>
			
			<hr>
			
			<label><i class='fa fa-circle'></i>توضیحات کوتاه <b class='fg-danger'></b></label>
			<div id='layer_commentCounter'></div>
			<textarea id='txt_comment' placeholder='توضیحات کوتاه' ></textarea>
			<script>text_drawLength('txt_comment','layer_commentCounter',170,true);</script>			
			
			<hr>
			
			<label><i class='fa fa-circle'></i>متن اصلی</label>
			<textarea id='txt_content'></textarea>	
			<script>CKEDITOR.replace('txt_content');</script>
			
			<hr>
			
         <label><i class='fa fa-circle'></i>تصویر</label>			
			<table class='display-inlineBlock'>
				<tr><td>{$html_img}</td></tr>
				<tr><td>
					<label class='lbl-file  lbl-file-right'>
						<input type='file' id='file_imgItem' onchange='oTools.previewImgFile(\"file_imgItem\",\"img_item\");' required >
						<span class='lbl-file-icon'></span>
						<span>انتخاب فایل</span>
					</label>					
				</td></tr>
			</table> 

			<hr>

			<label><input type='checkbox' id='chk_activeOpine' {$chk_activeOpine}>&nbsp;امکان نظر دهی کاربران</label>			
      </div>
		
      <div class='vSpace-4x'></div>		
		<hr>
		<button class='btn btn-default' onclick='blogItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
		<button class='btn btn-success' onclick='blogItems_update(\"new\",0);'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
		<div class='vSpace-4x'></div>
      ";
      cEngine::response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($request=="blogItems_update")
   {
      if($_REQUEST["purpose"]=="edit")
      {
         $id=cDataBase::escape($_REQUEST["id"]);
			$item=$oBlogItems->get($id);
      }
      else
      {
         $id=time();
      }
		
      $array=[];
      $array["purpose"]=cDataBase::escape($_REQUEST["purpose"]);
      $array["id"]=$id;	
      $array["groupId"]=cDataBase::escape(rtrim($_REQUEST["groupId"],','));	//groupId1,groupId2,id3,... conver to groupId1|groupId2|groupId3|...		
      $array["title"]=cDataBase::escape($_REQUEST["title"]);
      $array["activeOpine"]=cTools::misc_jsBln2PhpBln(cDataBase::escape($_REQUEST["activeOpine"]),true);
      $array["keywords"]=cDataBase::escape(rtrim(@$_REQUEST["keywords"],',')); //tag1,tag2,tag3,... conver to tag1|tag2|tag3|...
      $array["comment"]=cDataBase::escape($_REQUEST["comment"]);
      
		//contents
		if(!file_exists($oPath->manageDir("blog_bundle/data"))) mkdir($oPath->manageDir("blog_bundle/data"));
		if(!file_exists($oPath->manageDir("blog_bundle/data/contents"))) mkdir($oPath->manageDir("blog_bundle/data/contents"));
		//---		
		$content=$oTools->str_tagFitSend_decode($_REQUEST["content"]);
		file_put_contents($oPath->manageDir("blog_bundle/data/contents/item_{$id}.html"),$content);
		
      //image
      $imgDel=cDataBase::escape($_REQUEST["imgDel"]);	
      if($imgDel==1 || $imgDel=="true") 
		{
			@unlink($oPath->manageDir("blog_bundle/data/images/item_{$id}.jpg"));
			@unlink($oPath->manageDir("blog_bundle/data/images/itemLarg_{$id}.jpg"));
			@unlink($oPath->manageDir("blog_bundle/data/images/itemThumb_{$id}.jpg"));			
		}
      //---
		if(isset($_FILES["file_imgItem"]))
		{
			if(!$oTools->fs_fileIsImage($_FILES["file_imgItem"]["name"]))
			{
				$oEngine->response("!imageType");
				exit;
			}
			if($oTools->fs_fileSize($_FILES["file_imgItem"]["tmp_name"]) > 15728640) //15mb
			{
				$oEngine->response("!imageSize");
				exit;
			}				

			$img = new SimpleImage();
			$img->load($_FILES['file_imgItem']['tmp_name'])
			->fit_to_width(370)
			->save($oPath->manageDir("blog_bundle/data/images/item_{$id}.jpg"),100);
			//--
			$img->load($_FILES['file_imgItem']['tmp_name'])
			->fit_to_width(600)
			->save($oPath->manageDir("blog_bundle/data/images/itemLarg_{$id}.jpg"),100);
			//--
			$img->load($_FILES['file_imgItem']['tmp_name'])
			->fit_to_width(120)
			->save($oPath->manageDir("blog_bundle/data/images/itemThumb_{$id}.jpg"),100);				
			unlink($_FILES['file_imgItem']['tmp_name']);
		}

      if($array["purpose"]=="edit")
		{	
	      $array["editedById"]=$_SESSION['admin_id'];
         $ret=$oBlogItems->update($array);
		}
      else if($array["purpose"]=="new")
		{
			$array["createdById"]=$_SESSION['admin_id'];			
         $ret=$oBlogItems->insert($array);
		}
		
      cEngine::response("ok[|]{$array['purpose']}");
		//QRcode::png($oBlogItems->getUrl($id),$oPath->manageDir("blog_bundle/data/images/itemQr_{$id}.jpg"),4,6,false);
		//file_put_contents($oPath->publicDir("client/sitemap.xml"),$oBlogItems->siteMapUpdate());		
   }//------------------------------------------------------------------------------------
	
	/* OPINE */
   else if($request=="blogItems_opineDraw")
   {
		$itemId=$oDb->escape($_REQUEST["itemId"]);
		$confirm=$oDb->escape($_REQUEST["confirm"]);
		$checkOut=$oDb->escape($_REQUEST["checkOut"]);
		$page=isset($_REQUEST["page"]) ? cDataBase::escape($_REQUEST["page"]) : 1;
		
		//auto
		if($page=="-1") $page=$_SESSION["blogItemsOpine"];
		if($itemId=="-1") $itemId=$_SESSION["shopItemsOpine_itemId"];
		if($confirm=="-1") $confirm=$_SESSION["shopItemsOpine_confirm"];
		if($checkOut=="-1") $checkOut=$_SESSION["shopItemsOpine_checkOut"];
		
		//save
		$_SESSION["blogItemsOpine"]=$page;
		$_SESSION["shopItemsOpine_itemId"]=$itemId;
		$_SESSION["shopItemsOpine_confirm"]=$confirm;
		$_SESSION["shopItemsOpine_checkOut"]=$checkOut;
      
      //START PAGE ...................................
      $items=$oBlogItems->opine_getAll($itemId,$confirm,$checkOut);
      $count=count($items);
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
		for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
		{
			if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
				$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
			else
				$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='blogItems_opineDraw(-1,-1,-1,{$i});'>{$i}</a>";
		}
      $back=$page - 1;
      $next=$page + 1;
      if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='blogItems_opineDraw(-1,-1,-1,{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='-1,-1,-1,{$next});'><i class='fa fa-angle-left'></i></a>";		      
		$items=$oBlogItems->opine_getAll($itemId,$confirm,$checkOut,$pages['sqlLimit']);		
		//END PAGE ...................................
		
		if($itemId)
		{
			$shopItem=@$oBlogItems->get($itemId);
			if($shopItem) $title=$shopItem->title; else $title='جدید';
		}
		else
		{
			$shopItem='';
			$title='جدید';
		}
		
		$i=(($page-1)*3);
		$codeTr="";
      foreach($items as $item)
      {
         $i++;
		
			if(!$shopItem) $shopItem=$oBlogItems->get($itemId);
			
         if(file_exists($oPath->manageDir("blog_bundle/data/contents/itemOpineAnswer_{$item->id}.html")))
            $status='<span style="color: #009900">پاسخ داده شده</span>';
         else
            $status='<span style="color: #990000">پاسخ داده نشده</span>';
			
         if($item->confirm=="0") $confirm="<button class='btn btn-danger' onclick='blogItems_opineConfirm({$item->id});'>تایید نشده</button>";
         elseif($item->confirm=="1") $confirm="<button class='btn btn-success' onclick='blogItems_opineConfirm({$item->id});'>تایید شده</button>";			

			$user=@$oUsers->get($item->userId);
			$user=@"<a href='javascript:void(0)' onclick='user_profileDraw({$user->id});'>{$user->userName}</a>";
			
			if(file_exists($oPath->manageDir("blog_bundle/data/images/item0_{$shopItem->id}.jpg")))
				$shopItemImg="<img src='" . $oPath->manage("blog_bundle/data/images/item0_{$shopItem->id}.jpg") . "' style='border-radius:8px;width:120px'>";
			else
				$shopItemImg="<img src='" . $oPath->asset("default/images/noImage.gif") . "' style='border-radius:8px;width:120px'>";
         $shopItemTitle=$shopItemImg . "<br>" . $shopItem->title;
			
         $date=jdate("Y/m/d h:t:i",$item->id);
         if($item->checkOut=="0") $style="style='font-weight: bolder;color:#000;font-size:17px;'"; else $style="color:#666";
         $codeTr.="
			<tr>";
				if(count($items) > 1) $codeTr.="<td style='text-align:center;width: 10px;'><input type='checkbox' id='chk_{$i}' onclick='checkedOne();' value='{$item->id}' /></td>";
				$codeTr.="
				<td {$style}>{$shopItemTitle}</td>
				<td {$style}>{$user}</td>
				<td {$style}>{$status}</td>
				<td {$style}>{$date}</td>
				<td {$style}>
				   <span id='td_active_{$item->id}'>{$confirm}</span>
				   <button class='btn btn-danger' onclick='blogItems_opineDel({$item->id});'><i class='fa fa-trash-o'></i></button>
				   <button class='btn btn-info' onclick='blogItems_opineView({$item->id});'><i class='fa fa-eye'></i></button>
				</td>
			</tr>";
      }
		$chkCount=$i;
		if($codeTr)
		{
			$code= "
				<div class='vSpace-4x'></div>
				<h1><i class='fa fa-list'></i>&nbsp;نظرات {$title}</h1>
				<div class='vSpace-4x'></div>";
				if($itemId) $code.="<button class='btn btn-default' onclick='blogItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>";
			if(count($items) > 1) $code.="<button id='btn_trash' class='btn btn-danger' onclick='blogItems_opineDel(\"selected\");' disabled><i class='fa fa-trash-o'></i></button>";			
			$code.= "
			<div class='vSpace'></div>
			{$btnPageCode}			
			<table class='tbl tbl-striped tbl-bordered tbl-hover' style='direction:rtl'>
				<tr>";
					if(count($items) > 1) $code.="<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>";
					$code.="
					<th class='text-right'>موضوع</th>
					<th class='text-right'>کاربر</th>
					<th class='text-right'>وضعیت</th>
					<th class='text-right'>تاریخ</th>
					<th class='text-right'></th>
				</tr>
				{$codeTr}
			</table>
			{$btnPageCode}
			<br><br>";
		}
		else
		{
			$code= "
				<div class='vSpace-4x'></div>
				<h1><i class='fa fa-list'></i>&nbsp;نظرات {$title}</h1>
				<div class='vSpace-4x'></div>";
				if($itemId) $code.="<button class='btn btn-default' onclick='blogItems_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>";
			$code.= "
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'>";			
		}
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------	
   else if($request=="blogItems_opineDel")
   {
      $ids=cDataBase::escape($_REQUEST["ids"],false);
      $ids=$oBlogItems->opine_delete($ids);
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				@unlink ($oPath->manageDir("blog_bundle/data/contents/itemOpineAnswer_{$selectedId}.html"));
				@unlink ($oPath->manageDir("blog_bundle/data/contents/itemOpineContent_{$selectedId}.html"));
			}
		} 		
		
      $oEngine->response("ok");
   }//------------------------------------------------------------------------------------
   else if($request=="blogItems_opineConfirm")
   {
      $id=$oDb->escape($_REQUEST["id"]);
      $ret=$oBlogItems->opine_getConfirm($id);
		if($ret==true)
		{
		   $oBlogItems->opine_setConfirm($id,"0");	
			$code="<button class='btn btn-danger' onclick='blogItems_opineConfirm({$id});'>تایید نشده</button>";
		}
		else
		{
			$oBlogItems->opine_setConfirm($id,"1");	
			$code="<button class='btn btn-success' onclick='blogItems_opineConfirm({$id});'>تایید شده</button>";
		}
      $oEngine->response("ok[|]{$id}[|]{$code}");
   }//------------------------------------------------------------------------------------	
   if($request=="blogItems_opineView")
   {
      $opineId=$oDb->escape($_REQUEST["id"]);
      $item=$oBlogItems->opine_get($opineId);
		$oBlogItems->opine_setCheckOut($opineId,"1");
      
		//content and answer
		$content=@file_get_contents($oPath->manageDir("blog_bundle/data/contents/itemOpineContent_{$opineId}.html"));
		if(file_exists($oPath->manageDir("blog_bundle/data/contents/itemOpineAnswer_{$opineId}.html")))
		{	
         $answerContent=file_get_contents($oPath->manageDir("blog_bundle/data/contents/itemOpineAnswer_{$opineId}.html"));
		   $status="<span class='fg-success'>پاسخ داده شده</span>";
		}
		else
      {			
			$answerContent="";
			$status="<span class='fg-warning'>پاسخ داده نشده</span>";
		}
		
		if($item->confirm=="0") $optnConfirm="<option value='1'>تایید شده</option><option value='0' selected='selected'>تایید نشده</option>"; 
		else if($item->confirm=="1") $optnConfirm="<option value='1' selected='selected'>تایید شده</option><option value='0'>تایید نشده</option>"; 

		$user=$oUsers->get($item->userId);
		if(file_exists($oPath->manageDir("user_bundle/data/images/user_{$item->userId}.jpg")))
			$userImage="<div class='chatOnline img' style='background-image:url(" . $oPath->manage("user_bundle/data/images/user_{$item->userId}.jpg") . ");width:64px;height:64px;'></div>";
		else
			$userImage="<div class='chatOnline img' style='background-image:url(" . $oPath->asset("default/images/user_larg.png") . ");border-radius:0%' ></div>";
				
		$date=jdate("Y/m/d h:t:i",$item->id);
      $code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-eye'></i>&nbsp;نظر</h1>
		<div class='vSpace-4x'></div>
		<div class='dir-rtl'>
			<table class='tbl tbl-bordered'>
				<tr>
					<th>ایجاد شده</th>
					<th>وضعیت پاسخ</th>
					<th>وضعیت نمایش</th>
				</tr>
				<tr>
					<td>{$date}</td>
					<td>{$status}</td>
					<td><select id='slct_confirm' style='width:120px;'>{$optnConfirm}</select></td>
				</tr>								   
			</table>
		
			<div class='vSpace-2x'></div>
						
			<!-- opine content -->
			<div class='panel'>
				<div class='panel-titleBar'>نظر کاربر</div>
				<div class='panel-header'>
					<table>
						<tr>
							<td class='hide-auto'>{$userImage}</td>
							<td class='hide-auto'>&nbsp;|&nbsp;</td>
							<td><i class='fa fa-user'></i>&nbsp;<span class='title-h4'>{$user->userName}</span></td>
							<td>&nbsp;|&nbsp;</td>
							<td><i class='fa fa-calendar'></i>&nbsp;{$date}</td>
						</tr>
					</table>
				</div>
				<div class='panel-body'>
					{$content}  
				</div>
			</div>
			
			<div class='vSpace'></div>
			
			<hr>
			<div class='title-h3'><i class='fa fa-tag'></i>&nbspارسال پاسخ</div>
			<div class='vSpace-2x'></div>
			
			<div class='form dir-rtl algn-r'>
				<label>متن پاسخ</label>
				<textarea id='txt_answer'>{$answerContent}</textarea>
			</div>						
		</div>
				
		<div class='vSpace-4x'></div>
		<hr>
		<button class='btn btn-default' onclick='blogItems_opineDraw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
		<button class='btn btn-success' onclick='blogItems_opineAnswer({$opineId});'>ارسال</button>		
		<div class='vSpace-4x'></div>
      ";
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($request=="blogItems_opineAnswer")
   {
      $opineId=cDataBase::escape($_REQUEST["id"]);
      $confirm=cDataBase::escape($_REQUEST["confirm"]);
		$answerContent=$oTools->str_tagFitSend_decode($_REQUEST["answerContent"]);
		//save ansver
		if(!file_exists($oPath->manageDir("blog_bundle/data"))) mkdir($oPath->manageDir("blog_bundle/data"));
		if(!file_exists($oPath->manageDir("blog_bundle/data/contents"))) mkdir($oPath->manageDir("blog_bundle/data/contents"));
      file_put_contents($oPath->manageDir("blog_bundle/data/contents/itemOpineAnswer_{$opineId}.html"),$answerContent);
      //set confirm
		$oBlogItems->opine_setConfirm($opineId,$confirm);
		$oEngine->response("ok");
   }//------------------------------------------------------------------------------
	else if($request=="blogItems_opineNotification")
   {
		$count=count($oBlogItems->getOpineAll("","","0"));
		if(!isset($_SESSION["blogItems_opineNewCount"])) 
			$_SESSION["blogItems_opineNewCount"]=$count;
		else if($_SESSION["blogItems_opineNewCount"] > $count)
		{
		   $_SESSION["blogItems_opineNewCount"]=$count;	
		}
		if($_SESSION["blogItems_opineNewCount"] != $count)
		{
			$isNew="new";
		}
		else
		{
		   $isNew="";	
		}
		$_SESSION["blogItems_opineNewCount"]=$count;
      $oEngine->response("ok[|]{$count}[|]{$isNew}");
   }//------------------------------------------------------------------------------	
?>
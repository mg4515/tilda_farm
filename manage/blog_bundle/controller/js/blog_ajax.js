var url_blog=oPath.manage("blog_bundle/controller/php/blog_ajax.php");
function blogItems_opineSend(blogItemId)
{
	try {
		err=false;
		$("#spn_alert").html("");
		if($("#txt_comment").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا متن پیغام خود را وارد نمایید");
         err=true;			
		}
      else if(err==false)
		{
			$('#btn_opineSend').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
				url: url_blog,
				data: {"requestName": "blogItems_opineSend","blogItemId":blogItemId,"comment":$("#txt_comment").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
               if(result=="!login")
						script_alertStatus("spn_alert","danger","برای درج نظر، لطفا وارد شوید و یا ثبت نام نمایید");			
               else if (result=="ok")
					{
						script_alertStatus("spn_alert","success","نظر شما با موفقیت ارسال شد و پس از تایید، منتشر خواهد شد");
						$("#txt_comment").val('');
					}
               else
                  alert("انجام نشد");
					$('#btn_opineSend').html('ارسال');
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					$('#btn_opineSend').html('ارسال');
				}
			});
		}
	}
	catch (e) 
	{
	   alert("خطای اسکریپت");
	   $('#btn_opineSend').html('ارسال');
	}
}//---------------------------------------------------------------------------------------
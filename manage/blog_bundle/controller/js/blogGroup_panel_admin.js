var url_blogGroup=oPath.manage("blog_bundle/controller/php/blogGroup_panel_admin.php");
var isImageDelete=false;
var oMyModal=[];

function blogGroup_draw(parentId)
{
	isImageDelete=false;
   try {
      script_loadingShow();
      $.ajax({
         url: url_blogGroup,
         data: {"requestName":"blogGroup_draw", "parentId":parentId},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogGroup_del(parentId,id)
{
   script_confirm2("حذف شود ؟",'',function()
   {
      try {
         script_loadingShow();
         $.ajax({
            url: url_blogGroup,
            data: {"requestName":"blogGroup_del", "parentId":parentId, "id":id},
            method: "POST",
            success: function(result)
            {
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  blogGroup_draw(spl[1]);
               }
               else
               {
                  alert("انجام نشد");
               }
               script_loadingHide();
            },
            error: function() {
               script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
            }
         });
      } 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
      }
   });
}//-----------------------------------------------------------------------------
function blogGroup_active(id)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_blogGroup,
         data: {"requestName":"blogGroup_active","id":id},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#td_active_" + spl[1]).html(spl[2]);
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogGroup_new(parentId)
{
   try {
        script_loadingShow();
        $.ajax({
            url: url_blogGroup,
            data: {"requestName":"blogGroup_new", "parentId":parentId},
            method: "POST",
            success: function(result)
            {
               result=oEngine.response(result);
               var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
                  $("#layer_content").html(spl[1]);
               }
               else
               {
                  alert("انجام نشد!.");
                  alert(result);
               }
               script_loadingHide();
            },
            error: function() {
					alert("خطایی در اتصال رخ داده است");
               script_loadingHide();
            }
        });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogGroup_edit(parentId,id)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_blogGroup,
         data: {"requestName":"blogGroup_edit", "parentId":parentId, "id":id },
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogGroup_update(purpose,parentId,id)
{
   try {
      script_loadingShow();
      var form_data = new FormData();
		form_data.append("parentId",parentId);
		form_data.append("id",id);
		form_data.append("title",$("#txt_title").val());
		form_data.append("comment",$("#txt_comment").val());
		form_data.append("requestName","blogGroup_update");
		form_data.append("purpose",purpose);

      $.ajax({
         url: url_blogGroup,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               blogGroup_draw(spl[1]);
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
var url_blogSettings=oPath.manage("blog_bundle/controller/php/blogSettings_panel_admin.php");
function blogSettings_edit()
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_blogSettings,
         data: {"requestName":"blogSettings_edit"},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogSettings_update()
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_blogSettings,
         data: {'requestName':'blogSettings_update', 'activeOpine':$('#slct_activeOpine').val(), 'showDate':document.querySelector("#chk_showDate").checked },
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split('[|]');
            if(spl[0]=="ok")
            {
               blogSettings_edit();
					script_alert2('','با موفقیت ذخیره گردید','success');
            }
            else
            {
               alert('انجام نشد!.');
            }
            script_loadingHide();
         },
         error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
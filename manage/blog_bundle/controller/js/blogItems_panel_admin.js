var url_blogItems=oPath.manage("blog_bundle/controller/php/blogItems_panel_admin.php");
var isImageDelete=false;
oMyTag=[];

function blogItems_imgDel(itemId)
{
   if(isImageDelete==false)
   {
		script_confirm2("تصویر حذف شود ؟",'',function(){
			isImageDelete=true;
			$("#img_item").attr("src",oPath.asset("default/images/noImage.gif"));
			$("#i_itemImgDel").attr("class","fa fa-share fa-2x");
		});
   }
   else if(isImageDelete==true)
   {
      isImageDelete=false;
      $("#img_item").attr("src",oPath.manage("blog_bundle/data/images/itemThumb_" + itemId + ".jpg"));
      $("#i_itemImgDel").attr("class","fa fa-trash fa-2x");
   }
}//-----------------------------------------------------------------------------
function blogItems_search(obj)
{
   searchWord=$("#txt_search").val();
   if(searchWord != "")
		blogItems_draw({
			"active":obj.active,
			"trash":obj.trash,
			"page":obj.page,
			"searchWord":searchWord
		});
   else
		blogItems_draw({
			"active":obj.active,
			"trash":obj.trash,
			"page":obj.page,
			"searchWord":""
		});	
}//-----------------------------------------------------------------------------
function blogItems_draw(obj,active,searchWord) //{active, trash, page, searchWord}
{
	isImageDelete=false;
	if(obj=="auto")
	{
		obj={ 
			"page":-1,
			"active":-1,
			"trash":-1,
			"searchWord":-1 	
	   }
	}
	if(searchWord != undefined) obj.searchWord=searchWord;
	if(active != undefined) obj.active=active;
   try {
      script_loadingShow();
      $.ajax({
         url: url_blogItems,
         data: {"requestName":"blogItems_draw", "page":obj.page, "active":obj.active, "trash":obj.trash, "searchWord":obj.searchWord},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogItems_del(itemId) {
	script_confirm2("حذف شود ؟",'',function()
	{
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_blogItems,
					data: {"requestName": "blogItems_del","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							//{isSpecial,active,page,searchWord}
							blogItems_draw('auto');
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
		}		
	})
}//---------------------------------------------------------------------------------------
function blogItems_trash(itemId) {
	script_confirm2('',"موقتا حذف شود؟",function()
	{
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_blogItems,
					data: {"requestName": "blogItems_trash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							//{isSpecial,active,page,searchWord}
							blogItems_draw('auto');
							script_alert2('',"با موفقیت حذف شد","success");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
		}		
	})
}//---------------------------------------------------------------------------------------
function blogItems_unTrash(itemId) {
	script_confirm2('',"بازیابی شود؟",function()
	{
      try {
			if(itemId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=itemId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_blogItems,
					data: {"requestName": "blogItems_unTrash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							//{isSpecial,active,page,searchWord}
							blogItems_draw('auto');
							script_alert2('',"با موفقیت بازیابی شد","success");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} 
		catch (e) 
		{
			alert("خطای اسکریپت");
			script_loadingHide();
		}		
	});
}//---------------------------------------------------------------------------------------
function blogItems_active(itemId) //change active to unActive and reverse
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_blogItems,
         data: {"requestName":"blogItems_active","id":itemId},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					$("#td_active_" + spl[1]).html(spl[2]);     	
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogItems_edit(itemId) {
	try {
		script_loadingShow();
		isImageDelete=false;
		$.ajax({
			url: url_blogItems,
			data: {"requestName": "blogItems_edit","id":itemId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------
function blogItems_new() {
	try {
		script_loadingShow();
		isImageDelete=false;
		$.ajax({
			url: url_blogItems,
			data: {"requestName": "blogItems_new"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{	
					$("#layer_content").html(spl[1]);					
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() 
			{
				script_loadingHide();
				alert("خطایی در اتصال رخ داده است");
			}
		});
	} 
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function blogItems_update(purpose,itemId)
{
   //try {
		err=false;
		if($("#txt_title").val()=="")
		{
			script_alert2('',"عنوان را مشخص نمایید","danger");
			script_focus("txt_title");
			err=true;			
		}			
		//---
		layer_group=$('#layer_group input[type="checkbox"]');
		groupId='';
      for($i=0;$i < layer_group.length;$i++)
		{
			if(layer_group[$i].checked==true) groupId+= layer_group[$i].value + ",";
		}
		if(!groupId && err==false)
		{
			script_alert2('',"دسته بندی ها را مشخص نمایید","danger");
			script_focus("layer_group");
			err=true;			
		}	

		if(err==false)
		{
			script_loadingShow();
			var formData = new FormData();
			//---
			var content = CKEDITOR.instances.txt_content.getData();
			content.replace(/(?:\\[rn])+/g, "<br>");
			content=oTools.str_tagFitSend_encode(content);
			//---
			tags=oMyTag.get();
			keywords='';
			for($i=0;$i < tags.length;$i++)
			{
				keywords+=tags[$i].title + ',';
			}
			//---
			var file_imgItem = $('#file_imgItem').prop('files')[0];
			//---
			formData.append('imgDel',isImageDelete);
			formData.append('groupId',groupId);
			formData.append('title',$("#txt_title").val()); 
			formData.append('activeOpine',document.getElementById("chk_activeOpine").checked);
			formData.append('keywords',keywords);
			formData.append('comment',$("#txt_comment").val());
			formData.append('content',content);	
			formData.append('file_imgItem',file_imgItem);	
			formData.append('id',itemId);
			formData.append('purpose',purpose);
			formData.append('requestName',"blogItems_update");			
			//---
			$.ajax({
				url: url_blogItems,
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
					if(spl[0]=="!imageType")
					{
						if(spl[1]=="0") var num="اصلی"; else var num=spl[1];
						script_alert2('',"نوع تصویر غیر مجاز می باشد","danger");
						script_focus("img_item" + spl[1]);
					}
					else if(spl[0]=="!imageSize")
					{
						script_alert2('',"سایز فایل تصویر، بیش از حد مجاز است","danger");
					}
					else if(spl[0]=="ok")
					{
						purpose=spl[1];
						blogItems_draw('auto');

						if(purpose=="new") 
							message="مطلب جدید، با موفقیت ذخیره گردید";
						else
							message="مطلب با موفقیت ویرایش گردید";
						script_alert2('',message,"success");
					}
					else
					{
						alert("انجام نشد");
						alert(result);
					}
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		}
	//}
	//catch (e) 
	//{
		//alert("خطای اسکریپت");
		//script_loadingHide();
	//}
}//---------------------------------------------------------------------------------------
function blogItems_likesDraw(itemId,showSearch,page)
{
	searchWord="";
	if(showSearch==undefined) 
		showSearch="";
   else if(showSearch==1 || showSearch=="1" || showSearch==true) 
	{
      searchWord=$('#txt_search').val();
	}
	if(page == undefined || page <=0 ) page=1;
	
   try {
      script_loadingShow();
      $.ajax({
         url: url_blogItems,
         data: {"requestName":"blogItems_likesDraw", "itemId":itemId, "showSearch":showSearch, "searchWord":searchWord, "page":page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogItems_likeDel(itemId,likeId) {
	var ret=confirm("حذف شود ؟.");
	if(ret)
	{
		try {
			if(likeId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=likeId;
			if(id)
			{
				script_loadingShow();
				$.ajax({
					url: url_blogItems,
					data: {"requestName": "blogItems_likeDel","itemId":itemId,"id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						var spl=result.split("[|]");
						if(spl[0]=="ok")
						{
							//{shopId,showSearch,page}
							blogItems_likesDraw(spl[1],spl[2],spl[3]);
						}
						else
						{
							alert("انجام نشد");
							alert(result);
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
	}
}//---------------------------------------------------------------------------------------
function blogItems_opineDraw(itemId,confirm,checkOut,page)
{
	if(itemId=="auto")
	{
		itemId=-1;
		confirm=-1;
		checkOut=-1;
		page=-1;
	}

   try {
      script_loadingShow();
      $.ajax({
         url: url_blogItems,
         data: {"requestName":"blogItems_opineDraw", "itemId":itemId, "confirm":confirm, "checkOut":checkOut, "page":page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogItems_opineDel(opineId) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			if(opineId=="selected") //entekhab chand checkbox
			{
				ids="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) ids+=chkbox.value + ",";
				}
			}
			else
				ids=opineId;
			if(ids!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_blogItems,
					data: {"requestName": "blogItems_opineDel","ids":ids},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							blogItems_opineDraw("auto");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
    }
}//---------------------------------------------------------------------------------------
function blogItems_opineConfirm(opineId)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_blogItems,
         data: {"requestName":"blogItems_opineConfirm","id":opineId},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#td_active_" + spl[1]).html(spl[2]);
            }
            else
            {
               alert("انجام نشد");
            }
            script_loadingHide();
         },
         error: function() {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function blogItems_opineView(opineId) {
	try {
		script_loadingShow();
		$.ajax({
			url: url_blogItems,
			data: {"requestName": "blogItems_opineView","id":opineId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() 
			{
				script_loadingHide();
				alert("خطایی در اتصال رخ داده است");
			}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------
function blogItems_opineAnswer(opineId)
{
   try {
		err=false;
		if($("#txt_answer").val()=="")
		{
		   alert("خطا : متن پاسخ خالی است");
			document.querySelector("#txt_answer").scrollIntoView({ behavior: 'smooth' });
			document.getElementById("txt_answer").focus();
         err=true;			
		}
		if(err==false)
		{
			script_loadingShow();
			var formData = new FormData();
			formData.append('answerContent',$("#txt_answer").val());
			formData.append('confirm',$("#slct_confirm").val());
			formData.append('id',opineId);
			formData.append('requestName',"blogItems_opineAnswer");			

			$.ajax({
				url: url_blogItems,
				dataType: 'text',  // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
               if(result=="ok")
					{
					   blogItems_opineDraw("auto");
					}
					else
					{
						alert("انجام نشد");
						alert(result);
					}
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		}
	}
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------

//services 
function blogItems_opineNotification() {
	try {
		$.ajax({
			url: url_blogItems,
			data: {"requestName": "filmItemsOpine_notification"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
				   $("#spn_filmItemsOpine").html(spl[1]);
					if(spl[2]=="new") beep(3);
				}
			}
		});
	} 
	catch (e) {}
}//---------------------------------------------------------------------------------------
services_add("filmItemsOpine_notification",function(){filmItemsOpine_notification();});

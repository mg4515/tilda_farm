<?php
	class cTicket
	{
		public $table="ticket"; 
		public $tableMessage="ticketMessage"; 
		public $tableSettings="ticketSettings"; 

		function getAll($userId="",$visible="",$limitStr="") //in and out
		{
			global $oDbq;
			$where="";
			if($userId!="")
			{
				if($where=="") $where="(`firstFromId`={$userId} OR `firstToId`={$userId})"; else $where.=" AND (`firstFromId`={$userId} OR `firstToId`={$userId})";
			}
			if($visible!="")
			{
				if($where=="") $where="`visible`='{$visible}'"; else $where.=" AND `visible`='{$visible}'";
			}			
			$ret=$oDbq->table($this->table)->fields("*")->where($where . "AND `trash`='0'")->orderby("`dateUpdate` DESC")->limit($limitStr)->select();
			return $ret;
		}//------------------------------------------------------------------------------------
		function getAll_trash($userId="",$trash="0",$limitStr="") //in and out
		{
			global $oDbq;
			$where="";	
			if($userId!="")
			{
				if($where=="") $where="(`firstFromId`={$userId} OR `firstToId`={$userId})"; else $where.=" AND (`firstFromId`={$userId} OR `firstToId`={$userId})";
			}				
			$ret=$oDbq->table($this->table)->fields("*")->where($where . " AND `trash`='{$trash}'")->orderby("`dateUpdate` DESC")->limit($limitStr)->select();
			return $ret;
		}//------------------------------------------------------------------------------------		
		function getAll_inOut($fromId,$toId,$visible="",$limitStr="")
		{
			global $oDbq;
			$where="";
			if($visible!="")
			{
				if($where=="") $where="`visible`='{$visible}'"; else $where.=" AND `visible`='{$visible}'";
			}
			$ret=$oDbq->table($this->table)->fields("*")->where("(`firstFromId`={$fromId} OR `firstToId`={$toId}) AND `trash`='0' AND {$where}")->orderby("`dateUpdate` DESC")->limit($limitStr)->select();
			return $ret;
		}//------------------------------------------------------------------------------------
		function getAll_in($userId,$visible="",$limitStr="") //daryafty
		{
			global $oDbq;
			$where="";
			if($visible!="")
			{
				if($where=="") $where="`visible`='{$visible}'"; else $where.=" AND `visible`='{$visible}'";
			}
			$ret=$oDbq->table($this->table)->fields("*")->where("`firstToId`={$userId} AND `trash`='0' AND {$where}")->orderby("`dateUpdate` DESC")->limit($limitStr)->select();
			return $ret;
		}//------------------------------------------------------------------------------------
		function getAll_out($userId,$visible="",$limitStr="") //ersaly
		{
			global $oDbq;
			$where="";
			if($visible!="")
			{
				if($where=="") $where="`visible`='{$visible}'"; else $where.=" AND `visible`='{$visible}'";
			}
			$ret=$oDbq->table($this->table)->fields("*")->where("`firstFromId`={$userId} AND `trash`='0' AND {$where}")->orderby("`dateUpdate` DESC")->limit($limitStr)->select();
			return $ret;
		}//------------------------------------------------------------------------------------	
		function getAllByCheckout($userId,$checkOut="0",$visible="",$limitStr="") //ersaly
		{
			global $oDbq;
			$where="";
			if($visible!="")
			{
				if($where=="") $where="`visible`='{$visible}' AND "; else $where.=" AND `visible`='{$visible}'";
			}
			$ret=$oDbq->table($this->table)->fields("*")->where("{$where}`firstToId`={$userId} AND `trash`='0' AND `checkOut`='{$checkOut}'")->orderby("`dateUpdate` DESC")->limit($limitStr)->select();
			return $ret;
		}//------------------------------------------------------------------------------------		
		function get($ticketId)
		{
			global $oDbq;
			$item=$oDbq->table($this->table)->fields("*")->where("`id`='{$ticketId}'")->select()[0];
			return $item;
		}//--------------------------------------------------------------------------
		function delete($ticketId)
		{
			global $oDbq;		
			$oDbq->table($this->table)->where("`id`={$ticketId}")->delete();
			$oDbq->table($this->tableMessage)->where("`ticketId`={$ticketId}")->delete();
			
		}//--------------------------------------------------------------------------
		function insert($array)
		{
			global $oDbq;
			$id=time();
			if($array['status']=="")
				$oDbq->table($this->table)->set("
			                                       `id`={$id},
																`firstFromId`={$array['fromId']},
																`firstToId`={$array['toId']},
																`subject`='{$array['subject']}',
																`dateUpdate`='{$array['dateUpdate']}',
																`visible`='{$array['visible']}'"
															  )->insert();
			else
				$oDbq->table($this->table)->set("
			                                       `id`={$id},
																`firstFromId`={$array['fromId']},
																`firstToId`={$array['toId']},
																`subject`='{$array['subject']}',
																`dateUpdate`='{$array['dateUpdate']}',
																`status`='{$array['status']}',
																`visible`='{$array['visible']}'"
															  )->insert();			
			return $id;
		}//--------------------------------------------------------------------------
		public function isExists_byFileId($fileId)
		{
			global $oDbq;
			$items=$oDbq->table($this->table)->fields("*")->where("`purposeType_fileId`='{$fileId}' AND `trash`='0'")->select();		   	
		   return count($items);
		}//--------------------------------------------------------------------------
		public function getTrash($id)
		{
			global $oDbq;
			$ret=$oDbq->table($this->table)->fields("`trash`")->where("`id`={$id}")->select();
			if(count($ret) > 0)
			{
				if($ret[0]->trash=="1") return true;
			}
			else
				return false;
		}//---------------------------------------------------------------------------------
		public function setTrash($id,$value)
		{
			global $oDbq;
			$oDbq->table($this->table)->set("`trash`='{$value}'")->where("`id`={$id}")->update();
		}//--------------------------------------------------------------------------------- 		
		public function getCheckOut($id)
		{
			global $oDbq;
			$ret=@$oDbq->table($this->table)->fields("`checkOut`")->where("`id`={$id}")->select()[0];
			if($ret->checkOut=="1")
				return true;
			else
				return false;
		}//---------------------------------------------------------------------------------
		public function setCheckOut($id,$value)
		{
			global $oDbq;
			$oDbq->table($this->table)->set("`checkOut`={$value}")->where("`id`={$id}")->update();
		}//---------------------------------------------------------------------------------   
		public function getStatus($id)
		{
			global $oDbq;
			$ret=$oDbq->table($this->table)->fields("`status`")->where("`id`={$id}")->select();
			if(count($ret) > 0)
				return $ret[0];
			else
				return false;
		}//---------------------------------------------------------------------------------
		public function setStatus($id,$value)
		{
			global $oDbq;
			$oDbq->table($this->table)->set("`status`='{$value}'")->where("`id`={$id}")->update();
		}//---------------------------------------------------------------------------------  	
		public function getLock($id)
		{
			global $oDbq;
			$ret=@$oDbq->table($this->table)->fields("`lock`")->where("`id`={$id}")->select()[0]->lock;
			return $ret;
		}//---------------------------------------------------------------------------------
		public function setLock($id,$value)
		{
			global $oDbq;
			$oDbq->table($this->table)->set("`lock`='{$value}'")->where("`id`={$id}")->update();
		}//---------------------------------------------------------------------------------  	
		public function getVisible($id)
		{
			global $oDbq;
			$ret=$oDbq->table($this->table)->fields("`visible`")->where("`id`={$id}")->select();
			if(count($ret) > 0)
				return $ret[0];
			else
				return false;
		}//---------------------------------------------------------------------------------
		public function setVisible($id,$value)
		{
			global $oDbq;
			$oDbq->table($this->table)->set("`visible`='{$value}'")->where("`id`={$id}")->update();
		}//--------------------------------------------------------------------------------- 		
		function getCount()
		{
			global $oDbq;
			return count($oDbq->table($this->table)->fields("`id`")->where("`check`=0")->select());		
		}//--------------------------------------------------------------------------	
		function messageGetAll($ticketId,$limitStr="")
		{
			global $oDbq;
			$ret=$oDbq->table($this->tableMessage)->fields("*")->where("`ticketId`={$ticketId}")->orderby("`id` DESC")->limit($limitStr)->select();
			return $ret;
		}//------------------------------------------------------------------------------------
		function messageGet($id)
		{
			global $oDbq;
			$item=$oDbq->table($this->tableMessage)->fields("*")->where("`id`='{$id}'")->select()[0];
			return $item;
		}//--------------------------------------------------------------------------
		function messageDelete($id)
		{
			global $oDbq;		
			$oDbq->table($this->tableMessage)->where("`id`={$id}")->delete();
			
		}//--------------------------------------------------------------------------
		function messageDeleteByTicketId($ticketId)
		{
			global $oDbq;	
         $ret= $this->messageGetAll($ticketId);			
			$oDbq->table($this->tableMessage)->where("`ticketId`={$ticketId}")->delete();
			return $ret;
		}//--------------------------------------------------------------------------	
		function messageGetCount($userId)
		{
			global $oDbq;
			return count($oDbq->table($this->tableMessage)->fields("`id`")->where("`checkOut`=0 AND `toId`='{$userId}'")->select());		
		}//--------------------------------------------------------------------------	
		function messageGetCountByTicketId($ticketId)
		{
			global $oDbq;
			return count($oDbq->table($this->tableMessage)->fields("`id`")->where("`checkOut`=0 AND `ticketId`='{$ticketId}'")->select());		
		}//--------------------------------------------------------------------------	
		function messageInsert($ticketId,$id,$fromId,$toId,$message,$attachName="")
		{
			global $oDbq;
			$oDbq->table($this->tableMessage)->set("`id`={$id},`ticketId`={$ticketId},`fromId`={$fromId},`toId`={$toId},`message`='{$message}',`attachName`='{$attachName}'")->insert();		
			$oDbq->table($this->table)->set("`dateUpdate`={$id},`firstToId`={$toId},`firstFromId`={$fromId}")->where("`id`={$ticketId}")->update();
			return $id;
		}//--------------------------------------------------------------------------
		public function messagesSetCheckOut($id,$value)
		{
			global $oDbq;
			$oDbq->table($this->tableMessage)->fields("`checkOut`")->values("'{$value}'")->where("`id`={$id}")->update();
		}//---------------------------------------------------------------------------------  	
		public function messageSetCheckOutByTicketId($ticketId,$value)
		{
			global $oDbq;
			//$oDbq->table($this->tableMessage)->fields("`checkOut`")->values("'{$value}'")->where("`id`={$ticketId}")->update();
			$oDbq->table($this->tableMessage)->fields("`checkOut`")->values("'{$value}'")->where("`ticketId`={$ticketId}")->update();
		}//--------------------------------------------------------------------------------- 
		function settingsGet()
		{
			global $oDbq;
			$item=$oDbq->table($this->tableSettings)->fields("*")->select();
			if($item) return $item[0]; else return [];
		}//--------------------------------------------------------------------------










		function operatorGetAll($limitStr="")
		{
			global $oDbq;
			$ret=$oDbq->table($this->tableOperator)->fields("*")->orderby("`id` DESC")->limit($limitStr)->select();
			return $ret;
		}//------------------------------------------------------------------------------------
		function operatorGet($id)
		{
			global $oDbq;
			$item=$oDbq->table($this->tableOperator)->fields("*")->where("`id`='{$id}'")->select()[0];
			return $item;
		}//--------------------------------------------------------------------------
		function operatorDelete($id)
		{
			global $oDbq;		
			$oDbq->table($this->tableOperator)->where("`id`={$id}")->delete();
			
		}//--------------------------------------------------------------------------			
		function operatorInsert($userId,$title)
		{
			global $oDbq;
			$id=time();
			$oDbq->table($this->tableOperator)->set("`id`={$id},`userId`={$userId},`title`='{$title}'")->insert();		
			return $id;
		}//--------------------------------------------------------------------------
		function operatorUpdate($id,$userId,$title)
		{
			global $oDbq;
			$oDbq->table($this->tableOperator)->set("`userId`={$userId},`title`={$title}")->where("`id`={$id}")->insert();		
			return $id;
		}//--------------------------------------------------------------------------				
	}
?>
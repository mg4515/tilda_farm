<?php
   class cTicketAdminFroms
   {
      private $table="ticketAdminFroms";
      function get($id)
      {
         global $oDbq;
         $item=$oDbq->table($this->table)->fields("*")->where("`id`={$id}")->select()[0];
         return $item;
      }//-----------------------------------------------------------------------
      function gets($count=5,$page=0)
      {
         global $oDbq;
         if($page!=0) $page=$page*$count;
         $items=$oDbq->table($this->table)->fields("*")->limit("{$page},{$count}")->select();
         return $items;
      }//-----------------------------------------------------------------------
      function getAll()
      {
         global $oDbq;
         $items=$oDbq->table($this->table)->fields("*")->select();
         return $items;
      }//-----------------------------------------------------------------------
      function insert($id,$adminId,$title)
      {
         global $oDbq;
         $oDbq->table($this->table)->fields("`id`,`adminId`,`title`")->values("'{$id}','{$adminId}','{$title}'")->insert();
      }//-----------------------------------------------------------------------
      function update($id,$adminId,$title)
      {
         global $oDbq;
         $oDbq->table($this->table)->fields("`id`,`adminId`,`title`")->values("'{$id}','{$adminId}','{$title}'")->where("`id`={$id}")->update();
      }//-----------------------------------------------------------------------
      function delete($id)
      {
         global $oDbq;
         $oDbq->table($this->table)->fields("*")->where("`id`={$id}")->delete();
      }//-----------------------------------------------------------------------    
	}
?>
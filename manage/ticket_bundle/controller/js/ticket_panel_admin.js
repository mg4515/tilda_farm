var url_ticket=oPath.manage("ticket_bundle/controller/php/ticket_panel_admin.php");
var oMyList={};
var oMyTag={};

function ticket_draw(drawType,userId,page) {
	try {
	   script_loadingShow();
		if(drawType==undefined) drawType=$("#slct_drawType").val();
		if(drawType=="auto")
		{
		   drawType= -1;	
		   userId= -1;	
		   page= -1;	
		}
	   $.ajax({
			url: url_ticket,
			data: {"requestName":"ticket_draw","drawType":drawType, "userId":userId, "page":page},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} 
	catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticket_del(id) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"requestName":"ticket_del","id":id},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						ticket_draw("auto");
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
      }
		catch (e) 
		{
         alert("خطای اسکریپت");
			script_loadingHide();
      }
	}
}//---------------------------------------------------------------------------------------
function ticket_trash(id) {
   var ret=confirm("موقتا حذف شود؟");
   if(ret)
   {
      try {
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"requestName":"ticket_trash","id":id},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					if(result=="ok")
					{
						ticket_draw("auto");
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
      }
		catch (e) 
		{
         alert("خطای اسکریپت");
			script_loadingHide();
      }
	}
}//---------------------------------------------------------------------------------------
function ticket_unTrash(id) {
   var ret=confirm("بازیابی شود؟");
   if(ret)
   {
      try {
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"requestName":"ticket_unTrash","id":id},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					if(result=="ok")
					{
						ticket_draw("auto");
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
      }
		catch (e) 
		{
         alert("خطای اسکریپت");
			script_loadingHide();
      }
	}
}//---------------------------------------------------------------------------------------
function ticket_lock(id,lock)
{
   try {
		if(lock==1) msg='این درخواست بسته شود؟'; else msg='این درخواست باز شود؟';
		script_confirm2('', msg, function()
		{
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"requestName":"ticket_lock","id":id, "lock":lock},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						ticket_messageView(spl[1]);
						script_alert2('','با موفقیت انجام شد','success');
					}
					else
						alert("انجام نشد");
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		});
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function ticket_status(id,status)
{
   try {
		script_confirm2('', 'وضعیت تغییر کند؟', function()
		{
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"requestName":"ticket_status","id":id, 'status':status},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						script_alert2('','با موفقیت انجام شد','success');
					}
					else
						alert("انجام نشد");
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		});
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function ticket_new() {
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"requestName":"ticket_new"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
					oMyTag=myTag({
					   "elementId":"ul_toTags",	
					});
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticket_update() {
	var tags=oMyTag.get();
	if(tags.length > 0)
	{
   //try {
      script_loadingShow();
		var toIds="";
		var count=tags.length;
		for(i=0;i < count;i++)
		{
		   toIds+=tags[i].dataValue1 + ",";	
		}
      var form_data = new FormData();
		form_data.append("file_attach",$('#file_attach').prop('files')[0]);
		form_data.append("toIds",toIds);
		form_data.append("subject",$("#txt_subject").val());
		form_data.append("message",$("#txt_message").val());
		form_data.append("requestName","ticket_update");
      $.ajax({
         url: url_ticket,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            if(result=="ok")
            {
               ticket_draw("auto");
            }
				else if(result=="errType" || result=="errSize" || result=="errUpload")
				{
				   alert("فایل الصاقی نامعتبر است");
					script_loadingHide();					
				}
				else
				{
				   alert("انجام نشد");	
					script_loadingHide();
				}
         },
         error: function()
         {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   /*} catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }*/
	}
	else
	{
	   alert("هیچ گیرنده ای وجود ندارد");	
	}
}//---------------------------------------------------------------------------------------
function ticket_toListDraw(searchWord,selectedIds,multiSelect,page) //users list
{
   //try { 
      script_loadingShow();
      $.ajax({
         url: url_users,
         data: {"requestName":"users_listDraw", "searchWord":searchWord, "selectedIds":selectedIds, "multiSelect":multiSelect, "page":page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					var codeCenter=spl[1];
					var searchWord=spl[2];
					dialog=new myModal({
						'showBottomBar':true,
						'topBarStyle': "float:left;font-size:12px;",
						'title': "کاربران",
						'centerContent': "" +						
						   "<div style='margin-top:32px;position:relative'>" + 						
							   codeCenter + 
							"</div>"+
							"<div class='input-control input-control-right' style='position:fixed;height: 46px;margin-top: 45px;top:0px;left:5px;right:0px;padding:7px 10px 0px 10px;background-color:rgba(255,255,255,0.9);'>" + 
							   "<div class='input-control input-control-right'>" + 
								   "<span class='fa fa-search input-control-button' onclick='ticket_toListDrawSearch();'></span>" +
								   "<input type='text' id='txt_usersSearch' style='background-color:#fff' value='" + searchWord + "' placeholder='جستجو ...'>"+						
							   "</div>"+
							"</div>",
						'bottomBarContent': '<div style="padding-right:5px"><button id="btn_myListOk" class="btn btn-success" onclick="myList_to_myTag(oMyList,oMyTag);dialog.hide();">تایید</button></div>',						
					});
					dialog.show();
					oMyList=myList({
						"elementId":"ul_usersList",
						"multiSelect":true,
						"onSelected":function(a,b)
						{
							if(b.getSelected().length > 0)
							{
								document.getElementById("btn_myListOk").disabled=false;
								document.getElementById("btn_myListOk").innerHTML="تایید";
							}
							else
							{
								document.getElementById("btn_myListOk").disabled=true;
							   document.getElementById("btn_myListOk").innerHTML="تایید";
						   }
						}
					});
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   //}
   //catch (e)
   //{
		//alert("خطای اسکریپت");
		//script_loadingHide();
   //}
}//-----------------------------------------------------------------------------
function ticket_toListDrawSearch() //to
{
	dialog.hide();
	var searchWord=$("#txt_usersSearch").val();
	ticket_toListDraw(searchWord,"","","");
}//-----------------------------------------------------------------------------
function ticket_messageView(ticketId) {
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"requestName":"ticket_messageView", "ticketId":ticketId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
				   alert("انجام نشد");	
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticket_messageSend(ticketId,toId) {
   try {
      script_loadingShow();
      var form_data = new FormData();
		form_data.append("fileAttach",$('#fileAttach').prop('files')[0]);
		form_data.append("ticketId",ticketId);
		form_data.append("toId",toId);
		form_data.append("message",$("#txt_message").val());
		form_data.append("requestName","ticket_messageSend");
      $.ajax({
         url: url_ticket,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               ticket_messageView(spl[1])
            }
				else if(result=="errType" || result=="errUpload")
				{
				   alert("فایل الصاقی نامعتبر است");	
					script_loadingHide();
				}
				else
				{
				   alert("انجام نشد");
					script_loadingHide();					
				}
            
         },
         error: function()
         {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//---------------------------------------------------------------------------------------
function ticketOperator_draw() 
{
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"requestName":"ticketOperator_draw"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} 
	catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticketOperator_del(id) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"requestName":"ticketAdminFroms_del","id":id},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						ticketOperator_draw();
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
      }
		catch (e) 
		{
         alert("خطای اسکریپت");
			script_loadingHide();
      }
	}
}//---------------------------------------------------------------------------------------
function ticketOperator_edit(id) 
{
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"requestName":"ticketOperator_edit","id":id},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} 
	catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticketOperator_new() 
{
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"requestName":"ticketOperator_new"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				else
				{
				   alert("انجام نشد");	
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} 
	catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticketAdminFroms_update(purpose,id) {
   try {
      script_loadingShow();
      var form_data = new FormData();
      form_data.append('data', oEngine.request({
			"purpose":purpose,
			"id":id,
			"title":$("#txt_title").val(),
			"adminId":$("#slct_adminId").val(),
			"requestName":"ticketAdminFroms_update"
		}));
      $.ajax({
         url: url_ticket,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               ticketAdminFroms_draw()
            }
				else
				{
				   alert("انجام نشد");	
				}
            script_loadingHide();
         },
         error: function()
         {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//---------------------------------------------------------------------------------------


//services 
function ticket_notification() {
	try {
		$.ajax({
			url: url_ticket,
			data: {"requestName": "ticket_notification"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
				   $("#spn_ticket").html(spl[1]);
					if(spl[2]=="new") beep(3);
				}
			}
		});
	} catch (e) {}
}//---------------------------------------------------------------------------------------
services_add("ticket_notification",function(){ticket_notification();});
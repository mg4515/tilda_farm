var url_ticket=oPath.manage("ticket_bundle/controller/php/ticket_panel_client.php");
function ticket_draw(drawType,userId,page) {
	try {
	   script_loadingShow();
		if(drawType==undefined) drawType=$("#slct_drawType").val();
		if(drawType=="auto")
		{
			drawType= -1;
		   userId= -1;	
		   page= -1;	
		}
	   $.ajax({
			url: url_ticket,
			data: {"requestName":"ticket_draw","drawType":drawType, "userId":userId, "page":page},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} 
	catch (e) 
	{
      alert("خطای اسکریپت");
	  script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticket_del(id) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"requestName":"ticket_del","id":id},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						ticket_draw("auto");
					}
					else
					{
						alert("انجام نشد");
						alert(result);
					}
					script_loadingHide();
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
      }
		catch (e) 
		{
         alert("خطای اسکریپت");
			script_loadingHide();
      }
	}
}//---------------------------------------------------------------------------------------
function ticket_lock(id,lock)
{
   try {
		if(lock==1) msg='این درخواست بسته شود؟'; else msg='این درخواست باز شود؟';
		script_confirm2('', msg, function()
		{
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"requestName":"ticket_lock","id":id, "lock":lock},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						ticket_messageView(spl[1]);
						script_alert2('','با موفقیت انجام شد','success');
					}
					else
						alert("انجام نشد");
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		});
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function ticket_unLock(id) {
   var ret=confirm("باز شود ؟");
   if(ret)
   {
      try {
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"data": oEngine.request({"requestName":"ticket_unLock","id":id}) },
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						ticket_messageView(spl[1]);
					}
					else
					{
						alert("انجام نشد");
						alert(result);
					}
					script_loadingHide();
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
      }
		catch (e) 
		{
         alert("خطای اسکریپت");
			script_loadingHide();
      }
	}
}//---------------------------------------------------------------------------------------
function ticket_new() {
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"requestName":"ticket_new"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
					script_loadingHide();
				}
				else
				{
					alert("انجام نشد");
				   script_loadingHide();
				}
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticket_update() {
   //try {
      script_loadingShow();
      var form_data = new FormData();
		form_data.append("file_attach",$('#file_attach').prop('files')[0]);
		//form_data.append("firstToId",$("#slct_usersId").val());
		form_data.append("subject",$("#txt_subject").val());
		form_data.append("message",$("#txt_message").val());
		form_data.append("requestName","ticket_update");
      $.ajax({
         url: url_ticket,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               ticket_draw("auto");
            }
				else if(spl[0]=="errType" || spl[0]=="errUpload")
				{
				   alert("فایل الصاقی نامعتبر است");
					script_loadingHide();					
				}
				else
				{
				   alert("انجام نشد");	
					script_loadingHide();
				}
         },
         error: function()
         {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   /*} catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }*/
}//---------------------------------------------------------------------------------------
function ticket_messageView(ticketId) {
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"requestName":"ticket_messageView", "ticketId":ticketId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticket_messageSend(ticketId,toId) {
   try {
      script_loadingShow();
      var form_data = new FormData();
		form_data.append("fileAttach",$('#fileAttach').prop('files')[0]);
		form_data.append("ticketId",ticketId);
		form_data.append("toId",toId);
		form_data.append("message",$("#txt_message").val());
		form_data.append("requestName","ticket_messageSend");
      $.ajax({
         url: url_ticket,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               ticket_messageView(spl[1])
            }
				else if(result=="errType" || result=="errUpload")
				{
				   alert("فایل الصاقی نامعتبر است");	
					script_loadingHide();
				}
				else
				{
				   alert("انجام نشد");
					script_loadingHide();					
				}
            
         },
         error: function()
         {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//---------------------------------------------------------------------------------------
function ticketAdminFroms_draw() 
{
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"data": oEngine.request({"requestName":"ticketAdminFroms_draw"}) },
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} 
	catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticketAdminFroms_del(id) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			script_loadingShow();
			$.ajax({
				url: url_ticket,
				data: {"data": oEngine.request({"requestName":"ticketAdminFroms_del","id":id}) },
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						ticketAdminFroms_draw();
					}
					else
					{
						alert("انجام نشد");
						alert(result);
					}
					script_loadingHide();
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
      }
		catch (e) 
		{
         alert("خطای اسکریپت");
			script_loadingHide();
      }
	}
}//---------------------------------------------------------------------------------------
function ticketAdminFroms_edit(id) 
{
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"data": oEngine.request({"requestName":"ticketAdminFroms_edit","id":id}) },
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} 
	catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticketAdminFroms_new() 
{
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_ticket,
			data: {"data": oEngine.request({"requestName":"ticketAdminFroms_new"}) },
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} 
	catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function ticketAdminFroms_update(purpose,id) {
   try {
      script_loadingShow();
      var form_data = new FormData();
      form_data.append('data', oEngine.request({
			"purpose":purpose,
			"id":id,
			"title":$("#txt_title").val(),
			"adminId":$("#slct_adminId").val(),
			"requestName":"ticketAdminFroms_update"
		}));
      $.ajax({
         url: url_ticket,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
				var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               ticketAdminFroms_draw()
            }
				else
				{
				   alert("انجام نشد");	
				}
            script_loadingHide();
         },
         error: function()
         {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//---------------------------------------------------------------------------------------


//services 
function ticket_notification() {
	try {
		$.ajax({
			url: url_ticket,
			data: {"requestName": "ticket_notification"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
				   $("#spn_ticket").html(spl[1]);
					if(parseInt(spl[1]) > 0)
					{
						$("#spn_mnuPayKifPool").html(spl[1]);
						$("#spn_mnuPayKifPool").addClass("mtree-counter-bg");
					}					
					if(spl[2]=="new") beep(3);
				}
			}
		});
	} catch (e) {}
}//---------------------------------------------------------------------------------------
services_add("ticket_notification",function(){ticket_notification();});
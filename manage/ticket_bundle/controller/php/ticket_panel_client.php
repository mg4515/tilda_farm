<?php
   @session_start();
	include_once $_SESSION["engineRequire"];
   include_once $oPath->manageDir('jdf.php');
   require_once $oPath->manageDir('ticket_bundle/model/ticket_model.php');
   require_once $oPath->manageDir('admin_bundle/model/admin_model.php');
   require_once $oPath->manageDir('users_bundle/model/users_model.php');

   $oTicket=new cTicket();
   $oAdmin=new cAdmin();
   $oUsers=new cUsers();
	
	$requestName=@$_REQUEST["requestName"]; //request name
	//END  request check -------------------------------------------
	
	$lockTitle=["باز است","بسته است"];
	$lockColor=["fg-success","fg-danger"];
	$lockIcon=["fa fa-unlock","fa fa-lock"];
	$statusTitle=["خوانده نشده","خوانده شده","در حال بررسی","پاسخ داده شده"];
	$statusColor=["fg-danger","fg-warning","fg-info","fg-success"];
	
	//request process
   if($requestName=="ticket_draw")
   {
		$page=isset($_REQUEST["page"]) ? $oDb->escape($_REQUEST["page"]) : 1;
		$drawType=isset($_REQUEST['drawType']) ? $oDb->escape($_REQUEST['drawType']) : "all"; //in or out or all or user
		$userId=isset($_REQUEST['userId']) ? $oDb->escape($_REQUEST['userId']) : ""; //if $drawType='user' then
		$purposeType=isset($_REQUEST['purposeType']) ? $oDb->escape($_REQUEST['purposeType']) : ""; //if $drawType='user' then
		
		//auto value
		if($page==-1) $page=$_SESSION["ticket_page"];
		if($drawType==-1) $drawType=$_SESSION["ticket_drawType"];
		if($userId==-1) $userId=$_SESSION["ticket_userId"];
		if($purposeType==-1) $purposeType=$_SESSION["ticket_purposeType"];
		
		//save session
		$_SESSION["ticket_page"]=$page;
		$_SESSION["ticket_drawType"]=$drawType;
		$_SESSION["ticket_userId"]=$userId;
		$_SESSION["ticket_purposeType"]=$purposeType;
		
		$me=$_SESSION['user_id'];
		
      $i=0;
      $code="";
      $codeTr="";
      
      //START PAGE ...................................
		if($drawType=="in") $items=$oTicket->getAll_in($me,"1");
		else if($drawType=="out") $items=$oTicket->getAll_out($me,"1");
		else if($drawType=="all") $items=$oTicket->getAll($me,"1");
		$count=count($items);
      $myPage=$page;
		$pages=cTools::misc_sqlLimit($count,10,10,$myPage);
      $btnPageCode="";
      if($pages["pagesEnd"] > 1)
		{
			for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
			{
				if($i==$myPage) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn btn-fix-36 btn-default'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn btn-fix-36 bg-sky' onclick='ticket_draw(\"{$drawType}\",\"{$userId}\",{$i});'>{$i}</a>";
			}
			$back=$myPage - 1;
			$next=$myPage + 1;
			if($pages["isPrevious"]) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='ticket_draw(\"{$drawType}\",\"{$userId}\",{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages["isNext"]) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='ticket_draw(\"{$drawType}\",\"{$userId}\",{$next});'><i class='fa fa-angle-left'></i></a>";
      }
		//---

		if($drawType=="in") $items=$oTicket->getAll_in($me,"1",$pages['sqlLimit']); //inbox
		else if($drawType=="out") $items=$oTicket->getAll_out($me,"1",$pages['sqlLimit']); //outbox
		else if($drawType=="all") $items=$oTicket->getAll($me,"1",$pages['sqlLimit']);
		else if($drawType=="user") $items=$oTicket->getAll_inOut($me,$userId,"1",$pages['sqlLimit']);		
		//END PAGE ...................................
		
      foreach($items as $item)
      {
         $i++;
			$date=jdate("Y/m/d h:i:s",$item->id);
			$dateUpdate=jdate("Y/m/d h:i:s",$item->dateUpdate);

			//toUser
			$toUser=$oUsers->get($item->firstToId);
			if(!$toUser) $toUser=$oAdmin->get($item->firstToId,true)["profile"];
			if($toUser->userType=="admin")
			   $userTitle='مدیریت';
			else
			{
			   $userTitle=$toUser->userName;
            if($toUser->fName != "" || $toUser->lName != "") $userTitle.=" - {$toUser->fName} {$toUser->lName}";				
			}
			if($me == $toUser->id) $userTitle="خودم"; 
			$toUser=@"<a href='javascript:void(0)' onclick=''>{$userTitle}</a>";			
			
			//fromUser
			$fromUser=@$oUsers->get($item->firstFromId);
			if(!$fromUser) $fromUser=@$oAdmin->get($item->firstFromId,true)["profile"];
			if($fromUser->userType=="admin")
			   $userTitle='مدیریت';
			else
			{
			   $userTitle=$fromUser->userName;
            if($fromUser->fName != "" || $fromUser->lName != "") $userTitle.=" - {$fromUser->fName} {$fromUser->lName}";				
			}
			if($me == $fromUser->id) $userTitle="خودم"; 
		   $fromUser=@"<a href='javascript:void(0)' onclick=''>{$userTitle}</a>";	
		   
			//checkout
			if($item->checkOut=="0") $style="style='font-weight: bolder;color:#000;font-size:17px;'"; else $style="color:#666";
         
			//status
			if($item->lock==1)
			{
				$statusT="<b>{$lockTitle[1]}</b>";
				$statusC='fg-black';
			}
			else
			{
				if($item->firstToId==$_SESSION['user_id'] && $item->status==1)
					$statusT="<b>خوانده ام</b>";
				else
				   $statusT="<b>{$statusTitle[$item->status]}</b>";
				$statusC=$statusColor[$item->status];
			}
			
			$codeTr.="
			<tr>
				<td {$style} class='{$lockColor[$item->lock]}'><i class='{$lockIcon[$item->lock]}'></i></td>
				<td {$style}>{$item->id}</td>
				<td {$style}>{$item->subject}</td>
				<td {$style} class='hide-auto'>{$date}</td>
				<td {$style}>{$dateUpdate}</td>
				<td {$style} class='{$statusC}'>{$statusT}</td>
				<td {$style}>
				   <button class='btn btn-info' onclick='ticket_messageView({$item->id})'><i class='fa fa-eye'></i></button>
				</td>
         </tr>";
      }
		
		if($_SESSION["ticket_drawType"]=="all")
		{
		   $drawTypeOptn="<option value='all' selected>همه پیغام ها</option>";	
		   $drawTypeOptn.="<option value='in' >پیغام های دریافتی</option>";	
		   $drawTypeOptn.="<option value='out' >پیغام های ارسالی</option>";	
		}
		else if($_SESSION["ticket_drawType"]=="in")
		{
		   $drawTypeOptn="<option value='all' >همه پیغام ها</option>";	
		   $drawTypeOptn.="<option value='in' selected>پیغام های دریافتی</option>";	
		   $drawTypeOptn.="<option value='out' >پیغام های ارسالی</option>";	
		}
		else if($_SESSION["ticket_drawType"]=="out")
		{
		   $drawTypeOptn="<option value='all' >همه پیغام ها</option>";	
		   $drawTypeOptn.="<option value='in' >پیغام های دریافتی</option>";	
		   $drawTypeOptn.="<option value='out' selected>پیغام های ارسالی</option>";	
		}
		
      $code= "
      <div class='vSpace-4x'></div>
      <h1><i class='fa fa-envelope'></i>&nbsp;پشتیبانی - پیغام ها</h1>
		<div class='vSpace-4x'></div>
		
		<button class='btn btn-success' onclick='ticket_new();'><i class='fa fa-plus'></i>&nbsp;جدید</button>
		&nbsp;|&nbsp;
		<select id='slct_drawType' onchange='ticket_draw();'>{$drawTypeOptn}</select>
		";
		
		if($codeTr=="") 
		{
		   $code.="<h1 style='color:#ccc;text-align:center'><i class='fa fa-envelope'></i>&nbspپیغامی وجود ندارد</h1>";	
		}
		else
		{
			$code.="
			<div class='vSpace'></div>
			<table class='tbl tbl-zebra tbl-bordered tbl-hover' style='direction:rtl'>
				<tr>
					<th class='text-right'><i class='fa fa-lock'></i></th>
					<th class='text-right'>شناسه درخواست</th>
					<th class='text-right'>موضوع</th>
					<th class='text-right'>تاریخ ارسال</th>
					<th class='text-right'>تاریخ تغییرات</th>
					<th class='text-right'>وضعیت</th>
					<th class='text-right'></th>
				</tr>
				{$codeTr}
			</table><br>
			{$btnPageCode}
			<br><br>";
		}
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="ticket_lock")
   {
      $id=cDataBase::escape($_REQUEST["id"]);
      $lock=cDataBase::escape($_REQUEST["lock"]);
      $oTicket->setLock($id,$lock);	
      cEngine::response("ok[|]{$id}[|]{$lock}");
   }//------------------------------------------------------------------------------------	
   else if($requestName=="ticket_new")
   {		
      $ticketSettings=$oTicket->settingsGet();
		if($ticketSettings) 
		{
			$fileTypes=$ticketSettings->fileTypes;
			if($ticketSettings->fileAttach=="1") $fileAttach="show"; else $fileAttach="hide";
		}
		else 
		{
		   $fileTypes=="all";
			$fileAttach="hide";
		}
		
		if($fileTypes=="all") 
		{
			$fileTypesTitle="محدودیتی وجود ندارد";
		}
		else
		{
		   $fileTypes=explode("|",$ticketSettings->fileTypes);
			$fileTypesTitle=implode(" , ",$fileTypes);
		}
		
      $code= "
      <div class='vSpace-4x'></div>
      <h1><i class='fa fa-plus'></i>&nbsp;ارسال پیغام جدید</h1>
		<div class='vSpace'></div>
      
		<div class='form' style='direction:rtl;text-align:right'>
			<br>";
		
		$code.="	
	      <label>عنوان پیغام</label>
	      <input type='text' id='txt_subject' placeholder='عنوان پیغام' />

			<i class='vSpace'></i>
			
         <label>متن پیغام - 128 کاراکتر</label>
			<textarea id='txt_message' placeholder='متن پیغام'></textarea>

			<br><br>
			<div class='{$fileAttach}'>
				<label>نوع فایل های مجاز : {$fileTypesTitle}</label>
				<label class='lbl-file  lbl-file-right'>
					<input type='file' id='file_attach' onchange='' required >
					<span class='lbl-file-icon'></span>
					<span>انتخاب فایل</span>
				</label>	
			</div>
         <hr>
			<button class='btn' onclick='ticket_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
			<button class='btn btn-success' onclick='ticket_update();'>ارسال</button>         
      </div>		
      <div class='vSpace-4x'></div>
		";
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="ticket_update")
   {
      $subject=htmlspecialchars($oDb->escape($_REQUEST["subject"]));
      $firstFromId=$_SESSION["user_id"];
      $fromId=$firstFromId;
		$toIds=[$oAdmin->getGeneralManager()['admin']->id];
		$count=count($toIds);
		for($i=0;$i < $count;$i++)
		{
			$firstToId=$toIds[$i];
			$toId=$toIds[$i];
			$dateUpdate=time();
			$message=htmlspecialchars($oDb->escape($_REQUEST["message"]));
			$messageId=time();

			$ticketSettings=$oTicket->settingsGet();
			if($ticketSettings->fileTypes=="all") 
				$fileTypes=["all"];
			else
			{
				$fileTypes=explode("|",$ticketSettings->fileTypes);	
			}
			
			//attach
			$attachName="";
			if(count($_FILES) > 0 && $ticketSettings->fileAttach=="1")
			{
				//file
				$extension= pathinfo($_FILES['file_attach']['name'], PATHINFO_EXTENSION);
				$fileName=$_FILES['file_attach']['name'];
				$fileType=explode(".",$extension);
				$fileType=$fileType[count($fileType)-1];
				$fileType=strtolower($fileType);

				if(filesize($_FILES['file_attach']['tmp_name']) * 1024 > $ticketSettings->fileSize)
				{
					$oEngine->response('errSize');
					exit;
				}
				if($fileTypes[0]=="all" || in_array($fileTypes,$fileType))
				{
					$attachName=$fileName . "." . $fileType;			
					if(!file_exists("../data")) mkdir("../data");
					copy($_FILES['file_attach']['tmp_name'],"../data/attach_{$attachName}");
					unlink($_FILES['file_attach']['tmp_name']);
				}
				else
				{
					$oEngine->response('errType');
					exit;					
				}
			}
			$arry=[
			   "fromId"=>$firstFromId,
			   "toId"=>$toId,
			   "subject"=>$subject,
			   "dateUpdate"=>$dateUpdate,
			   "status"=>"",
			   "visible"=>1
			];
			$ticketId=$oTicket->insert($arry);
			$oTicket->messageInsert($ticketId,$messageId,$fromId,$toId,$message,$attachName);
         sleep(1);			
		}
      $oEngine->response("ok");
   }//------------------------------------------------------------------------------------ 	
	else if($requestName=="ticket_messageView")
   {
		$ticketId=$oDb->escape($_REQUEST['ticketId']);
      $items=$oTicket->messageGetAll($ticketId);
      $itemCount=count($items);
      $ticket=$oTicket->get($ticketId);
		$ticketDate=jdate("Y/m/d h:i:s",$ticket->id);
		$ticketDateUpdate=jdate("Y/m/d h:i:s",$ticket->dateUpdate);	
		$ticketSubject=$ticket->subject;		

		$meId=$_SESSION["user_id"];
		if($meId==$ticket->firstFromId)	$userId=$ticket->firstToId; else $userId=$ticket->firstFromId;
		$user=@$oUsers->get($userId);
		if(!$user) $user=@$oAdmin->get2($userId);
		if(!$user) {$oEngine->response("err");exit;}
		if($user->userType == "admin")
			$userTitle="مدیریت";
		else
		{
			$userTitle=$user->userName;
			if($user->fname != "" || $user->lname != "")	$userTitle.= " - " . $user->fname . " " . $user->lname;			
		}

		//change status and visits
		if($ticket->firstToId==$meId)//girande peygham hastam.
		{
		   $oTicket->setStatus($ticketId,1);//khande shode
		   $oTicket->setCheckOut($ticketId,1);//dideh shode 
		   $oTicket->messageSetCheckOutByTicketId($ticketId,1); //dideh shodeh
			$statusT="خوانده ام";
		}
		else
			$statusT=$statusTitle[$ticket->status];
		
		//lock
		if($ticket->lock==1)
			$btnLock="<button class='btn btn-danger' onclick='ticket_lock({$ticket->id}, 0);'><i class='fa fa-undo'></i>&nbsp;باز شود</button>";
		else
			$btnLock="<button class='btn btn-success' onclick='ticket_lock({$ticket->id}, 1);'><i class='fa fa-undo'></i>&nbsp;بسته شود</button>";		
		
		$codeMessage="";
		foreach($items as $item)
      {
			$date=jdate("Y/m/d h:t:i",$item->id);	
			
			//users

			$toUser=@$oUsers->get($item->toId);
			if(!$toUser) $toUser=@$oAdmin->get2($item->toId);
			if(!$toUser) continue;
			if($toUser->userType=="admin") 
			{			
				//admin image 
				if(file_exists($oPath->manageDir("admin_bundle/data/images/user_{$toUser->userId}.jpg")))
					$userToImg=$oPath->manage("admin_bundle/data/images/user_{$toUser->userId}.jpg");
				else
					$userToImg=$oPath->asset("default/images/user_larg.png");	
            $toUserLink=@"<a href='javascript:void(0)' onclick=''>{$toUser->fName} {$toUser->lName}</a>";				
			}
			else
			{
				//user image 
				if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$toUser->userId}.jpg")))
					$userToImg=$oPath->manage("users_bundle/data/images/user_{$toUser->userId}.jpg");
				else
					$userToImg=$oPath->asset("default/images/user_larg.png");	
            $toUserLink=@"<a href='javascript:void(0)' onclick=''>{$toUser->userName}</a>";				
			}

			$fromUser=@$oUsers->get($item->fromId);
			if(!$fromUser) $fromUser=@$oAdmin->get2($item->fromId); 
			if(!$toUser) continue;
			if($fromUser->userType=="admin") 
			{			
				//admin image 
				if(file_exists($oPath->manageDir("admin_bundle/data/images/user_{$fromUser->userId}.jpg")))
					$userFromImg=$oPath->manage("admin_bundle/data/images/user_{$fromUser->userId}.jpg");
				else
					$userFromImg=$oPath->asset("default/images/user_larg.png");	
            $toUserLink="<a href='javascript:void(0)' onclick=''>{$fromUser->fName} {$fromUser->lName}</a>";				
			}
			else
			{
				//user image 
				if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$fromUser->userId}.jpg")))
					$userFromImg=$oPath->manage("users_bundle/data/images/user_{$fromUser->userId}.jpg");
				else
					$userFromImg=$oPath->asset("default/images/user_larg.png");	
            $toUserLink="<a href='javascript:void(0)' onclick=''>{$fromUser->userName}</a>";				
			}

			//user send or receive
			if($item->fromId==$meId)
			{
			   $sendTypeTitle="ارسالی";
            $userImg=$userFromImg;	
				$userTitle="خودم";
				$statusType="success";
			}
			else if($item->toId==$meId)
			{
			   $sendTypeTitle="دریافتی";
				$userImg=$userFromImg;	
				$userTitle=$fromUser->userName;	
				$statusType="info";				
			}

			//attach
			if(file_exists("../data/attach_{$item->attachName}"))
			{
				$attach="<hr><i class='fa fa-paperclip fa-2x'></i>&nbsp;<a href='plugin/ticket/data/attach_{$item->attachName}' target='_blank'>{$item->attachName} - دانلود</a>";	
			}
			else
				$attach="";
         
			$codeMessage.="
				<div class='panel panel-{$statusType} panel-radius'>
					<div class='panel-titleBar'>
					   <h4>{$sendTypeTitle}</h4>
					   <table>
						   <tr>
							   <td class='hide-auto'><img src='{$userImg}' style='width:32px;height:32px;vertical-align: middle;border-radius: 50%;'></td>
								<td>&nbsp;{$userTitle}&nbsp;</span></td>
								<td class='hide-auto'>&nbsp;&nbsp;</td>
								<td>&nbsp;|&nbsp;</td>
								<td class='hide-auto'>&nbsp;&nbsp;</td>
								<td><i class='fa fa-calendar'></i>&nbsp;{$date}</td>
							</tr>
						</table>
					</div>
					<div class='panel-body'>
					   {$item->message} 
                  <br>
                  {$attach}							
					</div>
				</div>
				<br>
			";
      }
      $code= "
      <div class='vSpace-4x'></div>
      <h1>
		   <button class='btn' onclick='ticket_draw(-1);'><i class='fa fa-arrow-right'></i></button>
			&nbsp;|&nbsp;
		   <i class='fa fa-envelope'></i>&nbsp;پیغام {$ticketSubject}
		</h1>
		<div class='vSpace-4x'></div>	
		";	
      
		$code.= "
		<table class='tbl tbl-bordered'  style='direction:rtl'>
			<tr>
				<th class='text-right'><i class='fa fa-lock'></i></th>
				<th class='text-right'>ایجاد شده</th>
				<th class='text-right'>آخرین بروز رسانی</th>
				<th class='text-right'>آخرین وضعیت</th>
			</tr>	
			<tr>
			   <td id='td_lock'>{$btnLock}</td>
				<td>{$ticketDate}</td>
				<td>{$ticketDateUpdate}</td>
				<td class='{$statusColor[$ticket->status]}'>{$statusT}</td>
			</tr>				
		</table>
		<div class='vSpace-4x'></div>
		{$codeMessage}
		";
		
		if($ticket->lock==0)
		$code.=
	   "
		<div class='vSpace-4x'></div>
		
		<div class='panel'>
		   <div class='panel-body'>
				<div class='title-h3'><i class='fa fa-tag'></i>&nbspارسال پاسخ</div>
				<div class='vSpace-2x'></div>		
				<div class='form dir-rtl algn-r'>
					<lable>متن پیغام</lable>
					<textarea id='txt_message'></textarea>
					<label>فایل الصاقی - نوع فایل باید zip یا rar یا bin باشد.</label>
					<input type='file' id='fileAttach'>
					<hr>
					<button class='btn' onclick='ticket_draw(-1);'><i class='fa fa-arrow-right'></i></button>
					<button class='btn btn-success' onclick='ticket_messageSend({$ticket->id},{$userId})'>ارسال</button>
				</div>
			</div>
		</div>
		<div class='vSpace-4x'></div>	
		";
      $countNewMessage=$oTicket->messageGetCount($meId);
		$oEngine->response("ok[|]{$code}[|]{$countNewMessage}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="ticket_messageSend")
   {
		$ticketId=$oDb->escape($_REQUEST["ticketId"]);
      $fromId=$_SESSION["user_id"];
      $toId=$oDb->escape($_REQUEST["toId"]);
		$message=htmlspecialchars($oDb->escape($_REQUEST["message"]));
      $dateUpdate=time();
		$messageId=time();
      $ticketSettings=$oTicket->settingsGet();
		if($ticketSettings->fileTypes=="all") 
		{
			$fileTypes=["all"];
			$fileTypesTitle="محدودیتی وجود ندارد";
		}
		else
		{
		   $fileTypes=explode("|",$ticketSettings->fileTypes);
			$fileTypesTitle=implode(" , ",$fileTypes);
		}
		
		//attach
		$attachName="";
		if(count($_FILES) > 0)
      {
		   //file
			$extension= pathinfo($_FILES['file_attach']['name'], PATHINFO_EXTENSION);
			$fileName=$_FILES['file_attach']['name'];
			$fileType=explode(".",$extension);
			$fileType=$fileType[count($fileType)-1];
			$fileType=strtolower($fileType);

			if(filesize($_FILES['file_attach']['tmp_name']) * 1024 > $ticketSettings->fileSize)
			{
			   $oEngine->response('errSize');
				exit;
			}
			if($fileTypes[0]=="all" || in_array($fileTypes,$fileType))
			{
				$attachName=$fileName . "." . $fileType;			
				if(!file_exists("../data")) mkdir("../data");
				copy($_FILES['file_attach']['tmp_name'],"../data/attach_{$attachName}");
				unlink($_FILES['file_attach']['tmp_name']);
			}
			else
			{
				$oEngine->response('errType');
				exit;					
			}
      }
		$oTicket->messageInsert($ticketId,$messageId,$fromId,$toId,$message,$attachName);
		$oTicket->setStatus($ticketId,0);//khande nashode
		$oTicket->setCheckOut($ticketId,0);//dideh nashode 		
		$oTicket->messagesSetCheckOut($messageId,0);//dideh nashode 		
      $oEngine->response("ok[|]{$ticketId}");
   }//------------------------------------------------------------------------------------ 
   else if($requestName=="ticket_notification")
	{
		if(!isset($_SESSION['adminId'])) {$oEngine->response("err");exit;}
      $a=count($oTicket->getsByCheckout($_SESSION['adminId']));
      if(!isset($_SESSION["ticket_newCount"]))
		{
			$_SESSION["ticket_newCount"]=$a;
		   $isNew="0";	
		}
		else if($_SESSION["ticket_newCount"] < $a)
		{
		   $_SESSION["ticket_newCount"]=$a;
			$isNew="new";
		}
		else
		{
			$_SESSION["ticket_newCount"]=$a;
		   $isNew="0";				
		}
		$oEngine->response("ok[|]{$a}[|]{$isNew}");
	}//------------------------------------------------------------------------------------ 






   else if($requestName=="ticketOperator_draw")
	{
		$page=isset($_REQUEST['page']) ? $oDb->escape($_REQUEST['page']) : 1; 
		
		//auto value
		if($page==-1) $page=$_SESSION["ticket_page"];
		
		//save session
		$_SESSION["ticketOperator_page"]=$page;
		
      $i=0;
      $code="";
      $codeTr="";
      
      //START PAGE ...................................
		$items=$oTicket->operatorGetAll();
		$itemCount=count($items);
      $myPage=$page;
		$pages=$oTools->loopPage($itemCount,10,10,$myPage);
      $btnPageCode="";
      for($i=$pages["btnStart"];$i <= $pages["btnEnd"];$i++)
      {
         if($i==$myPage) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
            $btnPageCode.="<a class='btn btn-fix-36 btn-default'>{$i}</a>";
         else
            $btnPageCode.="<a class='btn btn-fix-36 bg-sky' onclick='ticketOperator_draw({$i});'>{$i}</a>";
      }
      $back=$myPage - 1;
      $next=$myPage + 1;
      if($pages["btnBack"]=="1") $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='ticketOperator_draw({$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($pages["btnNext"]=="1") $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='ticketOperator_draw({$next});'><i class='fa fa-angle-left'></i></a>";
      
		$limitStart=$pages["limitStart"] - 1;
      $limitCount=$pages["limitCount"]; 

		$items=$oTicket->operatorGetAll("{$limitStart},{$limitCount}"); //inbox
		//END PAGE ...................................
		
		foreach($items as $item)
		{
         $i++;
         $code.="
			<tr>
            <td><b>{$item->title}</b></td>
            <td class='algn-r-important'>
					<button class='btn btn-danger' onclick='ticketOperator_del({$item->id});'><i class='fa fa-trash-o'></i></button>
               <button class='btn btn-info' onclick='ticketOperator_edit({$item->id});'><i class='fa fa-pencil'></i></button>
				</td>
         </tr>";		   	
		}
		if(count($items) > 0)
		{
			$code="
			<div class='vSpace-4x'></div>
			<h1>واحد های پاسخگو (operator)</h1>
			<hr>
			<button type='button' class='btn btn-success' onclick='ticketOperator_new();'><i class='fa fa-plus'></i>&nbsp;&nbsp;افزودن</button>
			<div class='vSpace'></div>
			{$btnPageCode}
			<div class='vSpace'></div>
			<table class='tbl tbl-striped tbl-bordered tbl-hover' style='direction:rtl'>
				<tr>
					<th class='text-right'>تصویر</th>
					<th class='text-right'></th>
				</tr>
				{$code}
			</table>
			<div class='vSpace'></div>
			{$btnPageCode}
			<div class='vSpace-4x'></div>";
		}
		else
		{
			$code="
			<div class='vSpace-4x'></div>
			<h1>مجموعه تصاویر</h1>
			<hr>
			<button type='button' class='btn btn-success' onclick='gallery_new();'><i class='fa fa-plus'></i>&nbsp;افزودن</button>
			<div class='vSpace'></div>
			<div class='title-h3 fg-gray'><i class='fa fa-warning'></i>&nbsp;تصویری وجود ندارد</div>
			<div class='vSpace-4x'></div>";			
		}
		$oEngine->response("ok[|]{$code}");
	}//----------------------------------------------------------------------------------
   else if($requestName=="ticketOperator_del")
   {
      $id=$oDbq->escape($_REQUEST["id"]);
		$oTicket->delete($id);
      $oEngine->response("ok");
   }//------------------------------------------------------------------------------------
?>
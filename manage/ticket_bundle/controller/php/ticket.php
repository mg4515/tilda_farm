<?php
   /*header("Content-Type: application/json; charset=UTF-8");*/
   error_reporting(E_ALL);
   session_start();

	//engine.php
	include_once $_SESSION["engineRequire"];
   include_once $oPath->manageDir('jdf.php');
   require_once $oPath->manageDir('ticket_bundle/model/ticket_model.php');
   require_once $oPath->manageDir('ticket_bundle/model/ticketAdminFroms_model.php');
   require_once $oPath->manageDir('admin_bundle/model/admin_model.php');
   require_once $oPath->manageDir('user_bundle/model/user_model.php');

   function ticket_draw($page=1,$drawType="all")
   {
		$oTicket=new cTicket();
		$oTicketAdminFroms=new cTicketAdminFroms();
		$oAdmin=new cAdmin();
		$oUser=new cUser();
		
		$lockTitle=["باز است","بسته است"];
		$lockColor=["fg-success","fg-danger"];
		$lockIcon=["fa fa-unlock","fa fa-lock"];
		$statusTitle=["خوانده نشده","خوانده شده","در حال بررسی","پاسخ داده شده"];
		$statusColor=["fg-danger","fg-warning","fg-info","fg-success"];
	
		$page=$oDb->escape($data["page"]);
		$drawType=isset($data['drawType']) ? $oDb->escape($data['drawType']) : "all"; //in or out or all or user
		$userId=isset($data['userId']) ? $oDb->escape($data['userId']) : ""; //if $drawType='user' then
		
		//auto value
		if($page==-1) $page=$_SESSION["ticket_page"];
		if($drawType==-1) $page=$_SESSION["ticket_drawType"];
		if($userId==-1) $page=$_SESSION["ticket_userId"];
		
		//save session
		$_SESSION["ticket_page"]=$page;
		$_SESSION["ticket_drawType"]=$drawType;
		$_SESSION["ticket_userId"]=$userId;
		
		
		$me=$_SESSION["adminId"];
		$meGeneralManager=$_SESSION['adminGeneralManager'];
		
      $i=0;
      $code="";
      
      //START PAGE ...................................
		if($drawType=="in") $items=$oTicket->getsIn($me);
		else if($drawType=="out") $items=$oTicket->getsOut($me);
		else if($drawType=="all")
      {			
			if(!$meGeneralManager) $items=$oTicket->getAll($me);
			else $items=$oTicket->getAll();
		}
		else if($drawType=="user") $items=$oTicket->gets($me,$userId);

      $itemCount=count($items);
      $selectPage=isset($data["page"]) ? $data["page"] : 1;
      $limitPage=$oTools->misc_getLimitPage($itemCount,30,$selectPage);
      $limitStart=$limitPage["limitStart"];
      $limitEnd=$limitPage["limitEnd"];
      $pageCount=$limitPage["pageCount"];

      $loop=$oTools->misc_getLoopPaging($pageCount,30,$selectPage);
      $start=$loop["start"];
      $end=$loop["end"];
      $btnPageCode="";
      for($i=$start;$i < $end;$i++)
      {
         if($i==$selectPage) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
            $btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
         else
            $btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='ticket_draw(\"{$fromId}\",{$i});' >$i</a>";
      }
      $back=$selectPage - 1;
      $next=$selectPage + 1;
      if($selectPage > 1) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='ticket_draw(\"{$fromId}\",{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($selectPage < $pageCount) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='ticket_draw(\"{$fromId}\",{$next});'><i class='fa fa-angle-left'></i></a>";

		if($drawType=="in") $items=$oTicket->getsIn($me,"{$limitStart},{$limitEnd}");
		else if($drawType=="out") $items=$oTicket->getsOut($me,"{$limitStart},{$limitEnd}");
		else if($drawType=="all")
      {			
			if(!$meGeneralManager) $items=$oTicket->getAll($me,"{$limitStart},{$limitEnd}");
			else $items=$oTicket->getAll("","{$limitStart},{$limitEnd}");
		}
		else if($drawType=="user") $items=$oTicket->gets($me,$userId,"{$limitStart},{$limitEnd}");
		
		//END PAGE ...................................
		
		$i=(($page-1)*3);
      foreach($items as $item)
      {
         $i++;
			$date=jdate("Y/m/d h:i:s",$item->id);
			$dateUpdate=jdate("Y/m/d h:i:s",$item->dateUpdate);

			$toUser=@$oUser->get($item->firstToId);
			if(!$toUser) 
			{
			   $toUser=@$oAdmin->get($item->firstToId,true)["admin"];	
			   $toUser=@"<a href='javascript:void(0)' onclick='admin_profileDraw({$toUser->id});'>{$toUser->userName}</a>";			
			}
			else
			{
				$toUser=@"<a href='javascript:void(0)' onclick='user_profileDraw({$toUser->id});'>{$toUser->userName}</a>";
			}
			$fromUser=@$oUser->get($item->firstFromId);
			if(!$fromUser)
			{
				$fromUser=@$oAdmin->get($item->firstFromId,true)["admin"];
			   $fromUser="<a href='javascript:void(0)' onclick='admin_profileDraw({$fromUser->id});'>{$fromUser->userName}</a>";
         }
         else
         {
            $fromUser="<a href='javascript:void(0)' onclick='user_profileDraw({$fromUser->id});'>{$fromUser->userName}</a>";
			}		
			if($item->checkOut=="0") $style="style='font-weight: bolder;color:#000;font-size:17px;'"; else $style="color:#666";
         $code.="
			<tr>
				<td {$style} class='hide-auto'>{$i}</td>
				<td {$style} class='{$lockColor[$item->lock]}'><i class='{$lockIcon[$item->lock]}'></i></td>
				<td {$style}>{$item->subject}</td>
				<td {$style}>{$fromUser}</td>
				<td {$style}>{$toUser}</td>
				<td {$style} class='hide-auto'>{$date}</td>
				<td {$style}>{$dateUpdate}</td>
				<td {$style} class='{$statusColor[$item->status]}'>{$statusTitle[$item->status]}</td>
				<td {$style}>
				   <button class='btn btn-info' onclick='ticket_messageView({$item->id})'><i class='fa fa-eye'></i></button>
					<button class='btn btn-danger' onclick='ticket_del({$item->id});'><i class='fa fa-trash-o'></i></button>
				</td>
         </tr>";
      }
      $code= "
      <div class='vSpace-4x'></div>
      <div id='layer_scrollIntoView' class='title-h1 dir-rtl algn-c'><i class='fa fa-envelope'></i>&nbsp;پیغام ها</div>
		<div class='vSpace-4x'></div>	
		<button class='btn btn-success' onclick='ticket_new();'><i class='fa fa-plus'></i>&nbsp;جدید</button>
		<hr>
		
		{$btnPageCode}
		<table class='tbl tbl-striped tbl-bordered tbl-hover' style='direction:rtl'>
			<tr>
				<th class='text-right'><i class='fa fa-envelope'></i></th>
				<th class='text-right'><i class='fa fa-lock'></i></th>
				<th class='text-right'>عنوان</th>
				<th class='text-right'>ارسال کننده</th>
				<th class='text-right'>دریافت کننده</th>
				<th class='text-right'>تاریخ</th>
				<th class='text-right'>تاریخ تغییرات</th>
				<th class='text-right'>وضعیت</th>
				<th class='text-right'></th>
			</tr>
			{$code}
		</table>
		{$btnPageCode}
		<br><br>";
		return $code;
   }//------------------------------------------------------------------------------------
?>
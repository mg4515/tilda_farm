<?php
   class cSliders
   {
      private $table="sliders";
      function get($id=0,$checkActive=false)
      {
         global $oDbq;
         if($checkActive==true) $where="active=1 AND "; else $where="";
         $item=$oDbq->table($this->table)->fields("*")->where("{$where}id={$id}")->select()[0];
         return $item;
      }//-----------------------------------------------------------------------
      function gets($count=5,$page=0)
      {
         global $oDbq;
         if($page!=0) $page=$page*$count;
         $items=$oDbq->table($this->table)->fields("*")->where("active=1")->limit("{$page},{$count}")->select();
         return $items;
      }//-----------------------------------------------------------------------
      function getAll($checkActive=false)
      {
         global $oDbq;
         if($checkActive==true) $where="active=1"; else $where="";
         $items=$oDbq->table($this->table)->fields("*")->where("{$where}")->select();
         return $items;
      }//-----------------------------------------------------------------------
      function insert($id,$title,$link)
      {
         global $oDbq;
         $oDbq->table($this->table)->fields("`id`,`title`,`link`")->values("'{$id}','{$title}','{$link}'")->insert();
      }//-----------------------------------------------------------------------
      function update($id,$title,$link)
      {
         global $oDbq;
         $oDbq->table($this->table)->fields("`title`","`link`")->values("'{$title}'","'{$link}'")->where("`id`={$id}")->update();
      }//-----------------------------------------------------------------------
      function delete($id)
      {
         global $oDbq;
         $oDbq->table($this->table)->fields("*")->where("`id`={$id}")->delete();
      }//-----------------------------------------------------------------------    
	}
?>
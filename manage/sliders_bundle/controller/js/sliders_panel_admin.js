var url_sliders=oPath.manage("sliders_bundle/controller/php/sliders_panel_admin.php");
function slider_del(sliderId) {
	script_confirm2('',"حذف شود؟",function()
   {
      try {
         script_loadingShow();
         $.ajax({
            url: url_sliders,
            data: {"requestName":"slider_del", "id":sliderId},
            method: "POST",
            success: function(result)
            {
					result=oEngine.response(result);
               if(result=="ok")
                  sliders_draw();
               else
                  alert("انجام نشد");
               script_loadingHide();
            },
            error: function()
            {
					alert("خطایی در اتصال رخ داده است");
               script_loadingHide();
            }
         });
      }
      catch (e)
      {
			alert("خطای اسکریپت");
         script_loadingHide();
      }
   });
}//---------------------------------------------------------------------------------------
function slider_update(purpose,sliderId) {
   try {
      script_loadingShow();
      var file_data = $('#file').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);
		form_data.append("title",$("#txt_title").val());
		form_data.append("link",$("#txt_link").val());
		form_data.append("purpose",purpose);
		form_data.append("id",sliderId);
		form_data.append("requestName","slider_update");
      $.ajax({
         url: url_sliders,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            if(result=="ok")
            {
               sliders_draw();
            }
				else
				{
				   alert("انجام نشد");	
				}
            script_loadingHide();
         },
         error: function()
         {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   } catch (e) {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//---------------------------------------------------------------------------------------
function slider_edit(sliderId) {
   try {
      script_loadingShow();
      $.ajax({
         url: url_sliders,
         data: {"requestName":"slider_edit", "id":sliderId},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            spl=result.split("[|]");
            if(spl[0]=="ok")
               $("#layer_content").html(spl[1]);
            else
               alert("انجام نشد");
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//---------------------------------------------------------------------------------------
function slider_new() {
   try {
      script_loadingShow();
      $.ajax({
         url: url_sliders,
         data: {"requestName":"slider_new"},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            spl=result.split("[|]");
            if(spl[0]=="ok")
               $("#layer_content").html(spl[1]);
            else
               alert("انجام نشد");
            script_loadingHide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//---------------------------------------------------------------------------------------
function sliders_draw() {
   try {
      script_loadingShow();
      $.ajax({
         url: url_sliders,
         data: {"requestName":"sliders_draw"},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            spl=result.split("[|]");
            if(spl[0]=="ok")
               $("#layer_content").html(spl[1]);
            else
               alert("انجام نشد");
            script_loadingHide();
         },
         error: function() {
            alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
         }
      });
   }
   catch (e)
	{
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//---------------------------------------------------------------------------------------
<?php
   error_reporting(E_ALL);
   session_start();

	//include
	include_once $_SESSION["engineRequire"];//engine.php
	include_once $oPath->manageDir('sliders_bundle/model/sliders_model.php');
   
	//object
	$oSlider=new cSliders();

   //request
	$request=@$_REQUEST["requestName"];
	
   if($request=="sliders_draw")
   {
      $codeTr="";
      $items=$oSlider->getAll();
      $i=0;
      foreach($items as $item)
      {
         $i++;
         if(file_exists($oPath->manageDir("sliders_bundle/data/{$item->id}.jpg")))
            $img= $oPath->manage("sliders_bundle/data/{$item->id}.jpg");
         else
            $img=$oPath->asset("admin/images/noImage.gif");
			$t=time();
         $codeTr.="
			<tr>
            <td style='width:130px;padding:10px;'><img src='{$img}?t={$t}' style='border-radius:8px;width:120px'/><br><b>{$item->title}</b></td>
            <td class='algn-r-important'>
					<button class='btn btn-danger' onclick='slider_del({$item->id});'><i class='fa fa-trash-o'></i></button>
               <button class='btn btn-info' onclick='slider_edit({$item->id});'><i class='fa fa-pencil'></i></button>
				</td>
         </tr>";
      }
		if($codeTr)
		{
			$code="
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;لیست تصاویر</h1>
			<div class='vSpace-4x'></div>
			<button type='button' class='btn btn-success' onclick='slider_new();'><i class='fa fa-plus'></i>&nbsp;افزودن</button>
			<div class='vSpace'></div>
			<table class='tbl tbl-striped tbl-bordered tbl-hover' style='direction:rtl'>
				<tr>
					<th class='text-right'>تصویر</th>
					<th class='text-right'></th>
				</tr>
				{$codeTr}
			</table>
			<div class='vSpace-4x'></div>";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;لیست تصاویر</h1>
			<div class='vSpace-4x'></div>

			<button type='button' class='btn btn-success' onclick='slider_new();'><i class='fa fa-plus'></i>&nbsp;افزودن</button>
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'>";				
		}
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   if($request=="slider_del")
   {
      $sliderId=$oDb->escape($_REQUEST["id"]);
      $oSlider->delete($sliderId);
		@unlink($oPath->manageDir("sliders_bundle/data/{$sliderId}.jpg"));
      $oEngine->response("ok");
   }//------------------------------------------------------------------------------------
   if($request=="slider_edit")
   {
      $sliderId=$oDb->escape($_REQUEST["id"]);
      $item=$oSlider->get($sliderId);

		if(file_exists($oPath->manageDir("sliders_bundle/data/{$sliderId}.jpg")))
			$img= $oPath->manage("sliders_bundle/data/{$sliderId}.jpg");
		else
			$img=$oPath->asset("admin/images/noImage.gif");

      $code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;تصاویر نمایشی - جدید</h1>
		<div class='vSpace-4x'></div>
		
		<div class='form'>
			<label><i class='fa fa-circle'></i>عنوان</label>
			<input type='text' id='txt_title' value='{$item->title}'>
			
			<label><i class='fa fa-circle'></i>لینک</label>
			<input type='text' id='txt_link' value='{$item->link}' class='dir-ltr'>
			
			<label><i class='fa fa-circle'></i>تصوير</label>
			<img src='{$img}' id='img' style='border-radius:8px;width:120px'/><br>
			<label class='lbl-file lbl-file-right'>
				<input type='file' id='file' onchange='oTools.previewImgFile(\"file\",\"img\");' required/>
				<span class='lbl-file-icon'></span>
				<span>انتخاب فایل</span>
			</label>			
		</div>
		<hr>
		<button class='btn btn-default' onclick='sliders_draw();'><i class='fa fa-arrow-right'></i></button>
		<button class='btn btn-success' onclick='slider_update(\"edit\",${sliderId});'>ذخيره</button>
		<div class='vSpace-4x'></div>";
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   if($request=="slider_new")
   {
      $sliderId=time();
      $img=$oPath->asset("admin/images/noImage.gif");

      $code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;تصاویر نمایشی - جدید</h1>
		<div class='vSpace-4x'></div>
		
		<div class='form'>
			<label><i class='fa fa-circle'></i>عنوان</label>
			<input type='text' id='txt_title'>
			
			<label><i class='fa fa-circle'></i>لینک</label>
			<input type='text' id='txt_link' class='dir-ltr'>
			
			<label><i class='fa fa-circle'></i>تصوير</label>
			<img src='{$img}' id='img' style='border-radius:8px;width:120px'/><br>
			<label class='lbl-file lbl-file-right'>
				<input type='file' id='file' onchange='oTools.previewImgFile(\"file\",\"img\");' required/>
				<span class='lbl-file-icon'></span>
				<span>انتخاب فایل</span>
			</label>			
		</div>
		<hr>
		<button class='btn btn-default' onclick='sliders_draw();'><i class='fa fa-arrow-right'></i></button>
		<button class='btn btn-success' onclick='slider_update(\"new\",0);'>ذخيره</button>
		<div class='vSpace-4x'></div>";
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   if($request=="slider_update")
   {
      $sliderId=$_REQUEST["id"] ? $oDb->escape($_REQUEST["id"]) : time();

      if(count($_FILES) > 0)
      {
         if ( 0 < $_FILES['file']['error'])
         {
            if(!file_exists($oPath->manageDir("sliders_bundle/data/{$sliderId}.jpg")))
            {
               echo 'errUpload';
               exit;
            }
         }
         else
         {
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $fileType=explode(".",$extension);
            $fileType=$fileType[count($fileType)-1];
            if($fileType=="jpg" || $fileType=="gif" || $fileType=="png")
            {
					if(!file_exists($oPath->manageDir("sliders_bundle/data"))) mkdir($oPath->manageDir("sliders_bundle/data"));
               copy($_FILES['file']['tmp_name'],$oPath->manageDir("sliders_bundle/data/{$sliderId}.jpg"));
               unlink($_FILES['file']['tmp_name']);
            }
            else
            {
               if(!file_exists($oPath->manageDir("sliders_bundle/data/{$sliderId}.jpg")))
               {
                  echo 'errType';
                  exit;
               }
            }
         }
      }
      $title=htmlspecialchars($oDb->escape($_REQUEST["title"]));
      $link=htmlspecialchars($oDb->escape($_REQUEST["link"],false));

      if($_REQUEST["purpose"]=="edit")
         $oSlider->update($sliderId,$title,$link);
      else if($_REQUEST["purpose"]=="new")
         $oSlider->insert($sliderId,$title,$link);
      $oEngine->response("ok");
   }
?>
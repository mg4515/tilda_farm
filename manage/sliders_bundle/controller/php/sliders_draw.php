<?php
   include_once $oPath->manageDir("sliders_bundle/model/sliders_model.php");
   class cSlidersDraw 
	{
		function drawForHome()
		{
			global $oPath;
			$oSliders=new cSliders();
			$items=$oSliders->getAll();
			$count=count($items);
			$code='';
			for($i=0;$i < $count;$i++)
			{
				$index=$i + 1;
				$item=$items[$i];
				$img=$oPath->manage("sliders_bundle/data/{$item->id}.jpg");

				$code.="
				<div class='item position-relative' style='background-image:url({$img});'>
					<span class='slide-text text-white d-block text-center k-bl position-absolute'>
						{$item->title}
					</span>
				</div>				
				";
			}
			return $code;
		}//------------------------------------------------------------------------------------	
	}
?>
<?php
   class cKifPool
   {
   	private $tbl_kifPool='kifPool';
   	private $tbl_kifPoolTransaction='kifPoolTransaction';
      function kifPool_getAll($active="",$limitStr="")
      {
         global $oDbq;
         $where="";
         if($active!="")
         {
			   if($where=="") $where="`active`='{$active}'"; else $where.=" AND `active`='{$active}'";
         }			
			
			$ret=$oDbq->table($this->tbl_kifPool)->fields("*")->where($where)->orderBy("`id`");
			if($limitStr!="") return $ret->limit($limitStr)->select();
			else return $ret->select();
      }//--------------------------------------------------------------------------
      function kifPool_get($id) //$id=$userId
      {
			global $oDbq;
			$ret=@$oDbq->table($this->tbl_kifPool)->fields("*")->where("`id`='{$id}'")->select()[0];
			return $ret;
      }//--------------------------------------------------------------------------
      function kifPool_isExists($id) //$id=$userId
      {
			global $oDbq;
			$ret=@$oDbq->table($this->tbl_kifPool)->fields("*")->where("`id`='{$id}'")->select()[0];
			if($ret) return true; else return false;
      }//--------------------------------------------------------------------------      
		function kifPool_insert($id) //$id == $userId
      {
			global $oDbq;
			$ret=$oDbq->table($this->tbl_kifPool)->fields("`id`,`amount`,`active`")->values("'{$id}',0,1")->insert();
			return $ret;
      }//--------------------------------------------------------------------------      
      function kifPool_getActive($id)
      {
			global $oDbq;
			$ret=@$oDbq->table($this->tbl_kifPool)->fields("*")->where("`id`='{$id}'")->select()[0]->active;
			if($ret=='0') 
				return false; 
			else if($ret==1)
				return true;
      }//--------------------------------------------------------------------------  		
		function kifPool_getAmount($id) //$id == $userId
      {
			global $oDbq;
			if($id != '')
			{
				if(!$this->kifPool_isExists($id)) $this->kifPool_insert($id);
				$ret=@$oDbq->table($this->tbl_kifPool)->fields("*")->where("`id`='{$id}'")->select()[0]->amount;
				return $ret;
			}
			else
			{
				$items=@$oDbq->table($this->tbl_kifPool)->fields("*")->select();
				$count=count($items);
				$ret=0;
				for($i=0; $i < $count;$i++)
				{
					$ret+=$items->amount;
				}
				return $ret;
			}
      }//--------------------------------------------------------------------------
      function kifPool_setAmount($id,$value,$type="")
      {
			global $oDbq;
			if(!$this->kifPool_isExists($id)) $this->kifPool_insert($id);
			if($type=="") $oDbq->table($this->tbl_kifPool)->set("`amount`={$value}")->where("`id`='{$id}'")->update();
			else
			{
			   $kifpoolAmount=$this->kifPool_getAmount($id);	
				if($type=="in")
				   $kifpoolAmount=$kifpoolAmount + $value;
            else if($type=="out")
               $kifpoolAmount=$kifpoolAmount - $value;
            $oDbq->table($this->tbl_kifPool)->set("`amount`={$kifpoolAmount}")->where("`id`='{$id}'")->update();				
			}
      }//--------------------------------------------------------------------------
      function kifPool_updateAmount($id,$value,$comment="تراکنش سیستمی",$force=false)
      {
			global $oDbq;
			if(!$this->kifPool_isExists($id)) $this->kifPool_insert($id);
			if($value < 0) $value=0;
			
			$amountOld=$this->kifPool_getAmount($id);
			if($amountOld > $value)
			{
				$type="out";
				$amount=$amountOld - $value;
			}
			else if($amountOld < $value)
			{
				$type="in";
				$amount=$value - $amountOld;
			}
			else if($amountOld == $value && $force==false) 
			{
				return true;
			}		
			$ret=$this->transaction_insert([
				'kifPoolId'=>$id,
				'amount'=>$amount,
				'type'=>$type,
				'status'=>'success',
				'purpose'=>'system',
				'comment'=>$comment,
				'insertById'=>@$_SESSION['admin_id']
			]); 		   
			
			if($ret > 0) $this->kifPool_setAmount($id,$value); 	
      }//--------------------------------------------------------------------------			
      function updateKifPool_by_transactionId($transactionId,$payId='') //bar asase khosoosiate yek tarakonesh, mojoody kif pool afzayesh ya kahesh myabad
      {
			global $oDbq;
			$transaction=$this->transaction_get($transactionId);
			if($transaction)
			{
				$id=$transaction->kifPoolId;
				if(!$this->kifPool_isExists($id)) $this->kifPool_insert($id);
				$kifpoolAmount=$this->kifPool_getAmount($id);	
				if($transaction->applied=="0")
				{
					if($transaction->type=="in")
						$kifpoolAmount=$kifpoolAmount + $transaction->amount;
					else if($transaction->type=="out")
						$kifpoolAmount=$kifpoolAmount - $transaction->amount;
					$_SESSION["kifPool_amount"]=$kifpoolAmount;
					$oDbq->table($this->tbl_kifPool)->set("`amount`='{$kifpoolAmount}'")->where("`id`='{$id}'")->update();				
					$this->transaction_setApplied($transactionId,"1",$kifpoolAmount); 
					if($payId)$this->transaction_setPayId($transactionId,$payId); 
				}
				return true;
			}
			else
				return false;
      }//--------------------------------------------------------------------------
		function deposit($id,$amount,$comment,$insertById=0) //amaliate variz va afzayesh mojoody
		{
		   //$id = $kifPoolId = $userId
         $ret=$this->transaction_insert([
				'kifPoolId'=>$id,
				'amount'=>$amount,
				'type'=>'in',
				'status'=>'success',
				'purpose'=>'deposit',
				'comment'=>$comment
			]);
         if($ret['status'] == 1) 
			{
            $this->updateKifPool_by_transactionId($ret['id']);
				if($insertById > 0) $this->transaction_setInsertById($transactionId,$insertById);
			}
         
         return $ret;			
		}//--------------------------------------------------------------------------		
		function payment($id,$amount,$comment,$insertById=0) //amaliate pardakht ba kifpool
		{
		   //$id = $kifPoolId = $userId
         $ret=$this->transaction_insert([
				'kifPoolId'=>$id,
				'amount'=>$amount,
				'type'=>'out',
				'status'=>'success',
				'purpose'=>'buy',
				'comment'=>$comment
			]);
         if($ret['status'] == 1) 
			{
            $this->updateKifPool_by_transactionId($ret['id']);
				if($insertById > 0) $this->transaction_setInsertById($transactionId,$insertById);
			}
         
         return $ret;			
		}//--------------------------------------------------------------------------
		function withdrawal($id,$amount,$bankShaba,$bankCardNumber,$comment='') //amaliate darkhast bardashtt
		{
		   //$id = $kifPoolId = $userId
			if(!$comment) $comment='درخواست برداشت';
         $ret=$this->transaction_insert([
				'kifPoolId'=>$id,
				'amount'=>$amount,
				'type'=>'out',
				'status'=>'suspended',
				'purpose'=>'withdrawal',
				'comment'=>$comment,
				'otherData1'=>$bankShaba,
				'otherData2'=>$bankCardNumber
			]);
         if($ret['status'] == 1) 
			{
            $this->updateKifPool_by_transactionId($ret['id']);
			}
         
         return $ret;		
		}//--------------------------------------------------------------------------
		function withdrawalSetSuccess($transactionId, $comment='') //taeed darkhast bardasht
		{
		   $this->transaction_setStatus($transactionId, 'success');
			if($comment) $this->transaction_setComment($transactionId, $comment);
			if(@$_SESSION['admin_id']) $this->transaction_setUpdateById($transactionId, $_SESSION['admin_id']);
			$this->transaction_setUpdateDate($transactionId);
		}//--------------------------------------------------------------------------
		function withdrawalSetFailed($transactionId, $comment='') //adame taeed darkhast bardasht
		{
		   $this->transaction_setStatus($transactionId, 'failed');
			if($comment) $this->transaction_setComment($transactionId, $comment);
			if(@$_SESSION['admin_id']) $this->transaction_setUpdateById($transactionId, $_SESSION['admin_id']);
			$this->transaction_setUpdateDate($transactionId);
		}//--------------------------------------------------------------------------			
		function kifPool_delete($id) //delete KifPool or KifPools
		{
			global $oDbq;

			$where="";
			$id=explode(",",rtrim($id,","));
			for($i=0;$i < count($id);$i++)
			{
				$selectedId=$id[$i];
				if($selectedId !="" )
				{
					$where.="(`id`='{$selectedId}')";
					if($i < count($id) - 1) $where.=" OR ";
				}
			}

			$oDbq->table($this->tbl_kifPool)->fields("*")->where("({$where})")->delete();
		}//------------------------------------------------------------------------------------
		function kifPool_trash($id) //trash factor or factors
		{
			global $oDbq;

			$where="";
			$id=explode(",",rtrim($id,","));
			for($i=0;$i < count($id);$i++)
			{
				$selectedId=$id[$i];
				if($selectedId !="" )
				{
					$where.="(`id`='{$selectedId}')";
					if($i < count($id) - 1) $where.=" OR ";
				}
			}
			
			$adminId=$_SESSION['adminId'];
			$oDbq->table($this->tbl_kifPool)->set("`trash`='1',trashById='{$adminId}'")->where("({$where})")->update();
		}//------------------------------------------------------------------------------------
		function kifPool_unTrash($id) //unTrash KifPool or KifPools
		{
			global $oDbq;

			$where="";
			$id=explode(",",rtrim($id,","));
			for($i=0;$i < count($id);$i++)
			{
				$selectedId=$id[$i];
				if($selectedId !="" )
				{
					$where.="(`id`='{$selectedId}')";
					if($i < count($id) - 1) $where.=" OR ";
				}
			}

			$oDbq->table($this->tbl_kifPool)->set("`trash`=0")->where("({$where})")->update();
		}//------------------------------------------------------------------------------------
	
      function transaction_getAll($argArray)
		{
         global $oDbq;
			
		   $kifPoolId=@$argArray['kifPoolId'];
		   $type=@$argArray['type']; //out or in
		   $status=@$argArray['status']; //success or failed or suspended
		   $purpose=@$argArray['purpose']; //buy or transfer or withdrawal
		   $searchWord=@$argArray['searchWord']; 
		   $trash=@$argArray['trash']; 
		   $limitStr=@$argArray['limitStr']; 
		
         $where="";
         if($kifPoolId!="")
         {
			   if($where=="") $where="`kifPoolId`='{$kifPoolId}'"; else $where.=" AND `kifPoolId`='{$kifPoolId}'";
         }				
         if($type!="")
         {
			   if($where=="") $where="`type`='{$type}'"; else $where.=" AND `type`='{$type}'";
         }
         if($status!="")
         {
			   if($where=="") $where="`status`='{$status}'"; else $where.=" AND `status`='{$status}'";
         }
         if($purpose!="")
         {
			   if($where=="") $where="`purpose`='{$purpose}'"; else $where.=" AND `purpose`='{$purpose}'";
         }				
         if($trash!="")
         {
			   if($where=="") $where="`trash`='{$trash}'"; else $where.=" AND `trash`='{$trash}'";
         }	
         if($searchWord!="")
         {
			   if($where=="") $where="(`id` LIKE '%{$searchWord}%' OR `kifPoolId` LIKE '%{$searchWord}%' OR `payId` LIKE '%{$searchWord}%' OR `comment` LIKE '%{$searchWord}%')"; else $where.=" AND (`id` LIKE '%{$searchWord}%' OR `kifPoolId` LIKE '%{$searchWord}%' OR `payId` LIKE '%{$searchWord}%' OR `comment` LIKE '%{$searchWord}%')";
         }			
			$ret=$oDbq->table($this->tbl_kifPoolTransaction)->fields("*")->where($where)->orderBy("`id` DESC");
			if($limitStr!="") return $ret->limit($limitStr)->select();
			else return $ret->select();
		}//--------------------------------------------------------------------------	
      function transaction_get($id)
      {
			global $oDbq;
			$ret=$oDbq->table($this->tbl_kifPoolTransaction)->fields("*")->where("`id`='{$id}'")->select()[0];
			return $ret;
      }//--------------------------------------------------------------------------
      function transaction_getByToken($token)
      {
			global $oDbq;
			$ret=$oDbq->table($this->tbl_kifPoolTransaction)->fields("*")->where("`token`='{$token}'")->select()[0];
			return $ret;
      }//--------------------------------------------------------------------------			
      function transaction_isExists($id,$payId,$type) 
      {
			global $oDbq;
			$ret=$oDbq->table($this->tbl_kifPoolTransaction)->fields("*")->where("`id`='{$id}' AND `payId`='{$payId}' AND `type`='{$type}'")->select();
			return count($ret);
      }//--------------------------------------------------------------------------		
      function transaction_isExists_($id,$type,$return=false)
      {
			global $oDbq;
			$ret=$oDbq->table($this->tbl_kifPoolTransaction)->fields("*")->where("`id`='{$id}' AND `type`='{$type}'")->select();
			if($return==true && count($ret) > 0) return $ret[0]; else return count($ret);
      }//--------------------------------------------------------------------------			
		function transaction_delete($id) //delete Transaction or Transactions
		{
			global $oDbq;

			$where="";
			$id=explode(",",rtrim($id,","));
			for($i=0;$i < count($id);$i++)
			{
				$selectedId=$id[$i];
				if($selectedId !="" )
				{
					$where.="(`id`='{$selectedId}')";
					if($i < count($id) - 1) $where.=" OR ";
				}
			}

			$oDbq->table($this->tbl_kifPoolTransaction)->fields("*")->where("({$where})")->delete();
		}//------------------------------------------------------------------------------------
		function transaction_trash($id) //trash Transaction or Transactions
		{
			global $oDbq;

			$where="";
			$id=explode(",",rtrim($id,","));
			for($i=0;$i < count($id);$i++)
			{
				$selectedId=$id[$i];
				if($selectedId !="" )
				{
					$where.="(`id`='{$selectedId}')";
					if($i < count($id) - 1) $where.=" OR ";
				}
			}
			
			$adminId=$_SESSION['admin_id'];
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`trash`=1,trashById={$adminId}")->where("({$where})")->update();
		}//------------------------------------------------------------------------------------
		function transaction_unTrash($id) //unTrash Transaction or Transactions
		{
			global $oDbq;

			$where="";
			$id=explode(",",rtrim($id,","));
			for($i=0;$i < count($id);$i++)
			{
				$selectedId=$id[$i];
				if($selectedId !="" )
				{
					$where.="(`id`='{$selectedId}')";
					if($i < count($id) - 1) $where.=" OR ";
				}
			}

			$oDbq->table($this->tbl_kifPoolTransaction)->set("`trash`=0")->where("({$where})")->update();
		}//------------------------------------------------------------------------------------		
      function transaction_insert($argArray) //ijade yek tarakonesh
      {
			global $oDbq;
		
		   $id=@$argArray['id'];
		   $kifPoolId=@$argArray['kifPoolId'];
		   $amount=@$argArray['amount'];
		   $type=@$argArray['type']; //out or in
		   $status=@$argArray['status']; //success or failed or suspended
		   $purpose=@$argArray['purpose']; //buy or transfer or deposit or withdrawal
		   $comment=@$argArray['comment'];
		   $payId=@$argArray['payId'] ? $argArray['payId'] : '0';
		   $balanceUpdate=@$argArray['balanceUpdate'] ? $argArray['balanceUpdate'] : false;
		   $otherData1=@$argArray['otherData1'];
		   $otherData2=@$argArray['otherData2'];
			$insertDate=time();
		   
			//mojody kif pool
			$kifPoolAmount=$this->kifPool_getAmount($kifPoolId);
			
			//faal boodan ya nabodan kif pool. 0,1
			$active=$this->kifPool_getActive($kifPoolId);
			
			if($active==1)
			{
				if(!$id) $id=time(); //orderId
				$token=cTools::misc_createId('full',true);
				if($type=="out") //out:kharid, transfer:bardasht ya enteghal
				{
					$newAmount=$kifPoolAmount - $amount;
					if($newAmount < 0) return ['status'=>-1, 'id'=>0, 'token'=>'']; //mojoody kafy namybashad
				}				
				else if($type=="in")
					$newAmount=$kifPoolAmount + $amount; 
				else 
					return ['status'=>0, 'id'=>0, 'token'=>'']; //amaliat bi hadaf
            
				//agar balanceUpdate==true bashad, mojoody jadide kifPool bar asase in tarakonesh, sabt myshavad
				if($balanceUpdate==true) $kifPoolAmount=$newAmount;

				//insert
				$oDbq->table($this->tbl_kifPoolTransaction)->set("`id`='{$id}',
																				  `payId`='{$payId}',
																				  `kifPoolId`='{$kifPoolId}',
																				  `kifPoolAmount`={$kifPoolAmount},
																				  `type`='{$type}',
																				  `status`='{$status}',
																				  `purpose`='{$purpose}',
																				  `amount`='{$amount}',
																				  `comment`='{$comment}',
																				  `otherData1`='{$otherData1}',
																				  `otherData2`='{$otherData2}',
																				  `insertDate`='{$insertDate}',
																				  `token`='{$token}'")->insert();				
			
				return ['status'=>1, 'id'=>$id, 'token'=>$token]; //amaliate movafagh
         }
			else 
				return ['status'=>-2, 'id'=>0, 'token'=>'']; //hesab masdood mybashad
      }//--------------------------------------------------------------------------			
      function transaction_setCheckout($id,$value)
      {
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`checkout`='{$value}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------      
		function transaction_setPayId($id,$payId,$payCardNumber="") //tarakonesh pardakt bank
      {
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`payId`='{$payId}',`payCardNumber`='{$payCardNumber}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------	
      function transaction_setApplied($id,$value="1",$kifpoolAmount="")// bar rooye mojoody kifpool emal shode ast ya kheir
      {
			global $oDbq;
			if($kifpoolAmount=="")
            $oDbq->table($this->tbl_kifPoolTransaction)->set("`applied`='{$value}'")->values("'{$value}'")->where("`id`='{$id}'")->update();
         else
			   $oDbq->table($this->tbl_kifPoolTransaction)->set("`applied`='{$value}',`kifpoolAmount`='{$kifpoolAmount}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------	
      function transaction_setInsertById($id,$insertById) //modire ijad konandeh in tarakonesh
      {
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`insertById`='{$insertById}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------	
      function transaction_setUpdateById($id,$updateById) //shenase modire virayesh konandeh in tarakonesh
      {
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`updateById`='{$updateById}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------	
      function transaction_setUpdateDate($id) //tarikh virayesh in tarakonesh
      {
			global $oDbq;
			$date=time();
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`updateDate`='{$date}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------		
      function transaction_setRelatedId($id,$relatedId) //shenase : mahsool ya etebar ya khadamat ya ...
      {
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`relatedId`='{$relatedId}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------
      function transaction_setStatus($id,$value) //value : failed, success, suspended
      {
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`status`='{$value}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------
      function transaction_getStatus($id)
      {
			global $oDbq;
			return @$oDbq->table($this->tbl_kifPoolTransaction)->fields("`status`")->where("`id`='{$id}'")->select()[0]->status;
      }//--------------------------------------------------------------------------
      function transaction_setOtherData1($id,$value)
      {
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`otherData1`='{$value}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------
      function transaction_getOtherData1($id)
      {
			global $oDbq;
			return @$oDbq->table($this->tbl_kifPoolTransaction)->fields("`otherData1`")->where("`id`='{$id}'")->select()[0]->otherData1;
      }//--------------------------------------------------------------------------
      function transaction_setOtherData2($id,$value)
      {
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`otherData2`='{$value}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------
      function transaction_getOtherData2($id)
      {
			global $oDbq;
			return @$oDbq->table($this->tbl_kifPoolTransaction)->fields("`otherData2`")->where("`id`='{$id}'")->select()[0]->otherData2;
      }//--------------------------------------------------------------------------	
      function transaction_setComment($id,$value)
      {
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolTransaction)->set("`comment`='{$value}'")->where("`id`='{$id}'")->update();
      }//--------------------------------------------------------------------------
      function transaction_getComment($id)
      {
			global $oDbq;
			return @$oDbq->table($this->tbl_kifPoolTransaction)->fields("`comment`")->where("`id`='{$id}'")->select()[0]->otherData2;
      }//--------------------------------------------------------------------------			
      function withdrawal_getAll($argArray,$kifPoolId='',$confirm='',$trash='',$searchWord='',$limitStr="")
		{
         global $oDbq;
			
			$kifPoolId=@$argArray["kifPoolId"] ? $argArray["kifPoolId"] : "";
			$transactionId=@$argArray["transactionId"] ? $argArray["transactionId"] : "";
			$confirm=@$argArray["confirm"] ? $argArray["confirm"] : "";
			$sortByRow=@$argArray["sortByRow"] ? $argArray["sortByRow"] : true;
			$searchWord=@$argArray["searchWord"] ? $argArray["searchWord"] : "";
			$trash=@$argArray["trash"] ? $argArray["trash"] : "";
			$limitStr=@$argArray["limitStr"] ? $argArray["limitStr"] : "";	
		
         $where="";
         if($kifPoolId!=="")
         {
			   if($where=="") $where="`kifPoolId`='{$kifPoolId}'"; else $where.=" AND `kifPoolId`='{$kifPoolId}'";
         }
         if($transactionId!=="")
         {
			   if($where=="") $where="`transactionId`='{$transactionId}'"; else $where.=" AND `transactionId`='{$transactionId}'";
         }	
			if($confirm!=="")
			{
				if($where=="") $where="`confirm`='{$confirm}'"; else $where.=" AND `confirm`='{$confirm}'";
			}			
         if($trash!=="")
         {
			   if($where=="") $where="`trash`='{$trash}'"; else $where.=" AND `trash`='{$trash}'";
         }	
         if($searchWord!=="")
         {
			   if($where=="") $where="(`id` LIKE '%{$searchWord}%' OR `kifPoolId` LIKE '%{$searchWord}%' OR `transactionId` LIKE '%{$searchWord}%') OR `	dataForSearch1` LIKE '%{$searchWord}%') OR `	dataForSearch2` LIKE '%{$searchWord}%')"; else $where.=" AND (`id` LIKE '%{$searchWord}%' OR `kifPoolId` LIKE '%{$searchWord}%' OR `payId` LIKE '%{$searchWord}%')";
         }	
			if($sortByRow==true)$ret=$oDbq->table($this->tbl_kifPoolWithdrawal)->fields("*")->where($where)->orderBy("`id` DESC");			
			else $ret=$oDbq->table($this->tbl_kifPoolWithdrawal)->fields("*")->where($where);
			if($limitStr!="") return $ret->limit($limitStr)->select();
			else return $ret->select();
		}//--------------------------------------------------------------------------	
      function withdrawal_get($id)
      {
			global $oDbq;
			$ret=@$oDbq->table($this->tbl_kifPoolWithdrawal)->fields("*")->where("`id`='{$id}'")->select()[0];
			return $ret;
      }//--------------------------------------------------------------------------
		function withdrawal_trash($id) //trash withdrawal or withdrawals
		{
			global $oDbq;

			$where="";
			$id=explode(",",rtrim($id,","));
			for($i=0;$i < count($id);$i++)
			{
				$selectedId=$id[$i];
				if($selectedId !="" )
				{
					$where.="(`id`='{$selectedId}')";
					if($i < count($id) - 1) $where.=" OR ";
				}
			}
			
			$adminId=$_SESSION['admin_id'];
			$oDbq->table($this->tbl_kifPoolWithdrawal)->set("`trash`=1,trashById={$adminId}")->where("({$where})")->update();
		}//------------------------------------------------------------------------------------
		function withdrawal_unTrash($id) //unTrash withdrawal or withdrawals
		{
			global $oDbq;

			$where="";
			$id=explode(",",rtrim($id,","));
			for($i=0;$i < count($id);$i++)
			{
				$selectedId=$id[$i];
				if($selectedId !="" )
				{
					$where.="(`id`='{$selectedId}')";
					if($i < count($id) - 1) $where.=" OR ";
				}
			}

			$oDbq->table($this->tbl_kifPoolWithdrawal)->set("`trash`=0")->where("({$where})")->update();
		}//------------------------------------------------------------------------------------
		function withdrawal_delete($id) //delete withdrawal or withdrawals
		{
			global $oDbq;

			$where="";
			$id=explode(",",rtrim($id,","));
			for($i=0;$i < count($id);$i++)
			{
				$selectedId=$id[$i];
				if($selectedId !="" )
				{
					$where.="(`id`='{$selectedId}')";
					if($i < count($id) - 1) $where.=" OR ";
				}
			}

			$oDbq->table($this->tbl_kifPoolWithdrawal)->fields("*")->where("({$where})")->delete();
		}//------------------------------------------------------------------------------------
		public function withdrawal_getConfirm($id)
		{
			global $oDbq;
			$ret=@$oDbq->table($this->tbl_kifPoolWithdrawal)->fields("`confirm`")->where("`id`={$id}")->select()[0]->confirm;
			if($ret=="1")
				return true;
			else if($ret=="2")
				return false;
		}//---------------------------------------------------------------------------------		
		public function withdrawal_setConfirm($id,$value)
		{
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolWithdrawal)->set("`confirm`={$value}")->where("`id`={$id}")->update();
		}//---------------------------------------------------------------------------------
		public function withdrawal_getConfirmComment($id)
		{
			global $oDbq;
			$ret=@$oDbq->table($this->tbl_kifPoolWithdrawal)->fields("`confirmComment`")->where("`id`={$id}")->select()[0]->confirmComment;
			return $ret;
		}//---------------------------------------------------------------------------------
		public function withdrawal_setConfirmComment($id,$value)
		{
			global $oDbq;
			$oDbq->table($this->tbl_kifPoolWithdrawal)->set("`confirmComment`='{$value}'")->where("`id`={$id}")->update();
		}//---------------------------------------------------------------------------------
		function withdrawal_insert($array)
		{
			global $oDbq;
			$id=@$array['id'] ? $array['id'] : time();
			$confirm=@$array['confirm'] ? $array['confirm'] : '0';		
			$insertById=@$array['insertById'] ? $array['insertById'] : 0;
			$insertDate=time();
			$oDbq->table($this->tbl_kifPoolWithdrawal)->set("`id`={$id},
																`kifPoolId`={$array['kifPoolId']},
																`transactionId`={$array['transactionId']},
																`amount`={$array['amount']},
																`confirm`={$confirm},
																`otherData1`='{$otherData1}',
																`otherData2`='{$otherData2}',
																`insertById`={$insertById},
																`insertDate`={$insertDate}
															  ")->insert();
			return $id;												  
			
		}//--------------------------------------------------------------------------		
   }
?>
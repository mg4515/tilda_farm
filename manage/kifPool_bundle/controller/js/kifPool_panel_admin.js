var url_kifPool=oPath.manage("kifPool_bundle/controller/php/kifPool_panel_admin.php");

function kifPoolTransaction_draw(obj,searchWord_,page_)
{
	if(obj=="auto")
	{
		obj= {
			'type':-1,
			'status':-1,
			'purpose':-1,
			'kifPoolId':-1,
			'searchWord':-1,
			'trash':-1,
			'page':-1,
		}
	}
	if(searchWord_ != undefined) obj.searchWord=searchWord_;
	if(page_ != undefined) obj.page=page_;
   try {
      script_loadingShow();
      $.ajax({
         url: url_kifPool,
         data: {"requestName":"kifPoolTransaction_draw", "type":obj.type, "status":obj.status, "purpose":obj.purpose, "kifPoolId":obj.kifPoolId, "searchWord":obj.searchWord, 'trash':obj.trash, 'page':obj.page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPoolTransaction_del(transactionId) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			if(transactionId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=transactionId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_kifPool,
					data: {"requestName": "kifPoolTransaction_del","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						var spl=result.split("[|]");
						if(result.response=="ok")
						{
							kifPoolTransaction_draw("auto");
						}
						else
						{
							alert("انجام نشد");
							alert(result);
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
   }
}//---------------------------------------------------------------------------------------
function kifPoolTransaction_trash(transactionId) {
   script_confirm2('',"موقتا حذف شود؟",function()
   {
      try {
			if(transactionId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=transactionId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_kifPool,
					data: {"requestName": "kifPoolTransaction_trash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						var spl=result.split("[|]");
						if(spl[0]=="ok")
						{
							kifPoolTransaction_draw("auto");
						}
						else
						{
							alert("انجام نشد");
							alert(result);
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
   });
}//---------------------------------------------------------------------------------------
function kifPoolTransaction_unTrash(transactionId) {
   script_confirm2('',"بازیابی شود؟",function()
   {
      try {
			if(transactionId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=transactionId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_kifPool,
					data: {"requestName": "kifPoolTransaction_unTrash","id":id},
					method: "POST",
					success: function(result)
					{
						result=JSON.parse(oEngine.response(result));
						var spl=result.split("[|]");
						if(result.response=="ok")
						{
							kifPoolTransaction_draw(spl[1],spl[2],spl[3],spl[4],spl[5],spl[6]);
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
   });
}//---------------------------------------------------------------------------------------






function kifPoolWithdrawal_draw(obj,searchWord_,page_)
{
	if(obj=="auto")
	{
		obj= {
			'status':-1,
			'kifPoolId':-1,
			'searchWord':-1,
			'trash':-1,
			'page':-1,
		}
	}
	if(searchWord_ != undefined) obj.searchWord=searchWord_;
	if(page_ != undefined) obj.page=page_;
   try {
      script_loadingShow();
      $.ajax({
         url: url_kifPool,
         data: {"requestName":"kifPoolWithdrawal_draw", "status":obj.status, "kifPoolId":obj.kifPoolId, "searchWord":obj.searchWord, 'trash':obj.trash, 'page':obj.page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPoolWithdrawal_edit(id) //namayesh form virayesh
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_kifPool,
         data: {"requestName":"kifPoolWithdrawal_edit", "id":id},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPoolWithdrawal_update(id) //zakhire virayeshat dar edit
{
   try {
		err=false;
		if($("#txt_otherData1").val()=="" && $("#txt_otherData2").val()=="")
		{
			script_alert2('',"لطفا یکی از موارد اطلاعات بانکی را وارد تنایید","danger");
			script_focus("txt_otherData1");
			err=true;			
		}			
		//---
		if(err==false)
		{
			script_loadingShow();
			$.ajax({
				url: url_kifPool,
				data: {"requestName":"kifPoolWithdrawal_update", 'id':id, "otherData1":$("#txt_otherData1").val(), "otherData2":$("#txt_otherData2").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						kifPoolWithdrawal_draw("auto");
						script_alert2('','با موفقیت انجام شد',"success");
					}
					else
						alert("انجام نشد!.");
					script_loadingHide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
		}
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPoolWithdrawal_view(id) //namayesh moshakhasat va dokme haye taeed kardan va taeed nakardan
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_kifPool,
         data: {"requestName":"kifPoolWithdrawal_view", "id":id},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
               $("#layer_content").html(spl[1]);
            else
               alert("انجام نشد!.");
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPoolWithdrawal_setConfirm(id) //taeed kardan va ya taeed nakardan darkhast bardasht
{
   try {
		script_loadingShow();
		$.ajax({
			url: url_kifPool,
			data: {"requestName":"kifPoolWithdrawal_setConfirm", "comment":$("#txt_comment").val()},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					kifPoolWithdrawal_draw("auto");
					script_alert2('','با موفقیت انجام شد',"success");
				}
				else
					alert("انجام نشد!.");
				script_loadingHide();
			},
			error: function() {
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
		});
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPoolWithdrawal_del(transactionId) 
{
   script_confirm2("حذف شود ؟",'',function()
   {
      try {
			if(transactionId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=transactionId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_kifPool,
					data: {"requestName": "kifPoolWithdrawal_del","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						var spl=result.split("[|]");
						if(result.response=="ok")
						{
							kifPoolWithdrawal_draw("auto");
							script_alert2('','با موفقیت حذف شد',"success");
						}
						else
							alert("انجام نشد");
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
   });
}//---------------------------------------------------------------------------------------
function kifPoolWithdrawal_trash(transactionId) {
   script_confirm2('',"موقتا حذف شود؟",function()
   {
      try {
			if(transactionId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=transactionId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_kifPool,
					data: {"requestName": "kifPoolWithdrawal_trash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						var spl=result.split("[|]");
						if(spl[0]=="ok")
						{
							kifPoolWithdrawal_draw("auto");
							script_alert2('','با موفقیت حذف شد',"success");							
						}
						else
							alert("انجام نشد");
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
   });
}//---------------------------------------------------------------------------------------
function kifPoolWithdrawal_unTrash(transactionId) {
   script_confirm2('',"بازیابی شود؟",function()
   {
      try {
			if(transactionId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=transactionId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_kifPool,
					data: {"requestName": "kifPoolWithdrawal_unTrash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							kifPoolWithdrawal_draw("auto");
							script_alert2('','با موفقیت بازیابی شد',"success");							
						}
						else
							alert("انجام نشد");
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
   });
}//---------------------------------------------------------------------------------------

function kifPool_draw(kifPoolId)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_kifPool,
         data: {"requestName": "kifPool_draw","id":kifPoolId},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPool_amountUpdate(kifPoolId) 
{
   script_confirm2('',"تغییرات اعمال شود؟",function()
   {
      try {
			script_loadingShow();
			value=$("#txt_amount").val();
			comment=$("#txt_comment").val();
			$.ajax({
				url: url_kifPool,
				data: {"requestName": "kifPool_amountUpdate", "id":kifPoolId, "value":value, 'comment':comment},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						script_alert2('','تراکنش انجام شد','success');
						kifPool_draw(spl[1]);
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
   });
}//---------------------------------------------------------------------------------------

var url_kifPool=oPath.manage("kifPool_bundle/controller/php/kifPool_panel_client.php");
function kifPoolTransaction_draw(obj,searchWord_,page_)
{
	if(obj=="auto")
	{
		obj= {
			'type':-1,
			'status':-1,
			'purpose':-1,
			'kifPoolId':-1,
			'searchWord':-1,
			'trash':-1,
			'page':-1,
		}
	}
	if(searchWord_ != undefined) obj.searchWord=searchWord_;
	if(page_ != undefined) obj.page=page_;
   try {
      script_loadingShow();
      $.ajax({
         url: url_kifPool,
         data: {"requestName":"kifPoolTransaction_draw", "type":obj.type, "status":obj.status, "purpose":obj.purpose, "kifPoolId":obj.kifPoolId, "searchWord":obj.searchWord, 'trash':obj.trash, 'page':obj.page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPool_depositDraw()
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_kifPool,
         data: {"requestName": "kifPool_depositDraw"},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPool_withdrawalDraw()
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_kifPool,
         data: {"requestName": "kifPool_withdrawalDraw"},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_content").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPool_deposit() //afzayesh mojiidy
{
	try {
		err=false;
		if($("#txt_amount").val() < 100)
		{
			script_alert2("","حداقل مبلغ افزایش موجودی، 1،000 تومان می باشد",'danger')
         err=true;		
		}
      else if(err==false)
		{
			$('#btn_pay').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
				url: url_kifPool,
				data: {"requestName": "kifPool_deposit", "amount": $("#txt_amount").val(), "payTypeId":$("#slct_payType").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
               if(spl[0]=="ok")
               {
						script_alert2("","در حال انتقال به درگاه پرداخت","success");
						setTimeout(function(){
							oTools.link(spl[1]);
						}, 3000);
               }
               else if (spl[0]=="err" || spl[0]=="errPayType")
					{
						script_alert2("","اتصال به درگاه پرداخت، امکان پذیر نیست. لطفا دوباره امتحان کنید","danger");					
						$('#btn_pay').html('واریز');
					}
					else
               {
                  alert("انجام نشد");
               }
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					$('#btn_pay').html('واریز');
				}

			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
		$('#btn_pay').html('واریز');
	}
}//---------------------------------------------------------------------------------------
function kifPool_withdrawal() //darkhast bardasht
{
	try {
		err=false;
		if($("#txt_amount").val() < 10000)
		{
			script_alert2("","حداقل مبلغ برداشت از حساب، 10،000 تومان می باشد",'danger')
         err=true;		
		}
      else if(err==false)
		{
			script_loadingShow();
			$.ajax({
				url: url_kifPool,
				data: {"requestName": "kifPool_withdrawal", "amount": $("#txt_amount").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
               if(spl[0]=="ok")
               {
						script_alert2("","درخواست شما با موفقیت ثبت گردید","success");
						setTimeout(function(){
							oTools.link("user?page=kifPoolTransactionSuspended");
						}, 3000);
               }
               else if (spl[0]=="errInventory")
					{
						script_alert2("","موجودی حساب، کافی نمی باشد","danger");					
					}
               else if (spl[0]=="errAccount")
					{
						script_alert2("","حساب، مسدود می باشد","danger");					
					}
               else if (spl[0]=="errBank")
					{
						script_alert2("","مشخصات حساب بانکی، نا مشخص است","danger");					
					}						
					else
               {
                  script_alert2("","انجام نشد","danger");
               }
					script_loadingHide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					script_loadingHide();
				}

			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function kifPoolAmount_sharj(payId,amountSharj)
{
   try {
		err=false;
      status("layer_msg");
		if(amountSharj==undefined) amountSharj=$("#txt_sharjValue").val();
		if(amountSharj < 100)
		{
	      status("layer_msg","danger","مبلغ شارژ نباید از 100 تومان کمتر باشد");
			document.querySelector("#layer_msg").scrollIntoView({ behavior: 'smooth' });
         err=true;		   	
		}
		if(err==false)
		{
			script_loadingShow();
			$.ajax({
				url: url_kifPool,
				data: { "data": oEngine.request({"requestName":"kifPoolAmount_sharj","amountSharj":amountSharj,"payId":payId}) },
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					//alert(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						post_get(spl[1], {"RefId": spl[2]});
					}
					else
					{
						status("layer_msg","danger","اتصال به درگاه بانک، امکانپذیر نمی باشد. لطفا دوباره سعی نمایید.");
			         document.querySelector("#layer_msg").scrollIntoView({ behavior: 'smooth' });
					}
					script_loadingHide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
		}
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPoolAmount_sharjTest()
{
   try {
		err=false;
      status("layer_msg");
		if($("#txt_sharjValue").val() < 1000)
		{
	      status("layer_msg","danger","مبلغ شارژ نباید از 1000 تومان کمتر باشد");
			document.querySelector("#layer_msg").scrollIntoView({ behavior: 'smooth' });
         err=true;		   	
		}
		if(err==false)
		{
			script_loadingShow();
			$.ajax({
				url: url_kifPool,
				data: { "data": oEngine.request({"requestName":"kifPoolAmount_sharj","amountSharj":$("#txt_sharjValue").val()}) },
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					alert(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						post_get(spl[1], {"RefId": spl[2],"ResCode":spl[3],"SaleOrderId":spl[4],"SaleReferenceId":spl[5]});
					}
					else
					{
						status("layer_msg","danger","اتصال به درگاه بانک، امکانپذیر نمی باشد. لطفا دوباره سعی نمایید.");
			         document.querySelector("#layer_msg").scrollIntoView({ behavior: 'smooth' });
					}
					script_loadingHide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
		}
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function kifPoolAmount_notification() {
	try {
		//script_loadingShow();
		$.ajax({
			url: url_kifPool,
			data: {"requestName": "kifPoolAmount_notification"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
				   $("#spn_mnukifPool").html(spl[1] + " تومان");
				   $("#spn_mnukifPool").addClass("mtree-counter-bg");
				}
			},
			error: function() {
				//script_loadingHide();
				//alert("خطایی در اتصال رخ داده است");
			}
		});
	} catch (e) {
		//alert("خطای اسکریپت");
		//script_loadingHide();
	}
}//---------------------------------------------------------------------------------------


//services
services_add("kifPoolAmount_notification",function(){kifPoolAmount_notification();});
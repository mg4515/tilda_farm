<?php
	@session_start();

   //include
	include_once $_SESSION["engineRequire"];//engine.php
	require_once $oPath->manageDir("jdf.php");
	require_once $oPath->manageDir("users_bundle/model/users_model.php");
	require_once $oPath->manageDir("pay_bundle/model/pay_model.php");
	require_once $oPath->manageDir("pay_bundle/controller/php/pay_draw.php");
	require_once $oPath->manageDir("kifPool_bundle/model/kifPool_model.php");
	
	//object
   $oUsers=new cUsers();
   $oKifPool=new cKifPool();
   $oPay=new cPay();
   $oPayDraw=new cPayDraw();
	
	//requestName
	$requestName=@$_REQUEST["requestName"];
	
	//var
	$arryStatusTitle=array(''=>'ناموفق',"failed"=>"ناموفق","success"=>"موفق","suspended"=>"در حال بررسی");
	$arryStatusColor=array(''=>'danger','failed'=>'danger','success'=>'success','suspended'=>'warning');
	$arryStatusTitle2=array(''=>'تایید نشده',"failed"=>"تایید نشده","success"=>"تایید شده","suspended"=>"در حال بررسی");
	
   //actions by requestName	
   if($requestName=="kifPoolTransaction_draw")
   {
      $type=@cDataBase::escape($_REQUEST["type"]); //out or in
      $status=@cDataBase::escape($_REQUEST["status"]); //success, failed, suspended
      $kifPoolId=cDataBase::escape($_SESSION["user_id"]);
      $searchWord=@cDataBase::escape($_REQUEST["searchWord"]); //userId , payId
      $page=@$_REQUEST["page"] ? cDataBase::escape($_REQUEST["page"]) : 1;
  
      //init auto
		if($type==-1) $type=$_SESSION["kifPoolTransaction_type"];
		if($status==-1) $status=$_SESSION["kifPoolTransaction_status"];
		if($searchWord==-1) $searchWord=$_SESSION["kifPoolTransaction_searchWord"];
		if($page==-1) $page=$_SESSION["kifPoolTransaction_page"];
  
      //init session
      $_SESSION["kifPoolTransaction_type"]=$type;
      $_SESSION["kifPoolTransaction_status"]=$status;
      $_SESSION["kifPoolTransaction_searchWord"]=$searchWord;
      $_SESSION["kifPoolTransaction_page"]=$page;
		
		
		if($status=="success")
		   $transactionTitle="تراکنش های موفق";	
		else if($status=="failed")
		   $transactionTitle="تراکنش های ناموفق";
		else if($status=="suspended")
		   $transactionTitle="تراکنش های معلق";		
		else
			$transactionTitle="تراکنش ها";

		if($searchWord) $transactionTitle.=" - جستجو ({$searchWord})";

		//START PAGE ...................................	
      $items=$oKifPool->transaction_getAll([
		   'type'=>$type,
		   'status'=>$status,
			'kifPoolId'=>$kifPoolId,
			'trash'=>'0',
			'searchWord'=>$searchWord,
			'limitStr'=>''
		]);
      $count=count($items);
		$pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode=""; 
		if($pages['pagesEnd'] > 1)
		{
			for($i=$pages['pagesStart'];$i <= $pages['pagesEnd'];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='kifPoolTransaction_draw(\"auto\",undefined,{$i});'>{$i}</a>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='kifPoolTransaction_draw(\"auto\",undefined,{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='kifPoolTransaction_draw(\"auto\",undefined,{$next});'><i class='fa fa-angle-left'></i></a>";
		}
		$items=$oKifPool->transaction_getAll([
		   'type'=>$type,
		   'status'=>$status,
			'kifPoolId'=>$kifPoolId,
			'trash'=>'0',
			'searchWord'=>$searchWord,
		   'limitStr'=>$pages['sqlLimit']
		]);		
		//END PAGE ...................................

      $i=0;
      $code="";
      $trCode="";
				
      foreach($items as $item)
      {
         $i++;
         $id=$item->id;
         $date=jdate("Y/m/d h:i:s",$item->id);

	      //payStatus
			$statusTitle=$arryStatusTitle[$item->status];
			$statusColor=$arryStatusColor[$item->status];
			$span_status="<span class='lbl lbl-{$statusColor}'>{$statusTitle}</span>";
			
			//type
			if($item->type=="in") 
			   $i_type="<i class='fa fa-arrow-up' style='color:green'></i>&nbsp;";	
			else if($item->type=="out")
			   $i_type="<i class='fa fa-arrow-down' style='color:red'></i>&nbsp;";			
			
			//amount
			$amountTransaction=number_format($item->amount,0,".",",");
			$kifpoolAmount=number_format($item->kifPoolAmount,0,".",",");
			
			//checkout
         if($item->checkout=="0") {$b="<b>";$b_="</b>";} else {$b="";$b_="";}
			$oKifPool->transaction_setCheckout($item->id,'1');
			
         $trCode.="
         <tr>
				<td>{$b}{$item->id}{$b_}</td>
				<td>{$b}{$item->comment}{$b_}</td>
				<td>{$b}{$date}{$b_}</td>
				<td>{$b}{$span_status}{$b_}</td>
				<td>{$b}{$i_type}{$amountTransaction} تومان{$b_}</td>
				<td>{$b}{$kifpoolAmount} تومان{$b_}</td>
         </tr>";
      }
		$chkCount=$i;
		if($trCode)
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$transactionTitle}</h1>
			<div class='vSpace-4x'></div>
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='kifPoolTransaction_draw(\"auto\",$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
	
			<div class='vSpace'></div>
				
			<div class='hScrollBar-auto'>
				<table class='tbl tbl-center tbl-bordered tbl-hover'>
					<tr>
						<th>تراکنش</th>
						<th>توضیحات</th>
						<th>تاریخ</th>
						<th>وضعیت تراکنش</th>
						<th>مبلغ تراکنش</th>
						<th>مانده</th>
					</tr>
					{$trCode}
				</table>
			</div>
			<br>{$btnPageCode}<br>
			<br><br>
			";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$transactionTitle}</h1>
			<div class='vSpace-4x'></div>
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='kifPoolTransaction_draw(\"auto\",$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>			
				
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'>";				
		}
		$oEngine->response("ok[|]" . $code);
		exit;		
   }//------------------------------------------------------------------------------------
   else if($requestName=="kifPool_depositDraw") //namayesh form afzayesh mojoody
   { 
      $kifPoolId=$_SESSION["user_id"];
		$amount=$oKifPool->kifPool_getAmount($kifPoolId); 
		$amountLbl=number_format($amount,0,".",",") . " تومان"; 
		
		$user=$oUsers->get($kifPoolId);
		$kifPoolUser="-{$user->userName}";
      
		$htmlOption=$oPayDraw->htmlOption();
		
		if($amount==0)
		{
		   $status="
			<div class='lbl lbl-right lbl-danger'>
			   موجودی : {$amountLbl}  
			</div>			
			";	
		}
		else
      {
		   $status="
			<div class='lbl lbl-right lbl-success'>
			   موجودی : {$amountLbl}  
			</div>			
			";			
		}

      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-plus'></i>&nbsp;افزایش موجودی کیف پول</h1>
		<div class='vSpace-4x'></div>

      <div class='panel'>
		   <div class='panel-body form'>
				{$status}
				
				<div class='vSpace'></div>
				
				<label>
				   <b>توجه</b>
					حداقل مبلغ افزایش موجودی، 1،000 تومان می باشد
				</label>
				
				<div class='vSpace'></div>
				
				<label>مبلغ واریزی</label>
				<input type='text' id='txt_amount' class='dir-ltr' value='1000' >&nbsp;تومان		   
				
				<label>درگاه پرداخت<span></span></label>
				<select id='slct_payType'>{$htmlOption}</select>				
				
				<hr>
				
				<button id='btn_pay' class='btn btn-warning' onclick='kifPool_deposit();'>واریز</button>			
			</div>
		</div>
		<div class='vSpace-4x'></div>
		";
      $oEngine->response("ok[|]" . $code);
		exit;		
   }//------------------------------------------------------------------------------------
   else if($requestName=="kifPool_withdrawalDraw") //namayesh form bardasht mojoody
   { 
      $kifPoolId=$_SESSION["user_id"];
		$amount=$oKifPool->kifPool_getAmount($kifPoolId); 
		$amountLbl=number_format($amount,0,".",",") . " تومان"; 
		
		$user=$oUsers->get($kifPoolId);
      $bankShaba=@$user->bankShaba ? $user->bankShaba : 0;
		
		if($amount==0)
		{
		   $status="
			<div class='lbl lbl-right lbl-danger'>
			   موجودی : {$amountLbl}  
			</div>			
			";	
		}
		else
      {
		   $status="
			<div class='lbl lbl-right lbl-success'>
			   موجودی : {$amountLbl}  
			</div>			
			";			
		}

      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-plus'></i>&nbsp;درخواست برداشت از کیف پول</h1>
		<div class='vSpace-4x'></div>

      <div class='panel'>
		   <div class='panel-body form'>
				{$status}
				
				<div class='vSpace'></div>
				
				<label>
				   <b>توجه</b>
					حداکثر 48 ساعت پس از ثبت و تایید درخواست، عملیات بانکی انجام خواهد شد
				</label>
				
				<div class='vSpace'></div>
				
				<label>
				   شماره کارت بانکی 
					<a href='user?page=userBankEdit' class='lbl'><i class='fa fa-pencil'></i>&nbsp;ویرایش</a>
				</label>
				<input type='text' id='txt_bankCardNumber' class='dir-ltr' value='{$user->bankCardNumber}' disabled >
				
				<div class='vSpace'></div>
				
				<label>
				   شماره شبا بانکی
					<a href='user?page=userBankEdit' class='lbl'><i class='fa fa-pencil'></i>&nbsp;ویرایش</a>
				</label>
				<div class='input-control'>
					<span class='input-control-icon'>IR</span>
					<input type='text' id='txt_bankShaba' value='{$bankShaba}' disabled >
				</div>				
				
				<div class='vSpace'></div>
				
				<label>مبلغ. حداقل 10،000 تومان</label>
				<input type='number' id='txt_amount' class='dir-ltr' value='' >&nbsp;تومان		   
				
				<hr>
				<button class='btn btn-warning' onclick='kifPool_withdrawal();'>ثبت درخواست</button>			
			</div>
		</div>
		<div class='vSpace-4x'></div>
		";
      $oEngine->response("ok[|]" . $code);
		exit;		
   }//------------------------------------------------------------------------------------
   else if($requestName=="kifPool_deposit")
   {
      $kifPoolId=$_SESSION['user_id'];
		$amount=isset($_REQUEST['amount']) ? cDataBase::escape($_REQUEST['amount']) : 0;
		$payTypeId=cDataBase::escape(@$_REQUEST['payTypeId']); //shenase dargah
		$transactionId=time();
		
		//check property
		if(!$payTypeId){cEngine::response('errPayType');exit;}
		if($amount > 0) $amountRial= $amount . "0"; else $amount=1000;
		
		//start pay
      $otherData=$kifPoolId;
      $callBackUrl=$oPath->root("cb2");
      $description='افزایش موجودی';		
      $orderId=$transactionId;
		$result=$oPay->start($payTypeId,$orderId,$amount,$description,$callBackUrl,$otherData);					
		if($result['status']=='failed')
		{
			cEngine::response('err');
			exit;
		}
		else if($result['status']=='success')
		{
			$oKifPool->transaction_insert([
			   'id'=>$transactionId,
				'kifPoolId'=>$kifPoolId,
				'amount'=>$amount,
				'type'=>'in',
				'purpose'=>'increase',
				'comment'=>$description,
				'payId'=>'0',
				'balanceUpdate'=>false
			]);
			cEngine::response("ok[|]{$result['url']}");
			exit;
		}
   }//------------------------------------------------------------------------------
   else if($requestName=="kifPool_withdrawal")
   {
      $kifPoolId=$_SESSION['user_id'];
		$amount=isset($_REQUEST['amount']) ? cDataBase::escape($_REQUEST['amount']) : 0;
		
		//user
		$user=$oUsers->get($kifPoolId);
		if(!$user->bankShaba && !$user->bankCardNumber)
		{
			cEngine::response('errBank');
			exit;
		}
		
		//sabte tarakonesh darkhast 
		$result=$oKifPool->withdrawal($kifPoolId,$amount,$user->bankShaba,$user->bankCardNumber,'درخواست برداشت از حساب');
		
		//amaliat bi hadaf
		if($result['status']==0)
		{
			cEngine::response('err');
			exit;
		}		
		
		//mojoody kafy namybashad
		if($result['status']==-1)
		{
			cEngine::response('errInventory');
			exit;
		}

		//hesab masdood mybashad
		if($result['status']==-2)
		{
			cEngine::response('errAccount');
			exit;
		}	

		//hesab masdood mybashad
		if($result['status']==1)
		{
			cEngine::response('ok');
			exit;
		}		
   }//------------------------------------------------------------------------------	
	else if($requestName=="kifPool_notification")
   {
		if(isset($_SESSION["kifPool_amount"])) 
			$amount=$_SESSION["kifPool_amount"];
		else
		{
         $kifPoolId=$_SESSION["user_id"];
         $amount=$oKifPool->kifPool_getAmount($kifPoolId);
		   $_SESSION["kifPool_amount"]=$amount;
		}
		$amount=number_format($amount,0,".",",");
      $oEngine->response("ok[|]{$amount}");
   }//------------------------------------------------------------------------------	
   else if($requestName=="kifPool_sharj")
   { 
      $kifPoolId=$_SESSION["user_id"];
		$amountSharj=@$oDb->escape($data["amountSharj"]);
		$transactionId=time();
		
		if($amountSharj > 0) $amountSharjRial= $amountSharj . "0"; else $amountSharjRial=1000;
		//-------------------------
      $orderId=$transactionId;

      $additionalData=$kifPoolId;
      $callBackUrl=$oPath->root("client/cbu_kifPoolSharj.php");
      $callBackUrl=$oPath->root("user/cbu_pkpSharj");

		//start pay
		$payId=@$oDb->escape($data["payId"]);
		$_SESSION['pay_payId']=$payId;
      $result=$oPay->start($payId,$orderId,$amountSharjRial,$callBackUrl);			
		if($result["status"]=="err")
			$oEngine->response("err");
		else if($result["status"]=="ok")
		{
			$oKifPool->transaction_insert($transactionId,$kifPoolId,$amountSharj,"in","شارژ کیف پول"); //not Applied
         $oEngine->response("ok[|]{$result['startUrl']}");
		}
		
		/*
   	$oPayParsian=new cPayParsian();
      $result=$oPayParsian->start($orderId,$amountSharjRial,$callBackUrl);
		if($result["status"]=="err")
			$oEngine->response("err");
		else if($result["status"]=="ok")
		{
			$_SESSION["pay_currentRefId"]=$result["refId"];
			$oKifPool->transaction_insert($transactionId,$kifPoolId,$amountSharj,"in","شارژ کیف پول");
         $oEngine->response("ok[|]{$result['startUrl']}");
		}	
		*/
   }//------------------------------------------------------------------------------------	
?>
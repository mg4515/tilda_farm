<?php
	include_once $oPath->manageDir("pay_bundle/model/pay_model.php");
	include_once $oPath->manageDir("kifPool_bundle/model/kifPool_model.php");
   
   function kifPool_increase($argArray) //sharj kif pool
   {
      $kifPoolId=$_SESSION['user_id'];
		$transactionId=time();
		$amount=isset($argArray['amount']) ? $argArray['amount'] : 0;
		if($amount > 0) $amountRial= $amount . "0"; else $amount=1000;
      $additionalData=$kifPoolId;
      $callBackUrl=$oPath->root("client/cbu_kifPoolIncreaseSabad.php");
      $callBackUrl=$oPath->root("user/cbu_pkpIncSabad");
      $callBackUrl=$argArray["callBackUrl"];
      $otherData1=isset($argArray['otherData1']) ? $argArray['otherData1'] : '';
      $otherData2=isset($argArray['otherData2']) ? $argArray['otherData2'] : '';
      $comment=isset($argArray['comment']) ? $argArray['comment'] : 'شارژ کیف پول';
		
		//start pay
		$payTypeId=isset($argArray['payTypeId']) ? $argArray['payTypeId'] : "";
		if($payTypeId==='') return -1;
		$_SESSION['pay_typeId']=$payTypeId;
      $orderId=$transactionId;
		$result=$oPay->start($payTypeId,$orderId,$amountRial,$callBackUrl);			
		if($result['status']=='failed')
			return -2;
		else if($result['status']=='success')
		{
			$oKifPool->transaction_insert([
			   'id'=>$transactionId,
				'kifPoolId'=>$kifPoolId,
				'amount'=>$amount,
				'type'=>'in',
				'purpose'=>'increase',
				'comment'=>$comment,
				'payId'=>'0', 'balanceUpdate'=>false,
				'otherData1'=>$otherData1,
				'otherData1'=>$otherData2
			]);
         return $result['url'];
		}		
   }	
?>
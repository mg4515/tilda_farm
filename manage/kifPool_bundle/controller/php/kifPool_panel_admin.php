<?php
   @session_start();
   
	//include
	include_once $_SESSION["engineRequire"];//engine.php
	require_once $oPath->manageDir("jdf.php");
	require_once $oPath->manageDir("admin_bundle/model/admin_model.php");
	require_once $oPath->manageDir("users_bundle/model/users_model.php");
	require_once $oPath->manageDir("users_bundle/model/usersInform_model.php");
	require_once $oPath->manageDir("kifPool_bundle/model/kifPool_model.php");
	
	//object
   $oKifPool=new cKifPool();	
   $oUsers=new cUsers();
   $oUsersInform=new cUsersInform();
	
	//requestName
	$requestName=@$_REQUEST["requestName"];
	
	//var
	$arryStatusTitle=array(''=>'ناموفق',"failed"=>"ناموفق","success"=>"موفق","suspended"=>"در حال بررسی");
	$arryStatusColor=array(''=>'danger','failed'=>'danger','success'=>'success','suspended'=>'warning');
	$arryStatusTitle2=array(''=>'تایید نشده',"failed"=>"تایید نشده","success"=>"تایید شده","suspended"=>"در حال بررسی");

   //actions by requestName	
   if($requestName=="kifPoolTransaction_draw")
   {
      $type=@cDataBase::escape($_REQUEST["type"]); //out or in
      $status=@cDataBase::escape($_REQUEST["status"]); //success, failed, suspended
      $purpose=@cDataBase::escape($_REQUEST["purpose"]); //buy, sale, transfer, withdrawal, increase, system
      $kifPoolId=@cDataBase::escape($_REQUEST["kifPoolId"]);
      $searchWord=@cDataBase::escape($_REQUEST["searchWord"]); //userId , payId
      $trash=@$_REQUEST["trash"] ? cDataBase::escape($_REQUEST["trash"]) : '0' ; //userId , payId
      $page=@$_REQUEST["page"] ? cDataBase::escape($_REQUEST["page"]) : 1;
  
      //init auto
		if($type==-1) $type=$_SESSION["kifPoolTransaction_type"];
		if($status==-1) $status=$_SESSION["kifPoolTransaction_status"];
		if($purpose==-1) $purpose=$_SESSION["kifPoolTransaction_purpose"];
		if($kifPoolId==-1) $kifPoolId=$_SESSION["kifPoolTransaction_kifPoolId"];
		if($searchWord==-1) $searchWord=$_SESSION["kifPoolTransaction_searchWord"];
		if($trash==-1) $trash=$_SESSION["kifPoolTransaction_trash"];
		if($page==-1) $page=$_SESSION["kifPoolTransaction_page"];
  
      //init session
      $_SESSION["kifPoolTransaction_type"]=$type;
      $_SESSION["kifPoolTransaction_status"]=$status;
      $_SESSION["kifPoolTransaction_purpose"]=$purpose;
      $_SESSION["kifPoolTransaction_kifPoolId"]=$kifPoolId;
      $_SESSION["kifPoolTransaction_searchWord"]=$searchWord;
      $_SESSION["kifPoolTransaction_trash"]=$trash;
      $_SESSION["kifPoolTransaction_page"]=$page;
		
		
		if($status=="success")
		   $transactionTitle="تراکنش های موفق";	
		else if($status=="failed")
		   $transactionTitle="تراکنش های ناموفق";
		else if($status=="suspended")
		   $transactionTitle="تراکنش های معلق";		
		else
			$transactionTitle="تراکنش ها";
	   if($trash=="1")
			$transactionTitle.=" حذف شده";

		if($searchWord) $transactionTitle.=" - جستجو ({$searchWord})";
		
		if($type=="in") $typeOptn="<option value=''>نوع تراکنش</option><option value='in' selected='selected'>ورودی</option><option value='out'>خروجی</option>";
		else if($type=="out") $typeOptn="<option value=''>نوع تراکنش</option><option value='in'>ورودی</option><option value='out' selected='selected'>خروجی</option>";
		else $typeOptn="<option value='' selected='selected'>نوع تراکنش</option><option value='in'>ورودی</option><option value='out'>خروجی</option>";

		if($kifPoolId != "")
		{
			$user=@$oUsers->get($kifPoolId);
			$kifPoolUser="-{$user->userName}";
			$classBtnUserDialog="btn btn-success btn-image btn-image-right";
		}
		else
		{
			$kifPoolUser="";
			$classBtnUserDialog="btn btn-image btn-image-right";
		}

		//START PAGE ...................................	
      $items=$oKifPool->transaction_getAll([
		   'type'=>$type,
		   'status'=>$status,
		   'purpose'=>$purpose,
			'kifPoolId'=>$kifPoolId,
			'trash'=>$trash,
			'searchWord'=>$searchWord,
			'limitStr'=>''
		]);
      $count=count($items);
		$pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode=""; 
		if($pages['pagesEnd'] > 1)
		{
			for($i=$pages['pagesStart'];$i <= $pages['pagesEnd'];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='kifPoolTransaction_draw(\"auto\",undefined,{$i});'>{$i}</a>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='kifPoolTransaction_draw(\"auto\",undefined,{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='kifPoolTransaction_draw(\"auto\",undefined,{$next});'><i class='fa fa-angle-left'></i></a>";
		}
		$items=$oKifPool->transaction_getAll([
		   'type'=>$type,
		   'status'=>$status,
		   'purpose'=>$purpose,
			'kifPoolId'=>$kifPoolId,
			'trash'=>$trash,
			'searchWord'=>$searchWord,
		   'limitStr'=>$pages['sqlLimit']
		]);		
		//END PAGE ...................................

      $i=0;
      $code="";
      $trCode="";
				
      foreach($items as $item)
      {
         $i++;
         $id=$item->id;
         $date=jdate("Y/m/d h:i:s",$item->id);

	      //payStatus
			$statusTitle=$arryStatusTitle[$item->status];
			$statusColor=$arryStatusColor[$item->status];
			$span_status="<span class='msg msg-{$statusColor}'>{$statusTitle}</span>";
			
			//type
			if($item->type=="in") 
			   $i_type="<i class='fa fa-arrow-up' style='color:green'></i>&nbsp;";	
			else if($item->type=="out")
			   $i_type="<i class='fa fa-arrow-down' style='color:red'></i>&nbsp;";			
			
			//amount
			$amountTransaction=number_format($item->amount,0,".",",");
			$kifpoolAmount=number_format($item->kifPoolAmount,0,".",",");
			
			//checkout
         if($item->checkout=="0") {$b="<b>";$b_="</b>";} else {$b="";$b_="";}
			$oKifPool->transaction_setCheckout($item->id,'1');
			
			//user
			$user=@$oUsers->get($item->kifPoolId);
			if($user)
			   $html_user=@"<a href='javascript:void(0)' onclick='user_profileDraw({$user->userId});'><u>{$user->userName}</u></a>";
			else
				$html_user='<i class="fa fa-info-circle fa-2x fg-grayLight"></i>';
			
			//trash by admin
         if($item->trash == 1)
         {
            $trashById=$item->trashById;
            if($trashById)
            {
               $admin=new cAdmin();
               $trashByAdmin=$admin->get($trashById,true)["profile"];
               $html_trashByAdmin=$trashByAdmin->userName . '<br>' . $trashByAdmin->fName . ' ' . $trashByAdmin->lName;
            }
				if(!$html_trashByAdmin) $html_trashByAdmin='<i class="fa fa-info-circle fa-2x fg-grayLight"></i>'; 
         }
			
         $trCode.="
         <tr>
            <td><input type='checkbox' id='chk_{$i}' value='{$item->id}' onclick='checkedOne();' />&nbsp;{$b}{$b_}</td>
				<td>{$b}{$item->id}{$b_}</td>
				<td>{$b}{$item->comment}{$b_}</td>";
				if($item->trash == 1) $trCode.="<td>{$b}{$html_trashByAdmin}{$b_}</td>";
				$trCode.="
				<td>{$b}{$html_user}{$b_}</td>
				<td>{$b}{$date}{$b_}</td>
				<td>{$b}{$span_status}{$b_}</td>
				<td>{$b}{$i_type}{$amountTransaction} تومان{$b_}</td>
				<td>{$b}{$kifpoolAmount} تومان{$b_}</td>
            ";
				$trCode.="";
					if($item->trash == '1') $trCode.="
					<td>
					<button class='btn btn-success' onclick='kifPoolTransaction_unTrash({$item->id});'><i class='fa fa-undo'></i></button>
					<button class='btn btn-danger' onclick='kifPoolTransaction_del({$item->id});'><i class='fa fa-close'></i></button>
					</td>
					";
					else 
					$trCode.="<td><button class='btn btn-danger' onclick='kifPoolTransaction_trash({$item->id});'><i class='fa fa-trash-o'></i></button></td>";
           
					$trCode.="
         </tr>";
      }
		$chkCount=$i;
		if($trCode)
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$transactionTitle}{$kifPoolUser}</h1>
			<div class='vSpace-4x'></div>";
			
			if($trash == '1') 
				$code.="<button id='btn_trash' class='btn btn-danger' onclick='kifPoolTransaction_del(\"selected\");'><i class='fa fa-remove'></i></button>";
			else
				$code.="<button id='btn_trash' class='btn btn-danger' onclick='kifPoolTransaction_trash(\"selected\");' disabled><i class='fa fa-trash-o'></i></button>";
			$code.="
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='kifPoolTransaction_draw(\"auto\",$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
	
			<div class='hide'>
				<hr>
				<i class='fa fa-filter'></i>&nbsp;
				<select id='slct_type' onchange='kifPoolTransaction_draw(-1,this.value,-1,-1,-1,-1);'>{$typeOptn}</select>				
			</div>
			
			<div class='vSpace'></div>
				
			<div class='hScrollBar-auto'>
				<table class='tbl tbl-center tbl-bordered tbl-hover'>
					<tr>
						<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>
						<th>تراکنش</th>
						<th>توضیحات</th>";
						if($trash == '1') $code.="<th>حذف کننده</th>";
						$code.="
						<th>کاربر</th>
						<th>تاریخ</th>
						<th>وضعیت تراکنش</th>
						<th>مبلغ تراکنش</th>
						<th>مانده</th>
						<th></th>
					</tr>
					{$trCode}
				</table>
			</div>
			<br>{$btnPageCode}<br>
			<br><br>
			";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$transactionTitle}{$kifPoolUser}</h1>
			<div class='vSpace-4x'></div>
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='kifPoolTransaction_draw(\"auto\",$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
			
			<div class='hide'>
				<hr>
				<i class='fa fa-filter'></i>&nbsp;
				<select id='slct_type' onchange='kifPoolTransaction_draw(-1,this.value,-1,-1,-1,-1);'>{$typeOptn}</select>				
			</div>				
				
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'>";				
		}
		$oEngine->response("ok[|]" . $code);
		exit;		
   }//------------------------------------------------------------------------------------
   else if($requestName=="kifPoolTransaction_del")
   {
      $transactionId=cDataBase::escape($_REQUEST["id"],false);
      $oKifPool->transaction_delete($transactionId); 		
		//get session for back
		$kifPoolId=$_SESSION["kifPoolTransaction_kifPoolId"];
		$selectPage=$_SESSION["kifPoolTransaction_page"];
		
		$oEngine->response("ok");
   }//------------------------------------------------------------------------------------
   else if($requestName=="kifPoolTransaction_trash")
   {
      $transactionId=cDataBase::escape($_REQUEST["id"],false);		
      $oKifPool->transaction_trash($transactionId);     
		//get session for back
		$kifPoolId=$_SESSION["kifPoolTransaction_kifPoolId"];
		$selectPage=$_SESSION["kifPoolTransaction_page"];
		
		$oEngine->response("ok[|]{$kifPoolId}[|]{$selectPage}");
   }//------------------------------------------------------------------------------------	
   else if($requestName=="kifPoolTransaction_unTrash")
   {
      $transactionId=cDataBase::escape($_REQUEST["id"],false);		
      $oKifPool->transaction_unTrash($transactionId);     
		//get session for back
		$kifPoolId=$_SESSION["kifPoolTransaction_kifPoolId"];
		$selectPage=$_SESSION["kifPoolTransaction_page"];
		
		$oEngine->response("ok[|]{$kifPoolId}[|]{$selectPage}");
   }//------------------------------------------------------------------------------------	
   else if($requestName=="kifPool_draw")
   { 
      $kifPoolId=cDataBase::escape($_REQUEST["id"]); //userId
		$amount=$oKifPool->kifPool_getAmount($kifPoolId); 
		$amountLbl=number_format($amount,0,".",",") . " تومان"; 
		
		$user=$oUsers->get($kifPoolId);
		$kifPoolUser="-{$user->userName}";
      
		if($amount==0)
		{
		   $status="
			<div class='lbl lbl-right lbl-danger'>
			   موجودی : {$amountLbl}  
			</div>			
			";	
		}
		else
      {
		   $status="
			<div class='lbl lbl-right lbl-success'>
			   موجودی : {$amountLbl}  
			</div>			
			";			
		}

      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;کیف پول - {$kifPoolUser}</h1>
		<div class='vSpace-4x'></div>

      <div class='panel'>
		   <div class='panel-body form'>
				{$status}
				<div class='vSpace'></div>
				
				<label><b>توجه</b></label>
				<label>میزان مبلغ تراکنش، به صورت خودکار مشخص می شود.</label>
				<label>به عنوان مثال، اگر موجودی کیف پول 150 است و مقدار موجودی را به 100 تغییر دهید، تراکنش از نوع <b>برداشت</b> و با مقدار <b>50</b> ایجاد می شود</label>
				
				<div class='vSpace'></div>
				
				<label>موجودی کیف پول</label>
				<input type='text' id='txt_amount' value='{$amount}' class='dir-ltr'>&nbsp;تومان			   
				
				<div class='vSpace'></div>
				
				<label>توضیحات تراکنش</label>
				<textarea id='txt_comment'></textarea>
				
				<hr>
				<button class='btn' onclick='users_draw(\"\",\"\",-1);'><i class='fa fa-arrow-right'></i></button>
				<button class='btn btn-success' onclick='kifPool_amountUpdate({$kifPoolId});'>ذخیره</button>			
			</div>
		</div>
		<div class='vSpace-4x'></div>
		";
      $oEngine->response("ok[|]" . $code);		
   }//------------------------------------------------------------------------------------	
   else if($requestName=="kifPool_amountUpdate")
   {
      $kifPoolId=cDataBase::escape($_REQUEST["id"]); //userId
      $value=(double) cDataBase::escape($_REQUEST["value"]); //new amount
      $comment=cDataBase::escape(@$_REQUEST["comment"]); //transactionComment
		$oKifPool->kifPool_updateAmount($kifPoolId, $value, $comment);
		cEngine::response("ok[|]{$kifPoolId}");	
   }//------------------------------------------------------------------------------------
   
   if($requestName=="kifPoolWithdrawal_draw")
   {
      $status=@cDataBase::escape($_REQUEST["status"]); //success, failed, suspended
      $kifPoolId=@cDataBase::escape($_REQUEST["kifPoolId"]);
      $searchWord=@cDataBase::escape($_REQUEST["searchWord"]); //userId , payId
      $trash=@$_REQUEST["trash"] ? cDataBase::escape($_REQUEST["trash"]) : '0' ; //userId , payId
      $page=@$_REQUEST["page"] ? cDataBase::escape($_REQUEST["page"]) : 1;
  
      //init auto
		if($status==-1) $status=$_SESSION["kifPoolWithdrawal_status"];
		if($kifPoolId==-1) $kifPoolId=$_SESSION["kifPoolWithdrawal_kifPoolId"];
		if($searchWord==-1) $searchWord=$_SESSION["kifPoolWithdrawal_searchWord"];
		if($trash==-1) $trash=$_SESSION["kifPoolWithdrawal_trash"];
		if($page==-1) $page=$_SESSION["kifPoolWithdrawal_page"];
  
      //init session
      $_SESSION["kifPoolWithdrawal_status"]=$status;
      $_SESSION["kifPoolWithdrawal_kifPoolId"]=$kifPoolId;
      $_SESSION["kifPoolWithdrawal_searchWord"]=$searchWord;
      $_SESSION["kifPoolWithdrawal_trash"]=$trash;
      $_SESSION["kifPoolWithdrawal_page"]=$page;
		
		
		if($status=="success")
		   $transactionTitle="درخواست های تایید شده";	
		else if($status=="failed")
		   $transactionTitle="درخواست های تایید نشده";
		else if($status=="suspended")
		   $transactionTitle="درخواست های در انتظار تایید";		
		else
			$transactionTitle="درخواست های واریز";
	   if($trash=="1")
			$transactionTitle.=" حذف شده";

		if($searchWord) $transactionTitle.=" - جستجو ({$searchWord})";
		
		if($kifPoolId != "")
		{
			$user=@$oUsers->get($kifPoolId);
			$kifPoolUser="-{$user->userName}";
			$classBtnUserDialog="btn btn-success btn-image btn-image-right";
		}
		else
		{
			$kifPoolUser="";
			$classBtnUserDialog="btn btn-image btn-image-right";
		}

		//START PAGE ...................................	
      $items=$oKifPool->transaction_getAll([
		   'type'=>'out',
		   'status'=>$status,
		   'purpose'=>'withdrawal',
			'kifPoolId'=>$kifPoolId,
			'trash'=>$trash,
			'searchWord'=>$searchWord,
			'limitStr'=>''
		]);
      $count=count($items);
		$pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode=""; 
		if($pages['pagesEnd'] > 1)
		{
			for($i=$pages['pagesStart'];$i <= $pages['pagesEnd'];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
				else
					$btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='kifPoolWithdrawal_draw(\"auto\",undefined,{$i});'>{$i}</a>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages['isPrevious']) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='kifPoolWithdrawal_draw(\"auto\",undefined,{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
			if($pages['isNext']) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='kifPoolWithdrawal_draw(\"auto\",undefined,{$next});'><i class='fa fa-angle-left'></i></a>";
		}
		$items=$oKifPool->transaction_getAll([
		   'type'=>'out',
		   'status'=>$status,
		   'purpose'=>'withdrawal',
			'kifPoolId'=>$kifPoolId,
			'trash'=>$trash,
			'searchWord'=>$searchWord,
		   'limitStr'=>$pages['sqlLimit']
		]);		
		//END PAGE ...................................

      $i=0;
      $code="";
      $trCode="";
				
      foreach($items as $item)
      {
         $i++;
         $id=$item->id;
         $date=jdate("Y/m/d h:i:s",$item->insertDate);

	      //payStatus
			$statusTitle=$arryStatusTitle2[$item->status];
			$statusColor=$arryStatusColor[$item->status];
			$html_status="<span class='lbl fg-{$statusColor}'>{$statusTitle}</span><br>";
			if($item->status=='suspended') 
			{
				$html_status="<button class='btn btn-info' onclick='kifPoolWithdrawal_view({$item->id});'>تایید / عدم تایید</button>";
			   $btn_edit="<button class='btn btn-info' onclick='kifPoolWithdrawal_edit({$item->id});'><i class='fa fa-pencil'></i></button>";
			}
			else
			{
				$btn_edit='';
			}

					
			//amount
			$amountTransaction=number_format($item->amount,0,".",",");
			$kifpoolAmount=number_format($item->kifPoolAmount,0,".",",");
			
         if($item->checkout=="0") {$b="<b>";$b_="</b>";} else {$b="";$b_="";}
			
			//user
			$user=@$oUsers->get($item->kifPoolId);
			if($user)
			   $html_user=@"<a href='javascript:void(0)' onclick='user_profileDraw({$user->userId});'><u>{$user->userName}</u></a>";
			else
				$html_user='<i class="fa fa-info-circle fa-2x fg-grayLight"></i>';
			
			//trash by admin
         if($item->trash == 1)
         {
            $trashById=$item->trashById;
            if($trashById)
            {
               $admin=new cAdmin();
               $trashByAdmin=$admin->get($trashById,true)["profile"];
               $html_trashByAdmin=$trashByAdmin->userName . '<br>' . $trashByAdmin->fName . ' ' . $trashByAdmin->lName;
            }
				if(!$html_trashByAdmin) $html_trashByAdmin='<i class="fa fa-info-circle fa-2x fg-grayLight"></i>'; 
         }
			
         $trCode.="
         <tr>
            <td><input type='checkbox' id='chk_{$i}' value='{$item->id}' onclick='checkedOne();' />&nbsp;{$b}{$b_}</td>
				<td>{$b}{$html_user}{$b_}</td>";
				if($item->trash == 1) $trCode.="<td>{$b}{$html_trashByAdmin}{$b_}</td>";
				$trCode.="
				<td>{$b}{$date}{$b_}</td>
				<td>{$b}{$amountTransaction} تومان{$b_}</td>
				<td>{$b}{$html_status}{$b_}</td>
				<td>{$b}{$kifpoolAmount} تومان{$b_}</td>
            <td>";
				   $trCode.="";
					if($item->trash == '1') $trCode.="
					<button class='btn btn-success' onclick='kifPoolWithdrawal_unTrash({$item->id});'><i class='fa fa-undo'></i></button>
					<button class='btn btn-danger' onclick='kifPoolWithdrawal_del({$item->id});'><i class='fa fa-close'></i></button>
					";
					else 
					$trCode.="<button class='btn btn-danger' onclick='kifPoolWithdrawal_trash({$item->id});'><i class='fa fa-trash-o'></i></button>";
					$trCode.="
				   {$btn_edit}
				</td>
         </tr>";
      }
		$chkCount=$i;
		if($trCode)
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$transactionTitle}{$kifPoolUser}</h1>
			<div class='vSpace-4x'></div>";
			
			if($trash == '1') 
				$code.="<button id='btn_trash' class='btn btn-danger' onclick='kifPoolWithdrawal_del(\"selected\");'><i class='fa fa-remove'></i></button>";
			else
				$code.="<button id='btn_trash' class='btn btn-danger' onclick='kifPoolWithdrawal_trash(\"selected\");' disabled><i class='fa fa-trash-o'></i></button>";
			$code.="
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='kifPoolWithdrawal_draw(\"auto\",$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>
	
			<div class='vSpace'></div>
				
			<div class='hScrollBar-auto'>
				<table class='tbl tbl-center tbl-bordered tbl-hover'>
					<tr>
						<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>";
						if($trash == '1') $code.="<th>حذف کننده</th>";
						$code.="
						<th>کاربر</th>						
						<th>تاریخ درخواست</th>
						<th>مبلغ</th>
						<th>وضعیت</th>
						<th>مانده</th>
						<th></th>
					</tr>
					{$trCode}
				</table>
			</div>
			<br>{$btnPageCode}<br>
			<br><br>
			";
		}
		else
		{
			$code= "
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-list'></i>&nbsp;{$transactionTitle}{$kifPoolUser}</h1>
			<div class='vSpace-4x'></div>
			
			<div class='input-control input-control-right'>
			   <span class='input-control-button' onclick='kifPoolWithdrawal_draw(\"auto\",$(\"#txt_search\").val());'><i class='fa fa-search'></i></span>
				<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/>
			</div>				
				
			<hr>
			<h1 class='algn-c fg-gray'><i class='fa fa-info'></i>&nbsp;خالی است</h1>
			<div class='vSpace-4x'>";				
		}
		$oEngine->response("ok[|]" . $code);		
   }//------------------------------------------------------------------------------------
   else if($requestName=="kifPoolWithdrawal_del")
   {
      $transactionId=cDataBase::escape($_REQUEST["id"],false);
      $oKifPool->transaction_delete($transactionId); 		
		//get session for back
		$kifPoolId=$_SESSION["kifPoolTransaction_kifPoolId"];
		$selectPage=$_SESSION["kifPoolTransaction_page"];
		
		$oEngine->response("ok");
		exit;
   }//------------------------------------------------------------------------------------
   else if($requestName=="kifPoolWithdrawal_trash")
   {
      $transactionId=cDataBase::escape($_REQUEST["id"],false);		
      $oKifPool->transaction_trash($transactionId);     
		//get session for back
		$kifPoolId=$_SESSION["kifPoolTransaction_kifPoolId"];
		$selectPage=$_SESSION["kifPoolTransaction_page"];
		
		$oEngine->response("ok[|]{$kifPoolId}[|]{$selectPage}");
		exit;
   }//------------------------------------------------------------------------------------	
   else if($requestName=="kifPoolWithdrawal_unTrash")
   {
      $transactionId=cDataBase::escape($_REQUEST["id"],false);		
      $oKifPool->transaction_unTrash($transactionId);     
		//get session for back
		$kifPoolId=$_SESSION["kifPoolTransaction_kifPoolId"];
		$selectPage=$_SESSION["kifPoolTransaction_page"];
		
		$oEngine->response("ok[|]{$kifPoolId}[|]{$selectPage}");
		exit;
   }//------------------------------------------------------------------------------------
	else if($requestName=="kifPoolWithdrawal_edit")
   {
      $id=cDataBase::escape($_REQUEST["id"]);
		$transaction=@$oKifPool->transaction_get($id);
		if(!$transaction)
		{
			cEngine::response("err");
			exit;
		}			
		$oKifPool->transaction_setCheckout($transaction->id,'1');
		$user=@$oUsers->get($transaction->kifPoolId);
		$amount=number_format($transaction->amount) . ' تومان';
		$date=jdate("Y/m/d h:i:s",$transaction->insertDate);
		$statusTitle=$arryStatusTitle2[$transaction->status];
		$statusColor=$arryStatusColor[$transaction->status];
		$code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;درخواست برداشت (<i class='fa fa-user'></i>&nbsp;{$user->userName})</h1>
		<div class='vSpace-4x'></div>
		
		<div class='panel'>
		   <div class='panel-body form'>
			   <label>شماره تراکنش : {$transaction->id}</label>
				<label>تاریخ درخواست : {$date}</label>
				<label>مبلغ برداشت : {$amount}</label>
				<label>وضعیت : <span class='lbl fg-{$statusColor}'>{$statusTitle}</span></label>
				
				<hr>
				
				<label>شماره شبا</label>
				<input type='number' id='txt_otherData1' value='{$transaction->otherData1}' class='dir-ltr'>
				
				<div class='vSpace'></div>
				
				<label>شماره کارت</label>
				<input type='number' id='txt_otherData2' value='{$transaction->otherData2}' class='dir-ltr'>
				
				<hr>
				
				<button class='btn' onclick='kifPoolWithdrawal_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
				<button class='btn btn-success' onclick='kifPoolWithdrawal_update({$transaction->id});'>ذخیره</button>			
			</div>
		</div>
		<div class='vSpace-4x'></div>
		";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
	else if($requestName=="kifPoolWithdrawal_view")
   {
      $id=cDataBase::escape($_REQUEST["id"]);
		$transaction=@$oKifPool->transaction_get($id);
		if(!$transaction)
		{
			cEngine::response("err");
			exit;
		}			
		$oKifPool->transaction_setCheckout($transaction->id,'1');
		$user=@$oUsers->get($transaction->kifPoolId);
		$amount=number_format($transaction->amount) . ' تومان';
		$date=jdate("Y/m/d h:i:s",$transaction->insertDate);
		$statusTitle=$arryStatusTitle2[$transaction->status];
		$statusColor=$arryStatusColor[$transaction->status];
		$code="
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-pencil'></i>&nbsp;درخواست برداشت (<i class='fa fa-user'></i>&nbsp;{$user->userName})</h1>
		<div class='vSpace-4x'></div>
		
		<div class='panel'>
		   <div class='panel-body form'>
			   <label>شماره تراکنش : {$transaction->id}</label>
				<label>تاریخ درخواست : {$date}</label>
				<label>مبلغ برداشت : {$amount}</label>
				<label>وضعیت : <span class='lbl fg-{$statusColor}'>{$statusTitle}</span></label>
				<label>شماره شبا : {$transaction->otherData1}</label>
				<label>شماره کارت : {$transaction->otherData2}</label>
				
				<hr>
				
				<label>توضیحات</label>
				<label>در صورت تایید نکردن، می توانید دلیل آن را به کاربر اطلاع رسانی کنید</label>
				<textarea id='txt_comment'></textarea>
				
				<hr>
				
				<button class='btn' onclick='kifPoolWithdrawal_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
				<button class='btn btn-success' onclick='kifPoolWithdrawal_setConfirm({$transaction->id},0);'>تایید می شود</button>			
				<button class='btn btn-danger' onclick='kifPoolWithdrawal_setConfirm({$transaction->id},1);'>تایید نمی شود</button>			
			</div>
		</div>
		<div class='vSpace-4x'></div>
		";
		
      cEngine::response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
	else if($requestName=="kifPoolWithdrawal_update")
   {
      $id=cDataBase::escape($_REQUEST["id"]);
      $otherData1=cDataBase::escape($_REQUEST["otherData1"]);
      $otherData2=cDataBase::escape($_REQUEST["otherData2"]);
		//---
      $oKifPool->transaction_setOtherData1($id, $otherData1);
      $oKifPool->transaction_setOtherData2($id, $otherData2);
      //---

      cEngine::response("ok");
		exit;
   }//------------------------------------------------------------------------------------ 	
	else if($requestName=="kifPoolWithdrawal_setConfirm")
   {
      $id=cDataBase::escape($_REQUEST["id"]);
      $status=cDataBase::escape($_REQUEST["confirm"]);
      $comment=cDataBase::escape($_REQUEST["comment"]);
		//---
		if($status==1) $status='success'; else if($status=='0') $status='failed';
      $oKifPoo->transaction_setStatus($id, $status);
      $oKifPoo->transaction_setComment($id, $comment);
      //---
		//$userId=$oKifPool->transaction_get($id)->kifPoolId;
		//$oUsersInform->insert(['userId'=>$userId, 'message'=>]);
		
      cEngine::response("ok[|]{$status}");
		exit;
   }//------------------------------------------------------------------------------------  	
?>
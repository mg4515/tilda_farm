var url_contactUs=oPath.manage("contactUs_bundle/controller/php/contactUs_ajax.php");
function contactUs_send() {
	try {
		err=false;
		$("#layer_msg").html("");
		if($("#txt_name").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا نام خود را وارد نمایید")
         err=true;			
		}
		else if($("#txt_subject").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا موضوع پیغام را وارد نمایید")
         err=true;			
		}
		else if($("#txt_mail").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل خود را وارد نمایید")
         err=true;			
		}
		else if(!oTools.isMail($("#txt_mail").val()))
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل خود را به صورت صحیح وارد نمایید")
         err=true;		
		}		
		else if($("#txt_mobile").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا شماره موبایل خود را وارد نمایید")
         err=true;			
		}
		else if($("#txt_message").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا پیغام خود را وارد نمایید")
         err=true;			
		}
		else if($("#txt_sec").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا کد امنیتی در تصویر را وارد نمایید")
         err=true;			
		}
		
		if(err==false)
		{
			$('#btn_send').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
				url: url_contactUs,
				data: {
					"requestName":"contactUs_send",
					"name":$("#txt_name").val(),
					"subject":$("#txt_subject").val(),
					"mail":$("#txt_mail").val(),
					"mobile":$("#txt_mobile").val(),
					"message":$("#txt_message").val(),
					"sec":$("#txt_sec").val()
				},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						script_alertStatus("spn_alert","success","پیغام شما با موفقیت ارسال شد");
						$("#txt_name").val('');
						$("#txt_subject").val('');
						$("#txt_mail").val('');
						$("#txt_mobile").val('');
						$("#txt_message").val('');
						//---
						$("#txt_sec").val('');
						var d = new Date();
						var t = d.getTime();
						document.getElementById('img_sec').setAttribute("src",oPath.public("client/contents/security/sec_contactUs.php") + "?t=" + t);                  						
					}						
					else if(spl[0]=="errSec")
						script_alertStatus("spn_alert","danger","کد امنیتی با تصویر مطابقت ندارد");	
					$('#btn_send').html('ارسال');
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					$('#btn_send').html('ارسال');
				}
			});
		}
	} 
	catch (e) 
	{
	   alert("خطای اسکریپت");
	   $('#btn_send').html('ارسال');
	}
}//---------------------------------------------------------------------------------------
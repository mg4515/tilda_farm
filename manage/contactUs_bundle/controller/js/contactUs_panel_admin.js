var url_contactUs=oPath.manage("contactUs_bundle/controller/php/contactUs_panel_admin.php");
function contactUs_del(id) 
{
   try {
		script_confirm2('پیغام حذف شود؟','',function()
		{			
			script_loadingShow();
			$.ajax({
				url: url_contactUs,
				data: {"requestName":"contactUs_del","id":id},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						script_alert2('','با موفقیت انجام شد','success');
						contactUs_draw('auto');
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() 
				{
					alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
				}
			});
		});
	}
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function contactUs_view(id) {
	try {
	   script_loadingShow();
	   $.ajax({
			url: url_contactUs,
			data: {"requestName":"contactUs_view","id":id},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					$("#layer_content").html(spl[1]);
					CKEDITOR.replace('txt_contentMail');
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	} catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function contactUs_draw(trash, searchWord, page) {
    try {
        script_loadingShow();
        $.ajax({
            url: url_contactUs,
            data: {"requestName":"contactUs_draw", "trash":trash, "searchWord":searchWord, "page":page},
            method: "POST",
            success: function(result)
            {
					result=oEngine.response(result);
               var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						$("#layer_content").html(spl[1]);
					}
               script_loadingHide();
            },
            error: function() 
				{
               alert("خطایی در اتصال رخ داده است");
					script_loadingHide();
            }

        });
   } 
	catch (e) 
	{
      alert("خطای اسکریپت");
	   script_loadingHide();
   }
}//---------------------------------------------------------------------------------------
function contactUs_sendMail(id) {
	//try {
		script_loadingShow();
		var content = CKEDITOR.instances.txt_contentMail.getData();
		content.replace(/(?:\\[rn])+/g, "<br>");
		content=oTools.str_tagFitSend_encode(content);
	   $.ajax({
			url: url_contactUs,
			data: {"requestName":"contactUs_sendMail","id":id,"content":content},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
				{
					alert("با موفقیت انجام شد");
					contactUs_draw(-1,-1,-1);
				}
				script_loadingHide();
			},
			error: function() 
			{
				alert("خطایی در اتصال رخ داده است");
				script_loadingHide();
			}
	   });
	//} catch (e) {
	  //alert("خطای اسکریپت");
	  //script_loadingHide();
	//}
}//---------------------------------------------------------------------------------------
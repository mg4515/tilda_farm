<?php
   @session_start();

	//include
	include_once $_SESSION["engineRequire"]; //engine.php
   include_once $oPath->manageDir('jdf.php');
   require_once $oPath->manageDir('contactUs_bundle/model/contactUs_model.php');
   require_once $oPath->manageDir('mail_bundle/model/mail_model.php');
   require_once $oPath->manageDir('site_bundle/model/sitePro_model.php');

	//object
   $oMail=new cMail();
   $oSitePro=new cSitePro();
   $oContactUs=new cContactUs();
	
	//request
   $request=@$_REQUEST["requestName"];
	
	//request process
   if($request=="contactUs_send")
   {
      $sec=$_REQUEST["sec"];
      if($sec!=$_SESSION["sec_contactUs"])
      {
			$oEngine->response("errSec");
         exit;
      }
      else
      {
         $name=$oDb->escape($_REQUEST["name"]);
         $mail=$oDb->escape($_REQUEST["mail"]);
         $mobile=$oDb->escape($_REQUEST["mobile"]);
         $subject=$oDb->escape($_REQUEST["subject"]); //mail
         $comment=$oDb->escape($_REQUEST["message"]);
         
			$oContactUs->insert(["name"=>$name,"mail"=>$mail,"mobile"=>$mobile,"subject"=>$subject,"comment"=>$comment]);
			
         //send mail
         $mailFrom=$oSitePro->getMailNoreplay();
         $mailto = $oSitePro->getMailAdmin(); //daryaft konanndeh
         $message="
            <h1>(پیغام از طرف {$name})</h1>
            <hr>
				موضوع : {$subject} <br />
            نام : {$name}<br />
            ايميل : {$mail} <br />
            موبایل : {$mobile} <br />
            <br />
            <br />
            <h2>متن پیغام : </h2>
            {$comment}
            <hr>";
         $oMail->send($mailto,"contactUs new",$message);
         $oEngine->response("ok");
      }
   }//------------------------------------------------------------------------------------
?>
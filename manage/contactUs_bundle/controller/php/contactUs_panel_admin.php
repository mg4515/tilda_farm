<?php
   @session_start();

	//include
	include_once $_SESSION["engineRequire"]; //engine.php
   include_once $oPath->manageDir('jdf.php');
   require_once $oPath->manageDir('contactUs_bundle/model/contactUs_model.php');
   require_once $oPath->manageDir('mail_bundle/model/mail_model.php');
   require_once $oPath->manageDir('site_bundle/model/sitePro_model.php');

	//object
   $oMail=new cMail();
   $oSitePro=new cSitePro();
   $oContactUs=new cContactUs();
	
	//request name
	$requestName=@$_REQUEST['requestName'];
	
	//request action
   if($requestName=="contactUs_draw")
   {
		$page=isset($_REQUEST["page"]) ? $oDb->escape($_REQUEST["page"]) : 1;
		$trash=isset($_REQUEST['trash']) ? $oDb->escape($_REQUEST['trash']) : "";
		$searchWord=isset($_REQUEST['searchWord']) ? $oDb->escape($_REQUEST['searchWord']) : "";
		
		//auto value
		if($page==-1) $page=$_SESSION["contactUs_page"];
		if($trash==-1) $trash=$_SESSION["contactUs_trash"];
		if($searchWord==-1) $searchWord=$_SESSION["contactUs_searchWord"];
		
		//save session
		$_SESSION["contactUs_page"]=$page;
		$_SESSION["contactUs_trash"]=$trash;
		$_SESSION["contactUs_searchWord"]=$searchWord;
		
      $i=0;
      $code="";
      $codeTr="";
      
      //START PAGE ...................................
		$items=$oContactUs->getAll(['trash'=>$trash, 'searchWord'=>$searchWord]);
		$count=count($items);
		$pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
      if($pages["pagesEnd"] > 1)
		{
			for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
			{
				if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
					$btnPageCode.="<button class='btn btn-fix-36 btn-default' disabled>{$i}</button>";
				else
					$btnPageCode.="<button class='btn btn-fix-36 bg-info' onclick='contactUs_draw(-1,-1,{$i});'>{$i}</button>";
			}
			$back=$page - 1;
			$next=$page + 1;
			if($pages["isPrevious"]) $btnPageCode="<button class='btn btn-fix-36 btn-primary' onclick='contactUs_draw(-1,-1,{$back});'><i class='fa fa-angle-right'></i></button>" . $btnPageCode;
			if($pages["isNext"]) $btnPageCode=$btnPageCode . "<button class='btn btn-fix-36 btn-primary' onclick='contactUs_draw(-1,-1,{$next});'><i class='fa fa-angle-left'></i></button>";
      }
		//---
		$items=$oContactUs->getAll(['trash'=>$trash, 'searchWord'=>$searchWord, 'limitStr'=>$pages['sqlLimit']]);
		//END PAGE ...................................
		
      foreach($items as $item)
      {
         $i++;
			$date=jdate("Y/m/d h:i:s",$item->date);
	
			if($item->checkout=="0") $style="style='font-weight: bolder;color:#000;'"; else $style="color:#666";
         if($item->trash=="1") 
			{
				$btnDel="
				<button class='btn btn-danger' onclick='contactUs_del({$item->id});'><i class='fa fa-remove'></i></button>
				<button class='btn btn-warning' onclick='contactUs_unTrash({$item->id});'><i class='fa fa-undo'></i></button>";				
			}
			else 
			{
				$btnDel="<button class='btn btn-danger' onclick='contactUs_del({$item->id});'><i class='fa fa-trash-o'></i></button>";				
			} 
			
			$codeTr.="
			<tr>
				<td {$style}>{$item->id}</td>
				<td {$style}>{$item->subject}</td>
				<td {$style}>{$date}</td>
				<td {$style}>{$item->name}</td>
				<td {$style}>
				   <button class='btn btn-info' onclick='contactUs_view({$item->id})'><i class='fa fa-eye'></i></button>
					{$btnDel}				
				</td>
         </tr>";
      }
		
      $code= "
      <div class='vSpace-4x'></div>
      <h1 class='title-h1 dir-rtl algn-r'><i class='fa fa-envelope'></i>&nbsp;ارتباط با ما</h1>
		<div class='vSpace-4x'></div>
		";
		
		if($codeTr=="") 
		{
		   $code.="<hr><h1 style='color:#ccc;text-align:center'><i class='fa fa-envelope'></i>&nbspپیغامی وجود ندارد</h1>";	
		}
		else
		{
			$code.="
			<table class='tbl tbl-bordered tbl-hover' style='direction:rtl'>
				<tr>
					<th>شناسه پیغام</th>
					<th>موضوع</th>
					<th>تاریخ ارسال</th>
					<th>ارسال کننده</th>
					<th></th>
				</tr>
				{$codeTr}
			</table>
			{$btnPageCode}
			<br><br>";
		}
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   if($requestName=="contactUs_del")
   {
      $contactUsId=$oDb->escape($_REQUEST["id"]);
      $page=$oDb->escape($_REQUEST["page"]);
		$oContactUs->delete($contactUsId);
      @unlink($oPath->manageDir("contactUs_bundle/data/{$id}.html"));
      $oEngine->response("ok[|]{$page}");
   }//------------------------------------------------------------------------------------
   if($requestName=="contactUs_view")
   {
      $id=$oDb->escape($_REQUEST["id"]);
      $page=$oDb->escape($_REQUEST["page"]);
      if(file_exists($oPath->manageDir("contactUs_bundle/data/{$id}.html")))
         $content=file_get_contents($oPath->manageDir("contactUs_bundle/data/{$id}.html"));
      else 
			$content="";
      $oContactUs->setCheckout($id, 1);
      $item=$oContactUs->get($id);
      $code="
			<div class='vSpace-4x'></div>
			<h1><i class='fa fa-eye'></i>&nbsp;نمایش پیغام</h1>
			<div class='vSpace-4x'></div>
		
         <div class='panel'>
            <div class='panel-body'>
				   <h3>مشخصات</h3>
               <label><i class='fa fa-tag'></i>{$item->subject}</label>
               <label><i class='fa fa-user'></i>{$item->name}</label>
               <label><i class='fa fa-envelope'></i>{$item->mail}</label>
               <label><i class='fa fa-mobile'></i>{$item->mobile}</label>	
            </div>					
         </div>
         
			<div class='vSpace'></div>

         <div class='panel'>
            <div class='panel-body'>
				   <h3>متن پیغام کاربر</h3>
               <label>{$item->comment}</label>
            </div>					
         </div>
			
			<div class='vSpace'></div>
			";

			if($content)
			{
				$code.="
				<div class='panel'>
					<div class='panel-body'>
						<h3>آخرین پاسخ شما به کاربر</h3>
						<label>{$content}</label>
					</div>					
				</div>
				
				<div class='vSpace'></div>
				";				
			}
			
			
		$code.="	
         <div class='panel'>
            <div class='panel-body'>
				   <h3>ارسال پاسخ به ایمیل کاربر</h3>
					<div class='vSpace'></div>
					<textarea id='txt_contentMail'></textarea><br />
					<hr>
					<button class='btn btn-default' onclick='contactUs_draw({$page});'><i class='fa fa-arrow-right'></i></button>
					<button class='btn btn-success' onclick='contactUs_sendMail({$id});'>ارسال پاسخ</button><br /><br />
            </div>					
         </div>
			<div class='vSpace-4x'></div>
      ";
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="contactUs_sendMail") 
   {
      $id=$oDb->escape($_REQUEST["id"]);
      $content=$oTools->str_tagFitSend_decode($_REQUEST["content"]);
		if(!file_exists($oPath->manageDir("contactUs_bundle/data"))) mkdir($oPath->manageDir("contactUs_bundle/data"));
      file_put_contents($oPath->manageDir("contactUs_bundle/data/{$id}.html"),$content);
		
      $item=$oContactUs->get($id);

      //send mail
      $siteAddress=$oPath->root();
      $siteTitle=$oSitePro->getTitle();
      $mailto = $item->mail; //daryaft konanndeh
      $message="
			پیغام شما : <br />{$item->comment}
			<hr>
			پاسخ : <br />{$content}
			<br />
			<br />
			<hr>
			<a href='{$siteAddress}'>{$siteTitle}</a>";
      $oMail->send($mailto,"contactUs - pasokh",$message);
      $oEngine->response("ok");
   }//------------------------------------------------------------------------------
?>
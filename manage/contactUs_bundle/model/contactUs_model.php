<?php
class cContactUs
{
   public $tbl_contactUs="contactUs"; //contactUs

   function getAll($argArray)
   {
		global $oDbq;
		
		$sortByRow=@$argArray["sortByRow"] ? $argArray["sortByRow"] : true;
		$searchWord=@$argArray["searchWord"] ? $argArray["searchWord"] : "";
		$trash=@$argArray["trash"] ? $argArray["trash"] : "";
		$limitStr=@$argArray["limitStr"] ? $argArray["limitStr"] : "";	
		
      $where="";
		if($searchWord !== "")
		{
			if($where=="") 
				$where="(`subject` LIKE '%{$searchWord}%' OR `comment` LIKE '%{$searchWord}%' OR `mail` LIKE '%{$searchWord}%' OR `mobile` LIKE '%{$searchWord}%')";
		   else
				$where.=" AND (`subject` LIKE '%{$searchWord}%' OR `comment` LIKE '%{$searchWord}%' OR `mail` LIKE '%{$searchWord}%' OR `mobile` LIKE '%{$searchWord}%')";
		}
      if($trash!=="")
      {
         if($where=="") $where="`trash`='{$trash}'"; else $where.=" AND `trash`='{$trash}'";
      }						
		
      if($sortByRow==true)$ret=$oDbq->table($this->tbl_contactUs)->fields("*")->where($where)->orderBy("`id` DESC");   
		else $ret=$oDbq->table($this->tbl_contactUs)->fields("*")->where($where)->orderBy("`id` DESC");  
      
		if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//------------------------------------------------------------------------------------
   function get($id)
   {
		global $oDbq;
      $item=$oDbq->table($this->tbl_contactUs)->fields("*")->where("`id`='{$id}'")->select()[0];
      return $item;
   }//--------------------------------------------------------------------------
	function delete($id)
	{
		global $oDbq;
		$where="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_contactUs)->fields("*")->where("({$where})")->delete();	
		return $ids;		
	}//---------------------------------------------------------------------------
	function trash($id)
	{
		global $oDbq;
		$where="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_contactUs)->set("`trash`=1")->where("({$where})")->update();
		return $ids;		
	}//---------------------------------------------------------------------------
	function unTrash($id)
	{
		global $oDbq;
		$where="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_contactUs)->set("`trash`=0")->where("({$where})")->update();
		return $ids;		
	}//---------------------------------------------------------------------------
   function insert($array)
   {
		global $oDbq;
		$name=$array["name"];
		$mail=$array["mail"];
		$mobile=$array["mobile"];
		$subject=$array["subject"];
		$comment=$array["comment"];
		$id=time();
		$date=$id;
	   $oDbq->table($this->tbl_contactUs)->set("`id`={$id}, `name`='{$name}', `mail`='{$mail}', `mobile`='{$mobile}', `subject`='{$subject}', `comment`='{$comment}', date={$date}")->insert();		
   }//--------------------------------------------------------------------------
   function setCheckout($id, $value)
   {
		global $oDbq;
      $oDbq->table($this->tbl_contactUs)->set("`checkout`={$value}")->where("`id`={$id}")->update();		
   }//--------------------------------------------------------------------------
   function getCountNew()
   {
		global $oDbq;
      return count($oDbq->table($this->tbl_contactUs)->fields("`id`")->where("`checkout`=0 AND `trash`=0")->select());		
   }//--------------------------------------------------------------------------	
}

?>
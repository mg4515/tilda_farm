<?php
@session_start();
class cPay
{
   public $tbl_payType="payType";
	function getAll($active="")
   {
      global $oDbq;
      if($active=="") $ret=$oDbq->table($this->tbl_payType)->fields("*")->select();
      else $ret=$oDbq->table($this->tbl_payType)->fields("*")->where("`active`={$active}")->select();
		if(count($ret) > 0)
         return $ret;
		else
		{
			$oDbq->table($this->tbl_payType)->set("`idName`='kifPool'")->insert();
			if($active=="") return $oDbq->table($this->tbl_payType)->fields("*")->select();
			else return $oDbq->table($this->tbl_payType)->fields("*")->where("`active`={$active}")->select();
      }		
   }//--------------------------------------------------------------------------	
	function get($idName)
   {
      global $oDbq;
      $ret=$oDbq->table($this->tbl_payType)->fields("*")->where("(`id`='{$idName}' OR `idName`='{$idName}') AND `active`=1")->select();
      return @$ret[0];		
   }//--------------------------------------------------------------------------	
	function start($payTypeId,$orderId,$amount,$description,$callBackUrl,$otherData='') //start payment
	{
      global $oDbq,$oPath;
      $ret=@$oDbq->table($this->tbl_payType)->fields("*")->where("`id`='{$payTypeId}'")->select()[0];
		if($ret)
      {
			$_SESSION["pay_typeId"]=$payTypeId;
			$_SESSION["pay_thereIs"]=true;
         $idName=$ret->idName;

			include_once $oPath->manageDir("pay_bundle/pays/{$idName}/pay.php");
			$oPay_=new cPay_();
			$retStart= $oPay_->start($orderId,$amount,$description,$callBackUrl,$otherData);
			
			return $retStart;
		}
     	else
		{
         return 'err';
		} 			
	}//--------------------------------------------------------------------------
	function done($payTypeId='')
	{
      global $oDbq,$oPath;
		if(!$payTypeId) $payTypeId=$_SESSION["pay_typeId"];
      $ret=$oDbq->table($this->tbl_payType)->fields("*")->where("`id`='{$payTypeId}'")->select();
      if(count($ret) > 0)
      {
			$ret=$ret[0];
         $idName=$ret->idName;

			include_once $oPath->manageDir("pay_bundle/pays/{$idName}/pay.php");
			$oPay_=new cPay_();
			$retDone= $oPay_->done();
			
			return $retDone;
		}
     	else
         return []; 			
	}//--------------------------------------------------------------------------
	function thereIs()
	{
		if(@$_SESSION["pay_thereIs"]) return true; else return false;
	}//--------------------------------------------------------------------------
	function toEnd()
	{
		if(@$_SESSION["pay_thereIs"]) unset($_SESSION["pay_thereIs"], $_SESSION["pay_typeId"]);
	}//--------------------------------------------------------------------------
}

?>
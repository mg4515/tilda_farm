<?php
	require_once $oPath->manageDir('kifPool_bundle/model/kifPool_model.php');
	class cPay_ extends cKifPool
	{
		function start($orderId,$amount,$description,$callBackUrl,$otherData) // start payment
		{
			@session_start();
			$_SESSION["pay_orderId"]=$orderId;
			$_SESSION["pay_amount"]=$amount;
			$_SESSION["pay_description"]=$description;
			$_SESSION["pay_otherData"]=$otherData;
			//-----
			$ret=$oKifPool->transaction_insert([
				'kifPoolId'=>$kifPoolId,
				'amount'=>$amount,
				'type'=>'out',
				'purpose'=>'buy',
				'comment' => $description
			]); //not Applied
			if($ret['status'] == 1)
			{
				$callBackUrl=rtrim($callBackUrl,'/');
				if(strpos($callBackUrl,'?'))
					$url=$callBackUrl . "&token={$ret['token']}";
				else
					$url=$callBackUrl . "?token={$ret['token']}";
				return ['status'=>1, 'url'=>$url];
			}
			else
			{
				//-1 : The account is blocked
				//-2 : The account balance is not enough
				return ['status'=>$ret['status'], $url=''];
			}
		}//--------------------------------------------------------------------------
		public function done()
		{
			$token=@$_REQUEST['token'];
			if($token)
			{
				//token verify
				$transaction=@$this->transaction_getByToken($orderId);
				if($transaction->status=='failed')
				{
					$this->updateKifPool_by_transactionId($transaction->id);
					$ret["status"]="success";
					$ret["refId"]=$transaction->id;
					$ret["orderId"]=$_SESSION["pay_orderId"];
					$ret["amount"]=$_SESSION["pay_amount"];
					$ret["description"]=$_SESSION["pay_description"];
					$ret["otherData"]=$_SESSION["pay_otherData"];
				}
				else
				{
					$ret["status"]="exists";
					$ret["refId"]=$transaction->id;
					$ret["orderId"]=$_SESSION["pay_orderId"];
					$ret["amount"]=$_SESSION["pay_amount"];
					$ret["description"]=$_SESSION["pay_description"];
					$ret["otherData"]=$_SESSION["pay_otherData"];
				}
				return $ret;
			}
			else
				return ["status"=>'failed'];
		}//------------------------------------------------------------------------------------
	}
?>
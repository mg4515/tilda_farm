﻿<?php
//shaparak parsian
class cPay_{
   private $soap_class_url='lib/nusoap.php';
   private $client;
	private $namespace;
	private $terminalId=0;       //write Terminal ID here
	private $pin="";
   public  $startUrl="https://pec.shaparak.ir/NewIPG/?Token=";
	function __construct(){
		require_once($this->soap_class_url);
		$this->client = new nusoap_client('https://pec.shaparak.ir/NewIPGServices/Sale/SaleService.asmx?WSDL',true);
		$this->namespace='http://interfaces.core.sw.bps.com/';
	}

	function __set($property, $value)
	{
		$result = FALSE;
		$this->$property = $value;
		$result=TRUE;
		return $result;
	}

	function start($orderId,$amount,$description,$callBackUrl,$otherData='')
	{
		@session_start();
      $_SESSION["pay_orderId"]=$orderId;
      $_SESSION["pay_amount"]=$amount;
		$_SESSION["pay_description"]=$description;
		$_SESSION["pay_otherData"]=$otherData;
		//-----
	   $result=null;
	   require_once($this->soap_class_url);

		$params = array (
			"LoginAccount" => $this->pin,
			"Amount" => $amount,
			"OrderId" => $orderId,
			"CallBackUrl" => $callBackUrl 
		);

	  // Check for an error
		if ($err = $this->client->getError())
			return ['status'=>'failed', 'url'=>''];
	   else if ($res=$this->client->fault) {
			return ['status'=>'failed', 'url'=>''];
	   else 
		{
			$res=($this->client->call('SalePaymentRequest', array ("requestData" =>$params)));
			if($res["SalePaymentRequestResult"]["Status"]==0 && $res["SalePaymentRequestResult"]["Token"])
			{
				//Redirect to URL You can do it also by creating a form
				$url="https://pec.shaparak.ir/NewIPG/?Token=" . $res["SalePaymentRequestResult"]["Token"];
				return ['status'=>'success', 'url'=>$url]
			}				
		   else
			{
				return ['status'=>'failed', 'url'=>''];
			}
	   }
	}//------------------------------------------------------------------------------------------------------------------------	
	private function verify($token)
	{
      $result=null;
      
		require_once($this->soap_class_url);
		$client = new nusoap_client('https://pec.shaparak.ir/NewIPGServices/Confirm/ConfirmService.asmx?WSDL',true);
		
		$params=array(
    		'LoginAccount' => $this->pin,
    		'Token' => $token
		);
		
	  // Check for an error
		if ($err = $this->client->getError()) 
		{
			return ["status"=>"err","message"=>$err);
			exit;
		}
	   else if ($res=$this->client->fault) {
			return ["status"=>"err","message"=>$res];
			exit;
	   }
		else 
		{
			$ret=[];
			$res=$client->call('ConfirmPayment', array ("requestData" =>$params));
			if($res["ConfirmPaymentResult"]["Status"]==0)
			{
				$ret["status"]="success";
            $ret["refId"]=$res["ConfirmPaymentResult"]["RRN"];
            $ret["orderId"]=$_SESSION["pay_orderId"];
            $ret["amount"]=$_SESSION["pay_amount"];
            $ret["description"]=$_SESSION["pay_description"];
            $ret["otherData"]=$_SESSION["pay_otherData"];
            return $ret;				
			}
			else if($res["ConfirmPaymentResult"]["Status"]==-1533)
			{
            $ret["status"]="exists";
            $ret["refId"]=@$res["ConfirmPaymentResult"]["RRN"];
            $ret["orderId"]=$_SESSION["pay_orderId"];
            $ret["amount"]=$_SESSION["pay_amount"];
				$ret["description"]=$_SESSION["pay_description"];
				$ret["otherData"]=$_SESSION["pay_otherData"];
            return $ret;			
			}				
         else
         {
            return ['status'=>'err'];
         }
      }
      else
      {
         return ['status'=>'canceled'];
      }
   }//-----------------------------------------------------------------------------	
	function done()
	{
		if(isset($_POST['Token']))
		{
			$token = $_REQUEST ["Token"];
			$status = $_REQUEST ["status"]; //$status == 0
			$orderId = $_REQUEST ["OrderId"];
			$TerminalNo = $_REQUEST ["TerminalNo"];
			$Amount = $_REQUEST ["Amount"];
			$RRN = $_REQUEST ["RRN"]; // $RRN > 0

			$verifyRet=@$this->verify($token);
			return $verifyRet;
	
	}//-----------------------------------------------------------------------------
}

//$oPay=new cPayParsian();
//$ret=$oPay->start(time(),1000,"https://www.marketfo.com");
//header("Location:" . $ret["startUrl"]);
//echo "<script>window.location.href='{$ret["startUrl"]}';</script>";
?>
<?php
//shaparak zarinpal
class cPay_{
   private $merchantID_ = 'd6d55ee0-1f49-11e6-8672-000c295eb8fc'; //old
   private $merchantID = '6a539182-8ac7-11e9-a85a-000c29344814'; //new
   private $client;

   function __construct()
   {
      //$this->client = new NuSOAP_Client('https://pgws.bpm.bankmellat.ir/pgwchannel/services/pgw?wsdl');
      $this->client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
   }

   function start($orderId,$amount,$description,$callBackUrl,$otherData='')
	{
      @session_start();
      $_SESSION["pay_orderId"]=$orderId;
      $_SESSION["pay_amount"]=$amount;
      $_SESSION["pay_description"]=$description;
      $_SESSION["pay_otherData"]=$otherData;
		//-----
      $result = $this->client->PaymentRequest(
      [
         'MerchantID' => $this->merchantID,
         'Amount' => $amount,
         'Description' => $description,
         'Email' => '',
         'Mobile' => 0,
         'CallbackURL' => $callBackUrl,
      ]);
      if ($result->Status == 100)
      {
			//Redirect to URL You can do it also by creating a form
			$url="https://www.zarinpal.com/pg/StartPay/" . $result->Authority .'/ZarinGate';
			return['status'=>'success', 'url'=>$url];
			//return "https://www.zarinpal.com/pg/StartPay/" . $result->Authority;
         //���� ������� �� ���� ��� ���� ���� �� ���� ��� ����� ���:
         //Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority.'/ZarinGate');
      }
      else
      {
         //echo'ERR: '.$result->Status;
         return ['status'=>'failed', 'url'=>''];
      }		
	}//------------------------------------------------------------------------------------
	private function verify($status,$authority)
   {
      @session_start();
      $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
      if ($status == 'OK')
      {
         $result = $client->PaymentVerification(
            [
               'MerchantID' => $this->merchantID,
               'Authority' => $authority,
               'Amount' => $_SESSION["pay_amount"],
            ]
         );
         if ($result->Status == 100)
         {
				$ret["status"]="success";
            $ret["refId"]=$result->RefID;
            $ret["orderId"]=$_SESSION["pay_orderId"];
            $ret["amount"]=$_SESSION["pay_amount"];
				$ret["description"]=$_SESSION["pay_description"];
				$ret["otherData"]=$_SESSION["pay_otherData"];
            return $ret;
         }
         else if($result->Status == 101)
         {
            $ret["status"]="exists";
            $ret["refId"]=$result->RefID;
            $ret["orderId"]=$_SESSION["pay_orderId"];
            $ret["amount"]=$_SESSION["pay_amount"];
				$ret["description"]=$_SESSION["pay_description"];
				$ret["otherData"]=$_SESSION["pay_otherData"];
            return $ret;
         }
         else
         {
            $ret["status"]="err";
            $ret["refId"]=@$result->RefID;
            $ret["orderId"]=$_SESSION["pay_orderId"];
            $ret["amount"]=$_SESSION["pay_amount"];
				$ret["description"]=$_SESSION["pay_description"];
				$ret["otherData"]=$_SESSION["pay_otherData"];
            return $ret;
         }
      }
      else
      {
			$ret["status"]="canceled";
			$ret["refId"]=@$result->RefID;
			$ret["orderId"]=$_SESSION["pay_orderId"];
			$ret["amount"]=$_SESSION["pay_amount"];
			$ret["description"]=$_SESSION["pay_description"];
			$ret["otherData"]=$_SESSION["pay_otherData"];
         return $ret;
      }
   }//------------------------------------------------------------------------------------
	public function done()
	{
		$status=$_REQUEST['Status'];
		$authority=$_REQUEST['Authority'];
		$verifyRet=@$this->verify($status,$authority);
		return $verifyRet;
	}//------------------------------------------------------------------------------------
}
?>
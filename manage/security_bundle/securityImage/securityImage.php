<?php
   //programmer mgSoft (mohammad gholamy soft)
	@session_start();
   class securityImage
   {
      public $font = "./WebYekan.ttf";
      public $fontSize=18;
      private $bgR=250;
      private $bgG=250;
      private $bgB=250;
      private $colorR=0;
      private $colorG=0;
      private $colorB=0;
      function bgColor($r=250,$g=250,$b=250)
      {
         $this->bgR=$r;
         $this->bgG=$g;
         $this->bgB=$b;
      }
      function color($r=0,$g=0,$b=0)
      {
         $this->colorR=$r;
         $this->colorG=$g;
         $this->colorB=$b;
      }
      //tolid code alef baye farsi
      function generateCode($count=6)
      {
         $chars ="0123456789";
         $code = '';
         $i = 0;
         while ($i < $count)
         {
            $code .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
            $i++;
         }
         return $code;
      }
      //sakht tasvir va chape code rooye an
      function securityImage_create($width='140',$height='50',$count=6,$tokenName='securityImage_code')
      {
         $image = @imagecreate($width, $height) or die('Cannot initialize new GD image stream');
         $code = $this->generateCode($count);
       
         $_SESSION[$tokenName] = $code;
         /* set the colours */
         $background_color = imagecolorallocate($image, $this->bgR, $this->bgG, $this->bgB);
         $text_color = imagecolorallocate($image, $this->colorR, $this->colorG, $this->colorB);
         $noise_color = imagecolorallocate($image, $this->colorR, $this->colorG, $this->colorB);

         /* generate random lines in background */
         for( $i=0; $i<5;$i++ )
            imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
         if(function_exists("imagefilter")) imagefilter($image,IMG_FILTER_GAUSSIAN_BLUR,IMG_FILTER_SMOOTH);
         /* generate random dots in background */
         for( $i=0; $i<20; $i++ )
            imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
         /* create textbox and add text */
         $textbox = imagettfbbox($this->fontSize, -3, $this->font, $code) or die('Error in imagettfbbox function');
         $x = ($width - $textbox[4])/2;
         $y = ($height - $textbox[5])/2;
         imagettftext($image, $this->fontSize, 0, $x, $y, $text_color, $this->font , $code) or die('Error in imagettftext function');
         /* output captcha image to browser */
         @header('Content-Type: image/jpeg');
         imagejpeg($image);
         imagedestroy($image);
      }//---------------------------------------------------------------------------------
      function securityImage_draw($value,$width='140',$height='50',$tokenName='securityImage_value')
      {
         $image = @imagecreate($width, $height) or die('Cannot initialize new GD image stream');
         $code = $value;
         session_start();
         $_SESSION[$tokenName] = $code;
         /* set the colours */
         $background_color = imagecolorallocate($image, $this->bgR, $this->bgG, $this->bgB);
         $text_color = imagecolorallocate($image, $this->colorR, $this->colorG, $this->colorB);
         $noise_color = imagecolorallocate($image, $this->colorR, $this->colorG, $this->colorB);

         /* generate random lines in background */
         for( $i=0; $i<3;$i++ )
            imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
         if(function_exists("imagefilter")) imagefilter($image,IMG_FILTER_GAUSSIAN_BLUR,IMG_FILTER_SMOOTH);
         /* generate random dots in background */
         for( $i=0; $i<10; $i++ )
            imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
         /* create textbox and add text */
         $textbox = imagettfbbox($this->fontSize, -3, $this->font, $code) or die('Error in imagettfbbox function');
         $x = ($width - $textbox[4])/2;
         $y = ($height - $textbox[5])/2;
         imagettftext($image, $this->fontSize, 0, $x, $y, $text_color, $this->font , $code) or die('Error in imagettftext function');
         /* output captcha image to browser */
         header('Content-Type: image/jpeg');
         imagejpeg($image);
         imagedestroy($image);
      }
   }
   //$securityImage = new securityImage(100,30,5) or die('Error in MGS_securityImage1 function');
?>
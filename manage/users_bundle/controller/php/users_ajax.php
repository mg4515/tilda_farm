<?php
   @session_start();
	include_once $_SESSION["engineRequire"];//engine.php
	require_once $oPath->manageDir("jdf.php");
	require_once $oPath->manageDir("users_bundle/model/users_model.php");
	require_once $oPath->manageDir("users_bundle/model/usersMenu_model.php");
	require_once $oPath->manageDir("sms_bundle/kavenegarRest/classSms.php");
	require_once $oPath->manageDir("mail_bundle/model/mail_model.php");
	
	//object
	$oSms=new cSms();
	$oMail=new cMail();
   $oUsers=new cUsers();
	
	//request name
   $request=@$_REQUEST["requestName"];

   if($request=="user_login")
   {
      $userName=cTools::str_perIntToEnInt(cDataBase::escape($_REQUEST["userName"],false));
      $password=cTools::str_perIntToEnInt(cDataBase::escape($_REQUEST["password"],false));
		$backUrl=cDataBase::escape(@$_REQUEST["backUrl"],false);
      $ret=$oUsers->login($userName,$password);
		if($ret!='!userName' && $ret!='!password' && $ret!='!active')
		{
			$user=$oUsers->getByLoginToken($ret);
			//set session
			$_SESSION["user_isLogin"]=true;
			$_SESSION["user_id"]=$user->id;
			$_SESSION["user_userName"]=$user->userName;
			$_SESSION["user_fName"]=$user->fName;
			$_SESSION["user_lName"]=$user->lName;
			//goto userPanel
			$oUsersMenu=new cUsersMenu();
			$page=@$oUsersMenu->getPageIndex();			
			$oEngine->response("ok[|]{$backUrl}[|]{$page->url}[|]{$userName}");
		}
      else
         $oEngine->response("{$ret}[|]{$userName}");			
   }//------------------------------------------------------------------------------------
   else if($request=="user_logout")
   {
      unset($_SESSION["user_isLogin"],$_SESSION["user_id"],$_SESSION["user_userName"]);
		$oEngine->response("ok");
   }//------------------------------------------------------------------------------------
   else if($request=="user_register_sms")
   {
      $userName=$oTools->str_perIntToEnInt($oDbq->escape($data["userName"])); //user mobile
      $password=$oTools->str_perIntToEnInt($oDbq->escape($data["password"]));
      $ret=$oUser->register($userName,$password);
		if($ret > 0)
		{
			$userMobile=$userName;
         $oSms->send($userMobile,"کد فعال سازی : " . $ret);
			$ret="ok";
		}	
		
		$oEngine->response($ret);		
   }//------------------------------------------------------------------------------------
   else if($request=="user_register_mail")
   {
      $userName=cTools::str_perIntToEnInt($oDbq->escape($_REQUEST["userName"])); //user mail
      $password=cTools::str_perIntToEnInt($oDbq->escape($_REQUEST["password"]));
      $fName=cDataBase::escape($_REQUEST["fName"]);
      $ret=$oUsers->register($userName,$password,$fName);
		//echo $ret;
		if($ret > 0)
		{
			$message="<h1>کد فعال سازی : {$ret}</h1>";
         $mRet=$oMail->send($userName,'کد فعال سازی',$message,"gMail");
			$ret="ok[|]{$userName}";
		}	
		
		$oEngine->response($ret);		
   }//------------------------------------------------------------------------------------
   else if($request=="user_active_sms" || $request=="user_active_mail") 
   {
      $userName=cTools::str_perIntToEnInt(cDataBase::escape($_REQUEST["userName"]));
      $code=cTools::str_perIntToEnInt(cDataBase::escape($_REQUEST["code"]));
		
		$ret=$oUsers->activeByCode($userName,$code);
		if($ret)
		{
			$loginToken=$oUsers->login2($userName);
			if($loginToken)
			{
				$user=$oUsers->getByLoginToken($loginToken);
				//set session
				$_SESSION["user_isLogin"]=true;
				$_SESSION["user_id"]=$user->id;
				$_SESSION["user_userName"]=$user->userName;
				//goto userPanel
				$oUsersMenu=new cUsersMenu();
				$page=@$oUsersMenu->getPageIndex();			
            $oEngine->response("ok[|]{$page->url}");
			}
		}
		else
			$oEngine->response("err");	
   }//------------------------------------------------------------------------------------
   else if($request=="user_active_reSms")
   {
      $userMobile=$oTools->str_perIntToEnInt($oDbq->escape($data["userMobile"]));

		$user=$oUser->getByUserName($userMobile);
		if($user)
		{
         $oSms->send($user->userName,"کد فعال سازی : " . $oUser->resetActiveCodeByUserId($user->id));
			$oEngine->response("ok");
		}
		else
			$oEngine->response("err");
   }//------------------------------------------------------------------------------------
   else if($request=="user_active_reMail")
   {
      $userName=cTools::str_perIntToEnInt(cDataBase::escape($_REQUEST["userName"]));
		$user=$oUsers->get($userName,'0');
		if($user)
		{
			$code=$oUsers->updateVerifyCode($userName);
			$message="<h1>کد فعال سازی : {$code}</h1>";
         $mRet=$oMail->send($userName,'کد فعال سازی',$message,"order");
			$oEngine->response("ok");
		}
		else
			$oEngine->response("err");
   }//------------------------------------------------------------------------------------
   else if($request=="user_password_sms")
   {
      $userMobile=$oTools->str_perIntToEnInt($oDbq->escape($data["userMobile"]));

		$user=$oUser->getByUserName($userMobile);
		if($user)
		{
			$passwordNew=$oUsers->passwordChange($user->id);
			$code='رمز عبور جدید شما : ' . $passwordNew;
         $oSms->send($user->userName,$code);
			$oEngine->response("ok");
		}
		else
			$oEngine->response("err");
   }//------------------------------------------------------------------------------------
   else if($request=="user_password_mail")
	{
      $userName=cTools::str_perIntToEnInt(cDataBase::escape($_REQUEST["userName"]));
		$ret=$oUsers->changePassword2($userName);
		if($ret)
		{
			$message="<h1>رمز عبور جدید : {$ret}</h1>";
			file_put_contents("password.txt",$ret);
         $mRet=$oMail->send($userName,'رمز عبور',$message,"order");
			$oEngine->response("ok");
		}
		else
			$oEngine->response("err");
	}//------------------------------------------------------------------------------------		
   else if($request=="user_del")
   {
      $userId=$oDb->escape($data["id"],false);

      //get session for back
      $showActive=$_SESSION["user_showActive"];
      $showSearch=$_SESSION["user_showSearch"];
      $selectPage=$_SESSION["user_selectPage"];

      $ids=$oUser->delete($userId);
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				@unlink ($oPath->manageDir("user_bundle/data/images/user_{$selectedId}.jpg"));
			}
		}		
		$oEngine->response("ok[|]{$showActive}[|]{$showSearch}[|]{$selectPage}");
   }//------------------------------------------------------------------------------------
   else if($request=="user_trash")
   {
      $id=$oDb->escape($data["id"]);
      //get session for back
      $showActive=$_SESSION["user_showActive"];
      $showSearch=$_SESSION["user_showSearch"];
      $selectPage=$_SESSION["user_selectPage"];

      $oUser->trash($id);
		$oEngine->response("ok[|]{$showActive}[|]{$showSearch}[|]{$selectPage}");
   }//------------------------------------------------------------------------------------
   else if($request=="user_unTrash")
   {
      $id=$oDb->escape($data["id"]);
      //get session for back
      $showActive=$_SESSION["user_showActive"];
      $showSearch=$_SESSION["user_showSearch"];
      $selectPage=$_SESSION["user_selectPage"];
      $oUser->unTrash($id);
		$oEngine->response("ok[|]{$showActive}[|]{$showSearch}[|]{$selectPage}");
   }//------------------------------------------------------------------------------------
   else if($request=="user_edit")
   {
      $userId=$oDb->escape($data["id"]);
      $item=$oUser->get($userId);

      $t=time();
      //ADMIN IMAGE
      if(!file_exists($oPath->manageDir("user_bundle/data"))) mkdir($oPath->manageDir("user_bundle/data"));
      if(!file_exists($oPath->manageDir("user_bundle/data/images"))) mkdir($oPath->manageDir("user_bundle/data/images"));

      if(file_exists($oPath->manageDir("user_bundle/data/images/user_{$userId}.jpg")))
         $img="<img id='img_user' src='" . $oPath->manage("user_bundle/data/images/user_{$userId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_userImgDel' class='fa fa-trash fa-2x' onclick='user_imgDel({$userId});' style='cursor:pointer;color:red;'></i>";
      else
         $img="<img id='img_user' src='" . $oPath->asset("default/images/user_larg.png") . "' style='border-radius:8px;width:120px'>";

      $dateOfBirth=@jdate("Y/m/d",$item->dateOfBirth);
      $code= "
		   <div class='vSpace-4x'></div>
			<div id='layer_scrollIntoView' class='title-h1 dir-rtl algn-c'>
				<i class='fa fa-pencil-square-o'></i>&nbsp;ویرایش مشخصات {$item->userName}
				<div class='status' id='layer_msg'></div>
			</div>
         <div class='vSpace-4x'></div>			
			<div class='form'>
				<div class='panel'>
					<div class='panel-body'>
						<label>رمز عبور :</label>
						<input type='text' id='txt_password' value=''>
						
						<label>تکرار رمز عبور :</label>
						<input type='text' id='txt_rePassword' value=''>
					</div>
				</div>
				
				<div class='vSpace' ></div>
				
				<div class='panel'>
					<div class='panel-body'>
						<label>نام :</label>
						<input type='text' id='txt_fname' value='{$item->fname}'>

						<label>نام خانوادگي :</label>
						<input type='text' id='txt_lname' value='{$item->lname}'>

						<label>تاريخ تولد : </label>
						<input type='text' id='txt_dateOfBirth' class='pdate' value='{$dateOfBirth}' >

						<label>تلفن :</label>
						<input type='text' id='txt_phone' value='{$item->phone}'>

						<label>موبايل :</label>
						<input type='text' id='txt_mobile' value='{$item->mobile}'>

						<label>ایمیل :</label>
						<input type='text' id='txt_email' value='{$item->email}'>
			
					   <hr>

						<label>استان :</label>
						<select id='slct_state' class='province'></select>

						<label>شهر :</label>
						<select id='slct_city' class='city'></select>

						<label>کدپستی :</label>
						<input type='number' id='txt_zipCode' value='{$item->zipCode}'>

						<label>آدرس :</label>
						<textarea id='txt_address'>{$item->address1}</textarea>
					</div>
				</div>

				<div class='vSpace'></div>
				
				<div class='panel'>
				   <div class='panel-body'>
						<table class='tbl form'>
							<tr>
								<td style='width:180px;'>
									{$img}
									<br>
									<label class='lbl-file lbl-file-right'>
										<input type='file' id='fileImg' onchange='oTools.previewImgFile(\"fileImg\",\"img_user\");' required/>
										<span class='lbl-file-icon'></span>
										<span>انتخاب تصویر</span>
									</label>
								</td>
								<td></td>
							</tr>
						</table>
					</div>
				</div>
         </div>
         
			<div class='vSpace'></div>		
			
			<button class='btn btn-success' onclick='user_update(\"edit\",{$userId});'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
			<br><br>
      ";
      $code.="
      <script type='text/javascript'>
         var objCalDate = new AMIB.persianCalendar( 'txt_dateOfBirth' );
         persianCity_init('slct_state','slct_city','{$item->state}','{$item->city}');
      </script>";
      $oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($request=="user_new")
   {
      //get session for back
      $showActive=$_SESSION["user_showActive"];
      $showSearch=$_SESSION["user_showSearch"];
      $selectPage=$_SESSION["user_selectPage"];

      //karbar IMAGE
      $img="<img id='img_user' src='" . $oPath->asset("default/images/user_larg.png") . "' style='border-radius:8px;width:120px'>";

      $code= "
         <div class='page-content-titleBar'><i class='fa fa-pencil-square-o'></i>&nbsp;&nbsp;<b>کاربر جدید</b></div><br><br>
         <table class='tbl tbl-right form'>		
            <tr>
               <td style='width:100px;'>نام کاربري :</td>
               <td><input type='text' id='txt_userName' class='form-control'></td>
            </tr>
            <tr>
               <td>رمز عبور :</td>
               <td><input type='text' id='txt_password' value='' class='form-control'></td>
            </tr>
         </table>
         <hr>
         <table class='tbl tbl-right form'>
            <tr>
               <td style='width:100px;'>نام :</td>
               <td><input type='text' id='txt_fname' class='form-control'></td>
            </tr>
            <tr>
               <td>نام خانوادگي :</td>
               <td><input type='text' id='txt_lname' class='form-control'></td>
            </tr>
            <tr>
               <td>تاريخ تولد : </td>
               <td><input type='text' id='txt_dateOfBirth' class='pdate' ></td>
            </tr>
            <tr>
               <td>تلفن :</td>
               <td><input type='text' id='txt_phone'></td>
            </tr>
            <tr>
               <td>موبايل :</td>
               <td><input type='text' id='txt_mobile'></td>
            </tr>
            <tr>
               <td>ایمیل :</td>
               <td><input type='text' id='txt_email'></td>
            </tr>
            <tr><td><br></td></tr>
            <tr>
               <td>استان :</td>
               <td><select id='slct_state' class='province'></select></td>
            </tr>
            <tr>
               <td>شهر :</td>
               <td><select id='slct_city' class='city'></select></td>
            </tr>
            <tr>
               <td>کدپستی :</td>
               <td><input type='number' id='txt_zipCode'></td>
            </tr>
            <tr>
               <td>آدرس :</td>
               <td><textarea id='txt_address'></textarea></td>
            </tr>
         </table>
         <hr>
			<table class='tbl form'>
				<tr>
					<td style='width:180px;'>
						{$img}
						<br>
						<label class='lbl-file lbl-file-right'>
							<input type='file' id='fileImg' onchange='oTools.previewImgFile(\"fileImg\",\"img_user\");' required/>
							<span class='lbl-file-icon'></span>
							<span>انتخاب تصویر</span>
						</label>
					</td>
					<td></td>
				</tr>
			</table>	
         <hr>
         <button class='btn btn-default' onclick='users_draw(\"{$showActive}\",\"{$showSearch}\",{$selectPage});'><i class='fa fa-arrow-right'></i></button>
         <button class='btn btn-success' onclick='user_update(\"new\",0);'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
         <br><br>

      ";
      $code.="
      <script>
         var objCalDate = new AMIB.persianCalendar( 'txt_dateOfBirth' );
         persianCity_init('slct_state','slct_city');
      </script>";
      $oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($request=="user_update")
   {
      $imgDel=$oDb->escape($data["imgDel"]);
      $purpose=$oDb->escape($data["purpose"]);

      if($data["purpose"]=="edit")
      {
         $id=$oDb->escape($data["id"]);
      }
      else
      {
         $id=time();
      }

      $array=array();
      $array["purpose"]=$oDb->escape($data["purpose"]);
      $array["id"]=@$oDb->escape($data["id"]);
      $array["userName"]=$oTools->str_perIntToEnInt(@$oDb->escape($data["userName"]));
      $array["password"]=$oTools->str_perIntToEnInt(@$oDb->escape($data["password"]));

      //profile
      $array["fname"]=$oDb->escape($data["fname"]);
      $array["lname"]=$oDb->escape($data["lname"]);
      $array["phone"]=$oDb->escape($data["phone"]);
      $array["mobile"]=$oDb->escape($data["mobile"]);
      $array["email"]=$oDb->escape($data["email"]);
      $array["dateOfBirth"]=@jalali_to_unix_timestamp($oDb->escape($data["dateOfBirth"])) ? jalali_to_unix_timestamp($oDb->escape($data["dateOfBirth"])):time();
      $array["fatherName"]=@$oDb->escape($data["fatherName"]);
      $array["melliCode"]=@$oDb->escape($data["melliCode"]);
      $array["country"]=@$oDb->escape($data["country"]);
      $array["state"]=@$oDb->escape($data["state"]);
      $array["city"]=@$oDb->escape($data["city"]);
      $array["zipCode"]=$oDb->escape($data["zipCode"]);
      $array["address1"]=$oDb->escape($data["address1"]);
      $array["address2"]=$oDb->escape($data["address2"]);
      $array["bankName"]=@$oDb->escape($data["bankName"]);
      $array["bankHesab"]=@$oDb->escape($data["bankHesab"]);
      $array["bankCart"]=@$oDb->escape($data["bankCart"]);
      $array["comment"]=@$oDb->escape($data["comment"]);

      //image
      if($imgDel==1 || $imgDel=="true") @unlink($oPath->manageDir("user_bundle/data/images/user_{$id}.jpg"));
      if(count($_FILES) > 0)
      {
         if(!$oTools->fs_fileIsImage($_FILES["fileImg"]["name"]))
         {
            echo "errType";
            exit;
         }
         if($oTools->fs_fileSize($_FILES["fileImg"]["tmp_name"]) > 1073741824)
         {
            echo "errSize";
            exit;
         }
         @copy($_FILES["fileImg"]["tmp_name"],$oPath->manageDir("user_bundle/data/images/user_{$id}.jpg"));
         @unlink($_FILES["fileImg"]["tmp_name"]);
      }

      if($purpose=="edit")
         $ret=$oUser->update($array);
      else
         $ret=$oUser->insert($array);

      $oEngine->response("ok");
   }//------------------------------------------------------------------------------------
   else if($request=="user_profileDraw")
   {
      $userId=$oDb->escape($data["id"]);
      $isMe=$oDb->escape($data["isMe"]);	
		
      $user=$oUser->get($userId);
		
		if(file_exists($oPath->manageDir("user_bundle/data/images/user_{$user->id}.jpg")))
			$img=$oPath->manage("user_bundle/data/images/user_{$user->id}.jpg");
		else
			$img=$oPath->asset("default/images/user_larg.png");
		
		if($isMe=="1")
		{
         $code="
			<div class='myModal myModal-autoSize' id='layer_modalUser' >
				<div class='myModal-container' id='layer_modal_container'>
					<div class='myModal-topBar'>
						<i class='fa fa-times' onclick='myModal_hide(\"layer_modalUser\");'></i>
						<span>{$user->userName}</span>
					</div>
					<div class='myModal-content' id='layer_modal_content'>
						<div class='part-w-10 algn-c'>
							<div class='imageCircle' style='background-image:url({$img})'></div>
							<hr>
							<button class='btn btn-image btn-image-right part-w-5 part-w-10-auto  status-info bg brd-0' onclick='post_get(\"user?page=desktop\");'>
								<span>محیط کاربری</span>
								<i class='btn-image-icon fa fa-user'></i>
							</button>
							<div class='vSpace-05x'></div>							
							<button class='btn btn-image btn-image-right part-w-5 part-w-10-auto  status-success bg brd-0' onclick='post_get(\"user?page=userEdit\");'>
								<span>ویرایش مشخصات</span>
								<i class='btn-image-icon fa fa-pencil'></i>
							</button>
							<div class='vSpace-05x'></div>
							<button onclick='user_logout();' class='btn btn-image btn-image-right part-w-5 part-w-10-auto  status-warning bg brd-0'>
								<span>خروج</span>
								<i class='btn-image-icon fa fa-close'></i>
							</button>
							<div class='vSpace-2x'></div>
							<button class='btn btn-image btn-image-right part-w-5 part-w-10-auto status-danger bg'> <!-- status-danger.bg -->
								<span>حذف کاربری</span>
								<i class='btn-image-icon fa fa-user-times'></i>
							</button>
							<div class='vSpace'></div>
						</div>
					</div>
				</div>
			</div>
         <script>myModal_show('layer_modalUser');</script>			
			";			
		}
		else
		{
         $code="
			<div class='myModal myModal-autoSize' id='layer_modalUser' >
				<div class='myModal-container' id='layer_modal_container'>
					<div class='myModal-topBar'>
						<i class='fa fa-times' onclick='myModal_hide(\"layer_modalUser\");'></i>
						<span>{$user->userName}</span>
					</div>
					<div class='myModal-content' id='layer_modal_content'>
						<div class='part-w-10 algn-c'>
							<div class='imageCircle' style='background-image:url({$img})'></div>
							<div class='vSpace'></div>
							<i class='fa fa-user'></i>&nbsp;{$user->fname} {$user->lname}
							<br>
							<i class='fa fa-map-marker'></i>&nbsp;{$user->state} - {$user->city}
							<div class='vSpace'></div>
						</div>
					</div>
				</div>
			</div>
         <script>myModal_show('layer_modalUser');</script>			
			";			
		}
		
		$oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------	
?>
<?php
   @session_start();

   //include
	include_once $_SESSION["engineRequire"];//engine.php
	include_once $oPath->manageDir("users_bundle/model/usersMenu_model.php");
	include_once $oPath->manageDir("users_bundle/model/users_model.php");
	include_once $oPath->manageDir("kifPool_bundle/model/kifPool_model.php");
	include_once $oPath->manageDir("shop_bundle/model/shopItems_model.php");
	include_once $oPath->manageDir("shop_bundle/model/shopFactor_model.php");
	include_once $oPath->manageDir("jdf.php");
	
	//object
	$oUsersMenu=new cUsersMenu();
   $oUsers=new cUsers();
	$oKifPool=new cKifPool();
   $oShopItems=new cShopItems();
   $oShopFactor=new cShopFactor();
	
	//request
	$request=@$_REQUEST['requestName'];

   if($request=="user_edit")
   {
      $userId=cDataBase::escape($_SESSION["user_id"]);
      $item=$oUsers->get($userId);

      $t=time();
      //user image
      if(!file_exists($oPath->manageDir("users_bundle/data"))) mkdir($oPath->manageDir("users_bundle/data"));
      if(!file_exists($oPath->manageDir("users_bundle/data/images"))) mkdir($oPath->manageDir("users_bundle/data/images"));

      if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$userId}.jpg")))
         $img="<img id='img_user' src='" . $oPath->manage("users_bundle/data/images/user_{$userId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_userImgDel' class='fa fa-trash fa-2x' onclick='user_imgDel({$userId});' style='cursor:pointer;color:red;'></i>";
      else
         $img="<img id='img_user' src='" . $oPath->asset("default/images/user_larg.png") . "' style='border-radius:8px;width:120px'>";

      $dateOfBirth=@jdate("Y/m/d",$item->dateOfBirth);
      $code= "
		   <div class='vSpace-4x'></div>
         <h1><i class='fa fa-pencil'></i>&nbsp;مشخصات کاربری</h1>
         <div class='vSpace-4x'></div>
			
			<div class='form'>
				<div class='panel panel-radius'>
				   <div class='panel-body'>
					   <h4><i class='fa fa-key'></i>&nbsp;تغییر رمز عبور</h4>
						<label>رمز عبور</label>
						<input type='text' id='txt_password' value=''>
						<label>تایید رمز عبور</label>
						<input type='text' id='txt_rePassword' value=''>
					</div>
				</div>
				
				<div class='vSpace-2x'></div>
				
				<div class='panel panel-radius'>
				   <div class='panel-body'>
					   <h4>مشخصات فردی</h4>
						<label>نام<span class='fg-danger'>*</span></label>
						<input type='text' id='txt_fName' value='{$item->fName}'>
						
						<label>نام خانوادگی</label>
						<input type='text' id='txt_lName' value='{$item->lName}'>
						
						<div class='hide'><label>تاريخ تولد</label>
						<input type='text' id='txt_dateOfBirth' class='pdate' value='{$dateOfBirth}'/></div>
					</div>
				</div>
				
				<div class='vSpace-2x'></div>
				
				<div class='panel panel-radius'>
				   <div class='panel-body'>
					   <h4>مشخصات تماس</h4>

							<label>تلفن</label>
							<input type='text' class='dir-ltr' id='txt_phone' value='{$item->phone}'>

							<label>موبايل</label>
							<input type='text' class='dir-ltr' id='txt_mobile' value='{$item->mobile}'>
					</div>
				</div>
				
				<div class='vSpace-2x'></div>
				
				<div class='panel panel-radius'>
				   <div class='panel-body'>
					   <h4>مشخصات پستی</h4>
						<label>استان</label>
						<select id='slct_state' class='province'></select>

						<label>شهر</label>
						<select id='slct_city' class='city'></select>

						<label>کدپستی :</label>
						<input type='text' class='dir-ltr' id='txt_zipcode' value='{$item->zipcode}'>

						<label>آدرس :</label>
						<textarea id='txt_address'>{$item->address}</textarea>
					</div>
				</div>
				
				<div class='vSpace-2x'></div>
				
				<div class='panel panel-radius'>
				   <div class='panel-body'>
					   <h4>تصویر کاربری</h4>
						{$img}
						<br>
						<label class='lbl-file lbl-file-right'>
							<input type='file' id='fileImg' onchange='oTools.previewImgFile(\"fileImg\",\"img_user\");' required/>
							<span class='lbl-file-icon'></span>
							<span>انتخاب تصویر</span>
						</label>
					</div>
				</div>
			</div>
			
			<div class='vSpace-4x'></div>				
			<hr>
			<button class='btn btn-success' onclick='user_update(\"edit\",{$userId});'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
			<div class='vSpace-4x'></div>
      ";
      $code.="
      <script type='text/javascript'>
         var objCalDate = new AMIB.persianCalendar('txt_dateOfBirth');
         persianCity_init('slct_state','slct_city','{$item->state}','{$item->city}');
      </script>";
      $oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($request=="user_update")
   {	
      $imgDel=cDataBase::escape($_REQUEST["imgDel"]);

      $array=array();
      $id=cDataBase::escape($_REQUEST["id"]);
      $array["id"]=$id;
      $array["userName"]=@cDataBase::escape($_REQUEST["userName"]);
      $array["password"]=@cDataBase::escape($_REQUEST["password"]);

      //profile
      $array["fName"]=cDataBase::escape($_REQUEST["fName"]);
      $array["lName"]=cDataBase::escape($_REQUEST["lName"]);
      $array["phone"]=cDataBase::escape($_REQUEST["phone"]);
      $array["mobile"]=cDataBase::escape($_REQUEST["mobile"]);
      //$array["mail"]=cDataBase::escape($_REQUEST["mail"]);
      $array["dateOfBirth"]=@jalali_to_unix_timestamp(cDataBase::escape($_REQUEST["dateOfBirth"])) ? jalali_to_unix_timestamp(cDataBase::escape($_REQUEST["dateOfBirth"])):time();
      $array["country"]=@cDataBase::escape($_REQUEST["country"]) ?? 'iran';
      $array["state"]=@cDataBase::escape($_REQUEST["state"]);
      $array["city"]=@cDataBase::escape($_REQUEST["city"]);
      $array["zipcode"]=cDataBase::escape($_REQUEST["zipcode"]);
      $array["address"]=cDataBase::escape($_REQUEST["address"]);

      //image
		$userImage='';
      if($imgDel==1 || $imgDel=="true") @unlink($oPath->manageDir("users_bundle/data/images/user_{$id}.jpg"));
      if(count($_FILES) > 0)
      {
         if(!$oTools->fs_fileIsImage($_FILES["fileImg"]["name"]))
         {
            echo "errType";
            exit;
         }
         if($oTools->fs_fileSize($_FILES["fileImg"]["tmp_name"]) > 1073741824)
         {
            echo "errSize";
            exit;
         }
         @copy($_FILES["fileImg"]["tmp_name"],$oPath->manageDir("users_bundle/data/images/user_{$id}.jpg"));
         @unlink($_FILES["fileImg"]["tmp_name"]);
			$userImage=$oPath->manage("users_bundle/data/images/user_{$id}.jpg");
      }

      $ret=$oUsers->update($array);
      $oEngine->response("ok[|]{$id}[|]{$userImage}");
   }//------------------------------------------------------------------------------------
   if($request=="user_bankEdit")
   {
      $userId=cDataBase::escape($_SESSION["user_id"]);
      $item=$oUsers->get($userId);
		
		$bankCardNumber=@$item->bankCardNumber ? $item->bankCardNumber : 0;
		$bankShaba=@$item->bankShaba ? $item->bankShaba : 0;
		
      $code= "
		   <div class='vSpace-4x'></div>
         <h1><i class='fa fa-pencil'></i>&nbsp;مشخصات حساب بانکی</h1>
         <div class='vSpace-4x'></div>
			
			<div class='panel panel-radius'>
				<div class='panel-body form'>
					<label>شماره کارت بانکی</label>
					<input type='text' id='txt_bankCardNumber' value='{$bankCardNumber}' class='dir-ltr'>
					
					<label>شماره شبا</label>
					<div class='input-control'>
					   <span class='input-control-icon'>IR</span>
					   <input type='text' id='txt_bankShaba' value='{$bankShaba}'>
					</div>
					
					<hr>
					<button class='btn btn-success' onclick='user_bankUpdate();'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
				</div>
			</div>
			<div class='vSpace-4x'></div>
      ";

      $oEngine->response("ok[|]{$code}");
		exit;
   }//------------------------------------------------------------------------------------
   else if($request=="user_bankUpdate")
   {	
      $array=array();
      $id=cDataBase::escape($_SESSION["user_id"]);
      $array["id"]=$id;
      $array["bankCardNumber"]=isset($_REQUEST["bankCardNumber"]) ? cDataBase::escape($_REQUEST["bankCardNumber"]) : '0';
      $array["bankShaba"]=isset($_REQUEST["bankShaba"]) ? cDataBase::escape($_REQUEST["bankShaba"]) : '0';

      $ret=$oUsers->bankUpdate($array);
      $oEngine->response("ok");
   }//------------------------------------------------------------------------------------	
   else if($request=="user_logout")
   {
      unset($_SESSION["user_isLogin"],$_SESSION["user_id"],$_SESSION["user_userName"]);
		$oEngine->response("ok");
   }//------------------------------------------------------------------------------------	
   else if($request=="user_profileDraw")
   {
      $userId=cDataBase::ecape($_REQUEST["id"]);
      //get session for back
      $_SESSION["user_showActive"]="";
      $_SESSION["user_showSearch"]="";
      $_SESSION["user_selectPage"]=1;		
		
      $user=$oUsers->get($userId);
		if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$user->userId}.jpg")))
			$img=$oPath->manage("users_bundle/data/images/user_{$user->userId}.jpg");
		else
			$img=$oPath->asset("default/images/user_larg.png");
		
		$oEngine->response("ok[|]{$user->userId}[|]{$user->userName}[|]{$user->fName} {$user->lName}[|]{$img}");
   }//------------------------------------------------------------------------------------	
?>
<?php
   /*header("Content-Type: application/json; charset=UTF-8");*/
	error_reporting(E_ALL);
   @session_start();

   //include
	include_once $_SESSION["engineRequire"];//engine.php
	include_once $oPath->manageDir("admin_bundle/model/adminMenu_model.php");
	include_once $oPath->manageDir("users_bundle/model/users_model.php");
	include_once $oPath->manageDir("kifPool_bundle/model/kifPool_model.php");
	include_once $oPath->manageDir("shop_bundle/model/shopItems_model.php");
	include_once $oPath->manageDir("shop_bundle/model/shopFactor_model.php");
	include_once $oPath->manageDir("jdf.php");
	
	//object
	$oAdminMenu=new cAdminMenu();
   $oUsers=new cUsers();
	$oKifPool=new cKifPool();
   $oShopItems=new cShopItems();
   $oShopFactor=new cShopFactor();
	
	//access
	$accessIdName=['insert'=>'mnu_usersAccessInsert','edit'=>'mnu_usersAccessEdit','trash'=>'mnu_usersAccessTrash','delete'=>'mnu_usersAccessDel','active'=>'mnu_usersAccessActive'];
	
	//request
	$requestName=@$_REQUEST['requestName'];

   if($requestName=="users_draw")
   {
      $active=cDataBase::escape(@$_REQUEST["active"]);
      $trash=isset($_REQUEST["trash"]) ? cDataBase::escape(@$_REQUEST["trash"]) : 0;
      $searchWord=cDataBase::escape($_REQUEST["searchWord"]);
      $page=isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;
		
		//init session
		if($active==-1) 
			$active=$_SESSION["users_active"]; //auto
		else
		   $_SESSION["users_active"]=$active;	
		if($trash==-1) 
			$trash=$_SESSION["users_trash"]; //auto
		else
		   $_SESSION["users_trash"]=$trash;			
		if($searchWord==-1)
			$searchWord=$_SESSION["users_searchWord"];
		else
			$_SESSION["users_searchWord"]=$searchWord;
		if($page==-1) 
			$page=$_SESSION["users_page"]; //auto
		else
			$_SESSION["users_page"]=$page;
      
		//START PAGE ...................................
		$pages_html='';
      $items=$oUsers->getAll($active,$trash,$searchWord);
		$count=count($items);
		$pages=cTools::misc_sqlLimit($count,10,10,$page);
		if($pages['pagesEnd'] > 1)
		{
			for($i=$pages['pagesStart'];$i <= $pages['pagesEnd'];$i++)
			{
				if($i==$page)
					$pages_html.="<button class='btn btn-fix-36'>{$i}</button>";
				else
					$pages_html.="<button class='btn btn-info btn-fix-36' onclick='users_draw(-1,-1,-1,{$i})'>{$i}</button>";
			}		
			$back=$page - 1;
			$next=$page + 1; 
			if($pages['isPrevious']) $pages_html="<button class='btn btn-primary btn-fix-36' onclick='users_draw(-1,-1,-1,{$back});'><i class='fa fa-angle-right'></i></button>" . $btnPageCode;  
			if($pages['isNext']) $pages_html.="<button class='btn btn-primary btn-fix-36' onclick='users_draw(-1,-1,-1,{$next});'><i class='fa fa-angle-left'></i></button>"; 		
		}
		$items=$oUsers->getAll($active,$trash,$searchWord,$pages['sqlLimit']);
		//END PAGE ...................................

      $code="";
      $tr="";
      $i=0;
      foreach($items as $item)
      {
         $i++;
         $item->id=$item->userId;
         
         if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$item->id}.jpg")))
            $img="<div class='chatOnline img' style='background-image:url(" . $oPath->manage("users_bundle/data/images/user_{$item->id}.jpg") . ");'></div>";
         else
            $img="<div class='chatOnline img' style='background-image:url(" . $oPath->asset("default/images/user_larg.png") . ");border-radius:0%' ></div>";
         
			//new
			$btnNew='';
			if($oAdminMenu->checkAllowByIdName($accessIdName['insert'],$_SESSION['admin_id']))
			{
				$btnNew="<button type='button' class='btn btn-success' onclick='user_new();'><i class='fa fa-plus'></i>&nbsp;افزودن</button>";
			}	
			
			//edit
			$btnEdit='';
			if($oAdminMenu->checkAllowByIdName($accessIdName['edit'],$_SESSION['admin_id']))
			{
				$btnEdit="<button class='btn btn-info' onclick='user_edit({$item->id});'><i class='fa fa-pencil'></i></button>";
			}		
			
			//delete or trash 
			$btnDeleteTrash='';
			$btnDeleteTrashAll='';
			if($item->trash == '0' && $oAdminMenu->checkAllowByIdName($accessIdName['trash'],$_SESSION['admin_id']))
			{
				$btnDeleteTrash="<button class='btn btn-danger' onclick='user_trash({$item->id});'><i class='fa fa-trash-o'></i></button>";
				$btnDeleteTrashAll="<button class='btn btn-danger' id='btn_trash' onclick='user_trash(\"selected\");' disabled><i class='fa fa-trash-o'></i></button>";
			}
			else if($item->trash == '1' && $oAdminMenu->checkAllowByIdName($accessIdName['delete'],$_SESSION['admin_id']))
			{
				$btnDeleteTrash="<button class='btn btn-danger' onclick='user_delete({$item->id});'><i class='fa fa-remove'></i></button>";
				$btnDeleteTrashAll="<button class='btn btn-danger' id='btn_trash' onclick='user_delete(\"selected\");' disabled><i class='fa fa-remove'></i></button>";
			}
			
			//active
			$btnActive='';
			if($oAdminMenu->checkAllowByIdName($accessIdName['active'],$_SESSION['admin_id']))
			{
				if($item->active=="1") $btnActive="<button class='btn btn-success' onclick='user_active({$item->id});'><i class='fa fa-undo'></i>&nbsp;فعال</button>";
				else $btnActive="<button class='btn btn-danger' onclick='user_active({$item->id});'><i class='fa fa-undo'></i>&nbsp;غير فعال</button>";
				$btnActive="<span id='td_active_{$item->id}'>{$btnActive}</span>";
			}
	
			if($btnDeleteTrash || $btnEdit || $btnActive) $layerEdit=''; else $layerEdit='hide';
			
			//shopItms, list mahsoolat
			$btnShopItems='';
			if(isset($oShopItems))
			{
				$shopItems=$oShopItems->getAll(['userId'=>$item->id]);
				$count=count($shopItems);
				$disabled= @$count ? $count : "disabled";
				$btnShopItems="
				<button class='btn btn-label btn-block btn-default' onclick='shopItems_draw({\"userId\":\"{$item->id}\", \"page\":1, \"backName\":\"users_draw\"});' {$disabled}>
					<span class='btn-label-caption' style='width:40px'>{$count}</span>
					<span>محصولات</span>
				</button>				
				";
			}

			//shopFactor Seller
			$btnShopSeller='';
			if(isset($oShopFactor))
			{
				$shopFactor=$oShopFactor->factor_getAll(['userSellerId'=>$item->id, 'temp'=>0, 'payStatus'=>1]);
				$count=count($shopFactor);
				$disabled= @$count ? $count : "disabled";
				$btnShopSeller="
				<button class='btn btn-label btn-block btn-default' onclick='shopFactor_draw({\"userSellerId\":{$item->id}, \"backName\":\"users_draw\"});' {$disabled}>
					<span class='btn-label-caption' style='width:40px'>{$count}</span>
					<span>فروش ها</span>
				</button>				
				";
			}
			
			//shopFactor Marketer
			$btnShopMarketer='';
			if(isset($oShopFactor))
			{
				$shopFactor=$oShopFactor->factor_getAll(['userSellerId'=>$item->id, 'temp'=>0, 'payStatus'=>1]);
				$count=count($shopFactor);
				$disabled= @$count ? $count : "disabled";
				$btnShopMarketer="
				<button class='btn btn-label btn-block btn-default' onclick='shopFactor_draw({\"userSellerId\":{$item->id}, \"backName\":\"users_draw\"});' {$disabled}>
					<span class='btn-label-caption' style='width:40px'>{$count}</span>
					<span>فروش های بازاریابی</span>
				</button>				
				";
			}
			
			//seller income
			$lblSellerIncome='';
			if(isset($oShopFactor))
			{
				$sellerIncome=$oShopFactor->factor_getSellerIncome($item->id);
				$sellerIncome=number_format($sellerIncome,0,',','.');
				$lblSellerIncome="<label class='lbl lbl-block fg-success'>درآمد : {$sellerIncome} تومان</label>";
			}			
			
			//Marketer income
			$lblMarketerIncome='';
			if(isset($oShopFactor))
			{
				$marketerIncome=$oShopFactor->factor_getMarketerIncome($item->id);
				$marketerIncome=number_format($marketerIncome,0,',','.');
				$lblMarketerIncome="<label class='lbl lbl-block fg-success'>درآمد : {$marketerIncome} تومان</label>";
			}
			
			if($btnShopItems || $btnShopMarketer || $btnShopSeller || $lblSellerIncome || $lblMarketerIncome) $layerShop=''; else $layerShop='hide';			
			
			//kifPool
			$btnkifPool='';
			$layerKifPool='hide';
			if(isset($oKifPool))
			{
			   $kifPoolAmount=number_format($oKifPool->kifPool_getAmount($item->id),0,'.',',') . " تومان";
				$btnkifPool="
				<hr>
				<button class='btn btn-small' onclick='kifPool_draw({$item->id});'><i class='fa fa-pencil'></i></button><span class='lbl lbl-success' onclick='kifPool_draw({$item->id});'>موجودی : {$kifPoolAmount}</span>
			   <!-- <button class='btn btn-primary btn-block' onclick='kifPoolTransaction_draw({$item->id});'>تراکنش های کیف پول</button> -->
				";
				$layerKifPool='';
			}
			
         $tr.="
         <tr>
            <td style='text-align:center;width: 10px;'><input type='checkbox' id='chk_{$i}' value='{$item->id}' onclick='checkedOne();' /></td>
            <td style='text-align:center;width: 200px;padding:5px;'>
				   {$img}
					<br>
					{$item->userName}
					<br>
					{$item->fName}&nbsp;{$item->lName}
					{$btnkifPool}
				</td>
				<td style='max-width:200px;width:200px;padding:10px;' class='{$layerShop}'>
					{$btnShopMarketer}
					{$lblMarketerIncome}
				</td>				
				<td style='max-width:160px;width:160px;padding:10px;' class='{$layerShop}'>
				   {$btnShopItems}
					{$btnShopSeller}
					{$lblSellerIncome}
				</td>				
				<td class='{$layerEdit}'>
					{$btnActive}
					{$btnDeleteTrash}
					{$btnEdit}
				</td>				
         </tr>";
      }
      $chkCount=$i;
      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-list'></i>&nbsp;لیست بازاریابان</h1>
		<div class='vSpace-4x'></div>
		
		{$btnNew}
		{$btnDeleteTrashAll}
		&nbsp;|&nbsp;
		<button class='btn' onclick='users_search();'><i class='fa fa-search'></i></button>
		<input type='text' id='txt_search' value='{$searchWord}' placeholder='جستجو'/><br>	
		
		{$pages_html}
		<br><br>
		<div class='hScrollBar-auto'>
			<table class='tbl tbl-bordered tbl-hover tbl-right'>
				<tr>
					<th class='text-right'><input type='checkbox' id='chk_all' value='{$chkCount}' onclick='checkedAll();' /></th>
					<th class='text-right'>بازاریاب</th>
					<th class='text-right {$layerShop}'>بازاریابی</th>
					<th class='text-right {$layerShop}'>محصولات</th>					
					<th class='text-right {$layerEdit}'></th>
				</tr>
				{$tr}
			</table>
		</div>
		<br>
		{$pages_html}
		
		<div class='vSpace-4x'></div>
      ";
      $countNewuser=count($oUsers->getAll());
		$oEngine->response("ok[|]{$code}[|]{$countNewuser}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="users_listDraw")
   {
      $searchWord=@htmlspecialchars($oDb->escape($_REQUEST["searchWord"]));
      $selectedIds=@htmlspecialchars($oDb->escape($_REQUEST["selectedIds"])); //1,1000,26454,....
		$multiSelect=@htmlspecialchars($oDb->escape($_REQUEST["multiSelect"])); //0 or 1
		$page=@$_REQUEST["page"] ? htmlspecialchars($oDb->escape($_REQUEST["page"])) : 1; 
      
		//session auto
		if($searchWord==-1) $searchWord=$_SESSION["user_listDraw_searchWord"];
		if($selectedIds==-1) $selectedIds=$_SESSION["user_listDraw_selectedIds"];
		if($multiSelect==-1) $multiSelect=$_SESSION["user_listDraw_multiSelect"];
		if($page==-1) $page=$_SESSION["user_listDraw_page"];
		
		//init session
		$_SESSION["user_listDraw_searchWord"]=$searchWord;
		$_SESSION["user_listDraw_selectedIds"]=$selectedIds;
		$_SESSION["user_listDraw_multiSelect"]=$multiSelect;
		$_SESSION["user_listDraw_page"]=$page;
		
		//START PAGE ...................................	
      $items=$oUsers->getAll('1','0',$searchWord);
      $count=count($items);		
      $pages=cTools::misc_sqlLimit($count,10,10,$page);
      $btnPageCode="";
      for($i=$pages["pagesStart"];$i <= $pages["pagesEnd"];$i++)
      {
         if($i==$page) //agar dokmey ke dary chap mikony, page an baz bashad, ya karbar rooye an click karde ast
            $btnPageCode.="<a class='btn btn-fix-36 btn-info'>{$i}</a>";
         else
            $btnPageCode.="<a class='btn btn-fix-36 btn-default' onclick='users_listDraw(\"{$searchWord}\",{$i});'>{$i}</a>";
      }
      $back=$page - 1;
      $next=$page + 1;
      if($pages["isPrevious"]) $btnPageCode="<a class='btn btn-fix-36 btn-primary' onclick='users_listDraw(\"{$searchWord}\",{$back});'><i class='fa fa-angle-right'></i></a>" . $btnPageCode;
      if($pages["isNext"]) $btnPageCode=$btnPageCode . "<a class='btn btn-fix-36 btn-primary' onclick='users_listDraw(\"{$searchWord}\",{$next});'><i class='fa fa-angle-left'></i></a>";      
		$items=$oUsers->getAll('1','0',$searchWord,$pages['sqlLimit']);
		//END PAGE ...................................
		
		$codeSelected="";
      $code="";
      $tr="";
      $i=0;
		$usersData_js="var usersData=[];";
		$selectedIds=explode(",",$selectedIds);
      foreach($items as $item)
      {
         $i++;
         $id=$item->id;
         if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$item->id}.jpg")))
            $img=$oPath->manage("users_bundle/data/images/user_{$item->id}.jpg");
         else
            $img=$oPath->asset("default/images/user_larg.png");
			
			if(in_array($id,$selectedIds))
			{
			   $selected=true;
            $codeSelected="
				<li class='myStyle-item-selected' title='{$item->fName} {$item->lName} - {$item->userName}' dataValue1='{$item->id}' dataValue2='{$item->userName}' dataValue3='{$item->fName}' dataValue4='{$item->lName}'>
				   <span class='myList-item-icon' style='background-image:url({$img});'></span>
					<span class='myList-item-title'>{$item->fName} {$item->lName} - {$item->userName}</span>
				</li>				
				";				
			}
			else
			{
			   $selected=false;
				$codeSelected="";
				$code.="
				<li title='{$item->fName} {$item->lName} - {$item->userName}' dataValue1='{$item->id}' dataValue2='{$item->userName}' dataValue3='{$item->fName}' dataValue4='{$item->lName}'>
				   <span class='myList-item-counter hide-i'></span>
				   <span class='myList-item-icon' style='background-image:url({$img});'></span>
					<span class='myList-item-title'>{$item->fName} {$item->lName} - {$item->userName}</span>
				</li>
				";
			}
      }
		$codeHeader="
		<div style='position:absolute;left:0px;right:0px;top:0px;height:32px;'>
		   <button class='btn btn-default' onclick='users_listDrawBySearch();' style='height:32px;line-height:16px;'><i class='fa fa-search' style='font-size:12px;'></i></button>
		   <input type='text' id='txt_userssearch' value='{$searchWord}' style='height:32px;' placeholder='جستجو'/><br>			
		</div>
		";
		$codeMain="
		<ul class='myList myList-right' id='ul_usersList'>
			{$codeSelected}
			{$code}
		</ul>
		";
		$oEngine->response("ok[|]{$codeMain}[|]{$searchWord}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="user_delete")
   {
      $userId=$oDb->escape($_REQUEST["id"],false);
      $ids=$oUsers->delete($userId);
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				@unlink ($oPath->manageDir("users_bundle/data/images/user_{$selectedId}.jpg"));
			}
		}		
		$oEngine->response("ok");
   }//------------------------------------------------------------------------------------
   else if($requestName=="user_trash")
   {
      $id=$oDb->escape($_REQUEST["id"]);
      $oUsers->trash($id,$_SESSION['admin_id']);
		$oEngine->response("ok");
   }//------------------------------------------------------------------------------------
   else if($requestName=="user_unTrash")
   {
      $id=$oDb->escape($_REQUEST["id"]);
      $oUsers->unTrash($id);
		$oEngine->response("ok");
   }//------------------------------------------------------------------------------------
   else if($requestName=="user_active")
   {
      $userId=$oDb->escape($_REQUEST["id"]);
      $ret=$oUsers->getActive($userId);
		if($ret==true)
		{
		   $oUsers->setActive($userId,"0");	
			$code="<button class='btn btn-danger' onclick='user_active({$userId});'><i class='fa fa-undo'></i>&nbsp;غير فعال</button>";
		}
		else
		{
			$oUsers->setActive($userId,"1");	
			$code="<button class='btn btn-success' onclick='user_active({$userId});'><i class='fa fa-undo'></i>&nbsp;فعال</button>";
		}
      $oEngine->response("ok[|]{$userId}[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="user_edit")
   {
      $userId=cDataBase::escape($_REQUEST["id"]);
      $item=$oUsers->get($userId);

      $t=time();
      //ADMIN IMAGE
      if(!file_exists($oPath->manageDir("users_bundle/data"))) mkdir($oPath->manageDir("users_bundle/data"));
      if(!file_exists($oPath->manageDir("users_bundle/data/images"))) mkdir($oPath->manageDir("users_bundle/data/images"));

      if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$userId}.jpg")))
         $img="<img id='img_user' src='" . $oPath->manage("users_bundle/data/images/user_{$userId}.jpg") . "?t={$t}' style='border-radius:8px;width:120px'>&nbsp;<i id='i_userImgDel' class='fa fa-trash fa-2x' onclick='user_imgDel({$userId});' style='cursor:pointer;color:red;'></i>";
      else
         $img="<img id='img_user' src='" . $oPath->asset("default/images/user_larg.png") . "' style='border-radius:8px;width:120px'>";

      $dateOfBirth=@jdate("Y/m/d",$item->dateOfBirth);
      $code= "
		   <div class='vSpace-4x'></div>
         <h1><i class='fa fa-pencil'></i>&nbsp;ویرایش بازاریاب&nbsp;{$item->userName}</h1>
         <div class='vSpace-4x'></div>
			
			<div class='panel'>
			   <div class='panel-body form'>
					<label>نام کاربري</label>
					<input type='text' id='txt_userName' value='{$item->userName}'>

					<label>رمز عبور</label>
					<input type='text' id='txt_password' value=''>
					
					<hr>

					<label>نام</label>
					<input type='text' id='txt_fName' value='{$item->fName}'>
				
					<label>نام خانوادگي :</label>
					<input type='text' id='txt_lName' value='{$item->lName}'>

					<div class='hide'>
						<label>تاريخ تولد</label>
						<input type='text' id='txt_dateOfBirth' class='pdate' value='{$dateOfBirth}'>
					</div>

					<label>تلفن</label>
					<input type='text' class='dir-ltr' id='txt_phone' value='{$item->phone}'>

					<label>موبايل</label>
					<input type='text' class='dir-ltr' id='txt_mobile' value='{$item->mobile}'>

					<label>ایمیل</label>
					<input type='text' class='dir-ltr' id='txt_mail' value='{$item->mail}'>
			
					<hr>
					
					<label>استان</label>
					<select id='slct_state' class='province'></select>

					<label>شهر</label>
					<select id='slct_city' class='city'></select>

					<label>کدپستی</label>
					<input type='text' class='dir-ltr' id='txt_zipcode' value='{$item->zipcode}'>

					<label>آدرس</label>
					<textarea id='txt_address' style='height:100px;'>{$item->address}</textarea>

					<hr>
					
					{$img}
					<br>
					<label class='lbl-file lbl-file-right'>
						<input type='file' id='fileImg' onchange='oTools.previewImgFile(\"fileImg\",\"img_user\");' required/>
						<span class='lbl-file-icon'></span>
						<span>انتخاب تصویر</span>
					</label>

					<hr>
					<button class='btn btn-default' onclick='users_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
					<button class='btn btn-success' onclick='user_update(\"edit\",{$userId});'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
					
				</div>
			</div>
			
			<div class='vSpace-4x'></div>
      ";
      $code.="
      <script type='text/javascript'>
         var objCalDate = new AMIB.persianCalendar( 'txt_dateOfBirth' );
         persianCity_init('slct_state','slct_city','{$item->state}','{$item->city}');
      </script>";
      $oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="user_new")
   {
      //karbar IMAGE
      $img="<img id='img_user' src='" . $oPath->asset("default/images/user_larg.png") . "' style='border-radius:8px;width:120px'>";

      $code= "
		<div class='vSpace-4x'></div>
		<h1><i class='fa fa-plus'></i>&nbsp;بازاریاب جدید</h1>
		<div class='vSpace-4x'></div>         
		
		<div class='panel'>
		   <div class='panel-body form'>
				<label>نام کاربري</label>
				<input type='text' id='txt_userName'>

				<label>رمز عبور</label>
				<input type='text' id='txt_password' value=''>

				<hr>

				<label>نام</label>
				<td><input type='text' id='txt_fName' class='form-control'></td>

				<label>نام خانوادگي</label>
				<input type='text' id='txt_lName' class='form-control'>

				<div class='hide'>
					<label>تاريخ تولد</label>
					<input type='text' id='txt_dateOfBirth' class='pdate' >
				</div>

				<label>تلفن</label>
				<input type='text' class='dir-ltr' id='txt_phone'>


				<label>موبايل</label>
				<input type='text' class='dir-ltr' id='txt_mobile'>

				<label>ایمیل</label>
				<input type='text' class='dir-ltr' id='txt_mail'>

				<hr>
					
				<label>استان</label>
				<select id='slct_state' class='province'></select>

				<label>شهر</label>
				<select id='slct_city' class='city'></select>

				<label>کدپستی </label>
				<input type='text' class='dir-ltr' id='txt_zipcode'>

				<label>آدرس</label>
				<textarea id='txt_address' style='height:100px;'></textarea>

				<hr>
				
				{$img}
				<br>
				<label class='lbl-file lbl-file-right'>
					<input type='file' id='fileImg' onchange='oTools.previewImgFile(\"fileImg\",\"img_user\");' required/>
					<span class='lbl-file-icon'></span>
					<span>انتخاب تصویر</span>
				</label>

				<hr>
				<button class='btn btn-default' onclick='users_draw(\"auto\");'><i class='fa fa-arrow-right'></i></button>
				<button class='btn btn-success' onclick='user_update(\"new\",0);'><i class='fa fa fa-floppy-o'></i>&nbsp;ذخيره</button>
				
		   </div>
		</div>
		
		<div class='vSpace-4x'></div>
      ";
      $code.="
      <script>
         var objCalDate = new AMIB.persianCalendar( 'txt_dateOfBirth' );
         persianCity_init('slct_state','slct_city');
      </script>";
      $oEngine->response("ok[|]{$code}");
   }//------------------------------------------------------------------------------------
   else if($requestName=="user_update")
   {	
      $imgDel=$oDb->escape($_REQUEST["imgDel"]);
      $purpose=$oDb->escape($_REQUEST["purpose"]);

      if($_REQUEST["purpose"]=="edit")
      {
         $id=$oDb->escape($_REQUEST["id"]);
      }
      else
      {
         $id=time();
      }

      $array=array();
      $array["purpose"]=$oDb->escape($_REQUEST["purpose"]);
      $array["id"]=@$oDb->escape($_REQUEST["id"]);
      $array["userName"]=@$oDb->escape($_REQUEST["userName"]);
      $array["password"]=@$oDb->escape($_REQUEST["password"]);

      //profile
      $array["fName"]=$oDb->escape($_REQUEST["fName"]);
      $array["lName"]=$oDb->escape($_REQUEST["lName"]);
      $array["phone"]=$oDb->escape($_REQUEST["phone"]);
      $array["mobile"]=$oDb->escape($_REQUEST["mobile"]);
      $array["mail"]=$oDb->escape($_REQUEST["mail"]);
      $array["dateOfBirth"]=@jalali_to_unix_timestamp($oDb->escape($_REQUEST["dateOfBirth"])) ? jalali_to_unix_timestamp($oDb->escape($_REQUEST["dateOfBirth"])):time();
      $array["country"]=isset($_REQUEST["country"]) ? $oDb->escape($_REQUEST["country"]) : 'iran';
      $array["state"]=@$oDb->escape($_REQUEST["state"]);
      $array["city"]=@$oDb->escape($_REQUEST["city"]);
      $array["zipcode"]=$oDb->escape($_REQUEST["zipcode"]);
      $array["address"]=$oDb->escape($_REQUEST["address"]);

      //image
      if($imgDel==1 || $imgDel=="true") @unlink($oPath->manageDir("users_bundle/data/images/user_{$id}.jpg"));
      if(count($_FILES) > 0)
      {
         if(!$oTools->fs_fileIsImage($_FILES["fileImg"]["name"]))
         {
            echo "errType";
            exit;
         }
         if($oTools->fs_fileSize($_FILES["fileImg"]["tmp_name"]) > 1073741824)
         {
            echo "errSize";
            exit;
         }
         @copy($_FILES["fileImg"]["tmp_name"],$oPath->manageDir("users_bundle/data/images/user_{$id}.jpg"));
         @unlink($_FILES["fileImg"]["tmp_name"]);
      }

      if($purpose=="edit")
         $ret=$oUsers->update($array);
      else
         $ret=$oUsers->insert($array);

      $oEngine->response("ok");
   }//------------------------------------------------------------------------------------
   else if($requestName=="user_profileDraw")
   {
      $userId=$oDb->escape($_REQUEST["id"]);
      //get session for back
      $_SESSION["user_showActive"]="";
      $_SESSION["user_showSearch"]="";
      $_SESSION["user_selectPage"]=1;		
		
      $user=$oUsers->get($userId);
		if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$user->userId}.jpg")))
			$img=$oPath->manage("users_bundle/data/images/user_{$user->userId}.jpg");
		else
			$img=$oPath->asset("default/images/user_larg.png");
		
		$oEngine->response("ok[|]{$user->userId}[|]{$user->userName}[|]{$user->fName} {$user->lName}[|]{$img}");
   }//------------------------------------------------------------------------------------	
?>
<?php
	//include
	include_once $_SESSION["engineRequire"];
	include_once $oPath->manageDir("users_bundle/model/users_model.php");
	include_once $oPath->manageDir("users_bundle/model/usersMenu_model.php");
	
	class cUsersTools
	{
		function checkLogin($return=false,$redirect="")
		{
			if($return==true)
			{
				if(@$_SESSION['users_isLogin'])
					return true;	
				else
					return false;
			}
			else
			{
				if(@$_SESSION['users_isLogin'])
				{
					header("Location:{$redirect}");
					exit;			
				}			
			}
		}//--------------------------------------------------------------------------
		function menuListDraw($parentId=0,$pageActive="")  //list hame menuha be soorate derakhty
		{
			$oUsersMenu=new cUsersMenu();		
			$items=$oUsersMenu->getByParentId($parentId,1);
			$code="";
			if(count($items) > 0) 
			{
				$code.= "<ul class='mtree dark' >";
				foreach($items as $item)
				{
					$idCounter="spn_{$item->idName}";
					if($item->url=="" && $item->jsEvent=="")
					{ 
						$code.="<li id='li_{$item->id}' class='nav-second-level'>
								  <a href='#'>
									  <i class='mtree-icon {$item->fontIcon_class}'></i>
									  {$item->text}
									  <span class='mtree-counter' id='{$idCounter}'></span>
								  </a>";
							 $code.=$this->menuListDraw($item->id,$pageActive);
						$code.="</li>";
					}
					else
					{
						if($item->url == $pageActive) $activeClass='active'; else $activeClass='';
						if($item->url) 
							$event="oTools.link(\"?page={$item->url}\");";
						else if($item->jsEvent)
						{
							$event=$item->jsEvent;
							$event=str_replace("'",'"',$event);
						}
						$code.="
						<li id='li_{$pageActive}'>
							<a href='javascript:void(0)' onclick='{$event}' id='{$item->url}' class='{$activeClass}' >
								<i class='mtree-icon {$item->fontIcon_class}' ></i>
								{$item->text}
								<span class='mtree-counter' id='{$idCounter}'></span>
							</a>                    
						</li>
						";
						if($item->url == $pageActive) 
						{
							$oldParentId=@$oUsersMenu->get($item->parentId)->parentId;
							cEngine::echoAfter("
							<script>
								try{
									$('#li_{$oldParentId}').removeClass('mtree-closed');
									$('#li_{$oldParentId}').addClass('mtree-open');
									$('#li_{$oldParentId} ul:first').css({'display':'block','height':'auto'});
								}catch(e){}	
								
								$('#li_{$item->parentId}').removeClass('mtree-closed');
								$('#li_{$item->parentId}').addClass('mtree-open');
								$('#li_{$item->parentId} ul:first').css({'display':'block','height':'auto'});
							</script>");	
						}
					}
				}
				$code.="</ul>";
				return $code;
			}
			else
				return "";
		}//------------------------------------------------------------------------------------
		function getImage()
		{
			global $oPath;
			if(@$_SESSION["user_isLogin"])
			{
				$userId=$_SESSION["user_id"];
				if(file_exists($oPath->manageDir("users_bundle/data/images/user_{$userId}.jpg")))
					return $oPath->manage("users_bundle/data/images/user_{$userId}.jpg");
				else
					return $oPath->asset("default/images/users_larg.png");
			}		
		}
	}//------------------------------------------------------------------------------------	
?>
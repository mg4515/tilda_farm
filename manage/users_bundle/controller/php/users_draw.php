<?php
	//engine.php
	include_once $_SESSION["engineRequire"];
	include_once $oPath->manageDir("users_bundle/model/users_model.php");
	include_once $oPath->manageDir("users_bundle/model/usersMenu_model.php");
		
	$oUsers=new cUsers();
		
class cUsersDraw
{	
   function menuChksDraw($parentId=0,$accessArray) //list hame menuha be soorate check box
   {
		$oUsersMenu=new cUsersMenu(); 
		
      //list menuhaye ghabel dastras
      $items=$oUsersMenu->selectByParentId($parentId);
      $code="";
      if(count($items) > 0)
      {
         foreach($items as $item)
         {
            if(strlen(array_search($item->id,$accessArray)) > 0 ) $checked=" checked='checked'"; else $checked="";
            if((count($accessArray) == 1 && $accessArray[0]=="") || count($accessArray) == 0) $checked=" checked='checked'";
            if ($parentId == 0) $class="has-sub"; else $class="";
            $code.="
            <li class='{$class}'><input type='checkbox' id='chk_{$item->id}' value='{$item->superId}'{$checked} onchange='userAccessLevel_changedChk({$item->id});'>&nbsp;&nbsp;{$item->text}
               <ul style='list-style-type: none;'>";
                  $code.=menuChksDraw($item->id,$accessArray);
                  $code.="
               </ul>
            </li>
            ";
         }
         return $code;
      }
      else
         return "";
   }//--------------------------------------------------------------------------
   function menuListDraw($parentId=0,$pageActive="")  //list hame menuha be soorate derakhty
   {
		$oUsersMenu=new cUsersMenu(); 
      $items=$oUsersMenu->selectByParentId($parentId,1);
      $code="";
      if(count($items) > 0) 
      {
         $code.= "<ul class='mtree dark' >";
         foreach($items as $item)
         {
				$idCounter="spn_{$item->idName}";
				if($item->url=="")
				{   
					$code.="<li id='li_{$item->id}' class='nav-second-level'>
							  <a href='#'>
								  <i class='mtree-icon {$item->fontIcon_class}'></i>
								  {$item->text}
								  <span class='mtree-counter' id='{$idCounter}'></span>
							  </a>";
						 $code.=user_menuListDraw($item->id,$pageActive);
					$code.="</li>";
				}
				else
				{
					if($item->url == $pageActive) $activeClass='active'; else $activeClass='';
					$code.="
					<li id='li_{$pageActive}'>
						<a href='javascript:void(0)' onclick='oTools.link(\"?page={$item->url}\");' id='{$item->url}' class='{$activeClass}' >
							<i class='mtree-icon {$item->fontIcon_class}' ></i>
							{$item->text}
							<span class='mtree-counter' id='{$idCounter}'></span>
						</a>                    
					</li>
					";
					if($item->url == $pageActive) 
					{
						$oldSuperId=@$oUsersMenu->get($item->superId)->superId;
						cEngine::echoAfter("
						<script>
							try{
								$('#li_{$oldSuperId}').removeClass('mtree-closed');
								$('#li_{$oldSuperId}').addClass('mtree-open');
								$('#li_{$oldSuperId} ul:first').css({'display':'block','height':'auto'});
							}catch(e){}	
							
							$('#li_{$item->superId}').removeClass('mtree-closed');
							$('#li_{$item->superId}').addClass('mtree-open');
							$('#li_{$item->superId} ul:first').css({'display':'block','height':'auto'});
						</script>");	
					}
				}
         }
         $code.="</ul>";
         return $code;
      }
      else
         return "";
   }//------------------------------------------------------------------------------------
   function user($userId)
	{
		global $oPath;
		$oUsers=new cUsers();
		$user=(array)$oUsers->get($userId);
		if(file_exists($oPath->manageDir("user_bundle/data/images/user_{$userId}.jpg")))
			$user['imageSrc']=$oPath->manage("user_bundle/data/images/user_{$userId}.jpg");
		else
			$user['imageSrc']=$oPath->asset("default/images/admin_larg.png");
		return (object)$user;
	}//------------------------------------------------------------------------------------
}	
?>
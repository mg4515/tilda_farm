var url_users=oPath.manage("users_bundle/controller/php/users_panel_client.php");
isImageDelete=false;
function user_imgDel(userId)
{
   if(isImageDelete==false)
   {
      var ret=confirm("تصویر حذف شود ؟");
      if(ret)
      {
         isImageDelete=true;
         $("#img").attr("src",oPath.asset("default/images/user_larg.png"));
         $("#i_imageDelete").attr("class","fa fa-share fa-2x");
      }
   }
   else if(isImageDelete==true)
   {
      isImageDelete=false;
      $("#img").attr("src",oPath.manage("user_bundle/data/images/user_" + userId + ".jpg"));
      $("#i_imageDelete").attr("class","fa fa-trash fa-2x");
   }
}//-----------------------------------------------------------------------------
function user_logout()
{
	try {
		script_loadingShow();
		$.ajax({
			url: url_users,
			data: {"requestName": "user_logout"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				if(result=="ok")
				{
					oTools.link("login");
				}					
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() {
				alert("خطایی در اتصال رخ داده است");					
				script_loadingHide();
			}

		});
	} 
	catch (e) 
	{
	   alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function user_edit(userId) {
	try {
		script_loadingShow();
		isImageDelete=false;
		$.ajax({
			url: url_users,
			data: {"requestName": "user_edit","id":userId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
					$("#layer_content").html(spl[1]);
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------
function user_update(purpose,userId)
{
   try {
		err=false;
		if($("#txt_password").val() != '')
		{
			if($("#txt_password").val().length < 6)
			{
				script_alert2("","تعداد کاراکتر های رمز عبور نباید از 6 کاراکتر کمتر باشد","danger");
				script_focus("txt_password");
				err=true;							
			}
			else if($("#txt_password").val() != $("#txt_rePassword").val())
			{
				script_alert2("","تایید رمز عبور با رمز عبور نا برابر است","danger");
				script_focus("txt_rePassword");
				err=true;						
			}
		}
		if($("#txt_fName").val()=="" && err==false)
		{
			script_alert2('',"لطفا نام خود را وارد نمایید","danger");
			script_focus("txt_fName");
			err=true;			
		}

      if(err==false)
		{
			script_loadingShow();
			if(purpose=="new") userName=$("#txt_userName").val(); else userName="";
			var formData = new FormData();
			formData.append('fileImg', $('#fileImg').prop('files')[0]);
			formData.append('imgDel', isImageDelete);
			formData.append("userName",userName);
			formData.append("password",$("#txt_password").val());
			//profile
			formData.append('fName',$("#txt_fName").val());
			formData.append('lName',$("#txt_lName").val());
			formData.append('phone',$("#txt_phone").val());
			formData.append('mobile',$("#txt_mobile").val());
			formData.append('mail',$("#txt_mail").val());
			formData.append('dateOfBirth',$("#txt_dateOfBirth").val());
			formData.append('country','');
			formData.append('state',$("#slct_state").val());
			formData.append('city',$("#slct_city").val());
			formData.append('zipcode',$("#txt_zipcode").val());
			formData.append('address',$("#txt_address").val());
			formData.append('id',userId);
			formData.append('purpose',purpose);
			formData.append('requestName',"user_update");			

			$.ajax({
				url: url_users,
				dataType: 'text',  // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="errType")
					{
						script_alert2('',"نوع فایل تصویر، غیر مجاز می باشد","danger");
					}
					else if(spl[0]=="errSize")
					{
						script_alert2('',"سایز فایل تصویر، بیش از حد مجاز است","danger");
					}
					else if(spl[0]=="ok")
					{
						script_alert2('',"با موفقیت ذخیره شد","success");
						if(spl[2]!='') {document.querySelector('#layer_userImage').style.backgroundImage='';document.querySelector('#layer_userImage').style.backgroundImage="url('" + spl[2] + "')";}
						user_edit(spl[1]);
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() 
				{
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		}
	}
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function user_bankEdit() {
	try {
		script_loadingShow();
		isImageDelete=false;
		$.ajax({
			url: url_users,
			data: {"requestName": "user_bankEdit"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
					$("#layer_content").html(spl[1]);
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------
function user_bankUpdate()
{
   try {
		err=false;
		if($("#txt_bankCardNumber").val()=="" || $("#txt_bankCardNumber").val().length < 16)
		{
			script_alert2('',"لطفا شماره 16 رقمی کارت بانکی خود را وارد نمایید","danger");
			script_focus("txt_bankCardNumber");
			err=true;			
		}
		else if($("#txt_bankShaba").val()=="" || $("#txt_bankShaba").val() < 1)
		{
			script_alert2('',"لطفا شماره شبا حساب بانکی خود را وارد نمایید","danger");
			script_focus("txt_bankShaba");
			err=true;			
		}		

      if(err==false)
		{
			script_loadingShow();
			var formData = new FormData();
			formData.append("bankCardNumber",$("#txt_bankCardNumber").val());
			formData.append("bankShaba",$("#txt_bankShaba").val());
			formData.append('requestName',"user_bankUpdate");			

			$.ajax({
				url: url_users,
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					if(result=="ok")
					{
						script_alert2('',"با موفقیت ذخیره شد","success");
						user_bankEdit();
					}
					else
					{
						alert("انجام نشد");
					}
					script_loadingHide();
				},
				error: function() 
				{
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		}
	}
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function user_profileDraw(userId)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_users,
         data: {"requestName":"user_profileDraw","id":userId},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					code=''+		
					"<div class='myModal myModal-autoSize' id='layer_modalProfile' >"+
						"<div class='myModal-container' id='layer_modal_container'>"+
							"<div class='myModal-topBar'>"+
								"<i class='fa fa-times' onclick='myModal_hide(\"layer_modalProfile\");'></i>"+
								"<span>" + spl[2] + "</span>"+
							"</div>"+
							"<div class='myModal-content' id='layer_modal_content'>"+
								"<div class='part-w-10 algn-c'>"+
									"<div class='imageCircle' style='background-image:url("+ spl[4] +")'></div>"+
									"<div>"+ spl[3] +"</div>"+
									"<hr>"+
									"<button class='btn btn-label btn-info btn-block-auto' onclick='user_edit(" + spl[1] + ");myModal_hide(\"layer_modalProfile\");'>"+
										"<span class='btn-label-caption'><i class='btn-label-caption fa fa-pencil'></i></span>"+										
										"<span>ویرایش مشخصات</span>"+
									"</button>"+
									"<div class='vSpace'></div>"+
								"</div>"+
							"</div>"+
						"</div>"+
					"</div>";
               $("#layer_temp").html(code);
					myModal_show('layer_modalProfile');
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
var url_users=oPath.manage("users_bundle/controller/php/users_panel_admin.php");
isImageDelete=false;
function user_imgDel(userId)
{
   if(isImageDelete==false)
   {
      var ret=confirm("تصویر حذف شود ؟");
      if(ret)
      {
         isImageDelete=true;
         $("#img").attr("src",oPath.asset("default/images/user_larg.png"));
         $("#i_imageDelete").attr("class","fa fa-share fa-2x");
      }
   }
   else if(isImageDelete==true)
   {
      isImageDelete=false;
      $("#img").attr("src",oPath.manage("user_bundle/data/images/user_" + userId + ".jpg"));
      $("#i_imageDelete").attr("class","fa fa-trash fa-2x");
   }
}//-----------------------------------------------------------------------------
function users_search()
{
   searchWord=$("#txt_search").val();
   if(searchWord != "")
   	users_draw("","1",1,searchWord);	
   else
	   users_draw("","",1);	
}//-----------------------------------------------------------------------------
function users_draw(active,trash,searchWord,page) {
	try {
	   script_loadingShow();
	   if(searchWord == undefined) searchWord="";
		if(active=="auto")
		{
			active=-1;
			trash=-1;
			searchWord=-1;
			page=-1;
		}
	   $.ajax({
			url: url_users,
			data: {"requestName": "users_draw","active":active,"trash":trash,"searchWord":searchWord,"page":page},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
					$("#layer_content").html(spl[1]);
				else
				{
					alert("انجام نشد");
					alert(result);
				}
				script_loadingHide();
			},
			error: function() {
				alert("خطایی در اتصال رخ داده است");					
				script_loadingHide();
			}

	   });
	} 
	catch (e) 
	{
	   alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function users_listDraw(searchWord,selectedIds,multiSelect,page)
{
   try { 
      script_loadingShow();
      $.ajax({
         url: url_users,
         data: {"requestName":"users_listDraw", "searchWord":searchWord, "selectedIds":selectedIds, "multiSelect":multiSelect, "page":page},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					dialog=new myModal({
						'showBottomBar':true,
						'bottomBarContent': '<div style="padding-right:5px">' + spl[2] + '</div>',
						'centerContent':spl[1]
					});
					dialog.show();
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function users_listDrawBySearch()
{
	dialog.hide();
   users_listDraw($("#txt_usersSearch"));	
}//-----------------------------------------------------------------------------
function users_treeDraw(showSearch,searchWord,selectId)
{
   try { 
      script_loadingShow();
      $.ajax({
         url: url_users,
         data: {"requestName":"users_treeDraw", "showSearch":showSearch, "searchWord":searchWord, "selectId":selectId},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_temp").html(spl[1]);
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function user_delete(userId) 
{
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			script_loadingShow();
			if(userId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=userId;
			$.ajax({
				url: url_users,
				data: {"requestName": "user_delete","id":id},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
					if(spl[0]=="ok")
					{
						users_draw("auto");
					}
					else
					{
						alert("انجام نشد");
						alert(result);
					}
					script_loadingHide();
				},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
			});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
    }
}//---------------------------------------------------------------------------------------
function user_trash(userId) {
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			if(userId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=userId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_users,
					data: {"requestName": "user_trash","id":id},
					method: "POST",
					success: function(result)
					{
						result=oEngine.response(result);
						if(result=="ok")
						{
							users_draw("auto");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
   }
}//---------------------------------------------------------------------------------------
function user_unTrash(userId) 
{
   var ret=confirm("حذف شود ؟.");
   if(ret)
   {
      try {
			if(userId=="selected")
			{
				id="";
				var count=$("#chk_all").val();
				for(i=1;i <= count;i++)
				{
					var chkbox=document.getElementById("chk_" + i);
					if(chkbox.checked) id+=chkbox.value + ",";
				}
			}
			else
				id=userId;
			if(id!="")
			{
				script_loadingShow();
				$.ajax({
					url: url_users,
					data: {"requestName": "user_unTrash","id":id},
					method: "POST",
					success: function(result)
					{
						result=JoEngine.response(result);
						var spl=result.split("[|]");
						if(result=="ok")
						{
							users_draw("auto");
						}
						else
						{
							alert("انجام نشد");
						}
						script_loadingHide();
					},
					error: function() {
						script_loadingHide();
						alert("خطایی در اتصال رخ داده است");
					}
				});
			}
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
   }
}//---------------------------------------------------------------------------------------
function user_active(userId)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_users,
         data: {"requestName":"user_active","id":userId},
         method: "POST",
         success: function(result)
         {
            result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
               $("#td_active_" + spl[1]).html(spl[2]);
            else
               alert("انجام نشد");
            script_loadingHide();
         },
         error: function() {
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
function user_edit(userId) {
	try {
		script_loadingShow();
		isImageDelete=false;
		$.ajax({
			url: url_users,
			data: {"requestName": "user_edit","id":userId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
					$("#layer_content").html(spl[1]);
				else
				{
					alert("انجام نشد");
					alert(result);
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
		} catch (e) {
			alert("خطای اسکریپت");
			script_loadingHide();
		}
}//---------------------------------------------------------------------------------------
function user_new(userId) {
	try {
		script_loadingShow();
		isImageDelete=false;
		$.ajax({
			url: url_users,
			data: {"requestName": "user_new","id":userId},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				var spl=result.split("[|]");
				if(spl[0]=="ok")
					$("#layer_content").html(spl[1]);
				else
				{
					alert("انجام نشد");
					alert(result);
				}
				script_loadingHide();
			},
				error: function() {
					script_loadingHide();
					alert("خطایی در اتصال رخ داده است");
				}
		});
	} 
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function user_update(purpose,userId)
{
   try {
      script_loadingShow();
		if(purpose=="new") userName=$("#txt_userName").val(); else userName="";
      var formData = new FormData();
      formData.append('fileImg', $('#fileImg').prop('files')[0]);
		formData.append('imgDel', isImageDelete);
		formData.append("userName",userName);
		formData.append("password",$("#txt_password").val());
		//profile
		formData.append('fName',$("#txt_fName").val());
		formData.append('lName',$("#txt_lName").val());
		formData.append('phone',$("#txt_phone").val());
		formData.append('mobile',$("#txt_mobile").val());
		formData.append('mail',$("#txt_mail").val());
		formData.append('dateOfBirth',$("#txt_dateOfBirth").val());
		formData.append('country','');
		formData.append('state',$("#slct_state").val());
		formData.append('city',$("#slct_city").val());
		formData.append('zipcode',$("#txt_zipcode").val());
		formData.append('address',$("#txt_address").val());
		formData.append('id',userId);
		formData.append('purpose',purpose);
		formData.append('requestName',"user_update");			

      $.ajax({
         url: url_users,
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: formData,
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="errType")
            {
               alert("نوع فایل تصویر، غیر مجاز می باشد");
            }
            else if(spl[0]=="errSize")
            {
               alert("سایز فایل تصویر، بیش از حد مجاز است");
            }
            else if(spl[0]=="ok")
            {
               users_draw("auto");
            }
            else
            {
               alert("انجام نشد");
               alert(result);
            }
            script_loadingHide();
         },
         error: function() 
			{
            script_loadingHide();
            alert("خطایی در اتصال رخ داده است");
         }
      });
	}
	catch (e) 
	{
		alert("خطای اسکریپت");
		script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function user_profileDraw(userId)
{
   try {
      script_loadingShow();
      $.ajax({
         url: url_users,
         data: {"requestName":"user_profileDraw","id":userId},
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
					code=''+		
					"<div class='myModal myModal-autoSize' id='layer_modalProfile' >"+
						"<div class='myModal-container' id='layer_modal_container'>"+
							"<div class='myModal-topBar'>"+
								"<i class='fa fa-times' onclick='myModal_hide(\"layer_modalProfile\");'></i>"+
								"<span>" + spl[2] + "</span>"+
							"</div>"+
							"<div class='myModal-content' id='layer_modal_content'>"+
								"<div class='part-w-10 algn-c'>"+
									"<div class='imageCircle' style='background-image:url("+ spl[4] +")'></div>"+
									"<div>"+ spl[3] +"</div>"+
									"<hr>"+
									"<button class='btn btn-label btn-info btn-block-auto' onclick='user_edit(" + spl[1] + ");myModal_hide(\"layer_modalProfile\");'>"+
										"<span class='btn-label-caption'><i class='btn-label-caption fa fa-pencil'></i></span>"+										
										"<span>ویرایش مشخصات</span>"+
									"</button>"+
									"<div class='vSpace'></div>"+
								"</div>"+
							"</div>"+
						"</div>"+
					"</div>";
               $("#layer_temp").html(code);
					myModal_show('layer_modalProfile');
            }
            else
            {
               alert("انجام نشد!.");
            }
            script_loadingHide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            script_loadingHide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		script_loadingHide();
   }
}//-----------------------------------------------------------------------------
var url_users=oPath.manage("users_bundle/controller/php/users_ajax.php");
isItemDelImg=false;
function user_isLogin(yesUrl,yesScript,noUrl)
{
	try {
		loading_show();
		$.ajax({
			url: url_users,
			data: { "data":oEngine.request({"requestName": "user_isLogin","yesUrl":yesUrl,"yesScript":yesScript,"noUrl":noUrl}) },
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				spl=result.split("[|]");
				if(spl[0]=="yes")
				{
					if(spl[1]!="") post_get(spl[1]);//yesUrl
					else if(spl[2]!="") eval(spl[2]);//yesScript
				}	
				else if(spl[0]=="no")
				{
					post_get("login?b=" + spl[1]);//noUrl
				}				
				else
				{
					alert("انجام نشد");
					alert(resulte);
				}
				loading_hide();
			},
			error: function() {
				alert("خطایی در اتصال رخ داده است");					
				loading_hide();
			}

		});
	} catch (e) {
	   alert("خطای اسکریپت");
	   loading_hide();
	}
}//---------------------------------------------------------------------------------------
function user_login(backUrl)
{
	try {
		err=false;
		$("#spn_alert").html("");
		if($("#txt_userName").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل کاربری خود را وارد نمایید")
         err=true;			
		}	
		else if($("#txt_password").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا رمز عبور خود را وارد نمایید");
         err=true;			
		}
      else if(err==false)
		{
			$('#btn_login').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
				url: url_users,
				data: {"requestName": "user_login","backUrl":backUrl,"password":$("#txt_password").val(),"userName":$("#txt_userName").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
						if(spl[1])
					      oTools.link(spl[1]);	
                  else						
					      oTools.link("user?page=" + spl[2]);				
               }
               else if (spl[0]=="!userName" || spl[0]=="!password")
						script_alertStatus("spn_alert","danger","نام کاربری یا رمز عبور اشتباه است");	
               else if (spl[0]=="!active")
						oTools.link('active?alert=log&userName=' + spl[1]);					
               else
               {
                  alert("انجام نشد");
               }
					$('#btn_login').html('ورود');
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					$('#btn_login').html('ورود');
				}
			});
		}
	} 
	catch (e) 
	{
	   alert("خطای اسکریپت");
	   $('#btn_login').html('ورود');
	}
}//---------------------------------------------------------------------------------------
function user_logout()
{
	try {
		script_loadingShow();
		$.ajax({
			url: url_users,
			data: {"requestName": "user_logout"},
			method: "POST",
			success: function(result)
			{
				result=oEngine.response(result);
				if(result=="ok")
				{
					oTools.link("login");
				}					
				else
				{
					alert("انجام نشد");
				}
				script_loadingHide();
			},
			error: function() {
				alert("خطایی در اتصال رخ داده است");					
				script_loadingHide();
			}

		});
	} 
	catch (e) 
	{
	   alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function user_register_mobile()
{
	try {
		err=false;
		$("#layer_msg").html("");
		if($("#txt_userName").val()=="")
		{
			status("layer_msg","danger","لطفا شماره موبایل کاربری خود را وارد نمایید");
			status("layer_registerMsg","danger","لطفا شماره موبایل کاربری خود را وارد نمایید");
         err=true;			
		}
		else if($("#txt_userName").val().length != 11)
		{
			status("layer_msg","danger","لطفا شماره موبایل را به صورت صحیح وارد نمایید");
			status("layer_registerMsg","danger","لطفا شماره موبایل را به صورت صحیح وارد نمایید");
         err=true;			
		}
		else if($("#txt_password").val()=="")
		{
			status("layer_msg","danger","لطفا رمز عبور خود را وارد نمایید");
			status("layer_registerMsg","danger","لطفا رمز عبور خود را وارد نمایید");
         err=true;			
		}
		else if($("#txt_password").val().length < 6)
		{
			status("layer_msg","danger","تعداد کاراکتر های رمز عبور نباید از 6 کاراکتر کمتر باشد");
			status("layer_registerMsg","danger","تعداد کاراکتر های رمز عبور نباید از 6 کاراکتر کمتر باشد");
         err=true;			
		}
		else if($("#txt_password").val() != $("#txt_rePassword").val())
		{
			status("layer_msg","danger","تکرار رمز عبور با رمز عبور نا برابر است");
			status("layer_registerMsg","danger","تکرار رمز عبور با رمز عبور نا برابر است");
         err=true;			
		}
      else if(err==false)
		{
			loading_show();
			$.ajax({
				url: url_users,
				data: { "data":oEngine.request({"requestName": "user_register","userName":$("#txt_userName").val(),"password":$("#txt_password").val()}) },
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					alert(result);
					spl=result.split("[|]");
               if(spl[0]=="ok")
               {
						$("#layer_form").html("");	
						status("layer_msg","success","ثبت نام انجام شد. کد فعال سازی به شماره همراه شما پیامک گردید. به صفحه فعال سازی هدایت خواهید شد.&nbsp;<button class='btn btn-info' onclick='post_get(\"userActive\",{},\"get\");' >فعال سازی</button>");
						status("layer_registerMsg","success","ثبت نام انجام شد. کد فعال سازی به شماره همراه شما پیامک گردید. به صفحه فعال سازی هدایت خواهید شد.&nbsp;<button class='btn btn-info' onclick='post_get(\"userActive\",{},\"get\");' >فعال سازی</button>");
                  window.setTimeout(function(){
							post_get("userActive",{},"get");
						},8000);	
               }
               else if (spl[0]=="!userName")
					{
						status("layer_msg","danger","شماره موبایل وارد شده، قبلا ثبت شده است. لطفا از یک شماره موبایل دیگر استفاده کنید و یا اگر قبلا ثبت نام کرده اید، از قسمت ورود به سایت وارد قسمت کاربری خود شوید");					
						status("layer_registerMsg","danger","شماره موبایل وارد شده، قبلا ثبت شده است. لطفا از یک شماره موبایل دیگر استفاده کنید و یا اگر قبلا ثبت نام کرده اید، از قسمت ورود به سایت وارد قسمت کاربری خود شوید");					
               }
					else
               {
                  alert("انجام نشد");
               }
					loading_hide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					loading_hide();
				}

			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
	   loading_hide();
	}
}//---------------------------------------------------------------------------------------
function user_register_mail()
{
	try {
		err=false;
		$("#spn_alert").html("");
		if($("#txt_fName").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا نام خود را وارد نمایید")
         err=true;			
		}
		else if($("#txt_userName").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل خود را وارد نمایید")
         err=true;			
		}
		else if(!oTools.isMail($("#txt_userName").val()))
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل خود را به صورت صحیح وارد نمایید")
         err=true;		
		}
		else if($("#txt_password").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا رمز عبور خود را وارد نمایید")
         err=true;							
		}
		else if($("#txt_password").val().length < 6)
		{
			script_alertStatus("spn_alert","danger","تعداد کاراکتر های رمز عبور نباید از 6 کاراکتر کمتر باشد")
         err=true;							
		}
		else if($("#txt_password").val() != $("#txt_rePassword").val())
		{
			script_alertStatus("spn_alert","danger","تایید رمز عبور با رمز عبور نا برابر است")
         err=true;						
		}
      else if(err==false)
		{
			$('#btn_register').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
				url: url_users,
				data: {"requestName": "user_register_mail","fName":$("#txt_fName").val(),"userName":$("#txt_userName").val(),"password":$("#txt_password").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					spl=result.split("[|]");
               if(spl[0]=="ok")
               {
						$("#layer_form").html("");	
						oTools.link('active?alert=reg&userName=' + spl[1]);
               }
               else if (spl[0]=="!userName")
					{
						script_alertStatus("spn_alert","danger","نام کاربری موجود می باشد لطفا نام کاربری جدیدی وارد نمایید");					
               }
					else
               {
                  alert("انجام نشد");
               }
					$('#btn_register').html('ثبت نام');
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					$('#btn_register').html('ثبت نام');
				}

			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
		$('#btn_register').html('ثبت نام');
	}
}//---------------------------------------------------------------------------------------
function user_active_sms()
{
	try {
		err=false;
		$("#layer_msg").html("");
		if($("#txt_userMobile").val()=="")
		{
			status("layer_msg","danger","لطفا شماره موبایل کاربری خود را وارد نمایید");
         err=true;			
		}		
		else if($("#txt_activeCode").val()=="")
		{
			status("layer_msg","danger","لطفا کد فعال سازی را ورد نمایید");
         err=true;			
		}
      else if(err==false)
		{
			loading_show();
			$.ajax({
				url: url_users,
				data: { "data":oEngine.request({"requestName": "user_activeByCode","activeCode":$("#txt_activeCode").val(),"userMobile":$("#txt_userMobile").val()}) },
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					alert(result);
               if(result=="ok")
               {
						$("#layer_form").html("");	
						status("layer_msg","success","حساب کاربری شما با موفقیت فعال گردید. به صفحه ورود هدایت خواهید شد.");
                  window.setTimeout(function(){
							post_get("login",{},"get");
						},8000);						
               }
               else if (result=="err")
						status("layer_msg","danger","کد فعال سازی وارد شده،صحیح نمی باشد");					
               else
               {
                  alert("انجام نشد");
                  alert(resulte);
               }
					loading_hide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					loading_hide();
				}
			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
	   loading_hide();
	}
}//---------------------------------------------------------------------------------------
function user_active_mail()
{
	try {
		err=false;
		$("#spn_alert").html("");
		if($("#txt_userName").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل خود را وارد نمایید")
         err=true;			
		}
		else if(!oTools.isMail($("#txt_userName").val()))
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل خود را به صورت صحیح وارد نمایید")
         err=true;		
		}		
		else if($("#txt_code").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا کد فعال سازی را وارد نمایید");
         err=true;			
		}
      else if(err==false)
		{
			$('#btn_active').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
				url: url_users,
				data: {"requestName": "user_active_mail","code":$("#txt_code").val(),"userName":$("#txt_userName").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					var spl=result.split("[|]");
               if(spl[0]=="ok")
               {
						$("#layer_form").html("");	
						script_alertStatus("spn_alert","success","حساب کاربری شما با موفقیت فعال گردید. به صفحه کاربری هدایت خواهید شد.");
                  window.setTimeout(function(){
							oTools.link("user?page=" + spl[1]);
						},4000);						
               }
               else if (spl[0]=="err")
						script_alertStatus("spn_alert","danger","کد فعال سازی وارد شده،صحیح نمی باشد");					
               else
               {
                  alert("انجام نشد");
               }
					$('#btn_active').html('فعال سازی');
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					$('#btn_active').html('فعال سازی');
				}
			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
	   $('#btn_active').html('فعال سازی');
	}
}//---------------------------------------------------------------------------------------
function user_active_reSms()
{
	try {
		err=false;
		$("#layer_msg").html("");
		if($("#txt_userMobile").val()=="")
		{
			status("layer_msg","danger","لطفا شماره موبایل کاربری خود را وارد نمایید");
			document.querySelector("#layer_msg").scrollIntoView({ behavior: 'smooth' });
         err=true;			
		}		
      else if(err==false)
		{
			loading_show();
			$.ajax({
				url: url_users,
				data: { "data":oEngine.request({"requestName": "user_activeSendSmsCode","userMobile":$("#txt_userMobile").val()}) },
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
					alert(result);
               if(result=="ok")
               {
						status("layer_msg","success","کد فعال سازی به شماره موبایل شما، پیامک گردید.");
               }
               else if (result=="err")	
                  status("layer_msg","danger","شماره موبایل وارد شده در سیستم ثبت نشده است.");					
               else
               {
                  alert("انجام نشد");
                  alert(resulte);
               }
					loading_hide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					loading_hide();
				}

			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
	   loading_hide();
	}
}//---------------------------------------------------------------------------------------
function user_active_reMail()
{
	try {
		err=false;
		$("#spn_alert").html("");
		if($("#txt_userName").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل خود را وارد نمایید")
         err=true;			
		}
		else if(!oTools.isMail($("#txt_userName").val()))
		{
			script_alertStatus("spn_alert","danger","لطفا ایمیل خود را به صورت صحیح وارد نمایید")
         err=true;		
		}			
      else if(err==false)
		{
			$('#btn_reActive').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
				url: url_users,
				data: {"requestName": "user_active_reMail","userName":$("#txt_userName").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
               if(result=="ok")
               {
						script_alertStatus("spn_alert","success","کد فعال سازی به آدرس ایمیل شما ارسال شد");
               }
               else if (result=="err")	
                  script_alertStatus("spn_alert","danger","آدرس ایمیل وارد شده در سیستم ثبت نشده است");					
               else
               {
                  alert("انجام نشد");
                  alert(resulte);
               }
					$('#btn_reActive').html('ارسال مجدد کد');
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					$('#btn_reActive').html('ارسال مجدد کد');
				}

			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
	   $('#btn_reActive').html('ارسال مجدد کد');
	}
}//---------------------------------------------------------------------------------------
function user_password_mail()
{
	try {
		err=false;
		$("#spn_alert").html(""); 
		if($("#txt_userName").val()=="")
		{
			script_alertStatus("spn_alert","danger","لطفا آدرس ایمیل کاربری خود را وارد نمایید");
         err=true;
		}		
      else if(err==false)
		{
			script_loadingShow();
			$.ajax({
				url: url_users,
				data: {"requestName": "user_password_mail","userName":$("#txt_userName").val()},
				method: "POST",
				success: function(result)
				{
					result=oEngine.response(result);
               if(result=="ok")
               {	
						script_alertStatus("spn_alert","success","رمز عبور جدید به آدرس ایمیل شما ارسال گردید." + "<hr><a class='btn' href='login'>ورود</a>" + "&nbsp;<a class='btn' href='forget'>ارسال دوباره</a>");
						$("#layer_form").html("");
               }
               else if (result=="err")	
                  script_alertStatus("spn_alert","danger","آدرس ایمیل وارد شده در سیستم ثبت نشده است.");					
               else
               {
                  alert("انجام نشد");
                  alert(resulte);
               }
					script_loadingHide();
				},
				error: function() {
					alert("خطایی در اتصال رخ داده است");					
					script_loadingHide();
				}

			});
		}
	} catch (e) {
	   alert("خطای اسکریپت");
	   script_loadingHide();
	}
}//---------------------------------------------------------------------------------------
function user_profileDraw(userId,isMe)
{
	isMe=(typeof isMe == "undefined") ? "0":"1";
   try {
      loading_show();
      $.ajax({
         url: url_users,
         data: { "data": oEngine.request({"requestName":"user_profileDraw","id":userId,"isMe":isMe}) },
         method: "POST",
         success: function(result)
         {
				result=oEngine.response(result);
            var spl=result.split("[|]");
            if(spl[0]=="ok")
            {
               $("#layer_temp").html(spl[1]);
					//myModal_show('layer_modalProfile');
            }
            else
            {
               alert("انجام نشد!.");
            }
            loading_hide();
         },
         error: function() {
				alert("خطایی در اتصال رخ داده است");
            loading_hide();
         }
      });
   }
   catch (e)
   {
		alert("خطای اسکریپت");
		loading_hide();
   }
}//-----------------------------------------------------------------------------
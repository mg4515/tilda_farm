<?php
//use include myDb.php;
class cUsersMenu
{
   private $tbl_usersMenu="usersMenu"; 

   public $menus_ret;
   public $menu_id;
   public $menu_text;
   public $menu_title;
   public $menu_class;
   public $menu_style;
   public $menu_url;
   public $menu_checksum;
   public $menu_active;
   public $menu_trash;

   function getAll($sortByRow=true,$active="1",$limitStr="")
   {
      global $oDb;
      global $oDbq;
      $where="";
      if($active!="")
      {
         if($where=="") $where="`active`='{$active}'"; else $where.=" AND `active`='{$active}'";
      }
      if($sortByRow==true)$ret=$oDbq->table($this->tbl_usersMenu)->fields("*")->where($where)->orderBy("`id`");
      else if($sortByRow==false)$ret=$oDbq->table($this->tbl_usersMenu)->fields("*")->where($where);
      if($limitStr!="") $this->menus_ret=$ret->limit($limitStr)->select();
      else $this->menus_ret=$ret->select();
      return $this->menus_ret;
   }//--------------------------------------------------------------------------
   function get($id)
   {
      global $oDb;
      global $oDbq;
      $ret=$oDbq->table($this->tbl_usersMenu)->fields("*")->where("`id`='{$id}'")->select()[0];
      $this->menu_id=$id;
      $this->menu_active=$ret->act;
      $this->menu_checksum=$ret->urlChecksum;
      $this->menu_style=$ret->style;
      $this->menu_text=$ret->text;
      $this->menu_title=$ret->title;
      $this->menu_trash=$ret->trash;
      $this->menu_url=$ret->url;
      return $ret;
   }//--------------------------------------------------------------------------
   function getPageIndex()
   {
      global $oDbq;
      $ret=@$oDbq->table($this->tbl_usersMenu)->fields("*")->where("`pageIndex`='1'")->select()[0];
		return $ret;
   }//--------------------------------------------------------------------------
   function getListArray()
   {
      $menus=$this->getAll(false);
      foreach($menus as $menu)
      {
         $ret[]=$menu->id;
      }
      return $ret;
   }//---------------------------------------------------------------------------
   public function getByParentId($parentId=0,$showMenubar="")
   {
      global $oDb;
      global $oDbq;
      $where="";
      if($showMenubar!="")
      {
         if($where=="") $where="`showMenubar`='{$showMenubar}' AND "; else $where.=" AND `showMenubar`='{$showMenubar}'";
      }
      return $oDbq->fields('*')
                     ->table($this->tbl_usersMenu)
                     ->where("{$where}`trash`=0 AND `active`=1 AND `parentId`='{$parentId}'")
                     ->orderby("orderId")
                     ->select();
   }//--------------------------------------------------------------------------
   public function getByUrl($url)
   {
      if($url != "login")
      {
			global $oDb;
			global $oDbq;
         $ret=@$oDbq->fields('*')->table($this->tbl_usersMenu)->where("`trash`=0 AND `active`=1 AND `url`='{$url}'")->select()[0];
         return $ret;
      }
   }//--------------------------------------------------------------------------
   public function getByIdName($url)
   {
      if($url != "login")
      {
			global $oDb;
			global $oDbq;
         $ret=@$oDbq->fields('*')->table($this->tbl_usersMenu)->where("`trash`=0 AND `active`=1 AND `url`='{$url}'")->select()[0];
         return $ret;
      }
   }//--------------------------------------------------------------------------
   public function selectAll()
   {
      global $oDb;
      global $oDbq;
      return $oDbq->fields('*')
                     ->table($this->tbl_usersMenu)
                     ->where("`trash`=0 AND `active`=1")
                     ->select();
   }//--------------------------------------------------------------------------
   function delete($id)
   {
      global $oDb;
      global $oDbq;
      $oDbq->table($this->tbl_usersMenu)->where("`id`={$id}")->delete();
   }//--------------------------------------------------------------------------
   function runFirstCode()
   {
      /*$menus=$this->selectAll();
      $modir=new cAdmin();
      for($i=0;$i < count($menus);$i++)
      {
         $page=$menus[$i]->url;
         if($modir->isAllow($page))
            @include_once "../firstCode/" . $page . ".php";
      }*/
   }//------------------------------------------------------------------------------------
}

?>
<?php
class cUsersInform
{
   private $tbl_usersInform="usersInform";
	
	function getAll($argArray)
   {
		global $oDbq;
		
		$userId=isset($argArray["userId"]) ? $argArray["userId"] : "";
		$searchWord=isset($argArray["searchWord"]) ? $argArray["searchWord"] : "";
		$checkout=isset($argArray["checkout"]) ? $argArray["checkout"] : "";
		$sortByRow=isset($argArray["sortByRow"]) ? $argArray["sortByRow"] : true;		
		$limitStr=isset($argArray["limitStr"]) ? $argArray["limitStr"] : "";	

      $where="";
      if($userId!=="")
      {
         if($where=="") $where="`userId`='{$userId}'"; else $where.=" AND `userId`='{$userId}'";
      }	
		if($searchWord !== "")
		{
			if($where=="") 
				$where="`message` LIKE '%{$searchWord}%'";
		   else
				$where.=" AND `message` LIKE '%{$searchWord}%'";
		}
      if($checkout!=="")
      {
         if($where=="") $where="`checkout`='{$checkout}'"; else $where.=" AND `checkout`='{$checkout}'";
      }						
		
      if($sortByRow==true)$ret=$oDbq->table($this->tbl_usersInform)->fields("*")->where($where)->orderBy("`id` DESC");
		else $ret=$oDbq->table($this->tbl_usersInform)->fields("*")->where($where)->orderBy("`id` DESC");  
      
		if($limitStr!="") return $ret->limit($limitStr)->select();
      else return $ret->select();
   }//--------------------------------------------------------------------------
	function get($id)
   {
      global $oDbq;
      $ret=@$oDbq->table($this->tbl_usersInform)->fields("*")->where("`id`={$id}")->select()[0];
      return $ret;
   }//--------------------------------------------------------------------------
	function delete($id,$userId="")
	{
		global $oDbq;
		if($userId!="") $where="`userId`={$userId} AND "; else $where="";
		$ids=explode(",",rtrim($id,","));
		for($i=0;$i < count($ids);$i++)
		{
			$selectedId=$ids[$i];
			if($selectedId !="" )
			{
				$where.="(`id`='{$selectedId}')";
				if($i < count($ids) - 1) $where.=" OR ";
			}
		}

		if($where != "") $oDbq->table($this->tbl_usersInform)->fields("*")->where("({$where})")->delete();	
		return $ids;		
	}//---------------------------------------------------------------------------				
   function insert($array)
	{
      global $oDbq;
		$id=time();
		$insertById=isset($array['insertById']) ? $array['insertById'] : 0;
		$insertDate=time();
      $oDbq->table($this->tbl_usersInform)->set("`id`={$id},
		                                       `userId`='{$array['userId']}',												
															`message`='{$array['message']}',																																		
		                                       `insertById`={$insertById},
		                                       `insertDate`={$insertDate}
		                                      ")->insert();	
      return $id;		
	}//--------------------------------------------------------------------------
   function update($array)
	{
      global $oDbq;
		$id=time();
		$updateById=isset($array['updateById']) ? $array['updateById'] : 0;
		$updateDate=time();
      $oDbq->table($this->tbl_usersInform)->set("`message`='{$array['message']}',																																			
		                                           `updateById`={$updateById},
		                                           `updateDate`={$updateDate}
		                                       ")->where("`id`={$id}")->update();	
	}//--------------------------------------------------------------------------		
}

?>
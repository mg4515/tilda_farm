<?php
include_once $oPath->manageDir('users_bundle/model/usersMenu_model.php');
class cUsers
{
   private $tbl_users='users'; 
   private $tbl_usersProfile='usersProfile'; 
	
	public function getAll($active='',$trash='',$searchWord='',$limitStr='')
	{
		global $oDbq;
		$where="`{$this->tbl_usersProfile}`.`userType`='user' AND `{$this->tbl_users}`.`id`=`{$this->tbl_usersProfile}`.`userId`";
		//---
		if($active!=='')
		{
		   if($where=='') $where="`users`.`active`={$active}"; else $where.=" AND `users`.`active`={$active}";
		}		
		//---
		if($trash!=='')
		{
		   if($where=='') $where="`users`.`trash`={$trash}"; else $where.=" AND `users`.`trash`={$trash}";
		}
		//---
		if($searchWord!='')
		{
		   if($where=='') $where="(`{$this->tbl_users}`.`userName` LIKE '%{$searchWord}%' OR `{$this->tbl_usersProfile}`.`fName` LIKE '%{$searchWord}%' OR `{$this->tbl_usersProfile}`.`lName` LIKE '%{$searchWord}%' OR `{$this->tbl_usersProfile}`.`mobile` LIKE '%{$searchWord}%')"; else $where.=" AND (`{$this->tbl_users}`.`userName` LIKE '%{$searchWord}%' OR `{$this->tbl_usersProfile}`.`fName` LIKE '%{$searchWord}%' OR `{$this->tbl_usersProfile}`.`lName` LIKE '%{$searchWord}%' OR `{$this->tbl_usersProfile}`.`mobile` LIKE '%{$searchWord}%')";
		}
		$ret=$oDbq->table("`{$this->tbl_users}`,`{$this->tbl_usersProfile}`")->where($where);
		if ($limitStr!='') 
			return $ret->limit($limitStr)->select();
		else
			return $ret->select();
	}//------------------------------------------------------------------------------------------------------
	public function get($userNameId,$trash="")
	{
		global $oDbq,$oPath;
		$where='';
		if($trash!='')
		{
		   $where=" AND `users`.`trash`={$trash}";
		}		
		$ret=@$oDbq->table("`{$this->tbl_users}`,`{$this->tbl_usersProfile}`")->fields('*')->where("(`users`.`id`='{$userNameId}' AND `usersProfile`.`userId`='{$userNameId}'{$where}) OR (`users`.`userName`='{$userNameId}' AND `usersProfile`.`userName`='{$userNameId}'{$where})")->select('','assoc')[0];
	   if($ret)
		{
		   if(file_exists($oPath->manageDir("users_bundle/data/images/{$ret['id']}.png")))
			   $img=$oPath->manage("users_bundle/data/images/{$ret['id']}.png");
			else
				$img='';
			$ret['image']=$img;
			$ret=(object)$ret;
		}
		return $ret;
	}//------------------------------------------------------------------------------------------------------
	public function getByLoginToken($loginToken)
	{
		global $oDbq,$oPath;	
		$ret=@$oDbq->table("`{$this->tbl_users}`,`{$this->tbl_usersProfile}`")->fields('*')->where("`users`.`loginToken`='{$loginToken}' AND `usersProfile`.`userId`=`users`.`id`")->select()[0];
		return $ret;
	}//------------------------------------------------------------------------------------------------------	
	public function delete($userNameId) //dalete
	{
		global $oDbq;

		$where="";
		$whereProfile="";
		$userNameIds=explode(",",rtrim($userNameId,","));
		for($i=0;$i < count($userNameIds);$i++)
		{
			$selected=$userNameIds[$i];
			if($selected !="" )
			{
				$where.="(`username`='{$selected}' OR `id`='{$selected}')";
				if($i < count($userNameIds) - 1) $where.=" OR ";

				$whereProfile.="(`userName`='{$selected}' OR `id`='{$selected}')";
				if($i < count($userNameIds) - 1) $whereProfile.=" OR ";
			}
		}

		$oDbq->table($this->tbl_users)->where("({$where})")->delete();
		$oDbq->table($this->tbl_usersProfile)->where("({$whereProfile})")->delete();
		return $userNameIds;
	}//------------------------------------------------------------------------------------
	public function trash($userNameId,$userId=0)
	{
		global $oDbq;

		$where="";
		$userNameIds=explode(",",rtrim($userNameId,","));
		for($i=0;$i < count($userNameIds);$i++)
		{
			$selected=$userNameIds[$i];
			if($selected !="" )
			{
				$where.="(`username`='{$selected}' OR `id`='{$selected}')";
				if($i < count($userNameIds) - 1) $where.=" OR ";
			}
		}

		$oDbq->table($this->tbl_users)->set("`trash`=1,`trashById`={$userId}")->where("({$where})")->update();
		return $userNameIds;
	}//------------------------------------------------------------------------------------
	public function unTrash($userNameId)
	{
		global $oDbq;

		$where="";
		$userNameIds=explode(",",rtrim($userNameId,","));
		for($i=0;$i < count($userNameIds);$i++)
		{
			$selected=$userNameIds[$i];
			if($selected !="" )
			{
				$where.="(`username`='{$selected}' OR `id`='{$selected}')";
				if($i < count($userNameIds) - 1) $where.=" OR ";
			}
		}

		$oDbq->table($this->tbl_users)->set('`trash`=0')->where("({$where})")->update();
		return $userNameIds;
	}//--------------------------------------------------------------------------------------------------------------------
	public function insert($arr)
	{
		global $oDbq;
		$id=time();
		$userName=$arr["userName"];
		$saltPassword=$id . "SHIRAZZARIHS";
		$passwordHash=password_hash($arr["password"] . $saltPassword,PASSWORD_BCRYPT);
		
		//PROFILE
		$fName=$arr["fName"];
		$lName=$arr["lName"];
		$dateOfBirth=$arr["dateOfBirth"]; //time stamp format
		$country=@$arr["country"]; //IRAN
		$state=@$arr["state"];
		$city=@$arr["city"];
		$zipCode=@$arr["zipcode"];
		$address=$arr["address"];
		$phone=@$arr["phone"];
		$mobile=$arr["mobile"];
		//$mail=$arr["mail"];
		$active="1";

		$oDbq->table($this->tbl_users)->fields("
			`id`={$id},
			`userName`='{$userName}',
			`password`='{$passwordHash}',
			`saltPassword`='{$saltPassword}',
			`dateRegister`='{$id}',
			`active`='{$active}'
		")->insert();

		$oDbq->table($this->tbl_usersProfile)->fields("
			`id`={$id},
			`userId`={$id},
			`userName`={$userName},
			`userType`='user',
			`fName`='{$fName}',
			`lName`='{$lName}',
			`dateOfBirth`={$dateOfBirth},
			`country`='{$country}',
			`state`='{$state}',
			`city`='{$city}',
			`zipCode`='{$zipCode}',
			`address`='{$address}',
			`phone`='{$phone}',
			`mobile`='{$mobile}'
		")->insert();
		return $id;
	}//-----------------------------------------------------------------------
	function update($arr)
	{
		global $oDbq;
		$id=$arr["id"];
		if($arr["password"] != "")
		{
			$user=$oDbq->table($this->tbl_users)->fields("*")->where("`id`={$id}")->select()[0];
			$saltPassword=$user->saltPassword;
			$passwordHash=password_hash($arr["password"] . $saltPassword,PASSWORD_BCRYPT);
		}

		//PROFILE
		$fName=$arr["fName"];
		$lName=$arr["lName"];
		$dateOfBirth=@$arr["dateOfBirth"]; //time stamp format
		$country=@$arr["country"]; //IRAN
		$state=@$arr["state"];
		$city=@$arr["city"];
		$zipcode=@$arr["zipcode"];
		$address=$arr["address"];
		$phone=@$arr["phone"];
		$mobile=$arr["mobile"];

		if($arr["password"] != "")
		$oDbq->table($this->tbl_users)->fields("`password`")->values("'{$passwordHash}'")->where("`id`={$id}")->update();

		$oDbq->table($this->tbl_usersProfile)->fields("
			`fName`='{$fName}',
			`lName`='{$lName}',
			`dateOfBirth`='{$dateOfBirth}',
			`country`='{$country}',
			`state`='{$state}',
			`city`='{$city}',
			`zipcode`='{$zipcode}',
			`address`='{$address}',
			`phone`='{$phone}',
			`mobile`='{$mobile}'
		")->where("`userId`={$id}")->update();
		return $id;
	}//-----------------------------------------------------------------------
	function bankUpdate($argArray)
	{
		global $oDbq;

		$oDbq->table($this->tbl_usersProfile)->fields("
			`bankCardNumber`='{$argArray['bankCardNumber']}',
			`bankShaba`='{$argArray['bankShaba']}'
		")->where("`userId`={$argArray['id']}")->update();
	}//-----------------------------------------------------------------------	
	public function insertProfile ($arr)
	{
		global $oDbq;
		$oDbq->table($this->tbl_usersProfile)->set("
			`userId`={$arr['userId']},
			`userName`='{$arr['userName']}',
			`userType`='user',
			`fName`='{$arr['fName']}',
			`lName`='{$arr['lName']}',
			`description`='{$arr['description']}',
			`mail`='{$arr['mail']}',
			`mobile`='{$arr['mobile']}',
			`phone`='{$arr['phone']}'
		")->insert();
	}//--------------------------------------------------------------------------------------------------------------------	
   public function updateProfile ($arr)
	{
		global $oDbq;
		$oDbq->table($this->tbl_usersProfile)->set("
			`fName`={$arr['fName']},
			`lName`={$arr['lName']},
			`description`={$arr['description']},
			`mail`={$arr['mail']},
			`mobile`={$arr['mobile']},
			`phone`={$arr['phone']}
		")->where("`userId`={$arr['userId']}")->update();
	}//--------------------------------------------------------------------------------------------------------------------
	public function getActive($userNameId)
	{
		global $oDbq,$oPath;
		$ret= $oDbq->table($this->tbl_users)->fields("`active`")->where("`id`={$userNameId} OR `userName`='{$userNameId}'")->select()[0]->active;
	   if($ret==1)
			return true;
		else
			return false;
	}//------------------------------------------------------------------------------------------------------	
	public function setActive($userNameId,$value)
	{
		global $oDbq,$oPath;
		$oDbq->table($this->tbl_users)->set("`active`={$value}")->where("`id`='{$userNameId}' OR `userName`='{$userNameId}'")->update();
	}//------------------------------------------------------------------------------------------------------	
	public function isExistsByUserName($userNameId)
	{
		global $oDbq;
	   $ret=$oDbq->table($this->tbl_users)->where("`userName`='{$userNameId}' OR `id`={$userNameId}")->count();
      if($ret > 0)
		{
			return true;
		}
	   else
			return false;
	}//--------------------------------------------------------------------------------------------------------------------	
	public function changePassword($userId,$oldPassword,$newPassword) 
	{
		global $oDbq;
		$user=$oDbq->table($this->tbl_users)->where("`id`={$userId}")->select()[0];
		$oldPassword=$oldPassword . $user->saltPassword;
		if(password_verify($oldPassword,$user->password . $user->saltPassword))
		{
			$password=password_hash($newPassword . $user->saltPassword,PASSWORD_BCRYPT);
			$oDbq->table($this->tbl_users)->set("`password`='{$password}'")->where("`id`={$userId}")->update();
			return true;
		}
		else
			return false;
	}//--------------------------------------------------------------------------------------------------------------------	
	public function changePassword2($userNameId) 
	{
		global $oDbq;
		$userId=intval($userNameId);
		$user=@$oDbq->table($this->tbl_users)->where("`id`={$userId} OR `userName`='{$userNameId}'")->select()[0];
		if($user)
		{
			$newPassword=time();
			$password=password_hash($newPassword . $user->saltPassword,PASSWORD_BCRYPT);
			$oDbq->table($this->tbl_users)->set("`password`='{$password}'")->where("`id`={$userId} OR `userName`='{$userNameId}'")->update();
			return $newPassword;
		}
		else
			return false;
	}//--------------------------------------------------------------------------------------------------------------------		
	public function updateVerifyCode($userNameId,$code='')
	{
		global $oDbq;
		if(!$code) $code=substr(time(),6);
		$userId=intval($userNameId);
		$oDbq->table($this->tbl_users)->set("`verifyCode`='{$code}'")->where("`id`={$userId} OR `userName`='{$userNameId}'")->update();
	   return $code;
	}//--------------------------------------------------------------------------------------------------------------------
	public function getVerifyCode($userNameId)
	{
		global $oDbq;
		$userId=intval($userNameId);
		return @$oDbq->table($this->tbl_users)->fileds('`verifyCode`')->where("`id`={$userId} OR `userName`='{$userNameId}'")->select()[0]->verifyCode;
	}//--------------------------------------------------------------------------------------------------------------------
	public function activeByCode($userNameId,$code)
	{
		global $oDbq;
		$userId=intval($userNameId);
		$ret=$oDbq->table($this->tbl_users)->fields('`verifyCode`')->where("(`id`={$userId} OR `userName`='{$userNameId}') AND `verifyCode`='{$code}'")->select();
		if($ret)
		{
		   $oDbq->table($this->tbl_users)->set("`active`=1")->where("`id`={$userId} OR `userName`='{$userNameId}'")->update();
			return true;
		}
		else
			return false;
	}//-------------------------------------------------------------------------------------------------------------------
	public function login($userName,$password)
	{
	   global $oDbq;
		$user=@$oDbq->table($this->tbl_users)->fields('`userName`,`password`,`saltPassword`,`active`')->where("`userName`='{$userName}'")->select()[0];
		if($user)
		{
			if(password_verify($password . $user->saltPassword,$user->password))
			{
				if($user->active==1)
				{
					$token=uniqid();
					$date=time();
					$oDbq->table($this->tbl_users)->set("`loginToken`='{$token}',`dateLogin`={$date}")->where("`userName`='{$userName}'")->update();
					return $token;
				}
				else
				   return "!active";
			}
			else
				return "!password";
		}
		else
		   return "!userName";
	}//--------------------------------------------------------------------------------------------------------------------
	public function login2($userName)
	{
	   global $oDbq;
		$ret=@$oDbq->table($this->tbl_users)->fields('`userName`')->where("`userName`='{$userName}'")->select()[0];
		if($ret)
		{
			$token=uniqid();
			$date=time();
			$oDbq->table($this->tbl_users)->set("`loginToken`='{$token}',`dateLogin`={$date}")->where("`userName`='{$userName}'")->update();	
			return $token;
		}
	}//--------------------------------------------------------------------------------------------------------------------
	function register($userName,$password,$fName)
	{
		global $oDbq;
		$user=$oDbq->table($this->tbl_users)->fields("*")->where("`userName`='{$userName}'")->select();
		if(count($user) > 0)
		{
			return "!userName";
			exit;
		}
		else
		{
			$id=time();
			$saltPassword=intval(rand(5000,150000)) . "IRANNARI";
			$password=password_hash($password . $saltPassword,PASSWORD_BCRYPT);
			$verifyCode=substr($id,6);

			$oDbq->table($this->tbl_users)->set("
				`id`={$id},
				`userName`='{$userName}',
				`password`='{$password}',
				`saltPassword`='{$saltPassword}',
				`dateRegister`='{$id}',
				`active`='2',
				`verifyCode`='{$verifyCode}'
			")->insert();
			
			$oDbq->table($this->tbl_usersProfile)->fields("
				`id`={$id},
				`userId`={$id},
				`userType`='user',
				`userName`='{$userName}',
				`mobile`='',
				`mail`='',
				`fName`='{$fName}'
			")->insert();
			return $verifyCode;
		}
	}//-----------------------------------------------------------------------	
	public function checkLoginByToken($userId,$loginToken,$refreshToken=false)
	{
		global $oDbq;
		$ret=$oDbq->table($this->tbl_users)->where("`id`={$userId} AND `loginToken`='{$loginToken}'")->count();
		if($ret > 0)
		{
			if($refreshToken==true)
			{
				$token=uniqid();
				$oDbq->table($this->tbl_users)->set("`loginToken`='{$token}'")->where("`id`='{$userId}'")->update();
				return $token;
			}
			else
				return $loginToken;
		}
		else
			return false;
	}//--------------------------------------------------------------------------------------------------------------------	
	public function createToken($userNameId)
	{
		global $oDbq;
		$token=uniqid();
		$userId=$oDbq->table($this->tbl_users)->fields("`id`")->where("`id`='{$userNameId}' OR `userName`='{$userNameId}'")->select()[0]->id;
		$oDbq->table($this->tbl_users)->set("`loginToken`='{$token}'")->where("`id`='{$userNameId}' OR `userName`='{$userNameId}'")->update();
		return ["loginToken"=>$token, "id"=>$userId];
	}//--------------------------------------------------------------------------------------------------------------------	
	public function isAllowByPage($url)
	{
		@session_start(); 
		if(isset($_SESSION['user_isLogin']) && @$_SESSION['user_isLogin']==true)
		{
			$oUserMenu=new cUserMenu();
			$menuId=@$oUserMenu->selectByUrl($url)->id;
			if(!$menuId) return false; else return true;
		}
		else
		{
			return false;
		}
	}//-----------------------------------------------------------------------
	public function isAllowed($urlOrIdName,$byUrl=true) //check login and pageAllow
	{
		@session_start();
		global $oDbq;
		if(@$_SESSION['user_isLogin'])
		{
			$oUsersMenu=new cUsersMenu();
			if($byUrl==true)
				return $oUsersMenu->getByUrl($urlOrIdName);
			else
				return $oUsersMenu->getByIdName($urlOrIdName);
		}
		else
			return false;
	}//--------------------------------------------------------------------------	
}
?>